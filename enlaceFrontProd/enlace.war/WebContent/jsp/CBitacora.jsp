<html><!-- #BeginTemplate "/Templates/principal.dwt" -->
<head>
<!-- #BeginEditable "doctitle" -->
<title>Enlace</title>
<!-- #EndEditable -->

<!-- #BeginEditable "MetaTags" -->
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s25290">
<meta name="Proyecto" content="Portal">
<meta name="Version" content="1.0">
<meta name="Ultima version" content="27/04/2001 18:00">
<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico">
<meta http-equiv="pragma" content="no-cache"/>
<!-- #EndEditable -->

<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Mon Apr 23 20:21:38 GMT-0600 2001-->
<script type="text/javascript">
	var contextPath = "${pageContext.request.contextPath}";
</script>
<script type ="text/javascript" SRC= "/EnlaceMig/Bitacora.js"></script>
<script type ="text/javascript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/validacionesFiltroCuentas.js"></script>
<script type ="text/javascript" >

<%= request.getAttribute("newMenu") %>

var js_diasInhabiles="<%= request.getAttribute("diasInhabiles") %>";
var Nmuestra = 30;
//<%= request.getAttribute( "varpaginacion" ) %>;

function atras(){
    if((parseInt(document.frmbit.prev.value) - Nmuestra)>0){
      document.frmbit.next.value = document.frmbit.prev.value;
      document.frmbit.prev.value = parseInt(document.frmbit.prev.value) - Nmuestra;
      document.frmbit.action = "GetBit";
      document.frmbit.submit();
    }
}

function adelante(){
 if(parseInt(document.frmbit.next.value) != parseInt(document.frmbit.total.value)){
    if((parseInt(document.frmbit.next.value) + Nmuestra)<= parseInt(document.frmbit.total.value)){
      document.frmbit.prev.value = document.frmbit.next.value;
      document.frmbit.next.value = parseInt(document.frmbit.next.value) + Nmuestra;
       document.frmbit.action = "GetBit";
      document.frmbit.submit();
     }else if((parseInt(document.frmbit.next.value) + Nmuestra) > parseInt(document.frmbit.total.value)){
      document.frmbit.prev.value = document.frmbit.next.value;
      document.frmbit.next.value = parseInt(document.frmbit.next.value) + (parseInt(document.frmbit.total.value) - parseInt(document.frmbit.next.value));
      document.frmbit.action = "GetBit";
      document.frmbit.submit();
     }
 }else{
   //alert("no hay mas registros");
   cuadroDialogo("No hay m&aacute;s registros.",3);
 }

 }


 function continua()
 {
 }






 function WindowCalendar(txtFecha, pag){
  if(!document.Frmgetinfobit.deldia[0].checked){
    var m=new Date();
    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
    n=m.getMonth();
    document.Frmgetinfobit.campoFecha.value = txtFecha;
    msg=window.open("/EnlaceMig/"+ pag +".html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    msg.focus();
  }else{
	cuadroDialogo("La fecha no se puede cambiar para consultar la bit&aacute;cora del d&iacute;a",3);
  }
}



//-- *********************************************** -->
//-- modificación para integración pva 07/03/2002    -->
//-- *********************************************** -->
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}

//function actualizacuenta()
//{
  //document.Frmgetinfobit.cuenta.value=ctaselec;
  //document.Frmgetinfobit.textcuenta.value=ctaselec+" "+ctadescr;
//}

//function EnfSelCta()
//{
  // document.Frmgetinfobit.textcuenta.value="";
//}


function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>

<!---------------------------------------------------------------------------------------------->


<script type ="text/javascript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%
SimpleDateFormat formaHora = new SimpleDateFormat("yyyyMMddHHmmss");
%>

<script type ="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type ="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type ="text/javascript" src="/EnlaceMig/jquery-1.11.2.min.js"></script>
<script src="/EnlaceMig/json3.min.js" type="text/javascript"></script>
<script type ="text/javascript" src="/EnlaceMig/consultaCuentas.js?<%=formaHora.format(new Date())%>"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>

<body style="background-image:url('/gifs/EnlaceMig/gfo25010.gif'); background-color: #ffffff;" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onLoad="document.Frmgetinfobit.deldia[0].checked =true;PutDate(); MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');reiniciaCuentas();" >

<!-- #BeginLibraryItem "/Library/navegador.lbi" -->
<table width="571" border="0" cellspacing="0" cellpadding="0">
  <tr valign=top>
    <td width="*">
		<!-- MENU PRINCIPAL -->
        <%= request.getAttribute("MenuPrincipal") %>
    </td>
  </tr>
</table>
<!-- #EndLibraryItem --><!-- #BeginEditable "Contenido" -->

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="676" valign="top" align="left">
      <table width="666" border="0" cellspacing="6" cellpadding="0">
        <tr>
          <td width="528" valign="top" class="titpag"><%= request.getAttribute("Encabezado") %></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<!-- #BeginLibraryItem "/Library/contrato.lbi" -->

<form name = "Frmfechas">
  <%= request.getAttribute("Bitfechas") %>
</form>
<form name="Frmgetinfobit"  action="ConsultaBitacora">
<table width="760" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table width="670" border="0" cellspacing="2" cellpadding="3" bgcolor="#FFFFFF">
          <tr>
            <td class="tittabdat" width="326">
              <input type="radio" name="deldia" value="NORM" checked onClick="PutDate();"/>
              Bit&aacute;cora del d&iacute;a</td>
            <td class="tittabdat" width="326">
              <input type="radio" name="deldia" value="HIST" onCLick="PutDate1();"/>
              Bit&aacute;cora hist&oacute;rica</td>
          </tr>
          <tr>
          	<td colspan="2">
          		<div style="width: 100%">
						<table width="100%" cellpadding="5" class="textabdatcla">
							<tbody>
								<tr>
									<td colspan="4" class="tabmovtexbol"><b>B&uacute;squeda de cuentas:</b></td>
								</tr>
								<tr>
									<td align="right" class="tabmovtexbol">Cuenta:</td>
									<td width="200" colspan="2" class="tabmovtexbol">
										<input type="text" name="numeroCuenta0" id="numeroCuenta0" onblur="validaSoloNumeros(this,'Cuenta');"  class="numeroCuenta"/>
									</td>
									<td width="90" align="center" rowspan="2">
										<a href="javascript:consultaCuentas();">
											<img width="90" height="22" border="0" alt="Consultar" src="/gifs/EnlaceMig/Ir.png"/>
										</a>
									</td>
								</tr>
								<tr>
									<td width="100" nowrap="" align="right" class="tabmovtexbol">Descripci&oacute;n cuenta:</td>
									<td width="200" colspan="2" class="tabmovtexbol"><input type="text" name="descripcionCuenta0" id="descripcionCuenta0" onblur="validaEspeciales(this,'Descripci&oacute;n cuenta');" class="descripcionCuenta"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</td>
          </tr>
          <tr align="center">
            <td class="textabdatcla" valign="top" colspan="2">
              <table width="660" border="0" cellspacing="0" cellpadding="0">
                <tr valign="top">
                  <td width="420" align="left">
                    <table width="420" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td align="right" class="tabmovtex" width="140" nowrap>Usuario
                          (opcional): <br>
                        </td>
                        <td class="tabmovtex" width="260" nowrap>
                          <select name="usuario" class="tabmovtex">
                            <option value="" selected>Seleccione un usuario</option>
                            <%= request.getAttribute("Usuarios") %>
                          </select>
                        </td>
                      </tr>
                      <!-- modificacion para integracion
					    <tr>
                        <td align="right" class="tabmovtex" nowrap>Cuenta (opcional):</td>
                        <td class="tabmovtex" nowrap>
                          <select name="cuenta" class="tabmovtex">
                            <option value="" selected>Seleccione una cuenta</option>
                            <%= request.getAttribute("Cuentas") %>
                          </select>
                        </td>
                        </tr>
                      -->
                      <tr>
                        <td class="tabmovtex" colspan="2" align="right" nowrap>
	  				      <%@ include file="/jsp/listaCuentas.jspf" %>
					      <input id="cuenta" type="hidden" name="cuenta" value=""/>
					      <input id="ventana" name="ventana" type="hidden" value="2"/>
							<input id="opcion" name="opcion" type="hidden" value="1"/>
                        </td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex">De la fecha:<br>
                        </td>
                        <td class="tabmovtex" nowrap valign="middle">
                          <input id = "fecha1" type="text" name="fecha1" size="12" class="tabmovtex" OnFocus = "blur();"/>
                          <a href="javascript:WindowCalendar('fecha1', 'calBit3' );"><img src="/gifs/EnlaceMig/gbo25410.gif" style="border:0;" width="12" height="14" align="absmiddle"></a></td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex">A la fecha:</td>
                        <td class="tabmovtex" nowrap>
                          <input id= "fecha2" type="text" name="fecha2" size="12" class="tabmovtex" OnFocus = "blur();"/>
                          <a href="javascript:WindowCalendar('fecha2', 'calBit13');"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"/></a></td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex">Rango de importes:</td>
                        <td class="tabmovtex" style="text-align: left;" nowrap>
                        	<div style="display: inline;margin-left: 0px;">
                        		De: <input type="text" name="ImporteDe" size="15" maxlength=15 class="tabmovtex" onBlur = "return checkFloatValue(this);">
                        	</div>
                        	<div style="display: inline;margin-left: 5px;">
    	                    	A: <input type="text" name="ImporteA" size="15" maxlength=15 class="tabmovtex" onBlur = "return checkFloatValue(this);"/>
                        	</div>
                        </td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex">Referencia:</td>
                        <td class="tabmovtex" nowrap>
                          <input type="text" name="Refe" size="15" maxlength=12 class="tabmovtex" onBlur = "checkValue(this);">
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td width="240" align="left">
                    <table width="240" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="right" class="tabmovtex" width="107" nowrap>Tipo
                          de operaci&oacute;n:</td>
                        <td class="tabmovtex" width="15" valign="middle" align="center">
                          <input type="checkbox" name="ChkTranf" value="true"/>
                        </td>
                        <td class="tabmovtex" width="100" nowrap>Transferencia</td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
                        <td class="tabmovtex" align="center" valign="middle">
                          <input type="checkbox" name="ChkInversiones" value="true"/>
                        </td>
                        <td class="tabmovtex" nowrap>Inversiones </td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
                        <td class="tabmovtex" align="center" valign="middle">
                          <input type="checkbox" name="ChkInterbancario" value="true"/>
                        </td>
                        <td class="tabmovtex" nowrap>Interbancario</td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
                        <td class="tabmovtex" align="center" valign="middle">
                          <input type="checkbox" name="ChkSUA" value="true"/>
                        </td>
                        <td class="tabmovtex" nowrap>S.U.A. L&iacute;nea  de Captura</td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex" nowrap>Estatus:</td>
                        <td class="tabmovtex" align="center" valign="middle">
                          <input type="checkbox" name="ChkAceptadas" value="true">
                        </td>
                        <td class="tabmovtex" nowrap>Aceptadas</td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
                        <td class="tabmovtex" align="center" valign="middle">
                          <input type="checkbox" name="ChkRechazadas" value="true"/>
                        </td>
                        <td class="tabmovtex" nowrap>Rechazadas</td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
                        <td class="tabmovtex" align="center" valign="middle">
                          <input type="checkbox" name="ChkProgramadas" value="true"/>
                        </td>
                        <td class="tabmovtex" nowrap>Programadas</td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
                        <td class="tabmovtex" align="center" valign="middle">
                          <input type="checkbox" name="ChkPendientes" value="true"/>
                        </td>
                        <td class="tabmovtex" nowrap>Pendientes</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <br>
        <table>
          <tr id="enviar2" align="center" style="visibility:hidden" class="tabmovtex">
          	<td colspan=2>Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>
          </tr>
          <tr id="enviar">
            <td align="right" valign="top" height="22" width="90">
              <a href="javascript:DoPost();">
				<img border="0" name=Enviar value=Enviar src="/gifs/EnlaceMig/gbo25220.gif" width="90" height="22" alt="Consultar"/>
			  </a>
			</td>
			<td align="left" valign="middle" height="22" width="76">
			  <a href="javascript:FrmClean();">
				<img name=limpiar value=Limpiar border="0" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar"/>
			  </a>
			</td>
          </tr>
        </table>
      </td>
    </tr>
</table>
<input id = "campoFecha" type="hidden" value = "fecha1"/>
</form>
<!-- #EndEditable -->
</body>
<!-- #EndTemplate --></html>