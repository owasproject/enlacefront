<%
  /**
     Author: Daniel C�zares Beltr�n (ASTECI)
     Version: 1.1.1
     Create Date: 03-2002
     Modify Date: 12-04-2002

     08-11-2002 Update
     Se elimin� la lupa de cuentas y en su lugar se insert�
     un Combo Box que filtra las cuentas de Base Cero, mostrando
     s�lo las hijas.
  */

  //Some values for JavaScript
  char chrSlash = '/';
  char chrSemicolon = ';';
  char chrPageBrk = '\n';
  char chr2Comma = '"';

  //Strings for both Date Input Text
  String strDay = (String) (request.getAttribute("day"));
  String strMonth = (String) (request.getAttribute("month"));
  String strYear = (String)(request.getAttribute("year"));
  String strToday = strDay + chrSlash + strMonth + chrSlash + strYear;

  //Some values for Java Script
  String strDayJS = strDay + chr2Comma + chrSemicolon + chrPageBrk;
  String strMonthJS = strMonth + chr2Comma + chrSemicolon + chrPageBrk;
  String strYearJS = strYear + chr2Comma + chrSemicolon + chrPageBrk;

  String StrAccountOptions = (String)(request.getAttribute("ComboMtoEst"));
%>
<HTML>
<HEAD>
<TITLE>Enlace</TITLE>
<meta http-equiv="pragma" content="no-cache">
<!-- Include menu on page -->
<SCRIPT LANGUAGE="JavaScript" SRC="/EnlaceMig/fw_menu.js">
</SCRIPT>
<!-- Code to display a Dialog Window on error  -->
<Script Language = "JavaScript" SRC="/EnlaceMig/cuadroDialogo.js">
</Script>
<SCRIPT LANGUAGE="JavaScript">
<!--
/**Load images.
*/
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n];
	for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments;
  document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
/*****************************************/
<!-- Include Java Script code for menus -->
<%= request.getAttribute("newMenu") %>
/*****************************************/
var js_diasInhabiles = "<%= request.getAttribute("notWorkingDates") %>
var msgErrorDays = "<%= (String) (request.getAttribute("msgErrorDays")) %>
var msgErrorDates = "La fecha inicial debe ser previa a la fecha final";

var msgErrorAccNull = "No existen cuentas";



var msgErrorLimitDate = "La fecha l&iacute;mite es " + "<%= (String) (request.getAttribute("limitDate")) %>";
var dia = "<%= strDayJS %>
var mes = "<%= strMonthJS %>
var anio = "<%= strYearJS %>
var fechaseleccionada;
var txtBoxSelected = 0;



/**Opens calendar window.
   numTxtBox has a number asociated with a TextBox in which
	 its date must be updated.
*/
function WindowCalendar(numTxtBox){


	strProperties = "toolbar=no,location=no,directories=no, " +
                        "status=no,menubar=no,scrollbars=yes,resizable=no," +
                        "width=300,height=260";

	if(js_diasInhabiles != ""){
	   txtBoxSelected = numTxtBox;
	   msg=window.open("/EnlaceMig/calConSaldosBC2.html #Fin",
			    		      "calendario",strProperties);
	}else{
	   cuadroDialogo(msgErrorDays,3);
	}
}

/**this function copies date selected by the user on the calendar to text box dates.
   !!!!  FUNCTION'S NAME MUST NOT BE CHANGED !!!!!
*/
function Actualiza(){


   if(txtBoxSelected == 1){
      document.FrmQuery.FirstDate.value = fechaseleccionada;
   } else{
      if(txtBoxSelected == 2){
          document.FrmQuery.LastDate.value = fechaseleccionada;
      }
   }
}


/**
   Validates a form and then submit it.
	 Param Frm is a Form Object.
*/
function doSubmit(Frm){
   AccCboBox = Frm.Account;

   if(AccCboBox.length == 0){
       cuadroDialogo(msgErrorAccNull,3);
   }else{
       code = valiDate(Frm);

       if(code == 0){
           Frm.submit();
       }else{
           if(code == -1){
               cuadroDialogo(msgErrorDates,3);
           }else{
               if(code == -2){
                   cuadroDialogo(msgErrorLimitDate,3);
               }
           }
       }
   }

   return;
}





/**
   Parses a text date in "dd/mm/yyyy" format.
	 Param txtDate is a TextBox object.
	 Return Date
*/
function parseDate(txtDate,ParmDate){


   strDate = txtDate;
   firstSlash = strDate.indexOf("/");
   lastSlash = strDate.lastIndexOf("/");

   dtReturn = new Date();
   dtReturn.setDate(1);
   dtReturn.setMonth(strDate.substring(firstSlash+1,lastSlash));
   //Months in JavaScript begin in 0(January)
   dtReturn.setMonth(dtReturn.getMonth()-1);
   dtReturn.setDate(strDate.substring(0,firstSlash));
   dtReturn.setYear(strDate.substring(lastSlash+1,strDate.length));

   ParmDate = dtReturn;


   return ParmDate;
}

/**
   Validate two Dates.
   Param Frm is a Form Object
*/
function valiDate(Frm){

   //Builds first Date
   var firstDate = new Date();
   var lastDate = new Date();
   var limitDate = new Date();

   //parse dates
   firstDate = parseDate(Frm.FirstDate.value,firstDate);
   lastDate = parseDate(Frm.LastDate.value,lastDate);
   limitDate = parseDate(Frm.LimitDate.value,limitDate);


   if(compareDates(firstDate,lastDate) == 1){
       //alert("error -1 \n La fecha inicial debe ser menor que la final");
       //alert("FirstDate:" + firstDate + " > " + " LastDate:" + lastDate);
       return -1;
   }else{
       if(compareDates(limitDate,firstDate) == 2 ||
          compareDates(limitDate,firstDate) == 3){

           //alert("ok");
           //alert("Limite=" + limitDate + " <= " + " FirstDate=" + firstDate);
           return 0;
       }else{
           if(compareDates(limitDate,firstDate) == 1){
               //alert("La fecha inicial es previa que la fecha limite");
               //alert("limite="+ limitDate + " > " + " FirstDate=" + firstDate);
               return -2;
           }
       }
   }
}

/**
   Compares Date2 to Date1.
	 Parameters day is converted to the year day,
	 instead month day.

	 Each month has 30 days in this function.
	 Months in JavaScript are between 0(January) and
	   11(December)

	 i.e.

	 23/09/2002 is converted to 263-2002
	 02/03/2003 is converted to 62-2003

   @return IntReturn. Returns an integer.
	         1: Date2 > Date1
                 2: Date2 < Date1
                 3: Date2 = Date1
*/
function compareDates(Date2,Date1){
   yy2 = Date2.getYear();
   mm2 = Date2.getMonth();
   dd2 = Date2.getDate();

   yy1 = Date1.getYear();
   mm1 = Date1.getMonth();
   dd1 = Date1.getDate();

   days1 = 0;
   days2 = 0;

   var IntReturn = 0;

   if(yy2 > yy1){
       IntReturn = 1;
   }else{
       if(yy2 < yy1){
           IntReturn = 2;
       }else{
           if(yy2 == yy1){
               days2 = dd2;
               days2+= 30 * mm2;

               days1 = dd1;
               days1+= 30 * mm1;

               //Compare days
               if(days2 > days1){
                   IntReturn = 1;
               }else{
                   if(days2 < days1){
                       IntReturn = 2;
                   }else{
                       if(days2 == days1){
                           IntReturn = 3;
                       }
                   }
               }
           }
       }
   }

   return IntReturn;
}//end compareDates(Date2,Date1)

//-->
</SCRIPT>
<LINK REL="stylesheet" HREF="/EnlaceMig/consultas.css" TYPE="text/css">
</HEAD>

<BODY topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
 BGCOLOR="#ffffff"
 onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif',
			  '/gifs/EnlaceMig/gbo25111.gif',
			  '/gifs/EnlaceMig/gbo25151.gif',
			  '/gifs/EnlaceMig/gbo25031.gif',
			  '/gifs/EnlaceMig/gbo25032.gif',
			  '/gifs/EnlaceMig/gbo25051.gif',
			  '/gifs/EnlaceMig/gbo25052.gif',
			  '/gifs/EnlaceMig/gbo25091.gif',
			  '/gifs/EnlaceMig/gbo25092.gif',
			  '/gifs/EnlaceMig/gbo25012.gif',
			  '/gifs/EnlaceMig/gbo25071.gif',
			  '/gifs/EnlaceMig/gbo25072.gif',
			  '/gifs/EnlaceMig/gbo25011.gif');"
 background="/gifs/EnlaceMig/gfo25010.gif">


<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- HTML MAIN MENU -->
	   <%= request.getAttribute("MenuPrincipal") %>
    </td>
  </TR>
</TABLE>

<!-- Table with all the options user has selected to
     arrive this page. For instance "Servicios > Chequera Seguridad >
		 Registro de Cheques > En L�nea".
-->
<%= request.getAttribute("Encabezado" ) %>

<!-- ***** HERE BEGINS MAIN FORM OF THIS PAGE *****-->
<!-- Tab #1 -->
<table width="760" border="0" cellspacing="0" cellpadding="0">
<FORM NAME="FrmQuery" METHOD=post Action="bcConSaldos">
   <tr>
      <td align="center">
         <!-- Tab #2 -->
         <table width="390" border="0" cellspacing="2" cellpadding="3">
            <tr>
               <td class="tittabdat">
                   Seleccione la cuenta y per&iacute;odo a consultar
               </TD>
            </tr>

            <tr align="center">
              <td class="textabdatcla" colspan="2">
                <!-- Table #3 -->
                 <table width="390" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                       <td align="right" class="tabmovtexbol" width="100" nowrap>
                           Cuenta:
                       </td>
                       <td width="290">

                              <select name="Account" class="tabmovtexbol">
<%= StrAccountOptions %>
                              </select>



                        </td>

                     </tr>

                     <tr>
                         <td class="tabmovtexbol">
                             Per&iacute;odo:
                         </td>
                     </tr>
                     <tr>
                         <td align="right" class="tabmovtexbol" nowrap>
                             De la fecha:
                         </td>
                         <td class="tabmovtex" nowrap>
                             <input type="text" name="FirstDate" size="12"
                              class="tabmovtexbol"
                              value="<%= strToday %>" onfocus="blur();">

                                 <A href ="javascript:WindowCalendar(1);" border=0> <img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle" ALT="Calendario"></A>
                         </td>
                     </tr>
                     <tr>
                         <td align="right" class="tabmovtexbol" nowrap>
                             A la fecha:
                         </td>
                         <td class="tabmovtex" nowrap>
                             <input type="text" name="LastDate" size="12"
                              class="tabmovtexbol"
                              value="<%= strToday %>" onfocus="blur();">

                             <A href ="javascript:WindowCalendar(2);" border=0><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle" ALT="Calendario"></A>
                          </td>
                      </tr>
                  </table>
                  <!-- tab 3 -->
              </td><!-- tab 2 col 1 row 2 -->
          </tr> <!-- tab 2 row 2 -->
      </table> <!-- tab 2 -->

      <br>

        <table width="160" border="0" cellspacing="0" cellpadding="0"
         bgcolor="#FFFFFF" height="25">
        <tr>
            <td align="center" height="25">

                <A href="javascript:doSubmit(document.forms[0]);" border="0"><img  src = "/gifs/EnlaceMig/gbo25220.gif" border="0" ALT="Consultar" width="83" height="22"></A>

            </td>
        </tr>
        </table> <!-- tab 4-->

        </td><!-- tab 1 col 1-->
    </tr><!-- tab 1 row 1-->
<INPUT TYPE="hidden" NAME="event" VALUE=query>


<INPUT TYPE="Hidden" NAME="LimitDate" VALUE="<%= (String) (request.getAttribute("limitDate")) %>">
</form>
</table><!-- tab 1 -->
<!-- VERSION 1.1.2 -->
</BODY>
</HTML>