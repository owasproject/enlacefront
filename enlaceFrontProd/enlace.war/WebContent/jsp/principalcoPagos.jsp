<html>
<head>
<title>Banca Virtual</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<SCRIPT LANGUAGE="Javascript">
var facultades="<%= request.getAttribute( "facultades" ) %>"

function archivoErrores()
 {
  ventanaInfo1=window.open('','trainerWindow','width=450,height=300,toolbar=no,scrollbars=yes');
   ventanaInfo1.document.open();
   ventanaInfo1.document.write("<html>");
   ventanaInfo1.document.writeln("<head>\n<title>Estatus</title>\n");
   ventanaInfo1.document.writeln("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
   ventanaInfo1.document.writeln("</head>");
   ventanaInfo1.document.writeln("<body bgcolor='white'>");
   ventanaInfo1.document.writeln("<form>");
   ventanaInfo1.document.writeln("<table border=0 width=420 class='textabdatcla' align=center>");
   ventanaInfo1.document.writeln("<tr><th class='tittabdat'>Informaci&oacute;n del Archivo Importado</th></tr>");
   ventanaInfo1.document.writeln("<tr><td class='tabmovtex1' align=center>");
   ventanaInfo1.document.writeln("<table border=0 align=center><tr><td class='tabmovtex1' align=center width=0>");
   ventanaInfo1.document.writeln("<img src='/gifs/EnlaceMig/gic25060.gif'></td>");
   ventanaInfo1.document.writeln("<td class='tabmovtex1' align='center'><br>No se llevo a cabo la importaci&oacute;n ya que el archivo seleccionado tiene los siguientes errores.<br><br>");
   ventanaInfo1.document.writeln("</td></tr></table>");
   ventanaInfo1.document.writeln("</td></tr>");
   ventanaInfo1.document.writeln("<tr><td class='tabmovtex1'>");
   ventanaInfo1.document.writeln(document.ControlPagos.archivoEstatus.value+"<br><br>");
   ventanaInfo1.document.writeln("</td></tr>");
   ventanaInfo1.document.writeln("</table>");
   ventanaInfo1.document.writeln("<table border=0 align=center >");
   ventanaInfo1.document.writeln("<tr><td align=center><br><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0 alt='Cerrar'></a>");
   ventanaInfo1.document.writeln("<a href = 'javascript:window.print();'><img border='0' name='imageField3' src='/gifs/EnlaceMig/gbo25240.gif' width='83' height='22' alt='Imprimir'></a></td></tr>");
   ventanaInfo1.document.writeln("</table>");
   ventanaInfo1.document.writeln("</form>");
   ventanaInfo1.document.writeln("</body>\n</html>");
   ventanaInfo1.document.close();
   ventanaInfo1.focus();
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function js_importar(){

	if (facultades.indexOf("CCIMPARCHPA") ==-1)
		{
			cuadroDialogo ("No tiene facultades para importar archivos de Pagos",1);
			return ;
		}

	if (document.ControlPagos.archivoempleados.value=="") cuadroDialogo("Seleccione primero el archivo a importar con el boton Browse ...",1);
	else {
		document.ControlPagos.operacion.value="importar";
		document.ControlPagos.action="coPagos?opcion=1";
		document.ControlPagos.submit();
		}
}

function js_baja(){

	if (facultades.indexOf("CCBAJLOCPAG") ==-1)
		{
			cuadroDialogo ("No tiene facultades para dar de baja Pagos",1)
			return ;
		}

	if (document.ControlPagos.tipo_archivo.value=="enviado"){
		cuadroDialogo("El archivo ya ha sido enviado",1);
		return;
	}


	if (document.ControlPagos.tipo_archivo.value=="recuperado"){
		cuadroDialogo("No puede dar de baja en un archivo recuperado",1);
		return;
	}



	var tipo = new String( document.ControlPagos.tipo_archivo.value );
	if ( tipo.substring( tipo.indexOf("importado"), tipo.indexOf("importado") + 9 ) =="importado"){
		cuadroDialogo("No puede ejecutar esta operaci&oacute;n en un archivo importado",1);
		return;
	}
	if (!document.ControlPagos.archivo_actual.value==""){
		var seleccion=false;
		for (i=0;i<document.ControlPagos.elements.length;i++) {
		    if (document.ControlPagos[i].type=="radio")
			  	if (document.ControlPagos[i].checked) {
	    			document.ControlPagos.registro.value=document.ControlPagos[i].value;
		    		seleccion=true;
					break;
				}
		   }
			if (seleccion){
				document.ControlPagos.operacion.value="baja";
				document.ControlPagos.action="coPagos?opcion=2";
				document.ControlPagos.submit();
			}
			else {cuadroDialogo('Seleccione un registro por favor'),1}
		}
	else cuadroDialogo ("Seleccione Nuevo para crear un nuevo archivo \no Importar para dar de alta un archivo existente",1);
}

function js_alta(){
	if (facultades.indexOf("CCALTMANPAG") ==-1)
		{
			cuadroDialogo ("No tiene facultades para dar de alta Pagos",1)
			return ;
		}

	if (document.ControlPagos.tipo_archivo.value=="enviado"){
		cuadroDialogo("El archivo ya ha sido enviado",1);
		return;
	}

if (document.ControlPagos.tipo_archivo.value=="recuperado"){
		cuadroDialogo("No puede dar de alta sobre un archivo recuperado",1);
		return;
	}

	if (document.ControlPagos.archivo_actual.value=="")
		{
			cuadroDialogo ("Seleccione Nuevo para crear un nuevo archivo \no Importar para dar de alta un archivo existente",1);
			return;
		}
	var tipo = new String( document.ControlPagos.tipo_archivo.value );
	if ( tipo.substring( tipo.indexOf("importado"), tipo.indexOf("importado") + 9 ) =="importado"){
		cuadroDialogo("No puede ejecutar esta operaci&oacute;n en un archivo importado",1);
		return;
	}

	var js_total=parseInt(document.ControlPagos.total_registros.value);
	var js_max=parseInt(document.ControlPagos.max_registros.value);

	if (isNaN(js_total) || isNaN(js_max))
		{
			cuadroDialogo ("No es posible realizar el alta",3);
			return ;
		}

	if (js_total >= 30){
		cuadroDialogo ("Solo puede dar de alta "+ document.ControlPagos.max_registros.value +" registros",1);
		return ;
	}

		document.ControlPagos.operacion.value="alta";
		document.ControlPagos.action="coPagos?opcion=2";
		document.ControlPagos.submit();
}

function js_envio(){
	if (facultades.indexOf("CCENVARCHPA") ==-1)
		{
			cuadroDialogo ("No tiene facultades para enviar archivos de Pagos",1)
			return ;
		}

	var js_total=parseInt(document.ControlPagos.total_registros.value);
	var js_max=parseInt(document.ControlPagos.max_registros.value);

	if (isNaN(js_total) || isNaN(js_max))
		{
			cuadroDialogo ("No hay informacion para enviar",3);
			return ;
		}

	if (document.ControlPagos.tipo_archivo.value=="enviado"){
		cuadroDialogo("El archivo ya ha sido enviado",1)
		return;
	}

	if (document.ControlPagos.tipo_archivo.value=="recuperado"){
		cuadroDialogo("No puede enviar un archivo recuperado",1);
		return;
	}



	if (!document.ControlPagos.archivo_actual.value==""){
		document.ControlPagos.operacion.value="envio";
		document.ControlPagos.action="coPagos?opcion=2"
		document.ControlPagos.submit();
		}
	else cuadroDialogo ("Seleccione Nuevo para crear un nuevo archivo \no Importar para dar de alta un archivo existente",1);
}



function js_modificacion(){

	if (facultades.indexOf("CCMODLOCPAG") ==-1)
		{
			cuadroDialogo ("No tiene facultades para modificar Pagos",1)
			return ;
		}


	if (document.ControlPagos.tipo_archivo.value=="enviado"){
		cuadroDialogo("El archivo ya ha sido enviado",1)
		return;
	}

	if (document.ControlPagos.tipo_archivo.value=="recuperado"){
		cuadroDialogo("No puede modificar un archivo recuperado",1);
		return;
	}

	var tipo = new String( document.ControlPagos.tipo_archivo.value );
	if ( tipo.substring( tipo.indexOf("importado"), tipo.indexOf("importado") + 9 ) =="importado"){
		cuadroDialogo("No puede ejecutar esta operaci&oacute;n en un archivo importado",1)
		return;
	}

	if (!document.ControlPagos.archivo_actual.value==""){
		var seleccion=false;
		      for (i=0;i<document.ControlPagos.elements.length;i++) {
		      if (document.ControlPagos[i].type=="radio")
			  	if (document.ControlPagos[i].checked) {
						document.ControlPagos.registro.value=document.ControlPagos[i].value;
						seleccion=true;
						break;
				}
		   }

			if (seleccion){
				document.ControlPagos.operacion.value="modificacion";
				document.ControlPagos.action="coPagos?opcion=2"
				document.ControlPagos.submit();
			}
			else {cuadroDialogo('Seleccione un registro por favor',1)}
		}
	else cuadroDialogo ("Seleccione Nuevo para crear un nuevo archivo \no Importar para dar de alta un archivo existente",1);
}

function js_carga(){
	document.ControlPagos.operacion.value="carga";
    document.ControlPagos.carga_archivo.value=document.ControlPagos.archivos.options[document.ControlPagos.archivos.options.selectedIndex].text;
	document.ControlPagos.action=""
	document.ControlPagos.submit();
}


function js_recupera(){
	cuadroCaptura("Proporcione el numero de secuencia: ","Captura");
	document.ControlPagos.operacion.value="recuperar";
	document.ControlPagos.action="coPagos?opcion=2"
}
function js_nuevo(){

	cuadroCaptura("Proporcione el nombre del archivo a crear: ","Captura");
	document.ControlPagos.operacion.value="nuevo";
	document.ControlPagos.action="coPagos?opcion=1"
}

function continuaCaptura(){
	if (document.ControlPagos.operacion.value=="nuevo"){
		if (respuesta==1){
			nombre_nuevo=campoTexto;
			if (nombre_nuevo.length==0) cuadroDialogo ("Proporcione un nombre para el archivo",1)
				else {
						document.ControlPagos.nombre_nuevo.value=nombre_nuevo;
						document.ControlPagos.submit();
					}
			}
		}

	if (document.ControlPagos.operacion.value=="recuperar"){
		if (respuesta==1){
			if (campoTexto.length==0) cuadroDialogo ("Proporcione un valor numerico para la secuencia",1)
				else {
						secuencia=parseInt(campoTexto);
						document.ControlPagos.secuencia.value=secuencia;
						document.ControlPagos.submit();
					}
			}
		}
}


function js_exportar(){
	if (facultades.indexOf("CCEXPORARCH") ==-1)
		{
			cuadroDialogo ("No tiene facultades para exportar archivos de Pagos",1)
			return ;
		}

	if (document.ControlPagos.tipo_archivo.value=="recuperado"){
		cuadroDialogo("No puede Exportar un archivo recuperado",1);
		return;
	}


	if (!document.ControlPagos.archivo_actual.value=="" || document.ControlPagos.tipo_archivo.value=="recuperado"){
			document.ControlPagos.operacion.value="exportar";
			document.ControlPagos.action="coPagos?opcion=2";
			document.ControlPagos.submit();
		}
	else cuadroDialogo ("Seleccione Nuevo para crear un nuevo archivo \no Importar para dar de alta un archivo existente",1);


	var js_total=parseInt(document.ControlPagos.total_registros.value);
	var js_max=parseInt(document.ControlPagos.max_registros.value);

	if (isNaN(js_total) || isNaN(js_max))
		{
			cuadroDialogo ("No hay archivo para exportar.",1);
			return ;
		}



}








function js_borrar(){
	if (facultades.indexOf("CCELIMIARCH") ==-1)
		{
			cuadroDialogo ("No tiene facultades para borrar archivos de Pagos",1)
			return ;
		}



   var js_total=parseInt(document.ControlPagos.total_registros.value);
	var js_max=parseInt(document.ControlPagos.max_registros.value);

	if (isNaN(js_total) || isNaN(js_max))
		{
			cuadroDialogo ("No hay ningun archivo por eliminar.",1);
			return ;
		}


	cuadroDialogo ("Est&aacute; seguro que desea eliminar el archivo ?",2);
}


function continua(){
if (respuesta==1)
	{
		document.ControlPagos.operacion.value="borrar";
		document.ControlPagos.action="coPagos?opcion=2"
		document.ControlPagos.submit();
	}
}

function js_reporte(){
	if (facultades.indexOf("CCIMPRREPOR") ==-1)
		{
			cuadroDialogo ("No tiene facultades para imprimir reporte de archivos de Pagos",1)
			return ;
		}
	if (document.ControlPagos.tipo_archivo.value=="enviado"){
		cuadroDialogo("El archivo ya ha sido enviado, primero seleccione la opci&oacute;n Recuperar",1)
		return;
	}
	var js_total=parseInt(document.ControlPagos.total_registros.value);
	var js_max=parseInt(document.ControlPagos.max_registros.value);
	if (isNaN(js_total) || isNaN(js_max))
		{
			cuadroDialogo ("No es posible mostrar el reporte",1)
			return ;
		}
	if (!document.ControlPagos.archivo_actual.value=="" || document.ControlPagos.tipo_archivo.value=="recuperado"){
		if (js_total >= js_max){
			cuadroDialogo ("Solo puede ver hasta "+ js_max +" registros en el reporte, utilice la opcion Exportar",1)
			return ;
		}
		document.ControlPagos.operacion.value="reporte";
		document.ControlPagos.action="coPagos?opcion=2"
		document.ControlPagos.submit();
		}
	else cuadroDialogo ("No hay ningun archivo para crear Reporte.",1);
}
<%
     if (request.getAttribute("newMenu")!= null) {
     out.println(request.getAttribute("newMenu"));
	 }
%>
//-->
</SCRIPT>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</HEAD>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');<%
					if (request.getAttribute("ArchivoErr")!= null) {
						out.println(request.getAttribute("ArchivoErr"));
					}
				%>;" background="/gifs/EnlaceMig/gfo25010.gif" >

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
	<td width="*">
	<!-- MENU PRINCIPAL -->
	<%
	  			if (request.getAttribute("MenuPrincipal")!= null) {
      			out.println(request.getAttribute("MenuPrincipal"));
	  			}
	%>

	</TD>
  </TR>
</TABLE>
	<%
	  			if (request.getAttribute("Encabezado")!= null) {
      			out.println(request.getAttribute("Encabezado"));
	  			}
	%>
<br>
<FORM  NAME="ControlPagos" METHOD="POST" ACTION="coPagos">

<table width="760" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center"><!--Cambio a center-->
        <table width="680" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td rowspan="2" align="left" width="470">
              <table width="440" border="0" cellspacing="2" cellpadding="3">
                <tr>
                  <td class="tittabdat" align="left" colspan="2"> Datos del archivo</td>
                </tr>
                <tr align="center">
                  <td class="textabdatcla" valign="top" colspan="2">

                    <table width="430" border="0" cellspacing="0" cellpadding="0">
                      <tr valign="top">
                        <td align="right">
                          <table width="140" border="0" cellspacing="4" cellpadding="0">
                            <tr><td class="tabmovtex" nowrap>Archivo:</td></tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
                                <input type="text" name="archivo_actual" value="<%
	  								if (request.getAttribute("archivo_actual")!= null) {
      								out.println(request.getAttribute("archivo_actual"));
	  								}
								%>
                                " onfocus="blur()">
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td align="right">
                          <table width="140" border="0" cellspacing="4" cellpadding="0">
                            <tr><td class="tabmovtex" nowrap>N&uacute;mero de transmisi&oacute;n:</td></tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
                                <INPUT TYPE="text" SIZE="10" NAME="r_numtran" value="<%
	  								if (request.getAttribute("r_numtran")!= null) {
      								out.println(request.getAttribute("r_numtran"));
	  								}
								%>" onfocus="blur()">
                              </td>
                            </tr>
                            <tr><td class="tabmovtex" nowrap>Fecha de transmisi&oacute;n:</td></tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
                                <INPUT TYPE="text" SIZE="12" NAME="r_fchtran" value="<% if (request.getAttribute("r_fchtran")!= null) { out.println(request.getAttribute("r_fchtran")); } %>" onfocus="blur()">
                              </td>
                            </tr>
                            <tr><td class="tabmovtex" nowrap valign="middle">Fecha de actualizaci&oacute;n:</td></tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">
                                <INPUT TYPE="text" SIZE="12" NAME="r_fchact" value="<% if (request.getAttribute("r_fchact")!= null) { out.println(request.getAttribute("r_fchact")); } %>" onfocus="blur()">
                              </td>
                            </tr>
							 <tr><td class="tabmovtex" nowrap valign="middle">
							  Folio de autorizaci&oacute;n:</td></tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">
                                <INPUT TYPE="text" SIZE="12" NAME="f_actualiza" value="" >
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td align="right">
                          <table width="140" border="0" cellspacing="4" cellpadding="0">
                            <tr><td class="tabmovtex" nowrap>Total de registros:</td></tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
                                <INPUT TYPE="text" SIZE="10" NAME="text29" value="<% if (request.getAttribute("r_totregs")!= null) { out.println(request.getAttribute("r_totregs")); } %>" onfocus="blur()">
                              </td>
                            </tr>
                            <tr><td class="tabmovtex" nowrap>Registros aceptados:</td></tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
                                <INPUT TYPE="text" SIZE="10" NAME="text31" value="<% if (request.getAttribute("r_acept")!= null) { out.println(request.getAttribute("r_acept")); } %>" onfocus="blur()">
                              </td>
                            </tr>
                            <tr><td class="tabmovtex" nowrap valign="middle">Registros rechazados: </td></tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">
                                <INPUT TYPE="text" SIZE="10" NAME="text32" value="<% if (request.getAttribute("r_rech")!= null) { out.println(request.getAttribute("r_rech")); } %>" onfocus="blur()">
                              </td>
                            </tr>
                            <tr><td class="tabmovtex" nowrap valign="middle">Registros por transmitir:</td></tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">
                                <INPUT TYPE="text" SIZE="10" NAME="text30" value="<% if (request.getAttribute("r_portran")!= null) { out.println(request.getAttribute("r_portran")); } %>" onfocus="blur()">
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
            <td width="190" height="94" align="center" valign="middle">
              <A HREF="javascript:js_nuevo();"><img border="0" name="imageField2" src="/gifs/EnlaceMig/gbo25550.gif" width="115" height="22" alt="Crear archivo"></a>
            </td>
          </tr>
          <tr>
            <td align="center" valign="bottom" width="190">
              <table width="190" border="0" cellspacing="2" cellpadding="3">
                <tr align="center">
                  <td class="textabdatcla" valign="top" colspan="2">
                    <table width="180" border="0" cellspacing="4" cellpadding="0">
                      <tr valign="middle"><td class="tabmovtex" nowrap>Importar archivo</td></tr>
                      <tr><td nowrap class="tabmovtex"><input type="file" name="archivoempleados"></td></tr>
                      <tr align="center">
                        <td class="tabmovtex" nowrap>
                          <A HREF="javascript:js_importar();"><img border="0" name="imageField" src="/gifs/EnlaceMig/gbo25280.gif" width="80" height="22" alt="Aceptar"></a>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
	  </td>
    </tr>
  </table>
        <br>
		<%
	  			if (request.getAttribute("ContenidoArchivo")!= null) {
      			out.println(request.getAttribute("ContenidoArchivo"));
	  								}
		%>

        <br>
   <table width="760" border="0" cellspacing="0" cellpadding="0">
	<tr>
      <td align="center">

		<table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td align="right" valign="middle" width="66">
              <A HREF="javascript:js_alta();"><img border="0" name="boton" src="/gifs/EnlaceMig/gbo25480.gif" width="66" height="22" alt="Alta"></a>
            </td>
            <td align="left" valign="top" width="127">
              <A HREF="javascript:js_baja();"><img border="0" name="boton" src="/gifs/EnlaceMig/gbo25500.gif" width="127" height="22" alt="Baja de registro"></a>
            </td>
            <td align="left" valign="top" width="93">
              <A HREF="javascript:js_modificacion();"><img border="0" name="imageField3" src="/gifs/EnlaceMig/gbo25510.gif" width="93" height="22" alt="Modificar"></a>
            </td>
            <td align="left" valign="top" width="83">
              <A HREF="javascript:js_reporte();"><img border="0" name="imageField4" src="/gifs/EnlaceMig/gbo25240.gif" width="83" height="22" alt="Reporte"></a>
            </td>
          </tr>
        </table>
		<%
	  			if (request.getAttribute("ligaDownload")!= null) {
      			out.println(request.getAttribute("ligaDownload"));
	  								}
		%>

		<br>
        <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td align="right" valign="middle" width="78">
              <A HREF="javascript:js_envio();"><img border="0" name="boton2" src="/gifs/EnlaceMig/gbo25520.gif" width="78" height="22" alt="Enviar"></a>
            </td>
            <td align="left" valign="top" width="97">
              <A HREF="javascript:js_recupera();"><img border="0" name="boton2" src="/gifs/EnlaceMig/gbo25530.gif" width="97" height="22" alt="Recuperar"></a>
            </td>
            <td align="left" valign="top" width="77">
              <A HREF="javascript:js_borrar();"><img border="0" name="imageField32" src="/gifs/EnlaceMig/gbo25540.gif" width="77" height="22" alt="Borrar"></a>
            </td>
            <td align="left" valign="top" width="85">
              <A HREF="javascript:js_exportar();"><img border="0" name="imageField42" src="/gifs/EnlaceMig/gbo25230.gif" width="85" height="22" alt="Exportar"></a>
            </td>
          </tr>
        </table>

        <br>
      </td>
    </tr>
</table>


<INPUT TYPE="hidden" VALUE="0" NAME="operacion">
<INPUT TYPE="hidden" VALUE="-1" NAME="registro">
<INPUT TYPE="hidden" VALUE="" NAME="secuencia">
<INPUT TYPE="hidden" VALUE="" NAME="nombre_nuevo">
<INPUT TYPE="hidden" VALUE="<%= request.getAttribute("tipo_archivo") %>" NAME="tipo_archivo">
<INPUT TYPE="hidden" VALUE="<%= request.getAttribute("total_registros") %>" NAME="total_registros">
<INPUT TYPE="hidden" VALUE="<%= request.getAttribute("max_registros") %>" NAME="max_registros">
<INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute("lista_numeros_pagos") != null) { out.println(request.getAttribute("lista_numeros_pagos")); } %>" NAME="lista_numeros_pagos">
<INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute("lista_numeros_claves") != null) { out.println(request.getAttribute("lista_numeros_claves")); } %>" NAME="lista_numeros_claves">
<INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute("archivoEstatus") != null) { out.println(request.getAttribute("archivoEstatus")); } %>" NAME="archivoEstatus">

</FORM>
<script>
<%
		if (request.getAttribute("mensaje_js")!= null) {
			out.println(request.getAttribute("mensaje_js"));
		}
%>
</script>
</BODY>
</HTML>