<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<HTML>
<HEAD>
<TITLE>Enlace Banco Santander Mexicano</TITLE>

<meta http-equiv="Content-Type" content="text/html;"/>
<meta name="Codigo de Pantalla" content="s25630"/>
<meta name="Proyecto" content="Portal Santander"/>
<meta name="Version" content="1.0"/>
<meta name="Ultima version" content="16/05/2001 18:00"/>
<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico"/>

<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/flame.js" defer="defer"></script>

<c:if test="${not empty newMenu}">
<script type="text/javascript">
	${newMenu.toString()}
</script>
</c:if>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
<link rel="stylesheet" href="/EnlaceMig/flame.css" type="text/css"/>

</HEAD>
<c:if test="${empty error}">
	${EIGlobal.mensajePorTraceDebug("RET_pactaOper.jsp:: Entrando a FLAME.")}
	${EIGlobal.mensajePorTraceDebug("RET_pactaOper.jsp:: Pintando URL a FLAME->".concat(FLAME_URL).concat("?token=").concat(guid))}
	${EIGlobal.mensajePorTraceInfo("RET_pactaOper.jsp:: Entrando a FLAME.")}
</c:if>
<c:set var="errorFlame" value="false"></c:set>
<c:if test="${not empty error}">
	<c:set var="errorFlame" value="true"></c:set>
</c:if>

<script type="text/javascript">
   errorFlame=${errorFlame};
</script>
<body style="width: 98%; height: 97%"
   onload="ventanaEmergenteCentrada(errorFlame, '${FLAME_URL}','${guid}');">
   <div id="mainDiv" style="width: 100%; height: 100%; float:left; position:absolute; top:0px; z-Index:-1;">
			<div style="width: 100%;">
			<div style="position:absolute; text-align:center;top:230px;width:100%">
   			<h3 id="clicMessage" style="color:white;display:none;">Clic aqu&iacute; para regresar a Flame</h3></div>
			<div style="position:absolute; text-align:center;top:250px;width:100%">
   			<h3 id="waitMessage" style="color:black;display:none;">Espere un momento por favor, estamos redireccionando a la consulta de pactados...</h3>
			</div></div>
	</div>
   <div id="mainDiv2" style="width: 100%; height: 100%">
      <div id="header" style="width: 100%; height: 15%; min-height:100px; ">
         <iframe id="frameHeader" frameborder="0"
            style="border: 0; width: 100%; height: 100%; overflow: hidden;"
            seamless="seamless" scrolling="no" src="/Enlace/jsp/RET_pactaOperTopFlame.jsp"></iframe>
      </div>
      <div id="content"
         style="width: 90%; height: 73%; display: block; overflow: hidden;">
		 <div style="width: 90%; height:100%; margin: auto; display:none" >
		 </div>
		 <div style="width: 90%; height:100%; margin: auto; display:none" >
		 </div>
         </div>
      <div id="footer" style="width: 100%; height: 12%; ">
         <iframe id="frameFooter"
            style="border: 0; width: 100%; height: 100%; overflow: hidden;"
            seamless="seamless" scrolling="no" src="/Enlace/jsp/RET_pactaOperDown.jsp"></iframe>
      </div>
   </div>
   <iframe id="frameTimer" style="display: none;" src="/Enlace/jsp/RET_PactaOperTimer.jsp"></iframe>
</body>
</HTML>
