<html>
<head>
<title>Banca Virtual</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
<%= request.getAttribute( "newMenu" ) %>
//-->
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
	<td width="*">
	<!-- MENU PRINCIPAL -->
	<%= request.getAttribute( "MenuPrincipal" ) %></TD>
  </TR>
</TABLE>

<%= request.getAttribute( "Encabezado" ) %>
<br>

<!--  		<table><tr><td bgcolor="red">Operacion : </td><td>
         <%= request.getAttribute( "operacion" ) %>
		 </td></tr><tr><td bgcolor="red">Cuenta :</td><td>
         <%= request.getAttribute( "cuenta" ) %>
		 </td></tr><tr><td bgcolor="red">Importe :</td><td>
         <%= request.getAttribute( "importe" ) %>
		 </td></tr><tr><td bgcolor="red">Folio :</td><td>
         <%= request.getAttribute( "folio" ) %>
		</td></tr></table> -->

<!--          <br><br><br><center>
		 <A href = "javascript:self.print();"><img src = "/gifs/EnlaceMig/impresora.gif" border = 0></a>
		 </center> -->

 <table width="760" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
          <tr>
            <td align="center" width="120" class="tittabdat">Tipo de operaci&oacute;n</td>
            <td align="center" class="tittabdat" width="200">Cuenta de cargo</td>
            <td align="center" class="tittabdat" width="100">Importe</td>
            <td align="center" class="tittabdat" width="50">Folio</td>
          </tr>
          <tr>
            <td class="textabdatobs" nowrap align="center">
            		<%= request.getAttribute( "operacion" ) %>
            </td>
            <td class="textabdatobs" align="center" nowrap>
            		<%= request.getAttribute( "cuenta" ) %>
            </td>
            <td class="textabdatobs" align="right" nowrap>
            		<%= request.getAttribute( "importe") %>
            </td>
            <td class="textabdatobs" align="center" nowrap>
            		<%= request.getAttribute( "folio" ) %>
            </td>
          </tr>
        </table>
        <br>
        <table border="0" cellspacing="0" cellpadding="0" class="tabfonbla">
          <tr>
            <td width="83">
			<a href = "javascript:scrImpresion();"><img border="0" name="imageField3" src="/gifs/EnlaceMig/gbo25240.gif" width="83" height="22" alt="Imprimir"></a>
            </td>
          </tr>
        </table>
        <br>
      </td>
    </tr>
</table>

</body>
</html>