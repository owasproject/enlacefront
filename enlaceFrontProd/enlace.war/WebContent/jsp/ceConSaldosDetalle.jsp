<%@ page language="java" %>
<%@ page contentType="text/html;charset=ISO8859_1"%>
<html><!-- #BeginTemplate "/Templates/principal.dwt" -->
<head>
<!-- #BeginEditable "doctitle" -->
<title>Enlace</title>
<!-- #EndEditable -->
<%-- 01/10/2002 Correcci�n ortograf�a y etiquetas HTML
     02/10/2002 Se agreg� noLineas en la forma
     03/10/2002 Se cambi� el tama�o de la tabla para ajustar a impresi�n tama�o carta.
     04/10/2002 Se cambi� el tama�o de la tabla con datos y otros detalles de dise�o.
     07/10/2002 Ajuste botones.
     08/10/2002 Ajuste botones.
     22/10/2002 Ajuste botones.
     24/10/2002 Se centr� la p�gina -la impresi�n completa de la p�gina se debe hacer ajustando el ancho de m�rgen a cero en los controles de la impresora.
--%>
<!-- #BeginEditable "MetaTags" -->
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s25020">
<meta name="Proyecto" content="Portal">
<meta name="Version" content="1.0">
<meta name="Ultima version" content="27/04/2001 18:00">
<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico">
<!-- #EndEditable -->
<script language = "JavaScript" type="text/javascript" SRC="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javascript" type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<Script language = "JavaScript" type="text/javascript">

<%
       if (request.getAttribute("Errores")!= null) {
       out.println(request.getAttribute("Errores"));
       }
%>

function enviarpos()
{
	var ban1=true;
	document.fsaldo.action="ceConPosicion";
	ban1=validar(1);
	if (ban1==true)
		document.fsaldo.submit();
}

function enviarmovs()
{

	if(validar())
   {
      document.fsaldo.action="ceConMovimientos?Modulo=0";
      document.fsaldo.submit();
   }
}

function validar(boton)
{

	var cta;
	var resultado=false;
	var contador=0;
	for (i=0;i<document.fsaldo.elements.length;i++){
		if (document.fsaldo.elements[i].type=="radio"){
			if (document.fsaldo.elements[i].checked==true){
				contador++;
				resultado=true;
				break;
			}
		}
	}
	if (contador==0)
		cuadroDialogo("Usted no ha seleccionado una cuenta.",3);
	return resultado;
}
</script>


<script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Mon Apr 23 20:21:38 GMT-0600 2001-->
<script language="JavaScript" type="text/javascript">
<!--

function MM_preloadImages() { //v3.0
	var d=document;
	if(d.images){
		if(!d.MM_p)
			d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}
	}
}
function MM_swapImgRestore() { //v3.0
	var i,x,a=document.MM_sr;
	for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
	var p,i,x;
	if(!d) d=document;
	if((p=n.indexOf("?"))>0&&parent.frames.length) {
	d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
<%= request.getAttribute( "newMenu" ) %>

//-->
</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<!--
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">
-->
<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
<tr valign="top">
<td width="*">
<!-- MENU PRINCIPAL -->
<%= request.getAttribute( "MenuPrincipal" ) %></TD>
</TR>
</TABLE>
<%= request.getAttribute( "Encabezado" ) %>
<FORM  NAME="fsaldo" method=post action="" onSubmit="return validar();">
<table width="760" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="center" >
<table width="100%" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
<tr>
<TD width="40"><BR></TD>
<td align="center" class="tittabdat" width="60">Seleccione</td>
<td align="center" class="tittabdat" width="65">Cuenta de<BR>cheques</td>
<td align="center" class="tittabdat" width="85" >L&iacute;nea de<BR>cr&eacute;dito</td>
<td class="tittabdat" align="center" colspan="2">Descripci&oacute;n</td>
<td class="tittabdat" align="center" width="85">Cr&eacute;dito<BR>autorizado</td>
<td class="tittabdat" align="center" width="85">Cr&eacute;dito<BR>dispuesto</td>
<td class="tittabdat" align="center" width="85">Cr&eacute;dito<BR>disponible</td>
<TD width="20"><BR></TD>
</tr>
<%
 if (request.getAttribute( "valor" )!= null) {
       out.println(request.getAttribute( "valor" ));
       }

%>
<!--<%= request.getAttribute( "valor" ) %> -->
</table>
<br>
</br>
<!-- #BeginLibraryItem "/Library/footerPaginacion.lbi" -->
<table width="420" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
<tr>
<TD width="120">
   <BR>
</TD>
<td align="right" width="85">
<a href="javascript:enviarmovs();" border="0" width="85">
<img src="/gifs/EnlaceMig/gbo25260.gif" border="0" alt="Movimientos">
</a>
</td>
<td align="right" width="80">
<a href="javascript:enviarpos();" border="0" width="80">
<img src="/gifs/EnlaceMig/gbo25270.gif" border="0" alt="Posiciones">
</a>
</td>
<td align="right" width="80">
<a href="javascript:scrImpresion();" border="0" width="80">
<img src="/gifs/EnlaceMig/gbo25240.gif" border="0" alt="Imprimir">
</a>
</td>
<td align="center">
<%= request.getAttribute( "Exportar" ) %>
</td>
</tr>
</table></td>
</tr>
</table>
<input type = "Hidden" name ="cuenta" value="<%= request.getAttribute("cta") %>">
<input type="hidden" name="noLineas" value="<%= request.getAttribute("noLineas") %>">
</form>
</body>
</html>
