<jsp:useBean id='concepto' class='java.lang.String' scope='session'/>
<jsp:useBean id='importe' class='java.lang.String' scope='session'/>
<jsp:useBean id='titular' class='java.lang.String' scope='session'/>
<jsp:useBean id='tipoRelacion' class='java.lang.String' scope='session'/>
<jsp:useBean id='cuenta' class='java.lang.String' scope='session'/>
<jsp:useBean id='fecha' class='java.lang.String' scope='session'/>
<jsp:useBean id='hora' class='java.lang.String' scope='session'/>

<%@page import="java.io.*"%>
<%@page import="java.lang.reflect.*"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.*"%>
<%@page import="java.util.*"%>
<%@page import="mx.altec.enlace.bo.FormatoMoneda"%>

<html>
<head>
	<title>Env&iacute;o de operaci&oacute;n</TITLE>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javaScript">

function oculta() {
	document.getElementById("enviar").style.visibility="hidden";
	document.getElementById("enviar2").style.visibility="visible";
	enviarForma();
}

function VentanaAyuda(cad)
{
  return;
}

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function enviarForma()
{
  var forma = document.tabla;
  forma.submit();
}

function regresar()
{
  var forma = document.tabla;
  forma.Modulo.value=1;
  forma.submit();
}

<%= request.getAttribute("newMenu")%>

</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
      <%= request.getAttribute("MenuPrincipal")%>
    </TD>
  </TR>
</table>
<%= request.getAttribute("Encabezado")%>
<table width="571" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
  </tr>
</table>

<!-- Forma //-->
 <FORM  NAME="tabla" method=post action="CambioContrato">
  <p>

  <table border=0 width=720 border=0 cellpadding=5 cellspacing=3>
   <tr>
     <td class='textittab' colspan=5 align=center> Confirmaci&oacute;n de env&iacute;o </td>
   </tr>
   <tr>
    <th class="tittabdat" >Cuenta Cargo</th>
	<th class="tittabdat" >Titular </th>
	<th class="tittabdat" >Fecha </th>
	<th class="tittabdat" >Importe </th>
	<th class="tittabdat" >L&iacute;nea de Captura </th>
   </tr>
   <tr>
    <td class='textabdatobs'><%=cuenta%></td>
	<td class='textabdatobs'><%=titular%></td>
	<td class='textabdatobs' align=center><%=fecha%> <%=hora%></td>
	<td class='textabdatobs' align=right><%=FormatoMoneda.formateaMoneda(new Double(importe).doubleValue())%></td>
	<td class='textabdatobs'><%=concepto%></td>
   </tr>
	<!-- CSA - AVA - HD1000000085131 -->
	<tr align="center"  id="enviar2" style="visibility:hidden" class="tabmovtex">
		<td colspan=5 align=center>Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>
   	</tr>
	<tr id="enviar">
		<td colspan=5 align=center>
	   		<br>
	  	 	<a href = "javascript:oculta();" border = 0><img src = "/gifs/EnlaceMig/gbo25520.gif" border=0 alt="Enviar"></a>
	  	 	<!-- <a href = "javascript:enviarForma();" border = 0><img src = "/gifs/EnlaceMig/gbo25520.gif" border=0 alt="Enviar"></a> -->
	   		<a href = "javascript:regresar();" border = 0><img src = "/gifs/EnlaceMig/gbo25320.gif" border=0 alt="Regresar"></a>
	 	</td>
	</tr>
	<!-- CSA - AVA - HD1000000085131 -->
  </table>

  <input type=hidden name=concepto value='<%=concepto%>'>
  <input type=hidden name=importe value='<%=importe%>'>
  <input type=hidden name=Modulo value='3'>

 </form>
</body>
</html>

<%!
	public String FormatoMoneda( String cantidad )
	{
		//String language = "la"; // ar
		//String country = "MX";  // AF
		//Locale local = new Locale(language,  country);
		//NumberFormat nf = NumberFormat.getCurrencyInstance(local);
		//No se aceptan decimales
		DecimalFormat nf= new DecimalFormat("'$'#,###");
		String formato = "";

		if(cantidad ==null ||cantidad.equals(""))
			cantidad="0.00";

		double importeTemp = 0.0;
		importeTemp = new Double(cantidad).doubleValue();
		if (importeTemp < 0)
		{
			try {
				formato = nf.format(new Double(cantidad).doubleValue());
				if (!(formato.substring(0,1).equals("$")))
					formato ="$ -"+ formato.substring(2,formato.length());
			} catch(NumberFormatException e) {formato="$ 0.00";}
		} else {
			try {
				formato = nf.format(new Double(cantidad).doubleValue());
				if (!(formato.substring(0,1).equals("$")))
					formato ="$ "+ formato.substring(1,formato.length());
			} catch(NumberFormatException e) {
				formato="$ 0.00";}
		}
		if(formato==null)
			formato = "";
		return formato+".00";
	}
%>