<html>
<head>
<title>Enlace</title>

<meta http-equiv="Content-Type" content="text/html;"/>
<meta name="Codigo de Pantalla" content="s25310"/>
<meta name="Proyecto" content="Portal"/>
<meta name="Version" content="1.0"/>
<meta name="Ultima version" content="27/04/2001 18:00" />
<script type ="text/javascript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>

<script type="text/javascript">

function cambiaArchivoExport(extencion) {
    var boton = document.getElementById("bExport");
    if (extencion==="txt") {
		document.getElementById("tipoArchivoExportacion").value =extencion;
        document.getElementById("extTxt").checked= true;
    }else if(extencion==="csv"){
		document.getElementById("tipoArchivoExportacion").value =extencion;
        document.getElementById("extCsv").checked= true;
    }
    boton.href="javascript:exportaBitacora();";
}

function exportaBitacora(){
	var extencion = document.getElementById("tipoArchivoExportacion").value;
	window.open("/Enlace/enlaceMig/ConsultaBitacora?tipoArchivoExport=" + extencion,"Bitacora","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
}

var Nmuestra = <%= request.getAttribute( "varpaginacion" ) %>;


function atras(){
//alert("entrando a funcion atras");
    if((parseInt(document.frmbit.prev.value) - Nmuestra)>=0){
      document.frmbit.next.value = document.frmbit.prev.value;
      document.frmbit.prev.value = parseInt(document.frmbit.prev.value) - Nmuestra;
    //  alert("anteriores : " + document.frmbit.prev.value);\
      document.frmbit.action = "ConsultaBitacora?contrato="+"<%=((String)request.getAttribute("CONTRATO"))%>";
      document.frmbit.submit();
    }
}

function adelante(){
 if(parseInt(document.frmbit.next.value) != parseInt(document.frmbit.total.value)){
    if((parseInt(document.frmbit.next.value) + Nmuestra)<= parseInt(document.frmbit.total.value)){
      document.frmbit.prev.value = document.frmbit.next.value;
      document.frmbit.next.value = parseInt(document.frmbit.next.value) + Nmuestra ;
      document.frmbit.action = "ConsultaBitacora?contrato="+"<%=((String)request.getAttribute("CONTRATO"))%>";
      document.frmbit.submit();
   }else if((parseInt(document.frmbit.next.value) + Nmuestra) > parseInt(document.frmbit.total.value)){
      document.frmbit.prev.value = document.frmbit.next.value;
      document.frmbit.next.value = parseInt(document.frmbit.next.value) + (parseInt(document.frmbit.total.value) - parseInt(document.frmbit.next.value));
      document.frmbit.action = "ConsultaBitacora?contrato="+"<%=((String)request.getAttribute("CONTRATO"))%>";
      document.frmbit.submit();
   }
 }else{
   //alert("no hay mas registros");
   cuadroDialogo("No hay mas registros prueba", 3);

 }

 }

 function MakeFile(){
   document.frmbit.prev.value=0;
   document.frmbit.next.value = document.frmbit.total.value;
   document.frmbit.action = "CreaArchBit";
   document.frmbit.submit();

 }
</script>

<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
</script>
<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
</script>
<script type="text/javascript">
function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}
</script>
<script type="text/javascript">
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>

<script type="text/javascript">
<!--

<!---------------------------------------------------------------------------------------------->
<%= request.getAttribute("newMenu") %>
<!---------------------------------------------------------------------------------------------->

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}


//			link = "<a href=\"javascript:window.open('ConsultaBitacora?opcionLC=1&folioSUA=" + registro[1] + "','Impresion'," +
//					"'toolbar=no,location=no,directories=no,status=no,menubar=no," +
//					"scrollbars=yes,resizable=no,width=440,height=290\');\">"; */

function VentanaComprobanteSUALC(a,b)
{
//alert ("los valores de k,l>>>>" + k + "<>" + l);

    var param1  = "";
    var param2  = "";
    param1  = "folioSUA="+a;
    param2  = "lc="+b;
   	vc=window.open("ConsultaBitacora?opcionLC=1&"+param1+"&"+param2,'trainerWindow','width=490,height=420,toolbar=no,scrollbars=yes');
   	vc.focus();
}

function VentanaComprobante(a,b,c,d,e,f,g,h,i,j,k,l)
{
//alert ("los valores de k,l>>>>" + k + "<>" + l);

    var param1  = "";
    var param2  = "";
    var param3  = "";
    var param4  = "";
    var param5  = "";
    var param6  = "";
    var param7  = "";
    var param8  = "";
    var param9  = "";
    var param10 = "";
    var param11	= "";
    var param12 = "";
    var param13 = "";
    var contrato = "contrato="+"<%=((String)request.getAttribute("CONTRATO"))%>";
    var banco=document.frmbit.banco.value;

    param1  = "tipo="+formateaAmp(a);
    param2  = "refe="+formateaAmp(b);
    param3  = "importe="+formateaAmp(c);
    param4  = "cta_origen="+formateaAmp(d);
    param5  = "cta_destino="+formateaAmp(e);
    param6  = "enlinea=n";
    param7  = "fecha="+f;
    param8  = "hora="+g;
    param9  = "usuario="+formateaAmp(h);
    param10 = "desc_usuario="+formateaAmp(i);
    param11 = "descripcion="+formateaAmp(j);
    param12 = "rfc="+formateaAmp(k);
    param13 = "iva="+l;

   vc=window.open("comprobante_trans?"+param1+"&"+param2+"&"+param3+"&"+param4+"&"+param5+"&"+param6+"&"+param7+"&"+param8+"&"+param9+"&"+param10+"&"+param11+"&"+param12+"&"+param13+"&"+contrato+"&banco="+banco,'trainerWindow','width=490,height=420,toolbar=no,scrollbars=yes');
   vc.focus();
}

//CSA-----------------
function formateaAmp(para) {
  var formateado="";
  var car="";
  if(para.indexOf("&") != -1) {
      for(a2=0;a2<para.length;a2++) {
    	  if(para.charAt(a2)=='&')
    	   car="%26";
    	  else
    	   car=para.charAt(a2);
    
    	  formateado+=car;
    	}
  } else {
    formateado = para;
  }
  return formateado;
 
}
//------------------

// JVL 04/01/2005 Agrego esta funcion para llamar al nuevo comprobante de Pago de Impuestos
// por Linea de Captura, el comprobante es el JSP CompNvoPILC.jsp el cual se le envian los
/*
 * Mejoras de Linea de Captura
 * Se comenta var tipo = "";
 * Se manda llamar CompNvoPILCPagoRefSat.jsp, solo para cuando es Pago Referenciado SAT
 * Inicio RRR - Indra Marzo 2014
 */
// datos a mostrar
function VenComNvoPI(tipo,refe,impor,ctacarg,fch,hrs,usr,descusr,concept)
{

    //var tipo = "";
    var contrato   = "contrato="+"<%=((String)request.getAttribute("CONTRATO"))%>";
    var desccto    = "desccto="+"<%=((String)request.getAttribute("NOMCONTRATO"))%>";
    var importe    = "";
    var referencia = "";
    var ctacargo   = "";
    var fecha      = "";
    var hora       = "";
    var usuario    = "";
    var concepto   = "";

    //tipo     = "tipo="+a;
    referencia = "referencia="+refe;
    importe    = "importe="+impor;
    ctacargo   = "ctacargo="+ctacarg;
    fecha      = "fecha="+fch;
    hora       = "hora="+hrs;
    usuario    = "usuario="+usr;
    concepto   = "concepto="+concept;

	var parametros=contrato +"&" + desccto  +"&" + referencia +"&" + importe +"&" + ctacargo +"&" + fecha +"&" + hora +"&" + usuario +"&" + concepto;
	if( 'PILC' == tipo || 'PMRF' == tipo ){
		parametros = parametros +"&titcta=";
		vc=window.open("../jsp/CompNvoPILCPagoRefSat.jsp?"+parametros.replace(/\s/ig,'+'),'trainerWindow','width=490,height=430,toolbar=no,scrollbars=yes');
	} else {
		vc=window.open("../jsp/CompNvoPILC.jsp?"+parametros.replace(/\s/ig,'+'),'trainerWindow','width=490,height=430,toolbar=no,scrollbars=yes');
	}    
    vc.focus();
}
/*
 * Fin RRR - Indra Marzo 2014
 */
// JVL 04/01/2005 Fin modificacion

//********************** Empieza codigo nuevo HELG-Getronics
function fDetalle_divisa(fecha,ref)
 {
   /*Presentar el Detalle de los Cambios*/
   msg=window.open("ConsultaBitacora?Detalle=1&Fecha="+fecha+"&Ref="+ref,"Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=500,height=490");
   msg.focus();
 }
//********************** Termina Codigo nuevo HELG

//********************** Empieza codigo nuevo HELG-Getronics
function fDetalle_internacional(fecha,ref)
 {
   /*Presentar el Detalle de los Cambios*/
   msg=window.open("ConsultaBitacora?Detalle=2&Fecha="+fecha+"&Ref="+ref,"Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=500,height=490");
   msg.focus();
 }
//********************** Termina Codigo nuevo HELG

/*Funcion para mostrar el comprobante de reuters en la bitacora*/
function fDetalle_reutersfx(fecha,ref)
{
	msg=window.open("ConsultaBitacora?Detalle=3&Fecha="+fecha+"&Ref="+ref,"Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=490,height=640");
	msg.focus();
}
//********************** Termina Codigo nuevo HELG

//-->
</script>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="background-image: url('/gifs/EnlaceMig/gfo25010.gif');background-color: #ffffff ;"
onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')">

<table width="571" border="0" cellspacing="0" cellpadding="0">
  <tr valign="top">
    <td width="*">
        ${requestScope.MenuPrincipal}
    </td>
  </tr>
</table>

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="676" valign="top" align="left">
      <table width="666" border="0" cellspacing="6" cellpadding="0">
        <tr>
          <td width="528" valign="top" class="titpag">${requestScope.Encabezado}</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

${requestScope.Output1}

</body>
</html>