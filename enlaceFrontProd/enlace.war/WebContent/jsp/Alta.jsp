<html>

<script language = "JavaScript" SRC= "/EnlaceMig/ConsultCanc.js"></script>
<SCRIPT LANGUAGE="JavaScript" archive="/EnlaceMig/cuadroDialogo.js"> </SCRIPT>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>

<script language = "JavaScript">

function js_alta() {
	if (document.ChesPrincipal.archivo_actual.value != "") {
	  document.location = "AltaP.jsp?archivo=" + document.ChesPrincipal.archivo_actual.value;
    }
	else
	  cuadroDialogo("No ha creado ni importado un archivo aun", 3);
}


<!-- ***********************************************  -->
<!-- modificacion para integracion pva 07/03/2002	  -->
<!-- ***********************************************  -->

var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;


function PresentarCuentas()
{

  msg=window.open("/EnlaceMig/pantcuentas.html","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");

  msg.focus();

}

function actualizacuenta()
{
  document.frmConsultCanc.cuenta.value=ctaselec;
  document.frmConsultCanc.textcuenta.value=ctaselec;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

  var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

  if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_findObj(n, d) { //v3.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

  d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

<%= (String) session.getAttribute("newMenu") %>

</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="PutDate();MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">


  <tr valign="top">


	<td width="*">


	 <!-- MENU PRINCIPAL -->

	 <%= (String) session.getAttribute("MenuPrincipal") %>

	</td>

   </tr>

</table>

<%= (String) session.getAttribute("Encabezado") %>

<br>

<%!


/*  Bean para leer los datos del formulario de Alta de proveedores */
public class AltaBean
{
	private String vClave;
    private String vCodigo;
	private String vPaterno;
	private String vMaterno;
	private String vNombre;
	private String vRFC;
	private String vHomoclave;
	private String vContacto;
	private String vFactoraje;
	private String vCalle;
	private String vColonia;
	private String vDelegacion;
	private String Ciudad;
    private String vCodigoPostal;
	private String vEstado;
	private String vPais;
	private String vMedio;
	private String vEmail;
	private String vLada;
	private String vTelefonico;
	private String vExTelefono;
	private String vFax;
	private String vExFax;
	private String vForma;
	private String vCuenta;
	private String vDivisa;
	private String vBanco;
	private String vSucursal;
	private String vPlaza;
	private String vLimite;
	private String vDeudores;
	private String vVolumen;
	private String vImporte;


  public AltaBean() {}


/*  metodos set  */

  public void setvClave(String newvClave) {
	  vClave = newvClave;
  }

  public void setvCodigo(String newvCodigo) {
	  vCodigo = newvCodigo;
  }

  public void setvPaterno(String newvPaterno) {
	  vPaterno = newvPaterno;
  }

  public void setvMaterno(String newvMaterno) {
	  vMaterno = newvMaterno;
  }

  public void setvNombre(String newvNombre) {
	  vNombre = newvNombre;
  }

  public void setvRFC(String newvRFC) {
	  vRFC = newvRFC;
  }

  public void setvHomoclave(String newvHomoclave) {
	  vHomoclave = newvHomoclave;
  }

  public void setvContacto(String newvContacto) {
	  vContacto = newvContacto;
  }

  public void setvFactoraje(String newvFactoraje) {
	  vFactoraje = newvFactoraje;
  }

  public void setvCalle(String newvCalle) {
	  vCalle = newvCalle;
  }

  public void setvColonia(String newvColonia) {
	  vColonia = newvColonia;
  }

  public void setvDelegacion(String newvDelegacion) {
	  vDelegacion = newvDelegacion;
  }

  public void setCiudad(String newCiudad) {
	  Ciudad = newCiudad;
  }

  public void setvCodigoPostal(String newvCodigoPostal) {
	  vCodigoPostal = newvCodigoPostal;
  }

  public void setvEstado(String newvEstado) {
	  vEstado = newvEstado;
  }

  public void setvPais(String newvPais) {
	  vPais = newvPais;
  }

  public void setvMedio(String newvMedio) {
	  vMedio = newvMedio;
  }

  public void setvEmail(String newvEmail) {
	  vEmail = newvEmail;
  }

  public void setvLada(String newvLada) {
	  vLada = newvLada;
  }

  public void setvTelefonico(String newvTelefonico) {
	  vTelefonico = newvTelefonico;
  }

  public void setvExTelefono(String newvExTelefono) {
	  vExTelefono = newvExTelefono;
  }

  public void setvFax(String newvFax) {
	  vFax = newvFax;
  }

  public void setvExFax(String newvExFax) {
	  vExFax = newvExFax;
  }

  public void setvForma(String newvForma) {
	  vForma = newvForma;
  }

   public void setvCuenta(String newvCuenta) {
	  vCuenta = newvCuenta;
  }

   public void setvDivisa(String newvDivisa) {
	  vDivisa = newvDivisa;
  }

   public void setvBanco(String newvBanco) {
	  vBanco = newvBanco;
  }

   public void setvSucursal(String newvSucursal) {
	  vSucursal = newvSucursal;
  }

   public void setvPlaza(String newvPlaza) {
	  vPlaza = newvPlaza;
  }

   public void setvLimite(String newvLimite) {
	  vLimite = newvLimite;
  }

   public void setvDeudores(String newvDeudores) {
	  vDeudores = newvDeudores;
  }

   public void setvVolumen(String newvVolumen) {
	  vVolumen = newvVolumen;
  }

   public void setvImporte(String newvImporte) {
	  vImporte = newvImporte;
  }

  /*  metodos get    */

  public String getvClave() {
	  return vClave;
  }

  public String getvCodigo() {
	  return vCodigo;
  }

  public String getvPaterno() {
	  return vPaterno;
  }

  public String getvMaterno() {
	  return vMaterno;
  }

  public String getvNombre() {
	  return vNombre;
  }

  public String getvRFC() {
	  return vRFC;
  }

  public String getvHomoclave() {
	  return vHomoclave;
  }

  public String getvContacto() {
	  return vContacto;
  }

  public String setvFactoraje() {
	  return vFactoraje;
  }

  public String getvCalle() {
	  return vCalle;
  }

  public String getvColonia() {
	  return vColonia;
  }

  public String getvDelegacion() {
	  return vDelegacion;
  }

  public String getCiudad() {
	  return Ciudad;
  }

  public String getvCodigoPostal() {
	  return vCodigoPostal;
  }

  public String getvEstado() {
      return vEstado;
  }

  public String getvPais() {
	  return vPais;
  }

  public String getvMedio() {
      return  vMedio;
  }

  public String getvEmail() {
	  return vEmail;
  }

  public String getvLada() {
	  return vLada;
  }

  public String getvTelefonico() {
	  return vTelefonico;
  }
  public String getvExTelefono() {
	  return vExTelefono;
  }

  public String getvFax() {
	  return vFax;
  }

  public String getvExFax() {
	  return vExFax;
  }

  public String getvForma() {
	  return vEstado;
  }

   public String getvCuenta() {
	  return vCuenta;
  }

   public String getvDivisa() {
	  return vDivisa;
  }

   public String getvBanco() {
	  return vBanco;
  }

   public String getvSucursal() {
	  return vSucursal;
  }

   public String getvPlaza() {
	  return vPlaza;
  }

   public String getvLimite() {
	  return vLimite;
  }

   public String getvDeudores() {
	  return vDeudores;
  }

   public String getvVolumen() {
	  return vVolumen;
  }

   public String getvImporte() {
	  return vImporte;
  }


}
%>
<%! public void Escribe( java.io.FileWriter dato, String cadena)                                                         throws Exception {
	String thiscadena;
	if ( (cadena.equals("")) || (cadena == null) )
         thiscadena = " ";
	else
		thiscadena = cadena;
      try {
		  for (int i= 0; i < thiscadena.length(); i++)
            dato.write(
			  thiscadena.charAt(i));
      } catch(Exception e) {
	       }

    }
%>
<%! String nombreArchivo;
%>

 <%
   AltaBean Alta = new AltaBean();

   nombreArchivo = Global.DOWNLOAD_PATH + (String) session.getAttribute("ArchivoActual");

   if (session.getAttribute("TotalRegistros") == null)
		  session.setAttribute("TotalRegistros", String.valueOf(0));
	  session.setAttribute("TotalRegistros", String.valueOf(Integer.parseInt( (String) session.getAttribute("TotalRegistros")) + 1));



  // Alta.setvClave(request.getParameter("vClave"));
   Alta.setvCodigo(request.getParameter("vCodigo"));
   Alta.setvPaterno(request.getParameter("vPaterno"));
   Alta.setvMaterno(request.getParameter("vMaterno"));
   Alta.setvNombre(request.getParameter("vNombre"));
   Alta.setvRFC(request.getParameter("vRFC"));
   Alta.setvHomoclave(request.getParameter("vHomoclave"));
   Alta.setvContacto(request.getParameter("vContacto"));
   Alta.setvFactoraje(request.getParameter("vFactoraje"));
   Alta.setvCalle(request.getParameter("vCalle"));
   Alta.setvColonia(request.getParameter("vColonia"));
   Alta.setvDelegacion(request.getParameter("vDelegacion"));
   Alta.setCiudad(request.getParameter("Ciudad"));
   Alta.setvCodigoPostal(request.getParameter("vCodigoPostal"));
   Alta.setvEstado(request.getParameter("vEstado"));
   Alta.setvPais(request.getParameter("vPais"));
   Alta.setvMedio(request.getParameter("vMedio"));
   Alta.setvEmail(request.getParameter("vEmail"));
   Alta.setvLada(request.getParameter("vLada"));
   Alta.setvTelefonico(request.getParameter("vTelefonico"));
   Alta.setvExTelefono(request.getParameter("vExTelefono"));
   Alta.setvFax(request.getParameter("vFax"));
   Alta.setvExFax(request.getParameter("vExFax"));
   Alta.setvForma(request.getParameter("vForma"));
   Alta.setvCuenta(request.getParameter("vCuenta"));
   Alta.setvDivisa(request.getParameter("vDivisa"));
   Alta.setvBanco(request.getParameter("vBanco"));
   Alta.setvSucursal(request.getParameter("vSucursal"));
   Alta.setvPlaza(request.getParameter("vPlaza"));
   Alta.setvLimite(request.getParameter("vLimite"));
   Alta.setvDeudores(request.getParameter("vDeudores"));
   Alta.setvVolumen(request.getParameter("vVolumen"));
   Alta.setvImporte(request.getParameter("vImporte"));

   boolean noproblem = true;
  try {

      java.io.FileWriter dato = new java.io.FileWriter(nombreArchivo, true);

    Escribe(dato, request.getParameter("vClave"));
    Escribe(dato, ";");
    Escribe(dato, request.getParameter("vCodigo"));
    Escribe(dato, ";");
	Escribe(dato,request.getParameter("vContacto"));
	Escribe(dato, ";");
	Escribe(dato,request.getParameter("vFactoraje"));
	Escribe(dato, ";");
	Escribe(dato,request.getParameter("vMedio"));
	Escribe(dato, ";");
	Escribe(dato,request.getParameter("vForma"));
	Escribe(dato, ";");
	Escribe(dato,request.getParameter("vCuenta"));
	Escribe(dato, ";");

    Escribe(dato,request.getParameter("vBanco"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vSucursal"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vPlaza"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vLimite"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vDeudores"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vVolumen"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vImporte"));
	Escribe(dato, ";");
	Escribe(dato,"F");
	Escribe(dato,";");       // tipo de persona, cambiar

    Escribe(dato,request.getParameter("vPaterno"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vMaterno")); //faltaba
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vNombre"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vRFC"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vHomoclave"));
	Escribe(dato, ";");


    Escribe(dato,request.getParameter("vCalle"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vColonia"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vDelegacion"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("Ciudad"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vCodigoPostal"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vEstado"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vPais"));
	Escribe(dato, ";");


    Escribe(dato,request.getParameter("vLada"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vTelefonico"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vExTelefono"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vFax"));
	Escribe(dato, ";");
    Escribe(dato,request.getParameter("vExFax"));
	Escribe(dato, ";");
	Escribe(dato,request.getParameter("vEmail"));
	Escribe(dato, ";");
	Escribe(dato, " ");         // Este no se pa que es
	Escribe(dato,";");


	Escribe(dato, "\n");
    dato.close();
    /*archivoSalida.close();
    archivotemporal.close();
    temporal.close();*/

 }

    catch (Exception e) {
		out.println(e.toString());
		out.println("<h1> No se pudo procesar su Alta </h1>");
		noproblem = false;
		if (session.getAttribute("RegistrosRechazados") == null)
		  session.setAttribute("RegistrosRechazados", String.valueOf(0));
	  session.setAttribute("RegistrosRechazados", String.valueOf(Integer.parseInt( (String) session.getAttribute("RegistrosRechazados")) + 1));
    }

    int a;
	a = 0;

   if (noproblem) {

      out.println("<center><h1>  Alta exitosa  </h1></center>");
	  if (session.getAttribute("RegistrosAceptados") == null)
		  session.setAttribute("RegistrosAceptados", String.valueOf(a));
	  session.setAttribute("RegistrosAceptados", String.valueOf(Integer.parseInt( (String) session.getAttribute("RegistrosAceptados")) + 1));

   }



 %>
 <%! String link;  %>
 <%
     link  = "AltaP.jsp?archivo=" + (String) session.getAttribute("ArchivoActual");

 %>


 <center><A href = "javascript:history.back();" border=0><img src="/gifs/EnlaceMig/gbo25320.gif" border=0 alt="Regresar" width="77" height="22"></a></center>
</body>
</html>

