 <html>
<head>
	<title>CUENTAS -  Autorizaci&oacute;n y Cancelaci&oacute;n</TITLE>

<meta http-equiv="Expires" content="1" />
<meta http-equiv="pragma" content="no-cache" />


<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/util.js"></script>
<script type="text/javascript" src="/EnlaceMig/pm_fp.js"></script>
<script type="text/javascript">

 <%!
   String formateaFecha(String fec)
   {
		String fecha=fec;
		System.out.println("Longitud de fecha:" +fec.trim().length());
		if(fec.trim().length()==8)
			fecha=fec.substring(6) + "/" + fec.substring(4,6) + "/" + fec.substring(0,4);
		System.out.println("La nueva fecha es: " + fecha +" antes " + fec);
		return fecha;
   }
 %>

function js_ejecuta(c)
 {
	var forma=document.CtasConsulta;
	var seleccion=false;
	forma.chkNombre.value="";
	forma.tipoOper.value="";

	for(i=0;i<forma.length;i++)
	{
		 if(forma.elements[i].type=='checkbox')
	     {
            if(forma.elements[i].checked==true)
            {
                forma.chkNombre.value= forma.chkNombre.value+forma.elements[i].name;
					seleccion=true;
			}
		}
	}

	  if (!seleccion)
		cuadroDialogo('Seleccione un registro por favor',1)
	  else
	 {

       if(c==0)
  	   forma.tipoOper.value="A";
	   else
  	   forma.tipoOper.value="R";

	   forma.Modulo.value="5";
 	   forma.submit();
	}

 }

function BotonPagina(sigant)
 {
//	var forma=document.CtasConsulta;

   document.CtasConsulta.action="ContCtas_AL";
   document.CtasConsulta.pagina.value=sigant-1;
   document.CtasConsulta.submit();
 }

 function SeleccionaTodos()
 {

	var forma=document.CtasConsulta;

   if(forma.Todas.checked==true)
    {
 	  forma.Todas.checked=true;
      for(i2=0;i2<forma.length;i2++)
       if(forma.elements[i2].type=='checkbox' && forma.elements[i2].name!='Todas')
         forma.elements[i2].checked=true;
    }
   else
    {
      forma.Todas.checked=false;
      for(i2=0;i2<forma.length;i2++)
       if(forma.elements[i2].type=='checkbox' && forma.elements[i2].name!='Todas')
         forma.elements[i2].checked=false;
    }
 }

 function exportacion(){
 	
 	document.CtasConsulta.action = "ExportServlet";
 	document.CtasConsulta.submit();
 	document.CtasConsulta.action = "ContCtas_AL";
 }

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/
<%
	if(	session.getAttribute("strScript") !=null)
		out.println( session.getAttribute("strScript"));
	else
	 if(request.getAttribute("strScript") !=null)
		out.println( request.getAttribute("strScript") );
	 else
	  out.println("");
%>
<%
	if(	session.getAttribute("newMenu") !=null)
		out.println( session.getAttribute("newMenu"));
	else
	  if(request.getAttribute("newMenu")!=null)
		out.println(request.getAttribute("newMenu"));
	 else
	   out.println("");
%>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />
<style type="text/css">
.texconte{
	        font-family: Arial, Helvetica, sans-serif;
			font-size: 10px;
		}
</style>

</head>

<body style=" background-color: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);" leftMargin="0" topMargin="0" marginwidth="0" marginheight="0" onLoad="obtenDatosBrowser();" >

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<%-- MENU PRINCIPAL --%>
	<%
	if(	session.getAttribute("MenuPrincipal") !=null)
		out.println( session.getAttribute("MenuPrincipal"));
	else
	 if(request.getAttribute("MenuPrincipal")!=null)
		out.println(request.getAttribute("MenuPrincipal"));
	 else
	  out.println("");
	%>
   </td>
  </tr>
</table>

<%
	 if(request.getAttribute("Encabezado")!=null)
		out.println(request.getAttribute("Encabezado"));
	 else
	  out.println("");
%>

 <FORM   NAME="CtasConsulta" METHOD="Post" ACTION="ContCtas_AL">

 <table border="0" cellspacing="0" cellpadding="0">
  <tr>
   <td align="center">
   <%
   	String estatus="";

	String Fecha1_01=(String)request.getAttribute("Fecha1_01");
	String Fecha1_02=(String)request.getAttribute("Fecha1_02");
	String nombreArchivo=(String)request.getAttribute("nombreArchivo");
	String tipoOper=(String)request.getAttribute("tipoOper");
	String exportar=(String)request.getAttribute("Exportar");
	String numRegpage=(String)request.getAttribute("numRegpage");
	String af = (String)request.getAttribute("af");
	String metodoExportacion = (String)request.getAttribute("metodoExportacion");
	String nombreBeanExport = (String)request.getAttribute("nombreBeanExport");
	
	if(exportar==null)
	  exportar="";

	String contratoConsulta=(String)request.getAttribute("contratoConsulta");

	int totalPaginas=Integer.parseInt((String)request.getAttribute("totalPaginas"));
	int totalRegistros=Integer.parseInt((String)request.getAttribute("totalRegistros"));
	int registrosPorPagina=Integer.parseInt((String)request.getAttribute("registrosPorPagina"));
	int pagina=Integer.parseInt((String)request.getAttribute("pagina"));

	int A=registrosPorPagina*pagina;
	int de=(A+1)-registrosPorPagina;

	if(pagina==totalPaginas)
	   A=totalRegistros;


   %>

   <table border="0">
    <tr>
	 <td>

	 <table border="0" align="center" cellspacing="0" cellpadding="1" width="100%">

 	  <tr>
		<td class="textabref"><b>&nbsp;Total de registros encontrados: <font color="red"><%=totalRegistros%></font></b></td>
	  </tr>

	  <tr>
		<td colspan="11" align="left" class="textabdatcla"><b>&nbsp;Pagina <font color="blue"><%=pagina%></font> de <font color="blue"><%=totalPaginas%></font>&nbsp;</b></td>
		<td align="right" class="textabdatcla"><b>Registros <font color="blue"><%=de%></font> - <font color="blue"><%=A%></font></b>&nbsp;</td>
	  </tr>

	 </table>
	 <%-- Tabla de registros obtenidos --%>
	 <%=request.getAttribute("tablaRegistros")%>
	 <%-- fint Tabla --%>
 	 <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
	  <tr>
		<td align="left" class="textabref"><b>&nbsp;Pagina <font color="blue"><%=pagina%></font> de <font color="blue"><%=totalPaginas%></font>&nbsp;</b></td>
		<td align="right" class="textabref">&nbsp;<b>Registros <font color="blue"><%=de%></font> - <font color="blue"><%=A%></font></b>&nbsp;</td>
	  </tr>
	 </table>

	</td>
   </tr>
  </table>

     <%=request.getAttribute("Botones")%>

	<input type="hidden" name="Fecha1_01" value="<%=Fecha1_01%>" />
	<input type="hidden" name="Fecha1_02" value="<%=Fecha1_02%>" />
	<input type="hidden" name="nombreArchivo" value="<%=nombreArchivo%>" />
	<input type="hidden" name="contratoConsulta" value="<%=contratoConsulta%>" /> 
	<input type="hidden" name="totalRegistros" value="<%=totalRegistros%>" />
	<input type="hidden" name="registrosPorPagina" value="<%=registrosPorPagina%>" />
	<input type="hidden" name="numRegpage" value="<%=numRegpage%>" />
	<input type="hidden" name="totalPaginas" value="<%=totalPaginas%>" />
	<input type="hidden" name="Exportar" value="<%=exportar%>" />
	<input type="hidden" name="Modulo" value="4" />
	<input type="hidden" name="pagina" />
	<input type="hidden" name="chkNombre" />
	<input type="hidden" name="tipoOper" />
	<input type="hidden" name="af" value="<%=af%>" />
	<input type="hidden" name="metodoExportacion" value="<%=metodoExportacion%>" />
	<input type="hidden" name="nombreBeanExport" value="<%=nombreBeanExport%>" />
	<input type="hidden" value="em" name="em" />
	<input type="hidden" id="datosBrowser" name="datosBrowser" value=""/>

   </td>
  </tr>
  <tr>
   <td>
    <br>
	<!--<table align="center" border="0" cellspacing="0" cellpadding="0">
	   	<tr>
	 		<td style="text-align:center;"><input type="radio" value="txt" name="tipoArchivo">Exportar en TXT</td>
	 	</tr>
  		<tr>
	 		<td style="text-align:center;"><input type="radio" value="csv" name="tipoArchivo"  checked="">Exportar en XLS<br><br></td>
	 	</tr>
	 <tr>
	 -->
	 
	 <table width="760" border="0" cellspacing="2" cellpadding="3" align="center" style="background-color: #FFFFFF;">
	
	 
	  <tr>
			<td align="center"  colspan="2" class="tabmovtex11">
				<a style="cursor: pointer;" > <input  type="radio" value ="txt" name="tipoArchivo"/>Exporta en TXT</a>
			</td>			
		</tr>
	
		<tr>
			<td align="center"  colspan="2" class="tabmovtex11">
				<a style="cursor: pointer;" > <input type="radio" value ="csv" checked name="tipoArchivo"/>Exporta en XLS</a>
			</td>			
		</tr>	
	 
	 
	 <tr>
	 		<td><br></td>
	 	</tr>
	 <tr>
	 <%-- vswf:meg cambio de NASApp por Enlace 08122008 --%>
		<td align="center"  ><a href="javascript:js_ejecuta(0);" border = 0><img src= "/gifs/EnlaceMig/gbo25430.gif" style=" border: 0" alt="Autorizar" /></a><a href ="javascript:js_ejecuta(1);" border = 0><img src = "/gifs/EnlaceMig/gbo25190.gif" alt=Cancelar border=0 /></a><a href ="javascript:scrImpresion();" border = 0><img src = "/gifs/EnlaceMig/gbo25240.gif" border=0 alt=Imprimir/></a><a href="javascript:document.location.href='/Enlace/enlaceMig/ContCtas_AL?Modulo=2'"><img style=" border: 0;" src="/gifs/EnlaceMig/gbo25320.gif" alt="Regresar"/></a><%=exportar%></td>
		</tr>
    </table>
   </td>
  </tr>
 </table>

 </FORM>
</body>
</html>