<!------------------------------- COGIGO NUEVO ------------------------------------>
<%
String parStrCuentas = (String)request.getAttribute("strCuentas");
String parNumCuentas = (String)request.getAttribute("NumCuentas");
String parOpcionRv = (String)request.getAttribute("OpcionRv");
String parCtasXpag = (String)request.getAttribute("CtasXpag");
String parOffset = (String)request.getAttribute("Offset");

if(parStrCuentas == null) parStrCuentas = "";
if(parNumCuentas == null) parNumCuentas = "";
if(parOpcionRv == null) parOpcionRv = "";
if(parCtasXpag == null) parCtasXpag = "";
if(parOffset == null) parOffset = "";
%>
<!--------------------------------------------------------------------------------->

<html>
<head>
	<title>Saldos Consolidados</TITLE>

	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
	<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
	<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
	<script language="JavaScript" src="/EnlaceMig/ValidaFormas.js"></script>
	<!------------------------------- COGIGO NUEVO ------------------------------------>
	<SCRIPT language="JavaScript">
	function muestra(offset)
		{
		document.Forma.Offset.value = "" + offset;
		document.Forma.submit();
		}
	</SCRIPT>
	<!--------------------------------------------------------------------------------->
	<script language="javaScript">

	var errores = new Array(" la CUENTA.", " el tipo de MONEDA.", " la CLAVE.");

	var evalua = "SSnXXXX";

	function Enviar()
		{
		  textToUpper(document.Datos);

		  if(ValidaTodo(document.Datos,evalua,errores))
			  document.Datos.submit();
		}

	function Posicion()
		{
		//cuadroDialogo("Operación no disponible por el momento");

		var forma=document.Datos;
		var cont=0;

		textToUpper(forma);

		for(i=0;i<forma.length;i++)
			{
			if(forma.elements[i].type=='radio') if(forma.elements[i].checked==true)
				{
				cont=1;
				forma.cta_posi.value=forma.elements[i].value.substring(0,forma.elements[i].value.indexOf('@'));
				//alert(forma.cta_posi.value);
				//alert(forma.opcion.value);
				forma.action="poscpsrvr1";
				forma.submit();
				}
			}
		if(cont==0) {cuadroDialogo("Seleccione una cuenta por favor.");}
		}


function Movimientos()
		{
		//cuadroDialogo("Operación no disponible por el momento");

		var forma=document.Datos;
		var cont=0;

		textToUpper(forma);

		for(i=0;i<forma.length;i++)
			{
			if(forma.elements[i].type=='radio') if(forma.elements[i].checked==true)
				{
				cont=1;
				forma.radTabla.value=forma.elements[i].value.substring(0,forma.elements[i].value.indexOf('@'));
				//alert(forma.cta_posi.value);
				//alert(forma.opcion.value);
				forma.action="CMovimiento";
				forma.submit();
				}
			}
		if(cont==0) {cuadroDialogo("Seleccione una cuenta por favor.");}
		}
/*	function Movimientos()
		{
		//cuadroDialogo("Operación no disponible por el momento");
		textToUpper(document.Datos);

		document.Datos.action="CMovimiento";
		document.Datos.submit();
		}
*/

/******************  ********* ***************************************************/

	function MM_preloadImages()
		{ //v3.0
		var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
		}

	function MM_swapImgRestore()
		{ //v3.0
		var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
		}

	function MM_findObj(n, d)
		{ //v3.0
		var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
		}

	function MM_swapImage()
		{ //v3.0
		var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
		if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
		}

/********************************************************************************/
	<% if (request.getAttribute("newMenu")!= null) {out.println(request.getAttribute("newMenu"));} %>

	</script>
	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

	<table border="0" cellpadding="0" cellspacing="0" width="571">
	<tr valign="top">
		<td width="*">
		<!-- MENU PRINCIPAL -->
		<% if (request.getAttribute("MenuPrincipal")!= null) {out.println(request.getAttribute("MenuPrincipal"));} %>
		</td>
	</tr>
	</table>

	<% if (request.getAttribute("Encabezado")!= null) {out.println(request.getAttribute("Encabezado"));} %>

	<!------------------------------- COGIGO NUEVO ------------------------------------>
	<FORM action="MSC_Inicio" name="Forma">
	<INPUT Type=Hidden name="OpcionRv" value="<%= parOpcionRv %>">
	<INPUT Type=Hidden name="NumCuentas" value="<%= parNumCuentas %>">
	<INPUT Type=Hidden name="CtasXpag" value="<%= parCtasXpag %>">
	<INPUT Type=Hidden name="Offset" value="<%= parOffset %>">
	</FORM>
	<!--------------------------------------------------------------------------------->

	<FORM  NAME="Datos" method=post action="MTI_Transferencia?Modulo=0">

	<p>
	<table align=center border=0 cellspacing=0 cellpadding=0>


	<!------------------------------- COGIGO NUEVO ------------------------------------>
	<% if(parOpcionRv.equals("1") || parOpcionRv.equals("")) { %>
	<!--------------------------------------------------------------------------------->
	<tr>
		<td class='tabmovtex'> &nbsp; Saldo total moneda nacional:
		<% if (request.getAttribute("SaldoNacional")!= null) {out.println(request.getAttribute("SaldoNacional"));} %>
		</td>
	</tr>
	<!------------------------------- COGIGO NUEVO ------------------------------------>
	<% } %>
	<!--------------------------------------------------------------------------------->

	<!------------------------------- COGIGO NUEVO ------------------------------------>
	<% if(parOpcionRv.equals("2") || parOpcionRv.equals("")) { %>
	<!--------------------------------------------------------------------------------->
	<tr>
		<td class='tabmovtex'> &nbsp; Saldo total d&oacute;lares:
		<% if (request.getAttribute("SaldoDolares")!= null) {out.println(request.getAttribute("SaldoDolares"));} %>
		</td>
	</tr>
	<!------------------------------- COGIGO NUEVO ------------------------------------>
	<% } %>
	<!--------------------------------------------------------------------------------->

	<!------------------------------- COGIGO NUEVO ------------------------------------>
	<tr><td><br></td></tr>
	<tr>
		<td class='tabmovtex' colspan=2>
		<%= parStrCuentas %>
		</td>
	</tr>
	<!--------------------------------------------------------------------------------->

	<tr><td><br></td></tr>
	<tr>
		<td class='tabmovtex' colspan=2>
		<% if (request.getAttribute("strCuentasNacional")!= null) {out.println(request.getAttribute("strCuentasNacional"));} %>
		</td>
	</tr>
	<tr><td><br></td></tr>
	<tr>
		<td class='tabmovtex' colspan=2>
	    <% if (request.getAttribute("strCuentasDolares")!= null) {out.println(request.getAttribute("strCuentasDolares"));} %>
		</td>
	</tr>
	<tr><td><br></td></tr>
	</table>

	<!------------------------------- COGIGO NUEVO ------------------------------------>
	<DIV align=center>
	<%
	if(!parOpcionRv.equals(""))
		{
		int a;
		int offset = Integer.parseInt(parOffset);
		int totalCuentas = Integer.parseInt(parNumCuentas);
		int ctasXpag =  Integer.parseInt(parCtasXpag);
		String pag;

		if(offset>0) { %><A href="javascript:muestra(<%= offset-ctasXpag %>)" class=texfootpaggri>&lt;Anterior</A>&nbsp;<% }
		if(totalCuentas > ctasXpag) for(a=1;((a-1)*ctasXpag)<totalCuentas;a++)
			{
			pag = "" + a;
			if((a-1)*ctasXpag != offset)
				pag = "<A href=\"javascript:muestra(" + ((a-1)*ctasXpag) + ")\" class=texfootpaggri>" + pag + "</A>";
			else
				pag = "<SPAN  class=texfootpaggri>" + pag + "</SPAN>";
			out.println(pag);
			}
		if(offset+ctasXpag<totalCuentas) { %>&nbsp;<A href="javascript:muestra(<%= offset+ctasXpag %>)" class=texfootpaggri>Siguiente&gt;</A><% }
		}
	%>

	</DIV>
	<!--------------------------------------------------------------------------------->

	<p>
	<table align=center border=0 cellspacing=0 cellpadding=0>
	<tr>
		<td><A href="javascript:Movimientos();" border=0><img src = "/gifs/EnlaceMig/gbo25260.gif" border=0></a></td>
		<td><A href="javascript:Posicion();" border=0><img src = "/gifs/EnlaceMig/gbo25270.gif" border=0></a></td>
		<td><A href="javascript:scrImpresion();" border=0><img src = "/gifs/EnlaceMig/gbo25240.gif" border=0 alt="Imprimir"></a></td>
		<td><% if (request.getAttribute("Exportar")!= null) {out.println(request.getAttribute("Exportar"));} %></td>
	</tr>
	</table>

	<input type=hidden name=cta_posi value=''>
	<input type=hidden name=opcion value=0>

	</form>

</body>
</html>