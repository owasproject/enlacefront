<html>
<head>
	<title>Tesorer&iacute;a Inteligente Editar</TITLE>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript">


/**************************************************************************************/
/**************************************************************************************/
// Funciones para tramasn  EI_Tipo en javascript
var registrosTabla=new Array();
var camposTabla=new Array();
var totalCampos=0;
var totalRegistros=0;
var indiceCuenta=0;

function iniciaObjeto(campo2)
 {
   numeroRegistros(campo2);
   llenaTabla(campo2);
   separaCampos(registrosTabla);
 }

function numeroRegistros(campo2)
    {
      totalRegistros=0;
      for(i=0;i<campo2.length;i++)
       {
         if(campo2.charAt(i)=='@')
           totalRegistros++;
       }
    }

function llenaTabla(campo2)
 {
    var strCuentas="";
    var vartmp="";

	strCuentas=campo2;

    if(totalRegistros>=1)
      {
        for(i=0;i<totalRegistros;i++)
         {
           vartmp=strCuentas.substring(0,strCuentas.indexOf('@'));
           registrosTabla[i]=vartmp;
           if(strCuentas.indexOf('@')>0)
             strCuentas=strCuentas.substring(strCuentas.indexOf('@')+1,strCuentas.length);
         }
      }
 }

function separaCampos(arrCuentas)
 {
     var i=0;
     var j=0;
     var vartmp="";
     var regtmp="";

     for(i=0;i<=totalRegistros;i++)
       camposTabla[i]=new Array();

     totalCampos=0;
     if(totalRegistros>=1)
      {
        regtmp+=arrCuentas[0];
        for(i=0;i<regtmp.length;i++)
         {
           if(regtmp.charAt(i)=='|')
            totalCampos++;
         }
        for(i=0;i<totalRegistros;i++)
         {
           regtmp=arrCuentas[i];
           for(j=0;j<totalCampos;j++)
            {
			  if(regtmp.charAt(0)=="|")
               {
                 vartmp="";
                 regtmp=regtmp.substring(1,regtmp.length);
               }
              else
               {
                 vartmp=regtmp.substring(0,regtmp.indexOf("|"));
                 if(regtmp.indexOf("|")>0)
                   regtmp=regtmp.substring(regtmp.indexOf("|")+1,regtmp.length);
               }
              camposTabla[i][j]=vartmp;
            }
           camposTabla[i][j]=regtmp;
         }
      }
 }
/************************************************************************************/
/************************************************************************************/

 var errores=new Array(
                   //"Radio",
				   //"radio",
                   " la CUENTA",
                   //" el NIVEL",
				   "radio",
				   "radio",
				   " el DIA",
				   " la HORA",
				   " el SALDO OPERATIVO",
				   "checkbox",
				   " el SALDO OPERATIVO");

 var evalua="TXXsSnxnx";

  var respuesta=0;

function formatea(para)
 {
   var formateado="";
   var car="";

   for(a2=0;a2<para.length;a2++)
    {
	  if(para.charAt(a2)==' ')
	   car="+";
	  else
	   car=para.charAt(a2);

	  formateado+=car;
	}
   return formateado;
 }

function CheckBox()
{
  var forma=document.Datos;

  if(forma.Inversion.checked==false)
   forma.SaldoCheck.value="";
}

function VerificaCheck(texto)
{
  var forma=document.Datos;

  if(forma.Inversion.checked==false)
    texto.blur();
}

function verificaOpcion(obj)
{
  var forma=document.Datos;

  //Para radios no hacer nada

  //Para checkbox
  for(i=0;i<forma.length;i++)
   if(forma.elements[i].type=='checkbox' && forma.elements[i].name!='Inversion')
    if(forma.elements[i].checked==true)
	 if(forma.elements[i].name!=obj.name)
	  {
	    forma.elements[i].checked=false;
		obj.checked=true;
	  }
}

function inhabilitaDias()
{
  var forma=document.Datos;

  forma.cmbDia.options[0].selected=true;
}

function VerificaCombo(combo)
{
  var forma=document.Datos;

  if(forma.Periodicidad[0].checked==true)
    combo.blur();
}

function facultades(cveProducto)
{
  /*
	  Regresa 0: Modifica Todo
              1: No puede modificar Periodo (dia, horario)
			  2: No puede modificar datos (inv automatica)
			  3: No puede modificar nada.
  */

  var forma=document.Datos;
  var nivel=0;

  if(trimString(camposTabla[indiceCuenta][7])!="0")
	nivel=1;

  if( (cveProducto=="2" || cveProducto=="3") && nivel==1) //no raiz == 2,3
	return 0;

  if( !(cveProducto=="2" || cveProducto=="3") && nivel==0)  //raiz != 2,3
	return 3;

  if( (cveProducto=="2" || cveProducto=="3") && nivel==0)  //raiz == 2,3
    return 1;

  if( !(cveProducto=="2" || cveProducto=="3") )  //
	return 2;

  return 0;
}

function Modificar()
{
  var contrato="<%= request.getAttribute("NumContrato") %>";
  var descripcion="<%= request.getAttribute("NomContrato") %>";

  var cuenta="";
  var ctaSel="";
  var cveProducto="";
  var desCuenta="";
  var nivelTrama=0;

  var pipe="|";
  var Trama="";

  var periodicidad="";
  var cuentaPadre="";
  var horario="";
  var saldoOp="";
  var SaldoChk="";
  var invAuto="No";
  var noNivel=0;
  var tipo=0;
  var cont=0;
  var ctaTmp = 0;

  var selCuentaDeTabla=false;
  var forma=document.Datos;

  if(!ValidaTodo(document.Datos,evalua,errores))
	return ;

  //Desentrama los registros...
  iniciaObjeto(forma.TramasCuentas.value);

  //informacion de la cuenta que selecciono para agregar a la estructura.
  /*combo
  ctaSel=forma.Cuentas.options[forma.Cuentas.selectedIndex].value;
  cuenta=ctaSel.substring(0,ctaSel.indexOf('@'));
  desCuenta=ctaSel.substring(ctaSel.indexOf('@')+1,ctaSel.indexOf('|'));
  cveProducto=ctaSel.substring(ctaSel.indexOf('|')+1,ctaSel.length);
  */
  //texto hidden
  cuenta=camposTabla[indiceCuenta][0];
  desCuenta=camposTabla[indiceCuenta][10];
  cveProducto=camposTabla[indiceCuenta][11]
  nivelTrama=0;

  //Primero se verifica que la cuenta no exista en la estructura.
  for(i=0;i<totalRegistros;i++)
   if(trimString(cuenta)==trimString(camposTabla[i][0]))
	 if(i!=indiceCuenta)
	  {
	    forma.Cuentas.focus();
		cuadroDialogo("La Cuenta seleccionada ya existe en la estructura.",1);
		return ;
	  }

  // Horarios .... (8:00 ... 20:00)
  for(i=0;i<forma.cmbPeriodo.options.length;i++)
   {
     if(forma.cmbPeriodo.options[i].selected)
	  {
		cont++;
	    horario+=trimString(forma.cmbPeriodo.options[i].value)+", ";
	  }
   }
  horario=horario.substring(0,horario.length-2);

  // Periodicidad .... (Diaria / Lunes ... Viernes)
  if(forma.Periodicidad[0].checked==true)
    periodicidad="Diaria";
  else
   {
     periodicidad=forma.cmbDia.options[forma.cmbDia.selectedIndex].text;
     if(forma.cmbDia.options[forma.cmbDia.selectedIndex].value=="0")
      {
        forma.cmbDia.focus();
        cuadroDialogo("Debe seleccionar un d&iacute;a.",1);
        return;
      }

         if(forma.cmbDia.options[6].selected){
      //YHG Agrega horarios requeridos
			if(!forma.cmbPeriodo.options[13].selected && !forma.cmbPeriodo.options[14].selected && !forma.cmbPeriodo.options[11].selected && !forma.cmbPeriodo.options[12].selected)
			{
					cuadroDialogo("Para el s&aacute;bado debe seleccionar s&oacute;lo el horario de las 17:00 a las 20:00 hrs",1);
					return;

			}
		}


   }

  //saldos operativos...
  if(trimString(forma.SaldoOperativo.value)=="")
	forma.SaldoOperativo.value="0";
  if(forma.Divisa.value=="USD"){
	  forma.Inversion.checked==false;
  }
  if(forma.Inversion.checked==true)
   {
     if(trimString(forma.SaldoCheck.value)=="")
	   {
	     forma.SaldoCheck.focus();
	     cuadroDialogo("Debe especificar el Saldo Operativo",1);
		 return;
	   }
	 invAuto="Si";
   }
  else
    forma.SaldoCheck.value="0";
  saldoOp=forma.SaldoOperativo.value;
  saldoChk=forma.SaldoCheck.value;

  //Despues de asignar y verificar todos los datos...
  //Revisar las facultades para modificar el periodo (periodicidad, horario, saldo op.)
  //y para los datos (inv auto, saldo op.)
  var fac=facultades(cveProducto);
  var infoNoAplica="";

  if(fac==0 || fac==2)
   {
	 if(fac==2)
	  {
	  	cuentatmp = forma.Cuentas.value;
	  	if(cuentatmp.substring(0,2)=="60" || cuentatmp.substring(0,2)=="65"){
	  		ctaTmp = 1;
			invAuto="Si"
  			saldoChk=forma.SaldoCheck.value;
	  	}
	  	else{
			//Poner datos en blanco;
			invAuto="No"
			saldoChk="0";
		    //alert("No tiene facultad para modificar datos");
			if(forma.Divisa.value=="MN"){
				infoNoAplica="inversi&oacute;n autom&aacute;tica";
			}else{
				infoNoAplica="";
			}
		}
	  }
     if(cont==0)
      {
	   forma.cmbPeriodo.focus();
	   cuadroDialogo("Debe seleccionar un horario.",1);
	   return;
	  }
   }
  if(fac==1)
   {
	 //Poner datos en blanco;
	 horario="";
	 periodicidad=""
	 saldoOp="0";
     //alert("No tiene facultad para modificar Periodo");
	 infoNoAplica="periodicidad, horario y el saldo operativo";
   }
  if(fac==3)
   {
	 //Poner datos en blanco;
	 invAuto="No"
	 saldoChk="0";
	 horario="";
	 periodicidad=""
	 saldoOp="0";
	 //alert("No tiene facultad para modificar Nada");
	 if(forma.Divisa.value=="MN"){
		 infoNoAplica="periodicidad, horario, el saldo operativo y la inversi&oacute;n autom&aacute;tica ";
	 }else{
		 infoNoAplica="periodicidad, horario y el saldo operativo ";
	 }
   }

   //Manipular la informacion para armar las tramas
   //Desentrama los registros...
   //Armar la trama con la informacion de acuerdo a la cve de producto de la cuenta.
   	  Trama=cuenta+pipe;
	  //Trama+=camposTabla[indiceCuenta][1] +pipe; //** Descripcion Nivel
	  Trama+=""+pipe;
	  Trama+=periodicidad+pipe;
	  Trama+=horario+pipe;
	  Trama+=saldoOp+pipe;
	  Trama+=invAuto+pipe;
	  Trama+=saldoChk+pipe;
	  Trama+=camposTabla[indiceCuenta][7]+pipe;
	  Trama+=camposTabla[indiceCuenta][8]+pipe;     //** NumNivel (Padre,Hija)
	  Trama+=camposTabla[indiceCuenta][9]+pipe;     //** TipoNivel (1 ... n)
	  Trama+=desCuenta+pipe;
	  Trama+=cveProducto+pipe;
	  Trama+=camposTabla[indiceCuenta][12];
	  Trama+="@";

  forma.Modulo.value='1';
  forma.Trama.value="";

  var nuevaTramaCuentas="";
  if(trimString(forma.TramasCuentas.value)!="")
   {
	 for(a=0;a<totalRegistros;a++)
	  {
		if(a==indiceCuenta)
		  nuevaTramaCuentas+=Trama;
		else
		 {
			for(b=0;b<12;b++)
			  nuevaTramaCuentas+=camposTabla[a][b]+pipe;
			nuevaTramaCuentas+=camposTabla[a][12];
			nuevaTramaCuentas+="@";
		 }
	  }
	 forma.TramasCuentas.value=nuevaTramaCuentas;
   }

  if(ctaTmp==1){
  	document.Datos.submit();
  }
  else{
	  if(fac!=0){
		if(!trimString(infoNoAplica)==""){
			cuadroDialogo("La informacion de "+infoNoAplica +" no aplica.<br> Desea continuar?",2);
		}else{
			document.Datos.submit();
		}
	  }else{
	    document.Datos.submit();
	  }
  }
}

function continua()
{
	if(respuesta==1)
	  document.Datos.submit();
}

function cargaCuenta()
{
  var numReg=0;
  var forma=document.Datos;

  indiceCuenta=trimString(forma.Trama.value);
  iniciaObjeto(forma.TramasCuentas.value);
  seleccionarCuenta();
}

function seleccionarCuenta()
{
  var cuenta="";
  var ctaSel="";
  var ctaCmp="";
  var cadHA="";
  var cadHB="";
  var cveDia=0;
  var horas=0;
  var forma=document.Datos;

  ctaSel=camposTabla[indiceCuenta][0];

  //limpiar el combo de horas
  for(i=0;i<forma.cmbPeriodo.length;i++)
    forma.cmbPeriodo.options[i].selected=false;

  //Activar cuenta selecionada ...
  /* Combo
  for(i=0;i<forma.Cuentas.length;i++)
    {
      ctaCmp=forma.Cuentas.options[i].value;
      ctaCmp=ctaCmp.substring(0,ctaCmp.indexOf('@'));
      if(trimString(ctaSel)==trimString(ctaCmp))
        forma.Cuentas.options[i].selected=true;
    }
  */
  forma.Cuentas.value=ctaSel; //texto

  //Activar dia, si es semanal
  cveDia=formatoPeriodicidad(trimString(camposTabla[indiceCuenta][2]))
  if(cveDia!=0)
   {
     forma.Periodicidad[0].checked=false;
	 forma.Periodicidad[1].checked=true;
     for(i=0;i<forma.cmbDia.length;i++)
	  if(cveDia==forma.cmbDia.options[i].value)
	    forma.cmbDia.options[i].selected=true;
   }
  else
   {
     forma.cmbDia.options[0].selected=true;
     forma.Periodicidad[0].checked=true;
	 forma.Periodicidad[1].checked=false;
   }

  //Activar horarios
  horas=cuentaCampos(',',trimString(camposTabla[indiceCuenta][3]))
  cadHA=camposTabla[indiceCuenta][3];
  for(j=0;j<horas;j++)
   {
     cadHB=cadHA.substring(0,cadHA.indexOf(','));
	 selHoras(cadHB);
	 cadHA=cadHA.substring(cadHA.indexOf(',')+1,cadHA.length);
   }
  selHoras(cadHA);

  //Inversion automatica
  if(camposTabla[indiceCuenta][5]=="Si"){
     forma.Inversion.checked=true;
   } else {
     forma.Inversion.checked=false;
   }
  	var num1 = new Number(camposTabla[indiceCuenta][6]);
  	forma.SaldoCheck.value=num1.toFixed(2);

  //Saldo operativo
  var num = new Number(camposTabla[indiceCuenta][4]);
  forma.SaldoOperativo.value=num.toFixed(2);
}

function selHoras(cad)
{
  var forma=document.Datos;

  cad=trimString(cad);
  if(cad=="")
    return;

  for(i=0;i<forma.cmbPeriodo.length;i++)
   if(forma.cmbPeriodo.options[i].value==cad)
     forma.cmbPeriodo.options[i].selected=true;
}

function cuentaCampos(car,cadena)
{
  var total=0;

  for(a=0;a<cadena.length;a++)
   if(cadena.charAt(a)==car)
    total++;

  return total;
}
// YHG Modiifca valores de dias
function formatoPeriodicidad(dia)
{
  var per=0;

  if(dia=="Diaria")
	per=0;
  if(dia=="Lunes")
	per=1;
  if(dia=="Martes")
	per=2;
  if(dia=="Miercoles")
	per=3;
  if(dia=="Jueves")
	per=4;
  if(dia=="Viernes")
	per=5;
  if(dia=="Sabado")
	per=6;
if(dia=="Domingo")
	per=7;

  return per;
}

function Siguiente()
{
  indiceCuenta++;
  if(indiceCuenta>=totalRegistros)
    indiceCuenta=totalRegistros-1;
  else
   seleccionarCuenta();
}

function Anterior()
{
  indiceCuenta--;
  if(indiceCuenta<0)
    indiceCuenta=0;
  else
   seleccionarCuenta();
}

function Regresar()
{
  var forma=document.Datos;

  forma.Modulo.value='1';
  forma.Trama.value="";

  document.Datos.submit();
}

/******************  ********* ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/

<%
       if (request.getAttribute("newMenu")!= null) {
       out.println(request.getAttribute("newMenu"));
       }
%>

</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0" onLoad="cargaCuenta();">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%
           if (request.getAttribute("MenuPrincipal")!= null) {
           out.println(request.getAttribute("MenuPrincipal"));
           }
    %>
   </td>
  </tr>
</table>

<%
           if (request.getAttribute("Encabezado")!= null) {
           out.println(request.getAttribute("Encabezado"));
           }
%>


<FORM  NAME="Datos" method=post action="TIConcentracion">

  <p>

  <table align=center border=0 cellspacing=0 cellpadding=3 class='textabdatcla'>

   <tr>
     <td colspan=2 class="tittabdat"> Seleccione la cuenta a integrar y las condiciones de concentraci&oacute;n</td>
   </tr>

   <tr>
     <td class='tabmovtex' colspan=2><br></td>
   </tr>

   <tr>
     <td class='tabmovtex' colspan=2>
	   Cuenta
	 </td>
   </tr>

<!--  Combo
   <tr>
	 <td class='tabmovtex' colspan=2>
	   <select class='tabmovtex' name=Cuentas>
	     <option value=0>Seleccione una cuenta</option>
		 <%
		    if(request.getAttribute("Cuentas")!= null) {
			  out.println(request.getAttribute("Cuentas"));}
		 %>
	   </select>
	 </td>
   </tr>
//-->
   <tr>
    <td class='tabmovtex' colspan=2>
	  <input type=text size=25 name=Cuentas value="" onFocus="blur();">
	</td>
   </tr>

   <tr>
     <td class='tabmovtex' colspan=2><br></td>
   </tr>

   <tr>

   <td class='tabmovtex' colspan=2>
   <table border=0 cellpadding=0 cellspacing=0 >

   <tr>
     <td class='tabmovtex'>Periodicidad</td>
	 <td class='tabmovtex' width='15'><br></td>
	 <td class='tabmovtex'>Saldo operativo</td>
   </tr>

   <tr>
	 <td class='tabmovtex'>
	  <table border=0 cellspacing=0 cellpadding=0>
	    <tr>
		  <td class='tabmovtex'>
		    <input type=radio name=Periodicidad checked value="0" onClick="inhabilitaDias();"> Diaria<br>
			<input type=radio name=Periodicidad value="1"> Semanal
		  </td>
		  <td class='tabmovtex' valign=top width=10><br></td>
		  <td class='tabmovtex' valign=top>
		    <select class='tabmovtex' name=cmbDia onFocus="VerificaCombo(this);">
		    <!-- YHG Modiifca valores de dias-->
			  <option value=0> </option>
			  <option value=1>Lunes</option>
			  <option value=2>Martes</option>
			  <option value=3>Miercoles</option>
			  <option value=4>Jueves</option>
			  <option value=5>Viernes</option>
			  <option value=6>Sabado</option>


			</select>
		  </td>
		  <td class='tabmovtex' valign=top>
			<select class='tabmovtex' name=cmbPeriodo size=3 MULTIPLE>
			  <option value='06:00'> &nbsp; 06:00 &nbsp; </option>
			  <option value='07:00'> &nbsp; 07:00 &nbsp; </option>
			  <option value='08:00'> &nbsp; 08:00 &nbsp; </option>
			  <option value='09:00'> &nbsp; 09:00 &nbsp; </option>
			  <option value='10:00'> &nbsp; 10:00 &nbsp; </option>
			  <option value='11:00'> &nbsp; 11:00 &nbsp; </option>
			  <option value='12:00'> &nbsp; 12:00 &nbsp; </option>
			  <option value='13:00'> &nbsp; 13:00 &nbsp; </option>
			  <option value='14:00'> &nbsp; 14:00 &nbsp; </option>
			  <option value='15:00'> &nbsp; 15:00 &nbsp; </option>
			  <option value='16:00'> &nbsp; 16:00 &nbsp; </option>
			  <option value='17:00'> &nbsp; 17:00 &nbsp; </option>
			  <option value='18:00'> &nbsp; 18:00 &nbsp; </option>
			  <option value='19:00'> &nbsp; 19:00 &nbsp; </option>
			  <option value='20:00'> &nbsp; 20:00 &nbsp; </option>
			</select>
		  </td>
		</tr>
	  </table>
	 </td>

	 <td class='tabmovtex'><br></td>

	 <td class='tabmovtex' valign=top>
	   <input type=text size=10 maxlength=10 name=SaldoOperativo>
	 </td>
   </tr>

   </td>
   </tr>
   </table>

   <tr>
     <td class='tabmovtex' colspan=2><br></td>
   </tr>

<%
	String tipoCtas = "";
	if((String)session.getAttribute("Divisa")!=null){
		 tipoCtas = (String)session.getAttribute("Divisa");
		 tipoCtas = tipoCtas.trim();
	}

%>

   <tr>
     <td class='tabmovtex' colspan=2>
		<table border=0 cellspacing=0 cellpadding=0>
		<tr>
			<%if(tipoCtas.equals("MN")){%>
				<td class='tabmovtex'><input type=checkbox name=Inversion onClick="CheckBox();"></td>
				<td class='tabmovtex'>Inversi&oacute;n autom&aacute;tica</td>
			<%}
			if(tipoCtas.equals("USD")){%>
				<td class='tabmovtex'><input type=hidden name=Inversion></td>
		   <%}%>
		</tr>
		<tr>
			<td class='tabmovtex'><br></td>
			<%if(tipoCtas.equals("MN")){%>
				<td class='tabmovtex'>Saldo operativo</td>
		    <%}%>
		</tr>
		<tr>
			<%if(tipoCtas.equals("MN")){%>
				<td class='tabmovtex'><br></td>
				<td class='tabmovtex'>
					<input type=text name=SaldoCheck maxlength=10 size=10 onFocus="VerificaCheck(this);">
				</td>
			<%}
			if(tipoCtas.equals("USD")){%>
				<td class='tabmovtex'><input type=hidden name=SaldoCheck></td>
		    <%}%>
		</tr>
	  </table>
	 </td>
   </tr>

   <tr>
     <td colspan=2><br></td>
   </tr>

  </table>

  <br>
  <table border=0 cellpadding=0 cellspacing=0 align=center>
	<tr>
	 <td align=center>
	   <a href="javascript:Modificar();"><img src="/gifs/EnlaceMig/gbo25510.gif" border=0 alt="Modificar"></a><a href="javascript:Anterior();"><img src="/gifs/EnlaceMig/gbo25620.gif" border=0 alt="Anterior"></a><a href="javascript:Siguiente();"><img src="/gifs/EnlaceMig/gbo25610.gif" border=0 alt="Siguiente"></a><a href="javascript:Regresar();"><img src="/gifs/EnlaceMig/gbo25320.gif" border=0 alt="Regresar"></a>
	 </td>
	</tr>
  </table>

  <br>
  <input type=hidden name=Modulo value=0>
  <input type=hidden name=Trama value="<%= request.getAttribute("Trama") %>">
  <input type=hidden name=Alta value="<%= request.getAttribute("Alta") %>">
  <input type=hidden name=Modificar value="<%= request.getAttribute("Modificar") %>">
  <input type=hidden name=TramasCuentas value="<%= request.getAttribute("TramasCuentas") %>">
  <input type=hidden name=Divisa value="<%=session.getAttribute("Divisa")%>">
</form>

<%
       if (request.getAttribute("Consulta")!= null) {
       out.println(request.getAttribute("Consulta"));
       }
%>

</body>
</html>