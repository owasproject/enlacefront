<html>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@page import="mx.altec.enlace.bo.FormatoMoneda"%>

<%!
     public String FormatoMoneda ( String cantidad ) {
         String language = "la"; // ar
         String country = "MX";  // AF
         Locale local = new Locale (language,  country);
         NumberFormat nf = NumberFormat.getCurrencyInstance (local);
         String formato = "";

         if(cantidad ==null ||cantidad.equals (""))
             cantidad="0.0";

         double importeTemp = 0.0;
         importeTemp = new Double (cantidad).doubleValue ();
         if (importeTemp < 0) {
             try {
                 formato = nf.format (new Double (cantidad).doubleValue ());
                 if (!(formato.substring (0,1).equals ("$")))
                     formato ="$ -"+ formato.substring (2,formato.length ());
             } catch(NumberFormatException e) {formato="$0.00";}
         } else {
             try {
                 formato = nf.format (new Double (cantidad).doubleValue ());
                 if (!(formato.substring (0,1).equals ("$")))
                     formato ="$ "+ formato.substring (1,formato.length ());
             } catch(NumberFormatException e) {
                 formato="$0.00";}
         } // del else
         if(formato==null)
             formato = "";
         return formato;
     }

%>

<% String valor = (String)request.getAttribute("importe"); %>
<head>
<title>Comprobante</title>
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s25590">
<meta name="Proyecto" content="Portal">
<meta name="Version" content="1.0">
<meta name="Ultima version" content="16/05/2001 18:00">
<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico">
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="form1" method="post" action="">
  <table width="430" border="0" cellspacing="6" cellpadding="0" align="center">
    <tr>
      <% out.println("<td rowspan=\"2\" valign=\"middle\" class=\"titpag\" align=\"right\" width=\"35\"><IMG SRC=\"/gifs/EnlaceMig/glo25040b.gif\" BORDER=\"0\"></td>");%>
    </tr>
  </table>
  <table width="500" border="0" cellspacing="6" cellpadding="0" align="center">
    <tr>
      <td width="317" valign="top" class="titpag">Comprobante
        de operaci&oacute;n</td>
      <td valign="top" class="titpag" height="40"><img src="/gifs/EnlaceMig/glo25030.gif" width="152" height="30" alt="Enlace"></td>

    </tr>
    <tr>
      <td valign="top" class="titenccom">Transmisi&oacute;n
        por pago de impuestos
        y derechos federales</td>
    </tr>
  </table>
  <table width="500" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
    </tr>
  </table>
  <table width="500" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td colspan="3">
      </td>
    </tr>
    <tr>
      <td width="21" background="/gifs/EnlaceMig/gfo25030.gif"><img src="/gifs/EnlaceMig/gau25010.gif" width="21" height="2" alt=".." name=".."></td>
      <td width="458" height="150" valign="top" align="left">
        <table width="458" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td class="textabcom" colspan="3" nowrap><img src="/gifs/EnlaceMig/gau25010.gif" height="2" alt=".." name="." width="5"></td>
          </tr>
          <tr>
            <td class="textabcom" width="200" nowrap><span class="tittabcom">No.
              de folio:</span>
			  <%= request.getAttribute("folio") %>
			</td>
            <td class="textabcom" width="120" nowrap><span class="tittabcom">RFC:</span>
			  <%= request.getAttribute("rfc") %>
			</td>
            <td class="textabcom" width="120" nowrap><span class="tittabcom">Homoclave:</span>
              <%= request.getAttribute("homoclave") %>
			</td>
          </tr>
          <tr>
            <td class="textabcom" colspan="3" nowrap><span class="tittabcom">Cuenta:</span>
         	  <%= request.getAttribute("cuenta") %>
			</td>
          </tr>
          <tr>
            <td class="textabcom" colspan="3" nowrap><span class="tittabcom">Nombre
              o raz&oacute;n social:</span>
			  <%= request.getAttribute("nombrerazonsocial") %>
			  </td>
          </tr>
          <tr>
            <td class="tittabcom" colspan="3" nowrap><img src="/gifs/EnlaceMig/gau25010.gif" height="2" alt=".." name="." width="5"></td>
          </tr>
          <tr>
            <td class="tittabcom" colspan="3" nowrap>Domicilio</td>
          </tr>
        </table>
        <table width="458" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td class="tittabcom" align="right" width="134">Calle:</td>
            <td class="textabcom" width="120">
			<%= request.getAttribute("calle") %>
			</td>
            <td align="right" class="tittabcom" width="60">Colonia:</td>
            <td class="textabcom" width="120">
			<%= request.getAttribute("colonia") %>
			</td>
          </tr>
          <tr>
            <td class="tittabcom" align="right">Delegaci&oacute;n
              o municipio:</td>
            <td class="textabcom">
			<%= request.getAttribute("delegacion") %>
			</td>
            <td align="right" class="tittabcom">Estado:</td>
            <td class="textabcom">
			<%= request.getAttribute("estado") %>
			</td>
          </tr>
          <tr>
            <td class="tittabcom" align="right">Ciudad:</td>
            <td class="textabcom">
			<%= request.getAttribute("ciudad") %>
			</td>
            <td align="right" class="tittabcom">C.P.:</td>
            <td class="textabcom">
			<%= request.getAttribute("cp") %>
			</td>
          </tr>
          <tr>
            <td class="tittabcom" align="right">Tel&eacute;fono:</td>
            <td class="textabcom">
			<%= request.getAttribute("telefono") %>
			</td>
            <td align="right" class="tittabcom">&nbsp;</td>
            <td class="textabcom">&nbsp;</td>
          </tr>
        </table>
        <table width="458" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td class="textabcom" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" height="2" alt=".." name="." width="5"></td>
          </tr>
          <tr>
            <td class="textabcom" width="200"><span class="tittabcom">Tipo
              de pago:</span>
			  <%= request.getAttribute("tpago") %>
			  </td>
            <td class="textabcom" width="246"><span class="tittabcom">Per&iacute;odo
              de pago:</span>
  			  <%= request.getAttribute("periodopago") %>
			  </td>
          </tr>
          <tr>
            <td class="textabcom"><span class="tittabcom">Importe
              a pagar:</span>
			  <%--
				//BaseServlet baseServlet = new BaseServlet();
				//String importe = baseServlet.FormatoMoneda(request.getAttribute("importe"));
				importe = FormatoMoneda((String)request.getAttribute("importe"));
			  --%>
			  <%=FormatoMoneda.formateaMoneda(new Double(valor).doubleValue())%>
			  </td>
            <td class="textabcom"><span class="tittabcom">Fecha
              de pago:</span>
			  <%= request.getAttribute("fecha") %>
            </td>
          </tr>
        </table>
        <table width="458" border="0" cellspacing="0" cellpadding="3">
          <tr valign="top">
            <td class="tittabcom" nowrap>Forma
              de entrega
              de declaraci&oacute;n:</td>
            <td class="textabcom"><%= request.getAttribute("formaentrega") %>
              La declaraci&oacute;n
              se debe
              entregar
              el mismo
              d&iacute;a
              de pago.</td>
          </tr>
          <tr valign="top">
            <td class="textabcom" nowrap colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" height="2" alt=".." name="." width="5"></td>
          </tr>
        </table>
      </td>
      <td width="21" background="/gifs/EnlaceMig/gfo25040.gif"><img src="/gifs/EnlaceMig/gau25010.gif" width="21" height="2" alt=".." name=".."></td>
    </tr>
  </table>
  <table width="500" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
  </table>
  <table width="500" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td align="left" class="textabcom"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="3" alt=".." name="."></td>
    </tr>
    <tr>
      <td class="textabcom">NOTA: El presente documento no es un comprobante fiscal
        de pago. Para PRESENTACI&Oacute;N EN PAPEL, deber&aacute;
        presentarla a la sucursal, ubicada dentro
        de su circunscripci&oacute;n regional bancaria, EL MISMO D&Iacute;A EN
        QUE REALIZ&Oacute; SU PAGO, de conformidad con el punto 2.10.21, inciso
        b, fracci&oacute;n 5 de la resoluci&oacute;n Misc&eacute;lanea en vigor.
        Las declaraciones cuya transferencia se efect&uacute;en despu&eacute;s
        del cierre de la sucursal, se sellar&aacute;n con la fecha del siguiente
        d&iacute;a h&aacute;bil. </td>
    </tr>
  </table>
  <table width="500" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td align="center" height="30" valign="bottom">
        <table width="154" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="83">
			  <a href = "javascript:scrImpresion();">
			  <img src="/gifs/EnlaceMig/gbo25240.gif" width="83" height="22" border="0">
			  </a>
			  </td>
            <td width="71"><a href="javascript:;" onClick="window.close()"><img src="/gifs/EnlaceMig/gbo25200.gif" width="71" height="22" border="0" alt="Cerrar"></a></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
</body>
</html>