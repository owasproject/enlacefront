<%
String parDiasInhabiles = (String)request.getAttribute("diasInhabiles");
String parNewMenu = (String)request.getAttribute("newMenu");
String parMenuPrincipal = (String)request.getAttribute("MenuPrincipal");
String parEncabezado = (String)request.getAttribute("Encabezado");
String parFechaIni = (String)request.getAttribute("FechaIni");
String parFechaFin = (String)request.getAttribute("FechaFin");
String parFechaHoy = (String)request.getAttribute("FechaHoy");

if(parDiasInhabiles == null) parDiasInhabiles = "";
if(parNewMenu == null) parNewMenu = "";
if(parMenuPrincipal == null) parMenuPrincipal = "";
if(parEncabezado == null) parEncabezado = "";
if(parFechaIni == null) parFechaIni = "";
if(parFechaFin == null) parFechaFin = "";
if(parFechaHoy == null) parFechaHoy = "";
%>

<html>

<head>
	<title>Enlace</title>
	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
	<meta http-equiv="Content-Type" content="text/html;">
	<meta http-equiv="Pragma" content="no-cache"/>
	<script language="JavaScript" src="/EnlaceMig/cuadroDialogo.js"></script>
	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
	<Script language="JavaScript" src="/EnlaceMig/ConMov.js"></Script>
	<Script language="JavaScript">

	// --- Selección de fechas ---------------------------------------------------------
	var js_diasInhabiles = "<%= parDiasInhabiles %>"
	var dia;
	var mes;
	var anio;
	var fechaseleccionada;
	var opcioncaledario;

	function WindowCalendar(opcion)
		{
		dia= <%= parFechaHoy.substring(0,parFechaHoy.indexOf("/")) %>;
		mes= <%= parFechaHoy.substring(parFechaHoy.indexOf("/") + 1,parFechaHoy.lastIndexOf("/")) %>;
		anio= <%= parFechaHoy.substring(parFechaHoy.lastIndexOf("/") + 1) %>;
		n = mes-1;
		opcioncalendario=opcion;
		msg=window.open("/EnlaceMig/calpas1.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
		msg.focus();
		}

	function Actualiza()
		{
		if (opcioncalendario==1)
			document.FrmMov.fecha1.value=fechaseleccionada;
		else if (opcioncalendario==2)
			document.FrmMov.fecha2.value=fechaseleccionada;
		}

	// --- Filtro de cuentas -----------------------------------------------------------
	<!-- *********************************************** -->
	<!-- modificación para integración pva 07/03/2002    -->
	<!-- *********************************************** -->

	var ctaselec;
	var ctadescr;
	var ctatipre;
	var ctatipro;
	var ctaserfi;
	var ctaprod;
	var ctasubprod;
	var tramadicional;
	var cfm;

	function PresentarCuentas()
		{
		msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no," + "directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
		msg.focus();
		}

	function actualizacuenta()
		{
		document.FrmMov.textEnlaceCuenta.value=ctaselec+" "+ctadescr;
		}

	// --- Elimina espacios que sobran -------------------------------------------------
	function trim(val)
		{
		while(val != "" && val.charAt(0)== " ") val = val.substring(1);
		while(val != "" && val.charAt(val.length-1)== " ") val = val.substring(0,val.length-1);
		return val;
		}

	// --- Envía los datos al servlet --------------------------------------------------
	function DoPost()
		{
		var f1 = document.FrmMov.fecha1.value;
		var f2 = document.FrmMov.fecha2.value;
		var valVista = document.FrmMov.textEnlaceCuenta.value;

		var aux1 = "";
		var aux2 = "";
		//Getronics Inicia Modificación IM392231 NALM  15-enero-2004 Norma Angelica Lopez Meza
		//var refInt1 = "";
		//var refInt2 = "";
		//Getronics Fin Modificación IM392231 NALM  15-enero-2004 Norma Angelica Lopez Meza
		var numCheq1 = "";
		var numCheq2 = "";
		var retorno = true;

		if (valVista.length == 0)
			{
			cuadroDialogo("Debe seleccionar una cuenta",3);
			return;
			}

		aux1 = f1.substring(6,10) + f1.substring(3,5) + f1.substring(0,2);
		aux2 = f2.substring(6,10) + f2.substring(3,5) + f2.substring(0,2);
		//Getronics Inicia Getronics Modificación IM392231 NALM  15-enero-2004 Norma Angelica Lopez Meza
		//refInt1 = trim(document.FrmMov.refInt1.value);
		//refInt2 = trim(document.FrmMov.refInt2.value);
		//Getronics Fin Getronics Modificación IM392231 NALM  15-enero-2004 Norma Angelica Lopez Meza
		numCheq1 = trim(document.FrmMov.numCheq1.value);
		numCheq2 = trim(document.FrmMov.numCheq2.value);

		if(aux1 > aux2)
			{
			cuadroDialogo("La fecha inicial debe ser menor o igual a la fecha final",3);
			return;
			}

		if(numCheq1 != "" || numCheq2 != "")
			{
			if(numCheq2 == "" || isNaN(numCheq2) || numCheq2.indexOf(".") != -1) {document.FrmMov.numCheq2.focus(); retorno = false;}
			if(numCheq1 == "" || isNaN(numCheq1) || numCheq1.indexOf(".") != -1) {document.FrmMov.numCheq1.focus(); retorno = false;}
			}

		//Getronics Inicia Modificación IM392231 NALM  15-enero-2004 Norma Angelica Lopez Meza
	 //	if(refInt1 != "" || refInt2 != "")
		//	{
	//		if(refInt2 == "" || isNaN(refInt2) || refInt2.indexOf(".") != -1) {document.FrmMov.refInt2.focus(); retorno = false;}
	//		if(refInt2 == "" || isNaN(refInt1) || refInt1.indexOf(".") != -1) {document.FrmMov.refInt1.focus(); retorno = false;}
	//		}
		//Getronics Fin Modificación IM392231 NALM  15-enero-2004 Norma Angelica Lopez Meza

		if(!retorno)
			{
			cuadroDialogo("El valor debe ser un n&uacute;mero entero",3);
			return;
			}

		if(numCheq1 != "" || numCheq2 != "")
			if(parseInt(numCheq2)<parseInt(numCheq1))
				{
				cuadroDialogo("El n&uacute;mero de cheque final debe ser mayor al inicial",3);
				return;
				}

	//Getronics Inicia Modificación IM392231 NALM  15-enero-2004 Norma Angelica Lopez Meza
	//	if(refInt1 != "" || refInt2 != "")
	//		if(parseInt(refInt2)<parseInt(refInt1))
	//			{
	//			cuadroDialogo("La referencia interna final debe ser mayor a la inicial",3);
	//			return;
	//			}
	//Getronics Fin Modificación IM392231 NALM  15-enero-2004 Norma Angelica Lopez Meza
		document.FrmMov.submit();
		}

	// --- inicializador ---------------------------------------------------------------
	function inicializa()
		{
		MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif',
				'/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif',
				'/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif',
				'/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif',
				'/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif',
				'/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif',
				'/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif',
				'/gifs/EnlaceMig/gbo25011.gif');
		}

	// --- construye la trama de info antes de enviar el formulario --------------------
	function construyeTrama()
		{
		document.FrmMov.Trama.value = "@" + ctaselec + "|" + ctadescr + "|"
					+ document.FrmMov.fecha1.value + "|" + document.FrmMov.fecha2.value + "|"
//Getronics Inicia Modificación IM392231 NALM  15-enero-2004 Norma Angelica Lopez Meza
//					+ document.FrmMov.refInt1.value + "|" + document.FrmMov.refInt2.value + "|"
//Getronics Fin Modificación IM392231 NALM  15-enero-2004 Norma Angelica Lopez Meza
                    + "|" +  "|"
					+ document.FrmMov.numCheq1.value + "|" + document.FrmMov.numCheq2.value + "|";
		DoPost();
		}

	// --- Funciones para el menú ------------------------------------------------------

	function MM_preloadImages()
		{ //v3.0
		var d=document;
		if(d.images)
			{
			if(!d.MM_p) d.MM_p=new Array();
			var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
			for(i=0; i<a.length; i++)
				if (a[i].indexOf("#")!=0)
					{
					d.MM_p[j]=new Image;
					d.MM_p[j++].src=a[i];
					}
			}
		}

	function MM_swapImgRestore()
		{ //v3.0
		var i,x,a=document.MM_sr;
		for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
		}

	function MM_findObj(n, d)
		{ //v3.0
		var p,i,x;
		if(!d) d=document;
		if((p=n.indexOf("?"))>0&&parent.frames.length)
			{d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
		}

	function MM_swapImage()
		{ //v3.0
		var i,j=0,x,a=MM_swapImage.arguments;
		document.MM_sr=new Array;
		for(i=0;i<(a.length-2);i+=3)
			if ((x=MM_findObj(a[i]))!=null)
				{
				document.MM_sr[j++]=x;
				if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];
				}
		}

	<%= parNewMenu %>
	</script>
</head>

<!-- Aquí empieza el cuerpo =========================================================-->
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
	onLoad="inicializa()" background="/gifs/EnlaceMig/gfo25010.gif">


	<!-- MENU PRINCIPAL Y ENCABEZADO-->
	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top">
		<td width="*"><%= parMenuPrincipal %></TD>
	</TR>
	</TABLE>
	<%= parEncabezado %>

    <!-- consultaMulticheque -->
	<FORM NAME="FrmMov" METHOD="Post" onSubmit="DoPost();" Action="ChesBenefBaja">
	<input type="Hidden" name="Accion" value="CONSULTAR">
	<input type="Hidden" name="Trama" value="">
	<input type="Hidden" name="ventana" value="10">

	<table width="760" border="0" cellspacing="0" cellpadding="0">
	<tr><td align="center">


	<!-- Campos de entrada -------------------------------------------->
	<table width="400" border="0" cellspacing="2" cellpadding="3">
	<tr align="center">
		<td class="textabdatcla" colspan="2">


			<table width="390" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td align="right" class="tabmovtexbol" width="100" nowrap>Cuenta:</td>
				<td width="290">

					<!-- campo cuenta -->
					<input type="text" name=textEnlaceCuenta  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="">
					<A HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle></A>

				</td>
			</tr>
			<tr>
				<td align="right" class="tabmovtexbol" nowrap>De la fecha:</td>
				<td class="tabmovtex" nowrap>

					<!-- Campo fecha inicial -->
					<input type="text" name="fecha1" size="12" class="tabmovtexbol" value="<%= parFechaIni %>">
					<A href ="javascript:WindowCalendar(1);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></A>

				</td>
			</tr>
			<tr>
				<td align="right" class="tabmovtexbol" nowrap>A la fecha:</td>
				<td class="tabmovtex" nowrap>

					<!-- campo fecha final -->
					<input type="text" name="fecha2" size="12" class="tabmovtexbol" value="<%= parFechaFin %>">
					<A href ="javascript:WindowCalendar(2);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></A>

				</td>
			</tr>
			<!--Getronics Inicio Modificación IM392231 NALM  15-enero-2004 -->

			<!--tr>
				<td align="right" class="tabmovtex" rowspan=2 nowrap>Referencia interna</td>
				<td class="tabmovtex" nowrap>


					< campo referencia interna desde >
					Desde:&nbsp;<input type="text" name="refInt1" size=7>

				</td>
			</tr-->
			<!--tr>
				<td class="tabmovtex" nowrap>

					< campo referencia interna hasta >
					Hasta:&nbsp;<input type="text" name="refInt2" size=7>

				</td>
			</tr-->

			<!--Getronics Fin Modificación IM392231 NALM  15-enero-2004 -->
			<tr>
				<td align="right" class="tabmovtex" rowspan=2 nowrap>N&uacute;mero de cheque</td>
				<td class="tabmovtex" nowrap>

					<!-- campo referencia interna desde -->
					Desde:&nbsp;<input type="text" name="numCheq1" size=7>

				</td>
			</tr>
			<tr>
				<td class="tabmovtex" nowrap>

					<!-- campo referencia interna hasta -->
					Hasta:&nbsp;<input type="text" name="numCheq2" size=7>

				</td>
			</tr>
			</table>


		</td>
	</tr>

	</table>

	</td></tr><tr><td align="center">

	<br>

	<!-- Botones------------------------------------------------------ -->
	<table width="160" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="25">
		<tr>
			<td align="right" height="25"><a href="javascript:construyeTrama();" border=0><img  src="/gifs/EnlaceMig/gbo25220.gif" border=0></a></td>
			<td align="left" height="25"><a href="javascript:document.FrmMov.reset();" border=0><img src="/gifs/EnlaceMig/gbo25250.gif" border=0></a></td>
		</tr>
	</table>


	</td></tr>


	</table>
	</form>

</body>

</html>