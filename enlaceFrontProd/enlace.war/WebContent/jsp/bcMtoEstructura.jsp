	<%@ page import="java.util.StringTokenizer" %>
	<%@ page import="java.util.Vector"%>
	<%@ page import="mx.altec.enlace.bo.*" %>

	<%
		// ------------------- Obtencion de parametros ---------------------------
		String parMenuPrincipal = (String)request.getAttribute("MenuPrincipal");
		String parFuncionesMenu = (String)request.getAttribute("newMenu");
		String parEncabezado = (String)request.getAttribute("Encabezado");
		String parCuentas = (String)request.getAttribute("Cuentas");
		String parMensaje = (String)request.getAttribute("Mensaje");
		String parTrama = (String)request.getAttribute("Trama");
		String parDiasInhabiles = (String)request.getAttribute("parDiasInhabiles");
		String parFecha = (String)request.getAttribute("Fecha");
		String parMovFechas = (String)request.getAttribute("Movfechas");
		String parFechaAyer = (String)request.getAttribute("FechaAyer");
		String parFechaPrimero = (String)request.getAttribute("FechaPrimero");
		String parVarFechaHoy = (String)request.getAttribute("VarFechaHoy");
		String parFechaHoy = (String)request.getAttribute("FechaHoy");
		String parFechaDia = (String)request.getAttribute("FechaDia");
		String parLineaCredito = (String)request.getAttribute("LineaCredito");
		String darAltaBC		= (String)request.getAttribute("AltaBC");


		if(parMenuPrincipal == null) parMenuPrincipal = "";
		if(parFuncionesMenu == null) parFuncionesMenu = "";
		if(parEncabezado == null) parEncabezado = "";
		if(parCuentas == null) parCuentas = "";
		if(parMensaje == null) parMensaje = "";
		if(parTrama == null) parTrama = "";
		if(parDiasInhabiles == null) parDiasInhabiles = "";
		if(parFecha == null) parFecha = "";
		if(parMovFechas  == null) parMovFechas = "";
		if(parFechaAyer == null) parFechaAyer = "";
		if(parFechaPrimero == null) parFechaPrimero = "";
		if(parVarFechaHoy == null) parVarFechaHoy = "";
		if(parFechaHoy == null) parFechaHoy = "";
		if(parFechaDia == null) parFechaDia = "";
		if(parLineaCredito == null) parLineaCredito = "";


		Vector cuentas;
		cuentas = ((new mx.altec.enlace.servlets.bcMtoEstructura()).creaCuentas(parCuentas));

		int a, b;
	%>

<!--==========================COMIENZA CODIGO HTML ========================================-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>

<HEAD>
	<TITLE>Consulta y mantenimiento de estructuras de Cuentas</TITLE>
	<META NAME="Generator" CONTENT="EditPlus">
	<META NAME="Author" CONTENT="MARL.">
	<LINK rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

	<!--  ========================== Scripts ===================================-->
	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
	<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
	<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
	<script language="javascript" src="/EnlaceMig/ValidaFormas.js"></script>



	<SCRIPT language="javascript">

	//Obtenci�n de los respectivos dias Inhabiles y modificado para Integracin
	var js_diasInhabiles = "<%= request.getAttribute("diasInhabiles") %>"

	diasInhabiles = '<%= request.getAttribute("DiasInhabiles") %>';

	var indice =0;
	//----------------de lo dos calendarios--------------------------------------


	// de la actualizacion de las cuentas seleccionadas
	var ctaselec;
	var ctadescr;
	var ctatipre;
	var ctatipro;
	var ctaserfi;
	var ctaprod;
	var cfm;
	var y=0;
	var tipoCta = "" + ctatipro;
	var lineasCredito;
	var lineas2 = new Array()
	var contadorlin =0;


	//de la parte html cuentas y descripcin
	function PresentarCuentas()
	{

		msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1&LineasCredito=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
		msg.focus();
	}

	function actualizacuenta()
	{
		document.Forma.textdestino.value=ctaselec+" -- "+ctadescr;
		actualizaCombo();
	}

	function actualizaCombo()
	{
		//Desentramar la cadena
		var lineas       = new Array();
		var cont         = 0;
		var cadena       = lineasCredito;
		var cadenaAux    = "";
		var max          = 0;
		var lineasPropias= "";

		if(cadena.indexOf("$")>=0)
		 {

		   lineasPropias = cadena.substring(cadena.indexOf("$")+1,cadena.length);
		   cadena=cadena.substring(0,cadena.indexOf("$"));

		 }

		 //alert("CONSULTA EXITOSA");
		 //alert("LineasPropias:"+ lineasPropias + " Cadena:"+ cadena );

		if( cadena != "")
		{
			cadena = cadena.substring(1);
			while(cadena.length > 0)
				{
				max = (cadena.indexOf("|") == -1)?cadena.length:cadena.indexOf("|");
				cadenaAux = cadena.substring(0,max);

				if(lineasPropias.indexOf(cadenaAux) >=0 )
				 {

				  	lineas[cont++] = cadenaAux;

				 }

				cadena = cadena.substring(max+((cadena.indexOf("|")==-1)?0:1));

				}
		}
		else
		{

			avisoError("No se encontraron lineas de credito o propias.");

		}


		// Poner nuevos valores
		document.Forma.lineaCred.options.length = lineas.length;

		for(a=0;a<lineas.length;a++)
		{
			document.Forma.lineaCred.options[a].text = lineas[a];
			lineas2[a] = lineas[a];
		}




	}  //fin de la funcion


	// -------------------------------- BOTONES ------------------------------------

	function agregar()
	{
		var trama = "@";
		var a, b=0,c, x=0, r=1;
		var numCta = ctaselec;
		var esEditable = !(ctaSel() == -1 && tipoCta != "2" && tipoCta != "3");
		var seleccion;
		var impvalido;
		var sel = ctaSel();
		var bandera = 1;
		var numeltos;

		//Se valida los datos de usuario
		if(document.Forma.textdestino.value == "")
		{
			avisoError("Debe seleccionar una cuenta.");
			return;
		}

		//validacion de una cuenta hija asignada a una cuenta Padre.
		if(document.Forma.CtaSel.length >=3)
			if(sel == -1)
			{
				avisoError("Error: Debe Seleccionar la cuenta padre de nivel 1 para que &eacute;sta cuenta sea hija"); return;
			}


		//validaciones para el techo presupuestal
		if(document.Forma.valtecho[0].checked == true)
		{
			//Comprueba si lleva signo "$" y "," el techo presupuestal
			var Valortecho;

			if(document.Forma.techopre.value.substring(0,1) == "$")
			{
				Valortecho = document.Forma.techopre.value.substring(1);
				Valortecho = Valortecho.replace(',','');
				Valortecho = Valortecho.replace(' ','');

				document.Forma.techopre.value = Valortecho;
			}
			else
			{
				Valortecho = document.Forma.techopre.value;
				Valortecho = Valortecho.replace(',','');
				Valortecho = Valortecho.replace(' ','');

				document.Forma.techopre.value = Valortecho;
			}

			if(document.Forma.techopre.value== " " || document.Forma.techopre.value=="" || document.Forma.techopre.value== null)
			{
				avisoError("Error:Debe proporcionar un techo presupuestal.");
				return;
			}
		}
		else
		{
			document.Forma.techopre.value="0";
		}


		if(!validahoras())
			{ avisoError("La hora inicial no debe ser mayor a la hora final. "); return;}


		if(document.Forma.lineaCred.options.length == 0 || document.Forma.lineaCred.selectedIndex == -1)
		{
				avisoError("La cuenta de cheques debe tener una l&iacute;nea de cr&eacute;dito asociada, verifique.");
				return;
		}

		if(!ImportIsValid())
		{
			avisoError("Error:El Techo presupuestal debe ser num&eacute;rico.");
			return;
		}


		numeltos = document.Forma.CtaSel.length;
		trama +=ctaselec + "|"															 //Cuenta
		trama += ((ctaSel() == -1)?" |":(document.Forma.CtaSel[ctaSel()].value) + "|");  //posiblePadre

		for(a=0;a<document.Forma.lineaCred.options.length;a++)
				if(document.Forma.lineaCred.options[a].selected)
				{
					trama += document.Forma.lineaCred.options[a].text + "|";
				}
		trama +=  "1|";																//Nivel

		if (numeltos==2)
			trama+= "0|0|0|01/01/01|02/02/02";
		else {

		//techo presupuestal
		trama +=  document.Forma.techopre.value  + "|";

		for(b=0;b<document.Forma.horarioOper1.options.length;b++)
		{
			if(document.Forma.horarioOper1.options[b].selected)
				trama += document.Forma.horarioOper1.options[b].text + "|"; 		//Hora1
		}



		for(x=0;x<document.Forma.horarioOper2.options.length;x++)
		{
			if(document.Forma.horarioOper2.options[x].selected)
				trama += document.Forma.horarioOper2.options[x].text + "|01/01/01|02/02/02" ; 		}//Hora2

		}




		///Validacion para no agregar cuentas hijas a cuentas hijas...
		// no se pueden agregar cuentas nietas.
		if(sel== 1 || sel== 2 || sel== 3 || sel== 4 || sel== 5 || sel== 6 || sel== 7 || sel== 8 || sel== 9 || sel== 10)
		{
			avisoError("Error: debe seleccionar la cuenta padre de nivel 1, para que &eacute;sta cuenta sea hija.");
			return;
		}


		//Validaci�n para ignorar datos de la cuenta padre.

		if(document.Forma.CtaSel.length == 2)
		{

			confirma("La informaci&oacute;n de Horario de operaci&oacute;n y Techo presupuestal no aplica para &eacute;sta cuenta padre, &iquest;Desea continuar?");

			document.Forma.Trama.value = trama;
			accionConfirma =7;


		}
		else
		{

			 document.Forma.Accion.value = "AGREGA";
			 document.Forma.Trama.value = trama;
	 		 document.Forma.submit();
		}

		for(x=0;x<document.Forma.horarioOper1.options.length;x++)
		{
				if(document.Forma.horarioOper1.options[x].selected)
					Hora1 = document.Forma.horarioOper1.options[x].text

		}

		for(x=0;x<document.Forma.horarioOper2.options.length;x++)
		{
				if(document.Forma.horarioOper2.options[x].selected)
					Hora2 = document.Forma.horarioOper2.options[x].text

		}



	}


	function alta()
	{
	   var forma=document.Forma;
	   var darAltaBC=trimString(forma.AltaBC.value);

	   if(darAltaBC!="SI")
	   {
		document.Forma.Accion.value = "ALTA_ARBOL";

		if(document.Forma.CtaSel.length == 3)
		{
			avisoError("No se puede dar de alta una estructura con solo una cuenta.");
			return;
		}
		else if(document.Forma.CtaSel.length == 2)
		{
			avisoError("La estructura est&aacute; vac&iacute;a");
			return;
		}

   		document.Forma.submit();

	   }
	  else
	   {
		altaAlterna()
	   }

	}


	function altaAlterna()
	{

	  darAltaBC=document.Forma.AltaBC.value;

		if(document.Forma.CtaSel.length == 3)
		{
			avisoError("No se puede dar de alta una estructura con solo una cuenta.");
			return;
		}

		else if(document.Forma.CtaSel.length == 2)
		{
			avisoError("La estructura est&aacute; vac&iacute;a");
			return;
		}

		confirma("Ya existe una estructura para &eacute;ste contrato, &iquest;Desea reemplazarla?");
		accionConfirma  = 5;

	}

	function borrar()
	{
		if(document.Forma.CtaSel.length == 2)
			{avisoError("La estructura est&aacute; vac&iacute;a");
			return;}
		confirma("La estructura va a ser borrada, &iquest;Desea continuar?");
		accionConfirma = 2;
	}

	function modificar()
	{
		var sel = ctaSel();
		var y;
		var valHijaPadre = sel;



		if(sel == -1)
		{
			aviso("Debe seleccionar una cuenta para Modificar");
			return;
		}

		document.Forma.Accion.value = "MODIFICA";
		document.Forma.Trama.value  = document.Forma.CtaSel[ctaSel()].value;

		document.Forma.lineaCredito.value = lineas2;


		document.Forma.padreHija.value = valHijaPadre;
		document.Forma.submit();
	}

	function eliminar()
	{
	   	var sel = ctaSel();

		if(sel == -1)
		{
			aviso("Debe seleccionar una cuenta para eliminar");
			return;
		}

		if(sel==0)
		{
			confirma("Se va a borrar la cuenta seleccionada, as&iacute; como sus cuentas hija en caso de tenerlas. &iquest;Desea continuar?");
		}
		else
		{
			confirma("Se va a borrar el registro de la cuenta hija seleccionada, as&iacute; como sus programaciones. &iquest;Desea continuar?");
		}

		accionConfirma = 3;
		return;
	}

	function imprimir()
	{
		scrImpresion();
	}

	function validahoras()
	{
		var a = (document.Forma.horarioOper1.selectedIndex + 2);
		var b = (document.Forma.horarioOper2.selectedIndex +2 );
		if(a > b)
			return false;
		else
			return true;
	}


	function selUno(num)
		{
		var max = document.Forma.CtaSel.length - 2;
		for(a=0;a<max;a++) if(a != num) document.Forma.CtaSel[a].checked = false;
		}

	function ctaSel()
	{
		var sel = -1;
		var max = document.Forma.CtaSel.length - 2;
		for(a=0;a<max;a++) if(document.Forma.CtaSel[a].checked) {sel = a; break;}
		return sel;
	}

	//  Permite desabilitar la caja de texto de el Techo
	//  Presupuestal (2 funciones siguientes)
	var _T = "locked";
	var _F = "unlocked";

	function valTecho(_P)
	{
		var _L = document.Forma.lck.value;
		if(_L==_P)
			return;
		document.Forma.techopre.disabled=(document.Forma.lck.value=(_L==_F)?_T:_F)==_T;
		document.Forma.techopre.value="";


	}

	function isDis()
	{
		return (document.Forma.lck.value==_T);
	}


	function imprimeUnaLinea(texto)
	{
	    for(a=0;a<texto.length;a++)
		if(texto.substring(a,a+1) != " ")
			document.write(texto.substring(a,a+1));
		else
		{
			document.write("&");
			document.write("n");
			document.write("b");
			document.write("s");
			document.write("p");
			document.write(";");
		}
	}

	// --- Para cuadros de dialogo
	var respuesta = 0;
	var accionConfirma;
	var habilitaConfirma = true;

	//var trama = document.Forma.Trama.value;

	function continua()
	{

		trama = document.Forma.Trama.value;

		if(accionConfirma == 1)
		{

			if(respuesta == 1) agregar();
		}
		else if(accionConfirma == 2)
		{

			if(respuesta == 1)
				{

				document.Forma.Accion.value = "BORRA_ARBOL";
				document.Forma.submit();
				}
		}

			else if(accionConfirma == 5)	///validacion dela Alta Alterna
			{
				if(respuesta==1)
				{
				document.Forma.Accion.value = "ALTA_ARBOL";
				document.Forma.submit();
				}
			}//Hasta aqui nueva Mod.


			else if(accionConfirma == 7)	//nueva mod. hoy
			{
				if(respuesta==1)
				{
					 document.Forma.Accion.value ="AGREGA";
					 document.Forma.Trama.value = trama;
					 document.Forma.submit();
				}
			}

		else
		{
			if(respuesta == 1)
			{
				document.Forma.Accion.value = "ELIMINA";
				document.Forma.Trama.value  = document.Forma.CtaSel[ctaSel()].value;
				document.Forma.submit();
			}
		}


		habilitaConfirma = true;

	} //function continua()

	function confirma(mensaje)
	{

		if(habilitaConfirma)
			cuadroDialogo(mensaje,2);
		habilitaConfirma = false;

	}

	function aviso(mensaje) {cuadroDialogo(mensaje,1);}

	function avisoError(mensaje) {cuadroDialogo(mensaje,3);}

	// --- Inicio y envio
	function inicia()
	{

	}

	function validaEnvio()
	{
		if(document.Forma.Accion.value == "")
			return false;

		return true;
	}

	// --- Funciones de meno
	function MM_preloadImages()
	{ //v3.0
		var d=document;
		if(d.images)
			{
			if(!d.MM_p) d.MM_p=new Array();
			var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
			for(i=0; i<a.length; i++)
			if (a[i].indexOf("#")!=0)
				{d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}
			}
	}

	function MM_swapImgRestore()
	{ //v3.0
		var i,x,a=document.MM_sr;
		for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++)
		x.src=x.oSrc;
	}

	function MM_findObj(n, d)
	{ //v3.0
		var p,i,x;
		if(!d) d=document;
		if((p=n.indexOf("?"))>0&&parent.frames.length)
			{
			d=parent.frames[n.substring(p+1)].document;
			n=n.substring(0,p);
			}
		if(!(x=d[n])&&d.all) x=d.all[n];
		for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
		return x;
	}

	function MM_swapImage()
	{ //v3.0
		var i,j=0,x,a=MM_swapImage.arguments;
		document.MM_sr=new Array;
		for(i=0;i<(a.length-2);i+=3)
			if((x=MM_findObj(a[i]))!=null)
				{
				document.MM_sr[j++]=x;
				if(!x.oSrc) x.oSrc=x.src;
				x.src=a[i+2];
				}
	}

	<%= parFuncionesMenu %>

	function ImportIsValid()
	{
		var x1=0;
		var cont=0;
		Texto1=document.Forma.techopre.value;
		var Texto2 = "";

		for(x1=0;x1<Texto1.length;x1++)
			{
			Texto2 = Texto1.charAt(x1);
			if((Texto2>='0' && Texto2<='9') || Texto2=='.') cont++;
			}

		if(cont!=x1){	return false; }

		return true;
	}

	function SeleccionaFecha(ind)
	{
		var m=new Date();
		n=m.getMonth();
		Indice=ind;

			js_diasInhabiles = "<%= request.getAttribute("diasInhabiles") %>";
			var parMovFechasx = "<%= request.getAttribute("Movfechas") %>";
			var FrmFechasDia = parMovFechasx.substring(0,parMovFechasx.indexOf("-"));
			parMovFechasx = parMovFechasx.substring(parMovFechasx.indexOf("-") + 1);
			var FrmFechasMes = parMovFechasx.substring(0,parMovFechasx.indexOf("-"));
			parMovFechasx = parMovFechasx.substring(parMovFechasx.indexOf("-") + 1);
			var FrmFechasAnio = parMovFechasx;

			dia = FrmFechasDia;
			mes = FrmFechasMes;
			anio = FrmFechasAnio;

			msg=window.open("/EnlaceMig/EI_CalendarioBC.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
		msg.focus();

	}



	function verificaFechas()
		{
		var TipoB="";
		var y=0;

		for(i1=0;i1<document.Forma.length;i1++)
		if(document.Forma.elements[i1].type=='radio')

				TipoB=document.Forma.elements[i1].value;


		if(TipoB=="techoNo")
			{
			if(mes1[0]>mes1[1] && anio1[0]==anio1[1])
				{
				cuadroDialogo("Error: La fecha Final no debe ser menor a la inicial.",3);
				return false;
				}
			if(mes1[0]==mes1[1] && anio1[0]==anio1[1])
				if(dia1[0]>dia1[1])
					{

					return false;
					}
			}
		return true;
		}

	<%= request.getAttribute("VarFechaHoy") %>   //del MCL_Movimientos
	</SCRIPT>

</HEAD>

<!-- CUERPO ==========================================================================-->
<BODY onload="inicia();" background="/gifs/EnlaceMig/gfo25010.gif" bgcolor=#ffffff>

	<FORM name = "Frmfechas">
	<%
		String FrmFechasAnio = parMovFechas.substring(0,parMovFechas.indexOf("-"));
		parMovFechas = parMovFechas.substring(parMovFechas.indexOf("-") + 1);
		String FrmFechasMes = parMovFechas.substring(0,parMovFechas.indexOf("-"));
		parMovFechas = parMovFechas.substring(parMovFechas.indexOf("-") + 1);
		String FrmFechasDia = parMovFechas;
	%>

	<!-- input name=Fecha type=hidden value='<%--= request.getAttribute("FechaDia") --%>' size=10  onFocus='blur();' maxlength=10-->
	<Input type = "hidden" name ="strDia" value = "<%= FrmFechasAnio %>">
	<Input type = "hidden" name ="strMes" value = "<%= FrmFechasMes %>">
	<Input type = "hidden" name ="strAnio" value = "<%= FrmFechasDia %>">
	</FORM>


	<!-- MENU PRINCIPAL Y ENCABEZADO ---------------------------->
	<TABLE border="0" cellpadding="0" cellspacing="0" width="610">
		<TR valign="top">
			<TD width="*">
				<%=parMenuPrincipal%>
			</TD>
		</TR>
	</TABLE>

	<%=parEncabezado%>


	<FORM name=Forma  onSubmit="return validaEnvio()" action="bcMtoEstructura">
	<INPUT type=Hidden name="Accion" value="">
	<INPUT type=Hidden name="Trama" value="">
	<INPUT type=Hidden name="Cuentas" value="<%= parCuentas %>">
	<INPUT type=Hidden name="lineaCredito" value="">
	<INPUT type=hidden name=AltaBC value="<%= request.getAttribute("AltaBC") %>" size=5>
	<INPUT type=Hidden name="padreHija" value="<%=request.getAttribute("padreHija")%>">


	<DIV align=center>

	<TABLE class="textabdatcla" border=0 cellspacing=0 cellpadding=3>
	<TR><TD class="tittabdat" colspan=3>&nbsp;Seleccione la cuenta a integrar y las condiciones de concentraci&oacute;n</TD></TR>

	<TR>
		<TD class='tabmovtex'>
			<BR>&nbsp;Cuenta:<BR>

			<!-- Campo de selecci�n de cuenta -->
			<INPUT type="Text" name="textdestino" size=35 onFocus="blur()">&nbsp;<A HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" border=0 align=absmiddle></A>
		</TD>
		<TD class='tabmovtex' colspan=2 width="200">
			<BR>&nbsp;L&iacute;nea de Cr&eacute;dito:<BR>

			<!-- CAMPO LINEA DE CREDITO -->
			<SELECT NAME="lineaCred" width=190 style="width:100px;" name="a" class="tabmovtex">
			<OPTION></OPTION><OPTION></OPTION><OPTION></OPTION><OPTION></OPTION><OPTION></OPTION>
			<OPTION></OPTION><OPTION></OPTION><OPTION></OPTION><OPTION></OPTION><OPTION></OPTION>
			</SELECT>

		</TD>
	</TR>

	<TR><TD colspan=3>&nbsp;<BR></TD></TR>

	<TR>
		<TD class="tabmovtex" width="200" rowspan=2 nowrap>
			Horario de operaci&oacute;n:<BR>

			<!-- Campos de Horario de operaci�n -->
			<TABLE width=150>
			<TR><TD class="tabmovtex">De&nbsp;</TD><TD class="tabmovtex"><select width=100 style="width:100px;" name="horarioOper1" class="tabmovtex" onChange="">
				<option>08:30</option><option>09:00</option><option>09:30</option><option>10:00</option>
				<option>10:30</option><option>11:00</option><option>11:30</option><option>12:00</option>
				<option>12:30</option><option>13:00</option><option>13:30</option><option>14:00</option>
				<option>14:30</option><option>15:00</option><option>15:30</option><option>16:00</option>
				<option>16:30</option><option>17:00</option><option>17:30</option><option>18:00</option>
				<option>18:30</option><option>19:00</option><option>19:30</option>
				</select>
			</TD></TR>
			<TR><TD class="tabmovtex">A&nbsp;</TD><TD class="tabmovtex"><select width=100 style="width:100px;" name="horarioOper2" class="tabmovtex" onChange="">
				<option>09:00</option><option>09:30</option><option>10:00</option>
				<option>10:30</option><option>11:00</option><option>11:30</option><option>12:00</option>
				<option>12:30</option><option>13:00</option><option>13:30</option><option>14:00</option>
				<option>14:30</option><option>15:00</option><option>15:30</option><option>16:00</option>
				<option>16:30</option><option>17:00</option><option>17:30</option><option>18:00</option>
				<option>18:30</option><option>19:00</option><option>19:30</option>
				</select>
			</TD></TR>
			</TABLE>

		</TD>

		<TD class="tabmovtex" width="200" colspan=2>
			Techo presupuestal:
		</TD>
	</TR>
	<TR valign="middle">
		<TD class="tabmovtex">

			<!-- Campos Techo presupuestal (Radio) -->
			<input type="radio" name="valtecho" value="techoSi"onClick="valTecho(_F);">&nbsp;S&iacute;<br>
			<input type="radio" name="valtecho" value="techoNo" checked onclick="valTecho(_T);">No

		</TD>
		<TD class="tabmovtex" align="left">

			<!-- Campo techo presupuestal (Text) -->
			<input id="techopre" name="techopre" size=19 class="tabmovtex" value= "" maxlength=19 onfocus="if(isDis())blur();">
			<input type=hidden name="lck" value="unlocked">

		</TD>
	</TR>

	<TR><TD colspan=3>&nbsp;<BR></TD></TR>


	</TABLE>

	<BR>
	<A href="javascript:agregar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25290.gif" alt="Agregar"></A>
	<BR>


	<!-- Lista de cuentas ------------------------------------------------------------->

	<TABLE width=650 border=0 cellspacing=1>
	   <TR>
		<TD class="tabmovtex">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total de cuentas: <%=cuentas.size()%>
		</TD>
	   </TR>
	</TABLE>

	<TABLE width=600 class="textabdatcla" border=0 cellspacing=1>
	<TR><TD colspan=6 class="tittabdat" align=center>Estructura</TD></TR>
	<TR>
		<TD class="tittabdat" align=center>Seleccione</TD>
		<TD class="tittabdat" align=center>Cuenta</TD>
		<TD class="tittabdat" align=center>L&iacute;nea de cr&eacute;dito</TD>
		<TD class="tittabdat" align=center>Nivel</TD>
		<TD class="tittabdat" align=center>Horario</TD>
		<TD class="tittabdat" align=center>Techo</TD>
	</TR>

	<!-- Esta secci�n se repite para cada cuenta -->
	<%
			String espacio;
			String estilo;
			BC_CuentaEstructura cta;

			estilo = "textabdatobs";
			for(a=0;a<cuentas.size();a++)
			{
		estilo = (estilo.equals("textabdatobs"))?"textabdatcla":"textabdatobs";
		cta = (BC_CuentaEstructura)cuentas.get(a);
		espacio = "";
		for(b=0;b<cta.nivel()-1;b++) espacio += "<IMG src=\"/gifs/EnlaceMig/gbo25625.gif\">";
		espacio += (((new mx.altec.enlace.servlets.bcMtoEstructura()).esHoja(cta,cuentas))?
		"<IMG src=\"/gifs/EnlaceMig/hoja.gif\">":
		"<IMG src=\"/gifs/EnlaceMig/folder.gif\">");
		espacio += "&nbsp;";
	%>
	<TR>
		<TD class="<%= estilo %>" align=center><INPUT type=CheckBox name="CtaSel" value="<%=cta.getNumCta()%>" onClick="selUno(<%= a %>)"></TD>
		<TD class="<%= estilo %>"><%= espacio %><SCRIPT>imprimeUnaLinea('<%= cta.getNumCta() + " " + cta.getDescripcion() %>');</SCRIPT></TD>
		<TD class="<%= estilo %>" align=center><%= cta.getLineaCred() %></TD>
		<TD class="<%= estilo %>" align=center><%= cta.nivel() %></TD>

		<!-- L�neas provisionales -->
		<TD class="<%= estilo %>" align=center><%= cta.getHorarios()%></TD>
		<TD class="<%= estilo %>" align=center>$<%=((cta.getTechopre()=="")?"0":cta.getTechopre())%></TD>

	</TR>

	<% } %>

	<!----------------------------------- Botones ----------------------------------->

	</TABLE>
		<BR>
	<TABLE>
		<TR align=center>
			<TD>
				<A href="javascript:alta()"><IMG border=0 src="/gifs/EnlaceMig/gbo25480.gif" alt="Alta"></A><A href="javascript:borrar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25540.gif" alt="Borrar"></A><A href="javascript:modificar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25510.gif" alt="Modificar"></A><A href="javascript:eliminar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25515.gif" alt="Eliminar registro"></A><A href="javascript:imprimir()"><IMG border=0 src="/gifs/EnlaceMig/gbo25240.gif" alt="Imprimir"></A>
			</TD>
		</TR>
	</TABLE>

	</DIV>

	<!-- Las siguientes dos l�neas son importantes, no borrar -->
	<INPUT type=Hidden name="CtaSel" value="X">
	<INPUT type=Hidden name="CtaSel" value="XX">
	<INPUT type=Hidden name="Hora1"  value ="">
	<INPUT type=Hidden name ="Hora2" value="">
	<INPUT type=Hidden name="cuenta" value="">



	</FORM>

	<SCRIPT>valTecho(_T);</SCRIPT>

	</BODY>

	</HTML>

	<% if(!parMensaje.equals("")) out.println("<SCRIPT>aviso('" + parMensaje + "');</SCRIPT>"); %>
