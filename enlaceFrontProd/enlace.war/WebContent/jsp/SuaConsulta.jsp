<HTML>
<HEAD><TITLE>SUA Consulta</TITLE>
  <script language="JavaScript"    SRC="/EnlaceMig/ValidaFormas.js"></script>
  <script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
  <script language="JavaScript"    SRC="/EnlaceMig/cuadroDialogo.js"></script>
  <script language="javascript"    src="/EnlaceMig/scrImpresion.js"></script>
  <script language="javaScript">

/*
 6 N Requerido Numerico
 5 T Requerido Texto
 3 S Reqerido Seleccion
 2 n Numerico
 1 t Texto
 0 i Ignorado

*/

  errores=new Array(" El Registro Patronal",
                   " El Folio");

  evalua = "TN";


function validaForma(par1,par2,par3)
{
	textToUpper(par1);
	return ValidaTodo(par1,par2,par3);
}

function generaComprobante(f)
{
	param1="ventana=3";
	param2="origen=Consulta";
	param3="strRegPatronal=" + f.hidRegPatronal.value;
	param4="strFolio="+f.hidFolio.value;
	param5="comp_contrato="+f.comp_contrato.value;
    param6="comp_usuario="+f.comp_usuario.value;
    param7="comp_perfil="+f.comp_perfil.value;

	var web_application=f.web_application.value;

	// vswf:meg cambio de NASApp por Enlace 08122008
	
	vc=window.open("/Enlace/"+web_application+"/SuaConsulta?" + param1+ "&" + param2 + "&" + param3 + "&" + param4 + "&" + param5 +  "&" + param6 +  "&" +param7, 'comprobante' ,'width=650,height=500,scrollbars=1,toolbar=0');
	vc.focus();
}

/*************************************************************************************/
function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
/*********************************************************************************/

<%= request.getAttribute("newMenu") %>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMIg/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL ---1 -->
	<%= request.getAttribute("MenuPrincipal")%>
   </td>
  </tr>
</table>

<%= request.getAttribute("Encabezado") %>

<%
String RegPatronal = "";
String Folio = "";
String Referencia = "";
String RFC = "";
String NomRazSoc = "";
String NumTrabaja = "";
String NumTrabajaCred = "";
String ImssPeriodo = "";
String ImssYear = "";
String SarBimestre = "";
String SarYear = "";
String FechaPago = "";
String ImssImpSeg = "";
String ImssImpAct = "";
String ImssImpRec = "";
String AfoImpRCV = "";
String AfoImpAct = "";
String AfoImpRec = "";
String AfoSubTotal = "";
String AfoImpAV = "";
String AportComp ="";
String InfImpApo = "";
String InfImpAct = "";
String InfSubTotal = "";
String InfAmortCred = "";
String InfActAmortCred = "";
String InfRecAmortCred = "";
String InfMultaInfonavit="";
String InfFundemex="";
String ImssTotal = "";
String AfoTotal = "";
String InfTotal = "";
String Estatus = "";
String GranTotal = "";

	if(request.getAttribute("txtRegPatronal")!=null)
	    RegPatronal = (String)request.getAttribute("txtRegPatronal");

	if(request.getAttribute("txtFolio")!=null)
	    Folio = (String)request.getAttribute("txtFolio");

	if(request.getAttribute("txtReferencia") !=null)
	    Referencia = (String)request.getAttribute("txtReferencia");

	if(request.getAttribute("txtRFC") !=null)
	    RFC = (String)request.getAttribute("txtRFC");

	if(request.getAttribute("txtNomRazSoc") !=null)
	    NomRazSoc = (String)request.getAttribute("txtNomRazSoc");

	if(request.getAttribute("txtNumTrabaja") !=null)
		NumTrabaja = (String)request.getAttribute("txtNumTrabaja");

	if(request.getAttribute("txtNumTrabajaCred") !=null)
		NumTrabajaCred= (String)request.getAttribute("txtNumTrabajaCred");

	if(request.getAttribute("txtImssPeriodo") !=null)
		ImssPeriodo = (String)request.getAttribute("txtImssPeriodo");

	if(request.getAttribute("Vencidos") !=null)
		ImssYear = (String)request.getAttribute("Vencidos");

	if(request.getAttribute("txtSarBimestre") !=null)
		SarBimestre = (String)request.getAttribute("txtSarBimestre");

	if(request.getAttribute("txtSarYear") !=null)
		SarYear= (String)request.getAttribute("txtSarYear");

	if(request.getAttribute("txtFechaPago") !=null)
		FechaPago = (String)request.getAttribute("txtFechaPago");

	if(request.getAttribute("txtImssImpSeg") !=null)
		ImssImpSeg = (String)request.getAttribute("txtImssImpSeg");

	if(request.getAttribute("txtImssImpAct") !=null)
		ImssImpAct = (String)request.getAttribute("txtImssImpAct");

	if(request.getAttribute("txtImssImpRec") !=null)
		ImssImpRec = (String)request.getAttribute("txtImssImpRec");

	if(request.getAttribute("txtAfoImpRCV") !=null)
		AfoImpRCV = (String)request.getAttribute("txtAfoImpRCV");

	if(request.getAttribute("txtAfoImpAct") !=null)
		AfoImpAct = (String)request.getAttribute("txtAfoImpAct");

	if(request.getAttribute("txtAfoImpRec") !=null)
		AfoImpRec = (String)request.getAttribute("txtAfoImpRec");

	if(request.getAttribute("txtAfoSubTotal") !=null)
		AfoSubTotal = (String)request.getAttribute("txtAfoSubTotal");

	if(request.getAttribute("txtAfoImpAV") !=null)
		AfoImpAV= (String)request.getAttribute("txtAfoImpAV");

	if(request.getAttribute("txtAportacionesComp") !=null)
		AportComp= (String)request.getAttribute("txtAportacionesComp");

	if(request.getAttribute("txtInfImpApo") !=null)
		InfImpApo = (String)request.getAttribute("txtInfImpApo");

	if(request.getAttribute("txtInfImpAct") !=null)
		InfImpAct= (String)request.getAttribute("txtInfImpAct");

	if(request.getAttribute("txtInfSubTotal") !=null)
		InfSubTotal = (String)request.getAttribute("txtInfSubTotal");

	if(request.getAttribute("txtInfAmortCred") !=null)
		InfAmortCred = (String)request.getAttribute("txtInfAmortCred");

	if(request.getAttribute("txtInfActAmortCred") !=null)
		InfActAmortCred = (String)request.getAttribute("txtInfActAmortCred");

	if(request.getAttribute("txtInfRecAmortCred") !=null)
		InfRecAmortCred = (String)request.getAttribute("txtInfRecAmortCred");
/*****************************************
 * Bloque Nuevos campos, para version W3xx
 * ***************************************/
      	if(request.getAttribute("txtMultasInfonavit") !=null)
		InfMultaInfonavit = (String)request.getAttribute("txtMultasInfonavit");

      	if(request.getAttribute("txtDonativofundemex") !=null)
		InfFundemex = (String)request.getAttribute("txtDonativofundemex");
/*****************************************
 * Fin Bloque Nuevos campos
 * ***************************************/

	if(request.getAttribute("txtImssTotal") !=null)
		ImssTotal = (String)request.getAttribute("txtImssTotal");

	if(request.getAttribute("txtAfoTotal") !=null)
		AfoTotal = (String)request.getAttribute("txtAfoTotal");

	if(request.getAttribute("txtInfTotal") !=null)
		InfTotal = (String)request.getAttribute("txtInfTotal");

	if(request.getAttribute("txtEstatus") !=null)
		Estatus = (String)request.getAttribute("txtEstatus");

	if(request.getAttribute("txtGranTotal") !=null)
		GranTotal = (String)request.getAttribute("txtGranTotal");
%>

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <FORM  NAME="frmConsulta" method=post  action="SuaConsulta?origen=Consulta">
    <tr>
      <td align="center">
        <table border="0" cellspacing="2" cellpadding="3">
          <tr>
            <td class="tittabdat" colspan="2"> Capture el registro patronal y el folio</td>
          </tr>
          <tr align="center">
            <td class="textabdatcla" valign="top" colspan="2">
              <table border="0" cellspacing="0" cellpadding="0">
                <tr valign="top">
                  <td>
                    <table border="0" cellspacing="4" cellpadding="0">
                      <tr>
                        <td class="tabmovtex" width="180" nowrap> Registro patronal: </td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap>
                          <INPUT TYPE="text" size="22" class="tabmovtex" MAXLENGTH="11" NAME="strRegPatronal" VALUE="<%=RegPatronal.trim()%>" >
                        </td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap align="Rigth">Folio:</td>
                      </tr>
                      <tr valign="middle">
                        <td class="tabmovtex" nowrap>
                          <INPUT TYPE="text" size="22" class="tabmovtex" MAXLENGTH="15" NAME="strFolio" VALUE="<%=Folio.trim()%>" >
                        </td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap>Referencia banco:</td>
                      </tr>
                      <tr valign="middle">
                        <td class="tabmovtex" nowrap>
                          <INPUT TYPE="text" size="28" class="tabmovtex" MAXLENGTH="20" NAME="txtReferencia" VALUE="<%=Referencia.trim()%>" onFocus="blur();" onChange="this.value='<%=Referencia.trim()%>'">
                        </td>
                      </tr>
                      <tr valign="middle">
                        <td class="tabmovtex" nowrap>R.F.C.:</td>
                      </tr>
                      <tr valign="middle">
                        <td class="tabmovtex" nowrap>
                          <INPUT TYPE="text" size="22" class="tabmovtex" MAXLENGTH="13" NAME="txtRFC" VALUE="<%=RFC.trim()%>" onFocus="blur();" onChange="this.value='<%=RFC.trim()%>'">
                        </td>
                      </tr>
                      <tr valign="middle">
                        <td class="tabmovtex" nowrap>Nombre o raz&oacute;n social:</td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap>
                          <INPUT TYPE="text" size="40" class="tabmovtex" MAXLENGTH="40" NAME="txtNomRazSoc" VALUE="<%=NomRazSoc.trim()%>" onFocus="blur();" onChange="this.value='<%=NomRazSoc.trim()%>'">
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td>
                    <table border="0" cellspacing="4" cellpadding="0">
                      <tr>
                        <td class="tabmovtex" nowrap colspan="2">No. de trabajadores:</td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap colspan="2">
                          <INPUT TYPE="text" size="22" class="tabmovtex" MAXLENGTH="5" NAME="txtNumTrabaja" VALUE="<%=NumTrabaja.trim()%>" onFocus="blur();" onChange="this.value='<%=NumTrabaja.trim()%>'">
                        </td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap colspan="2">No. de trabajadores con cr&eacute;dito:</td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap colspan="2">
                          <INPUT TYPE="text" size="22" class="tabmovtex" MAXLENGTH="5" NAME="txtNumTrabajaCred" VALUE="<%=NumTrabajaCred.trim()%>" onFocus="blur();" onChange="this.value='<%=NumTrabajaCred.trim()%>'">
                        </td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap valign="middle" width="90">IMSS periodo:</td>
                        <td class="tabmovtex" nowrap valign="middle" width="90">A&ntilde;o:</td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap valign="middle">
                          <INPUT TYPE="text" size="12" class="tabmovtex" MAXLENGTH="2" NAME="txtImssPeriodo" VALUE="<%=ImssPeriodo.trim()%>" onFocus="blur();" onChange="this.value='<%=ImssPeriodo.trim()%>'">
                        </td>
                        <td class="tabmovtex" nowrap valign="middle">
                          <INPUT TYPE="text" size="12" class="tabmovtex" MAXLENGTH="4" NAME="txtImssYear" VALUE="<%=ImssYear.trim()%>" onFocus="blur();" onChange="this.value='<%=ImssYear.trim()%>'">
                        </td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap valign="middle">Periodo:</td>
                        <td class="tabmovtex" nowrap valign="middle">A&ntilde;o:</td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap valign="middle">
                          <INPUT TYPE="text" size="12" class="tabmovtex" MAXLENGTH="2" NAME="txtSarBimestre" VALUE="<%=SarBimestre.trim()%>" onFocus="blur();" onChange="this.value='<%=SarBimestre.trim()%>'">
                        </td>
                        <td class="tabmovtex" nowrap valign="middle">
                          <INPUT TYPE="text" size="12" class="tabmovtex" MAXLENGTH="4" NAME="txtSarYear" VALUE="<%=SarYear.trim()%>" onFocus="blur();" onChange="this.value='<%=SarYear.trim()%>'">
                        </td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap valign="middle" colspan="2">Fecha de pago:</td>
                      </tr>
                      <tr>
                        <td class="tabmovtex" nowrap valign="middle" colspan="2">
                          <INPUT TYPE="text" size="15" class="tabmovtex" MAXLENGTH="10" NAME="txtFechaPago" VALUE="<%=FechaPago.trim()%>" onFocus="blur();" onChange="this.value='<%=FechaPago.trim()%>'">
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <p>&nbsp;</p>
        <table width="670" border="0" cellspacing="2" cellpadding="3">
          <tr>
            <td width="210" align="left" valign="top" class="tittabdat">Para abono en cuenta del IMSS </td>
            <td width="15" align="left">&nbsp;</td>
            <td width="210" align="left" valign="top" class="tittabdat">Para abono en cuenta del AFORE </td>
            <td width="15">&nbsp;</td>
            <td width="210" align="left" valign="top" class="tittabdat">Para abono en cuenta del INFONAVIT</td>
          </tr>
          <tr>
            <td width="210" align="left" valign="top" class="textabdatcla">
              <table border="0" cellspacing="4" cellpadding="0">
                <tr>
                  <td class="tabmovtex" nowrap>Cuota 4 seguros:</td>
                </tr>
                <tr>
                  <td class="tabmovtex" width="158">
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtImssImpSeg" VALUE="<%=ImssImpSeg.trim()%>" onFocus="blur();" onChange="this.value='<%=ImssImpSeg.trim()%>'">
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex" nowrap>Actualizaci&oacute;n:</td>
                </tr>
                <tr valign="middle">
                  <td class="tabmovtex">
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtImssImpAct" VALUE="<%=ImssImpAct.trim()%>" onFocus="blur();" onChange="this.value='<%=ImssImpAct.trim()%>'">
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex" nowrap>Recargos Moratorios:</td>
                </tr>
                <tr valign="middle">
                  <td class="tabmovtex">
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtImssImpRec" VALUE="<%=ImssImpRec.trim()%>" onFocus="blur();" onChange="this.value='<%=ImssImpRec.trim()%>'">
                  </td>
                </tr>
              </table>
            </td>
            <td width="15" align="left">&nbsp; </td>
            <td width="210" align="left" valign="top" class="textabdatcla">
              <table border="0" cellspacing="4" cellpadding="0">
                <tr>
                  <td class="tabmovtex" nowrap>RCV:</td>
                </tr>
                <tr>
                  <td class="tabmovtex">
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtAfoImpRCV" VALUE="<%=AfoImpRCV.trim()%>" onFocus="blur();" onChange="this.value='<%=AfoImpRCV.trim()%>'">
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex" nowrap>Actualizaci&oacute;n:</td>
                </tr>
                <tr valign="middle">
                  <td class="tabmovtex">
                    <INPUT TYPE="text" size="30" class="tabmovtex" NAME="txtAfoImpAct" VALUE="<%=AfoImpAct.trim()%>" onFocus="blur();" onChange="this.value='<%=AfoImpAct.trim()%>'">
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex" nowrap>Recargos Moratorios:</td>
                </tr>
                <tr valign="middle">
                  <td class="tabmovtex">
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtAfoImpRec" VALUE="<%=AfoImpRec.trim()%>" onFocus="blur();" onChange="this.value='<%=AfoImpRec.trim()%>'">
                  </td>
                </tr>
                <tr valign="middle">
                  <td class="tabmovtex" nowrap>Subtotal:</td>
                </tr>
                <tr valign="middle">
                  <td class="tabmovtex">
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtAfoSubTotal" VALUE="<%=AfoSubTotal.trim()%>" onFocus="blur();" onChange="this.value='<%=AfoSubTotal.trim()%>'">
                  </td>
                </tr>
                <tr valign="middle">
                  <td class="tabmovtex" nowrap>Aportaci&oacute;n voluntaria:
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex">
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtAfoImpAV" VALUE="<%=AfoImpAV.trim()%>" onFocus="blur();" onChange="this.value='<%=AfoImpAV.trim()%>'">
                  </td>
                </tr>



				<tr valign="middle">
                  <td class="tabmovtex" nowrap>Aportaci&oacute;n Complementaria:
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex">
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtAportacionesComp" VALUE="<%=AportComp.trim()%>" onFocus="blur();" onChange="this.value='<%=AportComp.trim()%>'">
                  </td>
                </tr>




              </table>
            </td>
            <td width="15">&nbsp; </td>
            <td width="210" align="left" valign="top" class="textabdatcla">
              <table border="0" cellspacing="4" cellpadding="0">
                <tr>
                  <td class="tabmovtex" nowrap>Aportaci&oacute;n de vivienda:</td>
                </tr>
                <tr>
                  <td class="tabmovtex">
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtInfImpApo" VALUE="<%=InfImpApo.trim()%>" onFocus="blur();" onChange="this.value='<%=InfImpApo.trim()%>'">
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex" nowrap>Aportaci&oacute;n de vivienda amortizaci&oacute;n de cr&eacute;dito:</td>
                </tr>
                <tr valign="middle">
                  <td class="tabmovtex">
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtInfImpAct" VALUE="<%=InfImpAct.trim()%>" onFocus="blur();" onChange="this.value='<%=InfImpAct.trim()%>'">
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex" nowrap>Subtotal:</td>
                </tr>
                <tr valign="middle">
                  <td class="tabmovtex">
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtInfSubTotal" VALUE="<%=InfSubTotal.trim()%>" onFocus="blur();" onChange="this.value='<%=InfSubTotal.trim()%>'">
                  </td>
                </tr>
                <tr valign="middle">
                  <td class="tabmovtex" nowrap>Amortizaci&oacute;n de cr&eacute;dito:</td>
                </tr>
                <tr valign="middle">
                  <td class="tabmovtex">
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtInfAmortCred" VALUE="<%=InfAmortCred.trim()%>" onFocus="blur();" onChange="this.value='<%=InfAmortCred.trim()%>'">
                  </td>
                </tr>
                <tr valign="middle">
                  <td class="tabmovtex" nowrap>Actualizaci&oacute;n amortizaci&oacute;n de cr&eacute;dito:</td>
                </tr>
                <tr valign="middle">
                  <td class="tabmovtex">
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtInfActAmortCred" VALUE="<%=InfActAmortCred.trim()%>" onFocus="blur();" onChange="this.value='<%=InfActAmortCred.trim()%>'">
                  </td>
                </tr>
                <tr valign="middle">
                  <td class="tabmovtex" nowrap>Recargos amortizaci&oacute;n de cr&eacute;dito:</td>
                </tr>
                <tr>
                  <td class="tabmovtex">
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtInfRecAmortCred" VALUE="<%=InfRecAmortCred.trim()%>" onFocus="blur();" onChange="this.value='<%=InfRecAmortCred.trim()%>'">
                  </td>
                </tr>


								<tr valign="middle">
                  <td class="tabmovtex" nowrap>Multa Infonavit:</td>
                </tr>
                <tr>
                  <td class="tabmovtex">
		  <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtMultasInfonavit" VALUE="<%=InfMultaInfonavit.trim()%>" onFocus="blur();" onChange="this.value='<%=InfMultaInfonavit.trim()%>'">
                  </td>
                </tr>


								<tr valign="middle">
                  <td class="tabmovtex" nowrap>Donativo Fundemex:</td>
                </tr>
                <tr>
                  <td class="tabmovtex">
                  <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtDonativofundemex" VALUE="<%=InfFundemex.trim()%>" onFocus="blur();" onChange="this.value='<%=InfFundemex.trim()%>'">
                  </td>
                </tr>




              </table>
            </td>
          </tr>
          <tr>
            <td width="210" align="left" valign="top" class="tittabdat">
              <table border="0" cellspacing="4" cellpadding="0">
                <tr>
                  <td class="tittabdat" nowrap>Total:</td>
                </tr>
                <tr>
                  <td class="tabmovtex">
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtImssTotal" VALUE="<%=ImssTotal.trim()%>" onFocus="blur();" onChange="this.value='<%=ImssTotal.trim()%>'">
                  </td>
                </tr>
              </table>
            </td>
            <td width="15" align="left">&nbsp;</td>
            <td width="210" align="left" valign="top" class="tittabdat">
              <table border="0" cellspacing="4" cellpadding="0">
                <tr>
                  <td class="tittabdat" nowrap>Total:</td>
                </tr>
                <tr>
                  <td class="tabmovtex">
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtAfoTotal" VALUE="<%=AfoTotal.trim()%>" onFocus="blur();" onChange="this.value='<%=AfoTotal.trim()%>'">
                  </td>
                </tr>
              </table>
            </td>
            <td width="15">&nbsp;</td>
            <td width="210" align="left" valign="top" class="tittabdat">
              <table border="0" cellspacing="4" cellpadding="0">
                <tr>
                  <td class="tittabdat" nowrap>Total:</td>
                </tr>
                <tr>
                  <td class="tabmovtex">
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtInfTotal" VALUE="<%=InfTotal.trim()%>" onFocus="blur();" onChange="this.value='<%=InfTotal.trim()%>'">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td width="210" align="left" valign="top" class="textabdatcla">
              <table border="0" cellspacing="4" cellpadding="0">
                <tr>
                  <td class="tabmovtex" nowrap>Estatus:</td>
                </tr>
                <tr>
                  <td class="tabmovtex">
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtEstatus" VALUE="<%=Estatus.trim()%>" onFocus="blur();" onChange="this.value='<%=Estatus.trim()%>'">
                  </td>
                </tr>
              </table>
            </td>
            <td width="15" align="left">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="15">&nbsp;</td>
            <td width="210" align="left" valign="top" class="textabdatcla">
              <table border="0" cellspacing="4" cellpadding="0">
                <tr>
                  <td class="tabmovtex" nowrap>Total a pagar:</td>
                </tr>
                <tr>
                  <td class="tabmovtex" nowrap>
                    <INPUT TYPE="text" size="30" class="tabmovtex" MAXLENGTH="14" NAME="txtGranTotal" VALUE="<%=GranTotal.trim()%>" onFocus="blur();" onChange="this.value='<%=GranTotal.trim()%>'">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <br>
        <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td>
            <%
				if (request.getAttribute("strBoton")!= null)
				  out.println(request.getAttribute("strBoton"));
            %>
            </td>
          </tr>
        </table>
        <br>
      </td>
    </tr>
  <INPUT TYPE="hidden" NAME=ventana VALUE=2>
  <%
   String txtReg="";
   String txtFol="";

   if(request.getAttribute("txtRegPatronal")!= null)
     txtReg=(String)request.getAttribute("txtRegPatronal");
   if(request.getAttribute("txtFolio")!= null)
     txtFol=(String)request.getAttribute("txtFolio");
  %>
  <input type=hidden name="hidRegPatronal" value="<%=txtReg%>">
  <input type=hidden name="hidFolio" value="<%=txtFol%>">


   <input type="HIDDEN" name="web_application" value =
<%
  String web_application = "";
  if(session.getAttribute("web_application")!=null)
	web_application = (String)session.getAttribute("web_application");
  else if (request.getAttribute("web_application")!= null)
	web_application= (String)request.getAttribute("web_application");
  out.println(web_application);
  %>>

   <input type="HIDDEN" name="comp_contrato" value =
<%
  String comp_contrato= "";
  if(session.getAttribute("comp_contrato")!=null)
	comp_contrato = (String)session.getAttribute("comp_contrato");
  else if(request.getAttribute("comp_contrato")!=null)
	comp_contrato = (String)request.getAttribute("comp_contrato");
  out.println(comp_contrato);
  %>>

 <input type="HIDDEN" name="comp_usuario" value =
<%
  String comp_usuario = "";
  if(session.getAttribute("comp_usuario")!=null)
	comp_usuario = (String)session.getAttribute("comp_usuario");
  else if(request.getAttribute("comp_usuario")!=null)
	comp_usuario = (String)request.getAttribute("comp_usuario");
  out.println(comp_usuario);
  %>>

 <input type="HIDDEN" name="comp_perfil" value =
<%
  String comp_perfil = "";
  if(session.getAttribute("comp_perfil")!=null)
	comp_perfil = (String)session.getAttribute("comp_perfil");
  else if(request.getAttribute("comp_perfil")!=null)
	comp_perfil = (String)request.getAttribute("comp_perfil");
  out.println(comp_perfil);
  %>>

</form>
</table>
</BODY>
</HTML>
