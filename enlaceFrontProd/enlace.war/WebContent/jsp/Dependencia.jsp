<html>
<head>
<title>Pago SAR</TITLE>

<meta name="Generador" content="EditPlus">
<meta name="Autor" content="Juan Pablo Pe�a">
<meta name="Descripcion" content="Se encarga de presentar el resultado de la consulta de dependencias, as� como de enviar los datos necesarios para realizar la transacci�n de pago SAR.">
<meta name="Version" content="1.0">
<%@page import="mx.altec.enlace.bo.BaseResource;"%>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javaScript" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript">

<%
  String fechaHoy="";
  if(request.getAttribute("FechaHoy")!= null)
    fechaHoy=(String)request.getAttribute("FechaHoy");
%>

//Arreglos de fechas
<%
  if(request.getAttribute("VarFechaHoy")!= null)
    out.print(request.getAttribute("VarFechaHoy"));
%>
//Dias inhabiles
diasInhabiles='<% if(request.getAttribute("DiasInhabiles")!= null) out.print(request.getAttribute("DiasInhabiles")); %>';

var respuesta=0;
var Indice=0; //Indice de Calendario
function js_calendario(ind)
 {
    var m=new Date()

	Indice=ind;
    n=m.getMonth();
    msg=window.open("/EnlaceMig/CalendarioSAR.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
    msg.focus();
 }


function Actualiza()
 {
	if(Indice==0)
	{
		document.conDependencia.FechaProg.value=Fecha[Indice];
		document.conDependencia.txtFecha.value=Fecha[Indice];
	}
 }

function Transferir()
{

  if (document.conDependencia.textdestino.value!= "")
  {
  <%
	BaseResource ses = (BaseResource) request.getSession ().getAttribute ("session");
  	if (ses.getToken().getStatus() == 1) {%>
  		respuesta = 1;
  		continua();
	<%} else {%>
		cuadroDialogo("Desea realizar el pago?.",2);
	<%}%>
  }
  else
		cuadroDialogo("La cuenta de cargo es un dato obligatorio.", 3);
}

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}

function actualizacuenta()
{
  document.conDependencia.cuenta.value=ctaselec+"|"+ctatipre+"|"+ctadescr+"|";
  document.conDependencia.textdestino.value=ctaselec+" "+ctadescr;
}


function continua()
{
  if(respuesta==1)
  {
		window.document.conDependencia.action = "ConsultaUsuarios";
		window.document.conDependencia.submit();
  }
}


/**********************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

<%
	if(	session.getAttribute("newMenu") !=null)
		out.println( session.getAttribute("newMenu"));
	else
		if (request.getAttribute("newMenu")!= null)
			out.println(request.getAttribute("newMenu"));
%>

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/***********************************************************/


</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

<style type="text/css">
<!--
.Estilo1 {
	font-family: "Times New Roman", Times, serif;
	font-size: small;
}
-->
</style>
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

<table border="0" cellpadding="0" cellspacing="0" width="571">

  <tr valign="top">
   <td width="*">
    <!-- MENU PRINCIPAL -->
	<%
			if (request.getAttribute("MenuPrincipal")!= null)
				out.println(request.getAttribute("MenuPrincipal"));
	%>
   </td>
  </tr>
</table>


<%
			if (request.getAttribute("Encabezado")!= null)
				out.println(request.getAttribute("Encabezado"));
	%>
<div align="left"><font color="#000000" size="4"> </font><font size="3" face="Times New Roman, Times, serif">
  </font><font color="#000000" size="4"> </font></div>
<FORM  NAME="conDependencia" method="post" onSubmit="return fncEnviarDatosPago()">
  <font size="3" face="Times New Roman, Times, serif"> </font>
  <table width="702" border="0" cellspacing="0">
    <!--DWLayoutTable-->
    <tr>
      <td rowspan="2" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
      <td height="14"></td>
      <td></td>
      <td></td>
      <td></td>
      <td nowrap></td>
    </tr>
    <!-- aqui termina la tabla del  TLC-->
    <tr>
      <td rowspan="2" nowrap><font size="3" face="Times New Roman, Times, serif">Cuenta
        de cargo</font></td>
      <td colspan="2" rowspan="2"> <input type="text" name=textdestino  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value=
		   <%
			  if(request.getAttribute("textdestino")!= null)
				 out.print(request.getAttribute("textdestino"));
			  else
				 out.println("");
		   %>
		  > <A HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" border=0 align=absmiddle></A>
      </td>
      <td colspan="2" rowspan="2" nowrap><font size="3" face="Times New Roman, Times, serif">Folio
        de Mancomunidad</font></td>
      <td rowspan="2" align="right"> <input type="text" class="texdrocon" maxlength=16 size=16 name="refmancomunidad">
      </td>
      <td height="10" nowrap> </td>
    </tr>
    <tr>
      <td height="24">&nbsp;</td>
      <td nowrap></td>
    </tr>
    <tr>
      <td height="40">&nbsp;</td>
      <td>Fecha de Aplicaci&oacute;n</td>
      <td colspan="2"><font size="3" face="Times New Roman, Times, serif">
        <input name="FechaProg" type="text" id="FechaProg" OnFocus = "blur();" value='<%=fechaHoy%>' size="12">
        </font> <a href="javascript:js_calendario(0);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></a></td>
      <td colspan="3">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="40">&nbsp;</td>
      <td><span class="Estilo1"><font size="3" face="Times New Roman, Times, serif">L&iacute;nea
        de Captura</font></span></td>
      <td colspan="5" align="left"><span class="Estilo1"><font size="3" face="Times New Roman, Times, serif"><span class="Estilo1"><font size="3" face="Times New Roman, Times, serif"><span class="Estilo1"><font size="3" face="Times New Roman, Times, serif"><span class="Estilo1"><font size="3" face="Times New Roman, Times, serif"><span class="Estilo1">
        <font size="3" face="Times New Roman, Times, serif"><span class="Estilo1"><font size="3" face="Times New Roman, Times, serif"><span class="Estilo1"><font size="3" face="Times New Roman, Times, serif"><span class="Estilo1"><font size="3" face="Times New Roman, Times, serif"><span class="Estilo1">
        <input name="txtLineaCaptura" type="text" value='<%= request.getAttribute("LineCaptureValidate")%>' size="110" onFocus="blur();">
        </span></font></span></font></span></font></span></font> </span></font></span></font>
        </span></font> </span></font></span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="21">&nbsp;</td>
      <td>&nbsp;</td>
      <td width="181">&nbsp;</td>
      <td width="233">&nbsp;</td>
      <td colspan="3">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="40">&nbsp;</td>
      <td><span class="Estilo1"><strong><span class="Estilo1"><font size="3" face="Times New Roman, Times, serif">Datos
        del Pago</font></span></strong></span></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td colspan="3">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="24">&nbsp;</td>
      <td><font size="3" face="Times New Roman, Times, serif">Fecha de pago</font></td>
      <td colspan="2"><input name="txtFecha" type="text" id="txtFecha2" value='<%=fechaHoy%>' onFocus="blur();"></td>
      <td colspan="3">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="24">&nbsp;</td>
      <td><font size="3" face="Times New Roman, Times, serif">Retiro</font></td>
      <td colspan="2"><input name="txtRetiro" type="text" id="txtRetiro2" value='<%= request.getAttribute("Ahorro")%>' onFocus="blur();" align="right"></td>
      <td align="left">&nbsp;</td>
      <td align="left" nowrap><font size="3" face="Times New Roman, Times, serif">Bimestre
        de Pago </font></td>
      <td align="right"><font size="3" face="Times New Roman, Times, serif">&nbsp;
        </font><font size="3" face="Times New Roman, Times, serif">
        <input name="txtBimestre" type="text" id="txtBimestre5" size="10" value='<%= request.getAttribute("Bimestre")%>' onFocus="blur();">
        </font></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="24">&nbsp;</td>
      <td><font size="3" face="Times New Roman, Times, serif">Vivienda</font></td>
      <td colspan="2"><input name="txtVivienda" type="text" id="txtVivienda2" value='<%= request.getAttribute("Vivienda")%>' onFocus="blur();"  align="right"	></td>
      <td><font size="3" face="Times New Roman, Times, serif">&nbsp;</font></td>
      <td nowrap><font size="3" face="Times New Roman, Times, serif">Tipo de Pago
        <span class="Estilo1"> </span></font></td>
      <td align="right"><font size="3" face="Times New Roman, Times, serif"><span class="Estilo1">
        </span></font><font size="3" face="Times New Roman, Times, serif"><span class="Estilo1">
        <input name="txtTipoPago" type="text" id="txtTipoPago5" size="10" value='<%= request.getAttribute("TipoPago")%>' onFocus="blur();">
        </span></font></td>
      <td><span class="Estilo1"> </span></td>
    </tr>
    <tr>
      <td height="24">&nbsp;</td>
      <td><font size="3" face="Times New Roman, Times, serif">Total a Pagar</font></td>
      <td colspan="2"><input name="txtTotal" type="text" id="txtTotal2" value='<%= request.getAttribute("Total")%>' onFocus="blur();" align="right"></td>
      <td colspan="3">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="43">&nbsp;</td>
      <td>Tipo de L&iacute;nea de Captura </td>
      <td valign="top"> <input name="txtTipoLineaCap" type="text" id="txtTotal" onFocus="blur();" value='<%= request.getAttribute("TipoLineaCaptura")%>'
	  		  maxlength="2" align="right"></td>
      <td colspan="5" valign="top">&nbsp; &nbsp; <input name="txtTipoLinea" type="text" id="txtTipoLinea" onFocus="blur();" value='<%= request.getAttribute("NombreTipoLineaCaptura")%>' size="60"></td>
    </tr>
    <tr>
      <td height="21">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td colspan="3">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="21">&nbsp;</td>
      <td colspan="3"><strong><font size="3" face="Times New Roman, Times, serif">Datos
        de la Dependencia</font></strong></td>
      <td colspan="3">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="24">&nbsp;</td>
      <td><font size="3" face="Times New Roman, Times, serif">Dependencia</font></td>
      <td colspan="5" align="left"> <input name="txtDependencia" type="text" id="txtDependencia2" size="73" value='<%= request.getAttribute("Dependencia")%>' onFocus="blur();">
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="40">&nbsp;</td>
      <td><font size="3" face="Times New Roman, Times, serif">RFC Dependencia</font></td>
      <td colspan="5" align="left"> <input name="txtRFC" type="text" id="txtRFC2" onFocus="blur();" value='<%= request.getAttribute("RFC")%>' size="73">
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="24">&nbsp;</td>
      <td><font size="3" face="Times New Roman, Times, serif">Centro de Pago</font></td>
      <td colspan="5" align="left"> <input name="txtCentroPago" type="text" id="txtCentroPago2" size="73" value='<%= request.getAttribute("CentroPago")%>' onFocus="blur();">
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="24">&nbsp;</td>
      <td><font size="3" face="Times New Roman, Times, serif">Domicilio </font></td>
      <td colspan="5" align="left"> <input name="txtDomicilio" type="text" id="txtDomicilio2" size="73" value='<%= request.getAttribute("Domicilio")%>' onFocus="blur();">
      </td>
      <td>&nbsp;</td>
    </tr>
  </table>

  <p>&nbsp;</p>
  <table width="708">
  <tr>
    <!--<td align=center><br><% //if(request.getAttribute("Botones")!=null) out.print(request.getAttribute("Botones")); %></td>-->
    <td width="341">&nbsp;</td>
    <td class="textabref" colSpan="6" width="355"><a href="javascript:Transferir();"><img src='/gifs/EnlaceMig/pdr25560.gif' border=0 alt='Registrar Pago'></a></td>
   </tr>
 </table>


   <INPUT TYPE="hidden" VALUE='<%= request.getAttribute("LineCaptureValidate")%>' NAME="LineCaptureValidate">
   <INPUT TYPE="hidden" VALUE="2" NAME="Modulo">
   <input type="hidden" name="cuenta" value="">
   <input type="hidden" name="fechaHoy" value='<%=request.getAttribute("FechaHoy")%>'>
 </form>

</body>
</html>