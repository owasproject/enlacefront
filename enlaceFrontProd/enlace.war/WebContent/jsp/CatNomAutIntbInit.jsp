<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.*" %>
<%@ page import="EnlaceMig.*" %>


<html>
<head>
	<title>CUENTAS - Autorizaci&oacute;n y Cancelaci&oacute;n</TITLE>

<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript1.2" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript">

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
/********************************************************************************/
 <%
 String FechaHoy="";
 FechaHoy=(String)request.getAttribute("FechaHoy");
 String msgEmailUsr = (String)request.getAttribute("msgEmailUsr");
 String msgEmailCtr = (String)request.getAttribute("msgEmailCtr");
 if(FechaHoy==null)
    FechaHoy="";
 %>

   //Indice Calendario
  var Indice=0;


  //Arreglos de fechas
  <%
    if(request.getAttribute("VarFechaHoy")!= null)
      out.print(request.getAttribute("VarFechaHoy"));
  %>
  //Dias inhabiles
 diasInhabiles='<% if(request.getAttribute("DiasInhabiles")!= null) out.print(request.getAttribute("DiasInhabiles")); %>';


 <%

String DiaHoy="";
if(request.getAttribute("DiaHoy")!= null)
 DiaHoy=(String)request.getAttribute("DiaHoy");
 %>

			function msgEmail(msg) {
		        nuevaVentana=window.open("", "segundaPag",
		        "toolbar=no,width=400,height=120,top=380,left=340" );
		        nuevaVentana.document.write("<HTML>" +
											"<HEAD> " +
											"<TITLE>Aviso Importante</TITLE>" +
											"<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'> " +
											"</HEAD>\n");
		        nuevaVentana.document.write("<body bgcolor='white'><form>" +
											"<table border=0 width=365 class='textabdatcla' align=center cellspacing=3 cellpadding=0> " +
		         							"<tr><th height=25 class='tittabdat' colspan=2>\n");
		        nuevaVentana.document.write("Alerta!!!</th></tr></table>" +
		        							"<table border=0 width=365 class='textabdatcla' align=center cellspacing=0 cellpadding=1> ");
		        nuevaVentana.document.write("<tr><td class='tabmovtex1' align=right width=50><img src='/gifs/EnlaceMig/gic25050.gif' border=0></a></td>");
		        nuevaVentana.document.write("<td class='tabmovtex1' align=center width=300><br>" + msg + "<br><br></tr></td>");
		        nuevaVentana.document.write("<tr><td class='tabmovtex1'></td></tr></table>");
		        nuevaVentana.document.write("<table border=0 align=center cellspacing=6 cellpadding=0><tr><td align=right>");
		        nuevaVentana.document.write("<a href='javascript:window.opener.document.forms[0].action=\"CapturaDatosUsr?opcion=0\";window.opener.document.forms[0].submit();'>");
		        nuevaVentana.document.write("<img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
				nuevaVentana.document.write("<td align=left><a href='javascript:window.close();'>");
		        nuevaVentana.document.write("<img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
		        nuevaVentana.document.write("</table></form></body></html>\n");
		        nuevaVentana.document.close();
			}

function js_FrmClean()
 {
   document.ContCtasCons.reset();
 }

function verificaFechas(ind_1,ind_2)
 {
	 var forma=document.ContCtasCons;

	 if(mes1[ind_1]>mes1[ind_2] && anio1[ind_1]==anio1[ind_2])
      {
		cuadroDialogo("Error: La fecha Final no debe ser menor a la inicial.",3);
        return false;
      }
     if(mes1[ind_1]==mes1[ind_2] && anio1[ind_1]==anio1[ind_2])
      if(dia1[ind_1]>dia1[ind_2])
       {
		 cuadroDialogo("Error: La fecha Inicial debe ser menor a la final.",3);
         return false;
       }
     return true;
 }

/**
 @param s cadena
 @return boolean true, false
 @descripcion verifica se una cadena esta vacia
*/
function isEmpty(s)
{
  return ((s == null) || (trimString(s).length == 0));
}

function js_consultar()
 {
	var forma=document.ContCtasCons;
	var f3 = document.ContCtasCons.Folio_Registro;
	var f4 = document.ContCtasCons.FCuenta;


	if(isEmpty(forma.Fecha1_01.value) || isEmpty(forma.Fecha1_02.value))
	 {
		cuadroDialogo("Debe especificar un rango de fechas",3);
		return;
	 }
	if(!verificaFechas(0,1))
    return;

  if(!isInteger(f3.value))
   {
	 f3.focus();
	 cuadroDialogo("El folio debe ser num&eacute;rico",3);
	 return;
   }

	if(!especiales(f4))
	return;

   forma.Modulo.value="3";
   forma.action="CatalogoNominaAutInterb";
   forma.submit();

 }


/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/**
 @author  Alejandro Rada Vazquez
 @param num cadena numerica*/

function especiales(Txt1)
{
   var strEspecial=" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
   var cont=0;

    for(i0=0;i0<Txt1.value.length;i0++)
	 {
       Txt2 = Txt1.value.charAt(i0);
	   if( strEspecial.indexOf(Txt2)!=-1 )
		 cont++;
     }
	if(cont!=i0)
	 {
	   Txt1.focus();
	   cuadroDialogo("En la cuenta, no se permiten caracteres especiales",3);
	   return false;
	 }
   return true;
}

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/**
 @author  Alejandro Rada Vazquez
 @param num cadena numerica
 @return boolean true, false
 @descripcion verifica una expresion para determinar si es tipo entera sin signo
  utilizando expresiones regulares
*/
function isInteger(num)
{
  var TemplateI = /^\d*$/;	 /*** Formato de numero entero sin signo */
  return TemplateI.test(num); /**** Compara "num" con el formato "Template" */
}



function Actualiza()
 {
   if(Indice==0)
     document.ContCtasCons.Fecha1_01.value=Fecha[Indice];
   if(Indice==1)
     document.ContCtasCons.Fecha1_02.value=Fecha[Indice];
 }


function js_calendario(ind)
 {
    var m=new Date()

	Indice=ind;
    n=m.getMonth();
    msg=window.open("/EnlaceMig/EI_Calendario.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
    msg.focus();
 }


/********************************************************************************/
<%
	if(	session.getAttribute("strScript") !=null)
		out.println( session.getAttribute("strScript"));
	else
	 if(request.getAttribute("strScript") !=null)
		out.println( request.getAttribute("strScript") );
	 else
	  out.println("");
%>
<%
	if(	session.getAttribute("newMenu") !=null)
		out.println( session.getAttribute("newMenu"));
	else
	  if(request.getAttribute("newMenu")!=null)
		out.println(request.getAttribute("newMenu"));
	 else
	   out.println("");
%>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="//gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%
	if(	session.getAttribute("MenuPrincipal") !=null)
		out.println( session.getAttribute("MenuPrincipal"));
	else
	 if(request.getAttribute("MenuPrincipal")!=null)
		out.println(request.getAttribute("MenuPrincipal"));
	 else
	  out.println("");
	%>
   </td>
  </tr>
</table>

<%
	if(	session.getAttribute("Encabezado") !=null)
		out.println( session.getAttribute("Encabezado"));
	else
	 if(request.getAttribute("Encabezado")!=null)
		out.println(request.getAttribute("Encabezado"));
	 else
	  out.println("");
%>


 <FORM   NAME="ContCtasCons" METHOD="Post" ACTION="CatalogoNominaAutInterb">
   <table width="760" border="0" cellspacing="0" cellpadding="2">
    <tr>

	 <td align="center">
        <table width="620" border="0" cellspacing="0" cellpadding="3">
		 <tr>
            <td class="tittabdat">Capture los datos para su consulta</td>
		 </tr>

 	   <tr align="center">
       <td class="textabdatcla" valign="top">
         <table width="600" border="0" cellspacing="0" cellpadding="2">
			 <tr valign="top">
                <td align="left" width="280">
                  <table width="280" border="0" cellspacing="0" cellpadding="0">

					 <tr>
                      <td align="right" nowrap class="tabmovtex11" width="100">Usuario (opcional): </td>
						<td class="tabmovtex" nowrap width="185">
						  <SELECT NAME="usuarioOpcion" class="tabmovtex">
                           <Option value = " ">Seleccione un usuario (opcional)</Option>
							<%String strusuario="";
							if (request.getAttribute("Usuarios")!= null)
							        strusuario=(String)request.getAttribute("Usuarios");
							%>
							<%=strusuario%>
                         </SELECT>
					  </td>
				  </tr>

				<tr valign="top">
                  <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
 				</tr>
                  <td align="right" class="tabmovtex11">Consulta por:</td>
                  <td class="tabmovtex11" nowrap align="left" valign="middle">
                     <input type = "radio" name = "Registro" value = "R" checked>&nbsp;Por Fecha de Registro
                  </td>
			   </tr>


					  <tr valign="top">
                        <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>

				  <tr>
                     <td align="right" class="tabmovtex11">&nbsp;</td>
                     <td class="tabmovtex11" nowrap valign="middle" align="left">
                     <input type = "radio" name = "Registro" value ="A">&nbsp;Por Fecha de Autorizaci&oacute;n<P>
                   </td>
                   </tr>

                      <tr valign="top">
                        <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>

					  <tr valign="top">
                        <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>

                      <tr>
                        <td align="right" class="tabmovtex11">De la fecha:</td>
                        <td class="tabmovtex" nowrap valign="middle">
                          <Input type = "Text" size=10 name="Fecha1_01" value="<%=DiaHoy%>" OnFocus = "blur();"  class="tabmovtex">
                            <A href ="javascript:js_calendario(0);">
                              <img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle">
                            </a>
                        </td>
                      </tr>
                      <tr valign="top">
                        <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>


                      <tr>
                        <td align="right" class="tabmovtex11">A la fecha:</td>
                        <td class="tabmovtex" nowrap>
                          <Input type = "Text" size = 10 name = "Fecha1_02" value="<%=DiaHoy%>" OnFocus = "blur();" class="tabmovtex">
                            <A href ="javascript:js_calendario(1);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></a>
                        </td>
                      </tr>
                      <tr valign="top">
                        <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>

                      <tr valign="top">
                        <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>

                      <tr valign="top">
                        <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>


                      <tr>
                        <td align="right" class="tabmovtex11">Folio de Registro:</td>
                        <td nowrap class="tabmovtex">
                          <INPUT TYPE="Text" NAME="Folio_Registro" SIZE="15" maxlength=15 class="tabmovtex">
                        </td>
                      </tr>

                      <tr valign="top">
                        <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>

                      <tr>
                        <td align="right" class="tabmovtex11">Cuenta:</td>
                        <td nowrap class="tabmovtex">
                          <INPUT TYPE="Text" NAME="FCuenta" SIZE="20" maxlength=20 class="tabmovtex">
                        </td>
                      </tr>

       </table>
      </td>

             <td align="left" width="320">
               <table width="320" border="0" cellspacing="5" cellpadding="0">
		    <tr>
                <td align="right" class="tabmovtex11" width="89" nowrap>Estatus:</td>
                <td width="27" valign="middle" align="center">
                     <INPUT TYPE="CheckBox" value = "'A'," NAME="ChkAuto">
                </td>
                 <td class="tabmovtex11" width="212" nowrap>Ejecutada</td>
		   </tr>

		   		<tr>
                    <td align="right" nowrap>&nbsp;</td>
                    <td align="center" valign="middle">
                       <INPUT TYPE="CheckBox" value = "'P'," NAME="ChkAuto">
                	</td>
                 <td class="tabmovtex11" width="212" nowrap>Pendiente por activar</td>
                </tr>

  	            <tr>
                    <td align="right" nowrap>&nbsp;</td>
                    <td align="center" valign="middle">
                       <INPUT TYPE="CheckBox" value = "'R'," NAME="ChkRech">
           		   </td>
                   <td class="tabmovtex11" nowrap>Rechazadas</td>
                </tr>

                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'C'," NAME="ChkCanc"></td>
                        <td class="tabmovtex11" nowrap>Canceladas</td>
                      </tr>


                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'I',"  NAME="ChkPend"></td>
                        <td class="tabmovtex11" nowrap>Pendiente por autorizar</td>
                      </tr>

                      <tr valign="top">
                        <td class="tabmovtex11" nowrap colspan="3">
                          <img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5">
                        </td>
                      </tr>

                      <!--<tr>
                        <td align="right" class="tabmovtex11" nowrap>Tipo de cuenta:</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'P'," NAME="ChkPropiasMismo">
                        </td>
                        <td class="tabmovtex11" nowrap>Propia mismo banco</td>
                      </tr>

	  		       <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'T'," NAME="ChkTercerosMismo">
                        </td>
                        <td class="tabmovtex11" nowrap>De terceros mismo banco</td>
                      </tr>

                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'E'," NAME="ChkOtrosNales"></td>
                        <td class="tabmovtex11" nowrap>De otros bancos nacionales</td>
                      </tr>

                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'I'," NAME="ChkOtrosInter">
                        </td>
                        <td class="tabmovtex11" nowrap>De otros bancos internacionales</td>
                      </tr>-->

		    <tr>
                <td align="right" class="tabmovtex11" width="89" nowrap>Tipo de operaci&oacute;n:</td>
                <td width="27" valign="middle" align="center">
                     <INPUT TYPE="CheckBox" value = "'A'," NAME="Alta">
                </td>
                 <td class="tabmovtex11" width="212" nowrap>Alta</td>
		   </tr>

  	            <tr>
                    <td align="right" nowrap>&nbsp;</td>
                    <td align="center" valign="middle">
                       <INPUT TYPE="CheckBox" value = "'B'," NAME="Baja">
           		   </td>
                   <td class="tabmovtex11" nowrap>Baja</td>
                </tr>


			   </table>
			  </td>
           </tr>
		</table>
    </td>
  </tr>
</table>


        <br>
        <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td align="right" valign="middle" width="90"><A href ="javascript:js_consultar();"><img border="0" name="boton" src="/gifs/EnlaceMig/gbo25220.gif" width="90" height="22" alt="Consultar">
              </a>
            </td>
            <td align="left" valign="top" width="76"><A href ="javascript:js_FrmClean();"><img border="0" name="boton" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar">
              </a>
            </td>
          </tr>
        </table>
        <br>
 <input type=hidden name=Modulo >
        </table>
</FORM>
<%if(msgEmailCtr!=null || msgEmailUsr!=null){ %>
	<%if(msgEmailCtr!=null && msgEmailUsr==null){ %>
	    <script type="text/javascript">
			msgEmail("<%=msgEmailCtr%>");
		</script>
	<%}else if(msgEmailUsr!=null && msgEmailCtr==null){ %>
	       <script type="text/javascript">
			msgEmail("<%=msgEmailUsr%>");
		</script>
	<%}else if(msgEmailUsr!=null && msgEmailCtr!=null){ %>
	       <script type="text/javascript">
			msgEmail("Favor de actualizar Email de Usuario y de Contrato");
		</script>
	<%}%>
<%}%>
</body>
</html>