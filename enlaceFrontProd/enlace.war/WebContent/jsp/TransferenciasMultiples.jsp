<%@ page language="java" %>
<%@ page contentType="text/html;charset=ISO8859_1"%>
<html>
<head>
<title>Banca Virtual</title>
<script language = "JavaScript" type="text/javascript" SRC="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javascript" type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<SCRIPT LANGUAGE="JavaScript1.2" type="text/javascript" SRC="/EnlaceMig/list.js"></SCRIPT>
<script language="JavaScript" type="text/javascript" src="/EnlaceMig/ConMov.js"></script>
<script type="text/javascript" src="/EnlaceMig/util.js"></script>
<script type="text/javascript" src="/EnlaceMig/pm_fp.js"></script>

<%--
03/08/2002 Se cambi� el despliegue la cantidad de Total Cargo.
04/08/2002 Se cambi� el despliegue la cantidad de Total Abono.
06/09/2002 Se corrigi� la funci�n validarimportes
09/09/2002 Funci�n revisaNuevaCuenta
10/09/2002 Se termin� revisaNuevaCuenta. Se revis� el formato de los n�meros.
12/09/2002 Se corrigieron las funciones validarimpcargo, validarimpabono, sumarimpa, sumarimpc, restarimpa, restarimpc.
13/09/2002 Se revis� el redondeo
15/10/2002 Se corrigi� la excepci�n de que no se revisaba si se trataba de insertar cuenta de abono repetida
   cuando se trataba de insertar una cuenta de cargo no repetida al mismo tiempo.
16/10/2002 Se termin� de verificar SelAgrCuenta por el caso anterior.
--%>

<Script language = "JavaScript" type="text/javascript">
<!--
 function VentanaAyuda(ventana){
    hlp=window.open("/EnlaceMig/ayuda.html#" + ventana ,"hlp","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
    hlp.focus();
}
//-->
</Script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<!--
<meta http-equiv="Expires" content="1">

<meta http-equiv="pragma" content="no-cache">

  <%
	response.setHeader		( "Cache-Control"	, "no-store"	);
	response.setHeader		( "Pragma"			, "no-cache"	);
	response.setDateHeader	( "Expires"			, 0				);
  %>

-->

<script language="JavaScript" type="text/javascript">



function Formatea_Importe(importe)
{
   decenas="";
   centenas="";
   millon="";
   millares="";
   importe_final=importe;
   var posiciones=7;

   posi_importe=importe.indexOf(".");
   num_decimales=importe.substring(posi_importe + 1,importe.length);

   if (posi_importe==-1)
     importe_final= importe + ".00";

   if (posi_importe==4)
   {
     centenas=importe.substring(1, posiciones);
     millares=importe.substring(0,1);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==5)
   {
     centenas=importe.substring(2, posiciones + 1);
     millares=importe.substring(0,2);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==6)
   {
     centenas=importe.substring(3, posiciones + 2);
     millares=importe.substring(0,3);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==7)
   {
     centenas=importe.substring(4, posiciones + 3);
     millares=importe.substring(1,4);
     millon=importe.substring(0,1);
     importe_final= millon + "," + millares + "," + centenas;
   }
   if (posi_importe==8)
   {
     centenas=importe.substring(5, posiciones + 4);
     millares=importe.substring(2,5);
     millon=importe.substring(0,2);
     importe_final= millon + "," + millares + "," + centenas;
   }
   if (posi_importe==9)
   {
     centenas=importe.substring(6, posiciones + 5);
     millares=importe.substring(3,6);
     millon=importe.substring(0,3);
     importe_final= millon + "," + millares + "," + centenas;
   }
   return importe_final;
}


function validaImporte(compimporte)
{
  var cantidad=compimporte.value;

  strAux1 = "";
  strAux2 = "";
  entero = "";
  var cantidadAux=cantidad;

  if (cantidad.length<1)
  {
    compimporte.value="";
    cuadroDialogo("Capture el IMPORTE a transferir",3);


    return false;
  }
  else
  {
    if (isNaN(cantidad))
    {
	  compimporte.value="";
      cuadroDialogo("Favor de ingresar un valor num\351rico en el IMPORTE",3);
      return false;
    }



    if(parseFloat(cantidadAux)<=0)
    {
      cuadroDialogo("Favor de ingresar una cantidad mayor que cero.", 3);
      return false;
    }

    pos_punto = cantidadAux.indexOf (".");

    if(pos_punto>-1 && (cantidadAux.length-pos_punto)>3)
    {
      cuadroDialogo("S\363lo se aceptan hasta dos decimales en el importe", 3);
      return false;
    }

    return true;


    num_decimales=cantidad.substring(pos_punto + 1,cantidad.length);

    if (pos_punto == 0)
      strAux1 = "0." + cantidadAux.substring (1, cantidadAux.length);
    else
    {
      if (pos_punto != -1)     //-- si se teclearon los centavos
      {
        cents = cantidadAux.substring (pos_punto + 1, cantidadAux.length);
        entero = cantidadAux.substring (0, pos_punto);
      }
      else
      {
        cents = "00";
        entero = cantidadAux;
      }

      pos_coma = entero.indexOf (",");
      if (pos_coma != -1)     //-- si son mas de mil
      {
        cientos = entero.substring (entero.length - 3, entero.length);
        miles = entero.substring (0, entero.length - 3);
      }
      else
      {
        if (entero.length > 3) //-- si se teclearon mas de mil sin coma
        {
          cientos = entero.substring (entero.length - 3, entero.length);
          miles = entero.substring (0, entero.length - 3);
        }
        else
        {
          if (entero.length == 0)
            cientos = "";
          else
            cientos = entero;
          miles = "";
        }
      }

      if (miles != "")
        strAux1 = miles;
      if (cientos != "")
        strAux1 = strAux1 + cientos + ".";
      strAux1 = strAux1 + cents;

      if (miles != "")
        strAux2 = miles;
      if (cientos != "")
        strAux2 = strAux2 + cientos + ".";
      strAux2 = strAux2 + cents;

      transf = compimporte.value;
    }
    return true;
  }
}


function validAmount (cantidad)
{

  strAux1 = "";
  strAux2 = "";
  entero = "";
  cantidadAux = cantidad;
  var miles="";


  if (cantidadAux == "" || cantidadAux <= 0)
  {
    document.transferencia.monto.value="";
    cuadroDialogo("Capture el IMPORTE a transferir ",3);

    return false;
  }
  else
  {
    if (isNaN(cantidadAux))
    {
	  document.transferencia.monto.value="";
      cuadroDialogo("Favor de ingresar un valor num&#233;rico en el IMPORTE",3);


      return false;
    }

    pos_punto = cantidadAux.indexOf (".");

    if(pos_punto>-1 && (cantidadAux.length-pos_punto)>3)
    {
      cuadroDialogo("S\363lo se aceptan hasta dos decimales en el importe", 3);
      return false;
    }

    return true;


    num_decimales=cantidad.substring(pos_punto + 1,cantidad.length);

    if (pos_punto == 0)
      strAux1 = "0." + cantidadAux.substring (1, cantidadAux.length);
    else
    {
      if (pos_punto != -1)     //-- si se teclearon los centavos
      {
        cents = cantidadAux.substring (pos_punto + 1, cantidadAux.length);
        entero = cantidadAux.substring (0, pos_punto);
      }
      else
      {
        cents = "00";
        entero = cantidadAux;
      }

      pos_coma = entero.indexOf (",");
      if (pos_coma != -1)     //-- si son mas de mil
      {
        cientos = entero.substring (entero.length - 3, entero.length);
        miles = entero.substring (0, entero.length - 3);
      }
      else
      {
        if (entero.length > 3) //-- si se teclearon mas de mil sin coma
        {
          cientos = entero.substring (entero.length - 3, entero.length);
          miles = entero.substring (0, entero.length - 3);
        }
        else
        {
          if (entero.length == 0)
            cientos = "";
          else
            cientos = entero;
          miles = "";
        }
      }

      if (miles != "")
        strAux1 = miles;
      if (cientos != "")
        strAux1 = strAux1 + cientos + ".";
      strAux1 = strAux1 + cents;

      if (miles != "")
        strAux2 = miles;
      if (cientos != "")
        strAux2 = strAux2 + cientos + ".";
      strAux2 = strAux2 + cents;

      transf = document.transferencia.monto.value;
    }
    document.transferencia.montostring.value = strAux1;

    strAux1=Formatea_Importe(strAux1);
    if (miles != "")
      document.transferencia.montostring.value = strAux2;

    return true;
  }
}



function Formatea_Importe2 (cantidad)
{
  strAux1 = "";
  strAux2 = "";
  entero = "";
  cantidadAux = cantidad;


  pos_punto = cantidadAux.indexOf (".");

  num_decimales=cantidad.substring(pos_punto + 1,cantidad.length);

  if (pos_punto == 0)
     strAux1 = "0." + cantidadAux.substring (1, cantidadAux.length);
  else
  {
     if (pos_punto != -1)     //-- si se teclearon los centavos
      {
        cents = cantidadAux.substring (pos_punto + 1, cantidadAux.length);
        entero = cantidadAux.substring (0, pos_punto);
      }
      else
      {
        cents = "00";
        entero = cantidadAux;
      }

      pos_coma = entero.indexOf (",");
      if (pos_coma != -1)     //-- si son mas de mil
      {
        cientos = entero.substring (entero.length - 3, entero.length);
        miles = entero.substring (0, entero.length - 3);
      }
      else
      {
        if (entero.length > 3) //-- si se teclearon mas de mil sin coma
        {
          cientos = entero.substring (entero.length - 3, entero.length);
          miles = entero.substring (0, entero.length - 3);
        }
        else
        {
          if (entero.length == 0)
            cientos = "";
          else
            cientos = entero;
          miles = "";
        }
      }

      if (miles != "")
        strAux1 = miles;
      if (cientos != "")
        strAux1 = strAux1 + cientos + ".";
      strAux1 = strAux1 + cents;

    strAux1=Formatea_Importe(strAux1);
   if (miles != "")
      strAux1 = strAux2;
  }
  return strAux1;

}

function validctasdif (from_account, to_account)
{
  var result=true;

  if (from_account == to_account)
  {
    cuadroDialogo("La cuenta de cargo debe ser diferente a la cuenta de abono",3);
    //document.transferencia.monto.focus();
    return false;
  }
  return result;
}


function EsAlfa(cadena)
{
  for (var i=0;i<cadena.length;i++)
  {
      if(!(((cadena.charAt(i)==' ') && (i != 0))||(cadena.charAt(i)>='a' && cadena.charAt(i)<='z')||(cadena.charAt(i)>='A' && cadena.charAt(i)<='Z')||(cadena.charAt(i)>='0' && cadena.charAt(i)<='9')))
      {
          cuadroDialogo("No se permiten caracteres especiales en CONCEPTO, como: acentos, comas, puntos, etc ",3);
          return false;
      }

  }
  return true;
}

function Valida_Date ()
{
  document.transferencia.fecha1.value=document.transferencia.fecha_completa.value;
  return true;
}


<%
String diasInhabiles = "";
if( request.getAttribute("diasInhabiles")!=null)
	diasInhabiles=(String)request.getAttribute("diasInhabiles");
%>


js_diasInhabiles = '<%= diasInhabiles%>';

var dia;
var mes;
var anio;
var fecha_completa;

function WindowCalendar()
{
    var m=new Date();
    n=m.getMonth();
    n=document.transferencia.mes.value-1;

	dia=document.transferencia.dia.value;
	mes=document.transferencia.mes.value;
	anio=document.transferencia.anio.value;

	msg=window.open("/EnlaceMig/calfut2.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
    msg.focus();

}

function Actualiza()
{
   document.transferencia.fecha_completa.value=fecha_completa;
}

function validarimportes()
{
 var bandera=true;
 var sumaCargo;
 var sumaMonto;

 if(isFloat(document.transferencia.montoCargo.value)==false)
 {
   cuadroDialogo("El total de cargo no es n�mero v�lido", 3);
   bandera=false;
 }

 if(isFloat(document.transferencia.montoAbono.value)==false)
 {
   cuadroDialogo("El total de abono no es n�mero v�lido", 3);
   bandera=false;
 }

if(bandera==true)
{
 sumaCargo=parseFloat(document.transferencia.montoCargo.value);
 sumaAbono=parseFloat(document.transferencia.montoAbono.value);
}


 if ((eval(document.transferencia.selorigencontador.value)>=1)&&
      (eval(document.transferencia.seldestinocontador.value)>=1))
  {

    if (eval(document.transferencia.montoCargo.value)<=0)
   {
     cuadroDialogo("El Total de cargo no es mayor a cero",3);
     bandera=false;
   }
   else if (eval(document.transferencia.montoAbono.value)<=0)
   {
     cuadroDialogo("El Total de abono no es mayor a cero",3);
     bandera=false;
   }

   if (sumaCargo!==sumaAbono)
   {
     cuadroDialogo("El total de cargo no corresponde al total de abono",3);
     bandera=false;
   }

  }
  return bandera;
}

function obtenerimporte(i,letra)
{

   var importe;
   var nombre="imp"+letra+i;
   for(i=0;i<document.transferencia.elements.length;i++)
   {
     if ((document.transferencia.elements[i].type=="text")&&
	     (document.transferencia.elements[i].name==nombre)&&
		 (parseFloat(document.transferencia.elements[i].value)>0))
     {
         importe=document.transferencia.elements[i].value;
		 break;
	 }
	 else importe=-1;
   }
   return importe;
}


function armaroperaciones()
{
   var bandera=true;
   var contcargo=0;
   var contabono=0;
   var cadenactascargo="";
   var cadenactasabono="";
   var cadenaimportes="";
   var importe=0;


   if (document.transferencia.ctascargo1.value=="1")
   {
     if (document.transferencia.origen.checked==true)
      {
	    contcargo++;
	    cadenactascargo=document.transferencia.origen.value;
      }
   }
   else
   {
     for(i=0;i<document.transferencia.origen.length;i++)
     {
      if (document.transferencia.origen[i].checked==true)
      {
	   contcargo++;
	   cadenactascargo=document.transferencia.origen[i].value;
      }
     }
   }
   if (document.transferencia.ctasabono1.value=="1")
   {
      if(document.transferencia.destino.checked==true)
	  {
	    contabono++;
		cadenactasabono=document.transferencia.destino.value;
      }
   }
   else
   {
     for(i=0;i<document.transferencia.destino.length;i++)
     {
      if(document.transferencia.destino[i].checked==true)
	  {
	    contabono++;
		cadenactasabono=document.transferencia.destino[i].value;
      }
     }
   }

   if(contabono>1)
   {
      cadenactascargo="1@"+cadenactascargo+"@";
	  cadenactasabono=contabono+"@";
	  cadenaimportes=contabono+"@";
      for(i=0;i<document.transferencia.destino.length;i++)
	  {

         if (document.transferencia.destino[i].checked==true)
		 {
		    cadenactasabono+=document.transferencia.destino[i].value+"@";
			importe=obtenerimporte(i,'a');
			if(importe>0)
			  cadenaimportes+=importe+"@";
            else
            {
               cuadroDialogo("Existen importes no definidos",3);
               bandera=false;
			   break;
			}
         }
	  }
   }
   else if (contcargo>1)
   {

      cadenactasabono="1@"+cadenactasabono+"@";
	  cadenactascargo=contcargo+"@";
	  cadenaimportes=contcargo+"@";
	  for(i=0;i<document.transferencia.origen.length;i++)
	  {
		   if (document.transferencia.origen[i].checked==true)
		   {
		      cadenactascargo+=document.transferencia.origen[i].value+"@";
		 	  importe=obtenerimporte(i,'c');
			  if (importe>0)
			     cadenaimportes+=importe+"@";
              else
			  {
                 cuadroDialogo("Existen importes no definidos",3);
                 bandera=false;
                 break;
			  }
           }
	   }
   }
   else if ((contabono==1)&&(contcargo==1))
   {


	  if (eval(document.transferencia.montoCargo.value)==
		   eval(document.transferencia.montoAbono.value))
     {
	    if  (eval(document.transferencia.montoCargo.value)>0)
		{
		  cadenactascargo="1@"+cadenactascargo+"@";
	      cadenactasabono="1@"+cadenactasabono+"@";
	      cadenaimportes="1@"+document.transferencia.montoCargo.value+"@";
        }
		else
	    {
		  cuadroDialogo("El importe debe ser mayor a cero",3);
	      bandera=false;
		}
      }
	  else
	  {
         cuadroDialogo("Los importes de cargo y abono no son iguales",3);
	     bandera=false;
      }
   }
    else if((contabono==0)||(contcargo==0))
    {

       cuadroDialogo("No se ha definido por lo menos una transferencia",3);
	   bandera=false;
    }
	document.transferencia.ctasorigen.value=cadenactascargo;
   document.transferencia.ctasdestino.value=cadenactasabono;
   document.transferencia.ctasimporte.value=cadenaimportes;
   return bandera;
}

function validar()
{
      document.transferencia.concepto.value=document.transferencia.concepto.value.toUpperCase();

   var result=true;
   result = Valida_Date();
   if (result == true)
     result=EsAlfa(document.transferencia.concepto.value);
   if (result == true)
     result=validarimportes();
   if (result == true)
     result=armaroperaciones();
   return result;
}




function limpiar_datos()
{
   document.transferencia.concepto.value="";
   document.transferencia.origen.selectedIndex=0;
   for(i=0;i<document.transferencia.elements.length;i++)
   {
	 if(document.transferencia.elements[i].type=="text")
     {
        document.transferencia.elements[i].value="";
	 }
   }

   document.transferencia.montoCargo.value="0.0";
   document.transferencia.montoAbono.value="0.0";
   if (document.transferencia.ctasabono1.value=="1")
   {
     document.transferencia.destino.checked=false;
   }
   else
   {
     for(i=0;i<document.transferencia.destino.length;i++)
     {
      if(document.transferencia.destino[i].checked==true)
	  {
	    document.transferencia.destino[i].checked=false;
      }
     }
   }
   if (document.transferencia.ctascargo1.value=="1")
   {
      document.transferencia.origen.checked=false;
   }
   else
   {
     for(i=0;i<document.transferencia.origen.length;i++)
     {
       if(document.transferencia.origen[i].checked==true)
	    {
	      document.transferencia.origen[i].checked=false;
       }
	 }
   }
   document.transferencia.fecha_completa.value=document.transferencia.fecha1.value;
}





function restarimpc(i)
{
   var nombre="impc"+i;
   for(i=0;i<document.transferencia.elements.length;i++)
   {
     if ((document.transferencia.elements[i].type=="text")&&
	     (document.transferencia.elements[i].name==nombre)&&
		 (parseFloat(document.transferencia.elements[i].value)>0))
     {
         document.transferencia.montoCargo.value=parseFloat(document.transferencia.montoCargo.value)
	                                           -parseFloat(document.transferencia.elements[i].value);
         document.transferencia.montoCargo.value=redondear(document.transferencia.montoCargo.value);
         //document.transferencia.montoCargo.value=document.transferencia.montoCargo.value;
         break;
	 }
   }
   return true;
}

function restarimpa(i)
{
   var nombre="impa"+i;
   for(i=0;i<document.transferencia.elements.length;i++)
   {
     if ((document.transferencia.elements[i].type=="text")&&
	     (document.transferencia.elements[i].name==nombre)&&
	     (parseFloat(document.transferencia.elements[i].value)>0))
     {
         document.transferencia.montoAbono.value=parseFloat(document.transferencia.montoAbono.value)
		                                       -parseFloat(document.transferencia.elements[i].value);
         document.transferencia.montoAbono.value=redondear(document.transferencia.montoAbono.value);
         //document.transferencia.montoAbono.value=document.transferencia.montoAbono.value;
         break;
	 }
   }
   return true;
}

function sumarimpc(i)
{
   var nombre="impc"+i;
   for(i=0;i<document.transferencia.elements.length;i++)
   {
     if ((document.transferencia.elements[i].type=="text")&&
	     (document.transferencia.elements[i].name==nombre)&&
		 (parseFloat(document.transferencia.elements[i].value)>0))
     {
         document.transferencia.montoCargo.value=parseFloat(document.transferencia.montoCargo.value)
	                                           +parseFloat(document.transferencia.elements[i].value);
         document.transferencia.montoCargo.value=redondear(document.transferencia.montoCargo.value);
         //document.transferencia.montoCargo.value=document.transferencia.montoCargo.value;
         break;
	 }
   }
   return true;
}

function sumarimpa(i)
{

   var nombre="impa"+i;
   for(i=0;i<document.transferencia.elements.length;i++)
   {
     if ((document.transferencia.elements[i].type=="text")&&
	     (document.transferencia.elements[i].name==nombre)&&
	     (parseFloat(document.transferencia.elements[i].value)>0))
     {

		 document.transferencia.montoAbono.value=parseFloat(document.transferencia.montoAbono.value)
		                                       +parseFloat(document.transferencia.elements[i].value);
         document.transferencia.montoAbono.value=document.transferencia.montoAbono.value;
         break;
	 }
   }
   return true;
}


function validarcuentasdiferentesc(elemento)
{
   var bandera=true;
   var ctac=elemento.value;
   ctac=ctac.substring(0,ctac.indexOf('|'));
   var ctaa="";
   if (document.transferencia.ctasabono1.value=="1")
   {
      if (document.transferencia.destino.checked==true)
	   {
        ctaa=document.transferencia.destino.value;
	    ctaa=ctaa.substring(0,ctaa.indexOf('|'));
        if(ctac==ctaa)
	    {
	      bandera=false;

	    }
      }
   }
   else  if ((document.transferencia.ctasabono1.value)>1)
   {
    for(i=0;i<document.transferencia.destino.length;i++)
    {
      if (document.transferencia.destino[i].checked==true)
	  {
        ctaa=document.transferencia.destino[i].value;
	    ctaa=ctaa.substring(0,ctaa.indexOf('|'));
        if(ctac==ctaa)
	    {
	      bandera=false;
	      break;
	    }
      }
     }
   }
   if (bandera==false)
    cuadroDialogo("No se permiten cuentas iguales para cargo y abono ",3);
   return bandera;
}


function validarcuentasdiferentesa(elemento)
{
   var bandera=true;
   var ctaa=elemento.value;
   ctaa=ctaa.substring(0,ctaa.indexOf('|'));
   var ctac="";
   if (document.transferencia.ctascargo1.value=="1")
   {
      if (document.transferencia.origen.checked==true)
	  {
         ctac=document.transferencia.origen.value;
	     ctac=ctac.substring(0,ctac.indexOf('|'));
         if(ctac==ctaa)
	     {
	       bandera=false;

	     }
      }
   }
   else  if (parseFloat(document.transferencia.ctascargo1.value)>1)
   {
     for(i=0;i<document.transferencia.origen.length;i++)
     {
      if (document.transferencia.origen[i].checked==true)
	  {
         ctac=document.transferencia.origen[i].value;
	     ctac=ctac.substring(0,ctac.indexOf('|'));
         if(ctac==ctaa)
	     {
	       bandera=false;
	       break;
	     }
      }
     }
   }

   if (bandera==false)
     cuadroDialogo("No se permiten cuentas iguales para cargo y abono ",3);
   return bandera;
}


function validarchcargo(elemento)
{
  var contadorcargo=0;
  var contadorabono=0;
  var resultado=true;
  resultado=validarcuentasdiferentesc(elemento);

  if (resultado==true)
  {
    for (i=0;i<document.transferencia.elements.length;i++)
    {
      if (document.transferencia.elements[i].type=='checkbox')
	  {
        if((document.transferencia.elements[i].checked==true)&&
	       (document.transferencia.elements[i].name=="origen"))
            contadorcargo++;
	    else if((document.transferencia.elements[i].checked==true)&&
		        (document.transferencia.elements[i].name=="destino"))
            contadorabono++;
       }
    }
    if ((contadorcargo>1)&&(contadorabono>1))
    {
       cuadroDialogo("Operaci&#243;n no permitida",3);
       resultado=false;
    }
  }
  if (resultado==true)
  {
    if (document.transferencia.ctascargo1.value=="1")
	{
      if ((document.transferencia.origen.checked==false)
	       &&(document.transferencia.origen==elemento))
       {
	      restarimpc(0);
	   }
	   else if ((document.transferencia.origen.checked==true)
	       &&(document.transferencia.origen==elemento))
	   {
	      sumarimpc(0);

       }
    }
	else if (parseFloat(document.transferencia.ctascargo1.value)>1)
	{
     for (i=0;i<document.transferencia.origen.length;i++)
     {
       if ((document.transferencia.origen[i].checked==false)
	       &&(document.transferencia.origen[i]==elemento))
       {
	      restarimpc(i);
		  break;
	   }
	   else if ((document.transferencia.origen[i].checked==true)
	       &&(document.transferencia.origen[i]==elemento))
	   {
	      sumarimpc(i);
	      break;
       }
     }
	}
  }
  return resultado;
}


function validarchabono(elemento)
{
  var contadorcargo=0;
  var contadorabono=0;
  var resultado=true;
  resultado=validarcuentasdiferentesa(elemento);
  if (resultado==true)
  {

     for (i=0;i<document.transferencia.elements.length;i++)
     {
      if (document.transferencia.elements[i].type=='checkbox')
	  {
	    if((document.transferencia.elements[i].checked==true)&&
	       (document.transferencia.elements[i].name=="origen"))
            contadorcargo++;
	    else if((document.transferencia.elements[i].checked==true)&&
	           (document.transferencia.elements[i].name=="destino"))
           contadorabono++;
	   }
     }

    if ((contadorcargo>1)&&(contadorabono>1))
    {
      cuadroDialogo("Operaci&#243;n no permitida",3);
      resultado=false;
    }
  }
  if (resultado==true)
  {

	 if (document.transferencia.ctasabono1.value=="1")
	{
	   if ((document.transferencia.destino.checked==false)
	      &&(document.transferencia.destino==elemento))
        {
		  restarimpa(0);

        }
		else  if ((document.transferencia.destino.checked==true)
	      &&(document.transferencia.destino==elemento))
        {
          sumarimpa(0);

        }

    }
	else
if (parseFloat(document.transferencia.ctasabono1.value)>1)
	{
	 for (i=0;i<document.transferencia.destino.length;i++)
	 {
        if ((document.transferencia.destino[i].checked==false)
	      &&(document.transferencia.destino[i]==elemento))
        {
		  restarimpa(i);
		  break;
        }
		else  if ((document.transferencia.destino[i].checked==true)
	      &&(document.transferencia.destino[i]==elemento))
        {
          sumarimpa(i);
		  break;
        }
	 }
    }
  }
  return resultado;
}

function validarimpcargo(importe)
{
  var nombre=importe.name;
  var registro=eval(nombre.substring(4,nombre.length));
  var importecargo=0.0;
  var imported=0.0;
  var result=validaImporte(importe);


if (result==true)
  {
  if (document.transferencia.ctascargo1.value=="1")
  {
    if ((result==true)&&(document.transferencia.origen.checked==true))
    {
	   imported=obtenerimporte(0,'c');
	   if (imported>0)
      {
	    //importecargo=eval(importecargo)+eval(imported);
       importecargo=parseFloat(importecargo)+parseFloat(imported);
       }



        if( (eval(document.transferencia.selorigencontador.value)>0)&&
	        (eval(document.transferencia.seldestinocontador.value)>0))
           {
            document.transferencia.montoCargo.value=redondear(importecargo);
            //document.transferencia.montoCargo.value=importecargo;
           }

     }
     else
     {
	   result=false;
       importe.value="";
       cuadroDialogo("Primero se debe seleccionar la cuenta",3);
     }
  }
  else
  {
    if ((result==true)&&(document.transferencia.origen[registro].checked==true))
    {

       for(i=0;i<document.transferencia.origen.length;i++)
	   {
	    if(document.transferencia.origen[i].checked==true)
	    {
          imported=obtenerimporte(i,'c');
		  if (imported>0)
		  {
		     //importecargo=eval(importecargo)+eval(imported);
           importecargo=parseFloat(importecargo)+parseFloat(imported);

          }

	    }
      }
       if( (eval(document.transferencia.selorigencontador.value)>0)&&
	        (eval(document.transferencia.seldestinocontador.value)>0))
           {
             document.transferencia.montoCargo.value=redondear(importecargo);
             //document.transferencia.montoCargo.value=importecargo;
           }
    }
    else
    {
     result=false;
	 importe.value="";
	 cuadroDialogo("Primero se debe seleccionar la cuenta",3);
    }
   }
  }
  else
  {
    result=false;
	importe.value="";
    for(i=0;i<document.transferencia.origen.length;i++)
	{
	    if(document.transferencia.origen[i].checked==true)
	    {
          imported=obtenerimporte(i,'c');
		  if (imported>0)
		  {
		     importecargo=parseFloat(importecargo)+parseFloat(imported);
          }

	    }
    }
   if( (eval(document.transferencia.selorigencontador.value)>0)&&
       (eval(document.transferencia.seldestinocontador.value)>0))
       {
         document.transferencia.montoCargo.value=redondear(importecargo);
         //document.transferencia.montoCargo.value=importecargo;
       }

  }
  return result;
}

function redondear(valor)
{
  var valorcambiar="";
  var valornuevo="";
  var num_decimales=0;
  var parte_entera="";
  var parte_decimal="";
  var tercerdecimal=0;
  var nuevo_decimal=0;

  valorcambiar=""+valor;
  valornuevo=valor;

  if (valorcambiar.indexOf(".")==-1)
     num_decimales=0;
 else
 {
   parte_decimal=valorcambiar.substring(valorcambiar.indexOf(".")+1,valorcambiar.length);
   parte_entera=valorcambiar.substring(0,valorcambiar.indexOf("."));
   if (parte_decimal.length>2)
   {
	  tercerdecimal=parte_decimal.substring(2,3);
	  if(eval(tercerdecimal)>5)
	  {
	     parte_decimal=parte_decimal.substring(0,2);
	     nuevo_decimal=eval(parte_decimal)+1;
         if (nuevo_decimal<100)
		    valornuevo=parte_entera+"."+nuevo_decimal;
         else
		 {
		    valornuevo=eval(parte_entera)+1;
			valornuevo+=".00";
		 }

	  }
	  else
	  {
	     parte_decimal=parte_decimal.substring(0,2);
		 valornuevo=parte_entera+"."+parte_decimal;
      }
   }
 }
 return valornuevo;

}


function validarimpabono(importe)
{

  var nombre=importe.name;
  var registro=eval(nombre.substring(4,nombre.length));
  var registro;
  var importeabono=0.0;
  var imported=0.0;
  var indicepunto=0;
  var longitud=0;
  var result=validaImporte(importe);

  if (result==true)
  {
  if (document.transferencia.ctasabono1.value=="1")
  {
    if ((result==true)&&(document.transferencia.destino.checked==true))
    {
		imported=obtenerimporte(0,'a');
	    if (imported>0)
		   importeabono=parseFloat(importeabono)+parseFloat(imported);

       if( (eval(document.transferencia.selorigencontador.value)>0)&&
           (eval(document.transferencia.seldestinocontador.value)>0))
           {
               document.transferencia.montoAbono.value=redondear(importeabono);
               //document.transferencia.montoAbono.value=importeabono;
           }
    }
    else
    {
	  result=false;
	  importe.value="";
      cuadroDialogo("Primero se debe seleccionar la cuenta",3);
    }

  }
  else
  {
    if ((result==true)&&(document.transferencia.destino[registro].checked==true))
    {
      for(i=0;i<document.transferencia.destino.length;i++)
	  {
	    if(document.transferencia.destino[i].checked==true)
	    {
		  imported=obtenerimporte(i,'a');
		  if (imported>0)
		  {
		     importeabono=parseFloat(importeabono)+parseFloat(imported);

          }
	    }
      }
       if( (eval(document.transferencia.selorigencontador.value)>0)&&
           (eval(document.transferencia.seldestinocontador.value)>0))
         {
            document.transferencia.montoAbono.value=redondear(importeabono);
            //document.transferencia.montoAbono.value=importeabono;
         }
    }
    else
	{
 	   result=false;
	   importe.value="";
	   cuadroDialogo("Primero se debe seleccionar la cuenta",3);
    }
  }
  }
  else
  {
    result=false;
	importe.value="";
	for(i=0;i<document.transferencia.destino.length;i++)
	{
	  if(document.transferencia.destino[i].checked==true)
	  {
	     imported=obtenerimporte(i,'a');
		 if (imported>0)
		    importeabono=parseFloat(importeabono)+parseFloat(imported);
	  }
    }
   if( (eval(document.transferencia.selorigencontador.value)>0)&&
       (eval(document.transferencia.seldestinocontador.value)>0))
       {
         document.transferencia.montoAbono.value=redondear(importeabono);
         //document.transferencia.montoAbono.value=importeabono;
       }
  }
  return result;
}


<!-- *********************************************** -->
<!-- modificaci�n para integraci�n pva 07/03/2002    -->
<!-- *********************************************** -->
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var opcion;
var tramadicional;
var cfm;

function CuentasCargo()
{
   opcion=1;
   //PresentarCuentas();
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();

}
function CuentasAbono()
{
   opcion=2;
   //PresentarCuentas();
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=2","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}

/*
function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}*/
function actualizacuenta()
{
  if(opcion==1)
  {
	  document.transferencia.selorigen.value=ctaselec+"|"+ctatipre+"|"+ctatipro+"|"+ctadescr+"@";
      document.transferencia.textselorigen.value=ctaselec+" "+ctadescr;
  }
  else
  {
  	  document.transferencia.seldestino.value=ctaselec+"|"+ctatipre+"|"+ctatipro+"|"+ctadescr+"@";
      document.transferencia.textseldestino.value=ctaselec+" "+ctadescr;
  }
}

function ValidaCtaSel(cta)
{
  var result=true;
  var cuenta=""+cta;
  if(cta.length>0)
	  result=true;
  else
  	  result=false;
 return result;
}

function NuevaCtaSel(cta)
{
  var result=true;
  var cuenta=""+cta.substring(0,cta.indexOf("|"));
  var tramaux="";
  var tramacta="";
  var cuantasOrigen=document.transferencia.origen.length;
  var cuentasDestino=document.transferencia.destino.length;

  if (eval(document.transferencia.selorigencontador.value)>0)
  {

	  tramaux=document.transferencia.selorigentrama.value;
	  for(i=1;i<=eval(document.transferencia.selorigencontador.value);i++)
      {
         tramacta=tramaux.substring(0,tramaux.indexOf("@"));
         tramacta=tramacta.substring(0,tramacta.indexOf("|"));

		 if(tramacta==cuenta)
		 {
            return false;
		 }
		 if(eval(i)<eval(document.transferencia.selorigencontador.value))
		   tramaux=tramaux.substring(tramaux.substring("@")+1,tramaux.length);

      }

  }
  if(eval(document.transferencia.seldestinocontador.value)>0)
  {
	  tramaux=document.transferencia.seldestinotrama.value;
	  for(i=1;i<=eval(document.transferencia.seldestinocontador.value);i++)
      {
         tramacta=tramaux.substring(0,tramaux.indexOf("@"));
         tramacta=tramacta.substring(0,tramacta.indexOf("|"));
		  if(tramacta==cuenta)
		  {
            return false;
		  }
		 if(eval(i)<eval(document.transferencia.seldestinocontador.value))
		   tramaux=tramaux.substring(tramaux.substring("@")+1,tramaux.length);
      }
  }
  return result;
}



function revisaNuevaCuenta(cuenta, tipo)
{
  var cuantasOrigen;
  var cuantasDestino;
  var limite;
  var auxCuenta, auxValor;
  var numeroCuenta;
  var i=0;
  var contador;


   numeroCuenta=parseInt(cuenta.substring(0, cuenta.indexOf("|")));
   //alert("Cuenta: "+numeroCuenta);

  //if(tipo==1)
  //{
   // Cuentas de cargo

   if(!isInteger(document.transferencia.selorigencontador.value))
   {
      //alert("El contador no es numerico");
      return false;
   }

   //alert(document.transferencia.selorigencontador.value);
   contador=parseInt(document.transferencia.selorigencontador.value);

   if(contador<1)
   {
      return true;
   }

   if(contador==1)
   {
      limite=contador;
      //alert("Excepcion: "+contador);
   }
   else
   {
      cuantasOrigen=document.transferencia.origen.length;
      limite=cuantasOrigen;
   }


   //limite=contador;
   //alert(limite);


   if(limite==1)
   {
      auxValor=document.transferencia.origen.value;
      //alert("Revisando: "+auxValor);
      auxCuenta=parseInt(auxValor.substring(0, auxValor.indexOf("|")));
      //alert(auxCuenta);

         if(numeroCuenta==auxCuenta)
         {
            return false;
         }
   }
   else
   {
      for(i=0;i<limite;i++)
      {
         auxValor=document.transferencia.origen[i].value;
         auxCuenta=parseInt(auxValor.substring(0, auxValor.indexOf("|")));
         //alert(auxCuenta);

         if(numeroCuenta==auxCuenta)
         {
            return false;
         }
      } // Fin for
   } // Fin if-else limite
  //}
  //else
  //{
   // Cuentas de abono

   if(!isInteger(document.transferencia.seldestinocontador.value))
   {
      //alert("El contador no es numerico");
      return false;
   }

   //alert(document.transferencia.seldestinocontador.value);
   contador=parseInt(document.transferencia.seldestinocontador.value);


   if(contador<1)
   {
      return true;
   }

   if(contador==1)
   {
      limite=contador;
      //alert("Excepcion: "+contador);
   }
   else
   {
      cuantasDestino=document.transferencia.destino.length;
      limite=cuantasDestino;
   }


   //limite=contador;
   //alert(limite);

   if(limite==1)
   {
      auxValor=document.transferencia.destino.value;
      //alert("Revisando: "+auxValor);
      auxCuenta=parseInt(auxValor.substring(0, auxValor.indexOf("|")));
      //alert(auxCuenta);

      if(numeroCuenta==auxCuenta)
      {
         return false;
      }
   }
   else
   {
      for(i=0;i<limite;i++)
      {
         auxValor=document.transferencia.destino[i].value;
         auxCuenta=parseInt(auxValor.substring(0, auxValor.indexOf("|")));
         //alert(auxCuenta);

         if(numeroCuenta==auxCuenta)
         {
            return false;
         }
      } // Fin for
   } // Fin if-else limite
  //} // Fin if-else tipo


  return true;
}


function cuentasIgualesMismoTiempo(c01,c02)
{
var cuenta01, cuenta02;
var val01, val02;

   if(c01.length==0)
   {
      return false;
   }

   if(c02.length==0)
   {
      return false;
   }

   cuenta01=c01.substring(0,c01.indexOf("|"));
   cuenta02=c02.substring(0,c01.indexOf("|"));

   if((isInteger(cuenta01) && isInteger(cuenta02))==false)
   {
      return false;
   }

   val01=parseInt(cuenta01);
   val02=parseInt(cuenta02);


   if(val01==val02)
   {
      return true;
   }


   return false;
}


function definir_valores_iniciales()
{
   var trama="";
   if(eval(document.transferencia.selorigencontador.value)>0)
   {
	 if(eval(document.transferencia.selorigencontador.value)==1)
	 {
        if (document.transferencia.origen.checked==true)
			  trama+="1@";
		else
			  trama+="0@";

     }
	 else
	 {
	    for(i=0;i<document.transferencia.origen.length;i++)
        {
	      if(document.transferencia.origen[i].checked==true)
	        trama+="1@";
		  else
            trama+="0@";
        }
     }
   }
   document.transferencia.selorigenestatus.value=trama;

   trama="";
   if(eval(document.transferencia.seldestinocontador.value)>0)
   {
     if(eval(document.transferencia.seldestinocontador.value)==1)
	 {
       if (document.transferencia.destino.checked==true)
			  trama+="1@";
		else
			  trama+="0@";


	 }
	 else
	 {
	   for(i=0;i<document.transferencia.destino.length;i++)
       {
	    if(document.transferencia.destino[i].checked==true)
	      trama+="1@";
		else
          trama+="0@";
       }
     }

   }
   document.transferencia.seldestinoestatus.value=trama;
   var contadorc=0;
   var contadora=0;

   var nombre="";
   trama="";
   var trama2="";
   for (i=0;i<document.transferencia.elements.length;i++)
   {
      if(document.transferencia.elements[i].type=="text")
	  {
		  nombre="impc"+contadorc;
		  if(document.transferencia.elements[i].name==nombre)
          {
			  trama+=document.transferencia.elements[i].value+"@";
			  contadorc++;
		  }
		  nombre="impa"+contadora;
		  if(document.transferencia.elements[i].name==nombre)
          {
			  trama2+=document.transferencia.elements[i].value+"@";
			  contadora++;
		  }
	  }
   }
   document.transferencia.selorigenimporte.value=trama;
   document.transferencia.seldestinoimporte.value=trama2;
}


function AgrSelCuenta()
{
var tipo=0;
var resultado02=false;

   if (document.transferencia.selorigen.value.length>0)
   {
     tipo=1;
   }
   else
   {
      if (document.transferencia.seldestino.value.length>0)
         tipo=2;
   }


   if(document.transferencia.selorigen.value.length>0 && document.transferencia.seldestino.value.length>0)
   {
      tipo=3;
   }


    if(cuentasIgualesMismoTiempo(document.transferencia.selorigen.value, document.transferencia.seldestino.value)==true)
    {
      tipo=-1;
    }


   switch(tipo)
   {
      case -1:
         cuadroDialogo("La cuentas de cargo y abono no pueden ser la misma", 3);
         document.transferencia.selorigen.value="";
         document.transferencia.seldestino.value="";
         document.transferencia.textselorigen.value="";
         document.transferencia.textseldestino.value="";
         break;
      case 0:
         cuadroDialogo("No se ha definido la cuenta de Cargo o Abono",3);
         break;

      case 1:
         result=ValidaCtaSel(document.transferencia.selorigen.value);

         if (result==true)
         {
            //result=NuevaCtaSel(document.transferencia.selorigen.value);
            result=revisaNuevaCuenta(document.transferencia.selorigen.value, tipo);

            if (result==true)
            {
               definir_valores_iniciales();
               document.transferencia.action="TransferenciasMultiples?ventana=1";
               document.transferencia.submit();
            }
            else
            {
               cuadroDialogo("La cuenta ya se encuentra registrada",3);
               document.transferencia.selorigen.value="";
               document.transferencia.textselorigen.value="";
            }
         }
         else
         {
            cuadroDialogo("No se ha definido la cuenta de Cargo",3);
         }

         break;

      case 2:
         result=ValidaCtaSel(document.transferencia.seldestino.value);


         if (result==true)
         {
            //result=NuevaCtaSel(document.transferencia.seldestino.value);
            result=revisaNuevaCuenta(document.transferencia.seldestino.value, tipo);

            if (result==true)
            {
               definir_valores_iniciales();
               document.transferencia.action="TransferenciasMultiples?ventana=1";
               document.transferencia.submit();
            }
            else
            {
               cuadroDialogo("La cuenta ya se encuentra registrada",3);
               document.transferencia.seldestino.value="";
               document.transferencia.textseldestino.value="";
            }
         }
         else
         {
            cuadroDialogo("No se ha definido la cuenta de Abono",3);
         }

         break;

         case  3:
            result=ValidaCtaSel(document.transferencia.selorigen.value);

            if(result==false)
            {
               cuadroDialogo("No se ha definido la cuenta de cargo", 3);
               document.transferencia.selorigen.value="";
               document.transferencia.textselorigen.value="";
            }

            resultado02=ValidaCtaSel(document.transferencia.seldestino.value);

            if(resultado02==false)
            {
               cuadroDialogo("No se ha definido la cuenta de abono", 3);
               document.transferencia.seldestino.value="";
               document.transferencia.textseldestino.value="";
            }


            if(result && resultado02)
            {
               result=revisaNuevaCuenta(document.transferencia.selorigen.value, 1);
               resultado02=revisaNuevaCuenta(document.transferencia.seldestino.value, 2);

               if(result==false)
               {
                  cuadroDialogo("La cuenta de origen ya se encuentra registrada",3);
                  document.transferencia.selorigen.value="";
                  document.transferencia.textselorigen.value="";
               }


               if(resultado02==false)
               {
                  cuadroDialogo("La cuenta de destino ya se encuentra registrada",3);
                  document.transferencia.seldestino.value="";
                  document.transferencia.textseldestino.value="";
               }

               if(result && resultado02)
               {
                  definir_valores_iniciales();
                  document.transferencia.action="TransferenciasMultiples?ventana=1";
                  document.transferencia.submit();
               }
            }


         break;
   }
}


function EnfSelCta()
{
   document.transferencia.textselorigen.value="";
   document.transferencia.textseldestino.value="";
}

//-->
</script>

<script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

<%
if( request.getAttribute("newMenu")!=null)
	out.println(request.getAttribute("newMenu") );

//request.getAttribute("newMenu")
%>

</script>


<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');EnfSelCta(); obtenDatosBrowser();" background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
	<td width="*">
	<!-- MENU PRINCIPAL -->
		<%
if( request.getAttribute("MenuPrincipal")!=null)
	out.println(request.getAttribute("MenuPrincipal") );

		//request.getAttribute("MenuPrincipal")
		%>
	</TD>
  </TR>
</TABLE>

<%
if(request.getAttribute("Encabezado")!=null)
	out.println(request.getAttribute("Encabezado") );


//request.getAttribute("Encabezado")

%>

<!-- CONTENIDO INICIO -->
	<FORM  NAME="transferencia" METHOD=POST  ACTION="TransferenciasMultiples?ventana=2" onSubmit="return validar();">
<table width="644" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td>


			<TABLE ID="Table1" border=0 cellspacing=2 cellpadding=3 align=center >
			<tr  bgcolor=#A4BEE4>
			<TD align=center width="50%" class=tittabdat>Cuenta Cargo</td>
			<td align="center" width="50%"  class="tittabdat">Cuenta Abono/M&oacute;vil</td>
		    </tr>
			<tr>
			<td align=center>
			<input type="text" name=textselorigen  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="">
					<A HREF="javascript:CuentasCargo();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif"  width="12" height="14" border="0" align="absmiddle" alt="Cuentas cargo"></A>
					<input type="hidden" name="selorigen" value="">
            </td>
			<td align=center>
             <input type="text" name=textseldestino  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="">
					<A HREF="javascript:CuentasAbono();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif"  width="12" height="14" border="0" align="absmiddle" alt="Cuentas abono"></A>
					<input type="hidden" name="seldestino" value="">

 			 </td>
		    </tr>




			<tr>
             <td align=center colspan=2 >
             <A HREF="javascript:AgrSelCuenta();"><IMG SRC="/gifs/EnlaceMig/gbo25425.gif" border=0  alt="Agregar"></A>
              </td>
			 </tr>


             <TR>
             <TD valign = "TOP" align=center>
			 <%
				if( request.getAttribute("cuentas_origen")!=null)
					out.println(request.getAttribute("cuentas_origen"));
				else
					out.println("");
			 %>
    		 </TD>
			 <TD valign = "TOP" align=center>
			 <%
				if( request.getAttribute("cuentas_destino")!=null)
					out.println(request.getAttribute("cuentas_destino"));
				else
					out.println("");
			 %>
  		    </TD>
            </TR>




             <%
			   if( request.getAttribute("campos_totales")!=null)
	            out.println(request.getAttribute("campos_totales") );
             %>
            </TABLE>
             <%
			    if( request.getAttribute("campos_concepto_fecha")!=null)
	             out.println(request.getAttribute("campos_concepto_fecha") );
             %>
	</td>
    </tr>
</table>
            <%
			if( request.getAttribute("campos_botones")!=null)
	           out.println(request.getAttribute("campos_botones") );

          %>
<!----------------------------------------------------------->

            <INPUT TYPE="HIDDEN" NAME="opcion_pantalla" VALUE=<%
if( request.getAttribute("opcion_pantalla")!=null)
	out.println(request.getAttribute("opcion_pantalla"));
else
	out.println("1");
			%>>

			<INPUT TYPE="HIDDEN" NAME="ctasorigen" VALUE="">
           <INPUT TYPE="HIDDEN" NAME="ctasdestino" VALUE="">
			<INPUT TYPE="HIDDEN" NAME="ctasimporte" VALUE="">
	        <INPUT TYPE="HIDDEN" NAME="fecha_programada" VALUE=0>
            <INPUT TYPE="HIDDEN" NAME="montostring" VALUE=0>
            <INPUT TYPE="HIDDEN" NAME="fac_programadas1" VALUE=<%
if( request.getAttribute("fac_programadas1")!=null)
	out.println(request.getAttribute("fac_programadas1"));
else
	out.println("");
			%>>
			<INPUT TYPE="HIDDEN" NAME="divisa" VALUE=<%
if( request.getAttribute("divisa")!=null)
	out.println(request.getAttribute("divisa"));
else
	out.println("");
			%>>
   			<INPUT TYPE="HIDDEN" NAME="trans" VALUE=<%
if( request.getAttribute("trans")!=null)
	out.println(request.getAttribute("trans"));
else
	out.println("");
			%>>



					 <INPUT TYPE="hidden" NAME="selorigentrama" VALUE=
					<%
                          if( request.getAttribute("selorigentrama")!=null)
	                          out.println(request.getAttribute("selorigentrama"));
                         else
	                         out.println("");
			        %>>

					<INPUT TYPE="hidden" NAME="selorigencontador" VALUE=
					<%
                          if( request.getAttribute("selorigencontador")!=null)
	                          out.println(request.getAttribute("selorigencontador"));
                         else
	                         out.println("0");
			        %>>
					<INPUT TYPE="hidden" NAME="selorigenimporte" VALUE=
					<%
                          if( request.getAttribute("selorigenimporte")!=null)
	                          out.println(request.getAttribute("selorigenimporte"));
                         else
	                         out.println("");
			        %>>
					<INPUT TYPE="hidden" NAME="selorigenestatus" VALUE=
					<%
                          if( request.getAttribute("selorigenestatus")!=null)
	                          out.println(request.getAttribute("selorigenestatus"));
                         else
	                         out.println("");
			        %>>
             <INPUT TYPE="hidden" NAME="seldestinotrama" VALUE=
					<%
                          if( request.getAttribute("seldestinotrama")!=null)
	                          out.println(request.getAttribute("seldestinotrama"));
                         else
	                         out.println("");
			        %>>

					<INPUT TYPE="hidden" NAME="seldestinocontador" VALUE=
					<%
                          if( request.getAttribute("seldestinocontador")!=null)
	                          out.println(request.getAttribute("seldestinocontador"));
                         else
	                         out.println("0");
			        %>>


					<INPUT TYPE="hidden" NAME="seldestinoimporte" VALUE=
					<%
                          if( request.getAttribute("seldestinoimporte")!=null)
	                          out.println(request.getAttribute("seldestinoimporte"));
                         else
	                         out.println("");
			        %>>


					<INPUT TYPE="hidden" NAME="seldestinoestatus" VALUE=
					<%
                          if( request.getAttribute("seldestinoestatus")!=null)
	                          out.println(request.getAttribute("seldestinoestatus"));
                         else
	                         out.println("");
			        %>>




<INPUT TYPE="hidden" NAME="arregloctas_origen1"   VALUE=<%
if( request.getAttribute("arregloctas_origen1")!=null)
	out.println(request.getAttribute("arregloctas_origen1"));
else
	out.println("");
%>></INPUT>
<INPUT TYPE="hidden" NAME="arregloctas_destino1"  VALUE=<%
if( request.getAttribute("arregloctas_destino1")!=null)
	out.println(request.getAttribute("arregloctas_destino1"));
else
	out.println("");
%>></INPUT>
<INPUT TYPE="HIDDEN" NAME="arregloimportes1"      VALUE=<%
if( request.getAttribute("arregloimportes1")!=null)
	out.println(request.getAttribute("arregloimportes1"));
else
	out.println("");
%>></INPUT>
<INPUT TYPE="HIDDEN" NAME="arreglofechas1"        VALUE=<%
if( request.getAttribute("arreglofechas1")!=null)
	out.println(request.getAttribute("arreglofechas1"));
else
	out.println("");
%>></INPUT>
<INPUT TYPE="HIDDEN" NAME="arregloconceptos1"     VALUE=<%
if( request.getAttribute("arregloconceptos1")!=null)
	out.println(request.getAttribute("arregloconceptos1"));
else
	out.println("");
 %>></INPUT>
<INPUT TYPE="HIDDEN" NAME="contador1"             VALUE=<%
if( request.getAttribute("contador1")!=null)
	out.println(request.getAttribute("contador1"));
else
	out.println("");
%>></INPUT>
<INPUT TYPE="HIDDEN" NAME="arregloestatus1"       VALUE=<%
if( request.getAttribute("arregloestatus1")!=null)
	out.println(request.getAttribute("arregloestatus1"));
else
	out.println("");
%>></INPUT>
<INPUT TYPE="hidden" NAME="fecha1" VALUE =<%
if( request.getAttribute("fecha1")!=null)
	out.println(request.getAttribute("fecha1"));
else
	out.println("");
%>></INPUT>
<INPUT TYPE="HIDDEN" NAME="fecha2"  VALUE=<%
if( request.getAttribute("fecha2")!=null)
	out.println(request.getAttribute("fecha2"));
else
	out.println("");
 %>></INPUT>
<INPUT TYPE="HIDDEN" NAME="TIPOTRANS" VALUE=<%
if( request.getAttribute("TIPOTRANS")!=null)
	out.println(request.getAttribute("TIPOTRANS"));
else
	out.println("");
 %>></INPUT>
<INPUT TYPE="HIDDEN" NAME="INICIAL" VALUE=<%
if( request.getAttribute("INICIAL")!=null)
	out.println(request.getAttribute("INICIAL"));
else
	out.println("");
%>></INPUT>
<INPUT TYPE="HIDDEN" NAME="NMAXOPER" VALUE=<%
if( request.getAttribute("NMAXOPER")!=null)
	out.println(request.getAttribute("NMAXOPER"));
else
	out.println("");
%>></INPUT>
<INPUT TYPE="HIDDEN" NAME="ctascargo1"  VALUE=<%
if( request.getAttribute("ctascargo1")!=null)
	out.println(request.getAttribute("ctascargo1"));
else
	out.println("");
%>></INPUT>
<INPUT TYPE="HIDDEN" NAME="ctasabono1"  VALUE=<%
if( request.getAttribute("ctasabono1")!=null)
	out.println(request.getAttribute("ctasabono1"));
else
	out.println("");
%>></INPUT>
<input type="hidden" id="datosBrowser" name="datosBrowser" value=""/>
   </FORM>
<!-- CONTENIDO FINAL -->
</body>
</html>
<!-- 2007.01 -->