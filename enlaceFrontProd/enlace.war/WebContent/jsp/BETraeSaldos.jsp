<html>
 <head>
  <title>Banca Virtual</title>
  <!--
    Elaborado:  Francisco Serrato Jimenez (fsj)
    Aplicativo: Enlace Internet
    Proyecto:   MX-2002-257
                Getronics CP Mexico
    Archivo:   BETraeSaldos.jsp
  -->
  <script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
  <script language="JavaScript" src="/EnlaceMig/fw_menu.js"></script>
  <script language="JavaScript" src="/EnlaceMig/cuadroDialogo.js"></script>
  <script language="JavaScript">
   <!--
    function MM_preloadImages()
    {   //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if( a[i].indexOf("#")!=0 ){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }
    function MM_swapImgRestore()
    {   //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }
    function MM_findObj(n, d)
    {   //v3.0
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
        d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
    }
    function MM_swapImage()
    {   //v3.0
        var i,j=0,x,a=MM_swapImage.arguments;
        document.MM_sr=new Array;
        for(i=0;i<(a.length-2);i+=3)
            if( (x=MM_findObj(a[i]))!=null )
            {
                document.MM_sr[j++]=x;
                if( !x.oSrc )
                    x.oSrc=x.src;
                x.src = a[i+2];
            }
    }
    function js_posicion()
    {
        var numCta = "";
        var seleccion = false;
        for(i=0; i<document.tsaldos.elements.length ;i++)
            if( document.tsaldos[i].type=="radio" && document.tsaldos[i].checked )
            {
                numCta += document.tsaldos.elements[i].value + ",";
                seleccion = true;
                break;
            }//fin if
        if( seleccion )
        {
            document.tsaldos.cta_saldo.value = numCta.substring(0,numCta.indexOf("|")) +",";
            document.tsaldos.action = "BE_TraePosicion"
            document.tsaldos.submit();
        }//fin if
        else
            cuadroDialogo("Seleccione un registro por favor",3);
    }
    function enviar(opcion)
    {
        if( opcion==1 )document.tsaldos.action = "poscpsrvr1";
        else           document.tsaldos.action = "CMovimiento?Banca=E";
        if( validar() )
            document.tsaldos.submit();
    }
    function validar()
    {
        var resultado = false;
        for(i=0; i<document.tsaldos.elements.length ;i++)
            if( document.tsaldos.elements[i].type=="radio" && document.tsaldos.elements[i].checked==true )
            {
                resultado = true;
                break;
            }
        if( !resultado )
            cuadroDialogo("No se ha seleccionado ninguna cuenta", 3);
        return resultado;
    }
<%
    if( request.getAttribute("newMenu")!=null )
        out.print(request.getAttribute("newMenu"));
%>
   //-->
  </script>
  <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
 </head>
 <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">
  <table border="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
   <tr valign="top">
    <td width="*">
<%
    if( request.getAttribute("MenuPrincipal")!=null )
        out.print(request.getAttribute("MenuPrincipal"));
%>
    </TD>
   </TR>
  </table>
<%
    if( request.getAttribute("Encabezado")!=null )
        out.print(request.getAttribute("Encabezado"));
%>
  <FORM  name="tsaldos" method=post action="poscpsrvr1" onSubmit="return validar();">
   <table width="760" border="0" cellspacing="0" cellpadding="0">
    <tr>
     <td>
      <table border="0" CELLPADDING="3" CELLSPACING="2" align="center">
       <tr>
        <td class="tittabdat" align="center" width="50" >Seleccione        </td>
        <td class="tittabdat" align="center" width="110">Contrato<br>Fondos de Inversi&oacute;n</td>
        <td class="tittabdat" align="center" width="200">Descripci&oacute;n</td>
        <td class="tittabdat" align="center" width="80" >Mesa de Dinero    </td>
        <td class="tittabdat" align="center" width="80" >Fondos<br>de Inversi&oacute;n</td>
        <td class="tittabdat" align="center" width="80" >Total             </td>
       </tr>
<%
    if( request.getAttribute("cta")!=null )
        out.print(request.getAttribute("cta"));
%>
       <tr>
        <td align="CENTER" colspan="6"><br>
         <a href="javascript:js_posicion();" ><img src="/gifs/EnlaceMig/gbo25270.gif" width="85"  height="22" border="0" alt="Posicion"   ></a>
         <a href="javascript:enviar(2);"     ><img src="/gifs/EnlaceMig/gbo25260.gif" width="104" height="22" border="0" alt="Movimientos"></a>
         <a href="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" width="83"  height="22" border="0" alt="Imprimir"   ></a>
        </td>
       </tr>
       <tr>
        <td align="right" colspan = 7>
<%
    if( request.getAttribute("DownLoadFile")!=null )
        out.print(request.getAttribute("DownLoadFile"));
%>
        </td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
   <input type="hidden" name="cta_posi"     value="">
   <input type="hidden" name="cta_saldo"    value="">
   <input type="hidden" name="j"            value="1">
   <input type="hidden" name="tipoConsulta" value="soloPosicion">
  </FORM>
 </body>
</html>
<Script language = "JavaScript">
 <!--
  function VentanaAyuda(ventana)
  {
    hlp=window.open("/EnlaceMig/ayuda.html#" + ventana ,"hlp","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
    hlp.focus();
  }
 //-->
</Script>