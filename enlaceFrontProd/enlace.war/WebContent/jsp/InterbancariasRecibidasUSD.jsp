<%@page import="mx.altec.enlace.beans.InterbancariasRecibidasBeanUSD"%>
<%@ page import="mx.altec.enlace.beans.*,java.util.*"%>
<%@page import="java.util.TreeMap"%>


<%@page import="EnlaceMig.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>

<title>Enlace</title>

<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s25290">
<meta name="Proyecto" content="Portal">
<meta name="Version" content="1.0">

 <script type="text/javascript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
 <script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
 <script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>

<%
String bancoSpid = "";
if(request.getAttribute("ListSpid") != null){
	bancoSpid = (String) request.getAttribute("ListSpid");
}
 %>
<Script type="text/javascript" >


function cargaListas(Lista){
	   var lista = Lista;
	   var select = document.getElementById('bancoBN');
	   var strBancBN = document.getElementById(lista).value;
	   var array;
	   var arrayAux;
	    select.options.length = 0;
		select.value = "";
		if(strBancBN != "@"){
			array = strBancBN.split("@");
			select.options[0] = new Option ("Seleccione un banco", "0");
			for (var i = 1; i < (array.length)-1; i++){
					arrayAux = array[i].split("?");
					select.options[i] = new Option (arrayAux[1], arrayAux[0]);
			}
		}else{
			select.options[0] = new Option ("::No existen datos para mostrar::", "0");
		}
	}

function claveBanco()
{
	var cveBanco = "";
	var ceros = "0000";
	var forma=document.frmIntRecibidas;

	cveBanco = forma.bancoBN.options[forma.bancoBN.selectedIndex].value;
	if(cveBanco==0)
	  return "";
	cveBanco = trimString(cveBanco.substring(cveBanco.indexOf("+")+1,cveBanco.indexOf("-")));
	if(cveBanco.length != 3)
		cveBanco = (ceros.substring(0,3-(cveBanco.length)) )+cveBanco;
	return cveBanco;
}

 function validaNavegador(formObj) {
 	var mensaje = 'Al momento de ingresar a \u00e9ste sitio, por su seguridad se cerrar\u00e1 la sesi\u00f3n abierta con Banco Santander M\u00e9xico S.A., Instituci\u00f3n de Banca M\u00faltiple, Grupo Financiero Santander M\u00e9xico, en caso de estar de acuerdo de click en Aceptar';
 	var bandera=true;
	if((navigator.userAgent).indexOf('MSIE')!=-1) {
		var aux=navigator.userAgent.substring((navigator.userAgent).indexOf('MSIE')+4,(navigator.userAgent).indexOf('MSIE')+9);
		var arreglo = aux.split('.');
		if(!(arreglo[0]>=10)){//valida que sea la version 10 o superior
		bandera=false;
		}
	}else if((navigator.userAgent).indexOf('Firefox')!=-1) {
		if(!(navigator.userAgent.substring((navigator.userAgent).indexOf('Firefox')+8,(navigator.userAgent).indexOf('Firefox')+10)>=23)){//valida que sea la version 23 o superior
			bandera=false;
		}
	}else if((navigator.userAgent).indexOf('Chrome')!=-1) {
		if(!(navigator.userAgent.substring((navigator.userAgent).indexOf('Chrome')+7,(navigator.userAgent).indexOf('Chrome')+9)>=28)){//valida que sea la version 28 o superior
			bandera=false;
		}
	}else {
		alert('es otro navegador');
	}
	if(!bandera){
		mensaje='El portal de Banco de M\u00e9xico podr\u00eda no funcionar correctamente debido a la versi\u00f3n de su navegador, en caso de estar de acuerdo de click en Aceptar, Al momento de ingresar a \u00e9ste sitio, por su seguridad se cerrar\u00e1 la sesi\u00f3n abierta con Banco Santander M\u00e9xico S.A., Instituci\u00f3n de Banca M\u00faltiple, Grupo Financiero Santander M\u00e9xico';
	}
		if(!confirm(mensaje)){
			return false;
        }
	}

	function FrmClean(){
	  document.frmIntRecibidas.fecha1.value	= "";
	  document.frmIntRecibidas.textEnlaceCuenta.value	="";
	  document.frmIntRecibidas.txtReferencia.value	="";
	  document.frmIntRecibidas.txtCveRastreo.value	="";

	  document.frmIntRecibidas.txtReferencia.disabled=false;
	  document.frmIntRecibidas.txtCveRastreo.disabled=false;
	}

	function js_consultar(){
		fecha1 	   =   document.frmIntRecibidas.fecha1.value;
		cuenta	   =   document.frmIntRecibidas.textEnlaceCuenta.value;
	   	referencia =   document.frmIntRecibidas.txtReferencia.value;
	   	cveRastreo =   document.frmIntRecibidas.txtCveRastreo.value;

		if(fecha1 == ""){
			cuadroDialogo("Usted no ha seleccionado una fecha para la consulta.",1);
			return;
		}
		if(cuenta == ""){
			cuadroDialogo("Usted no ha seleccionado una cuenta para la consulta.",1);
			return;
		}
		if(referencia == "" && cveRastreo == ""){
			cuadroDialogo("Usted debe indicar una referencia o clave de rastreo",1);
			return;
		}

		if(document.frmIntRecibidas.txtReferencia.value != "" &&
			!document.frmIntRecibidas.txtReferencia.value.match(/^[0-9]+$/)){
			cuadroDialogo("La referencia debe ser num&eacute;rica.", 1);
			return;
		}

		if(document.frmIntRecibidas.txtCveRastreo.value != "" &&
			!document.frmIntRecibidas.txtCveRastreo.value.match(/^([a-z]|[0-9]|[\s])*$/i)){
			cuadroDialogo("La clave de rastreo debe ser alfanum&eacute;rica.", 1);
			return;
		}

		if(document.frmIntRecibidas.bancoBN.value == 0 && referencia == ""){
				cuadroDialogo("Usted no ha seleccionado el banco ordenante para la consulta.", 1);
				return;
			}


		document.frmIntRecibidas.action="InterbancariasRecibidasUSD?Modulo=consulta";
		document.frmIntRecibidas.submit();

	}

	function js_regresar(){
		document.frmIntRecibidas.action="InterbancariasRecibidasUSD?Modulo=inicio";
		document.frmIntRecibidas.submit();

	}

	function js_exportar(){
		document.location.href = "<%= (String)session.getAttribute("fileExport") %>";
	}

	function js_referencia(){
		if(document.frmIntRecibidas.txtReferencia.value.length > 0 ){
			document.frmIntRecibidas.txtCveRastreo.value = '';
			document.frmIntRecibidas.txtCveRastreo.disabled=true;
			document.frmIntRecibidas.bancoBN.disabled=true;
		}else if(document.frmIntRecibidas.txtReferencia.value.length <= 0 ){
			document.frmIntRecibidas.txtCveRastreo.disabled=false;
			document.frmIntRecibidas.bancoBN.disabled=false;
		}
	}

	function js_cveRastreo(){
		if(document.frmIntRecibidas.txtCveRastreo.value.length > 0 ){
			document.frmIntRecibidas.txtReferencia.value = '';
			document.frmIntRecibidas.txtReferencia.disabled=true;
		}else if(document.frmIntRecibidas.txtCveRastreo.value.length <= 0 ){
			document.frmIntRecibidas.txtReferencia.disabled=false;
		}
	}


	function Actualiza()
	 {
	     if(Indice==0)
			document.frmIntRecibidas.fecha1.value=Fecha[Indice];
	 }

	var ctaselec;
	var ctadescr;
	var ctatipre;
	var ctatipro;
	var ctaserfi;
	var ctaprod;
	var ctasubprod;
	var tramadicional;
	var cfm;

	function PresentarCuentas()
    {
        msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
        msg.focus();
    }

	function actualizacuenta()
    {
        document.frmIntRecibidas.EnlaceCuenta.value=ctaselec+"@"+ctatipre+"@"+ctadescr+"@";
        document.frmIntRecibidas.textEnlaceCuenta.value=ctaselec+" "+ctadescr;
    }
    function EnfSelCta()
    {
        if( document.frmIntRecibidas.EnlaceCuenta.value.length<=0 )
            document.frmIntRecibidas.textEnlaceCuenta.value="";
    }

	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_findObj(n, d) { //v3.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && d.getElementById) x=d.getElementById(n); return x;
	}

	function MM_preloadImages() { //v3.0
	 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	function MM_openBrWindow(theURL,winName,features) { //v2.0
	  window.open(theURL,winName,features);
	}
	/*funcion para inhabilitar el click derecho*/
	   document.oncontextmenu = function() {
	      return false;
	   }
	   function right(e) {

	      if (navigator.appName == 'Netscape' && e.which == 3) {

	         return false;
	      }
	      else if (navigator.appName == 'Microsoft Internet Explorer' && event.button==2) {
	      return false;
	      }
	   return true;
	}
	/*Funcion para mostrar los calendarios*/
	function js_calendario(ind){
		var m = new Date();
		Indice = ind;
	    n = m.getMonth();
	    msg = window.open("/EnlaceMig/EI_CalendarioLn.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
	    msg.focus();
	}

	<%String FechaHoy="";
	 FechaHoy=(String)request.getAttribute("FechaHoy");
	 if(FechaHoy==null)
	    FechaHoy="";%>

	//Indice Calendario
		var Indice=0;


	//Arreglos de fechas
	<%if(request.getAttribute("VarFechaHoy")!= null)
			out.print(request.getAttribute("VarFechaHoy"));%>

	//Dias inhabiles
		diasInhabiles='<% if(request.getAttribute("DiasInhabiles")!= null) out.print(request.getAttribute("DiasInhabiles")); %>';

	<%
		String DiaHoy="";
		if(request.getAttribute("DiaHoy")!= null)
	 		DiaHoy=(String)request.getAttribute("DiaHoy");
	%>

	<%= request.getAttribute("newMenu") %>
</script>

<script type="text/JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="cargaListas('listSpid');MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

	<table width="571" border="0" cellspacing="0" cellpadding="0">
	  <tr valign="top">
	    <td width="*">
	        <%=request.getAttribute("MenuPrincipal")%>
	    </td>
	  </tr>
	</table>

	<table width="760" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td width="676" valign="top" align="left">
	      <table width="666" border="0" cellspacing="6" cellpadding="0">
	        <tr>
	          <td width="528" valign="top" class="titpag"><%=request.getAttribute("Encabezado")%></td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	</table>

	<form  NAME="frmIntRecibidas" METHOD="Post" ACTION="">

  <%
   	InterbancariasRecibidasBeanUSD resultado = (InterbancariasRecibidasBeanUSD) request.getAttribute("resultadoConsulta");
   	if (resultado.getLstInterbancarias().size() == 0) {
  %>

	   <table width="760" border="0" cellspacing="0" cellpadding="0">
	    <tr>
		 <td align="center">
		    <table border="0" cellspacing="2" cellpadding="3">
			 <tr>
			   <td class="tittabdat" colspan="2" nowrap> * Indicar la Referencia � Clave de Rastreo</td>
			 </tr>
			 <tr align="center">
			   <td class="textabdatcla" valign="top" colspan="2">
				<table width="400" border="0" cellspacing="0" cellpadding="0">
				 <tr valign="top">
				  <td width="420" align="left">

	               <table width="430" border="0" cellspacing="0" cellpadding="5">
	               	<tr>
	                 <td align="right" class="tabmovtexbol">* Fecha transferencia:<br></br></td>
					 <td class="tabmovtex" valign="middle"><input type="text" name="fecha1" size="12" class="tabmovtexbol" readonly="readonly" OnFocus = "blur();" value="<%=request.getAttribute("fechaActual")%>"/><a href="javascript:js_calendario(0);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" style="vertical-align:middle;"/></a></td>
					</tr>
				    <tr>
					 <td align="right" class="tabmovtexbol" nowrap>* Cuenta:</td>
					 <td width="290">
			           <input type="text" name="textEnlaceCuenta"  class="tabmovtexbol" maxlength="22" size="22" onfocus="blur();" value=""/>
			      		<a href="javascript:PresentarCuentas();"><img src="/gifs/EnlaceMig/gbo25420.gif" width="12" height="14" border="0" align="absmiddle"></a>
			           <input type="hidden" name="EnlaceCuenta" value=""/>
			         </td>
					</tr>
					<tr>
			         <td align="right" class="tabmovtexbol" nowrap>* Referencia:</td>
			         <td><input type="text" name="txtReferencia" size="12" class="tabmovtexbol" onkeyup="javascript:js_referencia();" onblur="javascript:js_referencia();" maxlength="12"/></td>
			        </tr>
			        <tr>
			         <td align="right" class="tabmovtexbol" nowrap>* Clave de rastreo:</td>
			         <td><input type="text" name="txtCveRastreo" size="12" class="tabmovtexbol" onkeyup="javascript:js_cveRastreo();" onblur="javascript:js_cveRastreo();" maxlength="30"/></td>
			        </tr>


			         <!-- *************** ACTUALIZANDO FUNCIONALIDAD *************** -->

			        <tr>
						<td align="right" class="tabmovtexbol" nowrap>* Banco ordenante:</td>
							<%-- FSW TCS IJVA Se agregan hiddens para carga de listas spid --%>
				         	<input type="hidden" id="listSpid" value="<%=bancoSpid%>" />
						<td>
						   <SELECT id="bancoBN" NAME="bancoBN" class="componentesHtml"></SELECT>
						</td>
					</tr>

					<!-- *************** ACTUALIZANDO FUNCIONALIDAD *************** -->


			       </table>
	              </td>
				 </tr>
				</table>
			   </td>
	          </tr>
	        </table>
	        <br></br>
			<table width="200" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="25">
				<tr id="enviar">
					<td align="right" height="25"><a href="javascript:js_consultar();"><img border="0" name="Enviar" src="/gifs/EnlaceMig/gbo25220.gif" width="90" height="22" alt="Consultar"></a></td>
				    <td align="left"  height="25"><a href="javascript:FrmClean();"><img name="limpiar" border="0" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar"/></a></td>
			    </tr>
			</table>
		 </td>
	    </tr>

	  </table>
	<%
	}else{
	%>
	  <table width="500" border="0" cellspacing="1" cellpadding="2" class="tabfonbla">
	  	<tr>
			<td align="left" class="tabmovtex" colspan="11">
				<b>Resultado de la consulta para la cuenta ${pCuenta} </b><br/>
			</td>
		</tr>
		<tr>
			<td align="center" width="90" class="tittabdat">N&uacute;mero de referencia</td>
			<td align="center" width="150" class="tittabdat">Referencia interbancaria</td>
			<td align="center" width="100" class="tittabdat">Cuenta ordenante</td>
			<td align="center" width="150" class="tittabdat">Nombre del ordenante</td>
			<td align="center" width="80" class="tittabdat">RFC del ordenante</td>
			<td align="center" width="80" class="tittabdat">Banco ordenante</td>

			<td align="center" width="70" class="tittabdat">Importe</td>
			<td align="center" width="70" class="tittabdat">Estatus</td>
			<td align="center" width="150" class="tittabdat">Fecha y hora de confirmaci&oacute;n</td>
			<td align="center" width="70" class="tittabdat">Clave de rastreo</td>
			<td align="center" width="152" class="tittabdat">Concepto de pago/transferencia</td>
			<td align="center" width="150" class="tittabdat">Comprobante Electr&oacute;nico</td>
		</tr>
		<%
		Iterator iterator = resultado.getLstInterbancarias().iterator();
		boolean band = false;
		int i=0;
		while (iterator.hasNext()) {
		InterbancariasRecibidasBeanUSD registro = (InterbancariasRecibidasBeanUSD) iterator.next();
		String sClass = band ? "textabdatcla" : "textabdatobs";
		%>
		<tr>
			<td class="<%=sClass%>" nowrap align="center"><%= registro.getRefTransfer() %></td>
			<td class="<%=sClass%>" nowrap align="center"><%= registro.getRefPago() %></td>
			<td class="<%=sClass%>" nowrap align="center"><%= registro.getNumCtaOrdenante() %></td>
			<td class="<%=sClass%>" nowrap align="right"><%= registro.getNomOrdenante() %></td>
			<td class="<%=sClass%>" nowrap align="left"><%= registro.getRfcOrdenante() %></td>
			<td class="<%=sClass%>" nowrap align="left"><%= registro.getNomBanxLargOrdenante() %></td>
			<td class="<%=sClass%>" nowrap align="left"><%= registro.getImpAbono() %></td>
			<td class="<%=sClass%>" nowrap align="left"><%= registro.getEstatus() %></td>
			<td class="<%=sClass%>" nowrap align="left"><%= registro.getFechaHrConfirmacion() %></td>
			<td class="<%=sClass%>" nowrap align="left"><%= registro.getCveRastreo() %></td>
			<td class="<%=sClass%>" nowrap align="left"><%= registro.getConceptoPagoTransfer() %></td>
			<td class="<%=sClass%>" nowrap align="left"><%= registro.getUrl() %></td>
		</tr>
		<%
			band = !band;
			i++;
		}
		%>
		<tr>
			<td colspan="11" align="center">
				<a href="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" border="0" alt="Imprimir" width="80" height="22"/></a>
				<a href="javascript:js_exportar();"><img src="/gifs/EnlaceMig/gbo25230.gif"  border="0" alt="Exportar" width="80" height="22"/></a>
				<a href="javascript:js_regresar();"><img src="/gifs/EnlaceMig/gbo25320.gif" border="0" alt="Regresar" width="80" height="22"/></a>
			</td>
		</tr>
	  </table>
	<%
	}
	%>

	</form>
</body>
</html>