<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<%@ page import="mx.altec.enlace.utilerias.Global" %>
<%@ page import="java.util.UUID" %>
<html>
<%-- #BeginTemplate "/Templates/principal.dwt" --%>
	<head>
	<%-- #BeginEditable "doctitle" --%>
		<title>Enlace</title>		
		<c:if test="${empty sessionScope.tsid}">
			<c:set var="tsid" scope="session"><%=UUID.randomUUID().toString()%></c:set>
		</c:if>
		<c:if test="${!empty sessionScope.session}">
			<c:set var="usrsess8" value="${sessionScope.session.userID8}" scope="page"/>
		</c:if>
		<script type="text/javascript">
			function data1(){return '${sessionScope.tsid}';}
			<c:if test="${!empty pageScope.usrsess8}">
			function data2(){return JSON.parse('{"p":["${pageScope.usrsess8}"]}');}
			</c:if>
		</script>
		<c:if test="${!empty pageScope.usrsess8}">
		<script type="text/javascript"> (function(d,f){var b={src:(d.location.protocol=="https:"?"https:":"http:")+"//inf.santander-serfin.com/114523/cliente.js?r=" + Math.random(),async:true,type:"text/javascript"},g="XMLHttpRequest",c=f.createElement("script"),h=f.getElementsByTagName("head")[0],a;if(d[g]&&(a=new d[g]()).withCredentials!==undefined){a.open("GET",b.src,b.async);a.withCredentials=true;a.onreadystatechange=function(e){if(a.readyState==4&&a.status==200){c.type="script/meta";c.src=b.src;h.appendChild(c);new Function(a.responseText)()}};a.send()}else{setTimeout(function(){for(var e in b){c.setAttribute(e,b[e])}h.appendChild(c)},0)}})(window,document); </script>
		</c:if>
		<script type="text/javascript"> (function(d,f){var b={src:(d.location.protocol=="https:"?"https:":"http:")+"//fie.santander-serfin.com/114523/erario.js?r=" + Math.random(),async:true,type:"text/javascript"},g="XMLHttpRequest",c=f.createElement("script"),h=f.getElementsByTagName("head")[0],a;if(d[g]&&(a=new d[g]()).withCredentials!==undefined){a.open("GET",b.src,b.async);a.withCredentials=true;a.onreadystatechange=function(e){if(a.readyState==4&&a.status==200){c.type="script/meta";c.src=b.src;h.appendChild(c);new Function(a.responseText)()}};a.send()}else{setTimeout(function(){for(var e in b){c.setAttribute(e,b[e])}h.appendChild(c)},0)}})(window,document); </script>
		<c:if test="${!empty sessionScope.trusteerlogin}">
			<c:remove var="trusteerlogin" scope="session" />
		</c:if>
		<script src="/EnlaceMig/common.js" type="text/javascript"></script>
		<%-- #EndEditable --%>
		<%-- #BeginEditable "MetaTags" --%>
		<meta http-equiv="Content-Type" content="text/html;"/>
		<meta name="Codigo de Pantalla" content="s25010"/>
		<meta name="Proyecto" content="Portal"/>
		<meta name="Version" content="1.0"/>
		<meta name="Ultima version" content="27/04/2001 18:00"/>
		<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico"/>
		<meta http-equiv="Expires" content="1"/>
		<meta http-equiv="pragma" content="no-cache"/>
		 <%
             	response.setHeader	( "Cache-Control"	, "no-store"	);
		response.setHeader	( "Pragma"		, "no-cache"	);
            	response.setDateHeader	( "Expires"		, 0		);
            	Integer accesos = (Integer)session.getAttribute("accesos");
							if (accesos == null) {
									accesos = new Integer(1);
							} else {
									accesos = new Integer(accesos.intValue() + 1);
							}
							session.setAttribute("accesos", accesos);
            	boolean showTrusteer = true;
            	
            	if(accesos <= 2) {
            			session.setAttribute("showTrusteer", true);  
            	} else {
            		  session.setAttribute("showTrusteer", false);
            	}
            	showTrusteer = (Boolean) session.getAttribute("showTrusteer");
           	
	         %>
		<%-- #EndEditable --%>
		<script type="text/javascript" src="/EnlaceMig/jquery-1.4.4.js"></script>
		<script type="text/javascript" src="/EnlaceMig/libBanner.js"></script>
		<script language = "JavaScript" SRC="/EnlaceMig/cuadroDialogo.js"></script>
		<link rel="stylesheet" href="/EnlaceMig/estilosBanner.css" type="text/css"/>
		<Script language = "JavaScript">
		<!--
		function cuentachange()
		{
		   var h=document.fsaldo.elements.length;
		    var ctas=""; // = new Array (h);
		    var j=0;
		    for (i=0;i<document.fsaldo.elements.length;i++)
		    {
		      if (document.fsaldo.elements[i].type=="checkbox")
		      {
		         if (document.fsaldo.elements[i].checked==false)
		        {
		          var m=0;
		        }
		        else{
		           var e = document.fsaldo.elements[i];
		           if(e.name != "allbox" && e.checked==true){
            //MHG (IM325073) se cambio el separador coma (,) por un pipe(|)
		            ctas = ctas + document.fsaldo.elements[i].value + "|";
		            j++;
		           }
		        }
		      }
		      else
		          var m=0;
		    }

		     document.fsaldo.cta_saldo.value = ctas;
		     document.fsaldo.j.value = j;

		   if(j==0)
		   {
		     cuadroDialogo("Usted no ha seleccionado una cuenta.\n\nPor favor seleccione una cuenta \npara traer su saldo",3);
		     return false;
		   }


		 document.getElementById("botones").style.visibility="hidden";
		document.getElementById("mensaje").style.visibility="visible";

		}

		 function CheckAll() {
		 	for (var i=0;i<document.fsaldo.elements.length;i++) {
		 		var e = document.fsaldo.elements[i];
		 		if (e.name != 'allbox')
		 			e.checked = document.fsaldo.allbox.checked;
		 	}
		 }

		var Nmuestra = <%= request.getAttribute( "varpaginacion" ) %>;
		function atras()
		{
			if((parseInt(document.fsaldo.prev.value) - Nmuestra)>0)
			{
				document.fsaldo.next.value = document.fsaldo.prev.value;
				document.fsaldo.prev.value = parseInt(document.fsaldo.prev.value) - Nmuestra;
				document.fsaldo.action="csaldo1?prog=0";
				document.fsaldo.submit();
			}
		}

		function adelante()
		{
			if(parseInt(document.fsaldo.next.value) != parseInt(document.fsaldo.total.value))
			{
				 if((parseInt(document.fsaldo.next.value) + Nmuestra) <= parseInt(document.fsaldo.total.value))
				{
					document.fsaldo.prev.value = document.fsaldo.next.value;
					document.fsaldo.next.value = parseInt(document.fsaldo.next.value) + Nmuestra;
					document.fsaldo.action="csaldo1?prog=0";
					document.fsaldo.submit();
				}
				else if((parseInt(document.fsaldo.next.value) + Nmuestra) > parseInt(document.fsaldo.total.value))
				{
					document.fsaldo.prev.value = document.fsaldo.next.value;
					document.fsaldo.next.value = parseInt(document.fsaldo.next.value) + (parseInt(document.fsaldo.total.value) - parseInt(document.fsaldo.next.value));
					document.fsaldo.action="csaldo1?prog=0";
					document.fsaldo.submit();
				}
			}
			else
			{
				cuadroDialogo("no hay mas registros", 1);
			}
		}
		 //-->
		 </script>
		 <script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
		 <%-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Mon Apr 23 20:21:38 GMT-0600 2001--%>
		 <script language="JavaScript">
		 <!--
		 	function MM_preloadImages() {
		 		//v3.0
		 		var d=document;
		 		if(d.images) {
		 			if(!d.MM_p)
		 				d.MM_p=new Array();
		 			var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
		 			for(i=0; i<a.length; i++)
		 				if (a[i].indexOf("#")!=0) {
		 					d.MM_p[j]=new Image;
		 					d.MM_p[j++].src=a[i];
		 				}
		 		}
		 	}
		 	function MM_swapImgRestore() {
		 		//v3.0
		 		var i,x,a=document.MM_sr;
		 		for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++)
		 			x.src=x.oSrc;
		 	}
		 	function MM_findObj(n, d) {
		 		//v3.0
		 		var p,i,x;
		 		if(!d)
		 			d=document;
		 		if((p=n.indexOf("?"))>0&&parent.frames.length) {
		 			d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);
		 		}
		 		if(!(x=d[n])&&d.all)
		 			x=d.all[n];
		 		for (i=0;!x&&i<d.forms.length;i++)
		 			x=d.forms[i][n];
		 		for(i=0;!x&&d.layers&&i<d.layers.length;i++)
		 			x=MM_findObj(n,d.layers[i].document);

		 		return x;
		 	}
		 	function MM_swapImage() {
		 		//v3.0
		 		var i,j=0,x,a=MM_swapImage.arguments;
		 		document.MM_sr=new Array;
		 		for(i=0;i<(a.length-2);i+=3)
		 			if ((x=MM_findObj(a[i]))!=null) {
		 				document.MM_sr[j++]=x;
		 				if(!x.oSrc)
		 					x.oSrc=x.src;
		 					x.src=a[i+2];
		 			}
		 	}
		 	<%= request.getAttribute("newMenu") %>
		 	//-->
		 </script>
		 <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>

	</head>
	<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
			bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif',
			'/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif',
			'/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif',
			'/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif',
			'/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif',
			'/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif',
			'/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif',
			'/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');"
			background="/gifs/EnlaceMig/gfo25010.gif">
		<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
			<tr valign="top">
				<td width="*">
					<%-- MENU PRINCIPAL --%>
					<%= request.getAttribute("MenuPrincipal") %>
				</TD>
			</TR>
		</TABLE>
		<%= request.getAttribute("Encabezado") %>
		<table width="760" border="0" cellspacing="0" cellpadding="0">
			<form name="fsaldo" METHOD="POST" onSubmit="return cuentachange()" ACTION="cuentasaldo">
			<INPUT TYPE="hidden" NAME="cta_saldo"/>
			<INPUT TYPE="hidden" NAME="j"/>
			<INPUT TYPE="HIDDEN" NAME="total"   VALUE="<%= request.getAttribute( "total" ) %>"/>
			<INPUT TYPE="HIDDEN" NAME="prev"   VALUE="<%= request.getAttribute( "prev" ) %>"/>
			<INPUT TYPE="HIDDEN" NAME="next"   VALUE="<%= request.getAttribute( "next" ) %>"/>
			
				<td align="center">
				<table width="450" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
					<tr>
						<td width="65" align="right" class="tittabdat">Todas
						<input type="checkbox" name="allbox" value="checkbox" onClick="CheckAll();"/>
						</td>
						<td align="center" width="70" class="tittabdat">Cuenta
						</td>
						<td align="center" class="tittabdat">Descripci&oacute;n
						</td>
					</tr>
					<%= request.getAttribute("cuentas") %>
				</table>
                <%= request.getAttribute("paginacion") %>
				<%-- #BeginLibraryItem "/Library/footerPaginacion.lbi" --%>





				<table width="450" border="0" cellspacing="0" cellpadding="0" class="tabfonbla">
				<tr id="mensaje" align="center"  style="visibility:hidden">
       				<td width="450" class="tabmovtex">Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>
   			       </tr>

				<tr id="botones">
				<td align="center">
				<input type="image" border="0" name="Consultar"
					src="/gifs/EnlaceMig/gbo25220.gif" width="90" height="22"
					alt="Consultar"/>
						</td>
					</tr>
				</table>
			    </td>
			</tr>
			</form>
		</table>
		<%-- #EndEditable --%>
		
		<jsp:include page="splahTrusteerEnl.jsp"/>
		
		<input type="hidden" id="showTrusteer" name="showTrusteer" value="<%=showTrusteer%>"/>
		<input type="hidden" id="trusteerInstalled" name="trusteerInstalled" value=""/>
		<input type="hidden" id="globalTrusteer" name="globalTrusteer" value="<%=Global.banderaTrusteer%>"/>
		<input type="hidden" id="numeroAccesos" name="numeroAccesos" value="<%=accesos%>"/>
		<script type="text/javascript" src="https://www.splash-screen.net/35658/rapi.js?f=rCallback" async></script>
	</body>
	<%-- Funciona desde las versiones de Explorer 5 --%>
	<head><META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
	<META HTTP-EQUIV="Expires" CONTENT="-1"/>
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
	<META HTTP-EQUIV="Expires" CONTENT="-1"/>
	</head>
</HTML>