<%@ page contentType="text/html;charset=windows-1252"%>
<%@ page import="javax.servlet.*"%>
<%@ page import="javax.servlet.http.*"%>
<html>
<head>
<title>Banca Virtual</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">
<!-- Javascript del APP -->
 <SCRIPT PURPOSE="Include" SRC="/EnlaceMig/validatePWD.js"></SCRIPT>
<!-- JavaScript del App -->

<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script language = "JavaScript">
function nuevoAjax(){
	var xmlhttp=false;
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
			xmlhttp = false;
		}
	}
	try {
		if (!xmlhttp && typeof XMLHttpRequest!="undefined") {
			xmlhttp = new XMLHttpRequest();
		}

	} catch (E) {
	}

	return xmlhttp;
}

function cargarContenido(){
	try {
		ajax=nuevoAjax();
		ajax.open("GET", "cmdpasswd?bitacora=si",true);
		ajax.onreadystatechange=function() {}
		ajax.send(null)
	} catch (E) {

	}
}

cargarContenido();
<!--
function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

<%= request.getAttribute("newMenu") %>

function esNumero(campo, nombreCampo)
{
	tmp = campo.value;
	var numeros="0123456789.";

	for (i = 0; i<tmp.length; i++)
	{
		car=tmp.charAt(i)
		if (numeros.indexOf(car)==-1)
		{
			campo.value="";
			campo.focus();
			cuadroDialogo("La contrase&#241;a debe ser num&#233;rica", 3);
			return false
		}
	}
	return true
}

function isInteger (s)
{
	var i

	for (i = 0; i < s.length; i++)
	{
		var c = s.charAt(i)
		if (!isDigit(c))
		return false
	}
	return true
}

function isDigit (c)
{
	return ((c >= "0") && (c <= "9"))
}

function validClave (user)
{
	if (user == "")
	{
		document.Flogin.clave.focus()
		cuadroDialogo ("Favor de teclear su Contrase&#241;a Actual.", 1)
		result = false
	} else {
		if (user.length != 5)
		{
			return true
		}
		else
			result = true
	}
	return result
}

function completeClave (pwd)
{
	var result

	if (pwd.length > 0)
	{
		for (i = pwd.length; i < 5; i++)
		pwd = "0" + pwd
	}
	return pwd
}

function completeClaveNva (pwd)
{
	var result

	if (pwd.length > 0)
	{
		for (i = pwd.length; i < 4; i++)
		pwd = "0" + pwd
	}
	return pwd
}

function aceptar()
{
	var result;
	document.Flogin.action = "cmdpasswd";
	result = validClave(document.Flogin.clave.value);
	if (result == true)
	{
		result = validClavenva(document.Flogin.clavenva.value,document.Flogin.clave.value);
		if (result == true)
		{
			result=validClaveconf(document.Flogin.claveconf.value);
			if (result == true)
			{
				result=true
			}
		}
	}

	if (result)
	{
		if(document.Flogin.clavenva.value == document.Flogin.claveconf.value)
		{
			document.Flogin.submit();
		} else {
			document.Flogin.claveconf.value = "";
			document.Flogin.claveconf.focus();
			cuadroDialogo("Confirmaci&oacute;n de Contrase&#241;a incorrecto", 3);
		}
	}
}


function validaFormato (user)
{
	var TemplateF=/^[a-z\d]{0,20}$/i;  //dominio de a-z, A-Z, 0-9
	return TemplateF.test(user);  //Compara "pwd" con el formato "Template"
}

function validClavenva (user, user_viejo)
{
	if (!validaFormato (user))
	{
		document.Flogin.clavenva.value = "";
		document.Flogin.claveconf.value = "";
		document.Flogin.clavenva.focus()
		cuadroDialogo ("La nueva Contrase&#241;a no es valida.", 1)
		return false
	}

	if (user == "")
	{
		document.Flogin.clavenva.focus()
		cuadroDialogo ("\n Favor de teclear su Contrase&#241;a Nueva.", 1)
		result = false
	} else {
		if (user.length != 4)
		{
			result = true

		}
		else
			result = true
	}

	if (user == user_viejo.substring(1))
	{
		document.Flogin.clavenva.focus()
		document.Flogin.clavenva.value = ""
		document.Flogin.claveconf.value = ""
		cuadroDialogo ("\n Requiere teclear Contrase&#241;a Nueva diferente a Actual.", 1)
        result = false
	}

	return result
}

function validClaveconf (user)
{
	// regresa false si el campo esta en blanco
	if (user == "")
	{
		document.Flogin.claveconf.focus()
		cuadroDialogo ("\n Favor de Confirmar su Contrase&#241;a Nueva.", 1)
		result = false
	}
	else
		result = true
	return result
}

//-->
//<VC proyecto="200710001" autor="JGR" fecha="02/07/2007" descripción="CAMBIO DE CONTRASENA">
	function setLocationCambiaPass(){
	// vswf:meg cambio de NASApp por Enlace 08122008
		myFrame = document.getElementById("cambiaPasswordId");
		myFrame.src='<%=mx.altec.enlace.utilerias.Global.REDIR_CAMBIAR_PASSWORD%>';
	}
//</VC>
</script>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<!--<VC proyecto="200710001" autor="TIB" fecha="12/06/2007" descripción="CAMBIO DE CONTRASENA">-->
<%

String paramVal = (String)request.getAttribute("pwdSAM");

if (paramVal!=null) {
%>

<frameset onload="setLocationCambiaPass()">
   <frame id="cambiaPasswordId">
</frameset>

<%
} else {
%>
<!--</VC>-->
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="596" valign="top">
      <table width="596" border="0" cellspacing="0" cellpadding="0" class="tabfonbla">
        <tr valign="top">
          <td rowspan="2"><img src="/gifs/EnlaceMig/glo25010.gif" width="237" height="41"></td>
          <td rowspan="2"><a href="javascript:;" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contactanos','','/gifs/EnlaceMig/gbo25111.gif',1)"><img src="/gifs/EnlaceMig/gbo25110.gif" width="30" height="33" name="contactanos" border="0"></a></td>
          <td rowspan="2"><img src="/gifs/EnlaceMig/gbo25120.gif" width="59" height="20" alt="Cont&aacute;ctenos"></td>
          <td rowspan="2"><a href="javascript:;" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('atencion','','/gifs/EnlaceMig/gbo25131.gif',1)"><img src="/gifs/EnlaceMig/gbo25130.gif" width="36" height="33" name="atencion" border="0"></a></td>
          <td rowspan="2"><img src="/gifs/EnlaceMig/gbo25140.gif" width="90" height="20" alt="Atenci&oacute;n telef&oacute;nica"></td>
          <td rowspan="2"><a href="javascript:;" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('centro','','/gifs/EnlaceMig/gbo25151.gif',1)"><img src="/gifs/EnlaceMig/gbo25150.gif" width="33" height="33" name="centro" border="0"></a></td>
          <td rowspan="2"><img src="/gifs/EnlaceMig/gbo25160.gif" width="93" height="20" alt="Centro de mensajes"></td>
          <td width="18" bgcolor="#DE0000"><img src="/gifs/EnlaceMig/gau25010.gif" width="18" height="17"></td>
        </tr>
        <tr valign="top">
          <td><img src="/gifs/EnlaceMig/gau25010.gif" width="18" height="24"></td>
        </tr>
      </table>
</table>



<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
    <!-- MENU PRINCIPAL -->
      <%
		String MenuPrincipal = "";
		HttpSession session_cn = request.getSession();
		MenuPrincipal =(String) request.getAttribute("MenuPrincipal");
		if ( MenuPrincipal == null )
			MenuPrincipal = "";
      %>
      <%//= MenuPrincipal %>
    </TD>
  </TR>
</TABLE>

<%
	String Encabezado = "";
	Encabezado =(String) request.getAttribute("Encabezado");
	if ( Encabezado == null )
		Encabezado = "";
%>
<%= Encabezado %>
<%
        String v_msg_cambio_nip=" ";
        v_msg_cambio_nip = (String)session_cn.getAttribute("msg_cambio_nip");
        if (v_msg_cambio_nip == null)
        {
          v_msg_cambio_nip=" ";
        }
        session_cn.setAttribute("msg_cambio_nip",null);
%>
<table width="775"  border="0">
  <tr align="center">
     <td class=>
        <FONT SIZE=3 FACE=ARIAL COLOR=black > <%=v_msg_cambio_nip%> </FONT>
     </td>
  </tr>
</table>
    <!-- CONTENIDO INICIO -->


<br>
<table width="644" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
	<td>
	  <%
		String ContUser = "";
		ContUser = (String) request.getAttribute("ContUser");
		if ( ContUser == null )
			ContUser = "";
	  %>
	  <%//=ContUser %>

<FORM NAME="Flogin" METHOD="POST"  ACTION = "">
<table width="760" border="0" cellspacing="0" cellpadding="0">
  <form name="form1" method="post" >
    <tr>
      <td align="center">
        <table width="260" border="0" cellspacing="2" cellpadding="3">
          <tr align="left">
            <td class="tittabdat"> Capture los datos</td>
          </tr>
          <tr align="center" valign="middle">
            <td class="textabdatcla" height="35">
              <table border="0" cellspacing="5">
                <tr>
                  <td class="tabmovtex" align="right" nowrap>Contrase&ntilde;a actual:</td>
                  <td class="tabmovtex" width="100%">
                    <INPUT TYPE="PASSWORD" NAME="clave" SIZE="22" MAXLENGTH="20" FILEREFS="validate.js" class="tabmovtex">
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex" align="right" nowrap>Nueva contrase&ntilde;a:</td>
                  <td class="tabmovtex">
                    <INPUT TYPE="PASSWORD" NAME="clavenva" SIZE="22" MAXLENGTH="20" FILEREFS="validate.js" class="tabmovtex">
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex" align="right" nowrap>Confirmaci&oacute;n contrase&ntilde;a:</td>
                  <td class="tabmovtex">
					<INPUT TYPE="PASSWORD" NAME="claveconf" SIZE="22" MAXLENGTH="20" FILEREFS="validate.js" class="tabmovtex">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
		        <tr align="center">
                  <td class="tittabdat">Su nueva contrase&ntilde;a debe ser alfanum&eacute;rica
                    de 8 a 20 caracteres.</td>
          </tr>
        </table>
        <br>
        <table border="0" cellspacing="0" cellpadding="0" class="tabfonbla">
          <tr>
            <td width="87">
			  <a href="javascript:aceptar();">
			  <img border="0" name="imageField3" src="/gifs/EnlaceMig/gbo25450.gif" width="76" height="22" alt="Cambiar">
            </td>
            <td width="76">
			  <a href="javascript:limpiar();">
			  <img border="0" name="imageField" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar">
            </td>
          </tr>
        </table>
        <br>
      </td>
    </tr>
  </form>
</table>
</FORM>

    </td>
  </tr>
</table>
<!-- CONTENIDO FINAL -->
</body>
<!--<VC proyecto="200710001" autor="TIB" fecha="12/06/2007" descripción="CAMBIO DE CONTRASENA">-->
<%} %>
<!--</VC>-->
</html>
<Script language = "JavaScript">
<!--
function limpiar()
{
	document.Flogin.clave.value = "";
	document.Flogin.clavenva.value = "";
	document.Flogin.claveconf.value = "";
	document.Flogin.clave.focus();
}

<%= request.getAttribute("MensajeCambioPass") %>
//-->
</Script>