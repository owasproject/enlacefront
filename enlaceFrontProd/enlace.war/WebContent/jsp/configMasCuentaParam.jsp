<%@ page import="java.util.List, javax.servlet.http.HttpServletRequest" %>
<%@ page import="java.util.ArrayList, mx.altec.enlace.beans.ConfEdosCtaArchivoBean" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Configura Estatus de Envio de Estado de Cuenta</title>

<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>

<link href="/EnlaceMig/consultas.css" rel="stylesheet" type="text/css" id = "estilos"/>

<style type="text/css">
	.pag_btn {
		border: solid 1px;
		border-color: #A4BEE4;
		color: rgb(0, 0, 255);		
		background-color: #A4BEE4;			
	}
	.pag_btn span {
		text-decoration:none;
		font-family: Arial, Helvetica, sans-serif; 
		font-size: 10pt; 
		font-weight: bold; 
		color : rgb(255,255,255);
	}
	.pag_btn_des{
		border: solid 1px;
		border-color: rgb(200, 200, 200);
		color: rgb(200, 200, 200);
		background-color: rgb(245,245,245);
	}
	.pag_num {
		border: solid 1px;
		border-color: #A4BEE4;
		color: rgb(255,255,255);
		background-color: #A4BEE4;
	}
	.sobmbraUnload{
		display:none;
		top:0px;
		left:0px;
		width:0px;
		height:0px;
	}
	.sombraLoad{
	   display: block;
	   position: absolute;
	   top: 0%;
	   left: 0%;
	   width: 100%;
	   height: 100%;
	   background-color:gray;
	   z-index:1000;
	   opacity:0.9;
	   filter:alpha(opacity=70);
	}
	.windowUnload{
		display:none;
		top:0px;
		left:0px;
		width:0px;
		height:0px;
	}
	.windowLoad{
		display:block;
		position:absolute;
		background-color:rgb(234,234,234);
		border:rgb(119,119,119) 3px solid;
		top:15%;
		left:25%;
		width:700px;
		height:400px;
		z-index:1001;
	   /*-moz-border-radius: 10px;
		-webkit-border-radius: 10px;
		behavior:url(border-radius.htc);
		*/
	    text-align: left;
	  }
	.encabezado{
	    width:700px;
		height:10px;
		background-color:rgb(119,119,119);
		font-family:arial;
		font-weight:bold;
		font-size:9pt;
		color:white;
	}
	.titulo{
		font-weight:bold;
		font-family:arial;
		font-size:11pt;
	}
	.tabla{
		width:550px;
	   	font-family:arial;
		font-size:8pt;
	}
	.boton{
		font-size:8pt;
		width:60px;
	}
	.texto{
		margin:20px 20px 20px 20px;
		text-align:justify;
	}
	.textbox{
		font-family:arial;
		font-size:9pt;
		text-align:left;
		width:500px;
		height:150px;
	}
	
	.pag_btn {
		border: solid 1px;
		border-color: #A4BEE4;
		color: rgb(0, 0, 255);		
		background-color: #A4BEE4;			
	}
	.pag_btn a {
		text-decoration:none;
		font-family: Arial, Helvetica, sans-serif; 
		font-size: 10pt; 
		font-weight: bold; 
		color : rgb(255,255,255);
	}
	.pag_btn_des{
		border: solid 1px;
		border-color: rgb(200, 200, 200);
		color: rgb(200, 200, 200);
		background-color: rgb(245,245,245);
	}
	.pag_num {
		border: solid 1px;
		border-color: #A4BEE4;
		color: rgb(255,255,255);
		background-color: #A4BEE4;
	}
</style>
<script type="text/javascript">
	Paginador = function (divPaginador, tabla, tamPagina) {
		this.miDiv = divPaginador; 	//div de controles
		this.tabla = tabla;			//tabla a paginar
		this.tamPagina = tamPagina; //tama�o de la pagina
		this.pagActual = 1; 		//Se parte de la pagina 1
		this.paginas = 	Math.floor((this.tabla.rows.length-1)/this.tamPagina); 
		this.SetPagina=function(num) {
			if (num < 0 || num > this.paginas)
				return;
			this.pagActual = num;
			var min = 1 + (this.pagActual -1) * this.tamPagina;
			var max = min + this.tamPagina -1;
			
			for (var i = 1; i<this.tabla.rows.length; i++){
				if (i < min || i > max) {
					this.tabla.rows[i].style.display = 'none';
				} else {
					this.tabla.rows[i].style.display = '';
				};

			}
			this.miDiv.firstChild.rows[0].cells[1].innerHTML = this.pagActual;
		}
		this.Mostrar = function() {
			var tblPaginador=document.createElement('table');
			var fil=tblPaginador.insertRow(tblPaginador.rows.length);
			var ant=fil.insertCell(fil.cells.lenght);
			ant.innerHTML = '<span onmauseover="" style="cursor:pointer;">Anterior</span>';
			ant.className = 'pag_btn';
			var self = this;
			ant.onclick = function() {
				if (self.pagActual == 1) return
				self.SetPagina(self.pagActual -1);
			}
			var num = fil.insertCell(fil.cells.length);
			num.innerHTML = '';
			num.className = 'pag_num';
			var sig=fil.insertCell(fil.cells.length);
			sig.innerHTML = '<span onmauseover="" style="cursor:pointer;">Siguiente</span>';
			sig.className = 'pag_btn';
			sig.onclick = function () {
				if (self.pagActual == self.paginas) return;
				self.SetPagina(self.pagActual + 1);
			}
			this.miDiv.appendChild(tblPaginador);			
			if (this.tabla.rows.length - 1 > this.paginas * this.tamPagina) {
				this.paginas=this.paginas + 1;
			}
			this.SetPagina(this.pagActual);
		};	
	};
	function showContrato() {
				document.getElementById('sombra').className='sombraLoad';
				document.getElementById('window').className='windowLoad';
    }
	function acepto(){
		if (document.getElementById('ac').checked) {
			document.getElementById("cadena").value = generaCadena();
			var accion = document.getElementById("accion").value;
			if(accion == '1'){
				document.getElementById("accion").value = "3";
			};
			cierro();
			document.forms["formulario"].submit();
		} else {
			cuadroDialogo('Estimado cliente, por favor seleccione la casilla de verificaci\u00f3n. \"Acepto las Condiciones\"',1);
		};
	}
	function cierro() {
		document.getElementById('sombra').className='sombraUnload';
		document.getElementById('window').className='windowUnload';
	}		
	function validaRadioButtons() {
		var formRadios=document.getElementById('formulario');
		var grupos = [];
		for (var i=0; elem=formRadios.elements[i]; i++) {
			if (elem.type == "radio") {
				if (!grupos[elem.name]){
					grupos[elem.name] = elem.checked;
				};
			};
		}
		for (grupo in grupos) {
			if (!grupos[grupo]) {
				cuadroDialogo('Para continuar con la operaci\u00f3n es necesario que configure todas las cuentas.',4);
				return false;
			};
		}
		return true;
	}
	function EnviarForma() {
		if (validaRadioButtons()) {
			showContrato();
		}
	}
	function Cancelar() {
		document.getElementById("accion").value = null;
		document.forms["formulario"].submit();	 
	}
	function generaCadena() {
		var elementos = document.getElementById("elementos").value;
		var cadena = "";
		var i = 1;
		for(i; i<=elementos; i++){
			if(i!= 1){
				cadena += "##";
			}
			var cuenta = quitaEspacios(document.getElementById("cuenta"+i).innerHTML);
			var titular = quitaEspacios(document.getElementById("cuenta"+i).innerHTML);
			var paperless = 'n';
			if (document.getElementById("paperless"+i).checked) {
				paperless = 's';
			}
			cadena += cuenta+"#"+titular+"#"+paperless;		
		}
		return cadena;
	}
	
	function quitaEspacios(valor){
		return valor.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	}

	/********************************************************************************/
	/********************************************************************************/
	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}
	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}
	function MM_findObj(n, d) { //v3.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
	}
	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}
	
	/********************************************************************************/
	/********************************************************************************/
	
	<%
	       if (request.getAttribute("newMenu")!= null) {
	       out.println(request.getAttribute("newMenu"));
	       }
	%>
	
</script>


<link href="<%=request.getContextPath()%>/enlace/consultas.css" rel="stylesheet" type="text/css" id = "estilos"/>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor="#ffffff" leftMargin="0" topMargin="0" marginwidth="0" marginheight="0" onLoad="">

<div id="window" class="windowUnload">
<table class="encabezado">
	<tr>
		<td>T&eacute;rminos y condiciones para el servicio paperless</td>
	</tr>
</table>
</br>

<table class="tabla" border="0">
	<tr>
		<td width="20px">&nbsp;</td>
		<td><span class="titulo">Convenio de aceptaci&oacute;n de condiciones para el servicio Paperless</span></td>
	</tr>
	<tr><td colspan>&nbsp;</td></tr>
	<tr>
		<td width="20px">&nbsp;</td>
		<td>
			<textarea class="textbox" readonly="readonly">
CONVENIO DE ACEPTACI&Oacute;N DE CONDICIONES PARA EL SERVICIO DE BLOQUEO DE IMPRESI&Oacute;N DE ESTADO DE CUENTA

Estimado Cliente:
				
En cumplimiento a lo establecido por el C&oacute;digo Fiscal de la Federaci&oacute;n los estados de cuenta podr&aacute;n ser enviados por &eacute;ste medio, por lo que al solicitar su inscripci&oacute;n a &eacute;ste Servicio, usted est&aacute; de acuerdo en no recibir el(los) estado(s) de cuenta impreso(s) en papel a trav&eacute;s de correo de los diferentes productos asociados a su c&oacute;digo de cliente.

Una vez inscrito en el servicio, usted recibir&aacute; la informaci&oacute;n relativa a su estado de cuenta en l&iacute;nea, y &uacute;nicamente tendr&aacute; acceso al mismo a trav&eacute;s de medios electr&oacute;nicos, siguiendo las instrucciones proporcionadas por Banco Santander (M&eacute;xico) S.A., Instituci&oacute;n de Banca M&uacute;ltiple, Grupo Financiero Santander M&eacute;xico.

Usted podr&aacute; acceder a su estado de cuenta electr&oacute;nicamente, siempre que no revoque su consentimiento para recibirlo en l&iacute;nea. Si usted desea volver a recibir su Estado de Cuenta en papel a trav&eacute;s de correo, es necesario que regrese a esta p&aacute;gina y siga las instrucciones que se le indican y su impresi&oacute;n ser&aacute; recibida en el domicilio indicado al mes siguiente que usted haya desactivado la opci&oacute;n de bloqueo de impresi&oacute;n.
Si usted lo desea, podr&aacute; solicitar en cualquiera de nuestras sucursales una copia impresa en papel, de cualquier estado de cuenta con una antig�edad m&aacute;xima no mayor de 24 meses.

En caso que usted requiera cambiar o actualizar la direcci&oacute;n de correo electr&oacute;nico a d&oacute;nde se le enviar&aacute; las notificaciones, realice el cambio a trav&eacute;s de Enlace en la secci&oacute;n: Administraci&oacute;n > Mant. de Datos Personales. En caso que la direcci&oacute;n de correo electr&oacute;nico que usted proporcione sea incorrecta, inv&aacute;lida o si la misma no puede ser registrada y validada por nuestro sistema, usted no recibir&aacute; la notificaci&oacute;n.

Para cualquier duda o aclaraci&oacute;n, puede comunicarse a Superl&iacute;nea Empresarial a los tel&eacute;fonos 5169 4343 o Lada sin costo 01 800 509 5000 a trav&eacute;s de nuestro correo electr&oacute;nico enlace@santander.com.mx  donde uno de nuestros especialistas le brindara el apoyo.   
			</textarea>
		</td>
	</tr>
	<tr><td colspan>&nbsp;</td></tr>
	<tr>
		<td width="20px">&nbsp;</td>
		<td><input type="checkbox" name="ac" id="ac" value="ac" />Acepto las condiciones.</td>
	</tr>
		<tr>
		<td width="20px">&nbsp;</td>
		<td align="center">
			<input type="button" class="boton" value="Continuar" onClick="javascript:acepto();"/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="button" class="boton" value="Cerrar" onClick="javascript:cierro();"/>
		</td>
	</tr>
	<tr><td colspan>&nbsp;</td></tr>
	<tr>
		<td width="20px">&nbsp;</td>
		<td>Por favor lea el convenio de aceptaci&oacute;n de condiciones para el servicio de Paperless. Se requiere que Usted
		    acepte las condiciones para que deje de recibir (o pueda seguir recibiendo) su(s) estado(s) de cuenta de manera 
			impresa en su domicilio. En caso de no estar de acuerdo, podr&aacute; aceptar el cambio posteriormente.
			</td>
	</tr>
</table>

</div>
<div id="sombra" class="sobmbraUnload"></div>

<table border="0px" cellpadding="0px" cellspacing="0px" width="571px">
  <tr valign="top">
   <td width="*">
	<%
           if (request.getAttribute("MenuPrincipal")!= null) {
           		out.println(request.getAttribute("MenuPrincipal"));
           }
    %>
   </td>
  </tr>
</table>
<%
           if (request.getAttribute("Encabezado")!= null) {
           		out.println(request.getAttribute("Encabezado"));
           }
%>

<form  name="formulario" id="formulario" method="post" action="ConfigMasPorCuentaServlet">
	<input type="hidden" id="accion" name="accion" value="<%= request.getAttribute("mostrar") %>" />
	<input type="hidden" id="elementos" name="elementos" value="<%=request.getAttribute("elementos") %>" />
	<input type="hidden" id="cadena" name="cadena" value="" />
  	<table width="760" cellspacing="0" cellpadding="0" border="0">
  		<tbody>
  			<tr>
  				<td align="center">
					<table border="0" align="center" style="text-align:center; width:600px;">
					<tr><td align="center"><span id="paginador"></span></td></tr>
					</table>
					<table border="0" align="center" id="tblDatos" class="tabfonbla" style="text-align:center; width:750px;">
						<thead class="tittabdat">
							<tr><td style="width:110px;">Cuenta</td>
								<td style="width:390px;">Titular</td>
								<td style="width:100px;">Con Paperless</td>
								<td style="width:100px;">Sin Paperless</td></tr>
						</thead>
						<tbody>
									<%
										int cont = 0;
										List lista = new ArrayList();
										lista = (List)request.getAttribute("listaCuentasEdoCta") != null ? (List)request.getAttribute("listaCuentasEdoCta") :
										new ArrayList();
									 	for(int i = 0 ; i < lista.size(); i++) { 
									 	cont++;
									 	int mod = i%2;
										String estilo;
										if(mod == 0) {
											estilo = "textabdatcla";
										} else {
											estilo = "textabdatobs";
										}
						   			%>	
										<tr>
											<td colspan="1" class="<%=estilo %>" align="center"> 
												<label id="cuenta<%=cont %>">
													<%= ((ConfEdosCtaArchivoBean)lista.get(i)).getNomCuenta() %>
												</label>
											</td>
											<td colspan="1" class="<%=estilo %>" align="center"> 
												<label id="titular<%=cont %>">
													<%= ((ConfEdosCtaArchivoBean)lista.get(i)).getNombreTitular() %>
												</label>
											</td>
											<td colspan="1" class="<%=estilo %>" align="center">
												<input type="radio" id="paperless<%=cont %>" name="paperless<%=cont %>" value="s"/>
											</td>
											<td colspan="1" class="<%=estilo %>" align="center">
												<input type="radio" id="paperless<%=cont %>" name="paperless<%=cont %>" value="n"/>
											</td>
										</tr>
				    				<%
				    					}
				    				%>	
							
						</tbody>
					</table>	
					<script type="text/javascript">
						var p = new Paginador(
							document.getElementById('paginador'),
							document.getElementById('tblDatos'),
							30
						);
						p.Mostrar();
					</script>										
  				</td>
  			</tr>
  			<tr>
  				<td>
				  <table border="0px" cellpadding="0px" cellspacing="0px" align="center">
				   <tr>
				     <td><a href="javascript:EnviarForma();"><img src="/gifs/EnlaceMig/gbo25280.gif" border="0"/></a></td>
					 <td><a href="javascript:Cancelar();"><img src="/gifs/EnlaceMig/gbo25190.gif" border="0"/></a></td>
				   </tr>
				  </table>
				</td>  					
  			</tr>
  		</tbody>
	</table>
	</br>
	</br>
</form>
</body>
</html>