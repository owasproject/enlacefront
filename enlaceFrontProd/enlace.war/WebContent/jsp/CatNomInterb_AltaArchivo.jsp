<jsp:useBean id='facultadesAltaCtas' class='java.util.HashMap' scope='session'/>

<%

  String nombreArchivoImp="";
  String totalRegistros="";
  String numeroCtasNacionales="";
  String usuario="";
  String msg="";

  if(request.getAttribute("totalRegistros")!=null)
    totalRegistros=(String)request.getAttribute("totalRegistros");
  if(request.getAttribute("nombreArchivoImp")!=null)
    nombreArchivoImp=(String)request.getAttribute("nombreArchivoImp");
  if(request.getAttribute("numeroCtasNacionales")!=null)
    numeroCtasNacionales=(String)request.getAttribute("numeroCtasNacionales");

  if(request.getAttribute("NumUsuario")!=null)
    usuario=(String)request.getAttribute("NumUsuario");
  if(request.getAttribute("NomUsuario")!=null)
    usuario+=" "+(String)request.getAttribute("NomUsuario");

  if(request.getAttribute("msg")!=null)
    msg=(String)request.getAttribute("msg");

 %>

<html>
<head>
	<title>Alta de cuentas</TITLE>
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">

<script language="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>

<!-- Estilos //-->
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<style type="text/css">
.componentesHtml
  {
	  font-family: Arial, Helvetica, sans-serif;
	  font-size: 11px;
	  color: #000000;
  }
</style>


<script language="JavaScript">
/*************************************************************************************/
function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(! (x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
var respuesta=0;

cuadroDialogo("<%=msg%>",1);

function continua()
{
  if(respuesta==1)
	{
	  document.TesImporta.action="CatNomInterbRegistro";
	  document.TesImporta.Modulo.value="2";
      document.TesImporta.submit();
	}
}

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function js_enviar()
{
  cuadroDialogo("�Desea enviar el archivo?",2);
}

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function js_regresarImportacion()
{
  document.TesImporta.action="CatNomInterbRegistro";
  document.TesImporta.Modulo.value="0";
  document.TesImporta.submit();
}

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function js_verDetalle()
{
   var forma=document.TesImporta;
   var parametros="";

   parametros+="totalRegistros="+escape(forma.totalRegistros.value);
   parametros+="&nombreArchivoImp="+escape(forma.nombreArchivoImp.value);
   parametros+="&numeroCtasNacionales="+escape(forma.numeroCtasNacionales.value);
   parametros+="&usuario="+escape(forma.usuario.value);

   ventanaInfo=window.open('/Enlace/jsp/CatNomInterb_ImportaDetalle.jsp?'+parametros,'trainerWindow','width=780,height=400,toolbar=no,scrollbars=yes');
 }
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/

<%= request.getAttribute("newMenu")%>

</script>
</HEAD>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
      <%= request.getAttribute("MenuPrincipal")%>
    </TD>
  </TR>
</table>
<%= request.getAttribute("Encabezado")%>
<table width="571" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
  </tr>
</table>

<FORM  NAME="TesImporta" method=post action="CatNomInterbRegistro">

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <td align=center>
	<table width="640" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
	 <tr>
	  <td class="tittabdat" align="center"> &nbsp; Nombre de Archivo </td>
	  <td class="tittabdat" align="center"> &nbsp; Total de Registros </td>
	  <td class="tittabdat" align="center"> &nbsp; Cuentas Externas Nacionales </td>
	  <td class="tittabdat" align="center"> &nbsp; Usuario </td>
	 </tr>
	 <tr>
	  <td class="textabdatobs" nowrap align="center"><%=nombreArchivoImp.substring(nombreArchivoImp.lastIndexOf("/")+1)%></td>
	  <td class="textabdatobs" nowrap align="center"><%=totalRegistros%></td>
	  <td class="textabdatobs" nowrap align="center"><%=numeroCtasNacionales%></td>
	  <td class="textabdatobs" nowrap align="center"><%=usuario%></td>
	 </tr>
	</table>
	<br>
	<table align=center border=0 cellspacing=0 cellpadding=0>
	 <tr>
	  <td><A href = "javascript:js_enviar();" border = 0><img src = "/gifs/EnlaceMig/gbo25520.gif" border=0 alt=Envio></a></td>
	  <td><A href = "javascript:js_regresarImportacion();" border = 0><img src = "/gifs/EnlaceMig/gbo25320.gif" border=0 alt=Regresar></a></td>
	  <td><A href = "javascript:js_verDetalle();" border = 0><img src = "/gifs/EnlaceMig/gbo25248.gif" border=0 alt="Detalle"></a></td>
	 </tr>
	</table>
   </td>
  </tr>
</table>

 <input type=hidden name=Modulo value=2>
 <input type=hidden name=nombreArchivoImp value="<%=nombreArchivoImp%>">
 <input type=hidden name=totalRegistros value="<%=totalRegistros%>">
 <input type=hidden name=numeroCtasNacionales value="<%=numeroCtasNacionales%>">
 <input type=hidden name=usuario value="<%=usuario%>">

 </form>

</body>
</HTML>