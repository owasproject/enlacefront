<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head>
  <title>Enlace Banco Santander Mexicano</title>
  <script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
  <script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
  <script type="text/javascript" src="/EnlaceMig/ConMov.js"></script>
  <script type="text/javascript">
    var ctaselec;
	var ctadescr;
	var ctatipre;
	var ctatipro;
	var ctaserfi;
	var ctaprod;
	var ctasubprod;
	var tramadicional;
	var cfm;
  </script>
  <script type="text/javascript">
	function PresentarCuentas()
	{
	  msg=window.open("ConCtasPdfXmlServlet","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=420,height=370");
	  msg.focus();
	}
  </script>
  <script type="text/javascript">
	function actualizacuenta()
	{
	  document.frmEdoCtaIn.cuenta.value=ctaselec;
	  document.frmEdoCtaIn.textcuenta.value=ctaselec+" "+ctadescr;
	  document.frmEdoCtaIn.propCuenta.value=ctadescr;
	}
  </script>
  <script type="text/javascript">
	function consulta()
	{		
		if(document.frmEdoCtaIn.textcuenta.value == '') {
			cuadroDialogo("Por favor, seleccione la cuenta para la que desea modificar " +
			"el estatus de env\u00edo de Estado de Cuenta",4);
		}else {
			document.frmEdoCtaIn.submit();
		}
	}
  </script>
  <script type="text/javascript">
    function MM_preloadImages()
    {   //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }
  </script>
  <script type="text/javascript">
    function MM_swapImgRestore()
    {   //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }
  </script>
  <script type="text/javascript">
    function MM_findObj(n, d)
    { //v3.0
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
        d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
    }
  </script>
  <script type="text/javascript">
    function MM_swapImage()
    {   //v3.0
        var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }
<%
    if( request.getAttribute("newMenu")!=null )
        out.println(request.getAttribute("newMenu"));
%>
  </script>
  <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
 </head>
 <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">
  <table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
   <tr valign="top">
    <td width="*">
<%
    if( request.getAttribute("MenuPrincipal")!=null )
        out.println(request.getAttribute("MenuPrincipal"));
%>
    </td>
   </tr>
  </table>
<%
    if( request.getAttribute("Encabezado")!=null )
        out.println(request.getAttribute("Encabezado"));
%>
  <form name="frmEdoCtaIn" method="post" action="ConfEdosCtaIndServlet?flujoOp=IND">
  <input type="hidden" name="propCuenta"  value=""/>
  <table width="760" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td align="center">
     <table width="400" border="0" cellspacing="2" cellpadding="3">
      <tr>
       <td class="tittabdat" colspan="2">&nbsp;</td>
      </tr>
      <tr align="center">
       <td class="textabdatcla" colspan="2">
        <table width="390" border="0" cellspacing="0" cellpadding="5">
         <tr>
          <td align="right" class="tabmovtexbol" width="100" nowrap>Cuenta:</td>
          <td width="290">
           <input type="text" name="textcuenta" class="tabmovtexbol" maxlength="22" size="22" onfocus="blur();" value=""/>
           <a href="javascript:PresentarCuentas();"><img src="/gifs/EnlaceMig/gbo25420.gif" border="0" align="absmiddle"/></a>
           <input type="hidden" name="cuenta"  value=""/>
          </td>
         </tr>
        </table>
       </td>
      </tr>
  	</table>
  	<table width="160" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="25">
      <tr>
       <td align="center" height="25"><a href="javascript:consulta();"><img src="/gifs/EnlaceMig/gbo25310.gif" border="0"/></a></td>
      </tr>
     </table>
  	</td>
  	</tr>
  	</table>
  	</form>
  	
  	<script type="text/javascript">
		<% if (request.getAttribute("MensajeErr") != null ) { %>
		<%= request.getAttribute("MensajeErr") %>
		<%	} %>
	</script>
 </body>
</html>