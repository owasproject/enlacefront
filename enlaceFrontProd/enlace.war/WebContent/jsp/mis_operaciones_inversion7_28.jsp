<html>
<head>
<title>Banca Virtual</title>
<script language = "JavaScript" SRC="/EnlaceMig/cuadroDialogo.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript">
<!--
<%
if(request.getAttribute("ArchivoErr")!=null)
   out.println(request.getAttribute("ArchivoErr"));
%>


var js_diasInhabiles="<%= request.getAttribute( "diasInhabiles" ) %>"

function isDigit (c)
{
  document.transferencia.monto.value="";
  cuadroDialogo("\n Capture el importe a transferir ",3);
  //document.transferencia.monto.focus();
  return ((c >= "0") && (c <= "9"));

}

function Formatea_Importe(importe)
{

   decenas=""
   centenas=""
   millon=""
   millares=""
   importe_final=importe
   var posiciones=7;

   posi_importe=importe.indexOf(".");
   num_decimales=importe.substring(posi_importe + 1,importe.length)

   if (posi_importe==-1)
     importe_final= importe + ".00";

   if (posi_importe==4)
   {
     centenas=importe.substring(1, posiciones);
     millares=importe.substring(0,1);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==5)
   {
     centenas=importe.substring(2, posiciones + 1);
     millares=importe.substring(0,2);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==6)
   {
     centenas=importe.substring(3, posiciones + 2);
     millares=importe.substring(0,3);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==7)
   {
     centenas=importe.substring(4, posiciones + 3);
     millares=importe.substring(1,4);
     millon=importe.substring(0,1)
     importe_final= millon + "," + millares + "," + centenas;
   }
   if (posi_importe==8)
   {
     centenas=importe.substring(5, posiciones + 4);
     millares=importe.substring(2,5);
     millon=importe.substring(0,2)
     importe_final= millon + "," + millares + "," + centenas;
   }
   if (posi_importe==9)
   {
     centenas=importe.substring(6, posiciones + 5);
     millares=importe.substring(3,6);
     millon=importe.substring(0,3)
     importe_final= millon + "," + millares + "," + centenas;
   }
   return importe_final
}

function Formatea_Importe2 (cantidad)
{
  strAux1 = ""
  strAux2 = ""
  entero = ""
  cantidadAux = cantidad


  pos_punto = cantidadAux.indexOf (".");

  num_decimales=cantidad.substring(pos_punto + 1,cantidad.length)

  if (pos_punto == 0)
     strAux1 = "0." + cantidadAux.substring (1, cantidadAux.length)
  else
  {
     if (pos_punto != -1)     //-- si se teclearon los centavos
      {
        cents = cantidadAux.substring (pos_punto + 1, cantidadAux.length)
        entero = cantidadAux.substring (0, pos_punto)
      }
      else
      {
        cents = "00"
        entero = cantidadAux
      }

      pos_coma = entero.indexOf (",")
      if (pos_coma != -1)     //-- si son mas de mil
      {
        cientos = entero.substring (entero.length - 3, entero.length)
        miles = entero.substring (0, entero.length - 3)
      }
      else
      {
        if (entero.length > 3) //-- si se teclearon mas de mil sin coma
        {
          cientos = entero.substring (entero.length - 3, entero.length)
          miles = entero.substring (0, entero.length - 3)
        }
        else
        {
          if (entero.length == 0)
            cientos = ""
          else
            cientos = entero
          miles = ""
        }
      }

      if (miles != "")
        strAux1 = miles
      if (cientos != "")
        strAux1 = strAux1 + cientos + "."
      strAux1 = strAux1 + cents

    strAux1=Formatea_Importe(strAux1)
  }
  return strAux1;

}



function validAmount (cantidad)
{

  strAux1 = ""
  strAux2 = ""
  entero = ""
  cantidadAux = cantidad

  if (cantidadAux == "" || cantidadAux <= 0)
  {
    document.iv728.importe.value="";
    cuadroDialogo("\n Capture el IMPORTE  a transferir ",3);
    //document.iv728.importe.focus();
    return false;
  }
  else
  {
    if (isNaN(cantidadAux))
    {
	  document.iv728.importe.value=""
      cuadroDialogo("Favor de ingresar un valor num\351rico en el IMPORTE ",3)
      //document.iv728.importe.focus()
      return false
    }

    pos_punto = cantidadAux.indexOf (".")

    num_decimales=cantidad.substring(pos_punto + 1,cantidad.length)

    if (pos_punto == 0)
      strAux1 = "0." + cantidadAux.substring (1, cantidadAux.length)
    else
    {
      if (pos_punto != -1)     //-- si se teclearon los centavos
      {
        cents = cantidadAux.substring (pos_punto + 1, cantidadAux.length)
        entero = cantidadAux.substring (0, pos_punto)
      }
      else
      {
        cents = "00"
        entero = cantidadAux
      }

      pos_coma = entero.indexOf (",")
      if (pos_coma != -1)     //-- si son mas de mil
      {
        cientos = entero.substring (entero.length - 3, entero.length)
        miles = entero.substring (0, entero.length - 3)
      }
      else
      {
        if (entero.length > 3) //-- si se teclearon mas de mil sin coma
        {
          cientos = entero.substring (entero.length - 3, entero.length)
          miles = entero.substring (0, entero.length - 3)
        }
        else
        {
          if (entero.length == 0)
            cientos = ""
          else
            cientos = entero
          miles = ""
        }
      }

      if (miles != "")
        strAux1 = miles
      if (cientos != "")
        strAux1 = strAux1 + cientos + "."
      strAux1 = strAux1 + cents

      if (miles != "")
        strAux2 = miles
      if (cientos != "")
        strAux2 = strAux2 + cientos + "."
      strAux2 = strAux2 + cents

      transf = document.iv728.importe.value
    }
    document.iv728.importe.value = strAux1;

    strAux1=Formatea_Importe(strAux1)
    if (miles != "")
      document.iv728.importestring.value = strAux2;
    else
	  document.iv728.importestring.value = strAux1;

    return true;

  }
}

function Valida_Date (dia, mes, anio)
{
  result = true;
  document.iv728.fecha.value=document.iv728.fecha_completa.value;
  return result;
}

function validctasdif (from_account, to_account)
{
  result=true;

  if (from_account == to_account)
  {
    cuadroDialogo("La cuenta de cargo debe ser diferente a la cuenta de abono",3);
    //document.iv728.monto.focus()
    return false
  }
  return result;
}
/*
function validar()
{

  result=Valida_Date (document.iv728.dia.value,document.iv728.mes.value,document.iv728.anio.value);
  if (result==true)
    result=validAmount (document.iv728.importe.value);

  var cuenta=document.iv728.destino.options[document.iv728.destino.selectedIndex].value;
  var des=cuenta;
  cuenta=cuenta.substring(0,cuenta.indexOf("|"));
  des=des.substring(des.indexOf("|")+1,des.length);
  des=des.substring(des.indexOf("|")+1,des.length-1);

  if (result==true)
     result=confirm ("¿ Desea transferir de la cuenta:"+cuenta+" de "+des+ " a Inversi&oacute;n 7/28  la cantidad de "+document.iv728.importestring.value)
  return result;
}
*/
function ValidarRangoPlazo()
{
  var prod=document.iv728.producto.value;
  var subp=document.iv728.instrumento.options[document.iv728.instrumento.selectedIndex].value;


  var trama=document.iv728.tabla_plazos.value;
  var plazo=document.iv728.plazo.value;


  var mensaje="";
  var iprod="";
  var tramaux="";
  var isub="";
  var iini="";
  var ifin="";
  var respuesta=false;

  var contador=trama.substring(0,trama.indexOf("@"));
  trama=trama.substring(trama.indexOf("@")+1,trama.length);

  for(i=1;i<=eval(contador);i++)
  {
	tramaux=trama.substring(0,trama.indexOf("@"));
	iprod=tramaux.substring(0,tramaux.indexOf("|"));
	tramaux=tramaux.substring(tramaux.indexOf("|")+1,tramaux.length);
    isub=tramaux.substring(0,tramaux.indexOf("|"));
    tramaux=tramaux.substring(tramaux.indexOf("|")+1,tramaux.length);
    iini=tramaux.substring(0,tramaux.indexOf("|"));
    tramaux=tramaux.substring(tramaux.indexOf("|")+1,tramaux.length);
	ifin=tramaux;


	if  ( (prod==iprod)&&(subp==isub)&&
		 ( (eval(plazo)>=eval(iini))&&
		   (eval(plazo)<=eval(ifin))))
	{
		respuesta=true;
		break;
    }
    if(i<eval(contador))
	  trama=trama.substring(trama.indexOf("@")+1,trama.length);

  }
  if(respuesta==false)
  {
	trama=document.iv728.tabla_plazos.value;

	for(i=1;i<=eval(contador);i++)
    {
      tramaux=trama.substring(0,trama.indexOf("@"));
	  iprod=tramaux.substring(0,tramaux.indexOf("|"));
	  tramaux=tramaux.substring(tramaux.indexOf("|")+1,tramaux.length);
      isub=tramaux.substring(0,tramaux.indexOf("|"));
      tramaux=tramaux.substring(tramaux.indexOf("|")+1,tramaux.length);
      iini=tramaux.substring(0,tramaux.indexOf("|"));
      tramaux=tramaux.substring(tramaux.indexOf("|")+1,tramaux.length);
	  ifin=tramaux;
      if  ((prod==iprod)&&
         (subp==isub))

	  {
		  if(iini==ifin)
            mensaje+=iini+",  ";
		  else
            mensaje+=iini+"-"+ifin+",  ";
      }
	 if(i<eval(contador))
		   trama=trama.substring(trama.indexOf("@")+1,trama.length);

 	}
	if(mensaje.length>3)
     mensaje=mensaje.substring(0,mensaje.length-3);
   cuadroDialogo("El o los plazos v&aacute;lidos para su inversi&oacute;n son:"+mensaje+"   d&iacute;a(s)" ,3);

  }
  return respuesta;
}

function ValidarPlazo(valor)
{
   var result=true;
   var plazotec=""+valor;

   if (plazotec.length>0)
     result=true;
   else
   {
       document.iv728.plazo.value="";
	   cuadroDialogo("Favor de ingresar un valor num&eacute;rico en el PLAZO mayor a 0",3);
   	   result=false;
   }
   if (result==true)
   {
     if (isNaN(valor))
     {
	   document.iv728.plazo.value=""
       cuadroDialogo("Favor de ingresar un valor num&eacute;rico en el PLAZO  mayor a 0",3)
       //document.iv728.importe.focus()
       result=false;
      }
	  else  if(eval(valor)<=0)
	  {
	    document.iv728.plazo.value="";
        cuadroDialogo("Favor de ingresar un valor num&eacute;rico en el PLAZO mayor a 0",3)
        //document.iv728.importe.focus()
        result=false;
      }
    }
	if(result==true)
    {
       result=ValidarRangoPlazo();
    }
    return result;
}


function validar()
{

  result=Valida_Date (document.iv728.dia.value,document.iv728.mes.value,document.iv728.anio.value);

  if(result==true)
  {
     if (document.iv728.destino.value.length==0)
	  {
		result=false;
		cuadroDialogo("Favor de seleccionar la cuenta ",3)

      }
  }
  if (result==true)
    result=validAmount (document.iv728.importe.value);
  if (result==true)
     result=ValidarPlazo(document.iv728.plazo.value);

  return result;
}


function trimString (str)
{
    str = this != window? this : str;
	return str.replace(/^\s+/g, '').replace(/\s+$/g, '');
}

/*function redondea(num)
{
  var a=0;
  var b=0;

  a=parseInt(Math.round(num*100));
  b=a/100;
  return b;
}
*/
function redondear(valor)
{
  var valorcambiar="";
  var valornuevo="";
  var num_decimales=0;
  var parte_entera="";
  var parte_decimal="";
  var tercerdecimal=0;
  var nuevo_decimal=0;

  valorcambiar=""+valor;
  valornuevo=valor;

  if (valorcambiar.indexOf(".")==-1)
     num_decimales=0;
 else
 {
   parte_decimal=valorcambiar.substring(valorcambiar.indexOf(".")+1,valorcambiar.length);
   parte_entera=valorcambiar.substring(0,valorcambiar.indexOf("."));
   if (parte_decimal.length>2)
   {
	  tercerdecimal=parte_decimal.substring(2,3);
	  if(eval(tercerdecimal)>5)
	  {
	     parte_decimal=parte_decimal.substring(0,2);
	     nuevo_decimal=eval(parte_decimal)+1;
         if (nuevo_decimal<100)
		    valornuevo=parte_entera+"."+nuevo_decimal;
         else
		 {
		    valornuevo=eval(parte_entera)+1;
			valornuevo+=".00";
		 }

	  }
	  else
	  {
	     parte_decimal=parte_decimal.substring(0,2);
		 valornuevo=parte_entera+"."+parte_decimal;
      }
   }
 }
 return valornuevo;
}


function quitar_ceros_izquierdos(dato)
{
	var EsteCaracter;
	var contador = 0;
	var sinceros ="";
	for (var i=0; i < dato.length; i++) {
		EsteCaracter = dato.substring(i , i+1);
		if (EsteCaracter=="0")
			contador ++;
		else
			break;
	}
	sinceros=dato.substring(contador,dato.length);
	return sinceros;
}


function val_importe()
{
  var dato=document.iv728.importe.value;
  var result=true;
  if  (trimString(dato)!="")
  {
    result=validAmount(dato);
    if (result==true)
	{
      document.iv728.importe.value=redondear(dato);
      document.iv728.importe.value=quitar_ceros_izquierdos(document.iv728.importe.value);
    }
  }
}




var gConfirmacion= false;
function confirmacion()
{
  //modificacion para integracion
  //var cuenta=document.iv728.destino.options[document.iv728.destino.selectedIndex].value;
  var cuenta=document.iv728.destino.value;
  var des=cuenta;
  cuenta=cuenta.substring(0,cuenta.indexOf("|"));
  des=des.substring(des.indexOf("|")+1,des.length);
  des=des.substring(des.indexOf("|")+1,des.length);
  des=des.substring(0,des.indexOf("|"));


  result=validar();
  gConfirmacion = result;
  if ( result == true )
  {
     cuadroDialogo("Desea transferir de la cuenta:"+cuenta+"  "+des+ " a su inversi&oacute;n la cantidad de $"+Formatea_Importe2(document.iv728.importestring.value),2);
   }
}

function continua()
{
   if (gConfirmacion == true )
   {
      if (respuesta==1) document.iv728.submit();
        else return false;

   }
   else
       return false;
}




function limpiar_datos()
{
   document.iv728.importe.value="";
   //modificacion para integracion document.iv728.destino.selectedIndex=0;
   document.iv728.textdestino.value="";
   document.iv728.destino.value="";
   document.iv728.fecha_completa.value=document.iv728.dia.value+"/"+document.iv728.mes.value+"/"+document.iv728.anio.value;
   document.iv728.moneda.value="";
   document.iv728.plazo.value="";
   document.iv728.contratos.length=1;
   document.iv728.contratos.options[0].value="0";
   document.iv728.contratos.options[0].text="Seleccione Contrato";
   document.iv728.instrumento.length=1;
   document.iv728.instrumento.options[0].value="0";
   document.iv728.instrumento.options[0].text="Seleccione Tipo de inversion";
   document.iv728.inst1[0].checked=true;
}

var dia;
var mes;
var anio;
var fecha_completa;


function WindowCalendar1()
{
    var m=new Date();
    n=m.getMonth();
    n=document.iv728.mes.value-1;

    dia=document.iv728.dia.value;
	mes=document.iv728.mes.value;
	anio=document.iv728.anio.value;

	msg=window.open("/EnlaceMig/calfut.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    msg.focus();

}

function Actualiza()
{
   document.iv728.fecha_completa.value=fecha_completa;
}



function Cambiar_contrato()
{

  // modificacion para integracion
  //var cuenta=document.iv728.destino.options[document.iv728.destino.selectedIndex].value;
  /*var cuenta=document.iv728.destino.value;
  cuenta=cuenta.substring(0,cuenta.indexOf("|"));
  var cuenta=document.iv728.destino.value;
  cuenta=cuenta.substring(0,cuenta.indexOf("|"));
  var contratos1=document.iv728.contratos_inversion.value;
  var contratos2=contratos1.substring(contratos1.indexOf(cuenta),contratos1.length);
  contratos2=contratos2.substring(contratos2.indexOf("|")+1,contratos2.length);
  var contratos3=contratos2.substring(0,contratos2.indexOf("@"));
  var contador=contratos3.substring(0,contratos3.indexOf("|"));
  document.iv728.contratos.options.length=contador;

  var contratos4=contratos3.substring(contratos3.indexOf("|")+1,contratos3.length);
  var contratos5="";

  for (i=1;i<=contador;i++)
  {
    contratos5=contratos4;
    contratos4=contratos4.substring(0,contratos4.indexOf("|"));

    document.iv728.contratos.options[i-1].value=contratos4;
    document.iv728.contratos.options[i-1].text=contratos4;
    contratos4=contratos5.substring(contratos5.indexOf("|")+1,contratos5.length);

  }
  */

}

function   Cambiar_plazos(producto, subproducto)
{

}

function obtener_contador(producto,subproducto)
{
  var iprod="";
  var isub="";
  var descrip="";
  var tramaux="";
  var contselec=0;

  var trama=document.iv728.tabla_instrumentos.value;
  var contador=trama.substring(0,trama.indexOf("@"));
  trama=trama.substring(trama.indexOf("@")+1,trama.length);
  for(i=1;i<=eval(contador);i++)
  {
         tramaux=trama.substring(0,trama.indexOf("@"));
		 iprod=tramaux.substring(0,tramaux.indexOf("|"));
		 tramaux=tramaux.substring(tramaux.indexOf("|")+1,tramaux.length);
         isub=tramaux.substring(0,tramaux.indexOf("|"));
		 tramaux=tramaux.substring(tramaux.indexOf("|")+1,tramaux.length);
         descrip=tramaux;

		 if (iprod==producto)
		  contselec++;
		 if(i<eval(contador))
		   trama=trama.substring(trama.indexOf("@")+1,trama.length);
  }
  return contselec;
}

function   Cambiar_instrumentos(producto,subproducto)
{

  var iprod2="";
  var isub2="";
  var descrip2="";
  var tramaux2="";
  var elementos=0;

  if (document.iv728.tabla_instrumentos.value.length>0)
  {

      var elementos=obtener_contador(producto,subproducto);

	  var trama2=document.iv728.tabla_instrumentos.value;
      var contador2=trama2.substring(0,trama2.indexOf("@"));
      trama2=trama2.substring(trama2.indexOf("@")+1,trama2.length);
	  var indice2=0;

	  document.iv728.instrumento.options.length=elementos;
      var ind=1;
	  for(j=1;j<=eval(contador2);j++)
      {
         tramaux2=trama2.substring(0,trama2.indexOf("@"));
		 iprod2=tramaux2.substring(0,tramaux2.indexOf("|"));
		 tramaux2=tramaux2.substring(tramaux2.indexOf("|")+1,tramaux2.length);
         isub2=tramaux2.substring(0,tramaux2.indexOf("|"));
		 tramaux2=tramaux2.substring(tramaux2.indexOf("|")+1,tramaux2.length);
         descrip2=tramaux2;
		 if (iprod2==producto)
		 {
             indice2=eval(ind-1);
             ind++;
			 document.iv728.instrumento.options[indice2].value=isub2;
			 document.iv728.instrumento.options[indice2].text=descrip2;
			 if (isub2==subproducto)
               document.iv728.instrumento.options[indice2].selected=true;
		 }
		 if(j<eval(contador2))
		   trama2=trama2.substring(trama2.indexOf("@")+1,trama2.length);
      }




  }

}

function Consultar_tasas()
{
 	var subp=document.iv728.instrumento.options[document.iv728.instrumento.selectedIndex].value;
	var prod=document.iv728.producto.value;

    if ((prod.length>0)&&(eval(subp)>0))
	{
	  tas=window.open("inversion7_28?ventana=3&subproducto="+subp+"&producto="+prod+"&opcion=I" ,"tasas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
      tas.focus();

    }
	else
      cuadroDialogo("Debe definir el contrato y el tipo de inversi&oacute;n antes de consultar tasas");
 }

function llamada_posicion()
{
   //modificacion para integracion
   //document.iv728.cta_posi.value="2|"+document.iv728.destino.options[document.iv728.destino.selectedIndex].value;
   document.iv728.cta_posi.value="2|"+document.iv728.destino.value;
   document.iv728.action="poscpsrvr1";
   document.iv728.submit();
   return true;
}

 <!-- *********************************************** -->
<!-- modificación para integración pva 07/03/2002    -->
<!-- *********************************************** -->
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}

function formatea(para)
{
	var formateado = "";
	var car = "";

	for(a2=0;a2<para.length;a2++)
	{
		if(para.charAt(a2)==' ')
			car="+";
		else
			car=para.charAt(a2);
			formateado+=car;
	}
	return formateado;
}


function actualizacuenta()
{
  document.iv728.destino.value=ctaselec+"|"+ctatipre+"|"+ctadescr+"|"+ctatipro+"|";
  document.iv728.textdestino.value=ctaselec+" "+ctadescr;
  document.iv728.contratos_inversion.value=tramadicional;
  ctadescr=formatea(ctadescr);
  
  // vswf:meg cambio de NASApp por Enlace 08122008
  document.iv728.action="/Enlace/"+document.iv728.WEB_APPLICATION.value+"/inversion7_28?ventana=1&cuenta="+ctaselec+"&ctatipre="+ctatipre+"&descrip="+ctadescr+"&ctatipro="+ctatipro;
  document.iv728.submit();

 // Cambiar_contrato();
 // Cambiar_plazos();
 // Cambiar_instrumentos();
}


function Actualizar_Selects()
{
  var trama=document.iv728.contratos.options[document.iv728.contratos.selectedIndex].value;
  if(trama=="0")
  {
     cuadroDialogo("Debe seleccinar un Contrato de inversi&oacute;n");
  }
  else
  {
	var cuenta=trama.substring(0,trama.indexOf("|"));
	trama=trama.substring(trama.indexOf("|")+1,trama.length);
	var producto=trama.substring(0,trama.indexOf("|"));
	trama=trama.substring(trama.indexOf("|")+1,trama.length);
	var subproducto=trama;
    document.iv728.producto.value=producto;
	document.iv728.subproducto.value=subproducto;
	Cambiar_instrumentos(producto,subproducto);
	Cambiar_plazos(producto,subproducto);
  }
}
//-->
</script>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Mon Apr 23 20:21:38 GMT-0600 2001-->
<script language="JavaScript">
<!--

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
	<%
          if (request.getAttribute("newMenu")!= null) {
          out.println(request.getAttribute("newMenu"));
          }
    %>
//-->
</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/Enlacemig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
	<td width="*">
	<!-- MENU PRINCIPAL -->

	<%
          if (request.getAttribute("MenuPrincipal")!= null) {
          out.println(request.getAttribute("MenuPrincipal"));
          }
    %>
	</TD>
  </TR>
</TABLE>
<%
          if (request.getAttribute("Encabezado")!= null) {
          out.println(request.getAttribute("Encabezado"));
          }
%>

<FORM NAME="iv728" METHOD=POST  ACTION="inversion7_28?ventana=2" onSubmit="return validar();">
<INPUT TYPE="HIDDEN" NAME="fecha_programada" VALUE=0>
<INPUT TYPE="HIDDEN" NAME="importestring" VALUE=0>
<INPUT TYPE="HIDDEN" NAME="diaresp">
<INPUT TYPE="HIDDEN" NAME="mesresp">
<INPUT TYPE="HIDDEN" NAME="anioresp">
<INPUT TYPE="HIDDEN" NAME="fecha">
<INPUT TYPE="HIDDEN" NAME="fac_programadas1"      VALUE=
<%
          if (request.getAttribute("fac_programadas1")!= null) {
          out.println(request.getAttribute("fac_programadas1"));
          }
%>>
<INPUT TYPE="HIDDEN" NAME="cuentas"               VALUE="">
<INPUT TYPE="HIDDEN" NAME="contratos_inversion"   VALUE="
<%
          if (request.getAttribute("contratos_inversion")!= null) {
          out.println(request.getAttribute("contratos_inversion"));
          }
%>">
<INPUT TYPE="HIDDEN" NAME="tabla_plazos"                 VALUE=
<%
          if (request.getAttribute("tabla_plazos")!= null) {
          out.println(request.getAttribute("tabla_plazos"));
          }
%>>

<INPUT TYPE="HIDDEN" NAME="tabla_instrumentos"                 VALUE=
<%
          if (request.getAttribute("tabla_instrumentos")!= null) {
          out.println(request.getAttribute("tabla_instrumentos"));
          }
%>>

<input type="HIDDEN" name="producto" value=
<%
          if (request.getAttribute("producto")!= null)
            out.println(request.getAttribute("producto"));
          else
            out.println(request.getAttribute(""));

%>>

<input type="hidden" name="subproducto" value=
<%
          if (request.getAttribute("subproducto")!= null)
            out.println(request.getAttribute("subproducto"));
          else
		    out.println(request.getAttribute(""));
%>>

<INPUT TYPE="HIDDEN" NAME=WEB_APPLICATION value=
 <%if(request.getAttribute("WEB_APPLICATION")!= null)
   	  out.print(request.getAttribute("WEB_APPLICATION"));
  else
     out.println("");
  %>
>


<INPUT TYPE="HIDDEN" NAME="cta_posi" VALUE="">
<table width="760" border="0" cellspacing="0" cellpadding="0">
<tr>
      <td align="center">
        <table border="0" cellspacing="2" cellpadding="3" width="415">
          <tr>
            <td class="tittabdat" colspan="2">
              Capture los datos</td>
          </tr>
          <tr align="center">
            <td class="textabdatcla" valign="top" colspan="2">
              <table border="0" cellspacing="5" cellpadding="0" width="415">
                <tr>
                  <td class="tabmovtex" width="200" nowrap>Cuenta:</td>
                  <td class="tabmovtex" width="200" nowrap>Contrato:</td>
				</tr>
				<tr>
                  <td class="tabmovtex" nowrap>
	              <!-- modificacion para integracion
				  <SELECT NAME="destino" onChange="Cambiar_contrato();">
				  <%
                        if (request.getAttribute("cuentas_destino")!= null) {
                        out.println(request.getAttribute("cuentas_destino"));
                        }
                  %>
				  </SELECT>-->
				  <input type="text" name=textdestino  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value=
				  <%
				      if(request.getAttribute("textdestino")!= null)
                         out.print(request.getAttribute("textdestino"));
					  else
					     out.println("");
				   %>
				  >
					<A HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle></A>
					<input type="hidden" name="destino" value=
					<%
				      if(request.getAttribute("destino")!= null)
                      	  out.print(request.getAttribute("destino"));
					  else
					      out.println("");
				   %>
					>
                  </td>
                  <td class="tabmovtex" nowrap>
				    <SELECT NAME="contratos" onchange="Actualizar_Selects();">
					  <%
                        if (request.getAttribute("pantalla_contratos")!= null) {
                        out.println(request.getAttribute("pantalla_contratos"));
                        }
                  %>

		          <!--Modificacion para integracion


				  <%
                        if (request.getAttribute("cuentas_contratos")!= null) {
                        out.println(request.getAttribute("cuentas_contratos"));
                        }
                  %>
				  -->
                  </SELECT>
                  </td>
                </tr>
                <tr >
                  <td class="tabmovtex" nowrap>
				  <table border="0" cellspacing="5" cellpadding="0">
  				  <tr>
				  <td class="tabmovtex">Tipo de Inversi&oacute;n:</td>
				  <td class="tabmovtex" valign="middle" align="left">
                    <SELECT NAME="instrumento" >
					<%
                        if (request.getAttribute("pantalla_instrumentos")!= null) {
                        out.println(request.getAttribute("pantalla_instrumentos"));
                        }
                    %>
				    </SELECT>
				  </td>
                  </tr>

				  <tr>
				  <td class="tabmovtex">Plazo:</td>
				  <td class="tabmovtex" valign="middle" align="left">
				   <input type="text" name=plazo value="" maxlength=5>
				  </td>
                  </tr>

				  </table>
				  </td>
                  <td class="tabmovtex" nowrap valign="top" align="left">
                    <table border="0" cellspacing="5" cellpadding="0">
                      <tr>
                        <td class="tabmovtex">Importe:</td>
                        <td class="tabmovtex" valign="middle" align="left">
                          <input type="text" name="importe" size="22" class="tabmovtex" onBlur="val_importe();" >
                        </td>
                      </tr>
                      <tr>
                        <td class="tabmovtex">Moneda:</td>
                        <td class="tabmovtex" valign="middle" align="left">
                          <input type="text" name="moneda" size="10" class="tabmovtex" onfocus="blur();" value=
						  <%
				            if(request.getAttribute("moneda")!= null)
                      	      out.print(request.getAttribute("moneda"));
					        else
					          out.println("");
				           %>
						  >
                        </td>
                      </tr>

                    </table>
                  </td>
                </tr>
				<tr valign="top">
                  <td class="tabmovtex" nowrap colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="200" height="5"></td>
                </tr>
                <tr valign="middle">
                  <td class="tabmovtex" nowrap>Instrucci&oacute;n
                    al
                    vencimiento:</td>
                  <td class="tabmovtex" nowrap valign="top" align="left">
                    <table border="0" cellspacing="5" cellpadding="0">
                      <tr>
                        <td class="tabmovtex">Fecha
                          de
                          aplicaci&oacute;n:</td>
                        <td class="tabmovtex" valign="middle" align="left">
 						  <%
                                 if (request.getAttribute("campos_fecha")!= null) {
                                 out.println(request.getAttribute("campos_fecha"));
                                 }
                          %>
						  </td>
                      </tr>
                    </table>
					  </td>
                </tr>
                <tr valign="middle">
                  <td class="tabmovtex" nowrap align="left" valign="top">
                    <table width="200" border="0" cellspacing="5" cellpadding="0">
                      <tr>
                        <td align="right" class="tabmovtex" valign="middle">
                          <input type="radio" name=inst1 VALUE=1 CHECKED>
                        </td>
                        <td class="tabmovtex" valign="middle" width="100%" nowrap>Reinvertir
                          capital
                          e
                          intereses
                        </td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex" valign="middle">
                          <input type="radio" name=inst1 VALUE=2 >
                        </td>
                        <td class="tabmovtex" valign="middle" nowrap>Reinvertir
                          capital
                        </td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex" valign="middle">
                          <input type="radio" name=inst1 VALUE=3 >
                        </td>
                        <td class="tabmovtex" valign="middle" nowrap>Transferir
                          capital
                          e
                          intereses
                        </td>
                      </tr>
                    </table>
					</td>
                  <td class="tabmovtex" nowrap valign="top" align="left">&nbsp;
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
		<br>
        <table width="227" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="22">
          <tr>
            <td align="right" valign="top" height="22" width="80">
			   <a href="javascript:confirmacion();" border=0>
              <img src="/gifs/EnlaceMig/gbo25280.gif" border=0></a>
            </td>
            <td align="center" valign="top" height="22" width="76">
			   <a href="javascript:limpiar_datos();" border=0>
              <img src="/gifs/EnlaceMig/gbo25250.gif" border=0></a>
            </td>
            <td align="left" valign="top" height="22" width="71">
			   <a href="javascript:Consultar_tasas();" border=0>
              <img src="/gifs/EnlaceMig/gbo25580.gif" border=0></a>
            </td>
          </tr>
        </table>
        <br>
      </td>
    </tr>
</table>
</form>
</body>
</html>

<Script language = "JavaScript">
<!--
 function VentanaAyuda(ventana){
    hlp=window.open("/EnlaceMig/ayuda.jsp#" + ventana ,"hlp","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
    hlp.focus();
}
//-->
</Script>