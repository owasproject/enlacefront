<html>
<head>
	<title>Confirmacion</TITLE>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>

<script language="JavaScript">

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
/*************************************************************************************/
<%= request.getAttribute( "newMenu" ) %>
/*************************************************************************************/
</SCRIPT>
</HEAD>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
		bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif',
		'/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif',
		'/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif',
		'/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif',
		'/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif',
		'/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif',
		'/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')"
		background="/gifs/EnlaceMig/gfo25010.gif">

	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	    <tr valign="top">
			 <td width="*">
				<!--  MENU PRINCIPAL -->
				<%= request.getAttribute( "MenuPrincipal" ) %>
			  </TD>
		</TR>
	</TABLE>
	<%= request.getAttribute( "Encabezado" ) %>

<FORM  NAME="transferencia" method=post action="">
   <TABLE ALIGN="center" BORDER="0" cellpadding=2 cellspacing=3 WIDTH="50%">
   <tr>
     <td colspan=2 bgcolor='#DDDDDD'>&nbsp;&nbsp;&nbsp; Estatus de la Cuenta </td>
   </tr>

   <tr>
    <td colspan=2 class='tabfonazu' align=center>
      <%= request.getAttribute( "Tabla" ) %>
    </td>
   </tr>


   <tr>
     <td bgcolor='#CCCCCC'>
	   Cuenta: &nbsp;
	 </td>
     <td bgcolor='#CCCCCC'>
	  <%= request.getAttribute( "Cuenta" ) %>
	 </td>
   </tr>

   <tr>
     <td bgcolor='#EBEBEB'>
	   Descripción: &nbsp;
	 </td>
     <td bgcolor='#EBEBEB'>
	  <%= request.getAttribute( "Desc" ) %>
	 </td>
   </tr>

   <tr>
     <td bgcolor='#CCCCCC'>
	   Banco: &nbsp;
	 </td>
     <td bgcolor='#CCCCCC'>
	  <%= request.getAttribute( "Banco" ) %>
	 </td>
   </tr>

   <tr>
     <td bgcolor='#EBEBEB'>
	   Plaza: &nbsp;
	 </td>
     <td bgcolor='#EBEBEB'>
	  <%= request.getAttribute( "Plaza" ) %>
	 </td>
   </tr>

   <tr>
     <td bgcolor='#CCCCCC'>
	   Sucursal: &nbsp;
	 </td>
     <td bgcolor='#CCCCCC'>
	  <%= request.getAttribute( "Suc" ) %>
	 </td>
   </tr>

<!--
   <tr>
     <td bgcolor='#EBEBEB'>
	   Resultado: &nbsp;
	 </td>
     <td bgcolor='#EBEBEB'>
	  <%= request.getAttribute( "Resultado" ) %>
	 </td>
   </tr>
-->
   <%= request.getAttribute( "Relacion" ) %>
  </table>
 </form>
</body>
</html>