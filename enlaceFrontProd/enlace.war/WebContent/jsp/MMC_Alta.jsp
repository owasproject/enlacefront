<%@page import="java.util.List"%>
<%@page import="java.util.TreeMap"%>
<jsp:useBean id="facultadesAltaCtas" class="java.util.HashMap" scope="session"/>

<%@page import="EnlaceMig.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%--
 @author  Alejandro Rada Vazquez
 @descripcion Modulo de Alta de Cuentas para Enlace Internet
//--%>

<html>
<head>
	<title>Alta de cuentas</title>
<meta http-equiv="Expires" content="1"/>
<meta http-equiv="pragma" content="no-cache"/>

<script type="text/javascript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/util.js"></script>


<%-- Estilos //--%>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
<style type="text/css">
.componentesHtml
  {
	  font-family: Arial, Helvetica, sans-serif;
	  font-size: 11px;
	  color: #000000;
  }
</style>

<script type="text/javascript">
/*************************************************************************************/
function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************************/
<%
if(request.getAttribute("ErroresEnArchivo")!=null)
 {
   System.out.println("Errores en archivo != null " + request.getAttribute("ErroresEnArchivo"));
   out.print(request.getAttribute("ErroresEnArchivo"));
 }
%>

function js_despliegaErrores(archivo)
 {
   ventanaInfo1=window.open('/Download/'+archivo,'errWindow','width=450,height=300,toolbar=no,scrollbars=yes');
   ventanaInfo1.focus();
 }

/*************************************************************************************/
function claveBanco()
 {
	var cveBanco = "";
	var ceros = "0000";
	var forma=document.AltaCuentas;

	cveBanco = forma.bancoBN.options[forma.bancoBN.selectedIndex].value;
	if(cveBanco==0)
	  return "";
	cveBanco = trimString(cveBanco.substring(cveBanco.indexOf("+")+1,cveBanco.indexOf("-")));
	if(cveBanco.length != 3)
		cveBanco = (ceros.substring(0,3-(cveBanco.length)) )+cveBanco;
	return cveBanco;
 }

var paisAnterior="";

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function digitoVerificador(cuenta)
 {
    var pesos = "371371371371371371371371371";
    var DC = 0;
	var cuenta2 = cuenta.value.substring(0,17);
	var cveBanco = claveBanco();

    if(cuenta.value.substring(0,3) == cveBanco)
	 {
		for(i=0;i<cuenta2.length;i++)
		  DC += ( parseInt(cuenta2.charAt(i)) * parseInt(pesos.charAt(i))) % 10;
		DC = 10 - (DC % 10);
		if(DC == 10)	DC=0;
		  cuenta2 += DC;
		if((cuenta2) == cuenta.value)
		   return true;
	 }
	else
	 {
	   cuenta.focus();
	   cuadroDialogo("La Cuenta CLABE no coincide con el banco seleccionado",3);
	   return false;
	 }
	cuenta.focus();
	cuadroDialogo("La cuenta claBE no es correcta",3);
	return false;
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function Enviar()
 {
	var forma=document.AltaCuentas;
	var contador=0;

	for(j=0;j<forma.length;j++)
	  if(forma.elements[j].type=='checkbox')
		if(forma.elements[j].checked)
		  contador++;

	if(contador==0)
	 {
	   cuadroDialogo("Debe seleccionar al menos una opci&oacute;n",1);
	 }
	else
	 {
		var cadena="";
		for(x=1;x<forma.length;x++)
		 {
			if(forma.elements[x].type=="checkbox")
			 if(forma.elements[x].checked)
			   cadena+="1";
			 else
			   cadena+="0";
		 }
		forma.cuentasSel.value=cadena;
// 		INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token Se modifica el valor del modulo para enviarlo a token
		forma.Modulo.value="2";
		forma.submit();
	 }

 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function Agregar()
 {
   var forma=document.AltaCuentas;
   var valRadio=-1;
   var cuenta="";
   var strTramaMB="";
   var strTramaBN="";
   var strTramaBI="";

   if(validaForma(forma))
    {
	   for(j=0;j<forma.length;j++)
		 if(forma.elements[j].type=='radio' && forma.elements[j].name=='TipoAlta' )
			if(forma.elements[j].checked)
			   valRadio=forma.elements[j].value;

	   if(valRadio==0)
		 {
		   cuenta=trimString(forma.numeroCuentaMB.value);
		   strTramaMB=trimString(forma.strTramaMB.value);
			if(strTramaMB.indexOf(cuenta)>=0)
			  {
				cuadroDialogo("La cuenta ya fue seleccionada.",3);
				return ;
			  }
		 }

	   if(valRadio==1)
		 {
		   cuenta=trimString(forma.numeroCuentaBN.value);
		   strTramaBN=trimString(forma.strTramaBN.value);
			if(strTramaBN.indexOf(cuenta)>=0)
			  {
				cuadroDialogo("La cuenta ya fue seleccionada.",3);
				return ;
			  }
		 }

	   if(valRadio==2)
		 {

		   if(trimString(forma.strClaveABABI.value) == ""){
		   	cuadroDialogo("La Clave ABA/SWIFT debe ser informada.",3);
				return ;
		   }

		   cuenta=trimString(forma.numeroCuentaBI.value);
		   strTramaBI=trimString(forma.strTramaBI.value);
			if(strTramaBI.indexOf(cuenta)>=0)
			  {
				cuadroDialogo("La cuenta ya fue seleccionada.",3);
				return ;
			  }
		 }

		if(valRadio==4)
		 {
		   cuenta=trimString(forma.numeroCuentaNM.value);
		   strTramaBN=trimString(forma.strTramaBN.value);
			if(strTramaBN.indexOf(cuenta)>=0)
			  {
				cuadroDialogo("La cuenta ya fue seleccionada.",3);
				return ;
			  }
		 }

	  textToUpper(forma);
      forma.action = "MMC_Alta";
      /** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token
      	Cuando se ha elegido carga por archivo modificar el modulo
      	para verificar el token */
      if( valRadio == 3 ){
      	forma.Modulo.value = "2";
      } else {
      	forma.Modulo.value = "1";
      }
      /** FIN CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token */
	  forma.submit();
    }
 }

function eliminaCarcEsp(obj){
	obj.value = cadena = obj.value.replace(/[^0-9a-zA-Z]*/g, "");
}

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function validaForma(forma)
 {
   /* Mismo Banco ********/
   var i=0;
   var j=0;
   var valRadio=-1;

   for(j=0;j<forma.length;j++)
     if(forma.elements[j].type=='radio' && forma.elements[j].name=='TipoAlta' )
	    if(forma.elements[j].checked)
	       valRadio=forma.elements[j].value;

   if(valRadio==-1)
	 {
	   cuadroDialogo("Debe seleccionar una opci&oacute;n",1);
	   return false;
	 }
   else
   if(valRadio==0)
	{
	  if(!validaCuenta(forma.numeroCuentaMB,"MB") ||  !validaTitularCuenta(forma.nombreTitularMB))
		return false;
    }
   else
   /* Nacionales ********/
   if(valRadio==1)
	{
	  var divisaN=forma.cmbDivisaOBN.options[forma.cmbDivisaOBN.selectedIndex].value;
	  tipo=forma.tipoCuentaBN.options[forma.tipoCuentaBN.selectedIndex].value;
	  banco=forma.bancoBN.options[forma.bancoBN.selectedIndex].value;
	  if(tipo==0)
		{
		  forma.tipoCuentaBN.focus();
		  cuadroDialogo("Debe seleccionar un tipo de cuenta",1);
		  return false;
		}
	  else
	   {
		 if(validaCuenta(forma.numeroCuentaBN,"BN") &&  validaTitularCuenta(forma.nombreTitularBN))
		   {
			 if(banco==0)
			  {
			  	if(divisaN ==="MXM"){
			  		forma.bancoBN.focus();
			  	}

				 cuadroDialogo("Debe seleccionar un banco",1);
				 return false;
			  }
			 else
			   {
				 /*
				 if(trimString(forma.strPlazaBN.value)=="" || trimString(forma.cvePlazaBN.value)=="")
				   {
				     cuadroDialogo("Debe especificar la plaza del banco",3);
					 return false;
				   }
				 else*/
				//  if(/*trimString(forma.sucursalBN.value)=="" || */!isInteger(trimString(forma.sucursalBN.value)))
				//   {
				//	 forma.sucursalBN.focus();
				//	 cuadroDialogo("La sucursal del banco es num&eacute;rica",3);
				//	 return false;
				//   }
			   }
		   }
		  else
			 return false;
	   }
    }
   else
   /* Internacionales ********/
   if(valRadio==2)
	{
	   pais=trimString(forma.paisBI.options[forma.paisBI.selectedIndex].value);
	   divisa=forma.cmbDivisaBI.options[forma.cmbDivisaBI.selectedIndex].value;

	   if(pais==0)
		{
		  forma.paisBI.focus();
		  cuadroDialogo("Debe seleccionar un Pa&iacute;s",1);
		  return false;
		}
	   else
		{
		  if(!validaClaveABA(pais,forma.strClaveABABI)){
			  return false;
		  }
		  if(divisa==0)
			{
			  forma.cmbDivisaBI.focus();
		      cuadroDialogo("Debe seleccionar una Divisa",1);
			  return false;
			}
		  else{
		    if(!validaCuenta(forma.numeroCuentaBI,"BI") ||  !validaTitularCuenta(forma.nombreTitularBI))
		      return false;
		  }

		  if(!validaDatosBenefi(forma)){
			  return false;
		  }
		}
    }
   else
   /* Importacion ********/
   if(valRadio==3)
	{
	   if(trimString(forma.Archivo.value)=="")
		{
		  forma.Archivo.focus();
		  cuadroDialogo("Debe especificar el nombre del archivo",3);
		  return false;
		}

	   if(trimString(forma.strTramaMB.value)!="" ||
		   trimString(forma.strTramaBN.value)!="" ||
		    trimString(forma.strTramaBI.value)!=""  )
		{
		  forma.Archivo.focus();
		  cuadroDialogo("Se han registrado operaciones en l&iacute;nea, no puede importar archivos en este momento.",3);
		  return false;
		}
	}else /* Moviles ********/
   if(valRadio==4)
	{
	  tipo=forma.tipoCuentaNM.options[forma.tipoCuentaNM.selectedIndex].value;
	  banco=forma.bancoNM.options[forma.bancoNM.selectedIndex].value;
	  if(tipo==0)
		{
		  forma.tipoCuentaNM.focus();
		  cuadroDialogo("El tipo de cuenta es obligatorio",1);
		  return false;
		}
	  else
	   {
		 if(validaCuenta(forma.numeroCuentaNM,"NM") &&  validaTitularCuenta(forma.nombreTitularNM))
		   {
			 if(tipo == 2 && banco==0)
			  {
				 forma.bancoNM.focus();
				 cuadroDialogo("Debe seleccionar un banco",1);
				 return false;
			  }else{
			  	if(tipo == 1){
			  		//forma.bancoNM.disabled = true;
			  		forma.bancoNM.disabled = "disabled";
			  	}
			  }
		   }
		  else
			 return false;
	   }
    }
   return true;
 }

function validaClaveABA(pais,aba)
 {
   if(trimString(pais)=="USA")
	 if(trimString(aba.value)=="")
	  {
	    aba.focus();
		cuadroDialogo("La clave ABA es obligatoria para Cuentas en Estados Unidos.",1);
		return false;
	  }

   return true;
 }

 function validaDatosBenefi(forma)
 {
	domicilioBenef=trimString(forma.calleBenef.value);
	ciudadBenef=trimString(forma.ciudadBenef.value);
	noIdBenef=trimString(forma.idBenef.value);

	paisBenef=trimString(forma.paisBIBenef.options[forma.paisBIBenef.selectedIndex].value);

   if(paisBenef==0)
	{
	  forma.paisBI.focus();
	  cuadroDialogo("Debe seleccionar un Pa&iacute;s para el beneficiario",1);
	  return false;
	}

	if(domicilioBenef == "")
	{
		forma.calleBenef.focus();
		cuadroDialogo("El Domicilio del beneficiario es obligatorio.",1);
		return false;
	}
	if(!especialesBI(forma.calleBenef,"el Domicilio")){
		  return false;
	}

	if(ciudadBenef == "")
	{
		forma.ciudadBenef.focus();
		cuadroDialogo("La Ciudad del beneficiario es obligatorio.",1);
		return false;
	}
	if(!especialesBI(forma.ciudadBenef,"la ciudad del beneficiario")){
		  return false;
	}

	if(noIdBenef == "")
	{
		forma.idBenef.focus();
		cuadroDialogo("El N&uacute;mero de Identificaci&oacute;n del beneficiario es obligatorio.",1);
		return false;
	}
	if(!especialesBI(forma.idBenef,"el N&uacute;mero de Identificaci&oacute;n")){
		  return false;
	}

	return true;
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function validaCuenta(cuenta,tipo)
 {
	if(trimString(cuenta.value)=="")
	 {
	   cuenta.focus();
	   if( "NM" == tipo ){
	   		cuadroDialogo("El n&uacute;mero m&oacute;vil es obligatorio",1);
	   }
	   else{
	   		cuadroDialogo("La cuenta es obligatoria",1);
	   }

	   return false;
	 }


	if(cuenta.value == 0){
		cuenta.focus();
		cuadroDialogo("Ingrese un n&uacute;mero m&oacute;vil v&aacute;lido",3);
		return false;
	}

   if(tipo=="BI")
	 {
	   if(cuenta.value.length>20)
		 {
		   cuenta.focus();
		   cuadroDialogo("La longitud o formato de la cuenta no es correcto",3);
		   return false;
		 }
	 }
   else
	 {
	   if(isInteger(trimString(cuenta.value)))
		 {
		   if(tipo=="MB")
			 {
			   if(cuenta.value.length!=11 && cuenta.value.length!=16)
				{
				   /*
				   if(cuenta.value.substring(0,2)=="49")
					{
					   cuenta.focus();
					   cuadroDialogo("El formato de la cuenta no es v&aacute;lido", 3);
					   return false;
					}
				   */
				   cuenta.focus();
				   cuadroDialogo("El formato o longitud de la cuenta no es v&aacute;lido", 3);
				   return false;
				}

			   if(cuenta.value.length==16)
				{
				  var prefijo=cuenta.value.substring(0,4);
				  if(
					 prefijo!= "4931" && prefijo!= "4547" && prefijo!= "4915" && prefijo!= "5453" && prefijo!= "4941" && prefijo!= "5471" && prefijo!= "5470" && prefijo!= "5474" && prefijo!= "5408" && prefijo!= "4913" && prefijo!= "2728" && prefijo!= "5549" )
					{
					  cuenta.focus();
					  cuadroDialogo("El formato para la longitud de la cuenta no es v&aacute;lido", 3);
					  return false;
					}
				}
			   /*else
				{
				  cuenta.focus();
				  cuadroDialogo("El formato o longitud de la cuenta no es v&aacute;lido", 3);
				  return false;
				}*/
			 }
		   if(tipo=="BN")
			 {
			   forma=document.AltaCuentas;
			   tipocuenta=forma.tipoCuentaBN.options[forma.tipoCuentaBN.selectedIndex].value;
			   banco=forma.bancoBN.options[forma.bancoBN.selectedIndex].value;
			   cveBanco=claveBanco();

			   if(tipocuenta=="40") /*** Cuenta ClaBE */
				 {
				   if(cuenta.value.length == 18)
					 {
					   if(!digitoVerificador(cuenta))
						 return false;
					 }
					else
					 {
						cuenta.focus();
						cuadroDialogo("La longitud de la cuenta no corresponde con el tipo especificado",3);
						return false;
					 }
				 }
			   else
			   if(tipocuenta=="02") /*** TDD - TDC */
				 {
				   if( !(cuenta.value.length == 16 || cuenta.value.length == 15) )
					 {
					   cuenta.focus();
					   cuadroDialogo("La longitud de la cuenta no corresponde con el tipo especificado",3);
					   return false;
					 }
				 }
			   else
			   if(tipocuenta=="01") /*** Cheques */
				 {
				   if(cuenta.value.length != 11)
					 {
					   cuenta.focus();
					   cuadroDialogo("La longitud de la cuenta no corresponde con el tipo especificado",3);
					   return false;
					 }
				 }
			 }
			 if(tipo=="NM")
			 {
				if( cuenta.value.length != 10)
				 {
					   cuenta.focus();
					   cuadroDialogo("La longitud de la cuenta es incorrecta",3);
					   return false;
				}
			 }
		 }
		else
		 {
		   cuenta.focus();
		   cuadroDialogo("La cuenta debe ser num&eacute;rica.",3);
		   return false;
		 }
	 }
	return true;
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function especiales(Txt1)
{
	//VSWF BMB se modific� la cadena strEspecial, agregando & como car�cter v�lido
   var strEspecial=" &ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
   var cont=0;

    for(i0=0;i0<Txt1.value.length;i0++)
	 {
       Txt2 = Txt1.value.charAt(i0);
	   if( strEspecial.indexOf(Txt2)!=-1 )
		 cont++;
     }
	if(cont!=i0)
	 {
	   Txt1.focus();
	   cuadroDialogo("En el titular o descripcion de la cuenta, no se permiten caracteres especiales",3);
	   return false;
	 }
   return true;
}


/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function especialesBI(Txt1,campo)
{
   var strEspecial=" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789,";
   var cont=0;

    for(i0=0;i0<Txt1.value.length;i0++)
	 {
       Txt2 = Txt1.value.charAt(i0);
	   if( strEspecial.indexOf(Txt2)!=-1 )
		 cont++;
     }
	if(cont!=i0)
	 {
	   Txt1.focus();
	   cuadroDialogo("No se permiten caracteres especiales para " + campo,3);
	   return false;
	 }
   return true;
}

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function validaCiudad(ciudad)
 {
   if(trimString(ciudad.value).length==0)
	{
	  ciudad.focus();
	  cuadroDialogo("La ciudad es obligatoria",3);
	  return false;
  	}
   if(!especialesBI(ciudad,"la ciudad"))
	  return false
   return true;
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function validaBanco(banco)
 {
   if(trimString(banco.value).length==0)
	{
	  banco.focus();
	  cuadroDialogo("El banco es obligatorio",3);
	  return false;
  	}
   return true;
 }


/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function validaTitularCuenta(titular)
 {
   if(trimString(titular.value).length==0)
	{
	  titular.focus();
	  cuadroDialogo("La descripci&oacute;n o titular de la cuenta es obligatoria",3);
	  return false;
  	}
   if(!especiales(titular))
	  return false
   return true;
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function Limpiar()
 {
	document.AltaCuentas.reset();
	cargaListas('listAll');
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function verificaPais()
 {
   var forma=document.AltaCuentas;

   paisActual=trimString(forma.paisBI.options[forma.paisBI.selectedIndex].value);
   if(paisActual==paisAnterior)
	   return;
   if( paisActual=='USA' && paisAnterior!='USA' )
    {
	  forma.strCiudadBI.value="";
	  forma.strBancoBI.value="";
	}
   if(paisAnterior=='USA')
	{
	  forma.strCiudadBI.value="";
	  forma.strBancoBI.value="";
	  forma.strClaveABABI.value="";
	}
   paisAnterior=trimString(forma.paisBI.options[forma.paisBI.selectedIndex].value);
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function verificaClaveABA()
 {
   var forma=document.AltaCuentas;
   if(trimString(forma.paisBI.options[forma.paisBI.selectedIndex].value)!='USA')
     forma.strClaveABABI.blur();
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function verificaCiudad()
 {
   var forma=document.AltaCuentas;
   if(trimString(forma.paisBI.options[forma.paisBI.selectedIndex].value)=='USA')
     forma.strCiudadBI.blur();
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function verificaBanco()
 {
   var forma=document.AltaCuentas;
   if(trimString(forma.paisBI.options[forma.paisBI.selectedIndex].value)=='USA')
     forma.strBancoBI.blur();
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function seleccionaDivisa()
 {
   var forma=document.AltaCuentas;
   forma.cveDivisaBI.value=forma.cmbDivisaBI.options[forma.cmbDivisaBI.selectedIndex].value;
   forma.strDivisaBI.value=forma.cmbDivisaBI.options[forma.cmbDivisaBI.selectedIndex].text;
 }

/*************************************************************************************/
/**
*FSW IDS :::JMFR:::
*AJUSTE NORMATIVO SPID
*SE AGREGA FUNCIONALIDAD AL COMBO DE BANCOS PARA QUE MUESTRE LOS BANCOS SPID.
**/
/*************************************************************************************/
<%
String bancoSpid = "";
String bancoAll = "";
if(request.getAttribute("ListSpid") != null){
	bancoSpid = (String) request.getAttribute("ListSpid");
}
if(request.getAttribute("ListAll") != null){
	bancoAll = (String) request.getAttribute("ListAll");
}
 %>

function cargaListas(Lista){
   var lista = Lista;
   var select = document.getElementById('bancoBN');
   var strBancBN = document.getElementById(lista).value;
   var array;
   var arrayAux;
   if (select != null){
	    select.options.length = 0;
		select.value = "";
		if(strBancBN != "@"){
			array = strBancBN.split("@");
			select.options[0] = new Option ("Seleccione un banco", "0");
			for (var i = 1; i < (array.length)-1; i++){
					arrayAux = array[i].split("?");
					select.options[i] = new Option (arrayAux[1], arrayAux[0]);
			}
		}else{
			select.options[0] = new Option ("::No existen datos para mostrar::", "0");
		}
	}
}

function cargaComboNM(Lista){
   var lista = Lista;
   var select = document.getElementById('bancoNM');
   var strBancBN = document.getElementById(lista).value;
   var array;
   var arrayAux;
   if (select != null){
	    select.options.length = 0;
		select.value = "";
		if(strBancBN != "@"){
			array = strBancBN.split("@");
			select.options[0] = new Option ("Seleccione un banco", "0");
			for (var i = 1; i < (array.length)-1; i++){
					arrayAux = array[i].split("?");
					select.options[i] = new Option (arrayAux[1], arrayAux[0]);
			}
		}else{
			select.options[0] = new Option ("::No existen datos para mostrar::", "0");
		}
	}
}

function selecDivisaOBN()
{
    var forma=document.AltaCuentas;
    var divisaN=forma.cmbDivisaOBN.options[forma.cmbDivisaOBN.selectedIndex].value;
    forma.strDivisaOBN.value=divisaN;
	if (divisaN ==='USD') {
		forma.tipoCuentaBN.value=40;
		forma.hiTipoCuentaBN.value=40;
		forma.tipoCuentaBN.disabled = true;
		cargaListas('listSpid');
	} else {
		cargaListas('listAll');
	    forma.tipoCuentaBN.disabled = false;
	}
}

function selecTipoCuentaBN() {
	var forma=document.AltaCuentas;
	var valTipoCuentaBN=forma.tipoCuentaBN.options[forma.tipoCuentaBN.selectedIndex].value;
	forma.hiTipoCuentaBN.value=valTipoCuentaBN;
}
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function TraerCiudades()
 {
   forma=document.AltaCuentas;
   if(trimString(forma.paisBI.options[forma.paisBI.selectedIndex].value)=='USA')
    {
	  ventanaInfo2=window.open('EI_Ciudades','Ciudades','width=250,height=350,toolbar=no,scrollbars=no');
	  ventanaInfo2=window.open('/Download/EI_Ciudades.html','Ciudades','width=250,height=350,toolbar=no,scrollbars=no');
	  ventanaInfo2.focus();
	}
   else
	 cuadroDialogo("Solo aplica a Estados Unidos. Realice la captura manual.",1)
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function TraerBancos()
 {
   forma=document.AltaCuentas;
   if(trimString(forma.paisBI.options[forma.paisBI.selectedIndex].value)=='USA')
    {
	  ventanaInfo2=window.open('EI_Bancos','Bancos','width=280,height=350,toolbar=no,scrollbars=no');
	  ventanaInfo2=window.open('/Download/EI_Bancos.html','Bancos','width=250,height=350,toolbar=no,scrollbars=no');
	  ventanaInfo2.focus();
    }
   else
     cuadroDialogo("Solo aplica a Estados Unidos. Realice la captura manual.",1)
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function TraerClavesABA()
 {
   forma=document.AltaCuentas;
   if(trimString(forma.paisBI.options[forma.paisBI.selectedIndex].value)=='USA')
    {
      if(trimString(forma.strBancoBI.value)!="")
       {
         var Banco=forma.strBancoBI.value;
         Banco=escape(Banco);
         ventanaInfo2=window.open('EI_Bancos?BancoTxt='+Banco+"&Clave=1",'Bancos','width=250,height=350,toolbar=no,scrollbars=no');
         ventanaInfo2.focus();
       }
      else
       cuadroDialogo("Seleccione un Banco.",1)
    }
   else
     cuadroDialogo("Solo aplica para cuentas en Estados Unidos.",1)
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function traerDatosDeClave(obj)
{
   forma=document.AltaCuentas;

   clave=trimString(obj.value);


   if(trimString(forma.paisBI.options[forma.paisBI.selectedIndex].value)=='USA')
   {
	ventanaInfo2=window.open('EI_Bancos?BancoTxt=ABA&Clave='+clave,'Claves','width=300,height=180,toolbar=no,scrollbars=no');
        ventanaInfo2.focus();
   }

}
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function Selecciona(S)
 {
   var v1="";
   var v2="";
   var v3="";

   var sforma=document.AltaCuentas;

   if(S==1)
	{
      v1=ventanaInfo2.document.Forma.SelPlaza.options[ventanaInfo2.document.Forma.SelPlaza.selectedIndex].value;
	  v2=ventanaInfo2.document.Forma.SelPlaza.options[ventanaInfo2.document.Forma.SelPlaza.selectedIndex].text;
      if(v1)
		sforma.cvePlazaBN.value=v1;
	  if(v2)
		sforma.strPlazaBN.value=v2;
	}

   if(S==2)
	{
	  v1=ventanaInfo2.document.Forma.SelBanco.options[ventanaInfo2.document.Forma.SelBanco.selectedIndex].value;
	  v2=ventanaInfo2.document.Forma.SelBanco.options[ventanaInfo2.document.Forma.SelBanco.selectedIndex].text;

	  if(v2)
	    sforma.strBancoBI.value=v2;

	  sforma.strCiudadBI.value="";
	  sforma.strClaveABABI.value="";
	}

   if(S==3)
	{
	  v1=ventanaInfo2.document.Forma.SelCiudad.options[ventanaInfo2.document.Forma.SelCiudad.selectedIndex].value;
	  v2=ventanaInfo2.document.Forma.SelCiudad.options[ventanaInfo2.document.Forma.SelCiudad.selectedIndex].text;

	  if(v2)
	    sforma.strCiudadBI.value=v2;
	}

   if(S==4)
   {
      v1=ventanaInfo2.document.Forma.SelClave.options[ventanaInfo2.document.Forma.SelClave.selectedIndex].value;
	  v2=ventanaInfo2.document.Forma.SelClave.options[ventanaInfo2.document.Forma.SelClave.selectedIndex].text;

	  if(v2)
	   {
	     sforma.strCiudadBI.value=trimString(v2.substring(v2.indexOf("-")+1,v2.length));
		 sforma.strClaveABABI.value=trimString(v2.substring(0,v2.indexOf("-")));
	   }
    }

   if(S==5)
    {
      v1=ventanaInfo2.document.Forma.CiudadABA.value;
      v2=ventanaInfo2.document.Forma.BancoABA.value;
	  v3=ventanaInfo2.document.Forma.EstadoABA.value;

	  if(v1 && v2 && v3)
	   {
	     sforma.strCiudadBI.value=v1+", "+v3;
		 sforma.strBancoBI.value=v2;
	   }
	  else
	   {
		 sforma.strCiudadBI.value="";
		 sforma.strBancoBI.value="";
	   }
    }
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/**
 @author  Alejandro Rada Vazquez
 @param num cadena numerica
 @return boolean true, false
 @descripcion verifica una expresion para determinar si es tipo entera sin signo
  utilizando expresiones regulares
*/
function isInteger(num)
{
  var TemplateI = /^\d*$/;	 /*** Formato de numero entero sin signo */
  return TemplateI.test(num); /**** Compara "num" con el formato "Template" */
}

<%= request.getAttribute("newMenu")%>

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/

function msgCifrado(){
	try {
		var mensajeCifrado = document.AltaCuentas.mensajeCifrado.value;
		if(mensajeCifrado != null && mensajeCifrado != "") {
			var arreglo = mensajeCifrado.split("|");
            var msj = arreglo[1];
            var tipo = arreglo[0];
           cuadroDialogo( msj ,tipo);
		}
	} catch (e){};
}

/**
 * Habilitar el combobox de bancos si se ha seleccionado alg�n tipo de banco
 * diferente a "Santander", es decir, habilita el combobox cuando el valor
 * seleccionado es "Otros bancos nacionales" y lo deshabilita si el valor
 * es "Santander"
 * @author Armando Montoya
 */
function habilitaBancoNumerosMoviles(){
	  var forma=document.AltaCuentas;
	  tipoBanco=forma.tipoCuentaNM.options[forma.tipoCuentaNM.selectedIndex].value;
	  //enable
	  if(tipoBanco == 2){
		forma.bancoNM.removeAttribute('disabled');
	  }
	  //disable
	  else{
	   forma.bancoNM.disabled = "disabled";
	  }
}

</script>
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	style=" bgcolor: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);" onLoad="cargaListas('listAll');cargaComboNM('listAll');msgCifrado();MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif'); obtenDatosBrowser();" >
    <fmt:setLocale value="mx"/>
    <fmt:setBundle basename="mx.altec.enlace.utilerias.FielStringsBundle" var="lang"/>
<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
      <%= request.getAttribute("MenuPrincipal")%>
    </td>

  </tr>
</table>
<%= request.getAttribute("Encabezado")%>
<table width="571" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1" /></td>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1" /></td>
  </tr>
</table>

<form name="transferencia">
 <input type="hidden" name="Descripcion" />
</form>

<form name="AltaCuentas" enctype="multipart/form-data" method="post" action="MMC_Alta">
 <input type="hidden" name="mensajeCifrado" value="<%=request.getAttribute("mensajeCifrado")%>" />
 <%-- FSW IDS JMFR Se agregan hiddens para carga de listas spid --%>
 <input type="hidden" id="listSpid" value="<%=bancoSpid%>" />
 <input type="hidden" id="listAll" value="<%=bancoAll%>" />
 <table width="760" border="0" cellpadding="0" cellspacing="0" >
 <tr>
  <td>

  <table style="margin-left:auto; margin-right:auto; border: 0px;" cellpadding="0" cellspacing="0" width="99%">

   <%
   if (((Boolean) facultadesAltaCtas.get ("AltaCtasMB")).booleanValue())
    {
   %>
   <%-- ****************************************************************************************************** //--%>
   <%-- Cuentas Mismo banco //--%>
   <%-- ****************************************************************************************************** //--%>
   <tr>
     <td colspan="4" class="tittabdat">
     	<input type="radio" value="0" name="TipoAlta" checked />&nbsp;<b><fmt:message key="cuentas.titulo.alta.mb" bundle="${lang}"/></b>
     </td>
   </tr>

   <tr>
     <td class="textabdatcla" colspan="4" width="100%">
       <table border="0" class="textabdatcla" cellspacing="3" cellpadding="2">

        <tr>
		 <td class="tabmovtex" width="10"><br /></td>
         <td class="tabmovtex11" width="100" >N&uacute;mero de cuenta:</td>
         <td class="tabmovtex"><input type="text" name="numeroCuentaMB" class="componentesHtml" maxlength="22" size="22" /></td>
        </tr>

		<tr>
		 <td class="tabmovtex"><br /></td>
         <td class="tabmovtex11">Descripci&oacute;n Enlace:</td>
         <td class="tabmovtex"><input type="text" name="nombreTitularMB" class="componentesHtml" maxlength="40" size="22"/></td>
        </tr>

        <tr>
         <td class="tabmovtex" colspan="3" ><br /></td>
        </tr>

       </table>
     </td>
    </tr>

    <tr>
     <td class="tabmovtex" style=" bgcolor: white;" colspan="4"><br /></td>
    </tr>
   <%
   }
   %>

   <%
   if (((Boolean) facultadesAltaCtas.get ("AltaCtasBN")).booleanValue())
    {
   %>
   <%-- ****************************************************************************************************** //--%>
   <%--  Otros Bancos //--%>
   <%-- ****************************************************************************************************** //--%>
   <tr>
     <td colspan="4" class="tittabdat">
     <input type="radio" value="1" name="TipoAlta" checked />&nbsp;<b><fmt:message key="cuentas.titulo.alta.obn" bundle="${lang}"/></b></td>
   </tr>

   <tr>
     <td class="textabdatcla" colspan="4" width="100%">
       <table border="0" class="textabdatcla" cellspacing="3" cellpadding="2" >


		 <tr>
                 <td class="tabmovtex" width="10"><br /></td>
		 <td class="tabmovtex11" >Tipo de cuenta</td>
		 <td class="tabmovtex">
		 <input type="hidden" name="hiTipoCuentaBN" value="0"/>
		   <select name="tipoCuentaBN" class="componentesHtml" onchange="selecTipoCuentaBN();">
		   	 <option value="0" class="tabmovtex"> Seleccione un tipo </option>
		     <option value="40">Cuenta CLABE</option>
			 <option value="02">Tarjeta de Cr&eacute;dito/D&eacute;bito</option>
			 <%--<option value='02'>Tarjeta de D&eacute;bito</option>//--%>
			 <%--<option value='01'>Cheques</option>//--%>
		   </select>

		 </td>

            <td class="tabmovtex"><br /></td>
            <td class="tabmovtex11" ><fmt:message key="cuentas.label.divisa" bundle="${lang}"/></td>
         <td class="tabmovtex">
           <input type="text" SIZE="4" name="strDivisaOBN" value="MXP" disabled/>
         <select name="cmbDivisaOBN" class="componentesHtml" onchange="selecDivisaOBN();">
           <option value="MXP"><fmt:message key="cuentas.divisa.mxp" bundle="${lang}"/></option>
           <option value="USD"><fmt:message key="cuenta.divisa.usd" bundle="${lang}"/></option>
         </select>
         </td>
		</tr>

		<tr>
		  <td class="tabmovtex" width="10"><br /></td>
		  <td class="tabmovtex11" width="100">N&uacute;mero de Cuenta:</td>
		  <td class="tabmovtex"><input type="text" class="componentesHtml" SIZE="25" MAXLENGTH="22" name="numeroCuentaBN" /></td>
		</tr>

		<tr>
		 <td class="tabmovtex" width="10"><br /></td>
	     <td class="tabmovtex11">Nombre del Titular:</td>
		 <td class="tabmovtex"><input type="text" class="componentesHtml" SIZE="25" MAXLENGTH="40" NAME="nombreTitularBN" /></td>
	    </tr>

		<tr>
		 <td class="tabmovtex" width="10"><br /></td>
         <td class="tabmovtex11">Banco:</td>
         <%-- FSW IDS JMFR Se agregan hiddens para carga de listas spid --%>
          <td class="tabmovtex">
		   <SELECT id="bancoBN" NAME="bancoBN" class="componentesHtml"></SELECT>
		  </td>
		</tr>
			<INPUT TYPE="hidden" NAME="cvePlazaBN"/>
			<INPUT TYPE="hidden" class="componentesHtml" SIZE="25" MAXLENGTH="40" NAME="strPlazaBN" onFocus="blur();"/>&nbsp;</a></td>

        <tr>
         <td class="tabmovtex" colspan="3" ><br /></td>
        </tr>

       </table>
     </td>
    </tr>

    <tr>
     <td class="tabmovtex" style=" bgcolor: white;" colspan="4"><br /></td>
    </tr>
   <%
	}
   %>


   <%
   if (((Boolean) facultadesAltaCtas.get ("AltaCtasBI")).booleanValue())
    {
   %>
   <%-- ****************************************************************************************************** //--%>
   <%--  Cuentas Internacionales //--%>
   <%-- ****************************************************************************************************** //--%>
	   <tr>
		 <td colspan="4" class="tittabdat"><INPUT TYPE="radio" value="2" NAME="TipoAlta" checked/>&nbsp;<b><fmt:message key="cuentas.titulo.alta.obi" bundle="${lang}"/></b></td>
	   </tr>

	   <tr>
		 <td class="textabdatcla" colspan="4">
		  <table border="0" class="textabdatcla" cellspacing="3" cellpadding="2" width="100%">

		   <tr>
			<td colspan="6" class="tabmovtex" width="680"><br /></td>
		   </tr>

		   <tr>
			<td class="tabmovtex"><br /></td>
			<td class="tabmovtex11">Pa&iacute;s:</td>
			<td class="tabmovtex">
			 <SELECT NAME="paisBI" class="componentesHtml" onBlur="verificaPais();">
			   <option value="0" class="tabmovtex"> Seleccione un Pais </option>
			   <%
			   String comboPaises="";
		       if(request.getAttribute("comboPaises")!=null)
				  comboPaises=(String)request.getAttribute("comboPaises");
			   %>
			   <%=comboPaises%>
			 </SELECT>
			</td>
			<td class="tabmovtex"><br /></td>
			<td class="tabmovtex11" >Divisa:</td>
			<td class="tabmovtex" >
			  <INPUT TYPE="text" SIZE="4" class="componentesHtml" MAXLENGTH="5" NAME="cveDivisaBI" onFocus="blur();" />
			 <input type="hidden" name="strDivisaBI" />
			 <SELECT NAME="cmbDivisaBI" class="componentesHtml" onChange="seleccionaDivisa();">
			   <option value="" class="tabmovtex"> Seleccione una Divisa</option>
			   <%
			   String comboDivisas="";
		       if(request.getAttribute("comboDivisas")!=null)
				  comboDivisas=(String)request.getAttribute("comboDivisas");
			   %>
			   <%=comboDivisas%>

			 </SELECT>
			</td>

		   </tr>

		   <tr>
			<td class="tabmovtex"><br /></td>
			<td class="tabmovtex11">N&uacute;mero de Cuenta:</td>
			<td class="tabmovtex"><INPUT value="<%=request.getAttribute("numeroCuentaBI")!= null ? request.getAttribute("numeroCuentaBI") : "" %>" TYPE="text" SIZE="25" class="componentesHtml" MAXLENGTH="22" NAME="numeroCuentaBI" /></td>
			<td class="tabmovtex"><br /></td>
			<td class="tabmovtex11">Clave ABA/SWIFT:</td>
			<td class="tabmovtex">
			  <table cellspacing="0" cellpadding="0" border="0">
				<tr>
				  <td class="tabmovtex" >
					 <input type="text" size="25" maxlength="11" class="componentesHtml" name="strClaveABABI" onkeyup="eliminaCarcEsp(this);" />
				  </td>
				  <td>

				  </td>
				</tr>
			  </table>
			</td>

		   </tr>
		   <tr>
			<td class="tabmovtex"><br /></td>
			<td class="tabmovtex11">Nombre del Titular:</td>
			<td class="tabmovtex"><INPUT value="<%=request.getAttribute("nombreTitularBI")!= null ? request.getAttribute("nombreTitularBI") : "" %>" type="text" class="componentesHtml" SIZE="25" MAXLENGTH="40" NAME="nombreTitularBI" /></td>
			<td class="tabmovtex"><br /></td>
			<td class="tabmovtex"><br /></td>
			<td class="tabmovtex"><br /></td>
		   </tr>
		   <!-- ****************************************************************************************************** //-->
   		   <!--  Se agregan datos de beneficiario INDRA Agosto 2016 //-->
           <!-- ****************************************************************************************************** //-->
		   <tr>
		     <td></td>
			 <td class="tabmovtex11">Domicilio:</td>
			 <td class="tabmovtex">
			 <INPUT value="<%=request.getAttribute("calleBenef")!= null ? request.getAttribute("calleBenef") : "" %>" type="text" class="componentesHtml" SIZE="25" MAXLENGTH="40" NAME="calleBenef" />
			 </td>
			 <td></td>
			 <td class="tabmovtex11">Pa&iacute;s Beneficiario</td>
		 	<td class="tabmovtex">
				<SELECT NAME="paisBIBenef" class="componentesHtml">
			   		<option value="0" class="tabmovtex"> Seleccione un Pais </option>
			   			<%
			   			comboPaises="";
		       			if(request.getAttribute("comboPaises")!=null)
				  		comboPaises=(String)request.getAttribute("comboPaises");
			   			%>
			   			<%=comboPaises%>
			 	</SELECT>
			</td>
		   </tr>

           <tr>
			 <td></td>
			 <td class="tabmovtex11">Ciudad Beneficiario:</td>
			 <td class="tabmovtex" width="265">
			 <INPUT value="<%=request.getAttribute("ciudadBenef")!= null ? request.getAttribute("ciudadBenef") : "" %>" type="text" class="componentesHtml" SIZE="25" MAXLENGTH="40" NAME="ciudadBenef" />
			 </td>
			 <td></td>
			 <td class="tabmovtex11">N&uacute;mero de Identificaci&oacute;n:</td>
			 <td class="tabmovtex">
			 <INPUT value="<%=request.getAttribute("idBenef")!= null ? request.getAttribute("idBenef") : "" %>" type="text" class="componentesHtml" SIZE="25" MAXLENGTH="40" NAME="idBenef" />
			 </td>
		   </tr>
           <!-- ****************************************************************************************************** //-->
   		   <!--  Se agregan datos de beneficiario INDRA Agosto 2016 //-->
           <!-- ****************************************************************************************************** //-->

		   <tr>
			 <td colspan="6" class="tabmovtex" width="680"><br /></td>
		   </tr>

		  </table>
		 </td>
		</tr>

		<tr>
		 <td class="tabmovtex" style=" bgcolor: white;" colspan="4"><br /></td>
		</tr>

   <%
   	}
   %>

   <%

   if (((Boolean) facultadesAltaCtas.get ("AltaCtasNM")).booleanValue())
    {

   %>

		<tr>
    		<td colspan="4" class="tittabdat">
    		<INPUT TYPE="radio" value="4" NAME="TipoAlta" checked /> <b>N&uacute;meros M&oacute;viles Moneda Nacional </b></td>
    	</tr>
    	<tr>
            <td class="textabdatcla" colspan="4" width="100%">
                <table border="0" class="textabdatcla" cellspacing="3" cellpadding="2" >
				<tr>
					<td class="tabmovtex"><br /></td>
					<td class="tabmovtex11" width="150">N&uacute;mero M&oacute;vil (10 D&iacute;gitos):</td>
					<td class="tabmovtex"><INPUT TYPE="text" class="componentesHtml" SIZE="25" MAXLENGTH="10" NAME="numeroCuentaNM" /></td>
				</tr>
				<tr>
					<td class="tabmovtex"><br /></td>
					<td class="tabmovtex11" >Tipo de cuenta:</td>
					<td class="tabmovtex">
						<SELECT NAME="tipoCuentaNM" class="componentesHtml" onChange="javascript:habilitaBancoNumerosMoviles();" >
							<option value="0" class="tabmovtex"> Seleccione un tipo </option>
							<option value="1">Santander </option>
							<option value="2">Otros bancos Nacionales</option>
						</SELECT>
					</td>
				</tr>
				<tr>
					<td class="tabmovtex"><br /></td>
					<td class="tabmovtex11">Nombre del Titular:</td>
					<td class="tabmovtex"><INPUT TYPE="text" class="componentesHtml" SIZE="25" MAXLENGTH="40" NAME="nombreTitularNM" /></td>
				</tr>
				<tr>
					<td class="tabmovtex"><br /></td>
					<td class="tabmovtex11">Banco:</td>
					<td class="tabmovtex">
						<SELECT id="bancoNM" NAME="bancoNM" class="componentesHtml" disabled="disabled" >
						</SELECT>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
		 <td class="tabmovtex" style=" bgcolor: white;" colspan="4"><br /></td>
		</tr>
   <%
	}
   %>


   <%
    if (((Boolean) facultadesAltaCtas.get ("AltaCtasIA")).booleanValue())
     {
   %>
   <%-- ****************************************************************************************************** //--%>
   <%-- Importar Archivo //--%>
   <%-- ****************************************************************************************************** //--%>
	  <tr>
		<td class="tittabdat" colspan="4"><INPUT TYPE="radio" value="3" NAME="TipoAlta" />&nbsp;<b><fmt:message key="cuentas.titulo.alta.archivo" bundle="${lang}"/></b></td>
	  </tr>

	  <tr>
	   <td class="textabdatcla" colspan="4">
		<table border="0" class="textabdatcla" cellspacing="3" cellpadding="2" >
		 <tr>
		  <td class="tabmovtex" width="10"><br /></td>
		  <td class="tabmovtex11">Importar cuentas desde archivo &nbsp; <INPUT TYPE="file" NAME="Archivo" class="componentesHtml" value=" Archivo " /></td>
		 </tr>
		 <tr>
		  <td colspan="2" class="tabmovtex"><br /></td>
		 </tr>
		</table>
	   </td>
	  </tr>
   <%
    }
   %>

 </table>

 <br />
 <table align="center" border="0" cellspacing="0" cellpadding="0">
    <tr>
     <td><a href = "javascript:Agregar();" style="border-style: none;"><img src = "/gifs/EnlaceMig/gbo25290.gif" style="border-style: none;" /></a></td>
     <td><a href = "javascript:Limpiar();" style="border-style: none;"><img src = "/gifs/EnlaceMig/gbo25250.gif" style="border-style: none;" /></a></td>
    </tr>
  </table>


   </td>
  </tr>
 </table>

 <%
  String Modulo="0";

  String strTramaMB="";
  String strTramaBN="";
  String strTramaBI="";

  String tablaMB="";
  String tablaBN="";
  String tablaBI="";

  if(request.getAttribute("Modulo")!=null)
    Modulo=(String)request.getAttribute("Modulo");
  if(request.getAttribute("strTramaMB")!=null)
    strTramaMB=(String)request.getAttribute("strTramaMB");
  if(request.getAttribute("strTramaBN")!=null)
    strTramaBN=(String)request.getAttribute("strTramaBN");
  if(request.getAttribute("strTramaBI")!=null)
    strTramaBI=(String)request.getAttribute("strTramaBI");

  if(request.getAttribute("tablaMB")!=null && !request.getAttribute("tablaMB").equals(""))
    tablaMB=(String)request.getAttribute("tablaMB")+ "<br /><br />";

  if(request.getAttribute("tablaBN")!=null && !request.getAttribute("tablaBN").equals(""))
    tablaBN=(String)request.getAttribute("tablaBN")+ "<br /><br />";

  if(request.getAttribute("tablaBI")!=null && !request.getAttribute("tablaBI").equals(""))
    tablaBI=(String)request.getAttribute("tablaBI")+ "<br /><br />";
 %>

<br />
<table width="760" border="0" cellpadding="0" cellspacing="0" >
 <tr>
  <td width="100%"><%=tablaMB%></td>
 </tr>
</table>

<table width="760" border="0" cellpadding="0" cellspacing="0" >
 <tr>
  <td width="100%"><%=tablaBN%></td>
 </tr>
</table>

<table width="760" style="border: 0px;" cellpadding="0" cellspacing="0" >
 <tr>
  <td width="100%"><%=tablaBI%></td>
 </tr>
</table>

 <input type="hidden" name="Modulo" value="<%=Modulo%>" />
 <input type="hidden" name="strTramaMB" value="<%=strTramaMB%>" />
 <input type="hidden" name="strTramaBN" value="<%=strTramaBN%>" />
 <input type="hidden" name="strTramaBI" value="<%=strTramaBI%>" />
 <input type="hidden" name="cuentasSel" />
 <input type="hidden" id="datosBrowser" name="datosBrowser" value=""/>

 <%if(Modulo.trim().equals("1"))
  {
 %>
 <table width="760" style="border: 0px;" cellpadding="0" cellspacing="0">
    <tr>
     <td align="center"><a href = "javascript:Enviar();" style="border-style: none;"><img src = "/gifs/EnlaceMig/gbo25520.gif" style="border-style: none;" alt="Enviar" />
     	</a>
     </td>
    </tr>
  </table>
  <%
   }
  %>

<%=
	 (request.getAttribute("mensaje") != null)?
	 ("<script type=\"text/javascript\">cuadroDialogo(" + (String)request.getAttribute("mensaje") +
	 ");</script>"):""
%>
 </form>

</body>
</html>