<%-- 
  - Autor: FSW Everis
  - Aplicacion: Enlace
  - M�dulo: SPID
  - Fecha: 01/06/2016
  - Descripcion: Pantalla que contiene el comprobante fiel para las operaciones registro, modificacion y baja.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <fmt:setLocale value="mx"/>
        <fmt:setBundle basename="mx.altec.enlace.utilerias.FielStringsBundle" var="lang"/>

        <title><fmt:message key="fiel.comprobante.titulo" bundle="${lang}"/></title>

        <script type="text/javascript" src="/EnlaceMig/Convertidor.js"></script>
        <script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>

        <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
    </head>
    <body style="background-color:#ffffff;">
        <form name="formComprobante" method="post" action="">

            <c:choose>
                <c:when test="${not empty fielResult}">
                    <table width="430" border="0" cellspacing="6" cellpadding="0"  style="margin: 0 auto;">
                        <tr>
                            <td rowspan="2" valign="middle" class="titpag" align="left" width="35">
                                <img src="/gifs/EnlaceMig/glo25040.gif"  style="border: 0px"/>
                            </td>
                        </tr>
                    </table>
                    <table width="430" border="0" cellspacing="6" cellpadding="0"  style="margin: 0 auto;">
                        <tr>
                            <td width="247" valign="top" class="titpag"><fmt:message key="fiel.comprobante.compropera" bundle="${lang}"/></td>
                            <td valign="top" class="titpag"><img src="/gifs/EnlaceMig/glo25030.gif" width="152" height="30" alt="Enlace"/></td>
                        </tr>
                        <tr>
                            <td valign="top" class="titenccom">${comprobanteTitulo}</td>
                        </tr>
                    </table>
                    <table width="430" border="0" cellspacing="0" cellpadding="0"  style="margin: 0 auto;">
                        <tr>
                            <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."/></td>
                        </tr>
                        <tr>
                            <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."/></td>
                        </tr>
                        <tr>
                            <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."/></td>
                        </tr>
                    </table>
                    <table width="400" border="0" cellspacing="0" cellpadding="0"  style="margin: 0 auto;">
                        <tr>
                            <td align="center">
                                <table width="430" border="0" cellspacing="0" cellpadding="0" style="margin: 0 auto;">
                                    <tr>
                                        <td colspan="3"> </td>
                                    </tr>
                                    <tr>
                                        <td width="21" style = " background-image :url(/gifs/EnlaceMig/gfo25030.gif);"></td>
                                        <td width="428" align="top" style="text-align: center; height: 150;">
                                            <table width="380" border="0" cellspacing="2" cellpadding="3" style = " background-image :url(/gifs/EnlaceMig/gau25010.gif);">
                                                <tr>
                                                    <td class="tittabcom" align="right" width="0"><fmt:message key="fiel.comprobante.codpos" bundle="${lang}"/></td>
                                                    <td class="textabcom" align="left" style="white-space:nowrap;">${fielResult.detalleConsulta.datosTitular.codPostal}</td>
                                                </tr>
                                                <tr>
                                                    <td class="tittabcom" align="right" width="0"><fmt:message key="fiel.comprobante.curp" bundle="${lang}"/></td>
                                                    <td class="textabcom" align="left" style="white-space:nowrap;">${fielResult.detalleConsulta.datosTitular.curp}</td>
                                                </tr>
                                                <tr>
                                                    <td class="tittabcom" align="right" width="0"><fmt:message key="fiel.comprobante.direcc" bundle="${lang}"/></td>
                                                    <td class="textabcom" align="left" style="white-space:nowrap;">${fielResult.detalleConsulta.datosTitular.direccion}</td>
                                                </tr>
                                                <tr>
                                                    <td class="tittabcom" align="right" width="0"><fmt:message key="fiel.comprobante.email" bundle="${lang}"/></td>
                                                    <td class="textabcom" align="left" style="white-space:nowrap;">${fielResult.detalleConsulta.datosTitular.email}</td>
                                                </tr>
                                                <tr>
                                                    <td class="tittabcom" align="right" width="0"><fmt:message key="fiel.comprobante.estado" bundle="${lang}"/></td>
                                                    <td class="textabcom" align="left" style="white-space:nowrap;">${fielResult.detalleConsulta.datosTitular.estado}</td>
                                                </tr>
                                                <tr>
                                                    <td class="tittabcom" align="right" width="0"><fmt:message key="fiel.comprobante.localid" bundle="${lang}"/></td>
                                                    <td class="textabcom" align="left" style="white-space:nowrap;">${fielResult.detalleConsulta.datosTitular.localidad}</td>
                                                </tr>
                                                <tr>
                                                    <td class="tittabcom" align="right" width="0"><fmt:message key="fiel.comprobante.nomorganiz" bundle="${lang}"/></td>
                                                    <td class="textabcom" align="left" style="white-space:nowrap;">${fielResult.detalleConsulta.datosTitular.nombreOrganizacion}</td>
                                                </tr>
                                                <tr>
                                                    <td class="tittabcom" align="right" width="0"><fmt:message key="fiel.comprobante.nomtitul" bundle="${lang}"/></td>
                                                    <td class="textabcom" align="left" style="white-space:nowrap;">${fielResult.detalleConsulta.datosTitular.nombreTitular}</td>
                                                </tr>
                                                <tr>
                                                    <td class="tittabcom" align="right" width="0"><fmt:message key="fiel.comprobante.numpasaporteife" bundle="${lang}"/></td>
                                                    <td class="textabcom" align="left" style="white-space:nowrap;">${fielResult.detalleConsulta.datosTitular.numPasaporteIfe}</td>
                                                </tr>
                                                <tr>
                                                    <td class="tittabcom" align="right" width="0"><fmt:message key="fiel.comprobante.pais" bundle="${lang}"/></td>
                                                    <td class="textabcom" align="left" style="white-space:nowrap;">${fielResult.detalleConsulta.datosTitular.pais}</td>
                                                </tr>
                                                <tr>
                                                    <td class="tittabcom" align="right" width="0"><fmt:message key="fiel.comprobante.rfc" bundle="${lang}"/></td>
                                                    <td class="textabcom" align="left" style="white-space:nowrap;">${fielResult.detalleConsulta.datosTitular.rfc}</td>
                                                </tr>
                                                <tr>
                                                    <td class="tittabcom" align="right" width="0"><fmt:message key="fiel.comprobante.edocertif" bundle="${lang}"/></td>
                                                    <td class="textabcom" align="left" style="white-space:nowrap;">${fielResult.detalleConsulta.estadoCertificado}</td>
                                                </tr>
                                                <tr>
                                                    <td class="tittabcom" align="right" width="0"><fmt:message key="fiel.comprobante.fechaemision" bundle="${lang}"/></td>
                                                    <td class="textabcom" align="left" style="white-space:nowrap;">${fielResult.detalleConsulta.fchEmision}</td>
                                                </tr>
                                                <tr>
                                                    <td class="tittabcom" align="right" width="0"><fmt:message key="fiel.comprobante.fechaexpi" bundle="${lang}"/></td>
                                                    <td class="textabcom" align="left" style="white-space:nowrap;">${fielResult.detalleConsulta.fchExpiracion}</td>
                                                </tr>
                                                <tr>
                                                    <td class="tittabcom" align="right" width="0"><fmt:message key="fiel.comprobante.msj" bundle="${lang}"/></td>
                                                    <td class="textabcom" align="left" style="white-space:nowrap;">${fielResult.detalleConsulta.mensaje}</td>
                                                </tr>
                                                <tr>
                                                    <td class="tittabcom" align="right" width="0"><fmt:message key="fiel.comprobante.numserie" bundle="${lang}"/></td>
                                                    <td class="textabcom" align="left" style="white-space:nowrap;">${fielResult.detalleConsulta.numSerie}</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="21"  style = "background-image :url(/gifs/EnlaceMig/gfo25040.gif);"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table width="430" border="0" cellspacing="0" cellpadding="0"  style="margin: 0 auto;">
                        <tr>
                            <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."/></td>
                        </tr>
                        <tr>
                            <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."/></td>
                        </tr>
                        <tr>
                            <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."/></td>
                        </tr>
                    </table>
                    <table width="430" border="0" cellspacing="0" cellpadding="0"  style="margin: 0 auto;">
                        <tr>
                            <td align="center"><br/>
                                <table width="150" border="0" cellspacing="0" cellpadding="0" height="22">
                                    <tr>
                                        <td align="right" width="83">
                                            <a href="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" width="83" style="height:22; border: 0px;"/></a>
                                        </td>
                                        <td align="left" width="71"><a href="javascript:;" onClick="window.close()"><img src="/gifs/EnlaceMig/gbo25200.gif" width="71" height="22" style="border: 0px"/></a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </c:when>

                <c:otherwise>
                    <table border=0 width=365 class="textabdatcla"  style="margin: 0 auto;" cellspacing=3 cellpadding=0>
                        <tr><th style="height: 25" class="tittabdat" colspan=2><fmt:message key="fiel.comprobante.error" bundle="${lang}"/></th></tr>
                    </table>

                    <table style="border: 0px; margin: 0 auto;" width=365 class="textabdatcla"  cellspacing=0 cellpadding=1>
                        <tr>
                            <td class="tabmovtex1" align=right width=50><img src="/gifs/EnlaceMig/gic25060.gif" style="border: 0px"/></td>
                            <td class="tabmovtex1" align=center width=300><br/><fmt:message key="fiel.comprobante.errorcomprob" bundle="${lang}"/><br/><br/></td>
                        </tr>
                        <tr><td class="tabmovtex1"></td></tr>
                    </table>

                    <table style="margin: 0 auto; border: 0px;" cellspacing=6 cellpadding=0>
                        <tr>
                            <td style="margin: 0 auto;">
                                <a href="javascript:opener.ventanaActiva=0;opener.respuesta=0;window.close();">
                                    <img src="/gifs/EnlaceMig/gbo25200.gif" style="border: 0px"/>
                                </a>
                            </td>
                        </tr>
                    </table>

                </c:otherwise>
            </c:choose>
        </form>
    </body>
</html>
