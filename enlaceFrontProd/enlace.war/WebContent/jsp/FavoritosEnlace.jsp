<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="mx.altec.enlace.utilerias.FavoritosConstantes"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="mx.altec.enlace.servlets.FavoritosEnlaceServlet"%>
<%@page import="mx.altec.enlace.beans.FavoritosEnlaceBean"%>
<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.text.DecimalFormat"%>
<%@page contentType="text/html"%>
<html>
<head>
<title>Favoritos Enlace</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">
<link rel="stylesheet" href="/EnlaceMig/estilosFavoritos.css" />
<link rel="stylesheet" href="/EnlaceMig/consultas.css" />
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/funcionesFavoritos.js"></script>
</head>
<body>
<%

Object hayMensaje = request.getAttribute("hayMensaje");
Object errorMsj = request.getAttribute("Error");
if(!((hayMensaje!=null)&&(hayMensaje.toString().equals("1")))){
%>
	<div style="text-align: center;">
		<table style=" margin: auto;">
		<form action="FavoritosEnlace" name="favoritosForm" id="favoritosForm">
			<input type="hidden" id="<%=FavoritosConstantes.ID_NUM_PAGINA%>"
				name="<%=FavoritosConstantes.ID_NUM_PAGINA%>" /> <input
				type="hidden" id="<%=FavoritosConstantes.ID_OPERACION%>"
				name="<%=FavoritosConstantes.ID_OPERACION%>" /> <input type="hidden"
				id="<%=FavoritosConstantes.ID_NUM_FAVORITO_SEL%>"
				name="<%=FavoritosConstantes.ID_NUM_FAVORITO_SEL%>" /> <input
				type="hidden" id="<%=FavoritosConstantes.ID_NVO_DESC_FAV%>"
				name="<%=FavoritosConstantes.ID_NVO_DESC_FAV%>" />


			<br>
			<%
			String esArchivo = session.getAttribute(FavoritosConstantes.ID_PARAM_ES_ARCH).toString();
			String idModulo = session.getAttribute(
					FavoritosConstantes.ID_MODULO_PARAM).toString();
			boolean esNomina = false;
			if("IN04".equals(idModulo)){
				esNomina = true;
			}
			%>
			<table class="tablaEncabezados,tabfonbla">
			<% if(!esNomina){%>
				<tr>
					<td align="center" class='tittabdat'></td>
					<td align="center" class='tittabdat'>Favoritos</td>
					<td align="center" class='tittabdat'>Cuenta cargo</td>
					<td align="center" class='tittabdat'>Cuenta abono/Movil</td>
					<td align="center" class='tittabdat'>Importe</td>
					<td align="center" class='tittabdat'>Concepto</td>
				</tr>
			<%}else{ %>
			        <td align="center" class='tittabdat'></td>
					<td align="center" class='tittabdat'>Favoritos</td>
					<td align="center" class='tittabdat'>Cuenta cargo</td>
					<td align="center" class='tittabdat'>Importe</td>
					<td align="center" class='tittabdat'>Registros</td>
			<%} %>
				<%
					EIGlobal.mensajePorTrace(
							"Construyendo en el JSP la tabla de favoritos_",
							EIGlobal.NivelLog.INFO);
					List<FavoritosEnlaceBean> favoritos = ((List<FavoritosEnlaceBean>) session
							.getAttribute(FavoritosConstantes.ID_FAVORITOS_PAGINA));
					Iterator<FavoritosEnlaceBean> iFavorito = favoritos.iterator();
					FavoritosEnlaceBean favorito = null;
					DecimalFormat formateador = (DecimalFormat) DecimalFormat
							.getInstance();
					formateador.applyPattern("##0.##");


					int x = 1;
					String colLin = "";
					while (iFavorito.hasNext()) {
						favorito = iFavorito.next();
						if ((x % 2) == 0){
							colLin = "class='textabdatobs' align=center ";
						}
						else{
							colLin = "class='textabdatcla' align=center ";}

						if(FavoritosConstantes.ID_NO_ES_ARCHIVO.equals(esArchivo)){
				%>
				<tr>
					<td <%=colLin%>><input type="radio" name="chkBoxFav"
						onclick="javascript:seleccionUnFavorito(this,<%=x%>);"></input></td>
					<td <%=colLin%>><input type="text" name="txtFav" maxlength="25"
						value="<%=favorito.getDescripcion()%>" disabled="true"></td>
					<td <%=colLin%>><%=favorito.getCuentaCargo()%></td>
					<td <%=colLin%>><%=favorito.getCuentaAbono()%></td>
					<td <%=colLin%>>$<%=formateador.format(favorito.getImporte())%></td>
					<td <%=colLin%>><%=favorito.getConceptoOperacion()%></td>
				</tr>
				<%
						}else{%>
							<tr>
					<td <%=colLin%>><input type="radio" name="chkBoxFav"
						onclick="javascript:seleccionUnFavorito(this,<%=x%>);"></input></td>
					<td <%=colLin%>><input type="text" name="txtFav" maxlength="25"
						value="<%=favorito.getDescripcion()%>" disabled="true"></td>
					<td <%=colLin%>><%=favorito.getCuentaCargo()+" "+favorito.getDescCuentaCargo()%></td>
					<td <%=colLin%>>$<%=formateador.format(favorito.getImporte())%></td>
					<td <%=colLin%>><%=favorito.getNumeroRegistros()%></td>
				</tr>

				<%	}
					x++;
					}
				%>
				<tr>
					<td colspan="5" align="center" class='texfootpagneg'>
						<%
						int numPagina = Integer.parseInt(request.getAttribute(
								FavoritosConstantes.ID_NUM_PAGINA).toString());
						int numeroFavoritos = Integer.parseInt(session.getAttribute(
								FavoritosConstantes.ID_NUM_FAVORITOS).toString());
						int numFavoritosActual = Integer.parseInt(request.getAttribute(
								FavoritosConstantes.ID_FIN_PAGINA).toString());

							if ((numPagina - 1) > 0) {
						%> <a
						href="javascript:irPagina(<%=numPagina - 1%>,'<%=FavoritosConstantes.ID_NUM_PAGINA%>','<%=FavoritosConstantes.ID_OPERACION%>','<%=FavoritosConstantes.ID_PAGINADO%>','<%=idModulo%>');">
							<< </a> <%
	 	}
	 %> <%=numFavoritosActual%> / <%=numeroFavoritos%> <%
	 	if (FavoritosConstantes.NO_ES_PAG_FINAL.equals(request
	 			.getAttribute(FavoritosConstantes.ID_ES_PAGINA_FINAL))) {
	 %> <a
						href="javascript:irPagina(<%=numPagina + 1%>,'<%=FavoritosConstantes.ID_NUM_PAGINA%>','<%=FavoritosConstantes.ID_OPERACION%>','<%=FavoritosConstantes.ID_PAGINADO%>','<%=idModulo%>');">
							>> </a> <%
	 	}
	 %>
					</td>
				</tr>
				<tr>
					<%
						String idNumFav = FavoritosConstantes.ID_NUM_FAVORITO_SEL;
						String idNvoDescFav = FavoritosConstantes.ID_NVO_DESC_FAV;
					%>

					<td colspan="5" align="center">

						<a href="javascript:ejecutarAccion('<%=FavoritosConstantes.ID_REDIR%>','<%=FavoritosConstantes.ID_OPERACION%>','<%=idNumFav%>','<%=idNvoDescFav%>','<%=idModulo%>');" style="border: 0;">
								<img src="/gifs/EnlaceMig/gbo25280.gif" alt="Aceptar" style="border: 0;" />
						</a><a href="javascript:ejecutarAccion('<%=FavoritosConstantes.ID_MODIFICAR%>','<%=FavoritosConstantes.ID_OPERACION%>','<%=idNumFav%>','<%=idNvoDescFav%>','<%=idModulo%>');" style="border: 0;">
								<img src="/gifs/EnlaceMig/gbo25510.gif" alt="Modificar" style="border: 0;" />
						</a><a href="javascript:ejecutarAccion('<%=FavoritosConstantes.ID_BORRAR%>','<%=FavoritosConstantes.ID_OPERACION%>','<%=idNumFav%>','<%=idNvoDescFav%>','<%=idModulo%>');" style="border: 0;">
								<img src="/gifs/EnlaceMig/gbo25540.gif" alt="Borrar" style="border: 0;" />
						</a><a href="javascript:window.close();" style="border: 0;">
								<img src="/gifs/EnlaceMig/gbo25190.gif" alt="Cerrar" style="border: 0;" />
						</a>

					</td>
				<tr>
				<tr>
			</table>
		</form>
	 </table>
	</div>
<%}else { %>
<script type="text/javascript">
    cuadroDialogoFav("<%=errorMsj.toString()%>");
	window.close();
</script>
<%}%>
</body>
</html>