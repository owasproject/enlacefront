

<%@ page import="java.io.*"%>
<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<%@page import="mx.altec.enlace.utilerias.Global"%>
<%@page import="mx.altec.enlace.bo.ArchivoRemoto"%>
<HTML>
<HEAD>
<TITLE>Banca Virtual</TITLE>
<script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script language="Javascript" src="/EnlaceMig/scrImpresion.js"></script>

<script language="JavaScript">

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function formatea(para)
{
	var formateado = "";
	var car = "";

	for(a2=0;a2<para.length;a2++)
	{
		if(para.charAt(a2)==' ')
			car="+";
		else
			car=para.charAt(a2);
			formateado+=car;
	}
	return formateado;
}

function GenerarLiga(trans)
{
	var tipo=trans;
	var web_application=document.operacionrealizada.web_application.value;
	//vswf:meg cambio de NASApp por Enlace 08122008
	document.operacionrealizada.action="/Enlace/"+web_application+"/transferencia?ventana=10&trans="+tipo;
	document.operacionrealizada.submit();
}

<% /*CODIGO QUE DESPLIEGA EL COMPROBANTE SLF IM314336 */%>

function Comprobante()
{
<%
	System.out.println("*** ==> Entrando a MCL_EfectuaPagos.jsp");
  	out.println("ventanaC=window.open(\"/Download/"+request.getAttribute("NumUsuario")+"_PLC.html\",'Comprobante','width=450,height=470, scrollbars=yes')");

   	java.io.File Archivo = null;
   	java.io.FileWriter outf;
   	try{
  		Archivo = new java.io.File(Global.DOWNLOAD_PATH+request.getAttribute("NumUsuario")+"_PLC.html");
  		outf = new java.io.FileWriter(Archivo);
		outf.write((String)request.getAttribute("Comprobante"));
		outf.close();
   	}
   	catch(IOException e){
   		e.printStackTrace();
   	}

       try {
           
            ArchivoRemoto envArch = new ArchivoRemoto();
            // Archivo de configuracion

	    EIGlobal.mensajePorTrace("Global.DIRECTORIO_LOCAL = " + Global.DIRECTORIO_LOCAL, EIGlobal.NivelLog.DEBUG);
	    EIGlobal.mensajePorTrace("Global.DIRECTORIO_REMOTO_WEB = " + Global.DIRECTORIO_REMOTO_WEB, EIGlobal.NivelLog.DEBUG);
	    EIGlobal.mensajePorTrace("Global.HOST_LOCAL = " + Global.HOST_LOCAL, EIGlobal.NivelLog.DEBUG);
	    EIGlobal.mensajePorTrace("Global.HOST_REMOTO_WEB = " + Global.HOST_REMOTO_WEB, EIGlobal.NivelLog.DEBUG);

        if (Global.HOST_LOCAL.equals (Global.HOST_REMOTO_WEB) && Global.DIRECTORIO_LOCAL.equals (Global.DIRECTORIO_REMOTO_WEB)) 
		{
		 EIGlobal.mensajePorTrace("Envia. Hosts Remoto-Local y Rutas remota-local iguales, no se hace copia remota", EIGlobal.NivelLog.DEBUG);

	    }
	    else {
		EIGlobal.mensajePorTrace("Envia. Pasa validacion para generar copia remota", EIGlobal.NivelLog.DEBUG);
	
       	     //Validacion NAS
              
               	if(!envArch.copiaLocalARemoto(Archivo.getName(),"WEB"))
				{
    				
    			     EIGlobal.mensajePorTrace("*** Archivos.envia  no se pudo copiar archivo remoto_web:" + Archivo.getName(), EIGlobal.NivelLog.ERROR);
    		    }
    			else 
				{
    				 EIGlobal.mensajePorTrace("*** Archivos.envia archivo remoto copiado exitosamente:" + Archivo.getName(), EIGlobal.NivelLog.DEBUG);
    				    
    			}
           }
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
        }



%>
	ventanaC.focus();
}



function GenerarComprobante(datos,indice)
{
	var trama=obtentrama(datos,indice);
	var tipo=trama.substring(0,trama.indexOf('|'));
	trama=trama.substring(trama.indexOf('|')+1,trama.length);
	var sreferencia=trama.substring(0,trama.indexOf('|'));
	trama=trama.substring(trama.indexOf('|')+1,trama.length);
	var importe_string=trama.substring(0,trama.indexOf('|'));
	trama=trama.substring(trama.indexOf('|')+1,trama.length);
	var cta_origen=trama.substring(0,trama.indexOf("|"));
	trama=trama.substring(trama.indexOf('|')+1,trama.length);
	var cta_destino=trama.substring(0,trama.indexOf("|"));
	trama=trama.substring(trama.indexOf('|')+1,trama.length);
	var concepto="";
	var rfc="";
	var iva="";
	var plazo="";
	var tipo_inversion="";

	if ((tipo=="0")||(tipo=="1")||(tipo=="2"))
	{

	    concepto=trama.substring(0,trama.indexOf("|"));

		if(trama.indexOf('|')>0)
		{
			trama=trama.substring(trama.indexOf('|')+1,trama.length);
		    rfc=trama.substring(0,trama.indexOf("|"));
		    trama=trama.substring(trama.indexOf('|')+1,trama.length);
		     iva=trama;
		     if(trama.indexOf("@")>0)
			   iva=trama.substring(0,trama.indexOf("@"));
		     else
             {
			   if(iva!="@")
			     iva=trama;
			   else
                 iva="";
             }
		}
		else
        {
		  rfc="";
		  iva="";

}

	}
	else if (tipo=="5")
    {
        concepto=trama.substring(0,trama.indexOf("|"));
        trama=trama.substring(trama.indexOf('|')+1,trama.length);
		if(trama.indexOf('|')>0)
		{
          	plazo=trama.substring(0,trama.indexOf("|"));
		    tipo_inversion=trama.substring(trama.indexOf('|')+1,trama.length);
    	}
    }
	else
	  concepto=trama;

	//     termina Cambio Comprobante fiscal
	var cta_origen1=cta_origen;
	var cta_destino1=cta_destino;
	var concepto1=concepto;
	var tipo_inversion1=tipo_inversion;
	var usuario=document.operacionrealizada.datosusuario.value;
	var contrato=document.operacionrealizada.datoscontrato.value;
	cta_origen=formatea(cta_origen1);
	cta_destino=formatea(cta_destino1);
	concepto=formatea(concepto1);
	tipo_inversion=formatea(tipo_inversion1);
	usuario=formatea(usuario);
	contrato=formatea(contrato);
	var web_application=document.operacionrealizada.web_application.value;
	var banco=document.operacionrealizada.banco.value;
	 //vswf:meg cambio de NASApp por Enlace 08122008

	operacion= "/Enlace/"+web_application+"/comprobante_trans?tipo="+tipo;
	operacion+="&refe="+sreferencia+"&importe="+importe_string+"&cta_origen="+cta_origen;
   operacion+="&cta_destino="+cta_destino+"&enlinea=s&datosusuario="+usuario+"&datoscontrato="+contrato+"&concepto="+concepto+"&rfc="+rfc+"&iva="+iva+"&banco="+banco+"&plazo="+plazo+"&tipoinversion="+tipo_inversion; //     Cambio Comprobante fiscal

	ventanaInfo2=window.open(operacion,'Comprobante','width=450,height=470, scrollbars=yes');
	ventanaInfo2.focus();
}

function obtentrama(datos,indice)
{
	var ind=0;
	var cadena_retorno="";

	for(i=1;i<=eval(document.operacionrealizada.contadorok.value);i++)
	{
		ind=datos.substring(0,datos.indexOf("|"));
		if (eval(ind)==eval(indice))
		{
			cadena_retorno=datos.substring(datos.indexOf("|")+1,datos.indexOf("@"));
			break;
		}
		if (i<eval(document.operacionrealizada.contadorok.value))
		{
			datos=datos.substring(datos.indexOf("@")+1,datos.length);
		}
	}
	return cadena_retorno;
}

<%
     if (request.getAttribute("newMenu")!= null) {
     out.println(request.getAttribute("newMenu"));
     }
%>

</script>
<script language="JavaScript1.2" src = "/EnlaceMig/fw_menu.js"></script>
<script language="JavaScript">
</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
      <!-- MENU PRINCIPAL -->
		<%
          if (request.getAttribute("MenuPrincipal")!= null) {
          out.println(request.getAttribute("MenuPrincipal"));
          }
//	  System.out.println("MCL_EfectuaPagos.jsp: MenuPrincipal:" + request.getAttribute("MenuPrincipal"));
		%>

	</TR>
</TABLE>
		<%
		String Encabezado = "";
		  if(session.getAttribute("Encabezado")!=null)
			Encabezado = (String)session.getAttribute("Encabezado");
		  else if (request.getAttribute("Encabezado")!= null)
			Encabezado= (String)request.getAttribute("Encabezado");
		  out.println(Encabezado);

		%>


<!--TABLA PRINCIPAL-->
<table width="760" border="0" cellspacing="0" cellpadding="0">
	<form name="operacionrealizada" method="post" >

 <tr><td class="textabref" colspan="6"> Resultado de la Operaci&oacute;n </td></tr>
  <tr>
      <td align="center">
		<table width="750" border="0" cellspacing="2" cellpadding="3">
<TR>
 <TD class=tittabdat align=center width=200>Contrato</TD>
 <TD class=tittabdat align=center colspan=2>Usuario</TD>
 <TD class=tittabdat align=center width=90>Fecha y Hora</TD>
 <TD class=tittabdat align=center width=70>Importe</TD>
 <TD class=tittabdat align=center>Concepto</TD>
 <TD class=tittabdat align=center>Referencia</TD>
 <TD class=tittabdat align=center>Cuenta de cargo</TD>
 <TD class=tittabdat align=center>N&uacute;mero de cr&eacute;dito</TD>
 <TD class=tittabdat align=center>Estatus</TD>
</TR>
<TR>
 <TD class=textabdatcla align=left  nowrap >
				    <%
					String numContrato = "";

					if(session.getAttribute("NumContrato")!=null)
					  numContrato = (String)session.getAttribute("NumContrato");
					else
					   if(request.getAttribute("NumContrato")!= null)
					     numContrato = (String)request.getAttribute("NumContrato");
					out.println(numContrato);
                    %>
 </TD>
 <TD class=textabdatcla nowrap align=left colspan=2 >
 				    <%



					String numUsuario = "";

					if(session.getAttribute("NumUsuario")!=null)
					  numUsuario = (String)session.getAttribute("NumUsuario");
					else
					  if (request.getAttribute("NumUsuario")!= null)
					    numUsuario = (String)request.getAttribute("NumUsuario");
				     out.println(numUsuario);
                    %>
 </TD>
 <TD class=textabdatcla align=right nowrap >
 				    <%  String fechaHora = "";






					if(session.getAttribute("FechaHora")!=null)
					  fechaHora = (String)session.getAttribute("FechaHora");
					else
					  if (request.getAttribute("FechaHora")!= null)
					    fechaHora = (String)request.getAttribute("FechaHora");
				    out.println(fechaHora);
				    %>
 </TD>
 <TD class=textabdatcla align=center nowrap>
 				   <%



				   String importe = "";

				   if(session.getAttribute("Importe")!=null)
				     importe = (String)session.getAttribute("Importe");
				   else
				     if (request.getAttribute("Importe")!= null)
					   importe = (String)request.getAttribute("Importe");
				   out.println(importe);
				   %>
 </TD>
 <TD class=textabdatcla align=left nowrap>
 				   <%  String concepto = "";



				   if(session.getAttribute("Concepto")!=null)
				     concepto = (String)session.getAttribute("Concepto");
				   else
				     if (request.getAttribute("Concepto")!= null)
					   concepto = (String)request.getAttribute("Concepto");
				   out.println(concepto);
                   %>
 </TD>
  <TD class=textabdatcla align=left nowrap>
  				   <%  String referencia = "";
				   if(session.getAttribute("Referencia")!=null)
				     referencia = (String)session.getAttribute("Referencia");
				   else
				     if (request.getAttribute("Referencia")!= null)
					   referencia = (String)request.getAttribute("Referencia");

				   if (session.getAttribute("TipoFirma").equals("REALIZADA")){
				   		%>
						<a href="javascript:Comprobante();" border=0>
						<%out.println(referencia);%>
						</a>
				   		<%
				   }
				   else
				   		out.println(referencia);
                   %>

 </TD>
 <TD class=textabdatcla align=left nowrap>
 				   <%



				   String chequera = "";
				   if(session.getAttribute("Chequera")!=null)
				     chequera = (String)session.getAttribute("Chequera");
				   else
				     if (request.getAttribute("Chequera")!= null)
					   chequera = (String)request.getAttribute("Chequera");
				   out.println(chequera);
                   %>
 </TD>
 <TD class=textabdatcla align=left nowrap>
 				   <%



				   String cuenta = "";

				   if(session.getAttribute("Cuenta")!=null)
				     cuenta = (String)session.getAttribute("Cuenta");
				   else
				     if (request.getAttribute("Cuenta")!= null)
					    cuenta = (String)request.getAttribute("Cuenta");
				   out.println(cuenta);
                   %>
 </TD>
 <TD class=textabdatcla align=left nowrap>
 				   <%
				   String titulo = "";

				   if(session.getAttribute("titulo")!=null)
				     titulo = (String)session.getAttribute("titulo");
				   else
				     if (request.getAttribute("titulo")!= null)
					    titulo = (String)request.getAttribute("titulo");
				   out.println(titulo);
                   %>
 </TD>
</TR>
</table>
	  <table width="87" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
		<tr>
		  <td align="center"><a href="javascript:scrImpresion();" border=0><img border=0  src="/gifs/EnlaceMig/gbo25240.gif"></a></td>
		</tr>
	  </table>
	  <br>
	</td>
  </tr>
</form>
</table>
		<!-- CONTENIDO FINAL -->
</body>
</html>
<Script language = "JavaScript">
<!--
function VentanaAyuda(ventana)
{
	hlp=window.open("/EnlaceMig/ayuda.html#" + ventana ,"hlp","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
    hlp.focus();
}
//-->
</Script>