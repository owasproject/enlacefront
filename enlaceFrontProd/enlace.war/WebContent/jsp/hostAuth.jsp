<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<%@ page import="java.util.UUID" %>
<%@page import="mx.altec.enlace.bo.BaseResource"%>
<html>
<head>
<title>Bienvenido a Enlace Internet</title>
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s26050">
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">	
	<c:if test="${empty sessionScope.tsid}">
		<c:set var="tsid" scope="session"><%=UUID.randomUUID().toString()%></c:set>
	</c:if>
	<c:if test="${!empty sessionScope.session}">
		<c:set var="usrsess8" value="${sessionScope.session.userID8}" scope="page"/>
	</c:if>
	<script type="text/javascript">
		function data1(){return '${sessionScope.tsid}';}
		<c:if test="${!empty pageScope.usrsess8}">
		function data2(){return JSON.parse('{"p":["${pageScope.usrsess8}"]}');}
		</c:if>
	</script>
	<script src="/EnlaceMig/common.js" type="text/javascript"></script>
<!-- <SCRIPT SRC="/EnlaceMig/validate.js" LANGUAGE="JavaScript"></script> -->
<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script language = "JavaScript" SRC= "/EnlaceMig/Convertidor.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="Javascript">
//CSA
function oculta() {
	document.getElementById("enviar").style.visibility="hidden";
	document.getElementById("enviar2").style.visibility="visible";
}

function visualiza() {
	document.getElementById("enviar").style.visibility="visible";
	document.getElementById("enviar2").style.visibility="hidden";
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
// distintivo
function isDigit (c)
{
  return ((c >= "0") && (c <= "9"))
}


</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<style type="text/css">
<!--
td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: normal;
	color: #666666;
	text-decoration: none;
}
.titulo {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #FF0000;
	text-decoration: none;
}
.tituloCopy {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 19px;	
	color: #666666;
	text-decoration: none;
	font-weight:demi-bold;
}

.leyenda {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 19px;	
	color: #000000;
	
}

-->
</style>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('gifs/EnlaceMig/gbo25131.gif','gifs/EnlaceMig/gbo25151.gif','gifs/EnlaceMig/gbo25171.gif','gifs/EnlaceMig/gbo25111.gif','gifs/EnlaceMig/gba25020.gif')" background="/gifs/EnlaceMig/gfo25010.gif">
<div id="Layer1" style="position:absolute; left:315px; top:96px; width:316px; height:31px; z-index:1"><img src="/gifs/EnlaceMig/gti25030.gif" width="332" height="40"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="596" valign="top">
      <table width="596" border="0" cellspacing="0" cellpadding="0" class="tabfonbla">
        <tr valign="top">
          <td rowspan="2"><img src="/gifs/EnlaceMig/glo25010.gif" width="237" height="41"></td>
          <td rowspan="2"><a href="javascript:;" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contactanos','','gifs/EnlaceMig/gbo25111.gif',1)"><img src="/gifs/EnlaceMig/gbo25110.gif" width="30" height="33" name="contactanos" border="0"></a></td>
          <td rowspan="2"><img src="/gifs/EnlaceMig/gbo25120.gif" width="59" height="20" alt="Cont&aacute;ctenos"></td>
          <td rowspan="2"><a href="javascript:;" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('atencion','','gifs/EnlaceMig/gbo25131.gif',1)"><img src="/gifs/EnlaceMig/gbo25130.gif" width="36" height="33" name="atencion" border="0"></a></td>
          <td rowspan="2"><img src="/gifs/EnlaceMig/gbo25140.gif" width="90" height="20" alt="Atenci&oacute;n telef&oacute;nica"></td>
          <td rowspan="2"><a href="javascript:;" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('centro','','gifs/EnlaceMig/gbo25151.gif',1)"><img src="/gifs/EnlaceMig/gbo25150.gif" width="33" height="33" name="centro" border="0"></a></td>
          <td rowspan="2"><img src="/gifs/EnlaceMig/gbo25160.gif" width="93" height="20" alt="Centro de mensajes"></td>
          <td width="18" bgcolor="#DE0000"><img src="/gifs/EnlaceMig/gau25010.gif" width="18" height="17"></td>
        </tr>
        <tr valign="top">
          <td><img src="/gifs/EnlaceMig/gau25010.gif" width="18" height="24"></td>
        </tr>
      </table>
      <table border="0" cellspacing="6" cellpadding="0" align="right" class="tabfonbla">
        <tr>
          <td valign="bottom" class="texencfec" rowspan="2" width="400"><img src="/gifs/EnlaceMig/gti25020.gif" width="355" height="48"></td>
          <!--Etiqueta fechaHoy exhibe la fecha del sistema-->
          <td nowrap align="right" valign="bottom" class="texencfec" height="12">
			<% java.util.Date dt = new java.util.Date(); %>
			<%
			String mes;
			switch (dt.getMonth()+1 )
			{
				case  1: mes = "Enero";		 break;
				case  2: mes = "Febrero";	 break;
				case  3: mes = "Marzo";		 break;
				case  4: mes = "Abril";		 break;
				case  5: mes = "Mayo";       break;
				case  6: mes = "Junio";		 break;
				case  7: mes = "Julio";		 break;
				case  8: mes = "Agosto";	 break;
				case  9: mes = "Septiembre"; break;
				case 10: mes = "Octubre";	 break;
				case 11: mes = "Noviembre";	 break;
				case 12: mes = "Diciembre";  break;
				default: mes = "Enero";
			}

			String strDia = "";
			switch (dt.getDay())
			{
				case  0: strDia = "Domingo";   break;
				case  1: strDia = "Lunes";     break;
				case  2: strDia = "Martes";    break;
				case  3: strDia = "Miercoles"; break;
				case  4: strDia = "Jueves";    break;
				case  5: strDia = "Viernes";   break;
				case  6: strDia = "Sabado";    break;
			}

			int anio = dt.getYear()+1900;
			String mifecha = strDia +" " + dt.getDate() + " de " +  mes + " del "+anio;

			String minutos_ = ""+dt.getMinutes();
			if(minutos_.trim().length()==1)
				minutos_ = "0"+minutos_;
			String mihora = dt.getHours()+":"+minutos_;

			%>
			<%=mifecha %>
			<%-- request.getAttribute("fechaHoy") --%>
		  </td>
		  <!--*********************************************-->
        </tr>
        <tr>
		  <!--Etiqueta horaHoy exhibe la fecha del sistema-->
          <td class="texenchor" align="right" valign="top" nowrap>
		    <%=mihora%>
			<%-- = request.getAttribute("horaHoy") --%>
		  </td>
		  <!--*********************************************-->
        </tr>
      </table>
    </td>
  
    
    
    <td width="100%" valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
        <tr>
          <td bgcolor="#DE0000"><img src="/gifs/EnlaceMig/gau25010.gif" width="35" height="17"></td>
          <td align="center" rowspan="2"><img src="/gifs/EnlaceMig/glo25020.gif" width="57" height="41" alt=""></td>
          <td width="100%" bgcolor="#DE0000"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="17"></td>
        </tr>
        <tr>
          <td><img src="/gifs/EnlaceMig/gau25010.gif" width="35" height="24"></td>
          <td><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="24"></td>
        </tr>
      </table>
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="right" class="tabfonbla" width="126"><img src="/gifs/EnlaceMig/gba25010.gif" width="126" height="33" alt="Sitio seguro"></td>
          <td align="right" class="tabfonbla" rowspan="2" width="50">&nbsp;</td>
        </tr>
		<tr>
		
          <td align="right"><a href="/Enlace/enlaceMig/logout" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('finsesion','','gifs/EnlaceMig/gbo25181.gif',1)">
				<img src="/gifs/EnlaceMig/gbo25180.gif" width="44" height="49" name="finsesion" border="0"></a></td>


        </tr>
	  </table>

    </td>
  </tr>
</table>

	<table width="800">
		<tr>	
			<td>		
				<table width="800">	
					<tr>
				
							<td align="center" width="232" >
								<img src="/gifs/EnlaceMig/logocirculo.png" width="200" height="190">
							</td>
						
						    <td  align="center" width="336"> 
								  <table width="94%" border="0" cellspacing="0" cellpadding="0">
											<tr>
											 	 <td height="50" align="center" class="titulo">&nbsp;</td>
											</tr>					
												  <td height="30" align="left" class="titulo"> 
												     <img src="/gifs/EnlaceMig/AutSitio.png" width="500" height="38">
												  </td>					
											<tr>
												  <%
													String templateElegido = (String) request.getSession().getAttribute("plantilla");
													BaseResource sessions = (BaseResource) request.getSession().getAttribute("session");
													//System.out.println("plantilla1: " + templateElegido);
										
												  %>
												  <!-- vswf:meg cambio de NASApp por Enlace 08122008 -->     
									
											</tr>
											<tr>
												 <td>
												   <table width="100%" border="0" cellspacing="0" cellpadding="0">
														     <tr valign="top">
																<td>
																</td>
																<td>
																	<P align="justify" class="leyenda">
																    Si el valor mostrado en esta pantalla no es el mismo que el mostrado por su Token, es posible que se encuentre en un sitio <strong>no autorizado</strong>. Por su seguridad, cierre la sesi�n dando click en "Finalizar sesi�n" e ingrese nuevamente a trav�s de nuestro panel en 
																</td>
																<td>
																</td>           
														     </tr>
														     <tr>
																<td>
																	&nbsp;
																</td>
														     </tr>
												   </table><p>
												 </td>
											</tr>
								  </table>
							</td>
							<td align="center" width="232" >
								
							</td> 

					</tr>							
				</table>
			</td>
			
		  </tr>
		  
		  
		  
		  
		  <tr>
		     <td>
			  	<table>
			  		<tr>		  
				  	  <td>				  	    
					  	<table>
					  		<tr>		  
						  	  <td>				  	    
								  	  <table>
								  	  	 <tr>	  								        	
										  	<td align="right">							
										        <table>
											        <tr>
											        	<td width="212">							        		
															  
											      		</td>
											        	<td width="288">							        		
															  <P valign="top" align="right" class="leyenda">www.santander.com.mx</P>
											      		</td>
											      	</tr>
											    </table>
										    </td>
										 </tr>						    
								  	  	 <tr> 				  	
											<td align="right" width="570"> 
												<table border="0" cellspacing="0" cellpadding="0">
											        <tr>
											          <td width="2"><img src="/gifs/EnlaceMig/img001b.gif" width="173" height="49"></td>
											          <td width="2"><img src="/gifs/EnlaceMig/img4.gif" width="42" height="49"></td>
											          <td width="2"><img src="/gifs/EnlaceMig/img7.gif" width="97" height="49"></td>
											          <td><img src="/gifs/EnlaceMig/img12c.gif" width="39" height="49"></td>
											        </tr>
											        <tr>
											          <td><img src="/gifs/EnlaceMig/img2b.gif" width="173" height="56"></td>
											          <td><a href="#"><img src="/gifs/EnlaceMig/img5_an.gif" width="42" height="56" border="0"></a></td>
											          <td>
											          	<table width="100%" border="0" cellspacing="0" cellpadding="0">
											              <tr>
											                <td><img src="/gifs/EnlaceMig/img8.gif" width="97"></td>
											              </tr>
											              <tr>
											                <td>
											                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
											                    <tr>
											                      <td width="2"><img src="/gifs/EnlaceMig/img9.gif" width="18"></td>
											                          <%
																      BaseResource sess = (BaseResource) request.getSession().getAttribute("session");
																      %>
											
																      <td align="center" bgcolor="#A3B28E">
																	      <font color="#000000">
																		      <strong>	
																		      		<%=sess.getToken().getRetCode()%>
																              </strong>
																	      </font>
																      </td>
											                    </tr>
											                  </table></td>
											              </tr>
											              <tr>
											                <td><img src="/gifs/EnlaceMig/img10.gif" width="97"></td>
											              </tr>
											            </table></td>
											          <td><img src="/gifs/EnlaceMig/img13c.gif" width="48" height="56"></td>
											        </tr>
											        <tr>
											          <td><img src="/gifs/EnlaceMig/img3.gif" width="173" height="30"></td>
											          <td><img src="/gifs/EnlaceMig/img6.gif" width="42" height="30"></td>
											          <td><img src="/gifs/EnlaceMig/img11.gif" width="97" height="30"></td>
											          <td><img src="/gifs/EnlaceMig/img14c.gif" width="49" height="30"></td>
											        </tr>	  	
											    </table>  	
											    &nbsp;&nbsp;&nbsp;&nbsp;
											</td>
										 </tr>
									  </table>	
						      </td>
						      <td align="left" width="230">			  
								 <img src="/gifs/EnlaceMig/logoestrella.png" width="200" height="200">			  			  
  						      </td>
							</tr>
							<tr>
							  <td>
							  		<table>
							  			<!-- CSA -->
<tr align="center"  id="enviar2" style="visibility:hidden" class="tabmovtex">
					<td align="right">Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>
				</tr>
										<tr valign="top">
								      		<td align="right" width="480" id="enviar"><br>
								      			<% 
								      			System.out.println("Host Auth----->"+sessions.isValidaRSA());
								      			if(sessions.isValidaRSA()) {%>
								      				<a href="/Enlace/enlaceMig/EnrolamientoRSAServlet" onclick=oculta();>
										      			<img src="/gifs/EnlaceMig/gbo25280.gif" width="84" height="26" border="0">
										      		</a>
										      	<% } else {%>
									      		<a href="/Enlace<%=templateElegido%>" onclick=oculta();>
									      			<img src="/gifs/EnlaceMig/gbo25280.gif" width="84" height="26" border="0">
										      		</a>
										      	<% }%>
								      		</td>	
								      		<td>							
											</td>						    
										<tr>		
								    </table> 
							  </td>
							</tr>
						</table>					
				  	  </td>			 	  		 
					</tr>
	            </table>			
	        </td>  
		  
		  
		  	
				
	      </tr>          
	      <tr>
	        
	      </tr>	
	</table>


<!--
<table border='0' cellpadding='0' cellspacing='0' align='right' background="/gifs/EnlaceMig/fondo.gif" width='155' height='43'>
 <tr>
  <td width="123" valign=bottom>
	 <table border='0' cellpadding='0' cellspacing='0' width="123"  background="" >
	  <tr>
	    <td width=15><br></td>
	  </tr>
	  <tr>
	    <td height="15">&nbsp;</td>
	  </tr>
	 </table>
  </td>
 </tr>
</table>
//-->
</body>
</html>
<script>
<%= request.getAttribute("MensajeLogin01") %>
</script>