
	<%@ page import="java.util.StringTokenizer" %>
	<%@ page import="java.util.Vector" %>
	<%@ page import="mx.altec.enlace.bo.TI_CuentaDispersion" %>

	<%
			// --- Obtenci�n de par�metros -----------------------------------------------------
			String parMenuPrincipal = (String)request.getAttribute("MenuPrincipal");
			String parFuncionesMenu = (String)request.getAttribute("newMenu");
			String parEncabezado = (String)request.getAttribute("Encabezado");
			String parCuentas = (String)request.getAttribute("Cuentas");
			String parCuentasDesc = (String)request.getAttribute("CuentasDesc");
			String parMensaje = (String)request.getAttribute("Mensaje");
			String parTrama = (String)request.getAttribute("Trama");
			String parNuevo = (String)request.getAttribute("Nuevo");

			if(parMenuPrincipal == null) parMenuPrincipal = "";
			if(parFuncionesMenu == null) parFuncionesMenu = "";
			if(parEncabezado == null) parEncabezado = "";
			if(parCuentas == null) parCuentas = "";
			if(parCuentasDesc == null) parCuentasDesc = "";
			if(parMensaje == null) parMensaje = "";
			if(parTrama == null) parTrama = "";
			if(parNuevo == null) parNuevo = "";

			// --- Se preparan otras variables -------------------------------------------------
			Vector cuentas = ((new mx.altec.enlace.servlets.TIDispersion()).creaCuentas(parCuentas, request));
			String nombreCuenta = parTrama.substring(1,parTrama.indexOf("|"));
			int cont;
			for(cont=0;cont<cuentas.size() && !((TI_CuentaDispersion)cuentas.get(cont)).getNumCta().equals(nombreCuenta);cont++);
			TI_CuentaDispersion cuenta = (TI_CuentaDispersion)cuentas.get(cont);
			boolean esEditable = !(cuenta.nivel() == 1 && cuenta.getTipo() != 2 && cuenta.getTipo() != 3);

	%>

<!--== COMIENZA CODIGO HTML ==========================================================-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>

<HEAD>
	<TITLE>Bienvenido a Enlace Internet</TITLE>
	<META NAME="Generator" CONTENT="EditPlus">
	<META NAME="Author" CONTENT="Rafael Villar Villar">
	<LINK rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

	<!-- Scripts =====================================================================-->
	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
	<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
	<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
	<SCRIPT>

	// --- BOTONES ---------------------------------------------------------------------
	function modificar()
		{
		var trama = "@";
		var a, b=0;
		var esEditable = <%= esEditable %>;

		for(a=0;a<document.Forma.PerHoras.options.length;a++)
			if(document.Forma.PerHoras.options[a].selected) b++;

        //Se comento la linea ya que la validaci�n era erronea para lo concentrado
        //CCB 13Feb2004
		//if(<%= cuenta.estaEnConcentracion() %>) {document.Forma.TipoDis[1].checked = true;}

		//Se valida
		if(esEditable && document.Forma.TipoDis[1].checked && !DispersionEsValido())
			{aviso("Indique una cantidad a dispersar"); return;}
		if(b == 0 && esEditable) {aviso("Debe seleccionar al menos una hora"); return;}
		if(document.Forma.PerDias.options[5].selected){
			if(!document.Forma.PerHoras.options[0].selected){
				if(!document.Forma.PerHoras.options[1].selected){
					aviso("Para el s&aacute;bado debe seleccionar s&oacute;lo el horario de las 6:00 o las 7:00"); return;
				}
			}
		}
		if (!esEditable && (b>0 || document.Forma.Dispersion.value != "") && habilitaConfirma)
			{
			confirma("Ha seleccionado modificar una cuenta padre nivel 1,\n se van a ignorar los datos sobre periodicidad y condiciones de dispersion");
			accionConfirma = 1;
			return;
			}

		if(!esEditable)
			{
			document.Forma.Periodicidad[0].checked = true;
			document.Forma.TipoDis[1].checked = true;
			document.Forma.Dispersion.value = "$0.00";
			}

		//Se construye la trama
		trama += "<%= cuenta.getNumCta() %>" + "|";
		trama += "<%= (cuenta.posiblePadre.equals(""))?" |":(cuenta.posiblePadre + "|") %>";
		trama += "<%= cuenta.getDescripcion() %>" + "|";
		// YHG Modiifca manera de obtener dias
		trama += (document.Forma.Periodicidad[0].checked)?
					"0|":(document.Forma.PerDias.value) + "|";
		trama += (document.Forma.TipoDis[0].checked)?"0.0|S":(limpiaDispersion()+"|N");
		trama += "<%= (cuenta.estaEnConcentracion())?"|S":"|N" %>";
		trama += "|" + "<%= cuenta.getTipo() %>";
		if(esEditable)
			for(a=0;a<document.Forma.PerHoras.options.length;a++)
				if(document.Forma.PerHoras.options[a].selected)
					trama += "|" + document.Forma.PerHoras.options[a].text;
		document.Forma.Accion.value = "EDITA_MODIFICA";
		document.Forma.Trama.value = trama;
		document.Forma.submit();
		}

	function anterior()
		{
		document.Forma.Trama.value = "<%= parTrama.substring(1,parTrama.indexOf("|")) %>"
		document.Forma.Accion.value = "EDITA_ANTERIOR";
		document.Forma.submit();
		}

	function siguiente()
		{
		document.Forma.Trama.value = "<%= parTrama.substring(1,parTrama.indexOf("|")) %>"
		document.Forma.Accion.value = "EDITA_SIGUIENTE";
		document.Forma.submit();
		}

	function regresar()
		{
		document.Forma.Accion.value = "REGRESA";
		document.Forma.submit();
		}

	// --- Validaciones ----------------------------------------------------------------
	function trataDiario()
		{
		if(document.Forma.Periodicidad[0].checked)
			{
			document.Forma.PerDias.disabled = true;
			document.Forma.PerDias.selectedIndex = -1;
			}
		else
			{
			document.Forma.PerDias.disabled = false;
			document.Forma.PerDias.selectedIndex = 0;
			}
		}

	function validaFoco()
		{
		if(document.Forma.Periodicidad[0].checked) document.Forma.PerHoras.focus();
		}

	function limpiaDispersion()
		{
		var valor = quitaComas(document.Forma.Dispersion.value);
		var car;
		while(valor.length>0 && valor.substring(0,1) == " ") valor = valor.substring(1);
		while(valor.length>0 && valor.substring(valor.length-1) == " ")
			valor = valor.substring(0,valor.length-1);
		if(valor.substring(0,1)=="$") valor=valor.substring(1);
		while(valor.length>0 && valor.substring(0,1) == " ") valor = valor.substring(1);
		while(valor.length>0 && valor.substring(valor.length-1) == " ")
			valor = valor.substring(0,valor.length-1);
		return valor;
		}

	function DispersionEsValido()
		{
		var valor = limpiaDispersion();
		if(valor.substring(0,1)== ".") return false;
		if(valor.indexOf(".")!=-1) if(valor.indexOf(".")+3 < valor.length) return false;
		if(valor == "") return false;
		while(valor.length > 0)
			{
			car = valor.substring(0,1);
			if(isNaN(parseInt(car)) && car!="0" && car!=".") return false;
			valor = valor.substring(1);
			}
		return true;
		}


	function quitaComas(texto)
		{
		for(a=0;a<texto.length;a++)
			if(texto.charAt(a) == ',') {texto = texto.substring(0,a) + texto.substring(a+1); a--;}
		return texto;
		}

	function condiciones()
		{
		//if(<%= !cuenta.estaEnConcentracion() %>)
		if(!<%=cuenta.estaEnConcentracion() %>)
			{
			document.Forma.TipoDis[0].disabled = true;
			document.Forma.TipoDis[1].checked = true;
			}
		else
			{
			document.Forma.TipoDis[0].disabled = false;
			}
		habImporte()
		}

	function habImporte()
		{
		if(document.Forma.TipoDis[1].checked)
			{
			document.Forma.Dispersion.disabled = false;
			}
		else
			{
			document.Forma.TipoDis[0].focus();
			document.Forma.Dispersion.value = "";
			document.Forma.Dispersion.disabled = true;
			}
		}

	// --- Para cuadros de di�logo -----------------------------------------------------
	var respuesta = 0;
	var accionConfirma;
	var habilitaConfirma = true;

	function continua()
		{
		if(accionConfirma == 1)
			{
			if(respuesta == 1) modificar();
			habilitaConfirma = true;
			}
		else
			{
			if(respuesta == 1)
				{
				document.Forma.Accion.value = "BORRA_ARBOL";
				document.Forma.submit();
				}
			habilitaConfirma = true;
			}
		}

	function confirma(mensaje)
		{if(habilitaConfirma) cuadroDialogo(mensaje,2); habilitaConfirma = false;}

	function aviso(mensaje) {cuadroDialogo(mensaje,1);}

	function avisoError(mensaje) {cuadroDialogo(mensaje,3);}

	// --- Inicio y env�o --------------------------------------------------------------
	// -YHG MODIFICA  cuenta.getPeriodicidad() -2
	function inicia()
		{
		document.Forma.Periodicidad[<%=(cuenta.getPeriodicidad() == 0)?0:1 %>].checked = true;
		document.Forma.TipoDis[<%= (cuenta.getLoConcentrado())?0:1 %>].checked = true;
		document.Forma.PerDias.disabled = <%= "" + (cuenta.getPeriodicidad() == 0) %>;
		document.Forma.PerDias.selectedIndex = <%=
			(cuenta.getPeriodicidad() == 0)?"-1":("" + (cuenta.getPeriodicidad() - 1)) %>;

		<%
		Vector horas = cuenta.getHorarios();

		for(int a=0;a<horas.size();a++)
			{
			int sel= Integer.parseInt(((String)horas.get(a)).substring(0,2));
			sel = sel - 6;
			out.println("document.Forma.PerHoras.options[" + sel + "].selected = true;");
			}

		if (cuenta.nivel() == 1 && cuenta.getTipo() != 2 && cuenta.getTipo() != 3)
			{
			out.println("document.Forma.TipoDis[0].disabled = true;");
			out.println("document.Forma.TipoDis[1].disabled = true;");
			out.println("document.Forma.Dispersion.disabled = true;");
			out.println("document.Forma.Periodicidad[0].disabled = true;");
			out.println("document.Forma.Periodicidad[1].disabled = true;");
			out.println("document.Forma.PerDias[0].disabled = true;");
			out.println("document.Forma.PerDias[1].disabled = true;");
			out.println("document.Forma.PerHoras[0].disabled = true;");
			out.println("document.Forma.PerHoras[1].disabled = true;");
			}
		%>
		condiciones();
		}

	function validaEnvio()
		{
		if(document.Forma.Accion.value == "") return false;
		return true;
		}

	// --- Funciones de men� -----------------------------------------------------------
	function MM_preloadImages()
		{ //v3.0
		var d=document;
		if(d.images)
			{
			if(!d.MM_p) d.MM_p=new Array();
			var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
			for(i=0; i<a.length; i++)
			if (a[i].indexOf("#")!=0)
				{d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}
			}
		}

	function MM_swapImgRestore()
		{ //v3.0
		var i,x,a=document.MM_sr;
		for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
		}

	function MM_findObj(n, d)
		{ //v3.0
		var p,i,x;
		if(!d) d=document;
		if((p=n.indexOf("?"))>0&&parent.frames.length)
			{
			d=parent.frames[n.substring(p+1)].document;
			n=n.substring(0,p);
			}
		if(!(x=d[n])&&d.all) x=d.all[n];
		for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
		return x;
		}

	function MM_swapImage()
		{ //v3.0
		var i,j=0,x,a=MM_swapImage.arguments;
		document.MM_sr=new Array;
		for(i=0;i<(a.length-2);i+=3)
			if((x=MM_findObj(a[i]))!=null)
				{
				document.MM_sr[j++]=x;
				if(!x.oSrc) x.oSrc=x.src;
				x.src=a[i+2];
				}
		}

	<%= parFuncionesMenu %>

	</SCRIPT>

</HEAD>

<!-- CUERPO ==========================================================================-->

<BODY onload="inicia()" background="/gifs/EnlaceMig/gfo25010.gif" bgcolor=#ffffff>




	<!-- MENU PRINCIPAL Y ENCABEZADO ---------------------------->
	<table border="0" cellpadding="0" cellspacing="0" width="571">
	<tr valign="top"><td width="*">
	<%= parMenuPrincipal %>
	</td></tr></table>
	<%= parEncabezado %>




	<FORM name=Forma  onSubmit="return validaEnvio()" action="TIDispersion">
	<INPUT type=Hidden name="Accion" value="">
	<INPUT type=Hidden name="Trama" value="<%= cuenta.getNumCta() %>">
	<INPUT type=Hidden name="Cuentas" value="<%= parCuentas %>">
	<INPUT type=Hidden name="Nuevo" value="<%= parNuevo %>">

	<DIV align=center>

	<TABLE width=400px class="textabdatcla" border=0 cellspacing=0>
	<TR>
		<TD class="tittabdat">
		&nbsp;Especifique las condiciones de dispersi&oacute;n
		</TD>
	</TR>
	<TR>
		<TD class='tabmovtex'>


			<TABLE>
			<TR>
				<TD width=300 class='tabmovtex'>&nbsp;Cuenta:</TD>
				<TD class='tabmovtex'>Nivel:</TD>
			</TR>
			<TR>
				<TD class='tabmovtex'>&nbsp;<%= cuenta.getNumCta() + " " + cuenta.getDescripcion()%></TD>
				<TD class='tabmovtex'><%= cuenta.nivel() %></TD>
			</TR>
			</TABLE>

			<BR><BR>
			<TABLE>
			<TR>
				<TD colspan=2 width=250 class="tabmovtex">Periodicidad:</TD>
				<TD width=150 class="tabmovtex">Importe a dispersar:</TD>
			</TR>
			<TR valign=top>
				<TD style="font-weight:normal;" width=100 class="tabmovtex">
					<INPUT type="Radio" name="Periodicidad" value="Diaria" onClick="trataDiario();"> Diaria<BR>
					<INPUT type="Radio" name="Periodicidad" value="Semanal" onClick="trataDiario();"> Semanal
				</TD>
				<TD width=150 class="tabmovtex">


					<!-- HORAS ----------------------------------------------------------->
					<SELECT name="PerHoras" width=140 style="width:140px;" MULTIPLE size=3>
					<OPTION>06:00</OPTION> <OPTION>07:00</OPTION> <OPTION>08:00</OPTION>
					<OPTION>09:00</OPTION> <OPTION>10:00</OPTION> <OPTION>11:00</OPTION>
					<OPTION>12:00</OPTION> <OPTION>13:00</OPTION> <OPTION>14:00</OPTION>
					<OPTION>15:00</OPTION> <OPTION>16:00</OPTION> <OPTION>17:00</OPTION>
					<OPTION>18:00</OPTION> <OPTION>19:00</OPTION> <OPTION>20:00</OPTION>
					</SELECT>


				<!-- YHG Modiifca manera de obtener dias-->
			<SELECT name="PerDias" width=140 style="width:140px;" onFocus="validaFoco();">
			  <option value=1>Lunes</option>
			  <option value=2>Martes</option>
			  <option value=3>Miercoles</option>
			  <option value=4>Jueves</option>
			  <option value=5>Viernes</option>
			  <option value=6>Sabado</option>

			</select>



				</TD>
				<TD class="tabmovtex">
					<INPUT type="Radio" name="TipoDis" value="LoConcentrado" onClick="condiciones()">
					Lo concentrado<BR>
					<INPUT type="Radio" name="TipoDis" value="LoEspecificado" onCLick="habImporte()">
					<INPUT type="Text" name="Dispersion" size=10  onFocus="habImporte()" value=<%= (!esEditable)?"":cuenta.getStrImporte() %>>
				</TD>
			</TR>
			</TABLE>

			<BR><BR>

		</TD>
	</TR>
	</TABLE>

	<DIV>
	<BR>
	<A href="javascript:modificar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25510.gif" alt="Modificar"></A><A href="javascript:anterior()"><IMG border=0 src="/gifs/EnlaceMig/gbo25620.gif" alt="Anterior"></A><A href="javascript:siguiente()"><IMG border=0 src="/gifs/EnlaceMig/gbo25610.gif" alt="Siguiente"></A><A href="javascript:regresar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25320.gif" alt="Regresar"></A>
	<BR>
	</DIV>

	</DIV>

	</FORM>

</BODY>

</HTML>

<%
	if(!parMensaje.equals(""))
		{
		String prefijo = parMensaje.substring(0,3);
		parMensaje = parMensaje.substring(3);
		if(prefijo.equals(":-)")) out.println("<SCRIPT>aviso('" + parMensaje + "');</SCRIPT>");
		if(prefijo.equals(":-/")) out.println("<SCRIPT>avisoError('" + parMensaje + "');</SCRIPT>");
		}
%>