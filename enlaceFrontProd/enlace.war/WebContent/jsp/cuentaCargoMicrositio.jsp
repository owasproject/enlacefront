<%@ page import="java.math.BigDecimal" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Vector"%>
<%@ page import="mx.altec.enlace.utilerias.*"%>
<%
    Calendar dt = Calendar.getInstance();
    dt.setTime( new java.util.Date() );
    int mes = dt.get(Calendar.MONTH);
    int dia = dt.get(Calendar.DAY_OF_MONTH);
    int anio = dt.get(Calendar.YEAR);

	java.util.Date hora = new java.util.Date();
	int hh = hora.getHours();
	int mm = hora.getMinutes();
	int ss = hora.getSeconds();
%>
<%
	System.out.println("Ya estoy en el cuentaCargoMicrositio.jsp - Despliego valores.....................");
	String url = "";
	String referencia = "";
	String concepto = "";
	String servicio_id = "";
	String lineaCaptura = "";

	String fecha = (String)request.getAttribute("fecha");
	if (fecha == null ) {
		fecha = (String)request.getParameter("fecha");
	}
	System.out.println("fecha: " + fecha);
	String ctaabono = (String)request.getAttribute("ctaabono");
	if (ctaabono == null ) {
		ctaabono = (String)request.getParameter("ctaabono");
	}
	System.out.println("ctaabono: " + ctaabono);
	String empresa=((String)request.getAttribute("empresa"))!=null?(Util.HtmlEncode((String)request.getAttribute("empresa"))):(Util.HtmlEncode((String)request.getParameter("empresa")));
	System.out.println("cuentaCargoMicrositio.jsp::Empresa::GetAttribute::"+(String)request.getAttribute("empresa"));
	System.out.println("cuentaCargoMicrositio.jsp::Empresa::GetParameter::"+(String)request.getParameter("empresa"));
	System.out.println("cuentaCargoMicrositio.jsp::Empresa::"+empresa);
	String convenio = (String)request.getAttribute("convenio");
	if (convenio == null ) {
		convenio = (String)request.getParameter("convenio");
	}
	System.out.println("convenio: " + convenio);

	String importe = (String)request.getAttribute("importe");
	if (importe == null ) {
		importe = (String)request.getParameter("importe");
	}
	System.out.println("importe: " + importe);
	String numContrato = (String)request.getAttribute("numContrato");
	if (numContrato == null ) {
		numContrato = (String)request.getParameter("numContrato");
	}
	System.out.println("numContrato: " + numContrato);
	String nomContrato = (String)request.getAttribute("nomContrato");
	if (nomContrato == null ) {
		nomContrato = (String)request.getParameter("nomContrato");
	}
	System.out.println("nomContrato: " + nomContrato);
	String web_application = (String)request.getAttribute("web_application");
	if (web_application == null ) {
		web_application = (String)request.getParameter("web_application");
	}
	System.out.println("web_application: " + web_application);

	if ("PMRF".equals((String)request.getAttribute("servicio_id")) || "PMRF".equals((String)request.getParameter("servicio_id")))
	{
		EIGlobal.mensajePorTrace("CuentaCargoMicrositio.jsp::Pago referenciado :" + request.getAttribute("servicio_id"), EIGlobal.NivelLog.INFO);

			servicio_id = (String) request.getAttribute("servicio_id");
		if (servicio_id == null)
			servicio_id = (String)request.getParameter("servicio_id");
			System.out.println("servicio_id: " + servicio_id);

			lineaCaptura = (String) request.getAttribute("linea_captura");
		if (lineaCaptura == null)
			lineaCaptura = (String) request.getParameter("linea_captura");
			System.out.println("linea_captura: " + lineaCaptura);

	} else {

		EIGlobal.mensajePorTrace("CuentaCargo.jsp::el contenido del campo URL es:" + request.getParameter("url"), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("CuentaCargo.jsp::el contenido del campo URL despues del URLEncoder es:" + java.net.URLEncoder.encode((String)request.getParameter("url")), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("CuentaCargo.jsp::el contenido del campo URL despues es:" + request.getParameter("url"), EIGlobal.NivelLog.INFO);


		url = (String)request.getAttribute("url");
		if (url == null ) {
			url = (String)request.getParameter("url");
		}
		System.out.println("url: " + url);

		referencia = Util.HtmlEncode((String)request.getParameter("referencia"));
		if (referencia == null ) {
			referencia = Util.HtmlEncode((String)request.getParameter("referencia"));
		}
		System.out.println("referencia: " + referencia);


	    concepto = (String)request.getParameter("concepto");
	    System.out.println("concepto: " + concepto);

	    servicio_id = (String)request.getParameter("servicio_id");
	    System.out.println("servicio_id: " + servicio_id);

	}


    String cuentaCargo = (String)request.getParameter("cuentaCargo");
	if (cuentaCargo == null ) {
		cuentaCargo = (String)request.getParameter("cuentaCargo");
	}

    String titularCuentaCargo = (String)request.getParameter("titularCuentaCargo");
    System.out.println("titularCuentaCargo : " + titularCuentaCargo);
    String procedencia = "";
	if (titularCuentaCargo != null ) {
		procedencia = "Adelante";
	}
	String moneda = ctaabono.substring(0,2);
	System.out.println("MONEDA ........." + moneda);
%>

<html>
<head>
<title>Santander</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<link rel="stylesheet" href="/EnlaceMig/pagos.css" type="text/css"/>
<style >
.Texto {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: normal;
	color: #666666;
	text-decoration: none;
}
</style>
<script type="text/JavaScript" SRC= "/EnlaceMig/microSitio_cuadroDialogo.js"></script>
<script type="text/JavaScript">

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function validarSesion()
 {
    var cerrar = document.cuentaMicrositio.cerrarSesion.value;
 	if (cerrar != "NO"){
 		ventana=window.open('/Enlace/enlaceMig/logout?operacionOK=N','trainerWindow','width=580,height=350,toolbar=no,scrollbars=no,left=210,top=225');
 	}
 //	document.cuentaMicrositio.cerrarSesion.value="SI";
 }

function regreso(){
	document.cuentaMicrositio.cerrarSesion.value="NO";
	if (document.cuentaMicrositio.procedencia.value == "") {
		history.back();
	} else {
		document.cuentaMicrositio.action="/Enlace/jsp/confirmarPagoMicrositio.jsp";
		document.cuentaMicrositio.submit();
	}
	}

var dayarray=new Array("Domingo","Lunes","Martes","Mi&eacute;rcoles","Jueves","Viernes","S&aacute;bado");
var montharray=new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");


var mydate=new Date(<%=anio%>,<%=mes%>,<%=dia%>,<%=hh%>,<%=mm%>,<%=ss%>);



function reloj()
 {
   if(!document.layers && !document.all)
     return;

   var digital = new Date();
   var year=digital.getYear();
   if (year < 1000)
      year+=1900;
   var day=digital.getDay();
   var month=digital.getMonth();
   var daym=digital.getDate();
   if (daym<10)
      daym="0"+daym;

   var horas = digital.getHours();
   var minutos = digital.getMinutes();
   var segundos = digital.getSeconds();
   var amOrPm = "AM";
   if (horas > 11)
     amOrPm = "PM";
   if (horas > 12)
     horas = horas - 12;
   if (horas == 0)
     horas = 12;
   if (minutos <= 9)
     minutos = "0" + minutos;
   if (segundos <= 9)
     segundos = "0" + segundos;

   dispTime = "<font color='666666' face='Verdana'>" + dayarray[day]+", "+montharray[month]+" "+daym+", "+year+" | "+horas+":"+minutos+":"+segundos+" "+ amOrPm +"</font>";
   //dispTime = horas + ":" + minutos + ":" + segundos + " " + amOrPm;
   if (document.layers)
    {
      document.layers.objReloj.document.write("<font face=Tahoma size=1>"+ dispTime + "</font>");
	  document.layers.objReloj.document.close();
	}
   else
    if (document.all)
	  objReloj.innerHTML = dispTime;
   setTimeout("reloj()", 1000);
 }

function enviarCuenta(){
	var len = (document.cuentaMicrositio.cuenta.options[document.cuentaMicrositio.cuenta.selectedIndex].value).length;
	var cuentaCliente = (document.cuentaMicrositio.cuenta.options[document.cuentaMicrositio.cuenta.selectedIndex].value).substring(0,11);
    var titularCuentaCliente = (document.cuentaMicrositio.cuenta.options[document.cuentaMicrositio.cuenta.selectedIndex].value).substring(12,len);
	if (cuentaCliente == ("Selecciona una cuenta")){
		alert("Debes de seleccionar una cuenta");
		return;
	}
	document.cuentaMicrositio.cuentaCargo.value = cuentaCliente;
	document.cuentaMicrositio.titularCuentaCargo.value = titularCuentaCliente;

	//EVERIS mantenimiento 1681, se hace la validaci�n de los campos en caso de que el campo concepto sea visible
	//document.cuentaMicrositio.cuentaCargo.value = cuentaCliente;
	var form = document.cuentaMicrositio;

	var servicio = form.servicio_id.value;

	if("PMRF" == servicio) {

		document.cuentaMicrositio.cerrarSesion.value="NO";
		document.cuentaMicrositio.action="/Enlace/jsp/confirmarPagoMicrositio.jsp";
		document.cuentaMicrositio.submit();

	} else {
		var type = form.concepto.type;

		if (type != "hidden") {
			var e = false;
			//alert(form.txtconcepto.value);
			var espacios = / {1,}/.test(form.concepto.value);
			if ((form.concepto.value)== ""){
				//alert ('El campo "Concepto" debe ser llenado con la SIIC');
				cuadroDialogo("El campo &quot;CONCEPTO&quot; debe ser llenado con la SIIC",1);
				//return false;
				e = true;
			}
			else if( isNaN(form.concepto.value)) {
					//alert("Solo debe introducir numeros y sin espacios");
					cuadroDialogo("S&oacute;lo debe introducir n&uacute;meros sin espacios en el campo &quot;CONCEPTO&quot;",1);
					//return false;
					e = true;
			} else if (espacios) {
				//alert("espacios en blanco no");
				cuadroDialogo("El campo &quot;CONCEPTO&quot; no debe contener espacios", 1);
				e = true;
			} else if (form.concepto.value.length != 11){
				//alert ("La SIIC debe tener 11 caracteres");
				cuadroDialogo("El campo &quot;CONCEPTO&quot; debe contener 11 d&iacute;gitos",1);
				e = true;
			}

			if (! e) {
				document.cuentaMicrositio.cerrarSesion.value="NO";
				document.cuentaMicrositio.action="/Enlace/jsp/confirmarPagoMicrositio.jsp";
				document.cuentaMicrositio.submit();
			}
		} else {
			document.cuentaMicrositio.cerrarSesion.value="NO";
			document.cuentaMicrositio.action="/Enlace/jsp/confirmarPagoMicrositio.jsp";
			document.cuentaMicrositio.submit();
		}
		//fin everis
	}

}


</script>

<%!
private String sinComas (String a) {
    StringBuffer sb = new StringBuffer();
    for (int k=0; k < a.trim().length() ;k++){
        if(a.charAt(k)!=',')
            sb.append(a.charAt(k));
    }
    return sb.toString();
}
%>
<%!
private   String formatea ( String a){
    String temp ="";
    String deci = "";
    String regresa = "";
    int valexp = 0;
    int indice =    a.indexOf(".");
    int otroindice  = a.indexOf("E");
	try{
		if( otroindice>0 ){
			BigDecimal mivalor = new BigDecimal( a );
			a = mivalor.toString();
			indice =    a.indexOf(".");
		}
		if( indice>0 )
			a = (new BigDecimal(a+"0")).toString();
		else
		{
			a = (new BigDecimal(a+".00")).toString();
		}
		indice =        a.indexOf(".");
		temp = a.substring (0, indice);
		deci = a.substring ( indice+1, indice+3 );

		String original = temp;
		int indexSN = 0;
		boolean bSignoNegativo = false;
		if( ( indexSN = original.indexOf("-") )> -1 ){
			bSignoNegativo = true;
			temp = original.substring( indexSN + 1 , temp.length() );
		}
		while( temp.length()>3 ){
	        a = temp.substring(temp.length() -3, temp.length() );
		    regresa = ","+a+regresa;
			temp = temp.substring(0,temp.length()-3);
		}
		regresa = temp + regresa+"."+deci;
	    if( bSignoNegativo ){
	        regresa = "($"+regresa +")";
		}
	}catch(Exception e){  }
    return regresa;
}
%>

</head>

<body onLoad="reloj()" onBeforeUnload="validarSesion()">

<%
	//EVERIS mantenimiento 1681
	String err = "";
	err = (String)session.getAttribute("error");
	System.out.println("cuentaCargoMicrositio.jsp ++++++++++++++++++EVERIS ++++++ ERROR!!!!!!!! " + err);

	if (err != null) {
	if (err.equals("1")){
		System.out.println("cuentaCargoMicrositio.jsp ++++++++++++++++++EVERIS ++++++ SE RECIBI� UN ERROR, EL USUARIO NO TIENE DERECHO A REALIZAR PAGO O NO SE ENCONTR�");
		%>
		<script type="text/javascript">
		//document.cuentaMicrositio.concepto.focus();
		cuadroDialogo("La SIIC proporcionada no se encuentra en la base de datos<br/>el pago no puede realizarse",3);
		</script>
	<% }
	}
	//fin everis mantenimiento 1681
	%>


<span id="objReloj" style="position:absolute;left:285;top:60;"></span>
<table width="740" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="2"><img src="/gifs/EnlaceMig/cheque_img.jpg" width="147" height="40"/></td>
	<td width="593" valign="top" bgcolor="#FF0000"><img src="/gifs/EnlaceMig/top.gif" width="453" height="40"/></td>
  </tr>
  <tr>
    <td background="/gifs/EnlaceMig/sombra_gris.jpg"><img src="/gifs/EnlaceMig/sombra_gris.jpg" width="18" height="14"/></td>
    <td background="/gifs/EnlaceMig/sombra.jpg"><img src="/gifs/EnlaceMig/sombra.jpg" width="21" height="14"/></td>
  </tr>
  <tr>
    <td bgcolor="#F0EEEE">&nbsp;</td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="6">
        <tr>
          <td align="left"> <div align="left">
              <table width="465" border="0" cellspacing="0" cellpadding="0" height="136">
                <tr>
                  <td class="minitxt"></td>
                  <td><p>&nbsp; </p></td>
                  <td><a href="javascript:;" onClick="javascript:document.cuentaMicrositio.cerrarSesion.value='NO';  MM_openBrWindow('/EnlaceMig/ayuda002_enlace.html','ayuda','scrollbars=yes,resizable=yes,width=320,height=320')" onMouseOver="MM_swapImage('Image32','','/gifs/EnlaceMig/ayuda_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/ayuda.jpg" name="Image32" width="47" height="40" border="0" id="Image3"/></a></td>
                </tr>
                <tr>
                  <td ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="34%" class="titulorojo">Informaci&oacute;n
                          del pago a : </td>
                        <td class="titulorojo"><%=convenio%> - <%=empresa%></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td class="titulorojo"><%=numContrato%> | <%=nomContrato%></td>
                      </tr>
                    </table>
                    <p class="Texto">Elija la cuenta con la que desea realizar el pago</p></td>
                  <td width="2">&nbsp;</td>
                </tr>
              </table>

            </div></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td bgcolor="#F0EEEE">&nbsp;</td>
    <td><form name="cuentaMicrositio" method="post" action="/Enlace/jsp/confirmarPagoMicrositio.jsp">
		<INPUT TYPE="hidden" name="cuentaCargo" value="<%=cuentaCargo%>"/>
		<INPUT TYPE="hidden" name="titularCuentaCargo" value=""/>
		<INPUT TYPE="hidden" name="cuentaAbono" value="<%=ctaabono%>"/>
		<INPUT TYPE="hidden" name="fecha" value="<%=fecha%>"/>
		<INPUT TYPE="hidden" name="importe" value="<%=importe%>"/>
		<INPUT TYPE="hidden" name="empresa" value="<%=empresa%>"/>
		<INPUT TYPE="hidden" name="convenio" value="<%=convenio%>"/>
		<INPUT TYPE="hidden" name="numContrato" value="<%=numContrato%>"/>
		<INPUT TYPE="hidden" name="nomContrato" value="<%=nomContrato%>"/>
		<INPUT TYPE="hidden" name="web_application" value="<%=web_application%>"/>
		<INPUT TYPE="hidden" name="servicio_id" value="<%=servicio_id%>"/>
		<INPUT TYPE="hidden" name="procedencia" value="<%=procedencia%>"/>

		<%if ("PMRF".equals(servicio_id)) {%>
			<INPUT TYPE="hidden" name="linea_captura" value="<%=lineaCaptura%>"/>
		<%} else { %>
			<INPUT TYPE="hidden" name="referencia" value="<%=referencia%>"/>
			<INPUT TYPE="hidden" name="url" value="<%=url%>"/>
		<%} %>

        <INPUT name="cerrarSesion" TYPE="hidden"  value="SI"/>
	<table width="100%" border="0" cellspacing="6" cellpadding="0">
          <tr align="center">
            <td colspan="2" align="left">
              <select name="cuenta">
				<%
			        Vector vCuentas  = null;
					String datos_cuenta[];
			        String cuenta ="";
					String titular ="";
			        String saldo="";
				    boolean muestra = true;
			        vCuentas = (Vector)request.getAttribute("vector_cuentas");
			        if (vCuentas == null) {
			        	vCuentas = (Vector)session.getAttribute("vector_cuentas");
			        }
			        if(vCuentas !=null && vCuentas.size()!=0 ){
					System.out.println("El vector no esta vacio");
				    for(int j=0 ; j<vCuentas.size() ;j++){
						try{
							datos_cuenta = (String[])vCuentas.elementAt(j);
							cuenta = datos_cuenta[0];
							titular = datos_cuenta[1];
							saldo = datos_cuenta[2];
			            }catch(Exception e){
							e.printStackTrace();
						}
				%>
				<option value="<%=cuenta%>|<%=titular%>"><%=cuenta%> | <%=titular%> | <%=saldo%></option>
					<%}} //fin del for%>
              </select> &nbsp;
            </td>
          </tr>
		  <tr>
		  <td>
		  <%
		  if(!"PMRF".equals(servicio_id)) {
			//EVERIS mant 1681
			System.out.println("cuentaCargoMicrositio.jsp +++++++++EVERIS++++++++++++++++ revisi�n de url, si viene de pemex el campo 'concepto' ser� visible");

			if (url.equals("https://www.comercialrefinacion.pemex.com")){
				System.out.println("cuentaCargoMicrositio.jsp ++++++++++EVERIS++++++++++++++++ usuario viene de PEMEX");
				%>
				Concepto:
				<input type="text" name="concepto" value="<%=referencia%>" size="22" maxlength="40" READONLY><br/><p></p>
				<%
			}else{
				System.out.println("cuentaCargoMicrositio.jsp +++++++++++++EVERIS++++++++++++++++ usuario no viene de pemex");
				%>
				<input type="hidden" name="concepto" value="<%=concepto%>"/>
				<%
			}
		  } else {
			 %> &nbsp; <%
		  }
		//fin EVERIS
		%>
		  </td>
		  </tr>

          <tr align="center">
            <td colspan="2"><table width="100%" border="0" cellspacing="4" cellpadding="4">
                <tr>
                  <td width="2"><img src="/gifs/EnlaceMig/bullet.jpg" width="21" height="21"/></td>
                  <td align="center" bgcolor="#CCCCCC"><strong>Empresa a la que
                    desea pagar</strong></td>
                  <td align="center" bgcolor="#CCCCCC"><div align="center"><strong><%=empresa%></strong></div></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td align="center" bgcolor="#F0EEEE">Importe:</td>
				  <%
				   String verimporte="";
				   if(moneda.equals("49") || moneda.equals("82") || moneda.equals("83")){
					  verimporte=formatea(importe) + " USD";
				   }else{
					  verimporte="$ " + formatea(importe);
				   }%>
                  <td align="center" bgcolor="#F0EEEE"><%=verimporte%></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <%if("PMRF".equals(servicio_id)) {%>
                  	  <td align="center" bgcolor="#F0EEEE">L&iacute;nea de Captura:</td>
	                  <td align="center" bgcolor="#F0EEEE"><%=lineaCaptura%></td>
	              <%} else {%>
	              	  <td align="center" bgcolor="#F0EEEE">Referencia:</td>
	                  <td align="center" bgcolor="#F0EEEE"><%=referencia%></td>
	              <%}%>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td align="center">&nbsp; <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><div align="right">La fecha de aplicaci&oacute;n del
                            pago ser&aacute;:</div></td>
                        <td align="center"> <input name="fecha_disp" type="text" value=<%=fecha%> size="10" OnFocus="blur();" readonly="true"/>
                          &nbsp;</td>
                        <td>
						<img src="/gifs/EnlaceMig/calendario.jpg" name="Image31" width="47" height="40" border="0" id="Image3"/></td>
                      </tr>
                    </table></td>
                  <td align="center">&nbsp;</td>
                </tr>
              </table></td>
          </tr>
          <tr align="center">
            <td><a href="javascript:enviarCuenta();" onClick='javascript:document.cuentaMicrositio.cerrarSesion.value="NO"'; onMouseOver="MM_swapImage('Image1','','/gifs/EnlaceMig/confirmar_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/confirmar.jpg" name="Image1" width="128" height="41" border="0" id="Image1"/></a></td>
            <td><a href="javascript:regreso();" onClick='javascript:document.cuentaMicrositio.cerrarSesion.value="NO";' onMouseOver="MM_swapImage('Image2','','/gifs/EnlaceMig/regresar_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/regresar.jpg" name="Image2" width="85" height="41" border="0" id="Image2"/></a></td>
          </tr>
        </table>
      </form></td>
  </tr>
  <tr>
    <td bgcolor="#F0EEEE">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#F0EEEE">&nbsp;</td>
    <td align="right"><img src="/gifs/EnlaceMig/sitio_seg.jpg" width="66" height="25"/>&nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#F0EEEE">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#F0EEEE">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#F0EEEE">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr bgcolor="#CCCCCC">
    <td><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="10"/></td>
    <td><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="10"/></td>
  </tr>
</table>
</body>
</html>