<html>
<head>
  <title>Banca Virtual.  Validacion de Transferencias Internas por los Registros Duplicados.</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" SRC= "/EnlaceMig/passmark.js"></script>
<script type="text/javascript">

 function exportacion(){

 	document.exportar_duplicados.submit();
 }

function cuadroDialogoMedidas( mensaje, tipo, ancho, alto)
{
	respuesta=0;
	var titulo="Error";
	var imagen="gic25020.gif";
	var param;

	if ( tipo == 1 )
	{
		imagen="gic25020.gif";
		titulo="Alerta";
	}
	else if ( tipo== 2 )
	{
		imagen ="gic25050.gif";
		titulo ="Confirmaci&oacute;n";
	}
	else if ( tipo== 5)
	{
		imagen ="gic25050.gif";
		titulo ="Confirmaci&oacute;n";
	}
	else if ( tipo== 6)
	{
		imagen ="gic25050.gif";
		titulo ="Confirmaci&oacute;n";
	}
	else if ( tipo== 7)
	{
		imagen ="gic25020.gif";
		titulo ="AVISO";
	}
	else if ( tipo== 8)
	{
		imagen ="gic25020.gif";
		titulo ="Obten Data";
	}
	else
	{
		imagen="gic25060.gif";
		titulo="Error";
	}

	param= "width=" + ancho + ",height=" + alto + ",toolbar=no,scrollbars=no,left=210,top=225";

	ventana=window.open('','trainerWindow',param);
	ventana.document.open();
	ventana.document.write("<html>");
	ventana.document.writeln("<head>\n<title>"+titulo+"</title>\n");
	ventana.document.writeln("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
	ventana.document.writeln("</head>");
	ventana.document.writeln("<body bgcolor='white'>");
	ventana.document.writeln("<form>");
	ventana.document.writeln("<table border=0 width=365 class='textabdatcla' align=center cellspacing=3 cellpadding=0>");
	ventana.document.writeln("<tr><th height=25 class='tittabdat' colspan=2>"+ titulo + "</th></tr>");
	ventana.document.writeln("</table>");
	ventana.document.writeln("<table border=0 width=365 class='textabdatcla' align=center cellspacing=0 cellpadding=1>");
	ventana.document.writeln("<tr><td class='tabmovtex1' align=right width=50><img src='/gifs/EnlaceMig/"+imagen+"' border=0></a></td>");
	ventana.document.writeln("<td class='tabmovtex1' align=center width=300><br>"+mensaje+"<br><br>");
	ventana.document.writeln("</tr></td>");
	ventana.document.writeln("<tr><td class='tabmovtex1'>");
	ventana.document.writeln("</td></tr>");
	ventana.document.writeln("</table>");
	ventana.document.writeln("<table border=0 align=center cellspacing=6 cellpadding=0>");
	if (tipo == 2)
	{
		ventana.document.writeln("<tr><td align=right><a href='javascript:opener.ventanaActiva=0;opener.respuesta=1;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=2;opener.continua();window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a></td></tr>");
	}
	else if (tipo == 5)
	{
		ventana.document.writeln("<tr><td align=right><a href='javascript:opener.ventanaActiva=0;opener.respuesta=1;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=2;opener.continua();window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
	}
	else if (tipo == 6)
	{
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=2;window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
	}else if (tipo == 7)
	{
		ventana.document.writeln("<tr><td align=right><a href='javascript:opener.ventanaActiva=0;opener.respuesta=8;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=7;opener.continua();window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a></td></tr>");
	}else if (tipo == 8)
	{
		ventana.document.writeln("<tr><td align=right><a href='javascript:opener.ventanaActiva=0;opener.respuesta=9;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=10;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a></td></tr>");
	}
	else if (tipo == 9)
	{
		ventana.document.writeln("<tr><td align=right><a href='javascript:opener.ventanaActiva=0;opener.respuesta=11;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=12;opener.continua();window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a></td></tr>");
	}
	else
	{
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=0;window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
	}
	ventana.document.writeln("</table>");
	ventana.document.writeln("</form>");
	ventana.document.writeln("</body>\n</html>");
	ventana.focus();
}


function ejecutar( )
{
	document.tabla_transferencias.action="transferencia?ventana=8";
	document.tabla_transferencias.submit();
}

function regresar( )
{
	document.tabla_transferencias.action="transferencia?ventana=0";
	document.tabla_transferencias.submit();

}

function js_exportar_aviso(){
	cuadroDialogo("No fu&#243; posible Exportar el Archivo", 3);
	return ;
}

/******************  Esto no es mio ***************************************************************/

function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/
/* Funciones para deplegar la ventana con la informacion*/

<%= request.getAttribute("newMenu") %>
</script>

<%--  OPC:PASSMARK 16/12/2006 ******************* BEGIN --%>
<%
	String FSOnuevo = (String)request.getAttribute("_fsonuevotrx");
	if( FSOnuevo!=null ) {
%>

	<SCRIPT type="text/javascript">
	  var expiredays = 365;
  	  var ExpireDate = new Date ();
      ExpireDate.setTime(ExpireDate.getTime() + (expiredays * 24 * 3600 * 1000));
      if (!(num=GetCookie("PMDATA"))) { }
	  DeleteCookie("PMDATA");
      SetCookie("PMDATA", "<%=FSOnuevo%>", ExpireDate,"/");
	</SCRIPT>
<%
	 request.removeAttribute("_fsonuevotrx");
	}
%>
<%--  OPC:PASSMARK 16/12/2006 ******************* END --%>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>

<body style=" bgcolor: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);" leftMargin="0" topMargin="0" marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
   <%-- MENU PRINCIPAL --%>
   <%= request.getAttribute("MenuPrincipal") %>
   </td>
  </tr>
</table>

<%= request.getAttribute("Encabezado") %>

<SCRIPT type="text/javascript">
<%
	if (request.getAttribute("InfoUser")!= null) {
		out.println(request.getAttribute("InfoUser"));
	}
%>
</SCRIPT>


<%
	if (request.getAttribute("ContenidoArchivoStr")!= null) {
%>

<%
		out.println(request.getAttribute("ContenidoArchivoStr"));
    }
%>



<FORM NAME="tabla_transferencias" METHOD="POST"  ACTION="transferencia?ventana=8">

<INPUT TYPE="HIDDEN" NAME="arregloctas_origen2"   VALUE=<%= request.getAttribute("arregloctas_origen2") %> />
<INPUT TYPE="HIDDEN" NAME="arregloctas_destino2"  VALUE=<%= request.getAttribute("arregloctas_destino2") %> />
<INPUT TYPE="HIDDEN" NAME="arregloimportes2"      VALUE=<%= request.getAttribute("arregloimportes2") %> />
<INPUT TYPE="HIDDEN" NAME="arreglofechas2"        VALUE=<%= request.getAttribute("arreglofechas2") %> />
<INPUT TYPE="HIDDEN" NAME="arregloconceptos2"     VALUE=<%= request.getAttribute("arregloconceptos2") %> />
<INPUT TYPE="HIDDEN" NAME="contador2"             VALUE=<%= request.getAttribute("contador2") %> />
<INPUT TYPE="HIDDEN" NAME="arregloestatus2"       VALUE=<%= request.getAttribute("arregloestatus2") %> />
<INPUT TYPE="HIDDEN" NAME="miSalida" VALUE=<%= request.getAttribute("miSalida") %> />
<INPUT TYPE="HIDDEN" NAME="imptabla"       VALUE=<%= request.getAttribute("imptabla") %> />
<INPUT TYPE="HIDDEN" NAME="trans" VALUE=<%= request.getAttribute("trans") %> />
<INPUT TYPE="HIDDEN" NAME="TIPOTRANS" VALUE=<%= request.getAttribute("TIPOTRANS") %> />
<INPUT TYPE="HIDDEN" NAME="TDC2CHQ" VALUE=<%= request.getAttribute("TDC2CHQ") %> />
<INPUT TYPE="HIDDEN" NAME="datos2" VALUE=""/>
<INPUT TYPE="HIDDEN" NAME="datoscta" VALUE=""/>
<INPUT TYPE="HIDDEN" NAME="datosimp" VALUE=""/>

<INPUT TYPE="HIDDEN" NAME="Descargar" VALUE=<%=request.getAttribute("DescargaArchivo") %> />

<%-- INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token
	Se debe de mostrar el Token para cuando sea transferencia Nacional y ocultar los botones--%>
<%-- <% --%>
<%-- String ivas2_ = ""; --%>
<%-- String rfcs2_ = ""; --%>
<%-- if(request.getAttribute("arregloivas2")!=null) --%>
<%-- 	ivas2_= (String)request.getAttribute("arregloivas2"); --%>
<%-- if(request.getAttribute("arreglorfcs2")!=null) --%>
<%-- 	rfcs2_=(String)request.getAttribute("arreglorfcs2"); --%>
<%-- %> --%>
<%-- <INPUT TYPE="HIDDEN" NAME="arreglorfcs2" VALUE=<%= rfcs2_%>> --%>
<%-- <INPUT TYPE="HIDDEN" NAME="arregloivas2" VALUE=<%= ivas2_ %>> --%>
<%if( !"0".equals( request.getAttribute("trans") ) && !"2".equals(request.getAttribute("trans")) ){ %>
	<table style="margin-left:auto; margin-right:auto; border: 0px; bgcolor: #FFFFFF;" cellpadding="0" cellspacing="0" width="99%">
		<tr>
			<br></br>
			 <%= request.getAttribute("botontransferir") %>
		</tr>
	</table>
<%} %>
</FORM>
<%if( "0".equals( request.getAttribute("trans") ) || "2".equals( request.getAttribute("trans") ) ){

	String af = (String)request.getAttribute("af");
	String metodoExportacion = (String)request.getAttribute("metodoExportacion");
	String em = (String)request.getAttribute("em");
	String botones = (String)request.getAttribute("botonesD");
%>
	<form action="ExportServlet" method="POST" name="exportar_duplicados">
	<%= botones != null ? botones : "" %>
	<input type="hidden" name="af" value="<%=af%>"/>
	<input type="hidden" name="metodoExportacion" value="<%=metodoExportacion%>"/>
	<input type="hidden" value="em" name="<%=em%>"/>
	</form>
	<br></br>
	<br></br>
	<table style="margin-left:5%;margin-right:auto; border: 0px;" cellpadding="0" cellspacing="0" width="641">
		<tr>
	 		<td colspan="2">
				<%@ include file="/jsp/ValidaOTPConfirmUnificado.jsp" %>
	   		</td>
	 	</tr>
	</table>
<%} %>
<%-- FIN CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token --%>
</body>
</html>
<%-- 2007.01 --%>