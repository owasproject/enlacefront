<%@ page language="java" %>
<%@ page contentType="text/html;charset=ISO8859_1"%>

<%-- 14/10/2002 Cambio de forma de obtención de claveBanco. --%>
<%-- 23/10/2002 Se recibe información comprobante como parámetro. --%>
<%-- 25/10/2002 Manejo comprobante --%>
<%-- 29/10/2002 Cambio parámetros por atributos --%>

<%! String mensajeConfirmacion; %>
<%! String banco=null; %>
<%! String imagen=null; %>
<%! String altImagen; %>

<html>
<head>
<Meta name="versionServlet" content="2.0.4">
	<title>Comprobante de disposici&oacute;n de cr&eacute;dito</TITLE>
   <!-- Autor: Hugo Ruiz Zepeda -->

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<Script language = "JavaScript" type="text/javascript" SRC = "/EnlaceMig/Convertidor.js"></Script>
<script language="javaScript">

function  formateaUsuario(str)
{

        var cadenaF="";
        var ini="Usuario:</B></TD><TD class='textabref'>";
        var fin="</TD>";

        var i1=parseInt(str.indexOf(ini))+ini.length;
        var str2=str.substring(i1);
        var i2=parseInt(str2.indexOf(fin))+1;

        cadenaF=str.substring(0,i1);
        cadenaF=cadenaF+cliente_7To8(str2.substring(0,i2-1));
        cadenaF=cadenaF+ (str2.substring(i2-1));

        return cadenaF;
}

</script>

</head>

<% //banco=(String)session.getAttribute("ClaveBanco");
   banco=(String)application.getAttribute("claveBancoCE");

   if(banco!=null)
   {

      if(banco.equalsIgnoreCase("003"))
      {
         imagen="/gifs/EnlaceMig/glo25040b.gif";
         altImagen="Banco Santander Serf&iacute;n";
      }

      if(banco.equalsIgnoreCase("014"))
      {
         imagen="/gifs/EnlaceMig/glo25040b.gif";
         altImagen="Banco Santander Serf&iacute;n";
      }
   }
%>

<%
  out.print("<!--");
  out.print('\u0048');
  out.print('\u0052');
  out.print('\u005A');
  out.println("-->");
%>

<body leftMargin="0" topMargin="0" marginwidth="0" marginheight="0">

<Basefont size="1">


 <table width="430" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF">
  <tr valign="top">
      <td width="*">
          <Img src="/gifs/EnlaceMig/glo25010.gif" border="0" alt="Banco Santander">
      </TD>
  </TR>
   <TR bgcolor="#FFFFFF">
      <TD bgcolor="#FFFFFF" class="titpag">
         <H3>Comprobante de operaci&oacute;n</H3>
         <B><I>Comprobante de disposici&oacute;n vista</I></B>
      </TD>
      <TD align="right">

         <% if(imagen!=null)
            {
         %>
            <Img src="<%=imagen%>" border="0" alt="<%=altImagen%>">
         <% }
            else
            {
         %>
            <BR>
         <% } %>
         <!--<Img src="/gifs/EnlaceMig/glo25040.gif" border="0" alt="Banco Santander">-->
      </TD>
   </TR>
 </Table>

 <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
  </tr>
  <tr>
    <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
  </tr>
  <tr>
    <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
  </tr>
 </table>



 <table width="400" border="0" cellspacing="0" cellpadding="0" align=center>
  <tr>
   <td align="center">
     <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
	   <tr>
	     <td colspan="3"> </td>
	   </tr>
	   <tr>
	     <td width="21" background="/gifs/EnlaceMig/gfo25030.gif"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." name=".."></td>
		 <td width="428" height="100" valign="top" align="center">
		   <table width="380" border="0" cellspacing="2" cellpadding="3" background="/gifs/EnlaceMig/gau25010.gif">

			 <tr>
			   <td class="tittabcom" align="center" valign="top">


<%
  mensajeConfirmacion=(String)application.getAttribute("mensajeConfirmacionCE");
  //mensajeConfirmacion=request.getParameter("mensajeConfirmacion");

    if(mensajeConfirmacion==null || mensajeConfirmacion.length()<1)
    {
      mensajeConfirmacion="";
      out.println("<BR>El atributo \"mensajeConfirmacion\" es nulo.<BR>");
    }
    else
    {
      //session.removeAttribute("mensajeConfirmacion");
      //out.println("<BR>El atributo \"mensajeConfirmacion\" no es nulo.<BR>");
    }


    //out.println("<center>Comprobante de disposici&oacute;n de cr&eacute;dito.</center>");

//    out.println(mensajeConfirmacion);


//     mensajeConfirmacion="<Table border=\"0\" align=\"center\"><TR><TD align=\"right\" class=\"textabref\"><B>Contrato:</B></TD><TD class=\"textabref\">80002984061</TD></TR><TR><TD align=\"right\" class=\"textabref\"><B>Usuario:</B></TD><TD class=\"textabref\">A000170</TD></TR><TR><TD align=\"right\" class=\"textabref\"><B>Cuenta:</B></TD><TD class=\"textabref\">65500144939</TD></TR><TR><TD align=\"right\" class=\"textabref\"><B>L&iacute;nea de cr&eacute;dito:</B></TD><TD class=\"textabref\">05000636032</TD></TR><TR><TD align=\"right\" class=\"textabref\"><B>Descripci&oacute;n:</B></TD><TD class=\"textabref\">GRUPO BANCA ELECTRONICA SA</TD></TR><TR><TD align=\"right\" class=\"textabref\"><B>Fecha del movimiento:</B></TD><TD class=\"textabref\">01/04/2004</TD></TR><TR><TD align=\"right\" class=\"textabref\"><B>Hora del movimiento:</B></TD><TD class=\"textabref\">12:04 p.m.</TD></TR><TR><TD align=\"right\" class=\"textabref\"><B>Referencia:</B></TD><TD class=\"textabref\">215196</TD></TR><TR><TD align=\"right\" class=\"textabref\"><B>Descripci&oacute;n del movimiento:</B></TD><TD class=\"textabref\">prueba</TD></TR><TR><TD align=\"right\" class=\"textabref\"><B>Importe:</B></TD><TD class=\"textabref\">$30.00</TD></TR><TR><TD align=\"right\" class=\"textabref\"><B>N&uacute;mero de disposici&oacute;n:</B></TD><TD class=\"textabref\">05001971595</TD></TR><TR><TD align=\"right\" class=\"textabref\"><B>Tasa:</B></TD><TD class=\"textabref\">11.2%</TD></TR><TR><TD align=\"right\" class=\"textabref\"><B>Plazo:</B></TD><TD class=\"textabref\">1</TD></TR><TR><TD align=\"right\" class=\"textabref\"><B>Inter&eacute;s:</B></TD><TD class=\"textabref\">$0.00</TD></TR><TR><TD align=\"right\" class=\"textabref\"><B>Total:</B></TD><TD class=\"textabref\">$30.00</TD></TR><TR><TD align=\"right\" class=\"textabref\"><B>Fecha de vencimiento:</B></TD><TD class=\"textabref\">02/04/04</TD></TR></Table>";

     mensajeConfirmacion=mensajeConfirmacion.replace('"','\'');

%>



<!-- <script>document.write(formateaUsuario("COSAS<TR><TD align=\"right\" class=\"textabref\"><B>Usuario:</B></TD><TD class=\"textabref\">A001851</TD>MAS COSAS"));</script> //-->


<script>document.write(formateaUsuario("<%=mensajeConfirmacion%>"));</script>


			   </td>
			 </tr>
		   </table>
		 </td>
         <td width="21" background="/gifs/EnlaceMig/gfo25040.gif"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." 	name=".."></td>
	   </tr>
	 </table>

	</td>
  </tr>
 </table>



 <table width="430" border="0" cellspacing="0" cellpadding="0" align=center>
  <tr>
    <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
  </tr>
  <tr>
    <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
  </tr>
  <tr>
    <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
  </tr>
 </table>



 <table width="430" border="0" cellspacing="0" cellpadding="0" align=center>
  <tr>
    <td align="center">

	 <br>

	 <table width="150" border="0" cellspacing="0" cellpadding="0" height="22">
	  <tr>
	    <td align="center"><a href="" border="0" onClick="self.close();">
		  <img border="0" name="imageField32" src="/gifs/EnlaceMig/gbo25200.gif" width="83" height="22" alt="Cerrar"></a>
		</td>
      <td align="center"><a href="" border="0" onClick="window.print();return false">
		  <img border="0" name="imageField33" src="/gifs/EnlaceMig/gbo25240.gif" width="83" height="22" alt="Imprimir"></a>
		</td>
	  </tr>
	 </table>

	</td>
  </tr>
 </table>
 <%-- session.removeAttribute("mensajeConfirmacion"); --%>
</body>
</html>
