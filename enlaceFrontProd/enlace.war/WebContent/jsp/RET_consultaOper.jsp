<jsp:useBean id='facultadesAltaCtas' class='java.util.HashMap' scope='session'/>
<%@ page import="java.util.*"%>
<%@page import="EnlaceMig.*"%>
<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<html>
<head>
<title>Consulta de Operaciones FX Online</TITLE>
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">

<script language="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

<style type="text/css">
.componentesHtml{
	  font-family: Arial, Helvetica, sans-serif;
	  font-size: 11px;
	  color: #000000;
}
</style>

<script language="JavaScript">
/*************************************************************************************/
/*************************************************************************************/
function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
/*************************************************************************************/
/*************************************************************************************/
function continua(){

}

<%
 String FechaHoy="";
 FechaHoy=(String)request.getAttribute("FechaHoy");
 if(FechaHoy==null)
    FechaHoy="";
%>

//Indice Calendario
	var Indice=0;


//Arreglos de fechas
<%
	if(request.getAttribute("VarFechaHoy")!= null)
		out.print(request.getAttribute("VarFechaHoy"));
%>

//Dias inhabiles
	diasInhabiles='<% if(request.getAttribute("DiasInhabiles")!= null) out.print(request.getAttribute("DiasInhabiles")); %>';

<%
	String DiaHoy="";
	if(request.getAttribute("DiaHoy")!= null)
 		DiaHoy=(String)request.getAttribute("DiaHoy");
%>

/*Funcion para boton consultar*/
function consultar(){
	var forma = document.frmRetConsultaOper;
	if(validaForma(forma)){
		textToUpper(forma);
   	  	forma.action = "RET_ConsultaOper";
	  	forma.opcion.value = "consultar";
	  	forma.submit();
    }
}
 
/*Funcion para validar la forma antes de regresar al servlet*/
function validaForma(forma){   
  	  	
  	if(document.frmRetConsultaOper.folioOperLiq.value!="" &&
  	   !EsNumerica(document.frmRetConsultaOper.folioOperLiq.value)){
  		document.frmRetConsultaOper.folioOperLiq.focus();
		cuadroDialogo("El Folio de Liquidaci�n no es valido.",11);
		return false;
  	}	 	
	  		  	 	   	
  	var bOk = true;
	var ahora = new Date();
   	var hoy = makeDateFormat(ahora.getDate()+1, ahora.getMonth()+1, ahora.getFullYear());
   	
	if(document.frmRetConsultaOper.fchConsIni.value==""){
  		document.frmRetConsultaOper.fchConsIni.focus();
		cuadroDialogo("La Fecha inicial es obligatoria.",11);
		return false;
  	}
	  	
	bOk = bOk && (valAno(document.frmRetConsultaOper.fchConsIni.value));
	bOk = bOk && (valMes(document.frmRetConsultaOper.fchConsIni.value));
	bOk = bOk && (valDia(document.frmRetConsultaOper.fchConsIni.value));
	bOk = bOk && (valSep(document.frmRetConsultaOper.fchConsIni.value));
  	if(!bOk){
  		document.frmRetConsultaOper.fchConsIni.focus();
		cuadroDialogo("El formato de fecha inicial debe ser: DD-MM-YYYY.",11);
		return false;
  	}
	  	
  	if(document.frmRetConsultaOper.fchConsFin.value==""){
  		document.frmRetConsultaOper.fchConsFin.focus();
		cuadroDialogo("La fecha final es obligatorio.",11);
		return false;
  	}
  	
  	bOk = true;
	bOk = bOk && (valAno(document.frmRetConsultaOper.fchConsFin.value));
	bOk = bOk && (valMes(document.frmRetConsultaOper.fchConsFin.value));
	bOk = bOk && (valDia(document.frmRetConsultaOper.fchConsFin.value));
	bOk = bOk && (valSep(document.frmRetConsultaOper.fchConsFin.value));
  	if(!bOk){
  		document.frmRetConsultaOper.fchConsFin.focus();
  		cuadroDialogo("El formato de fecha final debe ser: DD-MM-YYYY.",11);
		return false;
  	}
  	
  	if(!fechaMayorOIgualQue(document.frmRetConsultaOper.fchConsFin.value,
  						   document.frmRetConsultaOper.fchConsIni.value)){
		document.frmRetConsultaOper.fchConsIni.focus();
  		cuadroDialogo("La fecha inicio es mayor a la fecha fin.",11);
		return false;							
	}
		
	if(fechaMayorOIgualQue2(document.frmRetConsultaOper.fchConsIni.value,hoy) 
		|| fechaMayorOIgualQue2(document.frmRetConsultaOper.fchConsFin.value,hoy)){
		document.frmRetConsultaOper.fchConsIni.focus();
  		cuadroDialogo("El periodo de consulta no puede ser mayor a hoy.",11);
		return false;							
	}   	
	return true;
}

/*Valida que el a�o sea un digito01-01-2010*/
function valAno(oTxt){
	var bOk = true;
	var nAno = oTxt.substr(6);
 	bOk = bOk && (nAno.length == 4);
 	if (bOk){
		for (var i = 0; i < nAno.length; i++){
 	  		bOk = bOk && esDigito(nAno.charAt(i));
		}
	}
 	return bOk;
}

/*Valida que el mes sea valido*/
function valMes(oTxt){
     var bOk = false;
     var nMes = parseInt(oTxt.substr(3, 2), 10);
     bOk = bOk || ((nMes >= 1) && (nMes <= 12));
     return bOk;
}

/*Valida que el dia sea valido y se encuentre entre los dias que tiene el mes*/
function valDia(oTxt){
	var nDia = parseInt(oTxt.substr(0, 2), 10);
	var bOk = false;
	bOk = bOk || ((nDia >= 1) && (nDia <= finMes2(oTxt)));
	return bOk;
}

/*Valida que el texto fecha tenga separaciones en dos formatos DD/MM/AAAA*/
function valSep(oTxt){
		var bOk = false;
  bOk = bOk || ((oTxt.charAt(2) == "/") && (oTxt.charAt(5) == "/"));
  return bOk;
}

/*Funcion para validar que la cadena sea un digito*/
function esDigito(sChr){
	var sCod = sChr.charCodeAt(0);
	return ((sCod > 47) && (sCod < 58));
}

/*Funcion que trae el n�mero de dias de cada mes y valida si el a�o es bisiesto*/
function finMes2(oTxt){
	var nMes = parseInt(oTxt.substr(3, 2), 10);
 	var nAno = parseInt(oTxt.substr(6), 10);
 	var nRes = 0;
 	switch (nMes){
  		case 1: nRes = 31; break;
  		case 2: nRes = 28; break;
  		case 3: nRes = 31; break;
  		case 4: nRes = 30; break;
  		case 5: nRes = 31; break;
  		case 6: nRes = 30; break;
  		case 7: nRes = 31; break;
  		case 8: nRes = 31; break;
  		case 9: nRes = 30; break;
  		case 10: nRes = 31; break;
  		case 11: nRes = 30; break;
  		case 12: nRes = 31; break;
	}
 	return nRes + (((nMes == 2) && (nAno % 4) == 0)? 1: 0);
}

/*Compara fechas donde fec0 > fec1*/
function fechaMayorOIgualQue(fec0, fec1){
	var bRes = false;
	var sDia0 = fec0.substr(0, 2);
	var sMes0 = fec0.substr(3, 2);
	var sAno0 = fec0.substr(6, 4);
	var sDia1 = fec1.substr(0, 2);
	var sMes1 = fec1.substr(3, 2);
	var sAno1 = fec1.substr(6, 4);
	if (sAno0 > sAno1) bRes = true;
	else{
		if (sAno0 == sAno1){
			if (sMes0 > sMes1) bRes = true;
	      	else {
	       		if (sMes0 == sMes1)
	        		if (sDia0 >= sDia1) bRes = true;
	     	}
	     }
	}
	return bRes;
}

/*Funcion que suma o resta el incremento apartir de una fecha determinada*/
function addToDate(sFec0, sInc){
	var nInc = Math.abs(parseInt(sInc));
	var sRes = sFec0;
	if (parseInt(sInc) >= 0)
		for (var i = 0; i < nInc; i++) sRes = incDate(sRes);
	else
	    for (var j = 0; j < nInc; j++) sRes = decDate(sRes);
	return sRes;
}

/*Compara fechas donde fec0 > fec1, donde fec1 es una fecha creada por la app*/
function fechaMayorOIgualQue2(fec0, fec1){
	var bRes = false;
	var sDia0 = fec0.substr(0, 2);
	var sMes0 = fec0.substr(3, 2);
	var sAno0 = fec0.substr(6, 4);
	var sDia1 = fec1.substr(0, 2);
	var sMes1 = fec1.substr(3, 2);
	var sAno1 = fec1.substr(6, 4);
	if (sAno0 > sAno1) bRes = true;
	else {
		if (sAno0 == sAno1){
			if (sMes0 > sMes1) bRes = true;
			else {
	       		if (sMes0 == sMes1)
	        		if (sDia0 >= sDia1) bRes = true;
	      	}
		}
	}
	return bRes;
}

/*Realiza el incremento separando dia, mes y a�o afectando a los tres en el mismo*/
function incDate(sFec0){
	var nDia = parseInt(sFec0.substr(0, 2), 10);
	var nMes = parseInt(sFec0.substr(3, 2), 10);
	var nAno = parseInt(sFec0.substr(6, 4), 10);
	nDia += 1;
	if (nDia > finMes(nMes, nAno)){
		nDia = 1;
	    nMes += 1;
	    if (nMes == 13){
	    	nMes = 1;
	     	nAno += 1;
	    }
	}
	return makeDateFormat(nDia, nMes, nAno);
}

/*Realiza el decremento separando dia, mes y a�o afectando a los tres en el mismo*/
function decDate(sFec0){
	var nDia = Number(sFec0.substr(0, 2));
	var nMes = Number(sFec0.substr(3, 2));
	var nAno = Number(sFec0.substr(6, 4));
	nDia -= 1;
	if (nDia == 0){
		nMes -= 1;
	    if (nMes == 0){
	    	nMes = 12;
	     	nAno -= 1;
	    }
	    nDia = finMes(nMes, nAno);
	}
	return makeDateFormat(nDia, nMes, nAno);
}

/*Funcion que regresa el ultimo dia del mes */
function finMes(nMes, nAno){
	return aFinMes[nMes - 1] + (((nMes == 2) && (nAno % 4) == 0)? 1: 0);
}

/*Funcion que concatena el dia, mes y a�o para darle un formato a la fecha que se generara*/
function makeDateFormat(nDay, nMonth, nYear){
	var sRes;
	sRes = padNmb(nDay, 2, "0") + "/" + padNmb(nMonth, 2, "0") + "/" + padNmb(nYear, 4, "0");
	return sRes;
}

/*Arreglo que indica el n�mero de dias de cada mes*/
var aFinMes = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

/*Funcion para rellenar campos con un determinado valor*/
function padNmb(nStr, nLen, sChr){
	var sRes = String(nStr);
	for (var i = 0; i < nLen - String(nStr).length; i++)
		sRes = sChr + sRes;
	return sRes;
}

/*Funcion para validar cadenas especiales con &*/
function especiales(Txt1){	
	var strEspecial=" &ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
   	var cont=0;

    for(i0=0;i0<Txt1.value.length;i0++){
		Txt2 = Txt1.value.charAt(i0);
	   	if( strEspecial.indexOf(Txt2)!=-1 )
			cont++;
    }
	if(cont!=i0){
	   	Txt1.focus();
	   	cuadroDialogo("En el titular o descripcion de la cuenta, no se permiten caracteres especiales",11);
	   	return false;
	}
	return true;
}

/*Funcion para validar cadenas especiales*/
function especialesBI(Txt1,campo){
	var strEspecial=" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789,";
   	var cont=0;

    for(i0=0;i0<Txt1.value.length;i0++){
		Txt2 = Txt1.value.charAt(i0);
	   	if( strEspecial.indexOf(Txt2)!=-1 )
			cont++;
    }
	if(cont!=i0){
		Txt1.focus();
	   	cuadroDialogo("No se permiten caracteres especiales para " + campo,11);
	   	return false;
	}
	return true;
}

/*Funcion para boton limpiar*/
function limpiar(){
	document.frmRetConsultaOper.reset();
}

/*Funcion para revisar si el caracter es numerico*/
function Caracter_EsNumerico(caracter){
	var ValorRetorno = true;
	if( !(caracter >= "0" && caracter <= "9") ) ValorRetorno = false;
	return ValorRetorno;
}

/*Funcion principal para revisar si la cadena es alfa*/
function Caracter_EsAlfa(caracter){
	var ValorRetorno = true;

	if ( !(caracter >= "A" && caracter <= "Z") )
		if( !(caracter >= "&Aacute;" && caracter <= "&Uacute;") )
			if ( !(caracter >= "a" && caracter <= "z") )
				if( !(caracter >= "&aacute;" && caracter <= "&uacute;") )
						ValorRetorno = false;
	return ValorRetorno;
}

/*Funcion principal para revisar si la cadena es alfanumerica*/
function EsAlfaNumerica(cadena){
	var retorno = true;

	for(a=0;a<cadena.length && retorno;a++) {
		caracter = cadena.substring(a,a+1);
		if (!Caracter_EsAlfa(caracter))
			if (!Caracter_EsNumerico(caracter))
				retorno = false;
	}
	return retorno;
}

/*Funcion principal para revisar si la cadena es numerica*/
function EsNumerica(cadena){
	var retorno = true;

	for(a=0;a<cadena.length && retorno;a++) {
		caracter = cadena.substring(a,a+1);
		if (!Caracter_EsNumerico(caracter))
			retorno = false;
	}
	return retorno;
}

/*Funcion para revisar si el caracter es alfanumerica*/
function EsAlfa(cadena){
	var retorno = true;

	for(a=0;a<cadena.length && retorno;a++) {
		caracter = cadena.substring(a,a+1);
		if (!Caracter_EsAlfa(caracter))
			retorno = false;
	}
	return retorno;
}

/*Funcion para actualizar el registro*/
function Actualiza(){
	if(Indice==0)
		document.frmRetConsultaOper.fchConsIni.value=Fecha[Indice];
	if(Indice==1)
		document.frmRetConsultaOper.fchConsFin.value=Fecha[Indice];
}

/*Funcion para mostrar los calendarios*/
function js_calendario(ind){
	var m = new Date()
	Indice = ind;
    n = m.getMonth();
    msg = window.open("/EnlaceMig/EI_Calendario.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
    msg.focus();
}

<%= request.getAttribute("newMenu")%>

</script>
</HEAD>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif'); " background="/gifs/EnlaceMig/gfo25010.gif">
<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top">
	  <td width="*">
	    <%= request.getAttribute("MenuPrincipal")%>
	  </TD>
	</TR>
</table>
<%= request.getAttribute("Encabezado")%>
<table width="571" border="0" cellspacing="0" cellpadding="0">
	<tr>
	  <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
	  <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
	</tr>
</table>

<form name=transferencia>
 <input type=hidden name="Descripcion">
</form>

<FORM  NAME="frmRetConsultaOper" ENCTYPE="multipart/form-data" method=post action="RET_ConsultaOper">
<TABLE cellSpacing=0 cellPadding=0 align=center border=0 width="613">
	<tr>
  		<td>
  			<table ALIGN=center border=0 cellpadding=2 cellspacing=3 >
				<table width="660" border="0" cellspacing="2" cellpadding="3">
					<tr>
						<td class="tabmovtex">* Filtros de consulta obligatorios</td>
					</tr>
				    <tr>
				   		<td class="tittabdat" colspan="2">
						<b> Datos Generales para Consulta de Operaciones FX Online </td>
				  	</tr>
				    <tr>
				    	<td class='textabdatcla' colspan=4 width='100%'>
				       		<table width="660" id="tabla" border=0 class='textabdatcla' cellspacing=3 cellpadding=2>
				       			<tr>								
					        		<table width="660" id="tabla2" border=0 class='textabdatcla' cellspacing=3 cellpadding=2 align=center>
										<tr>
									 		<td class="tabmovtex"><br></td>
							         		<td class="tabmovtex11" width='25%' >* Fecha Inicio:</td>
							         		<td class="tabmovtex"><input type="text" name=fchConsIni class="componentesHtml" maxlength=15 size=15>
							         			<a href ="javascript:js_calendario(0);">
		                              				<img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle">
		                            			</a>
		                            		</td>
		                            	</tr>
		                            	<tr>
									 		<td class="tabmovtex"><br></td>
							         		<td class="tabmovtex11" width='25%' >* Fecha Fin:</td>
							         		<td class="tabmovtex"><input type="text" name=fchConsFin class="componentesHtml" maxlength=15 size=15>
							         			<a href ="javascript:js_calendario(1);">
		                              				<img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle">
		                            			</a>
		                            		</td>		                            		
							        	</tr>
										<tr>
									 		<td class="tabmovtex"><br></td>
							         		<td class="tabmovtex11" width='25%' > Estatus de Operaci�n:</td>
							         		<td class="tabmovtex" >
												<select name="EstatusOper" >
													<option CLASS='tabmovtex' selected value="X">--TODOS--</option>
													<option CLASS='tabmovtex' value="P">PACTADO</option>
													<option CLASS='tabmovtex' value="C">COMPLEMENTADO</option>
													<option CLASS='tabmovtex' value="N">ENVIADO CON ERRORES</option>
													<option CLASS='tabmovtex' value="L">ENVIADO</option>
													<option CLASS='tabmovtex' value="D">CANCELADO</option>													
													<option CLASS='tabmovtex' value="O">CONFIRMADO</option>
												</select>
							         		</td>
							        	</tr>							        	
						        		<tr>
									 		<td class="tabmovtex" width=10><br></td>
							         		<td class='tabmovtex11' width='25%' >Folio de Operaci�n:</td>
							         		<td class="tabmovtex"><input type="text" name=folioOperLiq class="componentesHtml" maxlength=22 size=22></td>
							        	</tr>	            
							        	<tr>
							         		<td class="tabmovtex" colspan=3 ><br></td>
							        	</tr>									
				       				</table>
				       			</tr>						        	
				       		</table>
				     	</td>
					</tr>
				</table>
				<br>
        		<table border="0" align="center" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          			<tr>
            			<td align="right" valign="middle" width="90"><A href ="javascript:consultar();"><img border="0" name="boton" src="/gifs/EnlaceMig/gbo25220.gif" width="90" height="22" alt="Consultar"></a></td>
			            <td align="left" valign="top" width="76"><A href ="javascript:limpiar();"><img border="0" name="boton" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar"></a></td>
					</tr>
        		</table>
        		<br>
			</table>				
		</td>
	</tr>
</table>
<input type=hidden name=opcion >
<input type=hidden name=benefSel >  
</form>
 
<%=
	(request.getAttribute("mensaje") != null)?
	("<script language='javascript'>cuadroDialogo(" + (String)request.getAttribute("mensaje") +
	");</script>"):""
%>
</body>
</HTML>