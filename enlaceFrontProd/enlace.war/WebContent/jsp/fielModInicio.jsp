<%-- 
  - Autor: FSW Everis
  - Aplicacion: Enlace
  - Módulo: SPID
  - Fecha: 01/06/2016
  - Descripcion: Pantalla de inicio de la modificacion de una fiel.
  --%>
<%@include file="/jsp/fielCabecera.jsp"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>    
<html>
<head> 
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
 style="background-color: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);"
 onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif',
            '/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif',
            '/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif',
            '/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif',
            '/gifs/EnlaceMig/gbo25011.gif');validaError();consultaCtas();">
    
    <table border="0" cellpadding="0" cellspacing="0" width="571">
        <tr valign="top">
            <td width="*">
                <%-- MENU PRINCIPAL --%>${MenuPrincipal}
            </td>
        </tr>
    </table>
    ${Encabezado}
    <table border="0" cellpadding="0" cellspacing="0" width="760">
        <tr>
            <td align="center">
    <form name="transferencia" method="post" id="transferencia" action="FielModificacion?modulo=1">
        <table style="margin: 0 auto;"  style="boder: 0;" cellpadding="5"
               cellspacing="3" class="textabdatcla" width="500">
            <tr>
                <td class="tittabdat" align="left">
                    <fmt:message key="grl.fiel.msg5" bundle="${lang}"/>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                   <%@ include file="/jsp/fielFiltroCtas.jspf" %>
                </td>
            </tr>
            <tr>
                <td class="tabmovtex" valign="top" colspan="4" width="673">
                   <fmt:message key="grl.fiel.msg2" bundle="${lang}"/>
                </td>
            </tr>

            <tr>
                <td class="tabmovtex" style="background-color: white;" colspan="4"><br/></td>
            </tr>
            <tr>
                <td class="textabdatcla" colspan="4" width="100%">
                    <table class="textabdatcla" cellspacing="3" cellpadding="2">
                        <tr>
                            <td class="tabmovtex" colspan="2" width="20%" align="left">
                                <fmt:message key="grl.fiel.cuenta" bundle="${lang}"/>
                            </td>
                        </tr>
                        <tr>
                            <td width="80%" class="tabmovtex"> 
                                <select id="cuenta" name="comboCuenta" style="width: 300px;" class="comboCuenta" disabled>
                                    <option value=""><fmt:message key="grl.fiel.sel.cta" bundle="${lang}"/></option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
    <br/>
    
      <table style="margin: 0 auto;" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <a href="javascript:validar('M');" style="border-style: none;">
                        <img src="/gifs/EnlaceMig/gbo25520.gif" style="border-style: none;" />
                    </a>
                </td>
            </tr>
        </table>
    <br/>
<c:if test="${not empty fiel}">  
    <form name="frmCambio" method="POST" action="FielModificacion?modulo=2">
        <table style="margin: 0 auto;"  style="border: 0;" cellpadding="5"
               cellspacing="3" class="textabdatcla" width="500">
            <tr>
                <td class="tittabdat" align="left">
                    <fmt:message key="grl.fiel.nva.fiel" bundle="${lang}"/>
                </td>
            </tr>
            <tr>
                <td class="textabdatcla" colspan="4" width="100%">
                    <table class="textabdatcla" cellspacing="3" cellpadding="2">
                        <tr>
                            <td class="tabmovtex" width="5%" align="left">
                                <fmt:message key="grl.fiel.cuenta" bundle="${lang}"/>
                            </td>
                            <td width="20%" class="tabmovtex">
                                <input type="hidden" name="cuenta" id="cuenta" value="${comboCuenta}"/>
                                <input type="text" name="txtCuenta" id="txtCuenta" class="componentesHtml"
                                       maxlength="21" size="40" size="60" width="100%"
                                       value="${comboCuenta}" disabled/>
                            </td>
                        </tr>
                        <tr>
                            <td class="tabmovtex" width="5%" align="left">
                                <fmt:message key="grl.fiel.vja.fiel" bundle="${lang}"/>
                            </td>
                            <td width="20%" class="tabmovtex">
                            <input type="hidden" name="fielAntigua" id="fielAntigua" value="${fiel}"/>
                                <input type="text" name="fiel" id="fiel" class="componentesHtml"
                                       maxlength="21" size="40" maxlength="22" size="60" width="100%"
                                       value="${fiel}"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br/>
        <table style="margin: 0 auto;" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <a href="javascript:continuar();" style="border-style: none;"> 
                         <img src="/gifs/EnlaceMig/gbo25222.gif" style="border-style: none;" />
                    </a>
                </td>
            </tr>
        </table>
    </form>
</c:if>
            </td>
        </tr>
    </table>
</body>
</html>
