<%@ page import="java.util.*" %>
<%@ page import="java.lang.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.math.BigDecimal" %>
<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<%@ page import="mx.altec.enlace.bo.RET_ControlConsOper"%>
<jsp:directive.page import="mx.altec.enlace.bo.RET_ControlConsOper"/>

<%@page import="mx.altec.enlace.bo.RET_CuentaAbonoVO"%><html>
<head>
	<% 
 	   RET_ControlConsOper controladorLista = (RET_ControlConsOper) session.getAttribute("controladorListaDetalle");	   
 	   String folioPadre = (String) request.getAttribute("folioPadre");
	%>
	<title>Consulta de Detalle de Operaciones FX Online</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
	<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
	<script language = "JavaScript" SRC= "/EnlaceMig/scrImpresion.js"></script>

	<SCRIPT language = "JavaScript">
	// ---------------------------------------------------

	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && d.getElementById) x=d.getElementById(n); return x;
	}

	<%= request.getAttribute("newMenu") %>

	// ---------------------------------------------------
	
	/*Funcion para el boton regresar*/
	function regresar(){
		document.frmRetConsultaOperDet.action = "RET_ConsultaOper";
		document.frmRetConsultaOperDet.detalle.value = "N";
  		document.frmRetConsultaOperDet.opcion.value = "primero";
  		document.frmRetConsultaOperDet.submit();
	}	

	/*Funcion para el link Primero*/
	function primero(){
		document.frmRetConsultaOperDet.action = "RET_ConsultaOper";
		document.frmRetConsultaOperDet.detalle.value = "S";
  		document.frmRetConsultaOperDet.opcion.value = "primero";
  		document.frmRetConsultaOperDet.submit();
	}
	
	/*Funcion para el link anterior*/
	function anterior(){
		document.frmRetConsultaOperDet.action = "RET_ConsultaOper";
		document.frmRetConsultaOperDet.detalle.value = "S";
  		document.frmRetConsultaOperDet.opcion.value = "anterior";
  		document.frmRetConsultaOperDet.submit();	
	}
				
	/*Funcion para el link siguiente*/
	function siguiente(){
		document.frmRetConsultaOperDet.action = "RET_ConsultaOper";
		document.frmRetConsultaOperDet.detalle.value = "S";
  		document.frmRetConsultaOperDet.opcion.value = "siguiente";
  		document.frmRetConsultaOperDet.submit();
	}
	
	/*Funcion para el link ultimo*/
	function ultimo(){
		document.frmRetConsultaOperDet.action = "RET_ConsultaOper";
		document.frmRetConsultaOperDet.detalle.value = "S";
  		document.frmRetConsultaOperDet.opcion.value = "ultimo";
  		document.frmRetConsultaOperDet.submit();	
	}

	</SCRIPT>
	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">
	<!-- MENU PRINCIPAL -->
	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
		<tr valign="top"><td width="*"><%= request.getAttribute("MenuPrincipal") %></td></tr>
	</TABLE>
	<%= request.getAttribute("Encabezado") %>
	<br>
	<FORM NAME="frmRetConsultaOperDet" METHOD="POST" ACTION="RET_ConsultaOper" >
	<%		
	if(controladorLista!=null){
		EIGlobal.mensajePorTrace("Operaciones RET JSP->"+controladorLista.getLista().size(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Operaciones RET!=null->"+(controladorLista!=null), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Operaciones RET.size()>0->"+(controladorLista.getLista().size()>0), EIGlobal.NivelLog.DEBUG);
	}	
	if(controladorLista!=null && controladorLista.getLista().size()>0){
	%>
	<table cellpadding='2' cellspacing='1' width = "80%" align="center">
		<tr>
			<td class='texenccon' colspan="9">
				Registros del <%=controladorLista.getNumRecordPageDesde()%> al <%=controladorLista.getNumRecordPageHasta()%><br>
				Pagina <%=controladorLista.getIdPage()%> de <%=controladorLista.getNumPages()%>
			</td>						
		</tr>
		<tr>			
			<td class="tittabdat" align = "center"><B>Folio de Operación</B></td>
			<td class="tittabdat" align = "center"><B>Folio de Operación Abono</B></td>
			<td class="tittabdat" align = "center"><B>Folio de Transfer</B></td>
			<td class="tittabdat" align = "center"><B>Cuenta Abono</B></td>			
			<td class="tittabdat" align = "center"><B>Importe de Operación</B></td>
			<td class="tittabdat" align = "center"><B>Tipo de Cuenta</B></td>
			<td class="tittabdat" align = "center"><B>Estatus Operación</B></td>	
		</tr>
		<%
		for(int i=0; i<controladorLista.getLista().size(); i++){%>
			<%if((i%2)==0){%>
				<tr>
					<%=((RET_CuentaAbonoVO)controladorLista.getLista().get(i)).toStringConDet(0, folioPadre)%>
				</tr>
			<%}else{ %>
				<tr>
					<%=((RET_CuentaAbonoVO)controladorLista.getLista().get(i)).toStringConDet(1, folioPadre)%>
				</tr>
			<%}%>
		<%}%>			
	</table>
	<br>
	<input type=hidden name=opcion >
	<input type=hidden name=detalle >
	<br>

	<%
    EIGlobal.mensajePorTrace("controladorLista.getNumRecords()->"+(controladorLista.getNumRecords()), EIGlobal.NivelLog.DEBUG);
	EIGlobal.mensajePorTrace("controladorLista.getMaxNumRecordsPage()->"+(controladorLista.getMaxNumRecordsPage()), EIGlobal.NivelLog.DEBUG);
	if(controladorLista.getIdPage()==1
		 && controladorLista.getNumRecords()>controladorLista.getMaxNumRecordsPage()){%>
		<div align='center'>
			<a href="javascript:siguiente();">Siguientes 50 ></a>&nbsp;
			<a href="javascript:ultimo();">Ultimos 50 ></a>
		</div>
	<%}else if(controladorLista.getIdPage()>1
				&& controladorLista.getIdPage()<controladorLista.getNumPages()){%>
		<div align='center'>				
			<a href="javascript:primero();"> Primeros 50</a>&nbsp;
			<a href="javascript:anterior();"> Anteriores 50</a>&nbsp;
			<a href="javascript:siguiente();">Siguientes 50 ></a>&nbsp;
			<a href="javascript:ultimo();">Ultimos 50 ></a>
		</div>			
	<%}else if(controladorLista.getIdPage()==controladorLista.getNumPages()
				&& controladorLista.getNumRecords()>controladorLista.getMaxNumRecordsPage()){%>
		<div align='center'>
			<a href="javascript:primero();"> Primeros 50</a>&nbsp;
			<a href="javascript:anterior();"> Anteriores 50</a>
		</div>			
	<%}%>
	
	<div align='center'>
		<a href="javascript:regresar();"><img border="0" src="/gifs/EnlaceMig/gbo25320.gif" alt='Regresar'></a>	
	</div>
	<%}%>
		
	</FORM>
	
	<%=
	(request.getAttribute("mensaje") != null)?
	("<script language='javascript'>cuadroDialogo(" + (String)request.getAttribute("mensaje") +
	");</script>"):""
	%>
</body>
</html>