
<%@ page import="java.util.StringTokenizer"%>
<%@ page import="mx.altec.enlace.bo.*"%>
<%@ page import="java.util.Vector"%>

<%
		// --- Obtenci�n de par�metros -----------------------------------------------------
		String parMenuPrincipal = (String)request.getAttribute("MenuPrincipal");
		String parFuncionesMenu = (String)request.getAttribute("newMenu");
		String parEncabezado = (String)request.getAttribute("Encabezado");
		String parCuentas = (String)request.getAttribute("Cuentas");
		String parMensaje = (String)request.getAttribute("Mensaje");

		if(parMenuPrincipal == null) parMenuPrincipal = "";
		if(parFuncionesMenu == null) parFuncionesMenu = "";
		if(parEncabezado == null) parEncabezado = "";
		if(parCuentas == null) parCuentas = "";
		if(parMensaje == null) parMensaje = "";

		// --- Se preparan otras variables -------------------------------------------------
		Vector cuentas;

		cuentas = ((new mx.altec.enlace.servlets.TIFondeo())).creaCuentas(parCuentas, request);

		int a, b;
%>

<!--== COMIENZA CODIGO HTML ==========================================================-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>

<HEAD>
<TITLE>Bienvenido a Enlace Internet</TITLE>
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="Rafael Villar Villar">
<LINK rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

<!-- Scripts =====================================================================-->
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<SCRIPT>

	// --- BOTONES ---------------------------------------------------------------------
	function importa()
		{
		var campo = document.Forma.Archivo;
		while(campo.value.substring(0,1) == " ") campo.value = campo.value.substring(1);
		while(campo.value.substring(campo.value.length-1) == " ") campo.value = campo.value.substring(0,campo.value.length-1);
		if(campo.value == "")
			{
			campo.focus();
			cuadroDialogo("Debe especificar un nombre de archivo.",1);
			return;
			}
		chkSess();
		document.Forma.Accion.value = "IMPORTA";
		document.Forma.submit();
		}



	function validaDecimales() {
			//document.vista.importe.value="";
			var cantidadAux = document.Forma.Importe.value
			pos_punto = cantidadAux.indexOf (".")
			if(pos_punto>0) {
				parte_decimal=cantidadAux.substring(cantidadAux.indexOf(".")+1,cantidadAux.length);
				parte_decimal= parte_decimal.substring(0,2);
				parte_entera=cantidadAux.substring(0,cantidadAux.indexOf("."));
				document.Forma.Importe.value = parte_entera+"."+parte_decimal;
			}
	}

	function validaDigitos(object){
		txt=object.value;
		if (txt>=0 && txt<=999999999999999999999){
			return true;
	 	}
	 	else{
			return false;
	 	}
	}

	function agregar()
		{
		var trama = "@";
		var a, b=0;
		var numCta = ctaselec;
		var esEditable = (ctaSel() != -1);

		//Se valida
		if(document.Forma.CuentaTxt.value == "") {aviso("Debe seleccionar una cuenta"); return;}
		if(esEditable && !importeEsValido())
			{aviso("Indique un importe v&aacute;lido"); return;}

		if(esEditable && !document.Forma.Fondeo[0].checked && !document.Forma.Fondeo[1].checked)
			{aviso("Indique un sentido para el fondeo"); return;}

		//CCB - Se valida no permitir datos diferentes de numeros y decimales
		if (document.Forma.Importe.value != "")
		{
	  		if(validaDigitos(document.Forma.Importe)!=true)
	  		{
	  			document.Forma.Importe.focus();
	  			aviso("El importe es incorrecto");
	  			return;
	  		}
	    }

		if (!esEditable && (document.Forma.Importe.value != "") && habilitaConfirma)
			{
			confirma("Ha seleccionado agregar una cuenta en el nivel 1; por lo que se van a ignorar los datos capturados\n\n&iquest;Desea continuar?");
			accionConfirma = 1;
			return;
			}

		if(!esEditable)
			{
			document.Forma.Fondeo[0].checked = false;
			document.Forma.Fondeo[1].checked = false;
			document.Forma.Importe.value = "$0.00";
			}

		//Se construye la trama
		trama += numCta + "|";
		trama += ((ctaSel() == -1)?" |":(document.Forma.CtaSel[ctaSel()].value) + "|");
		trama += ctadescr + "|";

		trama += limpiaImporte();
		trama += (document.Forma.Fondeo[0].checked)?"|2":((document.Forma.Fondeo[1].checked)?"|1":"|0");

		//Se reinicializan las variables
		ctaselec = "";
		ctadescr = "";
		ctatipro = "";
		chkSess();
		document.Forma.CuentaTxt.value = "";

		document.Forma.Accion.value = "AGREGA";
		document.Forma.Trama.value = trama;
		document.Forma.submit();
		}

	function alta()
		{
		document.Forma.Accion.value = "ALTA_ARBOL";
		if(document.Forma.CtaSel.length == 3)
			{
			avisoError("No se puede dar de alta una estructura con solo una cuenta.");
			return;
			}
		else if(document.Forma.CtaSel.length == 2)
			{
			avisoError("La estructura est&aacute; vac&iacute;a");
			return;
			}
		chkSess();
		document.Forma.submit();
		}

	function modificar()
		{
		document.Forma.Accion.value = "MODIFICA_ARBOL";
		if(document.Forma.CtaSel.length == 3)
			{
			avisoError("No se puede guardar una estructura con solo una cuenta.");
			return;
			}
		else if(document.Forma.CtaSel.length == 2)
			{
			avisoError("La estructura est&aacute; vac&iacute;a");
			return;
			}
		chkSess();
		document.Forma.submit();
		}

	function borrar()
		{
		confirma("La estructura va a ser borrada, &iquest;Desea continuar?");
		accionConfirma = 2;
		}

	function editar()
		{
		var sel = ctaSel();
		if(sel == -1) {aviso("Debe seleccionar una cuenta para editar"); return;}
		chkSess();
		document.Forma.Accion.value = "EDITA";
		document.Forma.Trama.value = document.Forma.CtaSel[ctaSel()].value;
		document.Forma.submit();
		}

	function copiar()
		{
		chkSess();
		document.Forma.Accion.value = "COPIA";
		document.Forma.submit();
		}

	function eliminar()
		{
		var sel = ctaSel();
		if(sel == -1) {aviso("Debe seleccionar una cuenta para eliminar"); return;}

		confirma("Se va a borrar la cuenta seleccionada, as&iacute; como sus cuentas hija en caso de tenerlas. &iquest;Desea continuar?");
		accionConfirma = 3;
		return;
		}

	function imprimir()
		{scrImpresion();}

	// --- Validaciones ----------------------------------------------------------------

	function selUno(num)
		{
		var max = document.Forma.CtaSel.length - 2;
		for(a=0;a<max;a++) if(a != num) document.Forma.CtaSel[a].checked = false;
		}

	function ctaSel()
		{
		var sel = -1;
		var max = document.Forma.CtaSel.length - 2;
		for(a=0;a<max;a++) if(document.Forma.CtaSel[a].checked) {sel = a; break;}
		return sel;
		}

	function limpiaImporte()
		{
		var valor = quitaComas(document.Forma.Importe.value);
		var car;

		while(valor.length>0 && valor.substring(0,1) == " ") valor = valor.substring(1);
		while(valor.length>0 && valor.substring(valor.length-1) == " ")
			valor = valor.substring(0,valor.length-1);
		if(valor.substring(0,1)=="$") valor=valor.substring(1);
		while(valor.length>0 && valor.substring(0,1) == " ") valor = valor.substring(1);
		while(valor.length>0 && valor.substring(valor.length-1) == " ")
			valor = valor.substring(0,valor.length-1);
		return valor;
		}

	function importeEsValido()
		{
		var valor = limpiaImporte();
		if(valor.substring(0,1)== ".") return false;
		if(valor.indexOf(".")!=-1) if(valor.indexOf(".")+3 < valor.length) return false;
		if(valor == "") return false;
		while(valor.length > 0)
			{
			car = valor.substring(0,1);
			if(isNaN(parseInt(car)) && car!="0" && car!=".") return false;
			valor = valor.substring(1);
			}
		return true;
		}

	function quitaComas(texto)
		{
		for(a=0;a<texto.length;a++)
			if(texto.charAt(a) == ',') {texto = texto.substring(0,a) + texto.substring(a+1); a--;}
		return texto;
		}

	function imprimeUnaLinea(texto)
		{
		for(a=0;a<texto.length;a++)
			if(texto.substring(a,a+1) != " ")
				document.write(texto.substring(a,a+1));
			else
				{
				document.write("&");
				document.write("n");
				document.write("b");
				document.write("s");
				document.write("p");
				document.write(";");
				}
		}

	// --- Para cuadros de di�logo -----------------------------------------------------
	var respuesta = 0;
	var accionConfirma;
	var habilitaConfirma = true;

	function continua()
		{
		if(accionConfirma == 1)
			{
			if(respuesta == 1) agregar();
			}
		else if(accionConfirma == 2)
			{
			if(respuesta == 1)
				{
				chkSess();
				document.Forma.Accion.value = "BORRA_ARBOL";
				document.Forma.submit();
				}
			}
		else
			{
			if(respuesta == 1)
				{
				chkSess();
				document.Forma.Accion.value = "ELIMINA";
				document.Forma.Trama.value = document.Forma.CtaSel[ctaSel()].value;
				document.Forma.submit();
				}
			}
		habilitaConfirma = true;
		}

	function confirma(mensaje)
		{if(habilitaConfirma) cuadroDialogo(mensaje,2); habilitaConfirma = false;}

	function aviso(mensaje) {cuadroDialogo(mensaje,1);}

	function avisoError(mensaje) {cuadroDialogo(mensaje,3);}

	function ventanaHtml(codigo)
		{
		var miVentana = window.open('','trainerWindow','width=450,height=300,toolbar=no,scrollbars=yes');
		miVentana.document.write(codigo);
		miVentana.focus();
		}

	// --- Inicio y env�o --------------------------------------------------------------
	function inicia()
		{
		document.Forma.Fondeo[0].checked = false;
		document.Forma.Fondeo[1].checked = false;
		document.Forma.Importe.value = "";
		document.Forma.CuentaTxt.value = "";
		ctaselec = "";
		ctadescr = "";
		ctatipro = "";
		}

	function validaEnvio()
		{
		if(document.Forma.Accion.value == "") return false;
		return true;
		}

	// --- Funciones de men� -----------------------------------------------------------
	function MM_preloadImages()
		{ //v3.0
		var d=document;
		if(d.images)
			{
			if(!d.MM_p) d.MM_p=new Array();
			var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
			for(i=0; i<a.length; i++)
			if (a[i].indexOf("#")!=0)
				{d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}
			}
		}

	function MM_swapImgRestore()
		{ //v3.0
		var i,x,a=document.MM_sr;
		for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
		}

	function MM_findObj(n, d)
		{ //v3.0
		var p,i,x;
		if(!d) d=document;
		if((p=n.indexOf("?"))>0&&parent.frames.length)
			{
			d=parent.frames[n.substring(p+1)].document;
			n=n.substring(0,p);
			}
		if(!(x=d[n])&&d.all) x=d.all[n];
		for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
		return x;
		}

	function MM_swapImage()
		{ //v3.0
		var i,j=0,x,a=MM_swapImage.arguments;
		document.MM_sr=new Array;
		for(i=0;i<(a.length-2);i+=3)
			if((x=MM_findObj(a[i]))!=null)
				{
				document.MM_sr[j++]=x;
				if(!x.oSrc) x.oSrc=x.src;
				x.src=a[i+2];
				}
		}

	<%= parFuncionesMenu %>

	// --- Funciones y variables de Paulina Ventura ------------------------------------

	/************************************************\
	*  modificaci�n para integraci�n pva 07/03/2002  *
	\************************************************/

	var ctaselec;
	var ctadescr;
	var ctatipre;
	var ctatipro;
	var ctaserfi;
	var ctaprod;
	var ctasubprod;
	var tramadicional;
	var cfm;

	function PresentarCuentas()
		{
		msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1&hdnDivisa="+document.Forma.hdnDivisa.value,"Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
		msg.focus();
		}

	function actualizacuenta()
		{
		document.Forma.CuentaTxt.value = ctaselec + " -- " + ctadescr;
		}

	//CCB - Jan 21 2004 - Variables para cambio de valor de session en el servlet
	function chkSess(){
		if (document.Forma.tipoEstructura[0].checked){
			document.Forma.hdnAgrega.value = "MN";
		}
		if (document.Forma.tipoEstructura[1].checked){
			document.Forma.hdnAgrega.value = "USD";
		}
	}
	//CCB - Jan 21 2004 - Despliega el valor del tipo de estructura del rdbutton
	function msgTipoE(){
		if (document.Forma.tipoEstructura[0].checked){
			document.Forma.msgTipo.value = "Concentracion en pesos";
			document.Forma.hdnDivisa.value = "MN";
			chkSess();
			document.Forma.submit();
		}
		if (document.Forma.tipoEstructura[1].checked){
			document.Forma.msgTipo.value = "Concentracion en dolares";
			document.Forma.hdnDivisa.value = "USD";
			chkSess();
			document.Forma.submit();
		}
	}
	//CCB - Jan 21 2004 - Despliega el mensaje correspondiente a la divisa USD/MN
	function msgDivisa(){
		if (document.Forma.tipoEstructura[0].checked){
			document.Forma.msgTipo.value = "Concentracion en pesos";
			document.Forma.hdnDivisa.value = "MN";
		}
		if (document.Forma.tipoEstructura[1].checked){
			document.Forma.msgTipo.value = "Concentracion en dolares";
			document.Forma.hdnDivisa.value = "USD";
		}
	}

	</SCRIPT>

</HEAD>

<!-- CUERPO ==========================================================================-->

<BODY onload="inicia(); msgDivisa();"
	background="/gifs/EnlaceMig/gfo25010.gif" bgcolor=#ffffff>


<!-- MENU PRINCIPAL Y ENCABEZADO ---------------------------->
<table border="0" cellpadding="0" cellspacing="0" width="571">
	<tr valign="top">
		<td width="*"><%=parMenuPrincipal%></td>
	</tr>
</table>
<%=parEncabezado%>


<FORM  name="Forma" ENCTYPE="multipart/form-data"
	onSubmit="return validaEnvio()" method=post action="TIFondeo">
<%
		  String tipoCtas = "";
		  String Divisa = (String)session.getAttribute("Divisa");
		  if(Divisa!=null){
	  tipoCtas = Divisa;
	  tipoCtas = tipoCtas.trim();
		  }
		  System.out.println("Ya estoy en FONDEO JSP y la Divisa es >" + tipoCtas + "<");
	/*	  if(!session.getAttribute("accesoMod").equals("FONDEO"))
		  {
	*/
	/*if (session.getAttribute("ctasUSD").equals("Existe"))
	  {
	  	if(session.getAttribute("ctasMN").equals("Existe"))
	  	{
	tipoCtas = "AMBAS";
	  	}
	  	else
	  	{
	  		tipoCtas = "USD";
	  	}
	  }
	  else
	  {
	  	tipoCtas="MN";
	  }
	//  	  }	  */
%> <INPUT type=Hidden name="Accion" value=""> <INPUT
	type=Hidden name="Trama" value=""> <INPUT type=Hidden
	name="Cuentas" value="<%= parCuentas %>">

<DIV align=center><!--CCB - Jan 21 2004 - RdButtons para control de USD o MN-->
<input type="hidden" name="hdnDivisa" value=""> <input
	type="hidden" name="hdnAgrega" value="">

<table align="center">
	<tr>
		<td class="tabmovtexbol" width="200"><input type="hidden"
			name="msgTipo" size="30" value=""
			style="border: none; font-weight: bold; font-family: Arial;">
		<%
		if(tipoCtas.equals("USD")) { %>
		   Fondeo autom&aacute;tico en d&oacute;lares
	 <% } else { %>
		   Fondeo autom&aacute;tico en pesos
		<% } %>
		</td>
		<td>&nbsp;</td>
		<%
		System.out.println ( "\n\n\ntipoCtas " + tipoCtas );
		if(tipoCtas.equals("USD")){
		%>
		<td class="tabmovtex" width="211"><input type="radio"
			name="tipoEstructura" value="MN" onclick="javascript:msgTipoE();">Estructura
		en Moneda Nacional</td>
		<td class="tabmovtex" width="250"><input type="radio"
			name="tipoEstructura" value="USD" onclick="javascript:msgTipoE();"
			checked>Estructura en D&oacute;lares</td>
		<%
				  }
				  if(tipoCtas.equals("MN") || tipoCtas.equals("AMBAS")){
		%>
		<td class="tabmovtex" width="211"><input type="radio"
			name="tipoEstructura" value="MN" onclick="javascript:msgTipoE();"
			checked>Estructura en Moneda Nacional</td>
		<td class="tabmovtex" width="250"><input type="radio"
			name="tipoEstructura" value="USD" onclick="javascript:msgTipoE();">Estructura
		en D&oacute;lares</td>
		<%
		}
		%>
	</tr>
</table>


<TABLE>
	<TR valign=bottom>
		<TD align=center><!---->

		<TABLE width=400px class="textabdatcla" border=0 cellspacing=2
			cellpadding=3>
			<TR>
				<TD class="tittabdat">&nbsp;Seleccione la cuenta a integrar y
				las condiciones de fondeo autom&aacute;tico</TD>
			</TR>
			<TR>
				<TD class='tabmovtex'><BR>
				&nbsp;Cuenta: <!-- Campo de cuenta --> <INPUT type="Text"
					name="CuentaTxt" size=40 onFocus="blur()"> <A
					HREF="javascript:PresentarCuentas();"> <IMG
					SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0
					align=absmiddle> </A> <BR>
				<BR>

				<TABLE>
					<TR>
						<TD width=200 class="tabmovtex">Sentido del fondeo:</TD>
						<TD width=200 class="tabmovtex">Importe m&aacute;ximo:</TD>
					</TR>
					<TR valign=top>
						<TD style="font-weight:normal;" class="tabmovtex"><INPUT
							type="Radio" name="Fondeo" value="Doble"> Doble<BR>
						<INPUT type="Radio" name="Fondeo" value="Unico"> Unico</TD>
						<TD class="tabmovtex"><INPUT type="Text" name="Importe"
							size='10' onchange="validaDecimales();"></TD>
					</TR>
				</TABLE>

				<BR>
				<BR>

				</TD>
			</TR>
		</TABLE>

		</TD>
		<TD><!---->

		<TABLE class="textabdatcla" border=0 cellspacing=0 width=250>
			<TR>
				<TD class="tittabdat">&nbsp;Importar archivo</TD>
			</TR>
			<TR>
				<TD align=center class='tabmovtex'><BR>
				<INPUT type="File" name="Archivo" value=" Archivo "><BR>
				<A href="javascript:importa()"><IMG border=0
					src="/gifs/EnlaceMig/gbo25280.gif" alt="Importar"></A> <BR>
				</TD>
			</TR>
		</TABLE>

		</TD>
	<TR>
		<TD><!---->

		<DIV align=center><BR>
		<A href="javascript:agregar()"><IMG border=0
			src="/gifs/EnlaceMig/gbo25290.gif" alt="Agregar"></A> <BR>
		</DIV>

		</TD>
		<TD>&nbsp;</TD>
	</TR>
</TABLE>
<!----> <BR>
<BR>

<TABLE width=600 border=0 cellspacing=1>
	<TR>
		<TD class="tabmovtex"><!-- Aqu� va el total de cuentas --> Total
		de cuentas: <%=cuentas.size()%></TD>
	</TR>
</TABLE>
<TABLE width=600 class="textabdatcla" border=0 cellspacing=1>
	<TR>
		<TD colspan=5 class="tittabdat" align=center>Estructura de fondeo</TD>
	</TR>
	<TR>
		<TD class="tittabdat" align=center>Seleccione</TD>
		<TD class="tittabdat" align=center>Cuenta</TD>
		<TD class="tittabdat" align=center>Nivel</TD>
		<TD class="tittabdat" align=center>Sentido</TD>
		<TD class="tittabdat" align=center>Importe m&aacute;ximo</TD>
	</TR>




	<!-- Esta secci�n se repite para cada cuenta -->
	<%
			TI_CuentaFondeo cta;
			String espacio;
			String estilo;

			estilo = "textabdatobs";
			for(a=0;a<cuentas.size();a++)
		{
		estilo = (estilo.equals("textabdatobs"))?"textabdatcla":"textabdatobs";
		cta = (TI_CuentaFondeo)cuentas.get(a);
		espacio = "";
		for(b=0;b<cta.nivel()-1;b++) espacio += "<IMG src=\"/gifs/EnlaceMig/gbo25625.gif\">";
		espacio += (((new mx.altec.enlace.servlets.TIFondeo()).esHoja(cta,cuentas))?
		"<IMG src=\"/gifs/EnlaceMig/hoja.gif\">":
		"<IMG src=\"/gifs/EnlaceMig/folder.gif\">");
		espacio += "&nbsp;";
	%>
	<TR>
		<TD class="<%= estilo %>" align=center><INPUT type=CheckBox
			name="CtaSel" value="<%=cta.getNumCta()%>" onClick="selUno(<%= a %>)">
		</TD>
		<TD class="<%= estilo %>"><%=espacio + cta.getNumCta() + " " + cta.getDescripcion()%></TD>
		<TD class="<%= estilo %>" align=center><%=cta.nivel()%></TD>
		<TD class="<%= estilo %>" align=center><%=cta.getStrFondeo()%></TD>
		<TD class="<%= estilo %>" align=center><%=(cta.getFondeo() == 0)?"&nbsp;":cta.getStrImporte()%>
		</TD>
	</TR>
	<%
	}
	%>
	<!----------------------------------------------->





	<TR>
</TABLE>

<TABLE>
	<TR align=center>
		<TD><A href="javascript:alta()"><IMG border=0
			src="/gifs/EnlaceMig/gbo25480.gif" alt="Alta"></A><A
			href="javascript:modificar()"><IMG border=0
			src="/gifs/EnlaceMig/gbo25510.gif" alt="Modificar"></A><A
			href="javascript:borrar()"><IMG border=0
			src="/gifs/EnlaceMig/gbo25540.gif" alt="Borrar"></A><A
			href="javascript:editar()"><IMG border=0
			src="/gifs/EnlaceMig/gbo25615.gif" alt="Editar"></A><BR>
		<BR>
		<A href="javascript:copiar()"><IMG border=0
			src="/gifs/EnlaceMig/gbo25285.gif" alt="Copiar estructura"></A><A
			href="javascript:eliminar()"><IMG border=0
			src="/gifs/EnlaceMig/gbo25515.gif" alt="Eliminar registro"></A><A
			href="javascript:imprimir()"><IMG border=0
			src="/gifs/EnlaceMig/gbo25240.gif" alt="Imprimir"></A></TD>
	</TR>
</TABLE>

</DIV>

<!-- Las siguientes dos l�neas son importantes, no borrar --> <INPUT
	type=Hidden name="CtaSel" value="X"> <INPUT type=Hidden
	name="CtaSel" value="XX"></FORM>

</BODY>

</HTML>

<%
		if(!parMensaje.equals(""))
		{
		String formato = parMensaje; parMensaje = "";
		while((a=formato.indexOf("\""))!=-1)
	{parMensaje += formato.substring(0,a) + "\\\""; formato = formato.substring(a+1);}
		parMensaje += formato;
		formato = parMensaje; parMensaje = "";
		while((a=formato.indexOf("'"))!=-1)
	{parMensaje += formato.substring(0,a) + "\\'"; formato = formato.substring(a+1);}
		parMensaje += formato;
		String prefijo = parMensaje.substring(0,3);
		parMensaje = parMensaje.substring(3);
		if(prefijo.equals(":-)")) out.println("<SCRIPT>aviso('" + parMensaje + "');</SCRIPT>");
		if(prefijo.equals(":-/")) out.println("<SCRIPT>avisoError('" + parMensaje + "');</SCRIPT>");
		if(prefijo.equals("X-O")) out.println("<SCRIPT>ventanaHtml('" + parMensaje + "');</SCRIPT>");
		}
%>
