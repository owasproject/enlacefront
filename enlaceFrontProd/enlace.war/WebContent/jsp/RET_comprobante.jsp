<%@page import="java.util.StringTokenizer"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="mx.altec.enlace.bo.BaseResource"%>
<%@page import="mx.altec.enlace.bo.RET_OperacionVO"%>
<%@page import="mx.altec.enlace.bo.RET_CuentaAbonoVO"%>
<%@page import="mx.altec.enlace.utilerias.Util"%>

<%
	RET_OperacionVO beanOper = (RET_OperacionVO) session.getAttribute("beanOperacionRET");
	HttpSession sess = request.getSession ();
	BaseResource sesBase = (BaseResource) sess.getAttribute ("session");
	String contrato = sesBase.getContractNumber ();
	String nombre = sesBase.getNombreContrato ();
	String detalleAbono = "";
	String separador = "|";
	for(int i=0; i<beanOper.getDetalleAbono().size(); i++){
		if((i+1)==beanOper.getDetalleAbono().size())
			separador = "";
    	detalleAbono += ((RET_CuentaAbonoVO)beanOper.getDetalleAbono().get(i)).getNumCuenta() + "-"
    	             + aMoneda(((RET_CuentaAbonoVO)beanOper.getDetalleAbono().get(i)).getImporteAbono())
    	             + separador;
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<%@page import="java.util.HashMap"%>
<html>
<head>
<title>Comprobante</title>
<meta http-equiv="Content-Type" content="text/html;">
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/Convertidor.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<body bgcolor="#ffffff">
<form name="form1" method="post" action="">

  <table width="430" border="0" cellspacing="6" cellpadding="0" align="center">
    <tr>
     <td rowspan="2" valign="middle" class="titpag" align="left" width="35"><img SRC="/gifs/EnlaceMig/glo25040b.gif" BORDER="0"></td>
     <td rowspan="2" valign="middle" class="titpag" align="left" width="35"></td>
    </tr>
  </table>

  <table width="430" border="0" cellspacing="6" cellpadding="0" align="center">
    <tr>
      <td width="247" valign="top" class="titpag">Comprobante de operaci&oacute;n</td>
       <td valign="top" class="titpag" height="40"><img src="/gifs/EnlaceMig/glo25030.gif" width="152" height="30" alt="Enlace"></td>
    </tr>
    <tr>
      <td valign="top" colspan="2" class="titenccom">FX Online</td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
    </tr>
  </table>
  <table width="400" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td align="center">
        <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td colspan="3"> </td>
          </tr>
          <tr>
            <td width="21" background="/gifs/EnlaceMig/gfo25030.gif"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." name=".."></td>
            <td width="428" height="120" valign="top" align="center">
              <table width="380" border="0" cellspacing="2" cellpadding="3" background="/gifs/EnlaceMig/gau25010.gif">
                <tr>
                  <td class="tittabcom" align="right" width="0">Contrato:</td>
                  <td class="textabcom" align="left" nowrap><%=contrato%></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Nombre o Raz&oacute;n Social:</td>
                  <td class="textabcom" align="left" nowrap><%=nombre%></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Referencia Enlace:</td>
                  <td class="textabcom" align="left" nowrap><%=beanOper.getFolioEnla()%> </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Folio Operaci�n:</td>
                  <td class="textabcom" align="left" nowrap><%=beanOper.getFolioOper()%> </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Tipo de Operaci�n:</td>
                  <td class="textabcom" align="left" nowrap><%=Util.muestraTipoOperUser(beanOper.getTipoOperacion())%> </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Divisa Operante:</td>
                  <td class="textabcom" align="left" nowrap><%=beanOper.getDivisaOperante()%> </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Contra Divisa:</td>
                  <td class="textabcom" align="left" nowrap><%=beanOper.getContraDivisa()%> </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Tipo de Cambio:</td>
                  <td class="textabcom" align="left" nowrap><%=beanOper.getTcPactado()%> </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Importe Operaci�n:</td>
                  <td class="textabcom" align="left" nowrap><%=aMoneda(beanOper.getImporteTotOper())%> </td>
                </tr>
				<tr>
                  <td class="tittabcom" align="right" width="0">Cuenta Cargo:</td>
                  <td class="textabcom" align="left" nowrap><%=beanOper.getCtaCargo()%> </td>
                </tr>
				<tr>
                  <td class="tittabcom" align="right" width="0">Cuenta(s) Abono:</td>
                  <td class="textabcom" align="left" nowrap><%=detalleAbono.replace("|","<br/>")%> </td>
                </tr>
				<tr>
                  <td class="tittabcom" align="right" width="0">Usuario Registro:</td>
                  <td class="textabcom" align="left" nowrap><%=beanOper.getUsrRegistro()%> </td>
                </tr>
				<tr>
                  <td class="tittabcom" align="right" width="0">Usuario Autoriza:</td>
                  <td class="textabcom" align="left" nowrap><%=beanOper.getUsrAutoriza()%> </td>
                </tr>
				<tr>
                  <td class="tittabcom" align="right" width="0">Estatus de Operaci�n:</td>
                  <td class="textabcom" align="left" nowrap><font color=green><%=beanOper.getDescEstatusOperacion()%></font> </td>
                </tr>
				<tr>
                  <td class="tittabcom" align="right" width="0">Concepto de Operaci�n:</td>
                  <td class="textabcom" align="left" nowrap><%=beanOper.getConcepto()%> </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Fecha y Hora Pactado:</td>
                  <td class="textabcom" align="left" nowrap><%=beanOper.getFchHoraPactado()%> </td>
                </tr>
				<tr>
                  <td class="tittabcom" align="right" width="0">Fecha y Hora Complemento:</td>
                  <td class="textabcom" align="left" nowrap><%=beanOper.getFchHoraComplemento()%> </td>
                </tr>
				<tr>
                  <td class="tittabcom" align="right" width="0">Fecha y Hora Liberado:</td>
                  <td class="textabcom" align="left" nowrap><%=beanOper.getFchHoraLiberado()%> </td>
                </tr>
              </table>
            </td>
            <td width="21" background="/gifs/EnlaceMig/gfo25040.gif"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." name=".."></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td align="center"><br>
        <table width="150" border="0" cellspacing="0" cellpadding="0" height="22">
          <tr>
            <td align="right" width="83">
			<a href="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" width="83" height="22" border="0"></a>
            </td>
            <td align="left" width="71"><a href="javascript:;" onClick="window.close()"><img src="/gifs/EnlaceMig/gbo25200.gif" width="71" height="22" border="0"></a>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
 </form>
</body>
</html>

<%!
	/** Devuelve un texto que representa un valor monetario */
	private String aMoneda(String num){
		int pos = num.indexOf(".");
		if(pos == -1) {pos = num.length(); num += ".";}
		while(pos+3<num.length()) num = num.substring(0,num.length()-1);
		while(pos+3>num.length()) num += "0";
		while(num.length()<4) num = "0" + num;
		for(int y=num.length()-6;y>0;y-=3) num = num.substring(0,y) + "," + num.substring(y);
		return num;
	}

	/** Devuelve la descripci�n correcta del tipo de operaci�n*/
	private String getDesTipoOperacion(String tipoOperacion){
		if(tipoOperacion.trim().equals("VTA"))
			return "Venta";
		return "Compra";
	}
%>