<html>

<head>

<title>Comprobante</title>
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s25670">
<meta name="Proyecto" content="Portal">
<meta name="Version" content="1.0">
<meta name="Ultima version" content="16/05/2001 18:00">
<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico">

<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</head>
<body bgcolor="#ffffff">
<form name="form1" method="post" action="">
  <table width="545" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td valign="top" class="titpag" height="40"><img src="/gifs/EnlaceMig/glo25030.gif" width="152" height="30" alt="Enlace"></td>
      <td valign="middle" height="40" align="right" class="tittabcom"><%= request.getAttribute("fechaCorta" ) %><br>
        <%= request.getAttribute("ContUser" ) %></td>
    </tr>
  </table>
  <table width="545" border="0" cellspacing="6" cellpadding="0" align="center">
    <tr>
      <td width="247" valign="top" class="titpag">Comprobante de operaci&oacute;n</td>
      <td rowspan="2" valign="middle" class="titpag" align="right" width="35"><img src="/gifs/EnlaceMig/glo25040b.gif" alt=""></td>
    </tr>
    <tr>
      <td valign="top" class="titenccom">Comprobante de recepci&oacute;n de declaraci&oacute;n
        electr&oacute;nica </td>
    </tr>
  </table>
  <table width="545" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
    </tr>
  </table>
  <table width="540" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td align="center">
        <table width="545" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td colspan="3"> </td>
          </tr>
          <tr>
            <td width="21" background="/gifs/EnlaceMig/gfo25030.gif"><img src="/gifs/EnlaceMig/gau25010.gif" width="21" height="2" alt=".." name=".."></td>
            <td width="428" valign="top" align="center">
              <table border="0" cellspacing="2" cellpadding="3" background="/gifs/EnlaceMig/gau25010.gif" width="500">
                <tr>
                  <td class="tittabcom" align="right" width="111">No. de folio:</td>
                  <td class="textabcom" nowrap width="90"><%= request.getAttribute("numFolio" ) %></td>
                  <td class="tittabcom" nowrap width="137" align="right">RFC del
                    pago:</td>
                  <td width="74" class="textabcom"><%= request.getAttribute("RFC" ) %></td>
                </tr>

				<%= request.getAttribute("cuenta_razonSocial" ) %>

                <tr>
                  <td class="tittabcom" align="right" width="111">Importe de pago:</td>
                  <td class="textabcom" nowrap width="90"><%= request.getAttribute("importe" ) %></td>
                  <td class="tittabcom" nowrap width="137" align="right">Periodo
                    del pago:</td>
                  <td class="textabcom" nowrap width="128"><%= request.getAttribute("periodoInicio" ) %> al <%= request.getAttribute("periodoFin" ) %></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="111"><%= request.getAttribute("titTipoDeclaracion" ) %></td>
                  <td class="textabcom" nowrap width="90"><%= request.getAttribute("tipoDeclaracion" ) %></td>
                  <td class="tittabcom" nowrap width="137" align="right">Fecha
                    de presentaci&oacute;n:</td>
                  <td class="textabcom" nowrap width="128"><%= request.getAttribute("fechaCorta" ) %></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="111">Clave de banco:</td>
                  <td class="textabcom" nowrap width="90"><%= request.getAttribute("cveBanco" ) %></td>
                  <td class="tittabcom" nowrap align="right" width="137">Sucursal:</td>
                  <td class="textabcom" nowrap width="128"><%= request.getAttribute("sucursal" ) %></td>
                </tr>
              </table>
            </td>
            <td width="21" background="/gifs/EnlaceMig/gfo25040.gif"><img src="/gifs/EnlaceMig/gau25010.gif" width="21" height="2" alt=".." name=".."></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table width="545" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
  </table>
  <table width="545" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td align="center"><br>
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right" width="83">
              <a href="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" width="83" height="22" border="0" alt="Imprimir"></a>
            </td>
            <td align="left" width="71">
              <a href="javascript:;" onClick="window.close()"><img src="/gifs/EnlaceMig/gbo25200.gif" width="71" height="22" border="0" alt="Cerrar"></a>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  </form>

</body>

</html>