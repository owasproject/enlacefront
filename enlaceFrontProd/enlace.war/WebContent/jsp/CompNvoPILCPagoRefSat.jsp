
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="mx.altec.enlace.bo.PagoReferenciadoSatBO"%>
<%@page import="mx.altec.enlace.bo.ConsultaDatosPersonaBO"%>
<%
  System.out.println("Entrando a CompNvoPILCPagoRefSat.jsp");
  
  System.out.println("Generando comprobante de operaciones para pago de Impuestos (Pago Referenciado SAT)");

  String referencia390="";
  String referencia="";
  String contrato="";
  String desccto="";
  String usuario="";
  String concepto="";
  String importe="";
  String fecha="";
  String hora="";
  String ctacargo="";
  String titcta="";
  String sufijoHora = "";
  String llaveDePago = "";  

  referencia=(request.getParameter("referencia")==null)?"":(String)request.getParameter("referencia");
  contrato=(request.getParameter("contrato")==null)?"":(String)request.getParameter("contrato");
  desccto=(request.getParameter("desccto")==null)?"":(String)request.getParameter("desccto");
  usuario=(request.getParameter("usuario")==null)?"":(String)request.getParameter("usuario");
  concepto=(request.getParameter("concepto")==null)?"":(String)request.getParameter("concepto");
  importe=(request.getParameter("importe")==null)?"":(String)request.getParameter("importe");
  fecha=(request.getParameter("fecha")==null)?"":(String)request.getParameter("fecha");
  hora=(request.getParameter("hora")==null)?"":(String)request.getParameter("hora");
  ctacargo=(request.getParameter("ctacargo")==null)?"":(String)request.getParameter("ctacargo");
  titcta= (request.getParameter("titcta")==null)?"":(String)request.getParameter("titcta");
  
  System.out.print("Referencia enlace="+referencia);
  
  ConsultaDatosPersonaBO consDatPersBo = new ConsultaDatosPersonaBO();
  PagoReferenciadoSatBO pagRefSatBo = new PagoReferenciadoSatBO();
  if( "".equals(titcta) ){  		
  	titcta = pagRefSatBo.consultaRazonSocCtaCargo(ctacargo, contrato);
  } 
  if( usuario.trim().length() < 10 ){
  	usuario = usuario + " " +consDatPersBo.consultarNombrePersona(usuario);
  }
   
  
  if( request.getParameter( "referencia390" ) != null && request.getParameter( "referencia390" ) != "0" ){
	 referencia390=(String)request.getParameter( "referencia390" );
  } else { 	
  	  String numeOP = "";  
  	  numeOP = pagRefSatBo.consultaNumeroDeOperacion(referencia, ctacargo, contrato, fecha);
	  referencia390 = numeOP == null | "".equals(numeOP) ? "0" : numeOP.trim();
   }
   
  if( !"0".equals(referencia390) && !"0000".equals(referencia390) ){
   		referencia390.replaceFirst ("^0*", "");
  }
  boolean tieneLlavePago = false;
  if( "02".equals(concepto.substring(0,2)) ){
  	tieneLlavePago = true;
	hora="";
  	llaveDePago = pagRefSatBo.obtieneLlaveDePago(concepto, referencia390);
  	if( llaveDePago == null || "X".equals(llaveDePago) || "".equals(llaveDePago) ){
  		llaveDePago = "";
  	}else{
		hora = llaveDePago.substring(10,15);
  		llaveDePago = llaveDePago.substring(0,10);
  	}
  }
  if( "01".equals(concepto.substring(0,2)) ){
	hora="";
  	llaveDePago = pagRefSatBo.obtieneLlaveDePago(concepto, referencia390);
  	if( llaveDePago == null || "X".equals(llaveDePago) || "".equals(llaveDePago) ){
  		llaveDePago = "";
  	}else{
		hora = llaveDePago.substring(10,15);
  	}
  }
  if( !"".equals(hora) ){
	sufijoHora = "HRS";
  } 
  System.out.print("Referencia 390="+referencia390);
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<title>Comprobante</title>
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s25390">
<meta name="Proyecto" content="Portal">
<meta name="Ultima version" content="07/05/2001 09:00">
<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico">
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/Convertidor.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />
</head>
<body bgcolor="#ffffff">
<form name="form1" method="post" action="">

  <table width="430" border="0" cellspacing="6" cellpadding="0" align="center">
    <tr>
     	<td rowspan="2" valign="top" valign="middle" class="titpag" align="left" width="35"><img SRC="/gifs/EnlaceMig/LogoSantanderMexico.gif" width="290px" height="104px" BORDER="0" /></td>
	 	<td valign="top" align="right" style="font-family: Arial; font-size: 11px;" width="308" height="8">Plaza</td>
	 	<td valign="top" align="right" style="font-family: Arial; font-size: 11px;" width="62" height="8"> 180</td>
    </tr>
    <tr>
		<td valign="top" align="right" style="font-family: Arial; font-size: 11px;" width="308" height="35">Sucursal</td>
		<td valign="top" align="right" style="font-family: Arial; font-size: 11px;" width="62" height="35"> 981</td>
    </tr>
  </table>

  <table width="430" border="0" cellspacing="6" cellpadding="0" align="center">
    <tr>
      <td width="247" valign="top" class="titpag">Comprobante de Operaci&oacute;n</td>
       <td valign="top" class="titpag" height="40"><img src="/gifs/EnlaceMig/glo25030.gif" width="152" height="30" alt="Enlace" /></td>
    </tr>
    <tr>
      <td valign="top" colspan="2" class="titenccom">Recibo Bancario de Pago de Contribuciones Federales</td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="." /></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="." /></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="." /></td>
    </tr>
  </table>
  <table width="400" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td align="center">
        <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td colspan="3"> </td>
          </tr>
          <tr>
            <td width="21" background="/gifs/EnlaceMig/gfo25030.gif"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." name=".." /></td>
            <td width="428" height="120" valign="top" align="center">
              <table width="380" border="0" cellspacing="2" cellpadding="3" background="/gifs/EnlaceMig/gau25010.gif">              	
                <tr>
                  <td class="tittabcom" align="right" width="0">Contrato:</td>
                  <td class="textabcom" align="left" nowrap><%=contrato%> &nbsp; <%=desccto%> </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Usuario:</td>
                  <td class="textabcom" align="left" nowrap><%=usuario%> </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">L&iacute;nea de Captura:</td>
                  <td class="textabcom" align="left" nowrap> <%=concepto%> </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Cuenta de Cargo:</td>
                  <td class="textabcom" align="left"><%=ctacargo%> &nbsp; <%=titcta%>
                  </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Importe Pagado:</td>
                  <td class="textabcom" align="left" nowrap> <%=importe%></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Fecha y Hora de Pago:</td>
                  <td class="textabcom" align="left"> <%=fecha%> &nbsp;&nbsp;&nbsp;<%=hora%>&nbsp;<%=sufijoHora%> </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">N&uacute;mero de Operaci&oacute;n:</td>
                  <td class="textabcom" align="left" nowrap> <%=referencia390%> </td>
                </tr>                
                <tr>
                  <td class="tittabcom" align="right" width="0">Medio de Presentaci&oacute;n:</td>
                  <td class="textabcom" align="left" nowrap>INTERNET</td>
                </tr>
                <tr style="display: <%=tieneLlavePago ? " ":"none" %>;" >
                  <td class="tittabcom" align="right" width="0">Llave de Pago:</td>
                  <td class="textabcom" align="left" nowrap> <%=llaveDePago%> </td>
                </tr>               
              </table>
            </td>
            <td width="21" background="/gifs/EnlaceMig/gfo25040.gif"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." name=".." /></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="." /></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="." /></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="." /></td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="6" cellpadding="0" align="center">
    <tr>
      	<td width="247" style="font-family: Arial; font-size: 9px;" height="48">Para cualquier aclaraci&oacute;n comunicarse a Superl&iacute;nea.</td>
      	<td valign="top" class="titpag" height="60"><img src="/gifs/EnlaceMig/SuperLineaEmpresarial.gif" alt="SuperLinea Empresarial" /></td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td align="center"><br />
        <table width="150" border="0" cellspacing="0" cellpadding="0" height="22">
          <tr>
            <td align="right" width="83">
			<a href="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" width="83" height="22" border="0" /></a>
            </td>
            <td align="left" width="71"><a href="javascript:;" onClick="window.close()"><img src="/gifs/EnlaceMig/gbo25200.gif" width="71" height="22" border="0" /></a>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
 </form>
</body>
</html>