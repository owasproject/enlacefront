<%@ page import="java.util.*" %>
<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<%!

// Valores de nombres de jpg sin extensi�n
final String[] color = {"porpagar", "liberados", "pagados", "anticipados", "cancelados", "rechazados", "vencidos","recibidos"};

// "recibidos",

// Genera c�digo html para un "cuadro de la gr�fica
String cuadroGrafica(int x, String color)
	{

	String opcion1 = "<TABLE border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"26\" height=\"49\">" +
		"<TR><TD width=\"26\" height=\"49\" bgcolor=\"#e8e8e8\">&nbsp;</TD></TR>" +
		"<TR><TD bgcolor=\"#000000\"><IMG src=\"/gifs/EnlaceMig/vencidos\"" +
		" width=\"1\" height=\"1\"></TD></TR></TABLE>";

	String opcion2 = "<TABLE border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"26\" height=\"49\">" +
		"<TR><TD colspan=\"2\" width=\"26\" height=\"49\" bgcolor=\"#e8e8e8\">&nbsp;</TD></TR>" +
		"<TR><TD><IMG src=\"/gifs/EnlaceMig/" + color + ".jpg\" width=\"22\"" +
		" height=\"1\"></TD><TD bgcolor=\"#000000\"><IMG src=\"/gifs/EnlaceMig/vencidos\"" +
		" width=\"1\" height=\"1\"></TD></TR></TABLE>";

	String opcion3 = "<TABLE border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"26\" height=\"49\">" +
		"<TR><TD width=\"22\" height=\"" + (50 - x) + "\" bgcolor=\"#e8e8e8\"><IMG src=\"/gifs/EnlaceMig/recibidos.jpg\"" +
		" width=\"1\" height=\"1\"></TD>" +
		"<TD rowspan=\"2\" width=\"4\" height=\"49\" bgcolor=\"#e8e8e8\"><IMG src=\"/gifs/EnlaceMig/recibidos.jpg\"" +
		" width=\"1\" height=\"1\"></TD></TR>" +
		"<TR><TD height=\"" + (x - 1) + "\"><IMG src=\"/gifs/EnlaceMig/" + color + ".jpg\" width=\"22\" height=\"" +
		(x - 1) + "\"></TD></TR><TR><TD><IMG src=\"/gifs/EnlaceMig/" + color + ".jpg\" width=\"22\"" +
		" height=\"1\"></TD><TD bgcolor=\"#000000\"><IMG src=\"/gifs/EnlaceMig/vencidos\"" +
		" width=\"1\" height=\"1\"></TD></TR></TABLE>";

	String opcion4 = "<TABLE border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"26\" height=\"49\">" +
		"<TR><TD width=\"22\" height=\"49\" bgcolor=\"#e8e8e8\"><IMG src=\"/gifs/EnlaceMig/" + color + ".jpg\"" +
		" width=\"22\" height=\"49\"></TD>" +
		"<TD width=\"4\" height=\"49\" bgcolor=\"#e8e8e8\"><IMG src=\"/gifs/EnlaceMig/recibidos.jpg\"" +
		" width=\"1\" height=\"1\"></TD></TR>" +
		"<TR><TD><IMG src=\"/gifs/EnlaceMig/" + color + ".jpg\" width=\"22\"" +
		" height=\"1\"></TD><TD bgcolor=\"#000000\"><IMG src=\"/gifs/EnlaceMig/vencidos\"" +
		" width=\"1\" height=\"1\"></TD></TR></TABLE>";

	String opcion;

	if(x == 0) opcion=opcion1; else if(x == 1) opcion=opcion2; else if(x < 50) opcion=opcion3; else opcion=opcion4;

	return opcion;
	}

// Genera c�digo html para una "l�nea" de la gr�fica
String lineaGrafica(int[] valor)
	{
	String codigo="<TABLE width=\"208\" height=\"50\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><TR>";
	for(int x=0;x<8;x++) codigo += "<TD>" + cuadroGrafica(valor[x],color[x]) + "</TD>";
	codigo += "</TR></TABLE>";
	return codigo;
	}

// Genera c�digo html para un dibujo de gr�fica
String dibGrafica(int[] valor)
	{
	int[] x = new int[8];
	String[] linea = new String[4];
	String codigo;
	int a,b;

	for(a=0;a<4;a++)
		{
		for(b=0;b<8;b++)
			{
			if(valor[b]>50) x[b] = 50; else if(valor[b]>0) x[b]=valor[b]; else x[b]=0;
			valor[b]-=50;
			}
		linea[a]=lineaGrafica(x);
		}
	codigo = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
	for(a=3;a>=0;a--) codigo += "<TR><TD colspan=\"7\">" + linea[a] + "</TD></TR>";
	codigo += "</table>";
	return codigo;
	}

// Genera c�digo html para un pie de gr�fica
String pie(long[] valores, boolean moneda)
	{
	      String valordivisa= Long.toString(valores[8]);

	      if(valordivisa.trim().equals("1"))
				valordivisa="MXN";
		  else if(valordivisa.trim().equals("2"))
				valordivisa="USD";
		  else if(valordivisa.trim().equals("0"))
				valordivisa="";


	final String estiloNumEscala = "style=\"text-align:left; font-size:10px; font-family:arial;" +
			" vertical-align:top;\"";

	String codigo = "<TABLE width=\"280\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
			"<TR><TD><IMG src='/gifs/EnlaceMig/porpagar.jpg' height=\"10\" width=\"10\" align=\"left\">" +
			"<DIV " + estiloNumEscala + ">" +
			"Por pagar: " + ((moneda)?aMoneda100("" + valores[0]):("" + valores[0])) + " " + /*valordivisa +*/ "</DIV></TD><TD>" +
			"<IMG src='/gifs/EnlaceMig/liberados.jpg' height=\"10\" width=\"10\" align=\"left\">" +
			"<DIV " + estiloNumEscala + ">" +
			"Liberados: " + ((moneda)?aMoneda100("" + valores[1]):("" + valores[1])) +  " " + /*valordivisa +*/"</DIV></TD></TR><TR><TD>" +
			"<IMG src='/gifs/EnlaceMig/pagados.jpg' height=\"10\" width=\"10\" align=\"left\">" +
			"<DIV " + estiloNumEscala + ">" +
			"Pagados: " + ((moneda)?aMoneda100("" + valores[2]):("" + valores[2])) +  " " + /*valordivisa +*/"</DIV></TD><TD>" +
			"<IMG src='/gifs/EnlaceMig/anticipados.jpg' height=\"10\" width=\"10\" align=\"left\">" +
			"<DIV " + estiloNumEscala + ">" +
			"Anticipados: " + ((moneda)?aMoneda100("" + valores[3]):("" + valores[3])) +  " " + /*valordivisa +*/"</DIV></TD></TR><TR><TD>" +
			"<IMG src='/gifs/EnlaceMig/cancelados.jpg' height=\"10\" width=\"10\" align=\"left\">" +
			"<DIV " + estiloNumEscala + ">" +
			"Cancelados: " + ((moneda)?aMoneda100("" + valores[4]):("" + valores[4])) +  " " + /*valordivisa +*/ "</DIV></TD><TD>" +
			"<IMG src='/gifs/EnlaceMig/rechazados.jpg' height=\"10\" width=\"10\" align=\"left\">" +
			"<DIV " + estiloNumEscala + ">" +
			"Rechazados: " + ((moneda)?aMoneda100("" + valores[5]):("" + valores[5])) +  " " + /*valordivisa +*/ "</DIV></TD></TR><TR><TD>" +
			"<IMG src='/gifs/EnlaceMig/vencidos.jpg' height=\"10\" width=\"10\" align=\"left\">" +
			"<DIV " + estiloNumEscala + ">" +
			"Vencidos: " + ((moneda)?aMoneda100("" + valores[6]):("" + valores[6])) +  " " + /*valordivisa +*/"</DIV></TD><TD>" +
			"<IMG src='/gifs/EnlaceMig/recibidos.jpg' height=\"10\" width=\"10\" align=\"left\">" +
			"<DIV " + estiloNumEscala + ">" +
			"Recibidos: " + ((moneda)?aMoneda100("" + valores[7]):("" + valores[7])) +  " " + /*valordivisa +*/"</DIV></TD></TR></TABLE>";

	return codigo;
	}

// Genera c�digo html para una gr�fica completa
String grafica(long[] valores, String titulo, boolean moneda)
	{
	final String estiloNumEscala = "style=\"text-align:right; font-size:10px; font-family:arial;" +
			" vertical-align:bottom;\"";
	long max=maximo(valores);
	int[] niveles = traspola(valores,max);
	String max44 = "" + max;
	String max34 = "" + (max*3/4);
	String max24 = "" + (max*2/4);
	String max14 = "" + (max/4);
	String max04 = "0";

	boolean frac3 = (max*3%4) != 0;
	boolean frac2 = (max*2%4) != 0;
	boolean frac1 = (max%4) != 0;

	if(moneda)
		{

		max44 = aMoneda100(max44);
		max34 = aMoneda100(max34);
		max24 = aMoneda100(max24);
		max14 = aMoneda100(max14);
		max04 = "0.00";
		}

	if(max<50)
		{
		if(max34.equals(max24) || frac3) max34="";
		if(max24.equals(max14) || frac2) max24="";
		if(max14.equals(max04) || frac1) max14="";
		}

	String codigo = "" +
		"<TABLE border=\"0\" cellspacing=\"0\" cellpadding=\"0\" height=\"250\"><TR>" +
		"<TD height=\"250\" rowspan=\"2\">" +
		"<TABLE border=\"0\" cellspacing=\"0\" cellpadding=\"0\" height=\"250\">" +
		"<TR><TD height=\"35\">&nbsp;</TD></TR>" +
		"<TR><TD><DIV " + estiloNumEscala + ">" + max44 + "</DIV></TD></TR>" +
		"<TR><TD height=\"35\">&nbsp;</TD></TR>" +
		"<TR><TD><DIV " + estiloNumEscala + ">" + max34 + "</DIV></TD></TR>" +
		"<TR><TD height=\"35\">&nbsp;</TD></TR>" +
		"<TR><TD><DIV " + estiloNumEscala + ">" + max24 + "</DIV></TD></TR>" +
		"<TR><TD height=\"35\">&nbsp;</TD></TR>" +
		"<TR><TD><DIV " + estiloNumEscala + ">" + max14 + "</DIV></TD></TR>" +
		"<TR><TD height=\"35\">&nbsp;</TD></TR>" +
		"<TR><TD><DIV " + estiloNumEscala + ">" + max04 + "</DIV></TD></TR></TABLE></TD>" +
		"<TD height=\"50\" align=\"left\" colspan=\"2\">&nbsp;" +
		"</TD></TR><TR><TD>" + dibGrafica(niveles) + "</TD></TR><TR><TD height=\"50\" colspan=\"2\" align=\"center\">" +
		"<SPAN style=\"font-family:tahoma; font-size:17px;\">" + titulo + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</SPAN><br>" +
		pie(valores,moneda) + "</TD></TR></TABLE>";
	return codigo;
	}

// Regresa un m�ximo para usar en la gr�fica
long maximo(long[] valor)
	{
	long max=0;
	int a, lon;
	for(a=0;a<valor.length;a++) if(max<valor[a]) max=valor[a];
	String str = "" + max;
	max=Integer.parseInt(str.substring(0,1));
	lon=str.length()-1;
	if(max<9) {max++;} else {max=1; lon++;}
	str = "" + max;
	for(a=0;a<lon;a++) str += "0";
	max = Long.parseLong(str);
	return max;
	}

// regresa un vector traspolado para usar directamente en el dibujo de la gr�fica
int[] traspola(long[] valor,long maximo)
	{
	int a;
	int res[] = new int[valor.length];
	for(a=0;a<valor.length;a++) res[a]=(int)((valor[a]*200)/maximo);
	return res;
	}

// Devuelve un texto que representa un valor monetario
private String aMoneda100(String num)
	{

	int pos;
	if((pos=num.indexOf("."))!=-1) num=num.substring(0,pos);
	long x = (long)Long.parseLong(num);
	String s = "" + x;
	while(s.length()<3) s = "0" + s;
	s=s.substring(0,s.length()-2) + "." + s.substring(s.length()-2);
	for(int y=s.length()-6;y>0;y-=3) s = s.substring(0,y) + "," + s.substring(y);
	return "" + s;
	}


	private String conDivisa(String num, String div)
	{

	int pos;
	if((pos=num.indexOf("."))!=-1) num=num.substring(0,pos);
	long x = (long)Long.parseLong(num);
	String s = "" + x;
	while(s.length()<3) s = "0" + s;
	s=s.substring(0,s.length()-2) + "." + s.substring(s.length()-2);
	for(int y=s.length()-6;y>0;y-=3) s = s.substring(0,y) + "," + s.substring(y);
	return "" + s + div;
	}
	private String aMoneda200(String num)
	{

	int pos;
	if((pos=num.indexOf("."))!=-1) num=num.substring(0,pos);
	long x = (long)Long.parseLong(num);
	String s = "" + x;
	while(s.length()<3) s = "0" + s;
	s=s.substring(0,s.length()-2) + "." + s.substring(s.length()-2);
	for(int y=s.length()-6;y>0;y-=3) s = s.substring(0,y) + "," + s.substring(y);
	return "" + s;
	}


%>

<%!

//
void agregaImporte(String clave, String importe, Hashtable info)
	{
	long monto = (long)(Double.parseDouble(importe)*100);
	Long valor = new Long(monto);
	if(info.containsKey(clave))
		{
		long x = ((Long)info.get(clave)).longValue();
		valor = new Long(x + monto);
		}
	info.put(clave,valor);
	}

//
void agregaNum(String clave, Hashtable info)
	{
	Long valor = new Long(1L);
	if(info.containsKey(clave))
		{
		long x = ((Long)info.get(clave)).longValue();
		valor = new Long(++x);
		}
	info.put(clave,valor);
	}

//
long tomaImporte100(String clave, String tipo, Hashtable info)
	{
	String key = clave + "|" + tipo;
	Long valor = ((Long)info.get(key));
	long resultado = 0;
	if(valor != null) resultado = valor.longValue();
	return resultado;
	}

//
long tomaValor(String clave, String tipo, Hashtable info)
	{
	String key = clave + "|" + tipo;
	Long valor = ((Long)info.get(key));
	long resultado = 0;
	if(valor != null) resultado = valor.longValue();
	return resultado;
	}

//
String numAtipo(int num)
	{
	String x = "no conocido";
	switch(num)
		{
		case 1: x = "R"; break;
		case 2: x = "L"; break;
		case 3: x = "P"; break;
		case 4: x = "A"; break;
		case 5: x = "C"; break;
		case 6: x = "Z"; break;
		case 7: x = "V"; break;
		}
	return x;
	}

//
String suma(String sumando1, String sumando2)
	{
	long suma1, suma2;
	boolean esMoneda = sumando1.startsWith("$") || sumando2.startsWith("$");
	int pos;
	String total;

	if(sumando1.startsWith("<4"))
		{
		sumando1=sumando1.substring(1);
		while((pos=sumando1.indexOf(","))!=-1) sumando1=sumando1.substring(0,pos)+sumando1.substring(pos+1);
		while((pos=sumando1.indexOf("."))!=-1) sumando1=sumando1.substring(0,pos)+sumando1.substring(pos+1);
		}
	if(sumando2.startsWith("$"))
		{
		sumando2=sumando2.substring(1);
		while((pos=sumando2.indexOf(","))!=-1) sumando2=sumando2.substring(0,pos)+sumando2.substring(pos+1);
		while((pos=sumando2.indexOf("."))!=-1) sumando2=sumando2.substring(0,pos)+sumando2.substring(pos+1);
		}
	suma1=Long.parseLong(sumando1);
	suma2=Long.parseLong(sumando2);
	total = "" + (suma1 + suma2);

	if(esMoneda)
		{
		while(total.length()<3) total = "0" + total;
		total=total.substring(0,total.length()-2) + "." + total.substring(total.length()-2);
		for(int y=total.length()-6;y>0;y-=3) total = total.substring(0,y) + "," + total.substring(y);
		total = total;
		}

	return total;
	}

%>

<html>

<head>

	<title>Banca Virtual</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
	<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
	<script language = "JavaScript" SRC= "/EnlaceMig/scrImpresion.js"></script>

	<SCRIPT language = "JavaScript">

	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && d.getElementById) x=d.getElementById(n); return x;
	}

	<%= request.getAttribute("newMenu") %>

	</SCRIPT>
	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad=" MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">


	<!-- MENU PRINCIPAL -->
	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top"><td width="*"><%= request.getAttribute("MenuPrincipal") %></td></tr>
	</TABLE>
	<%= request.getAttribute("Encabezado") %>

	<div align="center">

	<% String cuentaCargo = request.getAttribute("cuentaCargo")!=null?(String)request.getAttribute("cuentaCargo"):"";
		if(cuentaCargo==null || cuentaCargo.length()<=0)
			cuentaCargo = "Todas";
	 	String nomProve = request.getAttribute("nomProve")!=null?(String)request.getAttribute("nomProve"):"";
		if(nomProve==null || nomProve.length()<=0)
			nomProve = "Todos";
		
		int i_mxn = 0;
		int i_usd = 0;
		Hashtable nombres = new Hashtable();
		Hashtable numeros = new Hashtable();
		Hashtable importes = new Hashtable();
		StringTokenizer info = new StringTokenizer((String)request.getAttribute("Info"),"\n");
		StringTokenizer linea;
		String clave;
		String nombre;
		String redivisa = (String)request.getAttribute("ReDivisa");
		try{
			while(info.hasMoreTokens()){
				String auxLinea = info.nextToken();
				int pos = 0;
				while((pos=auxLinea.indexOf("||")) != -1) auxLinea = auxLinea.substring(0,pos+1) + " " + auxLinea.substring(pos+1);
				linea = new StringTokenizer(auxLinea,"|");
				clave = linea.nextToken();
				String clave2 = linea.nextToken();
				String imp = linea.nextToken();
				String div = linea.nextToken();
				if(div.equalsIgnoreCase("MXN")){
					i_mxn++;
				}else if(div.equalsIgnoreCase("USD")){
					i_usd++;
				}				
			}
		}catch(Exception e)
			{EIGlobal.mensajePorTrace("Excepci�n en coEstadisticasCon.jsp (try 1)\n" + e.getMessage(), EIGlobal.NivelLog.DEBUG);}
			
	 %>
	<TABLE border="0" cellspacing="5" cellpadding="10">
	<tr>
		<td colspan="3">
			<%if(i_mxn>0){ %>
			<table cellpadding='2' cellspacing='1' width="760">
				<tr><td width="80%" class = 'texenccon'>N&uacute;mero de Documentos en MONEDA NACIONAL</td>
				<td  width="20%" class = 'texenccon' rowspan="3"  align="right">Del <%= request.getAttribute("fecha1") %> al <%= request.getAttribute("fecha2") %></td></tr>
				<tr><td  class = 'texenccon'>Cuenta Cargo: <%= cuentaCargo %></td></tr>
				<tr><td  class = 'texenccon'>Proveedor: <%= nomProve %></td></tr>
			</table>
		    <%}%>
		</td>
	</tr>
	<TR>
	<%	
		final String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio",
		"Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		boolean moneda = ((String)request.getAttribute("Cant")).equals("0");

		nombres = new Hashtable();
		numeros = new Hashtable();
		importes = new Hashtable();
		info = new StringTokenizer((String)request.getAttribute("Info"),"\n");
		redivisa = (String)request.getAttribute("ReDivisa");

		// Se guarda la info recibida en los tres hashtables
		try{
			while(info.hasMoreTokens()){
				String auxLinea = info.nextToken();
				int pos = 0;
				while((pos=auxLinea.indexOf("||")) != -1) auxLinea = auxLinea.substring(0,pos+1) + " " + auxLinea.substring(pos+1);
				linea = new StringTokenizer(auxLinea,"|");
				clave = linea.nextToken();
				String clave2 = linea.nextToken();
				String imp = linea.nextToken();
				String div = linea.nextToken();
				if(div.equalsIgnoreCase("MXN")){
					nombres.put(clave,"");
					clave += "|" + clave2;
					System.out.println("JSP-clave:["+clave+"]");
					agregaNum(clave, numeros);
					agregaImporte(clave, imp, importes);
				}
			}
		}catch(Exception e)
			{EIGlobal.mensajePorTrace("Excepci�n en coEstadisticasCon.jsp (try 1)\n" + e.getMessage(), EIGlobal.NivelLog.DEBUG);}

		// Se crea c�digo html y se guarda para primero calcular el orden en que se deben imprimir los meses
		Hashtable codigo = new Hashtable();
		java.util.Enumeration x = nombres.keys();
		int n, a, b;
		long valores[] = new long[9];
		while(x.hasMoreElements())
			{
			clave = (String)x.nextElement();
			valores[7]=0;
			for(n=1;n<=7;n++)
		{
		valores[n-1]=(moneda)?tomaImporte100(clave,numAtipo(n),importes):tomaValor(clave,numAtipo(n),numeros);
		/*if(n!=6)*/ valores[7]+=valores[n-1];
		}

		valores[8]= Long.parseLong(redivisa.trim());


			codigo.put(clave,"<TD align='center'>" + grafica(valores,meses[Integer.parseInt(clave)-1],moneda) + "</TD>");
			}
		nombres = null; numeros = null; importes = null; System.gc();

		// Se calcula el orden en que se imprimir�n los meses
		int mesActual = (new java.util.GregorianCalendar()).get(java.util.Calendar.MONTH) - 1;
		boolean existeAnterior = false;
		if(mesActual<1) mesActual += 12;
		String mes1 = ((mesActual<10)?"0":"") + (mesActual++); if(mesActual>12) mesActual = 1;
		String mes2 = ((mesActual<10)?"0":"") + (mesActual++); if(mesActual>12) mesActual = 1;
		String mes3 = ((mesActual<10)?"0":"") + (mesActual++); if(mesActual>12) mesActual = 1;

		// Se imprimen los meses
		String mesHTML;
		mesHTML = (String)codigo.get(mes1); if(mesHTML != null) out.println(mesHTML);
		mesHTML = (String)codigo.get(mes2); if(mesHTML != null) out.println(mesHTML);
		mesHTML = (String)codigo.get(mes3); if(mesHTML != null) out.println(mesHTML);
	%>
	</TR>
	</TABLE>
	<BR>
	<TABLE border="0" cellspacing="5" cellpadding="10">
	<tr>
		<td colspan="3">
    		<% if(i_usd>0){%>
			<table align="left" cellpadding='2' cellspacing='1' width="760">
				<tr><td  width="80%" class = 'texenccon'>N&uacute;mero de Documentos en D&Oacute;LAR AMERICANO</td>
				<td  width="20%" class = 'texenccon' rowspan="3" align="right">Del <%= request.getAttribute("fecha1") %> al <%= request.getAttribute("fecha2") %></td></tr>
				<tr><td  class = 'texenccon'>Cuenta Cargo: <%= cuentaCargo %></td></tr>
				<tr><td  class = 'texenccon'>Proveedor: <%= nomProve %></td></tr>
			</table>
			<%}%>
		</td>
	</tr>
	<TR>
	<%
		moneda = ((String)request.getAttribute("Cant")).equals("0");

		nombres = new Hashtable();
		numeros = new Hashtable();
		importes = new Hashtable();
		info = new StringTokenizer((String)request.getAttribute("Info"),"\n");
		redivisa = (String)request.getAttribute("ReDivisa");

		// Se guarda la info recibida en los tres hashtables
		try{
			while(info.hasMoreTokens()){
				String auxLinea = info.nextToken();
				int pos = 0;
				while((pos=auxLinea.indexOf("||")) != -1) auxLinea = auxLinea.substring(0,pos+1) + " " + auxLinea.substring(pos+1);
				linea = new StringTokenizer(auxLinea,"|");
				clave = linea.nextToken();
				String clave2 = linea.nextToken();
				String imp = linea.nextToken();
				String div = linea.nextToken();
				if(div.equalsIgnoreCase("USD")){
					nombres.put(clave,"");
					clave += "|" + clave2;
					System.out.println("JSP-clave:["+clave+"]");
					agregaNum(clave, numeros);
					agregaImporte(clave, imp, importes);
				}
			}
		}catch(Exception e)
			{EIGlobal.mensajePorTrace("Excepci�n en coEstadisticasCon.jsp (try 1)\n" + e.getMessage(), EIGlobal.NivelLog.DEBUG);}

		// Se crea c�digo html y se guarda para primero calcular el orden en que se deben imprimir los meses
		codigo = new Hashtable();
		x = nombres.keys();
		valores = new long[9];
		while(x.hasMoreElements())
			{
			clave = (String)x.nextElement();
			valores[7]=0;
			for(n=1;n<=7;n++)
		{
		valores[n-1]=(moneda)?tomaImporte100(clave,numAtipo(n),importes):tomaValor(clave,numAtipo(n),numeros);
		valores[7]+=valores[n-1];
		}

		valores[8]= Long.parseLong(redivisa.trim());


			codigo.put(clave,"<TD align='center'>" + grafica(valores,meses[Integer.parseInt(clave)-1],moneda) + "</TD>");
			}
		nombres = null; numeros = null; importes = null; System.gc();

		// Se calcula el orden en que se imprimir�n los meses
		mesActual = (new java.util.GregorianCalendar()).get(java.util.Calendar.MONTH) - 1;
		existeAnterior = false;
		if(mesActual<1) mesActual += 12;
		mes1 = ((mesActual<10)?"0":"") + (mesActual++); if(mesActual>12) mesActual = 1;
		mes2 = ((mesActual<10)?"0":"") + (mesActual++); if(mesActual>12) mesActual = 1;
		mes3 = ((mesActual<10)?"0":"") + (mesActual++); if(mesActual>12) mesActual = 1;

		// Se imprimen los meses
		mesHTML = (String)codigo.get(mes1); if(mesHTML != null) out.println(mesHTML);
		mesHTML = (String)codigo.get(mes2); if(mesHTML != null) out.println(mesHTML);
		mesHTML = (String)codigo.get(mes3); if(mesHTML != null) out.println(mesHTML);
	%>
	</TR>
	</TABLE>
	<br>
	<div align='center'>
		<a href='javascript:scrImpresion();'><img src='/gifs/EnlaceMig/gbo25240.gif' border='0' alt='Imprimir'></A><a

		href ='confGrafica'><img src='/gifs/EnlaceMig/gbo25320.gif' border='0' alt='Regresar'></a>
	</div>

	</div>

</body>
</html>