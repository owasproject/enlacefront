<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Banca Virtual</title>
<META http-equiv=Content-Type content="text/html; charset=windows-1252"/>
<script type="text/javascript" SRC="/EnlaceMig/cuadroDialogo.js" type="text/javascript"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js" type="text/javascript"></script>
<script type="text/javascript" SRC="/EnlaceMig/list.js" type="text/javascript"></script>
<script type="text/javascript" SRC="/EnlaceMig/ocurre.js" type="text/javascript"></script>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js" type="text/javascript"></script>
<script type="text/javascript" src="/EnlaceMig/util.js"></script>
<script type="text/javascript" src="/EnlaceMig/pm_fp.js"></script>

<Script  type="text/javascript">

function validaLongitud() {
    inst = document.getElementById("Instrucciones");
    if(inst.value.length > 20) {
    	inst.value=inst.value.substring(0, 20);
    }
}

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_nbGroup(event, grpName) {
var i,img,nbArr,args=MM_nbGroup.arguments;
  if (event == "init" && args.length > 2) {
    if ((img = MM_findObj(args[2])) != null && !img.MM_init) {
      img.MM_init = true; img.MM_up = args[3]; img.MM_dn = img.src;
      if ((nbArr = document[grpName]) == null) nbArr = document[grpName] = new Array();
      nbArr[nbArr.length] = img;
      for (i=4; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
        if (!img.MM_up) img.MM_up = img.src;
        img.src = img.MM_dn = args[i+1];
        nbArr[nbArr.length] = img;
    } }
  } else if (event == "over") {
    document.MM_nbOver = nbArr = new Array();
    for (i=1; i < args.length-1; i+=3) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = (img.MM_dn && args[i+2]) ? args[i+2] : args[i+1];
      nbArr[nbArr.length] = img;
    }
  } else if (event == "out" ) {
    for (i=0; i < document.MM_nbOver.length; i++) {
      img = document.MM_nbOver[i]; img.src = (img.MM_dn) ? img.MM_dn : img.MM_up; }
  } else if (event == "down") {
    if ((nbArr = document[grpName]) != null)
      for (i=0; i < nbArr.length; i++) { img=nbArr[i]; img.src = img.MM_up; img.MM_dn = 0; }
    document[grpName] = nbArr = new Array();
    for (i=2; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = img.MM_dn = args[i+1];
      nbArr[nbArr.length] = img;
  } }
}

function msgCifrado(){
	try {
	var mensajeCifrado = document.Archivo_Ocurre.mensajeCifrado.value;
		if(mensajeCifrado != null && mensajeCifrado != "" && mensajeCifrado != "null" ) {
		var arreglo = mensajeCifrado.split("|");
           var msj = arreglo[1];
           var tipo = arreglo[0];
           cuadroDialogo( msj ,tipo);
		}
	} catch (e){};
}

<%
if(request.getAttribute("ArchivoErr")!=null) {
%>
<%= request.getAttribute("ArchivoErr") %>
<%}%>
--></Script>

<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Mon Apr 23 20:21:38 GMT-0600 2001-->
<script language="JavaScript" type="text/javascript">

<%= request.getAttribute("newMenu") %>

</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<META content="MSHTML 6.00.2600.0" name=GENERATOR></HEAD>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"  
onLoad="msgCifrado();MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');obtenDatosBrowser();" background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
	<td>
	<!-- MENU PRINCIPAL -->
		<%= request.getAttribute("MenuPrincipal") %>
	</TD>
  </TR>
</TABLE>

<%= request.getAttribute("Encabezado") %>

<!-- CONTENIDO INICIO -->

<FORM name="Ocurre" action="MDI_Orden_Pago_Ocurre?personalidad=<%= request.getAttribute("personalidad") %>&opcion=1" method="post">

<INPUT TYPE="HIDDEN" NAME="registros" VALUE=<%= request.getAttribute("registros") %>>
<INPUT TYPE="HIDDEN" NAME="NMAXOPER" VALUE=<%= request.getAttribute("NMAXOPER") %>>
<INPUT TYPE="HIDDEN" NAME="divisa" VALUE=<%= request.getAttribute("divisa") %>>
<INPUT TYPE="HIDDEN" NAME="personalidad" VALUE=<%= request.getAttribute("personalidad") %>>
<INPUT TYPE="HIDDEN" NAME="Arreglo_Cuentas_Cargo" VALUE="<%= request.getAttribute("Arreglo_Cuentas_Cargo") %>">
<INPUT TYPE="HIDDEN" NAME="Arreglo_Importes" VALUE="<%= request.getAttribute("Arreglo_Importes") %>">
<INPUT TYPE="HIDDEN" NAME="Arreglo_Instrucciones" VALUE="<%= request.getAttribute("Arreglo_Instrucciones") %>">
<INPUT TYPE="HIDDEN" NAME="Arreglo_Paterno" VALUE="<%= request.getAttribute("Arreglo_Paterno") %>">
<INPUT TYPE="HIDDEN" NAME="Arreglo_Materno" VALUE="<%= request.getAttribute("Arreglo_Materno") %>">
<INPUT TYPE="HIDDEN" NAME="Arreglo_Nombre"  VALUE="<%= request.getAttribute("Arreglo_Nombre") %>">
<INPUT TYPE="HIDDEN" NAME="Arreglo_Razones_Sociales" VALUE="<%=request.getAttribute("Arreglo_Razones_Sociales") %>">
<INPUT TYPE="HIDDEN" NAME="arregloestatus1" VALUE="<%= request.getAttribute("arregloestatus1") %>">
<INPUT TYPE="HIDDEN" NAME="Arreglo_RFCs" VALUE="<%= request.getAttribute("Arreglo_RFCs") %>">
<INPUT TYPE="HIDDEN" NAME="Arreglo_IVAs" VALUE="<%= request.getAttribute("Arreglo_IVAs") %>">

<TABLE cellSpacing=0 cellPadding=0 align=center border=0 width="613">
<TBODY>
  <TR>
    <TD width="611">
      <TABLE class=textabdatcla cellSpacing=3 cellPadding=2 align=center border=0>
		<TBODY>
         <TR>

     <TD class=tittabdat>Captura de Datos de la Transacci&oacute;n</TD>
        <TR>
          <TD class=textabdatcla width="100%">
            <TABLE class=textabdatcla cellSpacing=3 cellPadding=2 width="545">
              <TBODY>
              <TR>
                <TD class=tabmovtex width="216" colSpan=2>Cuenta de Cargo</TD>
                <TD class=tabmovtex width="263">Instrucciones</TD></TR>
              <TR>
                <TD class=tabmovtex colSpan=2 width="216"><INPUT class=tabmovtexbol
                  onfocus=blur(); maxLength=22 size=20
                  name=textEnlaceCuenta> <A
                  href="javascript:PresentarCuentas();"><IMG height=14
                  src="/gifs/EnlaceMig/gbo25420.gif" width=12
                  align=absMiddle border=0></A>
                  <p>&nbsp;Importe:<INPUT maxLength=13 size=13 name=Importe tabindex="1"></p>
                  <p><B>Datos del Beneficiario:</B><INPUT type=hidden name=Cuenta_Cargo> </p>
					<%= request.getAttribute("Paterno") %>
				 </TD>
                <TD class=tabmovtex rowSpan=3 width="263"><TEXTAREA name=Instrucciones id=Instrucciones rows=3 cols=25 tabindex="5" onkeypress="validaLongitud();" onchange="validaLongitud();"></TEXTAREA>
                  <p>Requiere edo. de cuenta fiscal?</p>
                  <P>&nbsp;&nbsp;S&iacute;<INPUT
                  onclick=asignar_valor(); type=radio value=1
                  name=CHRCF tabindex="6"> No<INPUT onclick=limpiar_cf();
                  type=radio CHECKED value=1 name=CHRCF tabindex="7"> </P>
                  <P>RFC Beneficiario:<INPUT onfocus=checaestatus(this);
                   maxLength=13 size=13 name=RFC tabindex="8">&nbsp; </P><P>Importe IVA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <INPUT onfocus=checaestatus(this);
                  maxLength=13 size=13 name=IVA tabindex="9" > </P>
                  <p>&nbsp;</p>
                </TD></TR>
              <TR>
                <TD class=tabmovtex colSpan=2 width="216">
 </TD>
              </TR>
              <TR>
                <TD class=tabmovtex width="216"> </TD>
                <TD class=tabmovtex width="4"> </TD></TR>
              </TBODY></TABLE></TD></TR></TBODY></TABLE>
      <p align="center">
	  <A href="javascript:validar();" border="0">
	  <IMG src="/gifs/EnlaceMig/gbo25290.gif" border=0 width="85" height="22" tabindex="10" NAME="Img_Agregar">
	  </A>
	  <A href="javascript:limpiar_datos();">
	  <IMG src="/gifs/EnlaceMig/gbo25250.gif" border=0 width="76" height="22" tabindex="11">
      </A>
	  </p>
	  <input type="hidden" id="datosBrowser" name="datosBrowser" value=""/>
</FORM>

<FORM name="Archivo_Ocurre" action="MDI_Orden_Pago_Ocurre?personalidad=<%= request.getAttribute("personalidad") %>&opcion=2" method="post" enctype="multipart/form-data">
<INPUT TYPE="hidden" name="mensajeCifrado" value="<%=request.getAttribute("mensajeCifrado")%>">
<TABLE cellSpacing=0 cellPadding=0 align=left border=0 width="608" >
<TBODY>
        <TR>
          <TD class=tittabdat width="608"><B>Archivo Externo</B></TD>
		</TR>
        <TR>
             <TD class=textabdatcla align=left width="100%">Importar Ordenes de Pago desde archivo: &nbsp;<input type="file" name="Arch" tabindex="12"></TD></TR></TBODY></TABLE>
			  <p>&nbsp;</p>
			  <p>&nbsp;</p>
              <p align="center">
			  <A href="javascript:Valida_Archivo();" border="0">
			  <IMG src="/gifs/EnlaceMig/gbo25300.gif" border=0 width="85" height="22" tabindex="13">
			  </A>
	          <A href="javascript:limpiar_datos_Archivo();">
	          <IMG src="/gifs/EnlaceMig/gbo25250.gif" border=0 width="76" height="22" tabindex="14">
              </A>
			  </p>

<INPUT TYPE="HIDDEN" NAME="registros" VALUE="<%= request.getAttribute("registros")%>" />
<INPUT TYPE="HIDDEN" NAME="Arreglo_Cuentas_Cargo" VALUE="<%= request.getAttribute("Arreglo_Cuentas_Cargo") %>"/>
<INPUT TYPE="HIDDEN" NAME="Arreglo_Importes" VALUE="<%= request.getAttribute("Arreglo_Importes") %>">
<INPUT TYPE="HIDDEN" NAME="Arreglo_Instrucciones" VALUE="<%= request.getAttribute("Arreglo_Instrucciones") %>"/>
<INPUT TYPE="HIDDEN" NAME="Arreglo_Paterno" VALUE="<%= request.getAttribute("Arreglo_Paterno") %>"/>
<INPUT TYPE="HIDDEN" NAME="Arreglo_Materno" VALUE="<%= request.getAttribute("Arreglo_Materno") %>"/>
<INPUT TYPE="HIDDEN" NAME="Arreglo_Nombre"  VALUE="<%= request.getAttribute("Arreglo_Nombre") %>"/>
<INPUT TYPE="HIDDEN" NAME="Arreglo_Razones_Sociales" VALUE="<%=request.getAttribute("Arreglo_Razones_Sociales") %>"/>
<INPUT TYPE="HIDDEN" NAME="arregloestatus1" VALUE="<%= request.getAttribute("arregloestatus1") %>"/>
<INPUT TYPE="HIDDEN" NAME="Arreglo_RFCs" VALUE="<%= request.getAttribute("Arreglo_RFCs") %>"/>
<INPUT TYPE="HIDDEN" NAME="Arreglo_IVAs" VALUE="<%= request.getAttribute("Arreglo_IVAs") %>"/>
<input type="hidden" id="datosBrowser" name="datosBrowser" value=""/>

</FORM>

<FORM name="Tabla_Ocurre" action="MDI_Orden_Pago_Ocurre?personalidad=<%= request.getAttribute("personalidad") %>&opcion=3" method="post">

<%= request.getAttribute("Mensaje_Salida") %>

<INPUT TYPE="HIDDEN" NAME="registros" VALUE="<%= request.getAttribute("registros") %>" />
<INPUT TYPE="HIDDEN" NAME="Arreglo_Cuentas_Cargo" VALUE="<%= request.getAttribute("Arreglo_Cuentas_Cargo") %>"/>
<INPUT TYPE="HIDDEN" NAME="Arreglo_Importes" VALUE="<%= request.getAttribute("Arreglo_Importes") %>"/>
<INPUT TYPE="HIDDEN" NAME="Arreglo_Instrucciones" VALUE="<%= request.getAttribute("Arreglo_Instrucciones") %>"/>
<INPUT TYPE="HIDDEN" NAME="Arreglo_Paterno" VALUE="<%= request.getAttribute("Arreglo_Paterno") %>"/>
<INPUT TYPE="HIDDEN" NAME="Arreglo_Materno" VALUE="<%= request.getAttribute("Arreglo_Materno") %>"/>
<INPUT TYPE="HIDDEN" NAME="Arreglo_Nombre"  VALUE="<%= request.getAttribute("Arreglo_Nombre") %>"/>
<INPUT TYPE="HIDDEN" NAME="Arreglo_Razones_Sociales" VALUE="<%=request.getAttribute("Arreglo_Razones_Sociales") %>"/>
<INPUT TYPE="HIDDEN" NAME="arregloestatus1" VALUE="<%= request.getAttribute("arregloestatus1") %>"/>
<INPUT TYPE="HIDDEN" NAME="Arreglo_RFCs" VALUE="<%= request.getAttribute("Arreglo_RFCs") %>"/>
<INPUT TYPE="HIDDEN" NAME="Arreglo_IVAs" VALUE="<%= request.getAttribute("Arreglo_IVAs") %>"/>

</FORM></TR></TBODY></TABLE></BODY></HTML>