<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.*" %>
<%@ page import="mx.altec.enlace.bo.BaseResource" %>


<html>
<head>
	<title>TESOFE - Consulta de Archivos y Registros</TITLE>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript1.2" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript">

 <%
 String FechaHoy="";
 FechaHoy=(String)request.getAttribute("FechaHoy");
 if(FechaHoy==null)
    FechaHoy="";
 %>

  //Indice Calendario
  var Indice=0;

  //Arreglos de fechas
  <%
    if(request.getAttribute("VarFechaHoy")!= null)
      out.print(request.getAttribute("VarFechaHoy"));
  %>
  //Dias inhabiles
  diasInhabiles='<% if(request.getAttribute("DiasInhabiles")!= null) out.print(request.getAttribute("DiasInhabiles")); %>';

/**
 @param s cadena
 @return boolean true, false
 @descripcion verifica se una cadena esta vacia
*/
function isEmpty(s)
{
  return ((s == null) || (trimString(s).length == 0));
}

/**
 @param str cadena
 @return str cadena sin espacios
 @descripcion elimina espacios a la derecha y a la izquierda de una cadena
*/
function trimString (str)
{
  str = this != window? this : str;
  return str.replace(/^\s+/g, '').replace(/\s+$/g, '');  //Quita espacios
}

/**
 @param num cadena numerica
 @return boolean true, false
 @descripcion verifica una expresion para determinar si es tipo entera sin signo
  utilizando expresiones regulares
*/
function isInteger(num)
{
  var TemplateI = /^\d+$/;	 //Formato de numero entero sin signo
  return TemplateI.test(num); //Compara "num" con el formato "Template"
}

/**
 @param num cadena numerica
 @return boolean true, false
 @descripcion evalua una cadena numerica y determina si cumple con el formato
  float sin signo
  d(n)
  d(n).d(n)
  .d(n)
  d(n).
  utilizando expresiones regulares.
*/
function isFloat(num)
{
  var TemplateF=/^\d*\.{1}\d+$|\d+\.{0,1}\d*$/;  //Formato de numero flotante sin signo
  return TemplateF.test(num);                   //Compara "num" con el formato "Template"
}

/**
 @param rfc cadena tipo rfc
 @return boolean true, false
 @descripcion valida una cadena y determina si cumple con el formato rfc.
  evalua:
  formato rfc personas fisicas sin homoclave
  formato rfc personas fisicas con homoclave
  formato rfc morales fisicas sin homoclave
  formato rfc morales fisicas con homoclave
  valida en fechas, los dias y meses validos
  valida para febrero unicamente dia hasta dia 29
*/
function verificaRFC(rfc)
{
  var fecha=6,mes=1,dia=1;
  var TemplateRFC=/^[a-z,�]{3,4}[0-9]{6}(\t*|[a-z0-9�]{3})$/i;
  if(TemplateRFC.test(rfc))
   {
     fecha=(rfc.length==9 || rfc.length==12)?5:6;
	 mes=rfc.substring(fecha,fecha+2);
     dia=rfc.substring(fecha+2,fecha+4);
     if( (dia >0 && dia<=31) && (mes >0 && mes<=12) )
	    if( !(eval(mes)==2 && eval(dia) >=30) )
           return true;
   }
  return false;
}

function verificaFechas(ind_1,ind_2)
 {
	 var forma=document.TesConsulta;
	 
	 	var fechaIni;
	   	var fechaFin;
	 
	   	if(ind_1 == 0){
	   		fechaIni = forma.Fecha1_01.value;
	   		fechaFin = forma.Fecha1_02.value;
	   	}else{
	   		fechaIni = forma.Fecha2_01.value;
	   		fechaFin = forma.Fecha2_02.value;
	   	}
	   	
	  	dia1[ind_1] = fechaIni.substring(0,2);
	   	mes1[ind_1] = fechaIni.substring(3,5);
	   	anio1[ind_1] = fechaIni.substring(6,10);
	   
	   	dia1[ind_2] = fechaFin.substring(0,2);
	   	mes1[ind_2] = fechaFin.substring(3,5);
	   	anio1[ind_2] = fechaFin.substring(6,10);

	 if(anio1[ind_1]==anio1[ind_2]){
	 	if(mes1[ind_1]>mes1[ind_2])
      	{
			cuadroDialogo("Error: La fecha Final no debe ser menor a la inicial.",3);
        	return false;
      	}
     	if(mes1[ind_1]==mes1[ind_2])
      		if(dia1[ind_1]>dia1[ind_2])
       		{
		 		cuadroDialogo("Error: La fecha Inicial debe ser menor a la final.",3);
         		return false;
       		}
	 }else if(anio1[ind_1]>anio1[ind_2]){
		 cuadroDialogo("Error: La fecha Final no debe ser menor a la inicial.",3);
         return false;
	 }
     return true;
 }

function verificaImporte()
 {
	var forma=document.TesConsulta;

	if(!isEmpty(forma.importe.value))
	  if(!isFloat(forma.importe.value))
		{
		  cuadroDialogo("El importe no es v&aacute;lido.",3);
		  return false;
		}
	return true;
 }

function verificaContrato()
 {
	var forma=document.TesConsulta;

	if(!isEmpty(forma.Contrato.value))
	  if(!isInteger(forma.Contrato.value))
		{
		  cuadroDialogo("El contrato no es v&aacute;lido.",3);
		  return false;
		}
	return true;
 }

function verificaSecuencia(ind)
 {
	var forma=document.TesConsulta;
	var msg="";

	if(ind==0)
	 {
	   secuencia=forma.secuencia.value;
	   msg="secuencia";
	 }
	else
	 {
	   secuencia=forma.secuenciaRegistro.value;
	   msg="referencia";
	 }

	if(!isEmpty(secuencia))
	  if(!isInteger(secuencia))
		{
		  cuadroDialogo("El n&uacute;mero de "+msg+" no es v&aacute;lido.",3);
		  return false;
		}
	return true;
 }

function js_consultar()
 {
	var forma=document.TesConsulta;
	<%
		boolean facConArc=false;
	    boolean facConReg=false;

		BaseResource sess =(BaseResource)request.getSession().getAttribute("session");
		facConArc=sess.getFacultad("TESOCONARC");
		facConReg=sess.getFacultad("TESOCONREG");
	%>
	var FAC_ARC=<%=facConArc%>;
	var FAC_REG=<%=facConReg%>;

	if(forma.tipoConsulta[0].checked) /* Consulta de archivos */
	 {
		if(!FAC_ARC)
		 {
		   cuadroDialogo("No tiene facultad para realizar la consulta de archivos",3);
		   return;
		 }

		if(isEmpty(forma.Fecha1_01.value) || isEmpty(forma.Fecha1_02.value))
		 {
			cuadroDialogo("Debe especificar un rango de fechas",3);
			return;
		 }
		if(!verificaImporte() || !verificaSecuencia(0) )
		  return;
		if(!verificaFechas(0,1))
		  return;
	 }
	else /* Consulta de registros */
	 {
		if(!FAC_REG)
		 {
			cuadroDialogo("No tiene facultad para realizar la consulta de registros",3);
			return;
		 }

		if(forma.tipoFechaConsulta[1].checked)
		 {
			if(isEmpty(forma.Fecha2_01.value) || isEmpty(forma.Fecha2_02.value))
			 {
				cuadroDialogo("Debe especificar un rango de fechas",3);
				return;
			 }
			if(!verificaFechas(2,3))
			  return;
		 }

		if(!isEmpty(forma.RFC.value))
		  if(!verificaRFC(forma.RFC.value))
		    {
			  cuadroDialogo("El formato del RFC no es v&aacute;lido",3);
			  return;
			}
		if(!verificaSecuencia(1) )
		  return;
	 }

   forma.action="MTE_ConsultaCancelacion";
   if(forma.RFC.value!="")
	 forma.RFC.value=forma.RFC.value.toUpperCase();
   forma.submit();
   //alert("OK");
 }

function js_frmClean()
 {
   document.TesConsulta.reset();
 }

function Actualiza()
 {
   if(Indice==0)
     document.TesConsulta.Fecha1_01.value=Fecha[Indice];
   if(Indice==1)
     document.TesConsulta.Fecha1_02.value=Fecha[Indice];
   if(Indice==2)
     document.TesConsulta.Fecha2_01.value=Fecha[Indice];
   if(Indice==3)
     document.TesConsulta.Fecha2_02.value=Fecha[Indice];
 }

function js_calendario(ind)
 {
    var m=new Date()

	Indice=ind;
    n=m.getMonth();
    msg=window.open("/EnlaceMig/MTE_CalendarConsulta.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
    msg.focus();
 }


/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/
<%
	if(	session.getAttribute("strScript") !=null)
		out.println( session.getAttribute("strScript"));
	else
	 if(request.getAttribute("strScript") !=null)
		out.println( request.getAttribute("strScript") );
	 else
	  out.println("");
%>
<%
	if(	session.getAttribute("newMenu") !=null)
		out.println( session.getAttribute("newMenu"));
	else
	  if(request.getAttribute("newMenu")!=null)
		out.println(request.getAttribute("newMenu"));
	 else
	   out.println("");
%>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="//gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%
	if(	session.getAttribute("MenuPrincipal") !=null)
		out.println( session.getAttribute("MenuPrincipal"));
	else
	 if(request.getAttribute("MenuPrincipal")!=null)
		out.println(request.getAttribute("MenuPrincipal"));
	 else
	  out.println("");
	%>
   </td>
  </tr>
</table>

<%
	if(	session.getAttribute("Encabezado") !=null)
		out.println( session.getAttribute("Encabezado"));
	else
	 if(request.getAttribute("Encabezado")!=null)
		out.println(request.getAttribute("Encabezado"));
	 else
	  out.println("");
%>


 <FORM   NAME="TesConsulta" METHOD="Post" ACTION="MTE_ConsultaCancelacion">

   <table width="760" border="0" cellspacing="0" cellpadding="0">

    <tr>
	 <td align="center">
	    <table border="0" cellspacing="2" cellpadding="3">
		 <tr>
		   <td class="tittabdat" colspan="2" nowrap><input type=radio name=tipoConsulta value=A checked> Consulta por archivos enviados</td>
		 </tr>
		 <tr align="center">
		   <td class="textabdatcla" valign="top" colspan="2">
			<table width="450" border="0" cellspacing="0" cellpadding="0" align=center>
			 <tr valign="top">
			  <td width="220" align="left">

               <table width="220" border="0" cellspacing="0" cellpadding="5">
				<tr>
                 <td align="right" class="tabmovtex">Fecha inicio:<br></td>
				 <td class="tabmovtex" nowrap valign="middle"><input type="text" name="Fecha1_01" size="12" class="tabmovtex" OnFocus = "blur();" value="<%=FechaHoy%>"><a href="javascript:js_calendario(0);" ><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></a></td>
				</tr>
				<tr>
				 <td align="right" class="tabmovtex">Fecha fin:</td>
				 <td class="tabmovtex" nowrap valign="middle"><input type="text" name="Fecha1_02" size="12" class="tabmovtex" OnFocus = "blur();" value="<%=FechaHoy%>"><a href="javascript:js_calendario(1);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></a></td>
				</tr>
				<tr>
				 <td align="right" class="tabmovtex">Importe:</td>
				 <td class="tabmovtex" nowrap><input type="text" name="importe" size="15" maxlength=15 class="tabmovtex"></td>
				</tr>
				<tr>
				 <td align="right" class="tabmovtex">Secuencia:</td>
				 <td class="tabmovtex" nowrap><input type="text" name="secuencia" size="15" maxlength=12 class="tabmovtex"></td>
				</tr>
			   </table>

              </td>
			  <td width="200" align="left">

			   <table width="200" border="0" cellspacing="0" cellpadding="3">
			    <tr>
				 <td align="right" class="tabmovtex" nowrap>Estatus:</td>
				 <td class="tabmovtex" valign="middle" align="center"><input type="checkbox" name="chkEstatusArc" value="R"></td>
				 <td class="tabmovtex" nowrap>Recibidos</td>
				</tr>
				<tr>
				 <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
				 <td class="tabmovtex" align="center" valign="middle"><input type="checkbox" name="chkEstatusArc" value="C"></td>
				 <td class="tabmovtex" nowrap>Procesados </td>
				</tr>
				<!--
				<tr>
				 <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
				 <td class="tabmovtex" align="center" valign="middle"><input type="checkbox" name="chkEstatusArc" value="E"></td>
				 <td class="tabmovtex" nowrap>En proceso</td>
				</tr>
			    <tr>
				 <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
				 <td class="tabmovtex" align="center" valign="middle"><input type="checkbox" name="chkEstatusArc" value="P"></td>
				 <td class="tabmovtex" nowrap>Procesados</td>
				</tr>
				//-->
			    <tr>
				 <td align="right" class="tabmovtex" nowrap colspan=3>&nbsp;</td>
				</tr>
			   </table>

			  </td>
			 </tr>
			</table>
		   </td>
          </tr>
        </table>

	 </td>
    </tr>

	<!-- Consulta por Registros //-->

	<tr>
	 <td align="center">
	    <table border="0" cellspacing="2" cellpadding="3">
		 <tr>
		   <td class="tittabdat" colspan="2" nowrap><input type=radio name=tipoConsulta value=R> Consulta por registros</td>
		 </tr>
		 <tr align="center">
		   <td class="textabdatcla" valign="top" colspan="2">
			<table width="450" border="0" cellspacing="0" cellpadding="0" align=center>
			 <tr valign="top">
			  <td align="left">

               <table border="0" cellspacing="0" cellpadding="5">
			    <tr>
				 <td class="tabmovtex" colspan=2>Tipo de Consulta:</td>
			    </tr>
			    <tr>
				 <td class="tabmovtex" colspan=2><input type="radio" name="tipoFechaConsulta" value="D" checked>Detallada</td>
			    </tr>

				<tr>
				 <td width="95%" >
				   <table border=0 width="92%" align=right>
					<tr>
					 <td align="right" class="tabmovtex">RFC:</td>
					 <td class="tabmovtex" nowrap><input type="text" name="RFC" size="20" maxlength=15 class="tabmovtex"></td>
					</tr>
					<tr>
					 <td align="right" class="tabmovtex">Referencia:</td>
					 <td class="tabmovtex" nowrap><input type="text" name="secuenciaRegistro" size="20" maxlength=12 class="tabmovtex"></td>
					</tr>
					</table>
				 </td>
				</tr>

				<tr>
				 <td colspan=2><hr width="90%"></td>
				</tr>

				<tr>
				  <td class="tabmovtex" colspan=2><input type="radio" name="tipoFechaConsulta" value="G">General</td>
				</tr>

				<tr>
				 <td width="95%" valign=top>
				   <table border=0 width="92%" align=right>
					<tr>
					  <td class="tabmovtex" colspan=2> Rango de consulta: </td>
					</tr>
					<tr>
					  <td align="right" class="tabmovtex">Fecha inicio:<br></td>
					  <td class="tabmovtex" nowrap valign="middle"><input type="text" name="Fecha2_01" size="12" class="tabmovtex" OnFocus = "blur();" value="<%=FechaHoy%>"><a href="javascript:js_calendario(2);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></a></td>
					</tr>
					<tr>
					 <td align="right" class="tabmovtex">Fecha fin:</td>
					 <td class="tabmovtex" nowrap valign="middle"><input type="text" name="Fecha2_02" size="12" class="tabmovtex" OnFocus = "blur();" value="<%=FechaHoy%>"><a href="javascript:js_calendario(3);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></a></td>
					</tr>
				   </table>
				 </td>

				 <td>
				  <table width="200" border="0" cellspacing="0" cellpadding="3">
					<tr>
					 <td align="right" class="tabmovtex" nowrap>Estatus:</td>
					 <td class="tabmovtex" valign="middle" align="center"><input type="radio" name="chkEstatusReg" value="R" checked></td>
					 <td class="tabmovtex" nowrap>Vigentes</td>
					</tr>
					<tr>
					 <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
					 <td class="tabmovtex" align="center" valign="middle"><input type="radio" name="chkEstatusReg" value="C"></td>
					 <td class="tabmovtex" nowrap>Cancelados </td>
					</tr>
					<tr>
					 <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
					 <td class="tabmovtex" align="center" valign="middle"><input type="radio" name="chkEstatusReg" value="D"></td>
					 <td class="tabmovtex" nowrap>Vencidos</td>
					</tr>
					<tr>
					 <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
					 <td class="tabmovtex" align="center" valign="middle"><input type="radio" name="chkEstatusReg" value="P"></td>
					 <td class="tabmovtex" nowrap>Pagados</td>
					</tr>
				   </table>
				  </td>

				</tr>
			   </table>

			  </td>
			 </tr>
			</table>
		   </td>
          </tr>
        </table>

        <br>
        <table width="166" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="22">
         <tr>
          <td align="right" valign="top" height="22" width="90"><a href="javascript:js_consultar();"><img border="0" name=Enviar value=Enviar src="/gifs/EnlaceMig/gbo25220.gif" width="90" height="22" alt="Consultar"></td>
		  <td align="left" valign="middle" height="22" width="76"><a href="javascript:js_frmClean();"><img name=limpiar value=Limpiar border="0" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar"></a></td>
		 </tr>
        </table>

	 </td>
    </tr>

  </table>

  <input type=hidden name=ventana value=1>

 </FORM>
</body>
</html>