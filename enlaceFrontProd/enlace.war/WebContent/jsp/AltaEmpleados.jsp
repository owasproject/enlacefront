<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
	<head>
		<title>Alta de empleado individual</title>
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="-1" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="-1" />
		<script type="text/javascript" src="/EnlaceMig/js/cuadroDialogo.js"></script>
		<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
		<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
		<script type ="text/javascript" src="/EnlaceMig/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="/EnlaceMig/altaEmpleados.js"></script>
		<script type="text/javascript">
    		${newMenu}
		</script>
		<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />
	</head>
	<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif',
							 '/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif',
							 '/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif',
							 '/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');cargaCombos();">
		<table style="border:0;" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
			<tr>
				<td width="">${MenuPrincipal}</td>
			</tr>
		</table>
		${Encabezado}
<form name="AltaIndividualForm" method="POST" action="AltaNomEmplInd">
<input type="hidden" name="hddEmpleados" id="hddEmpleados" value="<%=request.getAttribute("numEmpleados")%>"/>
<table width="760" style="border:0;" cellpadding="0" cellspacing="0">
	<tr>
		<td style="text-align: center;"><br/>
			<table width="760" style="border:0; background-color: #EBEBEB;" cellpadding="2" cellspacing="2">
				<tr>
					<td class="tittabdat" colspan="3">Alta empleado</td>
				</tr>
				<tr>
					<td class="textabdatcla" colspan="1" >* N�mero de empleado</td>
					<td class="textabdatcla" colspan="1" >N�mero de departamento</td>
					<td class="textabdatcla" colspan="5" >* Nombre del empleado</td>
				</tr>
				<tr>
					<td class="textabdatcla" colspan="1">
						<input name="numeroEmpleado" value="${empleado.numeroEmpleado}" maxlength="7" onBlur="toUpperCase(this); validaCampo(this, 'N�mero de empleado', 'ESPECIALES');" size="10" type="text"/>
					</td>
					<td class="textabdatcla" colspan="1">
						<input name="numeroDepto" value="${empleado.numeroDepto}"  onBlur="validaCampo(this, 'N�mero de departamento', 'SNUM');" maxlength="6" size="10" type="text"/>
					</td>
					<td class="textabdatcla" colspan="5">
						<input name="nombreEmpleado" value="${empleado.nombreEmpleado}" onBlur="toUpperCase(this); validaCampo(this, 'Nombre de empleado', 'ESPECIALES');" maxlength="30" size="30" type="text"/>
					</td>
				</tr>
				<tr>
					<td class="textabdatcla">* Apellido paterno</td>
					<td class="textabdatcla">* Apellido materno</td>
					<td class="textabdatcla">
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td class="textabdatcla" colspan="1" width="15%">* RFC</td>
								<td class="textabdatcla" colspan="1" width="4%">Homoclave</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="textabdatcla">
						<input name="apellidoPaterno" value="${empleado.apellidoPaterno}" onBlur="toUpperCase(this); validaCampo(this, 'Apellido Paterno', 'ESPECIALES');" maxlength="30" size="22" type="text"/>
					</td>
					<td class="textabdatcla">
						<input name="apellidoMaterno" value="${empleado.apellidoMaterno}" onBlur="toUpperCase(this); validaCampo(this, 'Apellido Materno', 'ESPECIALES');" maxlength="20" size="22" type="text"/>
					</td>
					<td class="textabdatcla">
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td class="textabdatcla">
									<input name="rfc" value="${empleado.rfc}" onBlur="toUpperCase(this); validaCampo(this, 'RFC', 'RFC');" maxlength="10" size="12" type="text"/>
								</td>
								<td class="textabdatcla">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input name="homoclave" value="${empleado.homoclave}" onBlur="toUpperCase(this); validaCampo(this, 'Homoclave', 'HOMOCLAVE');" maxlength="3" size="5" type="text"/>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="textabdatcla">* Sexo</td>
					<td class="textabdatcla" colspan="1">* Nacionalidad</td>
					<td class="textabdatcla">* Estado Civil</td>
				</tr>
				<tr>
					<td class="textabdatcla">
						<input id="sexo" value="${empleado.genero}" type="hidden"/>
						<select name="genero">
							<option value="M" selected="selected">Masculino</option>
							<option value="F">Femenino</option>
						</select>
					</td>
					<td class="textabdatcla">
						<input id="nacionalidad" value="${empleado.cveNacionalidad}" type="hidden"/>
						<select name="cveNacionalidad">
							<option value="MEXI" selected="selected">Mexicana</option>
							<option value="USA">Estadounidense</option>
							<option value="CANA">Canadiense</option>
							<option value="ESPA">Espa�ola</option>
							<option value="FRA">Franc�s</option>
						</select>
					</td>
					<td class="textabdatcla">
						<input id="estadoCivil" value="${empleado.edoCivil}" type="hidden"/>
						<select name="edoCivil">
							<option value="S" selected="selected">Soltero</option>
							<option value="C">Casado</option>
							<option value="V">Viudo</option>
							<option value="U">Union Libre</option>
							<option value="D">Divorciado</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="textabdatcla" colspan="1" style="height: 24px;">Nombre corto</td>
					<td class="textabdatcla" colspan="1" style="height: 24px;">* Fecha de ingreso a la empresa <br/>(dd/mm/aaaa)</td>
					<td class="textabdatcla" colspan="1" style="height: 24px;">Fecha de residencia empleado<br/>(dd/mm/aaaa)</td>
				</tr>
				<tr>
					<td class="textabdatcla">
						<input name="nombreCorto" value="${empleado.nombreCorto}" onBlur="toUpperCase(this); validaCampo(this, 'nombreCorto', 'ESPECIALES');" maxlength="26" size="20" onblur="" type="text"/>
					</td>
					<td class="textabdatcla" style="height: 24px;">
						<input name="fechaIngreso" value="${empleado.fechaIngreso}" onBlur="validaCampo(this, 'Fecha de ingreso', 'FECHA');" maxlength="10" size="15" onblur="" type="text"/>
					</td>
					<td class="textabdatcla" style="height: 24px;">
						<input name="fechaRecidencia" value="${empleado.fechaRecidencia}" onBlur="validaCampo(this, 'Fecha de residencia', 'FECHA');" maxlength="10" size="15" onchange="" type="text"/>
					</td>
				</tr>
				<tr>
					<td class="textabdatcla" colspan="1">* Domicilio empleado (calle y n�mero)</td>
					<td class="textabdatcla" colspan="1">* C�digo postal</td>
					<td class="textabdatcla" colspan="1">* Colonia </td>
				</tr>
				<tr>
					<td class="textabdatcla">
						<input name="domicilio" value="${empleado.domicilio}" onBlur="toUpperCase(this); validaCampo(this, 'Domicilio', 'ESPECIALES');" maxlength="60" size="20" type="text"/>
					</td>
					<td class="textabdatcla">
						<input name="cp" value="${empleado.cp}" onBlur="validaCampo(this, 'C�digo postal', 'SNUM'); consultaCodigoPostal(this,1);" maxlength="5" size="10" type="text"/>
					</td>
					<td class="textabdatcla" style="height: 24px;" >
						<select id="colonia" name="colonia" style="width: 190px; max-width: 190px;">
							<option value="">Seleccione una colonia</option>
							<c:if test="${!empty empleado.colonia}">
								<option value="${empleado.colonia}" selected="selected">${empleado.colonia}</option>
							</c:if>
						</select>
					</td>
				</tr>
				<tr>
					<td class="textabdatcla" width="54%" colspan="1">Delegaci�n o municipo</td>
					<td class="textabdatcla" width="34%" colspan="1">Ciudad o poblaci�n</td>
				</tr>
				<tr>
					<td class="textabdatcla" width="70%">
						<input name="delegacion" value="${empleado.delegacion}" onBlur="toUpperCase(this); validaCampo(this, 'Delegaci�n', 'ESPECIALES');" maxlength="50" size="30" type="text"/>
					</td>
					<td class="textabdatcla" width="70%">
						<input name="ciudad" value="${empleado.ciudad}" onBlur="toUpperCase(this); validaCampo(this, 'Ciudad', 'ESPECIALES');" maxlength="20" size="30" type="text"/>
					</td>
				</tr>
				<tr>
					<td class="textabdatcla" colspan="1">Clave pa�s empleado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Estatus De Casa
					</td>
					<td class="textabdatcla" colspan="1">Lada / Tel de casa</td>
					<td class="textabdatcla" colspan="1">* Sucursal tutora&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Clave de env�o</td>
				</tr>
				<tr>
					<td class="textabdatcla">
						<input id="paisEmpl" value="${empleado.paisEmpleado}" type="hidden"/>
						<select name="paisEmpleado">
							<option value="MEXI" selected="selected">M�xico</option>
							<option value="USA">Estados Unidos</option>
							<option value="CANA">Canada</option>
							<option value="ESPA">Espa�a</option>
							<option value="FRA">Francia</option>
						</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input id="statCasa" value="${empleado.estatusCasa}" type="hidden"/>
						<select name="estatusCasa">
							<option value="P" selected="selected">Propia</option>
							<option value="R">Rentada</option>
						</select>
					</td>
					<td class="textabdatcla" colspan="1">
						<input name="lada" value="${empleado.lada}" onBlur="validaCampo(this, 'Lada', 'LADA');" maxlength="3" size="5" type="text"/>
						<input name="telefono" value="${empleado.telefono}" onBlur="validaCampo(this, 'Tel�fono', 'TELEFONO');" maxlength="8" size="10" type="text"/>
					</td>
					<td class="textabdatcla">
						<input name="sucursal" value="${empleado.sucursal}" onBlur="validaCampo(this, 'Sucursal', 'MAXLENSNUM');" maxlength="4" size="10" type="text"/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input id="cveEnvio" value="${empleado.claveEnvio}" type="hidden"/>
						<select name="claveEnvio">
							<option value="1" selected="selected">Casa</option>
							<option value="2">Oficina</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="textabdatcla">* Domicilio de oficina (calle y n�mero)</td>
					<td class="textabdatcla">* C�digo postal de oficina</td>
					<td class="textabdatcla">* Colonia de oficina</td>
				</tr>
				<tr>
					<td class="textabdatcla">
						<input name="domicilioOficina" value="${empleado.domicilioOficina}" onBlur="toUpperCase(this); validaCampo(this, 'Domicilio Oficina', 'ESPECIALES');" maxlength="60" size="20" type="text"/>
					</td>
					<td class="textabdatcla">
						<input name="cpOficina" value="${empleado.cpOficina}" onBlur="validaCampo(this, 'C�digo postal de Oficina', 'SNUM'); consultaCodigoPostal(this,2);" maxlength="5" size="10" type="text"/>
					</td>
					<td class="textabdatcla">
						<select id="coloniaOficina" name="coloniaOficina" style="width: 190px; max-width: 190px;">
							<option value="">Seleccione una colonia</option>
							<c:if test="${!empty empleado.coloniaOficina}">
								<option value="${empleado.coloniaOficina}" selected="selected">${empleado.coloniaOficina}</option>
							</c:if>
						</select>
					</td>
				</tr>
				<tr>
					<td class="textabdatcla">Delegaci�n o municipo de oficina</td>
					<td class="textabdatcla">Ciudad o poblaci�n de la oficina</td>
				</tr>
				<tr>
					<td class="textabdatcla">
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td class="textabdatcla">
									<input name="delegacionOficina" value="${empleado.delegacionOficina}" onBlur="toUpperCase(this); validaCampo(this, 'Delegaci�n Oficina', 'ESPECIALES');" maxlength="30" size="40" type="text"/>
								</td>
							</tr>
						</table>
					</td>
					<td class="textabdatcla">
						<input name="ciudadOficina" value="${empleado.ciudadOficina}" onBlur="toUpperCase(this); validaCampo(this, 'Ciudad Oficina', 'ESPECIALES');" maxlength="20" size="40" type="text"/>
					</td>
				</tr>
				<tr>
					<td class="textabdatcla" width="54%">* Pa�s oficina</td>
					<td class="textabdatcla" colspan="1">* Lada / Tel. oficina</td>
					<td class="textabdatcla" colspan="3">Ext. oficina</td>
				</tr>
				<tr>
					<td class="textabdatcla">
						<input id="paisOf" value="${empleado.paisOficina}" type="hidden"/>
						<select name="paisOficina">
							<option value="MEXI" selected="selected">M�xico</option>
							<option value="USA">Estados Unidos</option>
							<option value="CANA">Canada</option>
							<option value="ESPA">Espa�a</option>
							<option value="FRA">Francia</option>
						</select>
					</td>
					<td class="textabdatcla" width="20%">
						<input name="ladaOficina" value="${empleado.ladaOficina}" onBlur="validaCampo(this, 'Lada oficina', 'LADA');" maxlength="3" size="5" type="text"/>
						<input name="telOficina" value="${empleado.telOficina}" onBlur="validaCampo(this, 'Tel�fono oficina', 'TELEFONO');" maxlength="8" size="11" type="text"/>
					</td>
					<td class="textabdatcla" colspan="3">
						<input name="extOficina" value="${empleado.extOficina}" onBlur="validaCampo(this, 'Extencion oficina', 'ESPECIALES');"  maxlength="6" size="7" type="text"/>
					</td>
				</tr>
				<tr>
					<td class="textabdatcla" colspan="1">* Ingreso mensual bruto</td>
					<td class="textabdatcla" colspan="1">N�mero de cuenta</td>
					<td class="textabdatcla" colspan="1">N�mero de tarjeta</td>
				</tr>
				<tr>
					<td class="textabdatcla" colspan="1">
						<input name="ingresoMensual" value="${empleado.ingresoMensual}" onBlur="validaCampo(this, 'Ingreso mensual', 'NUM'); formatoImporte(this, 2, '.', ',');" maxlength="19" size="25" type="text"/>
					</td>
					<td class="textabdatcla" colspan="1">
						<input name="numeroCuenta" value="${empleado.numeroCuenta}" onBlur="toUpperCase(this); validaCampo(this, 'N�mero de cuenta', 'MAXLENSNUM');" maxlength="11" size="25" type="text"/>
					</td>
					<td class="textabdatcla" colspan="1">
						<input name="numeroTarjeta" value="${empleado.numeroTarjeta}" onBlur="toUpperCase(this); validaCampo(this, 'N�mero de tarjeta', 'MAXLENSNUM');" maxlength="16" size="25" type="text"/>
					</td>
				</tr>
				<tr>
					<td class="textabdatcla">
						<br/>
						<b>* Campos requeridos.</b>
					</td>
					<td class="textabdatcla" colspan="2">
						<br/>
						* No se aceptan acentos ni caracteres especiales.
					</td>
				</tr>
			</table>
			<br/>
			<div style="text-align: center;">
				<table id="enviando" style="border:0; margin: auto;" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<a href="javascript:confirmaCancelacion();">
								<img style="border:0;" src="/gifs/EnlaceMig/gbo25190.gif" width="85" height="22" alt="Cancelar"/>
							</a>
						</td>
						<td>
							<a href="javascript:guardar(6);">
								<img style="border:0;" src="/gifs/EnlaceMig/guardarYSalir.png" width="130" height="21" alt="Guardar y salir"/>
							</a>
						</td>
						<td>
							<a href="javascript:guardar(2);">
								<img style="border:0;"  src="/gifs/EnlaceMig/gbo25290.gif" width="85" height="22" alt="Agregar"/>
							</a>
						</td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
</table>
<input name="opcion" type="hidden"/>
<input id="idEmpleado" name="idEmpleado" value="${idEmpleado}" type="hidden"/>
</form>
	</body>
</html>