<%@ page language="java" %>
<%@ page contentType="text/html;charset=ISO8859_1"%>
<html>
<head>
	<title>Consulta de Saldos (Cr&eacute;dito)</title>

<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript">

function cambiaArchivo(extencion) {
    var boton = document.getElementById("exportar");
    if (extencion==="txt") {
		document.getElementById("tArchivo").value =extencion;
        document.getElementById("idTxt").checked= true;
    }else if(extencion==="csv"){
		document.getElementById("tArchivo").value =extencion;
        document.getElementById("idCsv").checked= true;
    }
    boton.href="javascript:Exportar();";
}


function Exportar(){
 	
	var forma=document.SaldoCred;
	var contador=0;

	for(j=0;j<forma.length;j++){
		if(forma.elements[j].type=='radio' && forma.elements[j].name=='tipoArchivo'){
			if(forma.elements[j].checked){
			    contador++;								
			}		
		}
	}
			
	if(contador===0){
		cuadroDialogo("Indique el tipo de archivo a exportar ",1);
 		return;
	}else{
		var extencion = document.getElementById("tArchivo").value;
		window.open("/Enlace/enlaceMig/MCL_Posicion?tipoArchivoExport=" + extencion,"Exportar","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
	}

}


function validarForm(){
	var forma = document.SaldoCred;
	
}


<%
          if (request.getAttribute("Errores")!= null) {
          out.println(request.getAttribute("Errores"));
          }
%>



function VentanaAyuda(cad)
{
  return;
}

/******************  Esto no es MIO ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  
	var p, i, x;
		if (!d)
			d = document;
		if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
			d = parent.frames[n.substring(p + 1)].document;
			n = n.substring(0, p);
		}
		if (!(x = d[n]) && d.all)
			x = d.all[n];
		for (i = 0; !x && i < d.forms.length; i++)
			x = d.forms[i][n];
		for (i = 0; !x && d.layers && i < d.layers.length; i++)
			x = MM_findObj(n, d.layers[i].document);
		return x;
	}

	function MM_swapImage() { //v3.0
		var i, j = 0, x, a = MM_swapImage.arguments;
		document.MM_sr = new Array;
		for (i = 0; i < (a.length - 2); i += 3)
			if ((x = MM_findObj(a[i])) != null) {
				document.MM_sr[j++] = x;
				if (!x.oSrc)
					x.oSrc = x.src;
				x.src = a[i + 2];
			}
	}
		

	/********************************************************************************/
<%
          if (request.getAttribute("newMenu")!= null) {
          out.println(request.getAttribute("newMenu"));
          }
%>

</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor="#ffffff" leftMargin="0" topMargin="0" marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">	
<%
          if (request.getAttribute("MenuPrincipal")!= null) {
          out.println(request.getAttribute("MenuPrincipal"));
          }
    %>
   </td>
  </tr>
</table>
<%
          if (request.getAttribute("Encabezado")!= null) {
          out.println(request.getAttribute("Encabezado"));
          }
%>

<form  name="SaldoCred" method="post">
<input id="tArchivo" type="hidden" name="tArchivo" value="csv" readonly="readonly" />
  <p>
   <table border="0" align="center">
    <tr>
	 <td>
    <%
          if (request.getAttribute("Tabla")!= null)
		    out.println(request.getAttribute("Tabla"));
    %>
     </td>
    </tr>
   </table>
	<div style="text-align: center;">
		<table style="width=150px; margin: auto;">
			<tr>
			  	<td><input id="idTxt" type="radio" value="txt" name="tipoArchivo" onclick="cambiaArchivo('txt');" /></td>
			  	<td class="tabmovtex11">Exporta en TXT</td>
			  	</tr>
			  	<tr>
			  		<td><input id="idCsv" type="radio" value="csv" name="tipoArchivo" onclick="cambiaArchivo('csv');"   checked="checked"  /></td>
			  		<td class="tabmovtex11">Exporta en XLS</td>
			  	</tr>
		 </table>
	</div>
	  <br />
		     
  <p/>
  <table align="center" border="0" cellpadding="0" cellspacing="0">
   <tr>
     <td><%if (request.getAttribute("Exportar")!= null) out.println(request.getAttribute("Exportar"));%><a href="javascript:scrImpresion();" border="0"><img src="/gifs/EnlaceMig/gbo25240.gif" border="0" alt="Imprimir" /></a></td>
   </tr>
  </table>
</form>

</body>
</html>