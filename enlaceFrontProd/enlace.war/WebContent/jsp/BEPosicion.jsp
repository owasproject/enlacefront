<html>
 <head>
  <title>Banca Virtual</TITLE>
  <!--
    Elaborado:  Francisco Serrato Jimenez (fsj)
    Aplicativo: Enlace Internet
    Proyecto:   MX-2002-257
                Getronics CP Mexico
    Archivo:    BEPosicion.jsp
  -->
  <script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
  <script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
  <script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
  
  
  <script type="text/javascript">
function exportarArchivo() {
	var isActivo = false;
	
	if(document.getElementById("radioTXT").checked) {
		isActivo = true;
		document.getElementById("tArchivo").value = "txt";
		
	} else if(document.getElementById("radioXLS").checked) {
		isActivo = true;
		document.getElementById("tArchivo").value = "csv";
	}
	
	if(!isActivo) {
		alert("Debe seleccionar un tipo de archivo para exportar");
		return;
	}
	
	var extension = document.getElementById("tArchivo").value;
	msg=window.open("/Enlace/enlaceMig/BE_Posicion?archivoExportacion="+extension+"&tipoConsulta=soloPosicion","Exportacion","location=no,directories=no,status=no,menubar=no,resizable=no,width=-100,height=-100, fullscreen=no");
	
}
   
  
   <!--
    function MM_preloadImages()
    { //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }
    function MM_swapImgRestore()
    { //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }
    function MM_findObj(n, d)
    { //v3.0
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
        d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
    }
    function MM_swapImage()
    { //v3.0
        var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }
<%
    if( request.getAttribute("newMenu")!=null )
        out.println(request.getAttribute("newMenu"));
%>
   //-->
  </script>
  <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
 </head>
 <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">
  <TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
   <tr valign="top">
    <td width="*">
<%
    if( request.getAttribute("MenuPrincipal")!=null )
        out.println(request.getAttribute("MenuPrincipal"));
%>
   </tr>
  </table><br>
<%
    if( request.getAttribute("Encabezado")!=null )
        out.println(request.getAttribute("Encabezado"));
%>
  <FORM  NAME="BEPosicion" METHOD="POST" ACTION="BE_Posicion">
  <input id="tArchivo" type="hidden" name="tArchivo" value="csv" readonly="readonly" />
   <table width="760" border="0" cellspacing="0" cellpadding="0">
    <tr>
     <td align="center">
      <TABLE width="228" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
       <tr>
        <td class="tittabdat" nowrap>Contrato Fondos de Inversi&oacute;n:</td>
        <td class="textabdatcla" align="right"> <%= request.getAttribute("count") %></td>
       </tr>
       <tr>
        <td class="tittabdat" nowrap>Mesa de Dinero:</td>
        <td class="textabdatcla" align="right">$<%= request.getAttribute("totalMesa") %></td>
       </tr>
       <tr>
        <td class="tittabdat" nowrap>Fondos de Inversi&oacute;n:</td>
        <td class="textabdatcla" align="right">$<%= request.getAttribute("totalCart") %></td>
       </tr>
      </table><br>
      <table width="540" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
<%
    if( request.getAttribute("tabla")!=null )
        out.println(request.getAttribute("tabla"));
%>
      </table>
	  <br/>
	  	<div style="text-align: center;">
		  <table style="margin: auto; width: 150px;">
		  <tr>
		  		<td><input type="radio" name="radioExport" 	id="radioTXT"></td>
				<td class="tabmovtex11" ><label>Exporta en TXT</label></td>
		  </tr>
		  <tr>
			   <td><input type="radio" name="radioExport" id="radioXLS" checked="checked" ></td>
			   <td class="tabmovtex11" ><label>Exporta en XLS</label></td>
		  </tr>
		  </table>
		 </div>
		  <br/>
      <table width="540" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
       <tr>
        <td align="center">
         <a href = "javascript:scrImpresion();" border = 0><img src = "/gifs/EnlaceMig/gbo25240.gif" border=0 width="83" height="22" alt="Imprimir"></a>
		 <a style="border: 0;" id="exportar" href="javascript:exportarArchivo();" ><img src = "/gifs/EnlaceMig/gbo25230.gif"  alt="Exportar" style="border: 0;" /></a>
        </td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
   <INPUT TYPE="hidden" VALUE="-1" NAME="registro">
   <INPUT TYPE="hidden" VALUE=""   NAME="secuencia">
   <INPUT TYPE="hidden" VALUE="0"  NAME="operacion">
   <INPUT TYPE="hidden"            NAME="nuevoArchivo">
   <INPUT TYPE="hidden" VALUE="<%= request.getAttribute("fechaHoy") %>" NAME="fechaHoy">
  </form><br>
 </body>
</html>
<script>
<%
    if( request.getAttribute("errorEnPosicion")!= null )
        out.println(request.getAttribute("errorEnPosicion"));
%>
</script>