<html><%-- #BeginTemplate "/Templates/principal.dwt" --%>
<head>
<%-- #BeginEditable "doctitle" --%>
<title>Enlace</title>
<%-- #EndEditable --%>

<%-- #BeginEditable "MetaTags" --%>
<meta http-equiv="Content-Type" content="text/html;" />
<meta name="Codigo de Pantalla" content="s25290" />
<meta name="Proyecto" content="Portal" />
<meta name="Version" content="1.0" />
<%-- #EndEditable --%>

 <script type="text/javascript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
 <script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
 <script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/validacionesFiltroCuentas.js"></script>

<Script type="text/javascript" >

function FrmClean(){

  document.MantNomina.textcuenta.value	="";
  document.MantNomina.fecha1.value	= "";
  document.MantNomina.fecha2.value	= "";
  limpiarFiltro();

}
/**
 *Limpia filtro de consulta
 */
function limpiarFiltro(){
	document.getElementById("numeroCuenta0").value ="";
	document.getElementById("descripcionCuenta0").value ="";
	consultaCuentas(0,2,1);
}

// Marzo 2015
// Enlace PYMES

function obtenerCuentaSeleccionada(combo, elemento, desc) {
	var select = document.getElementById(combo);
	if (!(select === undefined)) {
		var valor = select.options[select.selectedIndex].value;

		if(valor !== null
	   	&& "" !==valor){
	   		if (desc) {
	   			document.getElementById(elemento).value = obtenerCuentaTexto(valor);
	   		} else {
				document.getElementById(elemento).value = obtenerCuenta(valor);
		 	}
		}
	}
}

function obtenerCuenta(valor){
    var res = valor.split("\|");
     if (res[0]=="") {
      return "";
    }
    if(res.length>1){
	    return res[0];
    }
}

function obtenerCuentaTexto(valor){
    var res = valor.split("\|");
     if (res[0]=="") {
      return "";
    }
    if(res.length>1){
	    return res[0] + " " + res[2];
    }
}


// Marzo 2015
// Enlace PYMES


function js_consultar(){
   	var pasa = true;
   	var cuentaCargo;
   	fecha1 =   document.MantNomina.fecha1.value;
   	fecha2 =   document.MantNomina.fecha2.value;

	 obtenerCuentaSeleccionada("comboCuenta0", "cuenta", false);
	obtenerCuentaSeleccionada("comboCuenta0", "textcuenta", true);
	var select = document.getElementById("comboCuenta0");
	if((document.MantNomina.cuenta.value==null || document.MantNomina.cuenta.value=="")
	   && select.selectedIndex > 0){
		cuadroDialogo("La cuenta de Cargo es invalida.", 11);
		return;
	}


	if(fecha1 == ""  && fecha2 == ""){
		cuadroDialogo("Usted no ha seleccionado un rango de fecha",1);
		return;
	}

	if (mayor(fecha1, fecha2)){
		cuadroDialogo("La fecha inicial no puede ser mayor a la fecha final",1);
		return;
	}

	if(fecha1 == ""){
		cuadroDialogo("Usted no ha seleccionado una fecha en el campo Desde",1);
		return;
	}

	if(fecha2 == ""){
		cuadroDialogo("Usted no ha seleccionado una fecha en el campo Hasta",1);
		return;
	}

	document.MantNomina.operacion.value="consulta";
	document.MantNomina.action="OpcionesNominaLn?NoPagina=1";
	document.MantNomina.submit();

}

function mayor(fecha, fecha2){
	var fechaIni=fecha.split("/");
	var fechaFin=fecha2.split("/");
	var fechainicial=fechaIni[2]+fechaIni[1]+fechaIni[0];
	var fechafinal=fechaFin[2]+fechaFin[1]+fechaFin[0];

	if(parseInt(fechainicial)>parseInt(fechafinal)){
		return(true);
	}else{
		return(false);
	}
}

function Actualiza()
 {
     if(Indice==0)
		document.MantNomina.fecha1.value=Fecha[Indice];
	 if(Indice==1)
		document.MantNomina.fecha2.value=Fecha[Indice];
 }

var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}

function actualizacuenta()
{
  document.MantNomina.cuenta.value=ctaselec;
  document.MantNomina.textcuenta.value=ctaselec+" "+ctadescr;
}

function EnfSelCta()
{
  document.MantNomina.textcuenta.value="";
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
/*funcion para inhabilitar el click derecho*/
   document.oncontextmenu = function() {
      return false
   }
   function right(e) {

      if (navigator.appName == 'Netscape' && e.which == 3) {

         return false;
      }
      else if (navigator.appName == 'Microsoft Internet Explorer' && event.button==2) {
      return false;
      }
   return true;
}
/*Funcion para mostrar los calendarios*/
function js_calendario(ind){
	var m = new Date()
	Indice = ind;
    n = m.getMonth();
    msg = window.open("/EnlaceMig/EI_CalendarioLn.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
    msg.focus();
}

<%
 String FechaHoy="";
 FechaHoy=(String)request.getAttribute("FechaHoy");
 if(FechaHoy==null)
    FechaHoy="";
%>

//Indice Calendario
	var Indice=0;


//Arreglos de fechas
<%
	if(request.getAttribute("VarFechaHoy")!= null)
		out.print(request.getAttribute("VarFechaHoy"));
%>

//Dias inhabiles
	diasInhabiles='<% if(request.getAttribute("DiasInhabiles")!= null) out.print(request.getAttribute("DiasInhabiles")); %>';

<%
	String DiaHoy="";
	if(request.getAttribute("DiaHoy")!= null)
 		DiaHoy=(String)request.getAttribute("DiaHoy");
%>

<%= request.getAttribute("newMenu") %>
</script>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
style=" bgcolor: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);"
 onLoad='consultaCuentas(0,2,1);<%= request.getAttribute("ArchivoErr")%>'; 'MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')' >

<%-- #BeginLibraryItem "/Library/navegador.lbi" --%>
<table width="571" border="0" cellspacing="0" cellpadding="0">
  <tr valign=top>
    <td width="*">
		<%-- MENU PRINCIPAL --%>
        <%= request.getAttribute("MenuPrincipal") %>
    </td>
  </tr>
</table>
<%-- #EndLibraryItem --%><%-- #BeginEditable "Contenido" --%>

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="676" valign="top" align="left">
      <table width="666" border="0" cellspacing="6" cellpadding="0">
        <tr>
          <td width="528" valign="top" class="titpag"><%= request.getAttribute("Encabezado") %></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<%-- #BeginLibraryItem "/Library/contrato.lbi" --%>

<FORM  NAME="MantNomina" METHOD="Post" ACTION="MtoNomina">

   <table width="760" border="0" cellspacing="0" cellpadding="0">
    <tr>
	 <td align="center">
	    <table border="0" cellspacing="2" cellpadding="3">
		 <tr>
		   <td class="tittabdat" colspan="2" nowrap> Seleccione los datos:</td>
		 </tr>

		 	 <tr valign="top" align="left" >
    					<td colspan="2">
		          			<%@ include file="/jsp/filtroConsultaCuentasNominaBoton.jspf" %>
		          		</td>
    				</tr>

		 <tr align="center">
		   <td class="textabdatcla" valign="top" colspan="2">
			<table width="400" border="0" cellspacing="0" cellpadding="0">
			 <tr valign="top">
			  <td width="420" align="left">

               <table width="420" border="0" cellspacing="0" cellpadding="5">
			    <tr>

				<td align="right" class="tabmovtex">
					Cuenta de cargo:
				</td>
				 <td class="tabmovtex" nowrap >
					<%--
				 <input type="text" name=textcuenta class="tabmovtexbol" maxlength=30 size=30 onfocus="blur();" onmousedown="right" value=""/>
				 &nbsp;

				 <a href="javascript:PresentarCuentas();">
				 <img src="/gifs/EnlaceMig/gbo25420.gif" border=0 style="vertical-align:middle;">
				 </a>--%>
				 <input type="hidden" name=textcuenta id=textcuenta value=""/>
				 <input type="hidden" name="cuenta" id="cuenta" value=""/>
					<%@ include file="/jsp/listaCuentasTransferencias.jspf" %>
				 </td>
				</tr>
				<tr>
                 <td align="right" class="tabmovtex"><input type="radio" name="rbtnFecha" checked="checked" value="F_RECEP" />Por fecha de recepci&oacute;n.<br></br></td>
				 <td align="left" class="tabmovtex"><input type="radio" name="rbtnFecha" value="F_APLIC" />Por fecha de aplicaci&oacute;n.<br></br></td>
				</tr>
				<tr>
                 <td align="right" class="tabmovtex">*Desde:<br></br></td>
				 <td class="tabmovtex" nowrap valign="middle"><input type="text" name="fecha1" size="12" class="tabmovtex" readonly="readonly" OnFocus = "blur();" value="<%= request.getAttribute("fechaActual")%>" /><a href="javascript:js_calendario(0);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" style="vertical-align:middle; border: 0px;" /></a></td>
				</tr>
				<tr>
				 <td align="right" class="tabmovtex">*Hasta:</td>
				 <td class="tabmovtex" nowrap valign="middle"><input type="text" name="fecha2" size="12" class="tabmovtex" readonly="readonly" OnFocus = "blur();" value="<%= request.getAttribute("fechaActual")%>" /><a href="javascript:js_calendario(1);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" style="vertical-align:middle; border: 0px;" /></a></td>
				</tr>
			   </table>

              </td>
			 </tr>
			</table>
		   </td>
          </tr>
        </table>

        <br></br>
        <table width="166" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" style="height:22">
         <tr>
          <td align="right" valign="top" height="22" width="90"><a href="javascript:js_consultar();"><img style=" border: 0px;" name="Enviar" src="/gifs/EnlaceMig/gbo25220.gif" width="90" height="22" alt="Consultar" /></a></td>
		  <td align="left" valign="middle" height="22" width="76"><a href="javascript:FrmClean();"><img name="limpiar" style=" border: 0px;" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar" /></a></td>
		 </tr>
        </table>

	 </td>
    </tr>

  </table>

  <INPUT TYPE="hidden" VALUE="0" NAME="operacion" />
  <INPUT TYPE="hidden" VALUE="-1" NAME="registro" />
  <INPUT TYPE="hidden" VALUE="<%
								if(	session.getAttribute("fechaHoy") !=null)
									out.println( session.getAttribute("fechaHoy"));
								else
									if (request.getAttribute("fechaHoy")!= null)
										out.println(request.getAttribute("fechaHoy"));
							%>" NAME="fechaHoy" />
  <INPUT TYPE="hidden" NAME="nuevoArchivo" />
  <INPUT TYPE="hidden" VALUE="<%
								if(	session.getAttribute("tipoArchivo") !=null)
									out.println( session.getAttribute("tipoArchivo"));
								else
									if (request.getAttribute("tipoArchivo")!= null)
										out.println(request.getAttribute("tipoArchivo"));
							%>"  NAME="tipoArchivo" />
  <INPUT TYPE="hidden" VALUE="<%
								if(	session.getAttribute("horaSistema") !=null)
									out.println( session.getAttribute("horaSistema"));
								else
									if (request.getAttribute("horaSistema")!= null)
										out.println(request.getAttribute("horaSistema"));
							%>" NAME="horaSistema" />

  <input type="hidden" name="archivoEstatus" value="<%
								if(	session.getAttribute("archivoEstatus") !=null)
									out.println( session.getAttribute("archivoEstatus"));
								else
									if (request.getAttribute("archivoEstatus")!= null)
										out.println(request.getAttribute("archivoEstatus"));
							%>" />

  <INPUT TYPE="hidden" VALUE="<%
								if (request.getAttribute("cantidadDeRegistrosEnArchivo")!= null)
								    out.println(request.getAttribute("cantidadDeRegistrosEnArchivo"));
								else
								if(session.getAttribute("cantidadDeRegistrosEnArchivo") !=null)
									out.println( session.getAttribute("cantidadDeRegistrosEnArchivo"));


							%>" NAME="cantidadDeRegistrosEnArchivo" />
  <INPUT TYPE="hidden" VALUE="<% if (request.getAttribute("nombre_arch_salida")!= null)
				    out.println(request.getAttribute("nombre_arch_salida"));
			    %>" NAME="nombre_arch_salida" />
  <INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute( "lista_nombres_archivos" ) != null ) { out.print( request.getAttribute( "lista_nombres_archivos" ) ); } %>" NAME="lista_nombres_archivos" />
  <INPUT TYPE="hidden" VALUE="<%
								if (request.getAttribute("fechaHoy")!= null)
								out.println(request.getAttribute("fechaHoy"));
							%>" NAME="fechaHoy" />

<%
	if(	session.getAttribute("facultadImportar") !=null)
		out.println( session.getAttribute("facultadImportar"));
	else
	if (request.getAttribute("facultadImportar")!= null) {
		out.println(request.getAttribute("facultadImportar"));
	}
%>
<%
	if(	session.getAttribute("facultadAlta") !=null)
		out.println( session.getAttribute("facultadAlta"));
	else
	if (request.getAttribute("facultadAlta")!= null) {
		out.println(request.getAttribute("facultadAlta"));
	}
%>
<%
	if(	session.getAttribute("facultadCambios") !=null)
		out.println( session.getAttribute("facultadCambios"));
	else
	if (request.getAttribute("facultadCambios")!= null) {
		out.println(request.getAttribute("facultadCambios"));
	}
%>
<%
	if(	session.getAttribute("facultadBajas") !=null)
		out.println( session.getAttribute("facultadBajas"));
	else
	if (request.getAttribute("facultadBajas")!= null) {
		out.println(request.getAttribute("facultadBajas"));
	}
%>
<%
	if(	session.getAttribute("facultadEnvio") !=null)
		out.println( session.getAttribute("facultadEnvio"));
	else
	if (request.getAttribute("facultadEnvio")!= null) {
		out.println(request.getAttribute("facultadEnvio"));
	}
%>
<%
	if(	session.getAttribute("facultadRecuperar") !=null)
		out.println( session.getAttribute("facultadRecuperar"));
	else
	if (request.getAttribute("facultadRecuperar")!= null) {
		out.println(request.getAttribute("facultadRecuperar"));
	}
%>
<%
	if(	session.getAttribute("facultadExportar") !=null)
		out.println( session.getAttribute("facultadExportar"));
	else
	if (request.getAttribute("facultadExportar")!= null) {
		out.println(request.getAttribute("facultadExportar"));
	}
%>
<%
	if(	session.getAttribute("facultadBorra") !=null)
		out.println( session.getAttribute("facultadBorra"));
	else
	if (request.getAttribute("facultadBorra")!= null) {
		out.println(request.getAttribute("facultadBorra"));
	}
%>
</form>
<%-- #EndEditable --%>
</body>
<%-- #EndTemplate --%></html>
<script type="text/javascript">
<%
	if(	session.getAttribute("infoUser") !=null)
		out.println( session.getAttribute("infoUser"));
	else
	if (request.getAttribute("infoUser")!= null) {
		out.println(request.getAttribute("infoUser"));
	}

	if(	session.getAttribute("infoImportados") !=null)
		out.println( session.getAttribute("infoImportados"));
	else
	if (request.getAttribute("infoImportados")!= null) {
		out.println(request.getAttribute("infoImportados"));
	}
%>


</script>