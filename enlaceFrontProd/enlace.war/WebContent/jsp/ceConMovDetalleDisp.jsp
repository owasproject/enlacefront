<%@ page language="java" %>
<%@ page contentType="text/html;charset=ISO8859_1"%>
<html><!-- #BeginTemplate "/Templates/principal.dwt" -->
<head>
<!-- #BeginEditable "doctitle" -->
<title>Enlace</title>
<!-- #EndEditable -->
<%-- 01/10/2002 Se corrigieron etiquetas HTML --%>
<!-- #BeginEditable "MetaTags" -->
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s25180">
<meta name="Proyecto" content="Portal">
<meta name="Version" content="1.0">
<meta name="Ultima version" content="27/04/2001 18:00">
<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico">
<!-- #EndEditable -->
<script language = "JavaScript" type="text/javascript" SRC="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javascript" type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<Script language = "Javascript" type="text/javascript">
var Nmuestra = 30;
function atras(){
//alert("entrando a funcion atras");
    if((parseInt(document.frmbit.prev.value) - Nmuestra)>0){
      document.frmbit.next.value = document.frmbit.prev.value;
      document.frmbit.prev.value = parseInt(document.frmbit.prev.value) - Nmuestra;

      document.frmbit.submit();
    }
}

function adelante(){
 if(parseInt(document.frmbit.next.value) != parseInt(document.frmbit.total.value)){
    if((parseInt(document.frmbit.next.value) + Nmuestra) <= parseInt(document.frmbit.total.value)){
      document.frmbit.prev.value = document.frmbit.next.value;
      document.frmbit.next.value = parseInt(document.frmbit.next.value) + Nmuestra;

      document.frmbit.submit();
   }else if((parseInt(document.frmbit.next.value) + Nmuestra) > parseInt(document.frmbit.total.value)){
      document.frmbit.prev.value = document.frmbit.next.value;
      document.frmbit.next.value = parseInt(document.frmbit.next.value) + (parseInt(document.frmbit.total.value) - parseInt(document.frmbit.next.value));

      document.frmbit.submit();
   }
 }else{
   cuadroDialogo("no hay mas registros", 1);
 }

 }
 function MakeFile(){
    document.frmbit.prev.value = 0;
    document.frmbit.next.value = document.frmbit.total.value;
    document.frmbit.action = "CreaArchMov";
    document.frmbit.submit();
 }
</Script>


<Script language = "JavaScript" type="text/javascript">

<%
       if (request.getAttribute("Errores")!= null) {
       out.println(request.getAttribute("Errores"));
       }
%>

function enviarpos()
{
	var ban1=true;
	document.fmov.action="ceConPosicion";
	ban1=validar(1);
	if (ban1==true)
		document.fmov.submit();
}

function enviarmovs()
{
	if(validar())
	{
		document.fmov.action="ceConMovimientos?Modulo=2";
		document.fmov.submit();
	}
}

function validar(boton)
{

	var cta;
	var resultado=false;
	var contador=0;
	for (i=0;i<document.fmov.elements.length;i++){
		if (document.fmov.elements[i].type=="radio"){
			if (document.fmov.elements[i].checked==true){
				contador++;
				resultado=true;
				break;
			}
		}
	}
	if (contador==0)
		cuadroDialogo("Usted no ha seleccionado una cuenta.",3);
	return resultado;
}
</script>


<script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Mon Apr 23 20:21:38 GMT-0600 2001-->
<script language="JavaScript" type="text/javascript">
<!--

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
<%= request.getAttribute("newMenu") %>

</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<!--<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">
-->
<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
	<td width="*">
	<!-- MENU PRINCIPAL -->
	<%= request.getAttribute("MenuPrincipal") %>
	</TD>
  </TR>
</TABLE>
<%= request.getAttribute("Encabezado") %>
<FORM  NAME="fmov" method=post action="" onSubmit="return validar();">
<table width="760" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td align="center">
		<table width="750" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
		  <tr>
<!--			<td align="center" class="tittabdat" width="65">Seleccione</td> -->
			<td align="center" class="tittabdat" width="90">Fecha</td>
			<td align="center" class="tittabdat" width="90">Concepto</td>
			<td class="tittabdat" align="center" width="90">Referencia</td>
			<td class="tittabdat" align="center" width="90">Cargo</td>
			<td class="tittabdat" align="center" width="90">Abono</td>
		  </tr>
		  <%
		  if (request.getAttribute( "valor" )!= null)
		  {
	         out.println(request.getAttribute( "valor" ));
		  }
		  %>
    	</table>
		<br>
		<br>
		<!-- #BeginLibraryItem "/Library/footerPaginacion.lbi" -->
		<table width="357" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
			<tr>
				<td align="center">
				<a href="javascript:scrImpresion();" border="0">
					<img  src="/gifs/EnlaceMig/gbo25240.gif" border="0" alt="Imprimir">
				</a>
				</td>
				<td align="center">
				<%= request.getAttribute( "Exportar" ) %>
				</td>
				<td align="center"><a href ="javascript:history.back();">
		  <img border="0" name="imageField32" src="/gifs/EnlaceMig/gbo25320.gif" width="83" height="22" alt="Regresar"></a>
		</td>
				</tr>
		</table>
</td>
</tr>
</table>

</form>
</body>
</html>