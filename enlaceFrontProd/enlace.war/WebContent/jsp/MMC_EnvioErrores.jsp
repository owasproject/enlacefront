<%@ page import="java.lang.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.*" %>

<%@ page import="mx.altec.enlace.dao.*" %>
<%@ page import="mx.altec.enlace.utilerias.*" %>
<%@ page import="mx.altec.enlace.bo.*" %>

<html>
<head>
	<title>Descripci&oacute;n y Estatus de cuentas</TITLE>

<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript1.2" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript">
	<%
  System.out.println("Se ejecuta MTE_ImportaDetalle.jsp");

  String nombreArchivoImp="";
  String registroDeAlta="";
  String registroDeBaja="";
  String registroDeSolicitud="";
  String totalRegistros="";
  String nombreArchivoDetalle="";

  boolean baja=false;

  if(request.getParameter("nombreArchivoImp")!=null)
    nombreArchivoImp=(String)request.getParameter("nombreArchivoImp");
  if(request.getParameter("nombreArchivoDetalle")!=null)
    nombreArchivoDetalle=(String)request.getParameter("nombreArchivoDetalle");
  if(request.getParameter("totalRegistros")!=null)
    totalRegistros=(String)request.getParameter("totalRegistros");
  if(request.getParameter("registroDeSolicitud")!=null)
    registroDeSolicitud=(String)request.getParameter("registroDeSolicitud");
  if(request.getParameter("registroDeAlta")!=null)
    registroDeAlta=(String)request.getParameter("registroDeAlta");
  if(request.getParameter("registroDeBaja")!=null)
    registroDeBaja=(String)request.getParameter("registroDeBaja");

  System.out.println("Recibiendo parametros MTE_ImportaDetalle.jsp");
  System.out.println("nombreArchivoImp "+nombreArchivoImp);
  System.out.println("nombreArchivoDetalle "+nombreArchivoDetalle);
  System.out.println("registroDeSolicitud " + registroDeSolicitud);
  System.out.println("registroDeBaja "+ registroDeBaja);
  System.out.println("registroDeAlta "+ registroDeAlta);

  int regDif=0;
  int regRec=0;

  if(registroDeAlta.trim().equals("") && !registroDeBaja.equals(""))
   {
	 baja=true;
	 System.out.println("Se procesa para baja de cuentas ");
	 regDif=Integer.parseInt(registroDeBaja)+Integer.parseInt(registroDeSolicitud);
	 regRec=Integer.parseInt(totalRegistros)-regDif;
   }
  else
   {
	 System.out.println("Se procesa para alta de cuentas ");
	 regDif=Integer.parseInt(registroDeAlta)+Integer.parseInt(registroDeSolicitud);
	 regRec=Integer.parseInt(totalRegistros)-regDif;
   }

	  System.out.println("Generando pantalla");
	  System.out.println("Registros: " + totalRegistros);

	  int inicio=0;
	  int fin=0;
	  int totalReg=Integer.parseInt(totalRegistros);

	  int pagina=0;
	  if(request.getParameter("pagina")!=null)
		 pagina=Integer.parseInt(request.getParameter("pagina"));
	  int regPorPagina=Global.MAX_REGISTROS;

	  inicio=(pagina*regPorPagina);
	  fin=inicio+regPorPagina;
	  if(fin>totalReg)
		fin=totalReg;

	  int totalPaginas=calculaPaginas(totalReg,regPorPagina);

	  int A=regPorPagina*(pagina+1);
	  int de=(A+1)-regPorPagina;

	  if((pagina+1)==totalPaginas)
		 A=totalReg;
	%>

function BotonPagina(sigant)
 {
   document.TesImporta.pagina.value=sigant-1;
   document.TesImporta.submit();
 }
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<FORM   NAME="TesImporta" METHOD="Post" ACTION="/Enlace/jsp/MMC_EnvioErrores.jsp">

<table width="700" border="0" cellspacing="0" cellpadding="0" align=center>
 <tr>
   <td align=center>
     <table border="0" width="700" cellspacing="0" cellpadding="0">
	   <tr>
	    <td><br></td>
	   </tr>
	 </table>


 <table width="700" border="0" cellspacing="0" cellpadding="0">
  <td align=center>
	<table width="700" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">

	 <tr>
	  <td class="tittabdat" align="center"> &nbsp; Nombre de Archivo Enviado </td>
	  <!--<td class="tittabdat" align="center"> &nbsp; Nombre de Archivo Detalle</td>//-->
	  <td class="tittabdat" align="center"> &nbsp; Registros Enviados</td>
	  <td class="tittabdat" align="center"> &nbsp; Registros Autorizados</td>
	  <td class="tittabdat" align="center"> &nbsp; Registros Pendientes Auto.</td>
	  <td class="tittabdat" align="center"> &nbsp; Registros Rechazados</td>
	 </tr>
	 <tr>
	  <td class="textabdatobs" nowrap align="center"><%=nombreArchivoImp.substring(nombreArchivoImp.lastIndexOf("/")+1)%></td>
	  <!--<td class="textabdatobs" nowrap align="center"><%=nombreArchivoDetalle%></td>//-->
	  <td class="textabdatobs" nowrap align="center"><%=totalRegistros%></td>
	  <td class="textabdatobs" nowrap align="center"><%=((baja==true)?registroDeBaja:registroDeAlta)%></td>
	  <td class="textabdatobs" nowrap align="center"><%=registroDeSolicitud%></td>
	  <td class="textabdatobs" nowrap align="center"><%=regRec%>&nbsp;</td>
	 </tr>
	</table>
	<br>

   </td>
  </tr>
</table>

<%
System.out.println(" verificando ");
%>

	<table border="0" width="700" cellspacing="0" cellpadding="0">
	   <tr>
	    <td><br></td>
	   </tr>
	   <tr>
	     <td class="textabref">Detalle de archivo</td>
	   </tr>
	   <tr>
	    <td colspan=4 align=left class='textabdatcla'>
		  <table width="100%" class='textabdatcla'>
		    <tr>
		     <td align=left class='textabdatcla'><b>&nbsp;Pagina <font color=blue><%=(pagina+1)%></font> de <font color=blue><%=totalPaginas%></font>&nbsp;</b></td>
		     <td align=right class='textabdatcla'><b>Registros <font color=blue><%=de%></font> - <font color=blue><%=A%></font></b>&nbsp;</td>
		    </tr>
		  </table>
		</td>
	  </tr>
	 </table>

     <table border="0" width="700" cellspacing="2" cellpadding="3" class="tabfonbla">
	   <tr>
	     <td class="tittabdat" align="center">Registro</td>
		 <td class="tittabdat" align="center">Estatus</td>
		 <td class="tittabdat" align="center">Cuenta</td>
		 <td class="tittabdat" align="center">Descripci&oacute;n</td>
	   </tr>
	 <%
	 	   BufferedReader archImp=null;
	 	   boolean error=false;
	 	   try
	 	    {
	 	     System.out.println("Generando pantalla");
	 		 System.out.println("Registros: " + totalRegistros);

	 		 archImp = new BufferedReader( new FileReader(nombreArchivoDetalle) );

	 		 String line="";
	 		 String cuenta="";
	 		 String descripcion="";
	 		 String clase="";
	 		 String campo="";
	 		 String estatus="";

	 		 EI_Tipo Err=new EI_Tipo();

	 		 int contador=0;

	 		 //line=archImp.readLine(); //Encabezado ...
	 		 int a = 0;
	 		 while( (line=archImp.readLine())!=null ){
	 		  
	 		 //for(int a=0;a<fin;a++)
	 		  if(line.indexOf("@")>=0 || line.startsWith("0;"))
	 		 {
	 			 System.out.println("Lineas no necesarias");

	 		  }
	 		 else if((a>=inicio)&&(a<=fin))
	 		{	
			 	  clase=(contador%2==0)?"textabdatobs":"textabdatcla";
			 	  Err.iniciaObjeto(';','@',line+"@");
			 	  estatus=Err.camposTabla[0][0];
			 	  cuenta=Err.camposTabla[0][1];
			 	  campo=Err.camposTabla[0][2];
			 	  descripcion=Err.camposTabla[0][3];
			 	  contador++;
		
			 	  if(estatus.equals("E"))
			 		estatus="<font color=red>ERROR</font>";
			 	  if(estatus.equals("P"))
			 		{
			 		  estatus="<font color=blue>PENDIENTE</font>";
			 		  descripcion="LA CUENTA SE ENCUENTRA PENDIENTE DE AUTORIZACI&Oacute;N";
			 		}
			 	  if(estatus.equals("A"))
			 		{
			 		  estatus="<font color=green>AUTORIZADA</font>";
			 		  descripcion="LA CUENTA SE AUTORIZ&Oacute; Y SE DI&Oacute; DE "+((baja==true)?"BAJA":"ALTA")+" EN EL SISTEMA";
			 		}
	 %>
		   <tr>
		     <td class="<%=clase%>" align="center"><%=(a+1)%></td>
		     <td class="<%=clase%>" align="left"><%=estatus%></td>
			 <td class="<%=clase%>" align="center"><%=cuenta%></td>
			 <td class="<%=clase%>" align="left"><%=descripcion%></td>
		   </tr>

	 <%
	   			a++;
			}
	        //line=archImp.readLine();
		  }

	    }catch(FileNotFoundException e)
		 {System.out.println("Archivo no encontrado ..."); error=true;}
		catch(IOException e)
		 {System.out.println("Error general..." + e.getMessage()); error=true;}
		finally
		 {
		   try
			{
	          archImp.close();
			}catch(IOException e)
			 {}
		 }

	  System.out.println("Se termino lectura con error: " + error);

	  if(error)
	  {
	 %>
	   <table border="0" width="700" cellspacing="2" cellpadding="3" class="tabfonbla">
	   <tr>
		 <td class="textabdatcla" align="center">No se pudo obtener el detalle del archivo...</td>
	   </tr>
	   </table>
	 <%
	  }
	 System.out.println("Finalizando jsp");
	 %>

     </table>

	  <%
	   System.out.println("MMC_ImportaDetalle.jsp-> Se genera la paginacion");
	   out.print(botones(totalReg,regPorPagina,pagina,totalPaginas));
	 %>
	 <table align=center border=0 cellspacing=0 cellpadding=0>
	  <tr>
	    <td><br></td>
	  </tr>
	  <tr>
	   <td align=center><A href = "javascript:window.close();" border = 0><img src="/gifs/EnlaceMig/gbo25200.gif" border=0 alt="Cerrar"></a></td>
	  </tr>
	 </table>

   </td>
 </tr>
</table>

 <input type=hidden name=nombreArchivoImp value='<%=nombreArchivoImp%>'>
 <input type=hidden name=totalRegistros value='<%=totalRegistros%>'>
 <input type=hidden name=registroDeAlta value='<%=registroDeAlta%>'>
 <input type=hidden name=registroDeBaja value='<%=registroDeBaja%>'>
 <input type=hidden name=registroDeSolicitud value='<%=registroDeSolicitud%>'>
 <input type=hidden name=nombreArchivoDetalle value='<%=nombreArchivoDetalle%>'>
 <input type=hidden name=pagina value='<%=pagina%>'>


 </FORM>
</body>
</html>

<%!
String botones(int totalRegistros,int regPorPagina,int pagina,int totalPaginas)
{
	StringBuffer botones=new StringBuffer("");

	if(totalRegistros>regPorPagina)
	   {
		  System.out.println("MMC_ImportaDetalle.jsp-> Estableciendo links para las paginas");
		  botones.append("\n <br>");
		  botones.append("\n <table width=620 border=0 align=center>");
		  botones.append("\n  <tr>");
		  botones.append("\n   <td align=center class='texfootpagneg'>");
		  System.out.println("MMC_ImportaDetalle.jsp-> Pagina actual = "+pagina);
		  if(pagina>0)
			 botones.append("&lt; <a href='javascript:BotonPagina("+(pagina)+");' class='texfootpaggri'>Anterior</a>&nbsp;");
		  if(totalPaginas>=2)
		   {
			 for(int i=1;i<=totalPaginas;i++)
			  if(pagina+1==i)
			   {
				 botones.append("&nbsp;");
				 botones.append(Integer.toString(i));
				 botones.append("&nbsp;");
			   }
			  else
			   {
				 botones.append("&nbsp;<a href='javascript:BotonPagina(");
				 botones.append(Integer.toString(i));
				 botones.append(");' class='texfootpaggri'>");
				 botones.append(Integer.toString(i));
				 botones.append("</a> ");
			   }
		   }
		  if(totalRegistros>((pagina+1)*regPorPagina))
			 botones.append("<a href='javascript:BotonPagina("+(pagina+2)+");' class='texfootpaggri'>Siguiente</a> &gt;");

		  botones.append("\n   </td>");
		  botones.append("\n  </tr>");
		  botones.append("\n </table>");
	   }
	 return botones.toString();
}

   int calculaPaginas(int total,int regPagina)
    {
      Double a1=new Double(total);
      Double a2=new Double(regPagina);
      double a3=a1.doubleValue()/a2.doubleValue();
      Double a4=new Double(total/regPagina);
      double a5=a3/a4.doubleValue();
      int totPag=0;

      if(a3<1.0)
        totPag=1;
      else
       {
         if(a5>1.0)
          totPag=(total/regPagina)+1;
         if(a5==1.0)
          totPag=(total/regPagina);
       }
      return totPag;
    }
%>