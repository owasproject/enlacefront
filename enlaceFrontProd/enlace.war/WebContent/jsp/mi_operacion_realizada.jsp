<%-- ATENCION GENTE DE MANTENIMIENTO: NO PONER COMENTARIOS con // y asteriscos, porque envía error de javascript--%>

<HTML>
<HEAD>
<TITLE>Banca Virtual</TITLE>
<script type="text/javascript" src = "/EnlaceMig/scrImpresion.js"></script>

<%-- Se agrega codigo para recibir mensaje cuando hay al menos una transaccion denegada. --%>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript">
<%
	if (request.getAttribute("LynxTranDenegadas") != null) {
		out.println(request.getAttribute("LynxTranDenegadas"));
		request.removeAttribute("LynxTranDenegadas");
	}
%>
</script>
<script type="text/javascript">
function formatea(para)
{
	var formateado = "";
	var car = "";

	for(a2=0;a2<para.length;a2++)
	{
		if(para.charAt(a2)==' ')
			car="+";
		else
			car=para.charAt(a2);
			formateado+=car;
	}
	return formateado;
}

function GenerarLiga(trans)
{
	var tipo=trans;
	var web_application=document.operacionrealizada.web_application.value;
	// vswf:meg cambio de NASApp por Enlace 08122008
	document.operacionrealizada.action="/Enlace/"+web_application+"/transferencia?ventana=10&trans="+tipo;
	document.operacionrealizada.submit();
}

function GenerarComprobante(datos,indice)
{
	var trama=obtentrama(datos,indice);
	/*
	var mitrama = datos.substring(datos.indexOf("|"));
	mitrama=mitrama.substring(datos.indexOf("|"));
	trama = mitrama;*/
	var tipo=trama.substring(0,trama.indexOf('|'));
	trama=trama.substring(trama.indexOf('|')+1,trama.length);
	var sreferencia=trama.substring(0,trama.indexOf('|'));
	trama=trama.substring(trama.indexOf('|')+1,trama.length);
	var importe_string=trama.substring(0,trama.indexOf('|'));
	trama=trama.substring(trama.indexOf('|')+1,trama.length);
	var cta_origen=trama.substring(0,trama.indexOf("|"));
	trama=trama.substring(trama.indexOf('|')+1,trama.length);
	var cta_destino=trama.substring(0,trama.indexOf("|"));
	trama=trama.substring(trama.indexOf('|')+1,trama.length);
	//var concepto=trama;
	//     inicia Cambio Comprobante fiscal
	var concepto="";
	var rfc="";
	var iva="";
	var plazo="";
	var tipo_inversion="";

	if ((tipo=="0")||(tipo=="1")||(tipo=="2"))
	{
	    //Q22887 - NVS - Inicio Modificación
        //If agregado para presentar concepto si la trama no tiene |
        if(trama.indexOf("|") != -1) {
	        concepto=trama.substring(0,trama.indexOf("|"));
	    }
	    else {
	        concepto=trama;
	    }
	    //Q22887 - NVS - Fin Modificación

		if(trama.indexOf('|')>0)
		{
			trama=trama.substring(trama.indexOf('|')+1,trama.length);
		    rfc=trama.substring(0,trama.indexOf("|"));
		    trama=trama.substring(trama.indexOf('|')+1,trama.length);
		//if(trama=="|")
        //   iva="";
		//else
		     iva=trama;
		     if(trama.indexOf("@")>0)
			   iva=trama.substring(0,trama.indexOf("@"));
		     else
             {
			   if(iva!="@")
			     iva=trama;
			   else
                 iva="";
             }
		}
		else
        {
		  rfc="";
		  iva="";
		}

	}
	else if (tipo=="5")
    {
        concepto=trama.substring(0,trama.indexOf("|"));
        trama=trama.substring(trama.indexOf('|')+1,trama.length);
		if(trama.indexOf('|')>0)
		{
          	plazo=trama.substring(0,trama.indexOf("|"));
		    tipo_inversion=trama.substring(trama.indexOf('|')+1,trama.length);
    	}
    }
	else
	  concepto=trama;

	//     termina Cambio Comprobante fiscal
	var cta_origen1=cta_origen;
	var cta_destino1=cta_destino;
	var concepto1=concepto;
	var tipo_inversion1=tipo_inversion;
	var usuario=document.operacionrealizada.datosusuario.value;
	var contrato=document.operacionrealizada.datoscontrato.value;
	cta_origen=formatea(cta_origen1);
	cta_destino=formatea(cta_destino1);
	concepto=formatea(concepto1);
	tipo_inversion=formatea(tipo_inversion1);
	usuario=formatea(usuario);
	contrato=formatea(contrato);
	var web_application=document.operacionrealizada.web_application.value;
	var banco=document.operacionrealizada.banco.value;

	
	//csa : ass
	//sreferencia= formateaAmp(sreferencia);
	importe_string= formateaAmp(importe_string);
	cta_origen=formateaAmp(cta_origen) ;
	cta_destino= formateaAmp(cta_destino);
	usuario= formateaAmp(usuario);
	contrato= formateaAmp(contrato);
	concepto= formateaAmp(concepto);
	//rfc= formateaAmp(rfc);
	//iva= formateaAmp(iva);
	banco=formateaAmp(banco);
	plazo=formateaAmp(plazo);
	tipo_inversion= formateaAmp(tipo_inversion);
	//fin csa : ass
	
	// vswf:meg cambio de NASApp por Enlace 08122008
	operacion= "/Enlace/"+web_application+"/comprobante_trans?tipo="+tipo;
	operacion+="&refe="+sreferencia+"&importe="+importe_string+"&cta_origen="+cta_origen;
    operacion+="&cta_destino="+cta_destino+"&enlinea=s&datosusuario="+usuario+"&datoscontrato="+contrato+"&concepto="+concepto+"&rfc="+rfc+"&iva="+iva+"&banco="+banco+"&plazo="+plazo+"&tipoinversion="+tipo_inversion; //     Cambio Comprobante fiscal

	ventanaInfo2=window.open(operacion,'Comprobante','width=470,height=450, scrollbars=yes');
	ventanaInfo2.focus();
}

// csa : ass
function formateaAmp(para) {
  var formateado="";
  var car="";
  if(para.indexOf("&") != -1) { 
      for(a2=0;a2<para.length;a2++) {
    	  if(para.charAt(a2)=='&')
    	   car="%26";
    	  else
    	   car=para.charAt(a2);
    
    	  formateado+=car;
    	}
  } else {
    formateado = para;
  }
  return formateado;
 
}

/*function obtentrama11(datos,indice)
{
	var ind=0;
	var cadena_retorno="";

	for(i=1;i<=eval(document.operacionrealizada.contadorok.value);i++)
	{
		ind=datos.substring(0,datos.indexOf("|"));
		if (eval(ind)==eval(indice))
		{
			cadena_retorno=datos.substring(datos.indexOf("|")+1,datos.indexOf("@"));
			break;
		}
		if (i<eval(document.operacionrealizada.contadorok.value))
		{
			datos=datos.substring(datos.indexOf("@")+1,datos.length);
		}
	}
	return cadena_retorno;
}*/

function obtentrama(datos,indice)
{
	var ind=0;
	var cadena_retorno="";

	for(i=1;i<=eval(document.operacionrealizada.contadorok.value);i++)
	{
		ind=datos.substring(0,datos.indexOf("|"));
		if (eval(ind)==eval(indice))
		{
			cadena_retorno=datos.substring(datos.indexOf("|")+1,datos.indexOf("@"));
			break;
		}
		if (i<eval(document.operacionrealizada.contadorok.value))
		{
			datos=datos.substring(datos.indexOf("@")+1,datos.length);
		}
	}
	return cadena_retorno;
}

</script>
<script type="text/javascript" src = "/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
		<%
		String newMenu = "";
		  if(session.getAttribute("newMenu")!=null)
			newMenu = (String)session.getAttribute("newMenu");
		  else if (request.getAttribute("newMenu")!= null)
			newMenu= (String)request.getAttribute("newMenu");
		  out.println(newMenu);

		%>
</script>
<%-- PYME 2015 INDRA se agrea funcion para exportar--%>
<script type="text/javascript">
	function fncExportar(){
	 	document.operacionrealizada.action = "ExportServlet";
	 	document.operacionrealizada.submit();
	}
</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />

</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
      style=" bgcolor: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);"
      onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" >

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top">
		<td width="*">
		<%-- MENU PRINCIPAL --%>
		<%
		String MenuPrincipal = "";
		  if(session.getAttribute("MenuPrincipal")!=null)
			MenuPrincipal = (String)session.getAttribute("MenuPrincipal");
		  else if (request.getAttribute("MenuPrincipal")!= null)
			MenuPrincipal= (String)request.getAttribute("MenuPrincipal");
		  out.println(MenuPrincipal);
		%>

	</TR>
</TABLE>
		<%
		String Encabezado = "";
		  if(session.getAttribute("Encabezado")!=null)
			Encabezado = (String)session.getAttribute("Encabezado");
		  else if (request.getAttribute("Encabezado")!= null)
			Encabezado= (String)request.getAttribute("Encabezado");
		  out.println(Encabezado);

		%>


<!--TABLA PRINCIPAL-->
<table width="760" border="0" cellspacing="0" cellpadding="0">
	<form name="operacionrealizada" method="post" >
		<input type="HIDDEN" name="web_application" value =
			<%
				  String web_application = "";
				  if(session.getAttribute("web_application")!=null)
						web_application = (String)session.getAttribute("web_application");
				  else if (request.getAttribute("web_application")!= null)
						web_application= (String)request.getAttribute("web_application");
				  out.println(web_application);
		%> />

		<input type="HIDDEN" name="banco" value =
			<%
				  String banco = "";
				  if (request.getAttribute("banco")!= null)
					banco = (String)request.getAttribute("banco");
				  else if(session.getAttribute("banco")!=null)
					banco = (String)session.getAttribute("banco");
				  out.println(banco);
		%> />



		<input type="HIDDEN" name="operrealizadas" value =
		  <%
			  String operrealizadas = "";
			  if(session.getAttribute("operrealizadas")!=null)
				operrealizadas = (String)session.getAttribute("operrealizadas");
			  else if (request.getAttribute("operrealizadas")!= null)
				operrealizadas= (String)request.getAttribute("operrealizadas");
			  out.println(operrealizadas);
		%> />


		<input type="HIDDEN" name="operprogramadas" value =
		  <%
			  String oprogramada = "";
			  if(session.getAttribute("operprogramadas")!=null)
				oprogramada = (String)session.getAttribute("operprogramadas");
			  else if (request.getAttribute("operprogramadas")!= null)
				oprogramada= (String)request.getAttribute("operprogramadas");
			  out.println(oprogramada);
		%> />


		<input type="HIDDEN" name="opermancomunadas" value =
		  <%
			  String omancomunadas = "";
			  if(session.getAttribute("opermancomunadas")!=null)
				omancomunadas = (String)session.getAttribute("opermancomunadas");
			  else if (request.getAttribute("opermancomunadas")!= null)
				omancomunadas= (String)request.getAttribute("opermancomunadas");
			  out.println(omancomunadas);
		%> />


		<input type="HIDDEN" name="operrechazadas" value =
		  <%
			  String operrechazadas = "";
			  if(session.getAttribute("operrechazadas")!=null)
				operrechazadas = (String)session.getAttribute("operrechazadas");
			  else if (request.getAttribute("operrechazadas")!= null)
				operrechazadas= (String)request.getAttribute("operrechazadas");
			  out.println(operrechazadas);
		%> />

		<input type="HIDDEN" name="contadorprog" value =
			<%
				  String contadorprog = "";
				  if(session.getAttribute("contadorprog")!=null)
					contadorprog = (String)session.getAttribute("contadorprog");
				  else if (request.getAttribute("contadorprog")!= null)
					contadorprog= (String)request.getAttribute("contadorprog");
				  out.println(contadorprog);
		%>>

		<input type="HIDDEN" name="contadormanc" value =
		  <%
			  String contadormanc = "";
			  if(session.getAttribute("contadormanc")!=null)
				contadormanc = (String)session.getAttribute("contadormanc");
			  else
			  if (request.getAttribute("contadormanc")!= null)
				contadormanc= (String)request.getAttribute("contadormanc");
			  out.println(contadormanc);
		%> />

		<input type="HIDDEN" name="contadorerror" value =
		  <%
			  String contadorerror = "";
			  if(session.getAttribute("contadorerror")!=null)
				contadorerror = (String)session.getAttribute("contadorerror");
			  else if (request.getAttribute("contadorerror")!= null)
				contadorerror= (String)request.getAttribute("contadorerror");
			  out.println(contadorerror);
		  %> />

  <tr>
    <td align="center">
		  <%
		  String mensaje_salida = "";
		  if(session.getAttribute("mensaje_salida")!=null)
			mensaje_salida = (String)session.getAttribute("mensaje_salida");
		  else if (request.getAttribute("contadorerror")!= null)
			mensaje_salida= (String)request.getAttribute("mensaje_salida");
		  out.println(mensaje_salida);
		  %>

		  <input type="hidden" name="contadorok" value =
				<%
				String contadorok = "";
				if(session.getAttribute("contadorok")!=null)
				  contadorok = (String)session.getAttribute("contadorok");
				else if (request.getAttribute("contadorok")!= null)
				  contadorok= (String)request.getAttribute("contadorok");
				out.println(contadorok);
				%> />

		  <input type="hidden" name="tramaok" value =
				<%
				String tramaok = "";
				if(session.getAttribute("tramaok")!=null)
				  tramaok = (String)session.getAttribute("tramaok");
				else if (request.getAttribute("tramaok")!= null)
				  tramaok= (String)request.getAttribute("tramaok");
				out.println(tramaok);
				%> />

		<input type="hidden" name="datoscontrato"  value =
				<%
				String datoscontrato = "";
				if(request.getAttribute("datoscontrato")!=null)
				  datoscontrato = (String)request.getAttribute("datoscontrato");
				else if (session.getAttribute("datoscontrato")!= null)
				  datoscontrato= (String)session.getAttribute("datoscontrato");
				out.println(datoscontrato);
				%> />

		<input type="hidden" name="datosusuario"  value =
				<%
				String datosusuario = "";
				if(session.getAttribute("datosusuario")!=null)
				  datosusuario = (String)session.getAttribute("datosusuario");
				else if (request.getAttribute("datosusuario")!= null)
				  datosusuario= (String)request.getAttribute("datosusuario");
				out.println(datosusuario);
				%> />
		<table width="87" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
			<tr>
				  <%
				  String botonexp = "";
				  if(session.getAttribute("botonexp")!=null){
					botonexp = (String)session.getAttribute("botonexp");}
				  else if (request.getAttribute("botonexp")!= null){
				  botonexp = (String)request.getAttribute("botonexp");}
				  out.println(botonexp);


				  	System.out.println("Entro a Nueva Exportacion");
				  	//PYME 2015 INDRA se agrega codigo para exportacion
				  	String af = (String)request.getAttribute("af");
					String metodoExportacion = (String)request.getAttribute("metodoExportacion");
					String em = (String)request.getAttribute("em");
				  %>
					<input type="hidden" name="af" value="<%=af%>"/>
					<input type="hidden" name="metodoExportacion" value="<%=metodoExportacion%>"/>
					<input type="hidden" value="em" name="<%=em%>"/>
				  <%System.out.println("Salio a Nueva Exportacion");
				  %>
		  <td align="left"><a href="javascript:scrImpresion();" border=0><img border=0  src="/gifs/EnlaceMig/gbo25240.gif"></a></td>
		</tr>
	  </table>
	  <br>
	</td>
  </tr>
</form>
</table>
		<!-- CONTENIDO FINAL -->
</body>
</html>
<Script language = "JavaScript">
<!--
function VentanaAyuda(ventana)
{
	hlp=window.open("/EnlaceMig/ayuda.html#" + ventana ,"hlp","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
    hlp.focus();
}
//-->
</Script>