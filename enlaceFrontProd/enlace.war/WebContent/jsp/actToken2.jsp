<%@ page import="mx.altec.enlace.admonContrasena.vo.UtilVO" %>
<%
  java.util.Date dt = new java.util.Date();
  String strDia="";
  String mes="";
  String mifecha="";
  String mihora="";
  String minutos="";
  if(dt.getMinutes()<10)
  	minutos = "0" + dt.getMinutes();
  else
  	minutos = "" + dt.getMinutes();
  switch (dt.getMonth())  {
	case  0: mes="Enero";break;
	case  1: mes="Febrero";break;
	case  2: mes="Marzo";break;
	case  3: mes="Abril";break;
	case  4: mes="Mayo";break;
	case  5: mes="Junio";break;
	case  6: mes="Julio";break;
	case  7: mes="Agosto";break;
	case  8: mes="Septiembre";break;
	case  9: mes="Octubre";break;
	case 10: mes="Noviembre";break;
	case 11: mes="Diciembre";break;
  }
  switch (dt.getDay()) {
	case 0:strDia="Domingo";break;
	case 1:strDia="Lunes";break;
	case 2:strDia="Martes";break;
	case 3:strDia="Miercoles";break;
	case 4:strDia="Jueves";break;
	case 5:strDia="Viernes";break;
	case 6:strDia="Sabado";break;
  }
  mifecha=strDia +" "+ dt.getDate() +" de "+  mes +" del "+(dt.getYear()+1900);
  mihora=dt.getHours()+":"+minutos;
%>
<html>
<head>
<title>Bienvenido a Enlace Internet</title>
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s26050">
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<style type="text/css">
<!--
td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: normal;
	color: #666666;
	text-decoration: none;
}
.titulo {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #FF0000;
	text-decoration: none;
}
.tituloCopy {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #666666;
	text-decoration: none;
}
-->
</style>

<SCRIPT LANGUAGE="JavaScript" src="/EnlaceMig/cuadroDialogo.js"> </SCRIPT>
<script language="JavaScript"><!--

var tiempo = new Date();

function validaTiempo() {
	var tiempoTmp = ((new Date() - tiempo) / 1000);
	if(tiempoTmp < 30) return true;

}
function actualizaTiempo() {
	tiempo = new Date();
}


function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
function FrameAyuda(ventana) {
	hlp = window.open("Ayuda?ClaveAyuda=" + ventana, "hlp", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
	hlp.focus();
}
function terminar() {
	// vswf:meg cambio de NASApp por Enlace 08122008
	document.registro.action="/Enlace/enlaceMig/IndexServlet";
	document.registro.submit();
}
function limpiar() {
	document.getElementById("serie").value="";
	document.getElementById("clave").value="";
}
function validaForma() {
	var expreg=/^[\d]{8,8}$/i;//ocho caracteres, solo digitos
	var expreg2=/^[\d]{8,10}$/i;//ocho a diez caracteres, solo digitos
	var token=document.getElementById("clave").value;
	var serie=document.getElementById("serie").value;
	if (!expreg2.test(serie)){
		cuadroDialogo ("El n&uacute;mero de serie debe ser num&eacute;rico, m&aacute;ximo 10 posiciones sin guiones.",1);
	} else if (!expreg.test(token)){
		cuadroDialogo ("La contrase&ntilde;a debe ser num&eacute;rica.",1);
	} else if (serie.length<8){
		cuadroDialogo ("La longitud del n&uacute;mero de serie es de 8 a 10 d&iacute;gitos.",1);
	} else if (token.length<8){
		cuadroDialogo ("La longitud de la contrase&ntilde;a debe ser de 8 posiciones.",1);
	} else if(!validaTiempo()) {
		document.getElementById("clave").value = '';
		cuadroDialogo ("Su contrase&ntilde;a din&aacute;mica ha expirado, <br>favor de introducir una nueva.",1);
	} else {
		while (serie.length<10) serie="0"+serie;
		registro.serie.value=serie;
		registro.submit();
	}
}
--></script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" background="/gifs/EnlaceMig/gfo25010.gif">
<div id="Layer1" style="position:absolute; left:315px; top:96px; width:316px; height:31px; z-index:1"><img src="/gifs/EnlaceMig/titToken.jpg" width="332" height="40"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="596" valign="top">
      <table width="596" border="0" cellspacing="0" cellpadding="0" class="tabfonbla">
        <tr valign="top">
          <td rowspan="2"><img src="/gifs/EnlaceMig/glo25010.gif" width="237" height="41"></td>
          <td rowspan="2"><a href="javascript:;" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contactanos','','/gifs/EnlaceMig/gbo25111.gif',1)"><img src="/gifs/EnlaceMig/gbo25110.gif" width="30" height="33" name="contactanos" border="0"></a></td>
          <td rowspan="2"><img src="/gifs/EnlaceMig/gbo25120.gif" width="59" height="20" alt="Cont&aacute;ctenos"></td>
          <td rowspan="2"><a href="javascript:;" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('atencion','','/gifs/EnlaceMig/gbo25131.gif',1)"><img src="/gifs/EnlaceMig/gbo25130.gif" width="36" height="33" name="atencion" border="0"></a></td>
          <td rowspan="2"><img src="/gifs/EnlaceMig/gbo25140.gif" width="90" height="20" alt="Atenci&oacute;n telef&oacute;nica"></td>
          <td rowspan="2"><a href="javascript:;" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('centro','','/gifs/EnlaceMig/gbo25151.gif',1)"><img src="/gifs/EnlaceMig/gbo25150.gif" width="33" height="33" name="centro" border="0"></a></td>
          <td rowspan="2"><img src="/gifs/EnlaceMig/gbo25160.gif" width="93" height="20" alt="Centro de mensajes"></td>
          <td width="18" bgcolor="#DE0000"><img src="/gifs/EnlaceMig/gau25010.gif" width="18" height="17"></td>
        </tr>
        <tr valign="top">
          <td><img src="/gifs/EnlaceMig/gau25010.gif" width="18" height="24"></td>
        </tr>
      </table>
      <table border="0" cellspacing="6" cellpadding="0" align="right" class="tabfonbla">
        <tr>
		  <td width="528" valign="top" class="titpag">Activaci&oacute;n de Token</td>
          <td nowrap align="right" valign="bottom" class="texencfec" height="12"><%=mifecha%></td>
        </tr>
        <tr>
		  <td valign="top" class="texencrut">
			<img src="/gifs/EnlaceMig/gic25030.gif" width="7" height="13">&nbsp; Men&uacute; &gt; Activaci&oacute;n de token
		  </td>
          <td class="texenchor" align="right" valign="top" nowrap><%=mihora%></td>
        </tr>
      </table>
    </td>
    <td width="100%" valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
        <tr>
          <td bgcolor="#DE0000"><img src="/gifs/EnlaceMig/gau25010.gif" width="35" height="17"></td>
          <td align="center" rowspan="2"><img src="/gifs/EnlaceMig/glo25020.gif" width="57" height="41" alt=""></td>
          <td width="100%" bgcolor="#DE0000"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="17"></td>
        </tr>
        <tr>
          <td><img src="/gifs/EnlaceMig/gau25010.gif" width="35" height="24"></td>
          <td><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="24"></td>
        </tr>
      </table>
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="right" class="tabfonbla" width="126"><img src="/gifs/EnlaceMig/gba25010.gif" width="126" height="33" alt="Sitio seguro"></td>
          <td align="right" class="tabfonbla" width="50">&nbsp;</td>
        </tr>
		<tr>
          <td align="right"><a href="javascript:FrameAyuda('tk0200');" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('ayuda','','/gifs/EnlaceMig/gbo25171.gif',1)" ><img src="/gifs/EnlaceMig/gbo25170.gif" width="33" height="49" name="ayuda" border="0" alt="Ayuda"></a></td>
          <!-- vswf:meg cambio de NASApp por Enlace 08122008 -->
          <td align="right"><a href="/Enlace/enlaceMig/logout" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('finsesion','','/gifs/EnlaceMig/gbo25181.gif',1)">
<img src="/gifs/EnlaceMig/gbo25180.gif" width="44" height="49" name="finsesion" border="0"></a></td>
        </tr>
	  </table>
    </td>
  </tr>
</table>

<table width="760" border="0" cellspacing="6" cellpadding="0">
<!-- vswf:meg cambio de NASApp por Enlace 08122008 -->
<form name="registro" method="post" action="/Enlace/enlaceMig/LoginServlet?opcion=2&accion=10">
  <input type="hidden" name="accion" value="actToken">
  <input type="hidden" name="origen" value="<%= request.getParameter("origen")%>">
	<tr>
      <td align="center" colspan="3">
	  	<table width="80%" border="0" cellspacing="0" cellpadding="0">
		  <tr valign="top">
			<td width="35%"><strong>Paso 1.</strong> <br>
			  Capture el n&uacute;mero de serie de su token, ubicado en la parte posterior de &eacute;ste.</td>
			<td>&nbsp;</td>
			<td width="30%">&nbsp;</td>
		  </tr>
		</table>
	  </td>
	</tr>
    <tr>
      <td align="center" valign="top" width="194">
       <table width="194" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="17"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="17"></td>
          </tr>
        </table>
        <br>
        <img src="/gifs/EnlaceMig/gau25010.gif" width="158" height="91" name="texto">
      </td>
      <td valign="top" width="282">
        <table width="323" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <table width="500" border="0" cellspacing="10" cellpadding="0">
                <tr>
				   <td width="20%">&nbsp;</td>
				  <td class="tabmovtex11" width="40%">C&oacute;digo del cliente:<br>
                    <input type="text" name="usuarioTxt" SIZE="20" MAXLENGTH="8" value="<%= UtilVO.convierteUsr7a8((String)session.getAttribute("codCliente"))%>" disabled>
                  </td>
				  <td class="texencconbol" align="left" width="40%"><br><%= session.getAttribute("usuarioEnlace")%></td>
                </tr>
                <tr>
				   <td width="20%">&nbsp;</td>
				  <td class="tabmovtex11" width="40%">N&uacute;mero de serie(Capturar sin gui&oacute;n):<br>
                    <INPUT TYPE="text" id="serie" NAME="serie" SIZE="20" MAXLENGTH="10">
                  </td>
				  <td class="texencconbol" width="40%">&nbsp;</td>
                </tr>
                <tr>
				   <td width="20%">&nbsp;</td>
				  <td class="tabmovtex11" width="40%">Contrase&ntilde;a din&aacute;mica:<br>
                    <INPUT TYPE="password" id="clave" NAME="clave" SIZE="20" MAXLENGTH="8" onKeyPress="actualizaTiempo()" AUTOCOMPLETE="off">
                  </td>
				  <td class="texencconbol" width="40%">&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td valign="top" width="260">
		<table width="260" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="17"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="17"></td>
          </tr>
        </table>
		<table border="0" cellspacing="0" cellpadding="5" width="260">
		 <tr>
		   <td><br></td>
		 </tr>
		</table>
      </td>
    </tr>

	<tr>
      <td align="center" colspan="3">
	  	<table width="80%" border="0" cellspacing="0" cellpadding="0">
		  <tr valign="top">
			<td width="30%"><strong>Paso 2.</strong> <br>
			  Presione el bot&oacute;n de su token</td>
			<td>&nbsp;</td>
			<td width="30%"><strong>Paso 3. </strong><br>
			  Capture la contrase&ntilde;a din&aacute;mica que aperece en
			  el Token.</td>
		  </tr>
		</table>
	  </td>
	</tr>

	<tr>
    <td align="center" colspan="3">
	  <table width="500" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="2"><img src="/gifs/EnlaceMig/img001.gif" width="173" height="49"></td>
          <td width="2"><img src="/gifs/EnlaceMig/img4.gif" width="42" height="49"></td>
          <td width="2"><img src="/gifs/EnlaceMig/img7.gif" width="97" height="49"></td>
          <td><img src="/gifs/EnlaceMig/img12.gif" width="205" height="49"></td>
        </tr>
        <tr>
          <td><img src="/gifs/EnlaceMig/img2.gif" width="173" height="56"></td>
          <td><img src="/gifs/EnlaceMig/img5_an.gif" width="42" height="56" border="0"></td>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><img src="/gifs/EnlaceMig/img8.gif" width="97"></td>
              </tr>
              <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="2"><img src="/gifs/EnlaceMig/img9.gif" width="18"></td>
                      <td align="center" bgcolor="#A3B28E"><font color="#000000">12345678</font></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td><img src="/gifs/EnlaceMig/img10.gif" width="97"></td>
              </tr>
            </table></td>
          <td><img src="/gifs/EnlaceMig/img13.gif" width="205" height="56"></td>
        </tr>
        <tr>
          <td><img src="/gifs/EnlaceMig/img3.gif" width="173" height="30"></td>
          <td><img src="/gifs/EnlaceMig/img6.gif" width="42" height="30"></td>
          <td><img src="/gifs/EnlaceMig/img11.gif" width="97" height="30"></td>
          <td><img src="/gifs/EnlaceMig/img14.gif" width="205" height="30"></td>
        </tr>
      </table>
	</td>
	</tr>
	<br>
    <tr>
	  <td class="tabmovtex11" align="center" colspan="3">
		<table border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td>
			<a href="javascript:validaForma();"><img border="0" name="imageField3" src="/gifs/EnlaceMig/gbo25520.gif" alt="Enviar">
	        </td>
	        <td>
			<a href="javascript:limpiar();"><img border="0" name="imageField" src="/gifs/EnlaceMig/gbo25250.gif" alt="Limpiar">
			</td>
			<% if(request.getParameter("origen")==null || !request.getParameter("origen").equals("login")) {%>
	        <td>
			<a href="javascript:terminar();"><img border="0" name="imageField" src="/gifs/EnlaceMig/Terminar.gif" alt="Terminar">
			</td>
			<%}%>
          </tr>
        </table>
       </td>
    </tr>
  </form>
</table>
<script>
document.registro.usuarioTxt.focus();
<%= request.getAttribute("mensajeError") %>
</script>
</body>
</html>