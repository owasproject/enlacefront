
<%@ page import="java.util.*"%>
<jsp:useBean id='ls_prov' class='java.util.ArrayList' scope='request'/>
<jsp:useBean id='Error' class='java.lang.String' scope='request'/>

<%
		Calendar fecha = new GregorianCalendar();
		String[] meses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
		String mes1, mes2, mes3;
                String vmes1, vmes2, vmes3;
		int anio = 0;
		if (fecha.get(Calendar.MONTH) == 0) {
			mes1 = meses[10];
			mes2 = meses[11];
			mes3 = meses[0];
                        vmes1 = "10";
                        vmes2 = "11";
                        vmes3 = "0";
                } else if (fecha.get(Calendar.MONTH) == 1) {
			mes1 = meses[11];
			mes2 = meses[0];
			mes3 = meses[1];
                        vmes1 = "11";
                        vmes2 = "0";
                        vmes3 = "1";
                } else {
			mes1 = meses[fecha.get(Calendar.MONTH)-2];
			mes2 = meses[fecha.get(Calendar.MONTH)-1];
			mes3 = meses[fecha.get(Calendar.MONTH)];
                        vmes1 = (fecha.get(Calendar.MONTH) - 2) + "";
                        vmes2 = (fecha.get(Calendar.MONTH) - 1) + "";
                        vmes3 = (fecha.get(Calendar.MONTH)) + "";
                }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page import="mx.altec.enlace.bo.confProveedor"%>
<HTML>
<HEAD>
<TITLE> Enlace Confirming - Gr&aacute;fica </TITLE>
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="">
<META NAME="Keywords" CONTENT="">
<META NAME="Description" CONTENT="">
</HEAD>


<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<script language="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language = "JavaScript" SRC="/EnlaceMig/cuadroDialogo.js"> </SCRIPT>

<SCRIPT LANGUAGE="JAVASCRIPT">

/******************  Esto no es mio ***************************************************/



function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}



function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}



function MM_findObj(n, d) { //v3.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;

}



function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}



/************************************************************************************/


function valida_vProveedor () {
	if (document.Graficar.vProveedor[0].checked)
		document.Graficar.vTodosProveedor.selectedIndex = 0;
}

function valida_vTodosProveedor () {
	var index_vTodosProveedor = document.Graficar.vTodosProveedor.selectedIndex;
	if (index_vTodosProveedor != 0) {
		document.Graficar.vProveedor[0].checked = false;
		document.Graficar.vProveedor[1].checked = true; }
	else
		cuadroDialogo("Debe de seleccionar a un proveedor.",3);
}

function valida_De () {
	<% Calendar fecha2 = new GregorianCalendar(); %>
	if (document.Graficar.vPeriodoDe[1].text == "Diciembre" && document.Graficar.vPeriodoDe.selectedIndex == 1)
		document.Graficar.vDE.value = <%=fecha2.get(Calendar.YEAR) - 1 %> ;
	else if ( (document.Graficar.vPeriodoDe[1].text == "Noviembre" ) && (document.Graficar.vPeriodoDe.selectedIndex == 1 || document.Graficar.vPeriodoDe.selectedIndex == 2) )
		document.Graficar.vDE.value = <%=fecha2.get(Calendar.YEAR) - 1 %> ;
	else
		document.Graficar.vDE.value = <%=fecha2.get(Calendar.YEAR) %>
	if (document.Graficar.vPeriodoDe.selectedIndex == 0)
		cuadroDialogo("Debe de seleccionar una fecha inicial", 3);
}

function valida_A() {
	<% Calendar fecha3 = new GregorianCalendar(); %>
	if (document.Graficar.vPeriodoA[1].text == "Diciembre" && document.Graficar.vPeriodoA.selectedIndex == 1)
		document.Graficar.vA.value = <%=fecha3.get(Calendar.YEAR) - 1 %> ;
	else if ( (document.Graficar.vPeriodoA[1].text == "Noviembre" ) && (document.Graficar.vPeriodoA.selectedIndex == 1 || document.Graficar.vPeriodoA.selectedIndex == 2) )
		document.Graficar.vA.value = <%=fecha3.get(Calendar.YEAR) - 1 %> ;
	else
		document.Graficar.vA.value = <%=fecha3.get(Calendar.YEAR) %> ;
	if (document.Graficar.vPeriodoA.selectedIndex == 0)
		cuadroDialogo("Debe de seleccionar una fecha final", 3);
}

function valida_Anio(opcion) {
	if (opcion == 1)
		valida_De();
	else
		valida_A();
	if ( document.Graficar.vPeriodoDe.selectedIndex > document.Graficar.vPeriodoA.selectedIndex)
		cuadroDialogo("La fecha del final debe ser mayor o igual a la de inicio", 3);
}

function genera_fechas () {
    document.Graficar.fechaIni.value = '01/' + document.Graficar.vPeriodoDe.value +
        '/' + document.Graficar.vDE.value;
    document.Graficar.fechaLim.value = <%=fecha.get (Calendar.DAY_OF_MONTH)%> + '/' + document.Graficar.vPeriodoA.value +
        '/' + document.Graficar.vA.value;
    document.Graficar.submit ();
}

<%= request.getAttribute("newMenu") %>

</SCRIPT>

<BODY topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"

onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');<%if (!Error.equals("")) out.println("cuadroDialogo('" + Error + "', 1)");%>"

background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">

  <tr valign="top">

    <td width="*">

       <!-- MENU PRINCIPAL -->

       <%= request.getAttribute("MenuPrincipal") %></TD>

  </TR>

</TABLE>



<%= request.getAttribute("Encabezado" ) %>

<CENTER class="titpag" >
<TABLE WIDTH="430" BORDER="0" CELLSPACING="0" CELLPADDING="0" >

<FORM NAME="Graficar" method='POST' action='confGrafica'>

 <tr>
   <td align="center">
     <table width="420" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td rowspan="2">

           <table width="410" border="0" cellspacing="2" cellpadding="3">

		   <tr>
	         <td class="tittabdat" colspan="2"> Proveedor:</td>
		    </tr>

			<tr align="center">
               <td class="textabdatcla" valign="top" colspan="2">
                 <table width="400" border="0" cellspacing="0" cellpadding="0">

   <TR>
    <TD class="tabmovtexbol" nowrap align="LEFT">
		<INPUT TYPE="RADIO" NAME="vProveedor" VALUE="Todos" onClick="valida_vProveedor();" checked>Todos
	</TD>
   </TR>

   <TR>
    <TD class="tabmovtexbol" nowrap align="LEFT">
		<INPUT TYPE="RADIO" NAME="vProveedor" VALUE="Proveedor" onClick="valida_vProveedor();">Proveedor

		<SELECT name="vTodosProveedor" onChange="valida_vTodosProveedor();">
		<OPTION VALUE="0">
        <%
                    java.util.ListIterator liProveedores = ls_prov.listIterator ();
                    while (liProveedores.hasNext ()) {
                        confProveedor prov = (confProveedor) liProveedores.next ();
        %>
			<OPTION VALUE="<%=prov.getClaveProveedor ()%>"><%=prov.getNombreProveedor ()%>
        <%
            }
        %>
		</SELECT>
	</TD>
   </TR>

 </TABLE>
	</td>
  </tr>


			<tr>
			    <td class="tittabdat" colspan="2"> Per&iacute;odo: </td>
		    </tr>

			<tr align="center">
               <td class="textabdatcla" valign="top" colspan="2">
                 <table width="400" border="0" cellspacing="0" cellpadding="0">
<TR>
    <TD class="tabmovtexbol" nowrap align="CENTER">
		De:
		<SELECT NAME="vPeriodoDe" onChange ="valida_Anio(1);">
			<OPTION VALUE="-1">
			<OPTION VALUE="<%=vmes1%>"><%= mes1%>
			<OPTION VALUE="<%=vmes2%>"><%= mes2%>
			<OPTION VALUE="<%=vmes3%>"><%= mes3%>
		</SELECT>

		<INPUT TYPE="TEXT" NAME="vDE" SIZE="5" VALUE="<%= fecha.get(Calendar.YEAR) %>" onFocus="blur();">
		A:
		<SELECT NAME="vPeriodoA" onChange ="valida_Anio(2);">
			<OPTION VALUE="0">
			<OPTION VALUE="<%=vmes1%>"><%= mes1%>
			<OPTION VALUE="<%=vmes2%>"><%= mes2%>
			<OPTION VALUE="<%=vmes3%>"><%= mes3%>
		</SELECT>
		<INPUT TYPE="TEXT" NAME="vA" SIZE="5" VALUE="<%= fecha.get(Calendar.YEAR) %>" onFocus="blur();">
	</TD>
</TR>

 </TABLE>
	</td>
  </tr>

			<tr>
			    <td class="tittabdat" colspan="2"> Graficar por: </td>
		    </tr>

			<tr align="center">
               <td class="textabdatcla" valign="top" colspan="2">
                 <table width="400" border="0" cellspacing="0" cellpadding="0">
<TR>
    <TD class="tabmovtexbol" nowrap align="LEFT">
		<INPUT TYPE="RADIO" NAME="vGraficar" value='N'>N&uacute;mero de documentos
	</TD>
</TR>
<TR>
    <TD class="tabmovtexbol" nowrap align="LEFT">
		<INPUT TYPE="RADIO" NAME="vGraficar" checked value='I'>Importe de los documentos
	</TD>
</TR>

 </TABLE>

	</td>
  </tr>
	<tr align="center">
       <td valign="top" colspan="2">
          <table width="400" border="0" cellspacing="0" cellpadding="0">

		  <tr>
        <td align="center" valign="middle" width="66">
            <A href = "javascript:genera_fechas();" border=0><img src="/gifs/EnlaceMig/gbo25280.gif" border=0 alt="Aceptar" width="66" height="22"></a>
        </td>
        <td align="center" valign="middle" width="66">
            <A href = "javascript:history.back();" border=0><img src="/gifs/EnlaceMig/gbo25320.gif" border=0 alt="Regresar" width="77" height="22"></a>
        </td>
		  </tr>

		  </table>
		</td>
	</tr>

 </TABLE>

	</td>
  </tr>
 </TABLE>

	</td>
  </tr>
  <INPUT type='hidden' name='Opcion' value='1'>
  <INPUT type='hidden' name='fechaIni' value=''>
  <INPUT type='hidden' name='fechaLim' value=''>

</FORM>

</TABLE>

</CENTER>



</BODY>

</HTML>
