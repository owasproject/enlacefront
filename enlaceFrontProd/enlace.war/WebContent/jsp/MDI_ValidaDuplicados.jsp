<html>
<head>
<title>Deposito Interbancario</title>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" type="text/javascript" SRC= "/EnlaceMig/passmark.js"></script>
<script type="text/javascript">


 function js_enviar()
 {
 	/* CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token
 	 * ahora se enviara al servlet MDI_Enviar para que primero valide
 	 * el token y despues de ahi se redirige al servlet MDI_Cotizar*/
// 	document.tabla.action="MDI_Cotizar";
	document.tabla.action="MDI_Enviar";
    document.tabla.submit();
 }

function ValidaDuplicados()
 {
   if(ValidaTabla(document.tabla))
   {
      document.tabla.action="MDI_Interbancario";
      document.tabla.submit();
   }
 }


function ValidaTabla(forma)
 {
   var cont=0;
   var str="";

   if(forma.TransArch.value.length<=1)
    {
      for(i2=0;i2<forma.length;i2++)
       {
         if(forma.elements[i2].type=='checkbox')
          {
            if(forma.elements[i2].checked==true)
             {
               str+="1";
               cont++;
             }
            else
              str+="0";
          }
       }
      if(cont==0)
       {
         cuadroDialogo("Debe Seleccionar m&iacute;nimo una transferencia.",3);
         return false;
       }
      forma.strCheck.value=str;
    }
   return true;
 }

function Regresar()
 {
	document.tabla.action="MDI_Interbancario?Modulo=0";
	document.tabla.submit();
 }

function cuadroDialogoMedidas( mensaje, tipo, ancho, alto)
{
	respuesta=0;
	var titulo="Error";
	var imagen="gic25020.gif";
	var param;

	if ( tipo == 1 )
	{
		imagen="gic25020.gif";
		titulo="Alerta";
	}
	else if ( tipo== 2 )
	{
		imagen ="gic25050.gif";
		titulo ="Confirmaci&oacute;n";
	}
	else if ( tipo== 5)
	{
		imagen ="gic25050.gif";
		titulo ="Confirmaci&oacute;n";
	}
	else if ( tipo== 6)
	{
		imagen ="gic25050.gif";
		titulo ="Confirmaci&oacute;n";
	}
	else if ( tipo== 7)
	{
		imagen ="gic25020.gif";
		titulo ="AVISO";
	}
	else if ( tipo== 8)
	{
		imagen ="gic25020.gif";
		titulo ="Obten Data";
	}
	else
	{
		imagen="gic25060.gif";
		titulo="Error";
	}

	param= "width=" + ancho + ",height=" + alto + ",toolbar=no,scrollbars=no,left=210,top=225";

	ventana=window.open('','trainerWindow',param);
	ventana.document.open();
	ventana.document.write("<html>");
	ventana.document.writeln("<head>\n<title>"+titulo+"</title>\n");
	ventana.document.writeln("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
	ventana.document.writeln("</head>");
	ventana.document.writeln("<body bgcolor='white'>");
	ventana.document.writeln("<form>");
	ventana.document.writeln("<table border=0 width=365 class='textabdatcla' align=center cellspacing=3 cellpadding=0>");
	ventana.document.writeln("<tr><th height=25 class='tittabdat' colspan=2>"+ titulo + "</th></tr>");
	ventana.document.writeln("</table>");
	ventana.document.writeln("<table border=0 width=365 class='textabdatcla' align=center cellspacing=0 cellpadding=1>");
	ventana.document.writeln("<tr><td class='tabmovtex1' align=right width=50><img src='/gifs/EnlaceMig/"+imagen+"' border=0></a></td>");
	ventana.document.writeln("<td class='tabmovtex1' align=center width=300><br>"+mensaje+"<br><br>");
	ventana.document.writeln("</tr></td>");
	ventana.document.writeln("<tr><td class='tabmovtex1'>");
	ventana.document.writeln("</td></tr>");
	ventana.document.writeln("</table>");
	ventana.document.writeln("<table border=0 align=center cellspacing=6 cellpadding=0>");
	if (tipo == 2)
	{
		ventana.document.writeln("<tr><td align=right><a href='javascript:opener.ventanaActiva=0;opener.respuesta=1;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=2;opener.continua();window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a></td></tr>");
	}
	else if (tipo == 5)
	{
		ventana.document.writeln("<tr><td align=right><a href='javascript:opener.ventanaActiva=0;opener.respuesta=1;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=2;opener.continua();window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
	}
	else if (tipo == 6)
	{
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=2;window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
	}else if (tipo == 7)
	{
		ventana.document.writeln("<tr><td align=right><a href='javascript:opener.ventanaActiva=0;opener.respuesta=8;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=7;opener.continua();window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a></td></tr>");
	}else if (tipo == 8)
	{
		ventana.document.writeln("<tr><td align=right><a href='javascript:opener.ventanaActiva=0;opener.respuesta=9;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=10;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a></td></tr>");
	}
	else if (tipo == 9)
	{
		ventana.document.writeln("<tr><td align=right><a href='javascript:opener.ventanaActiva=0;opener.respuesta=11;window.close();opener.continua();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a></td>");
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=12;opener.continua();window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a></td></tr>");
	}
	else
	{
		ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=0;window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
	}
	ventana.document.writeln("</table>");
	ventana.document.writeln("</form>");
	ventana.document.writeln("</body>\n</html>");
	ventana.focus();
}

/***************  Esto no es mio ****************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/****************************************************************************/
<%
	if(	session.getAttribute("newMenu") !=null)
		out.println( session.getAttribute("newMenu"));
	else
		if (request.getAttribute("newMenu")!= null)
			out.println(request.getAttribute("newMenu"));
%>

</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>

<%--  OPC:PASSMARK 16/12/2006 ******************* BEGIN --%>
<%
	String FSOnuevo = (String)request.getAttribute("_fsonuevoMID");
	if( FSOnuevo!=null ) {
%>

	<SCRIPT type="text/javascript">
	  var expiredays = 365;
  	  var ExpireDate = new Date ();
      ExpireDate.setTime(ExpireDate.getTime() + (expiredays * 24 * 3600 * 1000));
      if (!(num=GetCookie("PMDATA"))) { }
	  DeleteCookie("PMDATA");
      SetCookie("PMDATA", "<%=FSOnuevo%>", ExpireDate,"/");
	</SCRIPT>
<%
	 request.removeAttribute("_fsonuevoMID");
	}
%>
<%--  OPC:PASSMARK 16/12/2006 ******************* END --%>

</head>

<body background="/gifs/EnlaceMig/gfo25010.gif"
	style=" bgcolor: #ffffff;" leftMargin=0 topMargin=0 marginwidth="0" marginheight="0" onLoad='<%= request.getAttribute("ArchivoErr") %>;'>


<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
    <%-- MENU PRINCIPAL --%>
	<%
			if (request.getAttribute("MenuPrincipal")!= null)
				out.println(request.getAttribute("MenuPrincipal"));
	%>
   </td>
  </tr>
</table>

	<%
			if (request.getAttribute("Encabezado")!= null)
				out.println(request.getAttribute("Encabezado"));
	%>

<SCRIPT type="text/javascript">
<%
	if (request.getAttribute("InfoUser")!= null) {
		out.println(request.getAttribute("InfoUser"));
	}
%>
</SCRIPT>


 <FORM  NAME="tabla" method=post onSubmit="return ValidaTabla(this);" action="MDI_Cotizar">
 <table width=720>
  <tr>
   <td>

	  <table border=0 align=center>
	   <tr>
		<td align=right class='textabref'>&nbsp;</td>
	   </tr>

	   <tr>
		 <td><% if(request.getAttribute("Tabla")!=null) out.print(request.getAttribute("Tabla"));%></td>
	   </tr>

	   <tr>
		 <td class='textabref' valign=center>
		 <% if(request.getAttribute("Mensaje")!=null) out.println(request.getAttribute("Mensaje"));%></td>
	   </tr>

	   <tr>
	     <td><br></td>
	   </tr>

	   <% if (request.getAttribute("Tabla") != null) { %>
	   <tr>
		<td class="textabref" align=center><a href="javascript:js_enviar();"><img alt='Cotizar/Transferir' src="/gifs/EnlaceMig/gbo25390.gif" style=" border: 0;" name="boton2"></a><a href="javascript:Regresar();"><img src="/gifs/EnlaceMig/gbo25320.gif" border="0" name="boton" alt="Regresar"></a>  <a href=<%= request.getAttribute("DescargaArchivo") %> ><img src="/gifs/EnlaceMig/gbo25230.gif" style=" border: 0;" alt = "Exportar" /></a></td>
	   </tr>

	   <% } else { %>
	   <tr>
		<td class="textabref" align=center><a href="javascript:js_enviar();"><img alt='Cotizar/Transferir' src="/gifs/EnlaceMig/gbo25390.gif" style=" border: 0;" name="boton2"></a><a href="javascript:Regresar();"><img src="/gifs/EnlaceMig/gbo25320.gif" style=" border: 0;" name="boton" alt="Regresar"/></a> </td>
	   </tr>
	   <% } %>
	  </table>

   </td>
  </tr>
 </table>

   <input type="hidden" name="strCheck" value="<%= request.getAttribute("strCheck") %>"/>
   <input type="hidden" name="Lineas" value="<%= request.getAttribute("Lineas") %>"/>
   <input type="hidden" name="Importe" value="<%= request.getAttribute("Importe") %>"/>
   <input type="hidden" name="Modulotabla" value="<%= request.getAttribute("ModuloTabla") %>"/>
   <input type="hidden" name="TransReg" value="<%= request.getAttribute("TransReg") %>"/>
   <input type="hidden" name="TransNoReg" value="<%= request.getAttribute("TransNoReg") %>"/>
   <input type="hidden" name="TransArch" value="<%= request.getAttribute("TransArch") %>"/>
   <input type="hidden" name="archivoEstatus" value="<%= request.getAttribute("archivoEstatus") %>"/>
   <input type="hidden" name="comprobante" value="<%=request.getAttribute("comprobante") %>"/>
   <input type="hidden" name="Registradas" value="<%=request.getAttribute("Registradas") %>"/>
   <input type="hidden" name="Modulo" value="1"/>
 </form>

</body>
</html>
<%-- 2007.01 --%>