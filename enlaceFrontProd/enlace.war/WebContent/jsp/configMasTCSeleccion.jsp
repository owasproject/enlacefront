<%-- 
  - Autor: FSW INDRA
  - Aplicaci�n: ENLACE
  - M�dulo: Paperless MAsivo
  - Fecha: 26/02/2015
  - Descripci�n: Configuracion paperless masivo
  --%>

<%@ page import="java.util.List, javax.servlet.http.HttpServletRequest" %>
<%@ page import="java.util.ArrayList, mx.altec.enlace.beans.ConfEdosCtaArchivoBean" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	
<title>Selecci&oacute;n de Tarjetas de  Credito</title>

<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/configMasTCSeleccion.js"></script>
<link href="/EnlaceMig/consultas.css" rel="stylesheet" type="text/css" id = "estilos"/>

<script type="text/javascript">
<%if(request.getAttribute("TiempoTerminado")!=null) out.print(request.getAttribute("TiempoTerminado"));%>


<%
       if (request.getAttribute("newMenu")!= null) {
       out.println(request.getAttribute("newMenu"));
       }
%>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>

<body leftMargin="0" topMargin="0" marginwidth="0" marginheight="0" onLoad="">

<table border="0px" cellpadding="0px" cellspacing="0px" width="571px">
  <tr valign="top">
   <td width="*">
   		<c:if test="${not empty requestScope.MenuPrincipal}">
			${requestScope.MenuPrincipal}
		</c:if>
   </td>
  </tr>
</table>
	<c:if test="${not empty requestScope.Encabezado}">
		${requestScope.Encabezado}
	</c:if>
		

<form  name="formulario" id="formulario" method="post" action="ConfigMasPorTCServlet">
	<input type="hidden" id="accion" name="accion" value=""/>
	<input type="hidden" id="cadenaCuenta" name="cadenaCuenta" value=""/>
	  <p/>
	  	<table width="760" cellspacing="1" cellpadding="1" border="0" class="tabfonbla" style="font-weight:bold;">
	  		<thead class="tittabdat">
	  			<tr>
	  			<td>Seleccione las tarjetas para las que desea modificar el estatus de env&iacute;o</td>
	  			</tr>
	  		</thead>
			<tbody class="textabdatcla">
				<tr>
					<td align="center">
						<table border="0px" cellspacing="0px" cellpadding="3px" class="textabdatcla" width="450px">
							<tr>
								<td>
					    			<table border="0" width="400px" height="100px" >
					    				<tr>
											<td colspan="1" align="center"> 
												<select id="cuentas" name="cuentas" multiple="multiple" size="10" style="width:300px;">
												
												<c:if test="${not empty  listaCuentasEdoCta}">
													<c:forEach items="${listaCuentasEdoCta}" var="cuenta">
														<option value="<c:out value="${cuenta.nomCuenta}"/> ">
															<c:out value="${cuenta.nomCuenta}"/> <c:out value="${cuenta.nombreTitular}"/>
															
														</option>
													</c:forEach>
												</c:if>
												<c:if test="${empty  listaCuentasEdoCta}">
													<script type="text/javascript">
													 		cuadroDialogo("No existen tarjeas asociadas para modificar el estatus de env&iacute;o del estado de cuenta impreso",4);
													 	</script>
												</c:if>
													
												</select>
											</td>
											<td>
												<input type="button" style="width:40px;" id="agregarT" name="agregarT" value="&gt;&gt;" onclick="SelectMoveAll(document.formulario.cuentas,document.formulario.configurar)"/><br/>
												<input type="button" style="width:40px;" id="agregar" name="agregar" value="&gt;" onclick="SelectMoveRows(document.formulario.cuentas,document.formulario.configurar)"/><br/>
												<input type="button" style="width:40px;" id="quitar" name="quitar" value="&lt;" onclick="SelectMoveRows(document.formulario.configurar,document.formulario.cuentas)"/><br/>
												<input type="button" style="width:40px;" id="quitarT" name="agregarT" value="&lt;&lt;" onclick="SelectMoveAll(document.formulario.configurar,document.formulario.cuentas)"/><br/>
												
											</td>
											
											<td colspan="1" align="center"> 
												<select id="configurar" name="configurar" multiple="multiple" size="10" style="width: 300px">
													
												</select>
											</td>
										</tr>
					    			</table>
					    		</td>
					    	</tr>
						</table>
	
						  <br/>
						  <table border="0px" cellpadding="0px" cellspacing="0px" >
						   <tr>
						     <td>
						     	<a href="javascript:EnviarForma();"><img src="/gifs/EnlaceMig/gbo25280.gif" style=" border: 0;"/></a>
						     	<input type="hidden" id="formato" name="formato" value="1" />
						     </td>	
						   </tr>
						  </table>
					</td>
				</tr>
			</tbody>
		</table>
		<br/>
		<br/>
		<c:if test="${not empty  folio}">
		<script type="text/javascript">
			cuadroDialogo("Se ha generado el n&uacute;mero de folio <%=request.getAttribute("folio")%>.\n Utilice este n&uacute;mero para consultar el estatus de la modificaci&oacute;n en el modulo de consulta.",1);
		</script>

	</c:if>
</form>

</body>
</html>