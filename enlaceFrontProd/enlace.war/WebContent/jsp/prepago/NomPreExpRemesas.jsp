<%@ page import="mx.altec.enlace.beans.*,java.util.Iterator"%>
<%NomPreLM1A lm1a = (NomPreLM1A) request.getAttribute("lm1a");
if (lm1a != null) {%>
<html>
	<head>
		<title>Consulta de remesas</title>
	</head>
	<body>
		<table border="0">
			<tr>
				<td>
					<br>
					<table border="1">
						<tr>
							<td align="center"><b>Total de Remesas</b></td>
							<td align="center"><b><%=lm1a.getDetalle().size()%></b></td>
						</tr>
						<tr>
							<td></td><td></td>
						</tr>
						<tr>
							<td align="center"><b>N&uacute;mero de remesa</b></td>
							<td align="center"><b>Estatus</b></td>
						</tr>
						<%Iterator it = lm1a.getDetalle().iterator();
						while (it.hasNext()) {
							NomPreRemesa remesa = (NomPreRemesa) it.next();
						%>						
						<tr>
							<td align="center">
								&nbsp;<%=remesa.getNoRemesa()%>
							</td>
							<td align="center">
								<%=remesa.getDescEstadoRemesa()%>
							</td>
						</tr>
						<%}%>
					</table>
				</td>
			</tr>
		</table>
		<br>
	</body>
</html>
<%}%>