<html>
	<head>
		<title>Banca Virtual</TITLE>
		<meta http-equiv="Content-Type" content="text/html;" >
		<meta http-equiv="pragma" content="no-cache">
		<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
		<script type="text/javascript" language="javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
		<script type="text/javascript" language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
		<script type="text/javascript" language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>		
		<script type="text/javascript" language = "JavaScript" SRC= "/EnlaceMig/Bitacora.js"></script>

		<script type="text/javascript" language="JavaScript1.2">
			
			var defaultEmptyOK = false;
			
			function archivoErrores() {
			   ventanaInfo1=window.open('','trainerWindow','width=450,height=300,toolbar=no,scrollbars=yes');
			   ventanaInfo1.document.open();
			   ventanaInfo1.document.write("<html>");
			   ventanaInfo1.document.writeln("<head>\n<title>Estatus</title>\n");
			   ventanaInfo1.document.writeln("<link rel='stylesheet' href='../EnlaceMig/consultas.css' type='text/css'>");
			   ventanaInfo1.document.writeln("</head>");
			   ventanaInfo1.document.writeln("<body bgcolor='white'>");
			   ventanaInfo1.document.writeln("<form>");
			   ventanaInfo1.document.writeln("<table border=0 width=420 class='textabdatcla' align=center>");
			   ventanaInfo1.document.writeln("<tr><th class='tittabdat'>informacion del Archivo Importado</th></tr>");
			   ventanaInfo1.document.writeln("<tr><td class='tabmovtex1' align=center>");
			   ventanaInfo1.document.writeln("<table border=0 align=center><tr><td class='tabmovtex1' align=center width=>");
			   ventanaInfo1.document.writeln("<img src='../gifs/EnlaceMig/gic25060.gif'></td>");
			   ventanaInfo1.document.writeln("<td class='tabmovtex1' align='center'><br>No se llevo a cabo la importaci&oacute;n ya que el archivo seleccionado tiene los siguientes errores.<br><br>");
			   ventanaInfo1.document.writeln("</td></tr></table>");
			   ventanaInfo1.document.writeln("</td></tr>");
			   ventanaInfo1.document.writeln("<tr><td class='textabdatobs'>");
			   ventanaInfo1.document.writeln(document.MantNomina.archivoEstatus.value+"<br><br>");
			   ventanaInfo1.document.writeln("</td></tr>");
			   ventanaInfo1.document.writeln("</table>");
			   ventanaInfo1.document.writeln("<table border=0 align=center >");
			   ventanaInfo1.document.writeln("<tr><td align=center><br><a href='javascript:window.close();'><img src='../gifs/EnlaceMig/gbo25200.gif' border=0 alt='Cerrar'></a>");
			   ventanaInfo1.document.writeln("<a href = 'javascript:window.print();'><img border='0' name='imageField3' src='../gifs/EnlaceMig/gbo25240.gif' width='83' height='22' alt='Imprimir'></a></td></tr>");
			   ventanaInfo1.document.writeln("</table>");
			   ventanaInfo1.document.writeln("</form>");
			   ventanaInfo1.document.writeln("</body>\n</html>");
			   ventanaInfo1.document.close();
			   ventanaInfo1.focus();
			}									
			
			function MM_findObj(n, d) { //v4.01
				var p,i,x;  
				if(!d) 
					d=document; 
				if((p=n.indexOf("?"))>0&&parent.frames.length) {
					d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);
				}
				if(!(x=d[n])&&d.all) 
					x=d.all[n]; 
				for(i=0;!x&&i<d.forms.length;i++) 
					x=d.forms[i][n];
				for(i=0;!x&&d.layers&&i<d.layers.length;i++) 
					x=MM_findObj(n,d.layers[i].document);
				if(!x && d.getElementById) 
					x=d.getElementById(n); 
				return x;
			}
	
			function MM_preloadImages() { //v3.0
				var d=document; 
				if(d.images) { 
					if(!d.MM_p) 
						d.MM_p=new Array();
					var i,j=d.MM_p.length,a=MM_preloadImages.arguments; 
					for(i=0; i<a.length; i++)
	    				if (a[i].indexOf("#")!=0) { 
	    					d.MM_p[j]=new Image; 
	    					d.MM_p[j++].src=a[i];
	    				}
	    		}
			}
	
			function MM_swapImgRestore() { //v3.0
				var i,x,a=document.MM_sr; 
				for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) 
					x.src=x.oSrc;
			}
	
			function MM_swapImage() { //v3.0
				var i,j=0,x,a=MM_swapImage.arguments; 
				document.MM_sr=new Array; 
				for(i=0;i<(a.length-2);i+=3)
					if ((x=MM_findObj(a[i]))!=null) {
						document.MM_sr[j++]=x; 
						if(!x.oSrc) 
							x.oSrc=x.src; 
						x.src=a[i+2];
					}
			}	
			
			//Repetida
			function PresentarCuentas() {
  				msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  				msg.focus();
			}		
			
			//repetida
			function echeck(str) {
				var at="@";
				var dot=".";
				var lat=str.indexOf(at);
				var lstr=str.length;
				var ldot=str.indexOf(dot);
				
				if (str.indexOf(at)==-1)
					return false;
				if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr)
					return false;
				if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr)
					return false;
				if (str.indexOf(at,(lat+1))!=-1)
					return false;
				if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot)
					return false;
				if (str.indexOf(dot,(lat+2))==-1)
					return false;
				if (str.indexOf(" ")!=-1)
					return false;
				return true;
			}
			
			//repetido
			function validaEmail() {
				document.MantNomina.CorreoE.value=trimString(document.MantNomina.CorreoE.value);
			  	var emailID=document.MantNomina.CorreoE;
			
			  	if( trimString(emailID.value)!="") {
					if (echeck(emailID.value)==false) {
						emailID.focus();
						cuadroDialogo("El correo especificado no es v&aacute;lido.", 3);
						return false;
				  	}
			  	} else {
					cuadroDialogo( "Es necesario incluir el correo para env&iacute;o de detalle" ,3);
			     	return false;
			  	}
			  	return true;
			}
			
			//repetido
			function FrmClean() {
				document.MantNomina.textcuenta.value = "";
				document.MantNomina.fecha1.value	= "";
				document.MantNomina.fecha2.value	= "";
				document.MantNomina.Importe.value	= "";
				document.MantNomina.secuencia.value      = "";
				document.MantNomina.ChkRecibido.checked  = "";
				document.MantNomina.ChkCancelado.checked = "";
				document.MantNomina.ChkEnviado.checked   = "";
				document.MantNomina.ChkPagado.checked    = "";
				document.MantNomina.ChkCargado.checked   = "";
			}
			
			//repetido
			function checkFloatValueCon(myobj) {
				if(myobj != "") {
			    	if(!isFloatCon(myobj)) {
			      		myobj = "";
			      		document.MantNomina.Importe.focus();
			      		cuadroDialogo("El Importe debe ser num&eacute;rico",3);
			      		return false;
			    	}
			  	}
			  	return true;
			}
			
			function isFloatCon (s) {   
				var i;
			    var seenDecimalPoint = false;
			    if (isEmptyCon(s))
			       if (isFloat.arguments.length == 1) 
			       		return defaultEmptyOK;
			       else 
			       		return (isFloat.arguments[1] == true);
			    if (s == ".") 
			    	return false;
			    for (i = 0; i < s.length; i++) {
			       	// Check that current character is number.
			       	var c = s.charAt(i);
			       	if ((c == ".") && !seenDecimalPoint) 	
			       		seenDecimalPoint = true;
			       	else 
			       		if (!isDigit(c)) 
			       			return false;
			    }
			    return true;
			}
			
			function isEmptyCon(s) {
				return ((s == null) || (s.length == 0));
			}
			
			function checkValueCon(myobj) {
				if(myobj != "") {
			    	if(!isPositiveInteger(myobj)) {
			      		myobj = "";
				  		cuadroDialogo("El n&uacute;mero de registros debe ser num&eacute;rico",3);
				  		return false;
			    	}
			  	}
			  	return true;
			}
			
			function checkValueCon3(myobj) {
				if(myobj != "") {
			    	if(!isPositiveInteger(myobj)) {
			      		myobj = "";
			      		document.MantNomina.secuencia.focus();
				  		cuadroDialogo("El n&uacute;mero de secuencia debe ser num&eacute;rico",3);
				  		return false;
			    	}
			  	}
			  	return true;
			}
			
			function isPositiveInteger (s) {   
				var secondArg = defaultEmptyOK;
				if (isPositiveInteger.arguments.length > 1)
			        secondArg = isPositiveInteger.arguments[1];
			    return (isSignedInteger(s, secondArg) && ((isEmpty(s) && secondArg)  || (parseInt (s) > 0) ) );
			}
			
			function isSignedInteger (s) {
				if (isEmpty(s))
			       if (isSignedInteger.arguments.length == 1) 
			       		return defaultEmptyOK;
			       else 
			       		return (isSignedInteger.arguments[1] == true);
			    else  {
			        var startPos = 0;
			        var secondArg = defaultEmptyOK;
			        if (isSignedInteger.arguments.length > 1)
			            secondArg = isSignedInteger.arguments[1];
			        if ( (s.charAt(0) == "-") || (s.charAt(0) == "+") )
			           startPos = 1;
			        return (isInteger(s.substring(startPos, s.length), secondArg));
			    }
			}
			
			function isInteger (s) {   
				var i;
			    if (isEmpty(s))
			       if (isInteger.arguments.length == 1) 
			       		return defaultEmptyOK;
			       else 
			       		return (isInteger.arguments[1] == true);
			       		
			    for (i = 0; i < s.length; i++) {
			        var c = s.charAt(i);
			        if (!isDigit(c)) return false;
			    }
			    
			    return true;
			}
			
			function isDigit (c) {
				return ((c >= "0") && (c <= "9"));
			}
			
			function isEmpty(s) {
				return ((s == null) || (s.length == 0));
			}
			
			//Se repite
			function WindowCalendar() {
			    var m=new Date();
			    m.setFullYear(document.Frmfechas.strAnio.value);
			    m.setMonth(document.Frmfechas.strMes.value);
			    m.setDate(document.Frmfechas.strDia.value);
			    n=m.getMonth();
			    msg=window.open("/EnlaceMig/calNom.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
			    msg.focus();
			}
			
			//Se repite
			function SeleccionaFechaApli(ind) {
			   	var m=new Date();
			    m.setFullYear(document.Frmfechas.strAnio.value);
			    m.setMonth(document.Frmfechas.strMes.value);
			    m.setDate(document.Frmfechas.strDia.value);
			    n=m.getMonth();
			    Indice=ind;
			    msg=window.open("/EnlaceMig/calNom3.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
			    msg.focus();
			}
			
			//Se repite
			function WindowCalendarApliInicio() {
			    var m=new Date();
			    m.setFullYear(document.Frmfechas.strAnio.value);
			    m.setMonth(document.Frmfechas.strMes.value);
			    m.setDate(document.Frmfechas.strDia.value);
			    n=m.getMonth();
			    msg=window.open("/EnlaceMig/calNom2.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
			    msg.focus();
			}			 			
					
			//Sin uso en dos paginas WindowCalendar1			
			
			function trimString (str) {
				str = this != window? this : str;
				return str.replace(/^\s+/g, '').replace(/\s+$/g, '');
			}						
			
			function cuadroDialogoCatNominaPrep( mensaje, tipo ) {
				
				respuesta=0;
				var titulo="Error";
				var imagen="gic25060.gif";

				if ( tipo == 1 ) {	
					// Alerta	
					imagen="gic25020.gif";
					titulo="Alerta";
				} else {	
					// Error
					imagen="gic25060.gif";
				}

				ventana=window.open('','trainerWindow','width=420,height=150,toolbar=no,scrollbars=yes,left=210,top=225');
				ventana.document.open();
				ventana.document.write("<html>");
				ventana.document.writeln("<head>\n<title>"+titulo+"</title>\n");
				ventana.document.writeln("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
				ventana.document.writeln("</head>");
				ventana.document.writeln("<body bgcolor='white'>");
				ventana.document.writeln("<form>");
				ventana.document.writeln("<table border=0 width=365 class='textabdatcla' align=center cellspacing=3 cellpadding=0>");
				ventana.document.writeln("<tr><th height=25 class='tittabdat' colspan=2>"+ titulo + "</th></tr>");
				ventana.document.writeln("</table>");
				ventana.document.writeln("<table border=0 width=365 class='textabdatcla' align=center cellspacing=0 cellpadding=1>");
				ventana.document.writeln("<tr><td class='tabmovtex1' align=right width=50><img src='/gifs/EnlaceMig/"+imagen+"' border=0></a></td>");
				ventana.document.writeln("<td class='tabmovtex1' align=center width=300><br>"+mensaje+"<br><br>");
				ventana.document.writeln("</tr></td>");
				ventana.document.writeln("<tr><td class='tabmovtex1'>");
				ventana.document.writeln("</td></tr>");
				ventana.document.writeln("</table>");
				ventana.document.writeln("<table border=0 align=center cellspacing=6 cellpadding=0>");	
				ventana.document.writeln("<td align=left><a href='javascript:opener.ventanaActiva=0;opener.respuesta=0;window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
				ventana.document.writeln("</table>");
				ventana.document.writeln("</form>");
				ventana.document.writeln("</body>\n</html>");
				ventana.focus();
			}								
		</script>
		<script type="text/javascript">
			<%
			request.setAttribute("ctx", "/Enlace/enlaceMig/");
			if(session.getAttribute("newMenu")!=null)
				out.println(session.getAttribute("newMenu"));
			else
				if(request.getAttribute("newMenu")!= null)
					out.println(request.getAttribute("newMenu"));
			%>
		</script>
	</head>
	<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
			bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif',
			'/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif',
			'/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif',
			'/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif',
			'/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif',
			'/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif',
			'/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif',
			'/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');"
			background="/gifs/EnlaceMig/gfo25010.gif">

		<table width="571" border="0" cellspacing="0" cellpadding="0">
			<tr valign=top>
				<td width="*"><!-- MENU PRINCIPAL --> 
					<%
					if(request.getAttribute("MenuPrincipal") != null)
						out.print(request.getAttribute("MenuPrincipal"));	
					%>
				</td>
			</tr>
		</table>
		<!-- #EndLibraryItem -->
		<!-- #BeginEditable "Contenido" -->
		<table width="760" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="676" valign="top" align="left">
					<table width="666" border="0" cellspacing="6" cellpadding="0">
						<tr>
							<td width="528" valign="top" class="titpag">
								<%
								if(request.getAttribute("Encabezado") != null )
									out.print(request.getAttribute("Encabezado"));
								%>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table width="760" border="0" cellspacing="0" cellpadding="0">			
			<tr>
				<td align="center">				
					<div>					