<%@page import="java.util.*"%>
<jsp:include page="headerPrepago.jsp"/>
<script type="text/javascript">
<!--
	var js_diasInhabiles="<%=request.getAttribute("diasInhabiles")%>";
	var diasInhabiles = "<%=request.getAttribute("diasInhabiles")%>";
	function validaNumero(e) {
		var key = window.event ? e.keyCode : e.which;
		if (key == 8) return true;
		return /\d/.test(String.fromCharCode(key));
	}
	function abreCalendario(html) {
		var m = new Date();
		var forma = document.Frmfechas;
    	m.setFullYear(forma.strAnio.value);
    	m.setMonth(forma.strMes.value);
    	m.setDate(forma.strDia.value);
    	n = m.getMonth();
    	var msg = window.open("/EnlaceMig/" + html + ".html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    	msg.focus();
	}
	function consultar() {
		var forma = document.MantNomina;
		if (forma.secuencia.value != "" && !forma.secuencia.value.match(/^[0-9]+$/)) {
			cuadroDialogo("Debe proporcionar un n&uacute;mero de secuencia v&aacute;lido", 1);
			return;
		}
		forma.submit();
	}

	function limpiar(){
		document.MantNomina.reset();
	}
//-->
</script>
<form action="" name="Frmfechas">
	<%
	GregorianCalendar cal = new GregorianCalendar ();
	Date hoy = new Date();
	cal.setTime (hoy);
	int anio = cal.get (Calendar.YEAR);
	int mes = cal.get (Calendar.MONTH) + 1;
	int dia = cal.get (Calendar.DATE);
 	%>
	<input type="hidden" value="<%=anio%>" name="strAnio"/>
	<input type="hidden" value="<%=((mes < 10) ? "0" : "") + mes%>" name="strMes"/>
	<input type="hidden" value="<%=((dia < 10) ? "0" : "") + dia%>" name="strDia"/>
</form>
<form action="${ctx}NomPreConsultaEstado" method="get" name="MantNomina">
	<input type="hidden" name="inicial" value="true">
	<input type="hidden" name="operacion" value="detalle">
	<table cellspacing="2" cellpadding="3" border="0">
		<tr>
			<td class="tittabdat" colspan="2">Consulta estado asignaci&oacute;n</td>
		</tr>
		<tr align="center">
        	<td valign="top" class="textabdatcla" colspan="2">
				<table cellspacing="0" cellpadding="0" border="0" width="500">
              		<tr valign="top">
                  		<td align="left" width="360">
                    		<table cellspacing="0" cellpadding="5" border="0">
                       			<tr>
                        			<td align="right" class="tabmovtex">Fecha de recepci&oacute;n<br/></td>
                        			<td valign="middle" class="tabmovtex">
                          				<input type="text" name="fecha1" size="10" class="tabmovtex" maxlength="10" readonly="readonly"/>
                            			<a href="javascript:abreCalendario('calNom22');"><img height="14" border="0" width="12" src="/gifs/EnlaceMig/gbo25410.gif"/></a>
                        			</td>
                      			</tr>
                      			<tr>
                        			<td align="right" class="tabmovtex">Fecha de aplicaci&oacute;n</td>
                        			<td valign="middle" class="tabmovtex">
                          				<input type="text" name="fecha2" size="10" class="tabmovtex" maxlength="10" readonly="readonly"/>
                            			<a href="javascript:abreCalendario('calNom3');"><img height="14" border="0" width="12" src="/gifs/EnlaceMig/gbo25410.gif"/></a>
                        			</td>
                      			</tr>
                      			<tr>
                        			<td align="right" class="tabmovtex">Secuencia</td>
                        			<td class="tabmovtex"><input type="text" name="secuencia" size="14" maxlength="10" class="tabmovtex" onkeypress="return validaNumero(event);"/></td>
                      			</tr>
                    		</table>
                  		</td>
                  		<td align="left" width="140">
                    		<table cellspacing="0" cellpadding="3" border="0">
                      			<tr>
                        			<td align="right" width="65" class="tabmovtex">Estatus:</td>
                        			<td align="center" width="10" valign="middle" class="tabmovtex">
                          				<input type="checkbox" name="chkEstado" value="R"/>
                        			</td>
                        			<td width="65" class="tabmovtex">Recibidas</td>
                      			</tr>
                      			<tr>
                        			<td align="right" class="tabmovtex"></td>
                        			<td align="center" valign="middle" class="tabmovtex">
                          				<input type="checkbox" name="chkEstado" value="C"/>
                        			</td>
                        			<td class="tabmovtex">Canceladas </td>
                      			</tr>
                      			<tr>
                        			<td align="right" class="tabmovtex"> </td>
                        			<td align="center" valign="middle" class="tabmovtex">
                          				<input type="checkbox" name="chkEstado" value="E"/>
                        			</td>
                        			<td class="tabmovtex">En proceso</td>
								</tr>
							    <tr>
								 	<td align="right" class="tabmovtex"> </td>
								 	<td align="center" valign="middle" class="tabmovtex">
								 		<input type="checkbox" name="chkEstado" value="P"/></td>
								 	<td class="tabmovtex">Procesadas</td>
								</tr>
							</table>
			  			</td>
			 		</tr>
				</table>
		  	</td>
		</tr>
	</table>
	<br/>
    <table height="22" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" width="166">
		<tr>
        	<td height="22" align="right" width="90" valign="top">
        		<a href="javascript:consultar();"><img height="22" border="0" width="90" src="/gifs/EnlaceMig/gbo25220.gif" alt="Consultar"/></a>
        	</td>
			<td height="22" align="left" width="76" valign="middle">
				<a href="javascript:limpiar();"><img height="22" border="0" width="76" src="/gifs/EnlaceMig/gbo25250.gif" alt="Limpiar"/></a>
			</td>
		</tr>
	</table>
</form>
<jsp:include page="footerPrepago.jsp"/>
