<%@ page import="mx.altec.enlace.beans.*,java.util.*,mx.altec.enlace.beans.NomPreLM1D.Relacion"%>
<%NomPreLM1D lm1d = (NomPreLM1D) request.getAttribute("lm1d");
if (lm1d != null) {%>
<html>
	<head>
		<title>Detalle relacion tarjeta empleado</title>
	</head>
	<body>
		<table border="0">
			<tr>
				<td>
					<br>
					<table border="1">
						<tr>
							<td align="center"><b>No de empleado</b></td>
							<td align="center"><b>Nombre del empleado</b></td>
							<td align="center"><b>No. de tarjeta</b></td>
							<td align="center"><b>Estatus</b></td>
						</tr>
						<%Iterator it = lm1d.getDetalle().iterator();
						while (it.hasNext()) {
							Relacion relacionTE = (Relacion) it.next();
						%>						
						<tr>
							<td align="center">
								&nbsp;<%=relacionTE.getEmpleado().getNoEmpleado()%>
							</td>
							<td align="center">
								<%=relacionTE.getEmpleado().getNombre()+ " " + relacionTE.getEmpleado().getPaterno() + " " + relacionTE.getEmpleado().getMaterno()%>
							</td>
							<td align="center">
								&nbsp;<%=relacionTE.getTarjeta().getNoTarjeta()%>
							</td>
							<td align="center">
								<%=relacionTE.getTarjeta().getDescEstadoTarjeta()%>
							</td>
						</tr>
						<%}%>
					</table>
				</td>
			</tr>
		</table>
		<br>
	</body>
</html>
<%}%>