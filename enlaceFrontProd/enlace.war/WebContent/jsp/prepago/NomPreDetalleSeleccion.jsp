<%@ page import="mx.altec.enlace.beans.*,java.util.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
	<head>
		<title>NomPreDetalleSeleccion</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="GENERATOR" content="Rational Software Architect">
		<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
	</head>
	<body>
	<%List detalle = (List) request.getAttribute("detalle");%>
	<%NomPreRemesa remesa = (NomPreRemesa) request.getAttribute("remesa");%>
		<table width="369" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
			<tr>
				<td class="tittabdat" align="center" width="130">N&uacute;mero Remesa</td>
				<td class="tittabdat" align="center" width="130">N&uacute;mero Tarjeta</td>
				<td class="tittabdat" align="center" width="117">Fecha de Expiraci&oacute;n</td>
			</tr>
			<%
			if (detalle != null) {
				Iterator it = detalle.iterator();
				boolean claro = false;
				while (it.hasNext()) {
					NomPreTarjeta tarjeta = (NomPreTarjeta) it.next();
					String sClass = claro ? "textabdatcla" : "textabdatobs";
			%>
			<tr>
				<td class="<%=sClass%>" nowrap align="center" width="130"><%=remesa.getNoRemesa()%></td>
				<td class="<%=sClass%>" nowrap align="center" width="130"><%=tarjeta.getNoTarjeta()%></td>
				<td class="<%=sClass%>" align="center" width="117"><%=tarjeta.getFechaVigencia()%></td>
			</tr>
			<%		claro = !claro;
				}
			}%>
		</table>
		<br>
		<table>
			<tr>
				<td align="center" class="texfootpagneg">
					<a href="javascript:window.close();"><img border=0 name=imageField4 src="/gifs/EnlaceMig/gbo25200.gif" width="71" height="22" alt=Cerrar></a><nobr>		
				</td>
			</tr>			
		</table>		
	</body>
</html>