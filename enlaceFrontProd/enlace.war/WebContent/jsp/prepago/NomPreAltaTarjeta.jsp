<jsp:include page="headerPrepago.jsp"/>
<%@page import="mx.altec.enlace.beans.NomPreTarjeta"%>
<%@page import="mx.altec.enlace.beans.NomPreEmpleado"%>
<script type="text/javascript">

window.onload = load;
<% if (request.getAttribute("mensajeCifrado") != null && request.getAttribute("mensajeCifrado") != "" ) {%>
	window.onload =msgCifrado;
<% } %>
function msgCifrado(){
 	try {
	var mensajeCifrado = document.MantNomina.mensajeCifrado.value;
	if(mensajeCifrado != null && mensajeCifrado != "" && mensajeCifrado != "null" ) {
		var arreglo = mensajeCifrado.split("|");
               var msj = arreglo[1];
               var tipo = arreglo[0];
           cuadroDialogo( msj ,tipo);
	}
	} catch (e){};
	
}

var original;

	function validaRFC() {

		var valor = document.MantNomina.rfc.value.toUpperCase();

		if (/^[a-zA-Z � �][a-zA-Z � �][a-zA-Z � �][a-zA-Z � �]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9]$/.test(valor)) {
			if(validaLetrasRFC(valor)) {
				if(valida_FechaRFC(valor, 6)) {
					return true;
				}
			}
		}

		return false;
	}

	function validaLetrasRFC(rfc) {

		var resultado="";
		var rfc1, nombre, pat, mat;

		nombre = document.MantNomina.nom_empleado.value.toUpperCase();

		pat = document.MantNomina.paterno.value.toUpperCase();

		mat = document.MantNomina.materno.value.toUpperCase();

		rfc1 = rfc.toUpperCase().substring(0,4);

		if(mat == "") mat = rfc.substring(2,3);

		nombre=quitaPrep(nombre)
		pat=quitaPrep(pat)
		mat=quitaPrep(mat)

		if (pat.length==0 || nombre.length==0 || mat.length==0 || rfc1.length!=4) return false;
		resultado=pat.charAt(0);
		var vocal = buscaVocal(pat.substring(1));
		if (vocal=="")
			{if (pat.length>1) resultado+=pat.charAt(1); else resultado+=rfc1.charAt(1);}
		else
			resultado+=vocal;

		resultado+=mat.charAt(0);
		resultado+=letraNombre(nombre);
		resultado=frases(resultado);
		if (resultado==rfc1) return true; else return false;
	}

	function quitaPrep(cadena) {
		var temp = "", elem;
		var elem2 = new Array(),j=0;
		elem = cadena.split(" ");

		for (var i=0;i<elem.length;i++)
			if (elem[i]!="" && elem[i]!="DEL" && elem[i]!="DE" && elem[i]!="LA" && elem[i]!="LOS" && elem[i]!="LAS" &&
					elem[i]!="Y" && elem[i]!="MC" && elem[i]!="MAC" && elem[i]!="VON" && elem[i]!="VAN") {
				elem2[j]=elem[i];
				j++;
			}
		if (elem2.length==1) return elem2[0];
		for (var i=0;i<elem2.length;i++) temp+=" "+elem2[i];
		temp=temp.substring(1);
		return temp;
	}

	//
	function buscaVocal(cadena) {
		for (var i=0;i<cadena.length;i++)
			if (cadena.charAt(i)=="A" || cadena.charAt(i)=="E" || cadena.charAt(i)=="I" ||
				cadena.charAt(i)=="O" || cadena.charAt(i)=="U")
				return cadena.charAt(i);
		return "";
	}

	//
	function letraNombre(cadena) {
		var resultado="",temp="",elementos;
		elementos =cadena.split(" ");

		if (elementos.length==1) return cadena.charAt(0);

		temp=elementos[0];

		if (!(temp=="MARIA" || temp=="MA" || temp=="MA." || temp=="JOSE")) return "" +temp.charAt(0);

		temp=elementos[1];
		return "" +temp.charAt(0);
	}

	//
	function frases(f) {
		if (f=="BUEI" || f=="BUEY" || f=="CACA" || f=="CACO" || f=="CAGA" || f=="CAGO" || f=="CAKA" || f=="CAKO" || f=="COGE" ||
			f=="COJA" || f=="COJE" || f=="COJI" || f=="COJO" || f=="CULO" || f=="FETO" || f=="GUEY" || f=="JOTO" || f=="KACA" ||
			f=="KACO" || f=="KAGA" || f=="KAGO" || f=="KOGE" || f=="KOJO" || f=="KAKA" || f=="KULO" || f=="LOCA" || f=="LOCO" ||
			f=="LOKA" || f=="LOKO" || f=="MAME" || f=="MAMO" || f=="MEAR" || f=="MEAS" || f=="MEON" || f=="MION" || f=="MOCO" ||
			f=="MULA" || f=="PEDA" || f=="PEDO" || f=="PENE" || f=="PUTA" || f=="PUTO" || f=="QULO" || f=="RATA" || f=="RUIN")
			return f.substring(0,3)+ "X"
		else
			return f;
	}



	// Valida la Fecha del R.F.C. -------------------------------------------------------------------
	function valida_FechaRFC(valorCampo,Indice) {
		var ValorRegreso = true;
		var mes = valorCampo.substring(Indice, Indice + 2);
		var dia = valorCampo.substring(Indice + 2, Indice + 4);

		if((dia > 0 && dia <= 31) && (mes > 0 && mes <= 12)) {
			if(mes == 2 && dia > 29)	// Para el Mes de Febrero  MARL formato de A�o bisiesto
				{ValorRegreso = false;}
		}
		else {ValorRegreso = false;}

		return ValorRegreso;
	}

String.prototype.trim = function() {
 return this.replace(/^\s+|\s+$/g,"");
}
function load()
{
	if(document.MantNomina.tipoCarga[0].checked)
	{
		original = document.MantNomina.tipoCarga[0].value;
		document.MantNomina.tipoCarga[0].focus();
		document.MantNomina.tipoCarga[0].checked = true;
	}
	else
	{
		original = document.MantNomina.tipoCarga[1].value;
		document.MantNomina.tipoCarga[1].focus();
		document.MantNomina.tipoCarga[1].checked = true;
	}
}

	<%
	if(session !=null){
		if(session.getAttribute("tipoAlta")=="AI")
			session.setAttribute("tipoAlta","AL");
		else
			session.setAttribute("tipoAlta","AI");

		if(request.getAttribute("postal") != null &&  request.getAttribute("postal") != ""){
			session.setAttribute("tipoAlta","AL");
		}
	}
	%>

function reload()
{
	var i;
	var ctrl=document.MantNomina.tipoCarga;
	for(i=0; i<ctrl.length; i++)
	{
		if(ctrl[i].checked)
		{
			if(original != ctrl[i].value){
				window.location.reload();
				break;
			}
		}
	}
}

function altaImport()
{
	if (document.MantNomina.Archivo.value == "")
	{
		cuadroDialogo( "Por favor ingrese la ruta del archivo" ,1);
	}
	else
	{
		if(document.MantNomina.Archivo.value.substring(document.MantNomina.Archivo.value.length-4,document.MantNomina.Archivo.value.length).toUpperCase() != ".TXT")
		{
			cuadroDialogo( "La extensi&oacute;n del archivo es incorrecta, verifique que sea 'txt'" ,1);
		}
		else
		{
			document.MantNomina.MantNomina2.value="";
			document.MantNomina.action = "NomPreAltaTarjeta?opcion=13";
			document.MantNomina.submit();
		}
	}
}


function validaNE(campo){
	return campo.value.match(/^[0-9]{1,<%=NomPreEmpleado.LNG_NO_EMPLEADO%>}$/);
}
function validaNO(campo){
	return campo.value.match(/^[\sA-Za-z � �]*$/);
}
function validaFE(campo){
	return campo.value.match(/^\d{1,2}\/\d{1,2}\/\d{2,4}$/);
}
function validaHomoClave(campo){
	return campo.value.match(/^[a-zA-Z0-9 � �]*$/);
}
function validaMail(campo){
	return campo.value.match(/^[0-9A-Za-z][A-Za-z0-9_.-]*@[A-Za-z0-9_-]+\.[A-Za-z0-9_.]+[A-za-z]$/);
}
function validaAlfa(campo){
	return campo.value.match(/^[\s-\w � �]*$/);
}
function validaEntero(campo){
	return campo.value.match(/^(?:\+|-)?\d+$/);
}
function validaCodPos(campo){
	return campo.value.match(/^[0-9]{<%=NomPreEmpleado.LNG_CODIGO_POSTAL%>}$/);
}
function validaTelefono(){
	var f=document.MantNomina;
	var lonTel=f.lada.value.length+f.telefono.value.length;
	if(lonTel!=10){
		return false;
	}
	return true;
}
function validaFecha()
{
	var ruta = document.MantNomina.nacimiento;
	if(parseInt(ruta.value.substring(0,1))>=0 && parseInt(ruta.value.substring(0,1))<=3)
  	{
  		if((parseInt(ruta.value.substring(1,2))>=0 && parseInt(ruta.value.substring(1,2))<=9 && parseInt(ruta.value.substring(0,1))<3) || (parseInt(ruta.value.substring(1,2))>=0 && parseInt(ruta.value.substring(1,2))<=1 && parseInt(ruta.value.substring(0,1))==3))
  		{
  			if(parseInt(ruta.value.substring(0,1))!=0 || parseInt(ruta.value.substring(1,2))!=0)
  			{
  				if((parseInt(ruta.value.substring(4,5))>0 && parseInt(ruta.value.substring(4,5))<=9 && parseInt(ruta.value.substring(3,4))==0) || (parseInt(ruta.value.substring(4,5))>=0 && parseInt(ruta.value.substring(4,5))<=2 && parseInt(ruta.value.substring(3,4))==1))
  				{
  					var ano=new Date().getFullYear();
  					if(ruta.value.substring(6,10).length==4 && parseInt(ruta.value.substring(6,10))<ano && parseInt(ruta.value.substring(6,10))>1800)
  					{
  						if(ruta.value.substring(2,3)=='/' && ruta.value.substring(5,6)=='/')
  						{
  							return true;
  						}
  						else
  						{
  							cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar los separadores" ,1);
  							return false;
  						}
  					}
  					else
  					{
  						cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el a�o" ,1);
  						return false;
  					}
  				}
  				else
  				{
  					cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el mes" ,1);
  					return false;
  				}
  			}
  			else
  			{
  				cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el d&iacute;a" ,1);
  				return false;
  			}
  		}
  		else
  		{
  			cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el d&iacute;a" ,1);
  			return false;
  		}
  	}
  	else
  	{
		cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el d&iacute;a" ,1);
		return false;
	}
}

function validaDatos(){
	var f=document.MantNomina;

	if( f.tarjeta.value.trim() == "" ){
		cuadroDialogo("El campo n&uacute;mero de tarjeta es requerido.", 1);
		return false;
	}

	if (f.tarjeta.value != "" && !f.tarjeta.value.match(/^[0-9]+$/)) {
		cuadroDialogo("El n&uacute;mero de tarjeta no es v&aacute;lido.", 1);
		return;
	} else if(f.tarjeta.value != "" && f.tarjeta.value.length < <%=NomPreTarjeta.LNG_NO_TARJETA_MIN%>) {
		cuadroDialogo("Longitud incorrecta de n&uacute;mero de tarjeta", 1);
		return;
	}

	if( f.nom_empleado.value.trim() == "" ){
		cuadroDialogo("El campo nombre del empleado es requerido.", 1);
		return false;
	}

	if(f.nom_empleado.value!='' && !validaNO(f.nom_empleado)){
		cuadroDialogo("Los caracteres ingresados en el campo nombre del empleado son inv&aacute;lidos.", 1);
		return false;
	}

	if( f.paterno.value.trim() == "" ){
		cuadroDialogo("El campo apellido paterno es requerido.", 1);
		return false;
	}

	if(f.paterno.value.trim() != "" && !validaNO(f.paterno)){
		cuadroDialogo("Los caracteres ingresados en el apellido paterno son inv&aacute;lidos.", 1);
		return false;
	}

	if(f.materno.value.trim() != "" && !validaNO(f.materno)){
		cuadroDialogo("Los caracteres ingresados en el apellido materno son inv&aacute;lidos.", 1);
		return false;
	}

	if(f.nacimiento.value.trim() == ""){
		cuadroDialogo("El campo fecha de nacimiento es requerido.",1);
        return false;
	}

	if(f.nacimiento.value!='' && !validaFE(f.nacimiento)){
		cuadroDialogo("Los caracteres ingresados en el campo fecha de nacimiento son inv&aacute;lidos.", 1);
		return false;
	}
	else
		if(!validaFecha())
		{
			return false;
		}

	if(f.rfc.value.trim() == "" ){
		cuadroDialogo("El campo RFC es requerido.", 1);
		return false;
	}

	if(f.rfc.value!='' && !validaRFC(f.rfc)){
		cuadroDialogo("Los caracteres ingresados en el campo RFC  son inv&aacute;lidos.", 1);
		return false;
	}

	var valor = document.MantNomina.rfc.value.toUpperCase();
	var naci = document.MantNomina.nacimiento.value.toUpperCase();
	var fenaci = (naci.substring(8, 10) + naci.substring(3, 5) + naci.substring(0, 2));
	var ferfc = valor.substring(4, 10);
	if (fenaci != ferfc) {
		cuadroDialogo("La fecha de nacimiento no concuerda con el RFC", 1);
		return false;
	}

	if(f.homoclave.value!='' && !validaHomoClave(f.homoclave)){
		cuadroDialogo("Los caracteres ingresados en el campo homoclave  son inv&aacute;lidos.", 1);
		return false;
	}

	if(f.num_id.value.trim() == ""){
		cuadroDialogo("El campo n&uacute;mero de identificaci&oacute;n es requerido.",1);
        return false;
	}

	if(f.num_id.value!='' && !validaAlfa(f.num_id)){
		cuadroDialogo("Los caracteres ingresados en el campo n&uacute;mero de identificaci&oacute;n  son inv&aacute;lidos.", 1);
		return false;
	}

	if(f.num_empleado.value.trim() == ""){
		cuadroDialogo("El campo n&uacute;mero de empleado es requerido.",1);
        return false;
	}

	if(f.num_empleado.value!='' && !validaNE(f.num_empleado)){
		cuadroDialogo("Los caracteres ingresados en el campo n&uacute;mero de empleado son inv&aacute;lidos.", 1);
		return false;
	}

	if(f.correo.value!='' && !validaMail(f.correo)){
		cuadroDialogo("Los caracteres ingresados en el campo correo electr&oacute;nico son inv&aacute;lidos.", 1);
		return false;
	}

	if( f.calle.value.trim() == "" ){
		cuadroDialogo("El campo calle es requerido.", 1);
		return false;
	}

	if(f.calle.value!='' && !validaAlfa(f.calle)){
		cuadroDialogo("Los caracteres ingresados en el campo calle son inv&aacute;lidos.", 1);
		return false;
	}

	if( f.numExt.value.trim() == "" ){
		cuadroDialogo("El campo n&uacute;mero exterior es requerido.", 1);
		return false;
	}

	if(f.numExt.value!='' && !validaAlfa(f.numExt)){
		cuadroDialogo("Los caracteres ingresados en el campo n&uacute;mero exterior son inv&aacute;lidos.", 1);
		return false;
	}

	if(f.numInt.value!='' && !validaAlfa(f.numInt)){
		cuadroDialogo("Los caracteres ingresados en el campo n&uacute;mero interior son inv&aacute;lidos.", 1);
		return false;
	}

	if( f.colonia.selectedIndex == -1){
		cuadroDialogo("El campo colonia es requerido.", 1);
		return false;
	}

	if( f.postal.value.trim() == "" ){
		cuadroDialogo("El campo c&oacute;digo postal es requerido.", 1);
		return false;
	}
	if(f.postal.value !='' && !validaEntero(f.postal)){
		cuadroDialogo("Los caracteres ingresados en el campo c&oacute;digo postal son inv&aacute;lidos.", 1);
		return false;
	}
	if(f.postal.value !='' && (f.postal.value.length!=<%=NomPreEmpleado.LNG_CODIGO_POSTAL%>)){
		cuadroDialogo("La longitud del campo c&oacute;digo postal es incorrecta.", 1);
		return false;
	}
	if(f.postal.value !='' && !validaCodPos(f.postal)){
		cuadroDialogo("Los caracteres ingresados en el campo c&oacute;digo postal son inv&aacute;lidos.", 1);
		return false;
	}

	if( f.delegacion.value.trim() == "" ){
		cuadroDialogo("El campo delegaci&oacute;n o municipio es requerido.", 1);
		return false;
	}

	if(f.delegacion.value!='' && !validaAlfa(f.delegacion)){
		cuadroDialogo("Los caracteres ingresados en el campo delegaci&oacute;n o municipio son inv&aacute;lidos.", 1);
		return false;
	}
	if( f.estado.value.trim() == "" ){
		cuadroDialogo("El campo ciudad o estado es requerido.", 1);
		return false;
	}

	if(f.estado.value!='' && !validaAlfa(f.estado)){
		cuadroDialogo("Los caracteres ingresados en el campo ciudad o estado son inv&aacute;lidos.", 1);
		return false;
	}

	if(f.lada.value!='' && !validaEntero(f.lada)){
		cuadroDialogo("Los caracteres ingresados en el campo lada son inv&aacute;lidos.", 1);
		return false;
	}

	if(f.telefono.value!='' && !validaEntero(f.telefono)){
		cuadroDialogo("Los caracteres ingresados en el campo tel&eacute;fono son inv&aacute;lidos.", 1);
		return false;
	}

	if((f.lada.value!='' || f.telefono.value!='') && !validaTelefono()){
		cuadroDialogo("La longitud del tel&eacute;fono es incorrecta.", 1);
		return false;
	}

	return true;
}
function altaLinea(){
	var	f=document.MantNomina;
	if(validaDatos()){
		f.action = "NomPreAltaTarjeta?opcion=2";
		f.submit();
	}
}
function Cancelar()
	{
		document.MantNomina.action = "NomPreAltaTarjeta?opcion=12";
		document.MantNomina.submit();
	}

function cargaCodPostal()
	{

		document.MantNomina.action = "NomPreAltaTarjeta?opcion=15";
		document.MantNomina.submit();
	}

function Enviar()
	{
		var correcto = Valida(document.MantNomina);
				if(correcto != (true))
				{
					return;
				}
	}

function continua()
			{
				if(respuesta == 1)
				{
					document.MantNomina.action = "NomPreAltaTarjeta?opcion=14";
					document.MantNomina.submit();
				}
			}

function Valida(forma)
	{
		if(cuadroDialogo("�Est&aacute; seguro de dar de Alta a los empleados?",2)){
					return true;
				}else{
					return false;
				}
	}

	function validaNumero(e) {
		var key = window.event ? e.keyCode : e.which;
		if (key == 8) return true;
		return /\d/.test(String.fromCharCode(key));
	}

	function regresa(e) {
 		return true;
	}

function js_regresar()
{
	document.MantNomina.action = "NomPreAltaTarjeta";
	document.MantNomina.submit();
}

function convierteMayus(campo)
{
	tmp = campo.value;
	tmp=tmp.toUpperCase();
	campo.value=tmp;
	return true;
}

function soloNumeros(evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
        ((evt.which) ? evt.which : 0));
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
 }
function cambiaEmpleado()
{
	var f=document.MantNomina;
	var cadena=f.num_empleado.value;
	var folioReturn = "";
	if(cadena.length>0){
		var longitud = cadena.length;
		if (longitud < 7)
			for (i = 1; i <= 7 - longitud; i++)
				folioReturn += "0";
		folioReturn += cadena;
		f.num_empleado.value=folioReturn;

	}
}


</script>

<style>
	div.fileinputs {
	position: relative;
	}

	div.fakefile {
		position: absolute;
		top: 0px;
		left: 0px;
		z-index: 1;
	}
</style>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="msgCifrado();MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">


<FORM  NAME="MantNomina" METHOD="Post" ACTION="MantNomina" ENCTYPE="multipart/form-data">
<INPUT TYPE="hidden" name="mensajeCifrado" value="<%=request.getAttribute("mensajeCifrado")%>"/>
<INPUT TYPE="hidden" VALUE="1" NAME="statusDuplicado"/>
<INPUT TYPE="hidden" VALUE="1" NAME="statushrc"/>
<%

	if(request.getAttribute("TablaTotales") == null){ %>
	<%if(( session != null && (session.getAttribute("tipoAlta")== null || session.getAttribute("tipoAlta").equals("AI"))) && request.getAttribute("LineaCantidad") == null){ %>
			<table width="760" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td align="center">
				<table width="600" border="0" cellspacing="2" cellpadding="3" bgcolor="#EBEBEB">
					<tr>
						<td width="100%" class="tittabdat" colspan="3">
							<input type="radio" name="tipoCarga" value="AI" onclick="reload();" checked="checked"/>Importar
						</td>
					</tr>
					<tr>
						<td class=textabdatcla bgcolor="#FFFFFF"  colspan="3">
							<div class="fileinputs">
								<INPUT TYPE="file" NAME="Archivo" size="50" onchange="document.MantNomina.MantNomina2.value=this.value">
								<div class="fakefile">
									<input type="Text" NAME="MantNomina2" id="MantNomina2" readonly size="50">
								</div>
							</div>
						</td>
					</tr>
				</table>
				<br>
				<table width="600" border="0" cellspacing="2" cellpadding="2" bgcolor="#EBEBEB">
					<tr>
						<td width="600" class="tittabdat" colspan="3">
							<input type="radio" name="tipoCarga" value="AL" onclick="reload();"/>En l&iacute;nea
						</td>
					</tr>
					<tr>
						<td class=textabdatcla colspan="1">
							* No. de tarjeta asignada
						</td>
						<td class=textabdatcla colspan="2">
							* Nombre del empleado
						</td>
					</tr>
					<tr>
						<td class=textabdatcla colspan="1">
							<input type="text" value="" maxlength="<%=NomPreTarjeta.LNG_NO_TARJETA%>" size="25" disabled>
						</td>
						<td class=textabdatcla colspan="2">
							<input type="text" value="" maxlength="<%=NomPreEmpleado.LNG_NOMBRE%>" size="50" disabled>
						</td>
					</tr>
					<tr>
						<td class=textabdatcla>
							<table cellpadding="0" cellspacing="0">
								<tr>
									<td class=textabdatcla>* RFC</td>
									<td class=textabdatcla>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Homoclave</td>
								</tr>
							</table>
						</td>
						<td class=textabdatcla>
							* Apellido paterno
						</td>
						<td class=textabdatcla>
							Apellido materno
						</td>
					</tr>
					<tr>
						<td class=textabdatcla>
							<table cellpadding="0" cellspacing="0">
								<tr>
									<td class=textabdatcla><input type="text" value="" maxlength="<%=NomPreEmpleado.LNG_RFC%>" size="10" disabled></td>
									<td class=textabdatcla>&nbsp;<nobr/><input type="text" value="" maxlength="<%=NomPreEmpleado.LNG_HOMOCLAVE%>" size="5" disabled></td>
								</tr>
							</table>
						</td>
						<td class=textabdatcla>
							<input type="text" value="" maxlength="<%=NomPreEmpleado.LNG_PATERNO%>" size="22" disabled>
						</td>
						<td class=textabdatcla>
							<input type="text" value="" maxlength="<%=NomPreEmpleado.LNG_MATERNO%>" size="22" disabled>
						</td>
					</tr>
					<tr  bgcolor=#FFFFFF>
						<td class=textabdatcla>
							* No. de identificaci&oacute;n
						</td>
						<%--<td class=textabdatcla colspan="2">
							* Tipo de identificaci&oacute;n
						</td>--%>
					</tr>
					<tr>
						<td class=textabdatcla>
							<input type="text" value="" maxlength="<%=NomPreEmpleado.LNG_NUM_ID%>" size="25" disabled>
						</td>
						<%--<td class=textabdatcla colspan="2">
							<select disabled><option>IFE</option><option>Pasaporte</option></select>
						</td>--%>
					</tr>
					<tr  bgcolor=#FFFFFF>
						<td class=textabdatcla>
							* No. de empleado
						</td>
						<td class=textabdatcla>
							* Fecha de nacimiento<br>(dd/mm/aaaa)
						</td>
						<td class=textabdatcla colspan="3">
							* Nacionalidad
						</td>
					</tr>
					<tr>
						<td class=textabdatcla>
							<input type="text" value="" maxlength="<%=NomPreEmpleado.LNG_NO_EMPLEADO%>" size="20" disabled>
						</td>
						<td class=textabdatcla>
							<input type="text" value="" maxlength="<%=NomPreEmpleado.LNG_FECHA_NACIMIENTO%>" size="12" disabled>
						</td>
						<td class=textabdatcla>
							<select name="nacionalidad" disabled="disabled"><option>MEXI=Mexicana</option></select>
						</td>
					</tr>
					<tr  bgcolor=#FFFFFF>
						<td class=textabdatcla colspan="3">
							* Pa&iacute;s de nacimiento
						</td>
					</tr>
					<tr>
						<td class=textabdatcla>
							<select disabled="disabled"><option>MEXICO&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
							</select>
						</td>
					</tr>
					<tr  bgcolor=#FFFFFF>
						<td class=textabdatcla>
							* Estado civil
						</td>
						<td class=textabdatcla>
							* Sexo
						</td>
						<td class=textabdatcla>
							Correo electr&oacute;nico
						</td>
					</tr>
					<tr>
						<td class=textabdatcla>
							<select disabled="disabled"><option>Soltero</option></select>
						</td>
						<td class=textabdatcla>
							<select disabled><option>Masculino</option></select>
						</td>
						<td>
							<input type="text" value="" maxlength="<%=NomPreEmpleado.LNG_CORREO_ELECTRONICO%>" size="40" disabled>
						</td>
					</tr>
					<tr>
						<td class=textabdatcla width="54%">* Calle</td>
						<td class=textabdatcla width="34%">* No. Ext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No. Int</td>
						<td class=textabdatcla colspan="1">* Colonia</td>
					</tr>
					<tr>
						<td class=textabdatcla width="70%"><input type="text" value="" maxlength="<%=NomPreEmpleado.LNG_CALLE%>" size="30" disabled></td>
						<td class=textabdatcla width="20%"><input type="text" value="" maxlength="<%=NomPreEmpleado.LNG_NUM_EXT%>" size="5" disabled>
						&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="" maxlength="<%=NomPreEmpleado.LNG_NUM_INT%>" size="5" disabled></td>
						<td class=textabdatcla>	<select disabled="disabled"><option>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</option></select></tr>
					<tr>
						<td class=textabdatcla>
							* C&oacute;digo postal
						</td>
						<td class=textabdatcla>
							* Delegaci&oacute;n o municipio
						</td>
						<td class=textabdatcla>
							* Ciudad
						</td>
					</tr>
					<tr>
						<td class=textabdatcla colspan="1">
							<input type="text" value="" maxlength="<%=NomPreEmpleado.LNG_CODIGO_POSTAL%>" size="12" disabled>
						</td>
						<td class=textabdatcla>
							<input type="text" value="" maxlength="<%=NomPreEmpleado.LNG_DELEGACION%>" size="20" disabled>
						</td>
						<td class=textabdatcla>
							<input type="text" value="" maxlength="<%=NomPreEmpleado.LNG_ESTADO%>" size="35" disabled="disabled">
						</td>
					</tr>
					<tr>
						<td class=textabdatcla>
							* Estado
						</td>
						<td class=textabdatcla colspan="3">
							Lada / Tel&eacute;fono
						</td>
					</tr>
					<tr>
						<td class=textabdatcla>
							<input type="text" value="" maxlength="<%=NomPreEmpleado.LNG_ESTADO%>" size="35" disabled="disabled">
						</td>
						<td class=textabdatcla colspan="3">
							<input type="text" value="" maxlength="3" size="5" disabled>
							<input type="text" value="" maxlength="8" size="10" disabled>
						</td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td class="textabdatcla"><br><b>* Campos requeridos</b></td>
						<td class="textabdatcla" colspan="2"><br>* No ponga acentos ni caracteres especiales</td>
					</tr>
				</table>
				<br>
				<!-- #BeginLibraryItem "/Library/footerPaginacion.lbi" -->
				<table width="90" border="0" cellspacing="0" cellpadding="0" class="tabfonbla">
					<tr>
						<td align="center">
							<input type="image" border="0" name="Aceptar"
								src="../../gifs/EnlaceMig/gbo25280.gif" width="90" height="22"
								alt="Aceptar" onclick="javascript:altaImport();return false">
						</td>
					</tr>
				</table>
			    </td>
			</tr>
		</table>
	<%}else
		if(request.getAttribute("LineaCantidad") == null){ %>
		<table width="760" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td align="center">
				<table width="600" border="0" cellspacing="2" cellpadding="3" bgcolor="#EBEBEB">
					<tr>
						<td width="100%" class="tittabdat" colspan="3">
							<input type="radio" name="tipoCarga" value="AI" onclick="reload();"/>Importar
						</td>
					</tr>
					<tr>
						<td class=textabdatcla bgcolor="#FFFFFF"  colspan="3">
							<input type="file" size="50" NAME="Archivo" disabled/>
						</td>
					</tr>
				</table>
				<br>
				<table width="600" border="0" cellspacing="2" cellpadding="2" bgcolor="#EBEBEB">
					<tr>
						<td width="600" class="tittabdat" colspan="3">
							<input type="radio" name="tipoCarga" value="AL" onclick="reload();" checked/>En l&iacute;nea
						</td>
					</tr>
					<tr>
						<td class=textabdatcla colspan="1">
							* No. de tarjeta asignada
						</td>
						<td class=textabdatcla colspan="2">
							* Nombre del empleado
						</td>
					</tr>
					<tr>
						<td class=textabdatcla colspan="1">
							<input type="text" value="<%=request.getAttribute("tarjeta")%>" tabindex="10" size="25"
					maxlength="<%=NomPreTarjeta.LNG_NO_TARJETA%>" name="tarjeta"
					onkeypress="return soloNumeros(event)">
						</td>
						<td class=textabdatcla colspan="2">
							<input type="text" value="<%=request.getAttribute("nom_empleado")%>" size="50" name="nom_empleado" maxlength="<%=NomPreEmpleado.LNG_NOMBRE%>" tabindex="20" onchange="convierteMayus(this);">
						</td>
					</tr>
					<tr>
						<td class=textabdatcla>
							<table cellpadding="0" cellspacing="0">
								<tr>
									<td class=textabdatcla>* RFC</td>
									<td class=textabdatcla>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Homoclave</td>
								</tr>
							</table>
						</td>
						<td class=textabdatcla>
							* Apellido paterno
						</td>
						<td class=textabdatcla>
							Apellido materno
						</td>
					</tr>
					<tr>
						<td class=textabdatcla>
							<table cellpadding="0" cellspacing="0">
								<tr>
									<td class=textabdatcla><input type="text" value="<%=request.getAttribute("rfc")%>" size="10" name="rfc" maxlength="<%=NomPreEmpleado.LNG_RFC%>" tabindex="30" onchange="convierteMayus(this);"></td>
									<td class=textabdatcla>&nbsp;<nobr/><input type="text" value="<%=request.getAttribute("homoclave")%>" size="5" maxlength="<%=NomPreEmpleado.LNG_HOMOCLAVE%>" name="homoclave" tabindex="40" onchange="convierteMayus(this);"></td>
								</tr>
							</table>

						</td>
						<td class=textabdatcla>
							<input type="text" value="<%=request.getAttribute("paterno")%>" size="22" name="paterno" maxlength="<%=NomPreEmpleado.LNG_PATERNO%>" tabindex="50" onchange="convierteMayus(this);">
						</td>
						<td class=textabdatcla>
							<input type="text" value="<%=request.getAttribute("materno")%>" size="22" name="materno" maxlength="<%=NomPreEmpleado.LNG_MATERNO%>" tabindex="60" onchange="convierteMayus(this);">
						</td>
					</tr>
					<tr  bgcolor=#FFFFFF>
						<td class=textabdatcla>
							* No. de identificaci&oacute;n
						</td>
						<%--<td class=textabdatcla colspan="2">
							* Tipo de identificaci&oacute;n
						</td>--%>
					</tr>
					<tr>
						<td class=textabdatcla>
							<input type="text" value="<%=request.getAttribute("num_id")%>" name="num_id" maxlength="<%=NomPreEmpleado.LNG_NUM_ID%>" size="25" tabindex="70">
						</td>
						<%--<td class=textabdatcla colspan="2">
							<select name="tipoID"><option>IFE</option><option>Pasaporte</option></select>
						</td>--%>
					</tr>
					<tr  bgcolor=#FFFFFF>
						<td class=textabdatcla>
							* No. de empleado
						</td>
						<td class=textabdatcla>
							* Fecha de nacimiento<br>(dd/mm/aaaa)
						</td>
						<td class=textabdatcla colspan="3">
							* Nacionalidad
						</td>
					</tr>
					<tr>
						<td class=textabdatcla>
							<input type="text" value="<%=request.getAttribute("num_empleado")%>" tabindex="80" size="20" name="num_empleado" maxlength="<%=NomPreEmpleado.LNG_NO_EMPLEADO%>" onkeypress="return soloNumeros(event)" onblur="cambiaEmpleado();">
						</td>
						<td class=textabdatcla>
							<input type="text" value="<%=request.getAttribute("nacimiento")%>" size="12" name="nacimiento" maxlength="<%=NomPreEmpleado.LNG_FECHA_NACIMIENTO%>" tabindex="90">
						</td>
						<td class=textabdatcla>
							<select name="nacionalidad" tabindex="100"><option>MEXI=Mexicana</option><option>USA=Estadounidense</option><option>CANA=Canadiense</option><option>ESPA=Espa�ola</option><option>FRA=Franc&eacute;s</option></select>
						</td>
					</tr>
					<tr  bgcolor=#FFFFFF>
						<td class=textabdatcla colspan="1">
							* Pa&iacute;s de nacimiento
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
							<select name="vPais" tabindex="110">
								<%=request.getAttribute("Paises")%>
							</select>
						</td>
					</tr>
					<tr  bgcolor=#FFFFFF>
						<td class=textabdatcla>
							* Estado civil
						</td>
						<td class=textabdatcla>
							* Sexo
						</td>
						<td class=textabdatcla>
							Correo electr&oacute;nico
						</td>
					</tr>
					<tr>
						<td class=textabdatcla>
							<select name="edo_civil" tabindex="120"><option>Soltero</option><option>Casado</option><option>Viudo</option><option>Uni&oacute;n Libre</option></select>
						</td>
						<td class=textabdatcla>
							<select name="sexo" tabindex="130"><option>Masculino</option><option>Femenino</option></select>
						</td>
						<td>
							<input type="text" value="<%=request.getAttribute("correo")%>" size="40" name="correo" maxlength="40" tabindex="140">
						</td>
					</tr>
					<tr>
						<td class=textabdatcla width="54%">* Calle</td>
						<td class=textabdatcla width="34%">* No. Ext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No. Int</td>
						<td class=textabdatcla >* Colonia</td>
					</tr>
					<tr>
						<td class=textabdatcla width="70%"><input type="text" value="<%=request.getAttribute("calle")%>" size="30" name="calle" maxlength="<%=NomPreEmpleado.LNG_CALLE%>" tabindex="150" onchange="convierteMayus(this);"></td>
						<td class=textabdatcla width="20%"><input type="text" value="<%=request.getAttribute("numExt")%>" tabindex="160" size="5" onchange="convierteMayus(this);" name="numExt" maxlength="<%=NomPreEmpleado.LNG_NUM_EXT%>">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="<%=request.getAttribute("numInt")%>" tabindex="170" size="5" onchange="convierteMayus(this);" name="numInt" maxlength="<%=NomPreEmpleado.LNG_NUM_INT%>"></td>
						<td class="tabmovtexbol" nowrap>
							<select name="colonia" tabindex="180">
								<%=request.getAttribute("colonia")%>
							</select>
						</td>
					</tr>
					<tr>
						<td class=textabdatcla>
							* C&oacute;digo postal
						</td>
						<td class=textabdatcla>
							* Delegaci&oacute;n o municipio
						</td>
						<td class=textabdatcla>
							* Ciudad
						</td>
					</tr>
					<tr>
						<td class=textabdatcla colspan="1">
							<input type="text" value="<%=request.getAttribute("postal")%>" tabindex="190" size="12" name="postal" onkeypress="return soloNumeros(event)" onblur="javascript:cargaCodPostal()" maxlength="<%=NomPreEmpleado.LNG_CODIGO_POSTAL%>">
						</td>
						<td class=textabdatcla>
							<input type="text" name="delegacion" value="<%=request.getAttribute("delegacion")%>" maxlength="<%=NomPreEmpleado.LNG_DELEGACION%>" size="30" onchange="convierteMayus(this);" readonly="readonly">
						</td>
						<td class=textabdatcla>
							<input type="text" name="ciudad" value="<%=request.getAttribute("ciudad")%>" maxlength="<%=NomPreEmpleado.LNG_CIUDAD%>" size="35" onchange="convierteMayus(this);" readonly="readonly">
						</td>
					</tr>
					<tr>
						<td class=textabdatcla>
							* Estado
						</td>
						<td class=textabdatcla colspan="3">
							Lada / Tel&eacute;fono
						</td>
					</tr>
					<tr>
						<td class=textabdatcla>
							<input type="text" name="estado" value="<%=request.getAttribute("estado")%>" maxlength="<%=NomPreEmpleado.LNG_ESTADO%>" size="35" readonly="readonly">
						</td>
						<td class=textabdatcla colspan="3">
							<input type="text" value="<%=request.getAttribute("lada")%>" tabindex="200" size="5" name="lada" onkeypress="return soloNumeros(event)" maxlength="3">
							<input type="text" value="<%=request.getAttribute("telefono")%>" tabindex="210" size="10" name="telefono" onkeypress="return soloNumeros(event)" maxlength="8">
						</td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td class="textabdatcla"><br><b>* Campos requeridos</b></td>
						<td class="textabdatcla" colspan="2"><br>* No ponga acentos ni caracteres especiales</td>
					</tr>
				</table>
				<br>
				<!-- #BeginLibraryItem "/Library/footerPaginacion.lbi" -->
				<table width="90" border="0" cellspacing="0" cellpadding="0" class="tabfonbla">
					<tr>
						<td align="center">
							<!-- <input type="image" border="0" tabindex="210" name="Aceptar"
								src="../../gifs/EnlaceMig/gbo25280.gif" width="90" height="22"
								alt="Aceptar" onclick="javascript:altaLinea();">  -->

								<a href="javascript:altaLinea();" border=0 > <img  src="/gifs/EnlaceMig/gbo25280.gif" border=0></a>
						</td>
					</tr>
				</table>
			    </td>
			</tr>
		</table>
	<%}else { %>
 <div align="center">
  <% if (request.getAttribute("TablaRegistros") != null ) { %>
			<%= request.getAttribute("TablaRegistros") %>
			<%	} %>

  <% if (request.getAttribute("LineaCantidad") != null ) { %>
			<%= request.getAttribute("LineaCantidad") %>
			<%	} %>

 </div>

 <table align=center border=0 cellspacing=0 cellpadding=0>
	<tr><br></tr>
    <tr>
     <% if (request.getAttribute("TablaRegistros") != null || request.getAttribute("LineaCantidad") != null) { %>
			<td><A href = "javascript:Enviar(); return false" ><img src = "../../gifs/EnlaceMig/gbo25520.gif"  border=0 alt="Enviar"></a></td>
			<td><A href = "javascript:Cancelar(); return false" ><img src = "../../gifs/EnlaceMig/gbo25190.gif"  border=0 alt="Cancelar"></a></td>
 	<%	} %>
    </tr>
  </table>
	<%}%>
<%}%>
<div align="center">
	<% if (request.getAttribute("TablaTotales") != null ) { %>
			<%= request.getAttribute("TablaTotales") %>
			<%	} %>
</div>
  <script type="text/javascript">
  			<% if (request.getAttribute("RutaArchivo") != null ) { %>
			  	document.pantallaAlta.Archivo.value = '<%= request.getAttribute("RutaArchivo") %>';
			<%	} %>
	</script>
	<script type="text/javascript">
			<% if (session != null && session.getAttribute("MensajeLogin01") != null ) { %>
			<%= session.getAttribute("MensajeLogin01") %>
			<%	} %>
	</script>

</form>
<jsp:include page="footerPrepago.jsp"/>