<jsp:useBean id='fechaLib' class='java.util.GregorianCalendar' scope='session'/>
<jsp:useBean id='fechaLimite' class='java.util.GregorianCalendar' scope='session'/>
<jsp:useBean id='Pago' class='mx.altec.enlace.bo.pdPago' scope='session'/>
<jsp:include page="headerPrepago.jsp"/>
<%@page import="mx.altec.enlace.beans.NomPreTarjeta"%>
<%@page import="mx.altec.enlace.beans.NomPreEmpleado"%>
<script language="javascript">

<%
    response.setDateHeader ("Expires", 0);
    response.setHeader ("Pragma", "no-cache");
    if (request.getProtocol().equals ("HTTP/1.1")) {
       response.setHeader ("Cache-Control", "no-cache");
    }
%>

window.onload = <%=request.getAttribute("ArchivoErr")%>;
var fechaLib;
var fechaLim;
function archivoErrores()
 {
   ventanaInfo1=window.open('','trainerWindow','width=450,height=300,toolbar=no,scrollbars=yes');
   ventanaInfo1.document.open();
   ventanaInfo1.document.write("<html>");
   ventanaInfo1.document.writeln("<head>\n<title>Estatus</title>\n");
   ventanaInfo1.document.writeln("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
   ventanaInfo1.document.writeln("</head>");
   ventanaInfo1.document.writeln("<body bgcolor='white'>");
   ventanaInfo1.document.writeln("<form>");
   ventanaInfo1.document.writeln("<table border=0 width=420 class='textabdatcla' align=center>");
   ventanaInfo1.document.writeln("<tr><th class='tittabdat'>informacion del Archivo Importado</th></tr>");
   ventanaInfo1.document.writeln("<tr><td class='tabmovtex1' align=center>");
   ventanaInfo1.document.writeln("<table border=0 align=center><tr><td class='tabmovtex1' align=center width=>");
   ventanaInfo1.document.writeln("<img src='/gifs/EnlaceMig/gic25060.gif'></td>");
   ventanaInfo1.document.writeln("<td class='tabmovtex1' align='center'><br>No se puede realizar el pago individual ya que los datos capturados tienen los siguientes errores.<br><br>");
   ventanaInfo1.document.writeln("</td></tr></table>");
   ventanaInfo1.document.writeln("</td></tr>");
   ventanaInfo1.document.writeln("<tr><td class='textabdatobs'>");
   ventanaInfo1.document.writeln(document.MantNomina.archivoEstatus.value+"<br><br>");
   ventanaInfo1.document.writeln("</td></tr>");
   ventanaInfo1.document.writeln("</table>");
   ventanaInfo1.document.writeln("<table border=0 align=center >");
   ventanaInfo1.document.writeln("<tr><td align=center><br><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0 alt='Cerrar'></a>");
   ventanaInfo1.document.writeln("<a href = 'javascript:window.print();'><img border='0' name='imageField3' src='/gifs/EnlaceMig/gbo25240.gif' width='83' height='22' alt='Imprimir'></a></td></tr>");
   ventanaInfo1.document.writeln("</table>");
   ventanaInfo1.document.writeln("</form>");
   ventanaInfo1.document.writeln("</body>\n</html>");
   ventanaInfo1.document.close();
   ventanaInfo1.focus();
}

<%
  String diasInhabiles="";

  if (request.getAttribute("diasInhabiles")!= null)
     diasInhabiles=(String)request.getAttribute("diasInhabiles");
%>

var js_diasInhabiles="<%=diasInhabiles%>";
var diasInhabiles = "<%=diasInhabiles%>";

String.prototype.trim = function() {
 return this.replace(/^\s+|\s+$/g,"");
}
function validaNE(campo){
	return campo.value.match(/^[0-9]{1,<%=NomPreEmpleado.LNG_NO_EMPLEADO%>}$/);
}
function js_consultar()
{
	var pasa = false;
	var f=document.MantNomina;
   	campo1 =   document.MantNomina.valorEmpleado.value;
   	campo2 =   document.MantNomina.valorTarjeta.value;
   	campo3 =   document.MantNomina.valorCargo.value;
   	campo4 =   document.MantNomina.valorImporte.value;
   	campo5 =   document.MantNomina.fecha2.value;

   	if( f.valorTarjeta.value.trim() == "" ){
		cuadroDialogo("El campo n&uacute;mero de tarjeta es requerido.", 1);
		return;
	}
	if (f.valorTarjeta.value != "" && !f.valorTarjeta.value.match(/^[0-9]+$/)) {
		cuadroDialogo("El n&uacute;mero de tarjeta no es v&aacute;lido.", 1);
		return;
	} else if(f.valorTarjeta.value != "" && f.valorTarjeta.value.length < <%=NomPreTarjeta.LNG_NO_TARJETA_MIN%>) {
		cuadroDialogo("Longitud incorrecta de n&uacute;mero de tarjeta", 1);
		return;
	}
	if(f.valorEmpleado.value.trim() == ""){
		cuadroDialogo("El campo n&uacute;mero de empleado es requerido.",1);
        return;
	}
	if(f.valorEmpleado.value!='' && !validaNE(f.valorEmpleado)){
		cuadroDialogo("Los caracteres ingresados en el campo n&uacute;mero de empleado son inv&aacute;lidos.", 1);
		return;
	}

	if(campo1 != "" && campo2 != "" && campo3 != "" && campo4 != "" && campo5 != "") {
		if(!checkFloatValueCon(campo4)) {
			cuadroDialogo("El importe no debe contener letras ni m&aacute;s de dos puntos decimales",1);
			return;
		}
	} else {
		cuadroDialogo("Todos los campos son requeridos para realizar el Pago Individual.",1);
		return;
	}

		var mydate=new Date();
		var year = mydate.getYear();
		var aceptaFecha = true;
		if (year < 1000)
			year+=1900;
		var mes = mydate.getMonth();
		mes=mes+1;
		if (mes<10)
			mes="0"+mes;
		var dia = mydate.getDate();
		if (dia<10)
			dia="0"+dia;
		if(document.MantNomina.fecha2.value.substring(6,10)<year)
		{
			cuadroDialogo("El pago se debe realizar en una fecha pr&oacute;xima.",1);
			aceptaFecha=false;
		}
		if(document.MantNomina.fecha2.value.substring(6,10)==year && document.MantNomina.fecha2.value.substring(3,5)<mes)
		{
			cuadroDialogo("El pago se debe realizar en una fecha pr&oacute;xima.",1);
			aceptaFecha=false;
		}
		if(document.MantNomina.fecha2.value.substring(6,10)==year && document.MantNomina.fecha2.value.substring(3,5)==mes && document.MantNomina.fecha2.value.substring(0,2)<dia)
		{
			cuadroDialogo("El pago se debe realizar en una fecha pr&oacute;xima.",1);
			aceptaFecha=false;
		}
		if(aceptaFecha)
		{
			document.MantNomina.operacion.value="individual";
			document.MantNomina.action="NomPreImportNomina";
			document.MantNomina.submit();
		}
}

function js_regresarPagosInd()
{
	document.MantNomina.operacion.value="regresarPagosInd";
	document.MantNomina.tipoArchivo.value="regresa";
	document.MantNomina.action="NomPreImportOpciones";
	document.MantNomina.submit();
}

function js_envio()
{
	var statusHorario;
	statusHorario = "<%= request.getAttribute("HorarioPremier")%>";
	try
	{
		var horarioSeleccionado = document.MantNomina.HorarioPremier.options[document.MantNomina.HorarioPremier.selectedIndex].value;
		if ( horarioSeleccionado == "" || horarioSeleccionado ==  null)
		{
			cuadroDialogo("Debe seleccionar un horario antes de enviar el pago.", 3);
			return;
		}
		document.MantNomina.horario_seleccionado.value = horarioSeleccionado;
	}
	catch (e)
	{
		document.MantNomina.horario_seleccionado.value = "17:00";
	}
	if (trimString (document.MantNomina.cantidadDeRegistrosEnArchivo.value)==""
		|| trimString(document.MantNomina.cantidadDeRegistrosEnArchivo.value) == "00000"
		|| trimString(document.MantNomina.cantidadDeRegistrosEnArchivo.value) == "0")
	{
		cuadroDialogo("No hay registros que enviar, verifique por favor.", 1);
	}
	else
	{
		document.MantNomina.operacion.value="individual";
		document.MantNomina.tipoArchivo.value="enviado";
		cuadroDialogo("&#191;Esta seguro que desea enviar el pago?",2);
	}
}

function continua()
{
	if(respuesta==1)
	{
		if(document.MantNomina.operacion.value=="individual")
		{
			document.MantNomina.action="NomPreImportOpciones";
			document.MantNomina.submit();
		}
		else
			document.MantNomina.submit();
	/**
	*		Se envia una contestacion de aceptacion al usuario. El servidor puede procesar el archivo duplicado.
	*/
		var statusHorario;
		statusHorario = "<%= request.getAttribute("HorarioPremier")%>";
	}
	else if(respuesta==11)
	{
			if(statusHorario != null || statusHorario != undefined)
			{
				document.MantNomina.HorarioPremier.value = "<%= request.getAttribute("HorarioPremier")%>";
			}
				document.MantNomina.operacion.value="individual";
				document.MantNomina.statusDuplicado.value="0";
				/*varialbe agregada por Eric Gomez para validar los horarios de 17:00 3 / FEB / 2004*/
				document.MantNomina.statushrc.value="0";
				document.MantNomina.tipoArchivo.value="enviado";
				document.MantNomina.action="NomPreImportOpciones";
				document.MantNomina.submit();
	}
	else if(respuesta==12)
	{
		    document.MantNomina.operacion.value="regresarPagosInd";
			document.MantNomina.action="NomPreImportOpciones";
			document.MantNomina.submit();
	}
	else if(respuesta==13)
	{
		statusHorario = nh;
		if(statusHorario != null || statusHorario != undefined)
		{
			document.MantNomina.horario_seleccionado.value = nh;
		}
		document.MantNomina.statusDuplicado.value="0";
		document.MantNomina.operacion.value="individual";
		document.MantNomina.tipoArchivo.value="enviado";
		document.MantNomina.action="NomPreImportOpciones";
		document.MantNomina.submit();
	}
}

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}

function checkFloatValueCon(myobj)
{
  if(myobj != "")
  {
    if(!isFloatCon(myobj))
    {
      myobj = "";
      document.MantNomina.valorImporte.focus();
      //cuadroDialogo("El Importe debe ser num&eacute;rico",3);
      return false;
    }
  }
  return true;
}

function isFloatCon (s)
{
	var i;
    var seenDecimalPoint = false;
    if (isEmptyCon(s))
    {
       if (isFloat.arguments.length == 1)
       		return true;
       else
       		return (isFloat.arguments[1] == true);
    }
    if (s == ".")
    {
    	return false;
    }
    for (i = 0; i < s.length; i++)
    {
        // Check that current character is number.
        var c = s.charAt(i);
        if ((c == ".") && !seenDecimalPoint)
        {
        	seenDecimalPoint = true;
        }
        else
        {
        	if (!isDigit(c))
        	{
        		//cuadroDialogo("El n&uacute;mero no debe contener letras",1);
        		return false;
        	}
        }
    }
    return true;
}

function isEmptyCon(s)
{
	return ((s == null) || (s.length == 0))
}

function checkValueCon(myobj){
  if(myobj != ""){
    if(!isPositiveInteger(myobj)){
      myobj = "";
	  cuadroDialogo("El n&uacute;mero de registros debe ser num&eacute;rico",3);
	  return false;
    }
  }
  return true;
}

function checkValueCon3(myobj)
{
  if(myobj != "")
  {
    if(!isPositiveInteger(myobj))
    {
      myobj = "";
      document.MantNomina.secuencia.focus();
	  cuadroDialogo("El n&uacute;mero de secuencia debe ser num&eacute;rico",3);
	  return false;
   }
  }
 return true;
}

function isPositiveInteger(s)
{
	var secondArg = true;
   	if (isPositiveInteger.arguments.length > 1)
        secondArg = isPositiveInteger.arguments[1];
    return (isSignedInteger(s, secondArg) && ( (isEmpty(s) && secondArg)  || (parseInt (s) > 0) ) );
}

function isSignedInteger(s)
{
	if (isEmpty(s))
       if (isSignedInteger.arguments.length == 1)
       		return true;
       else
       		return (isSignedInteger.arguments[1] == true);
    else
    {
        var startPos = 0;
        var secondArg = true;
        if (isSignedInteger.arguments.length > 1)
            secondArg = isSignedInteger.arguments[1];
        if ( (s.charAt(0) == "-") || (s.charAt(0) == "+") )
           startPos = 1;
        return (isInteger(s.substring(startPos, s.length), secondArg));
    }
}

function isInteger (s)
{
	var i;

    if (isEmpty(s))
       if (isInteger.arguments.length == 1)
       		return true;
       else
       		return (isInteger.arguments[1] == true);
    for (i = 0; i < s.length; i++)
    {
        var c = s.charAt(i);
        if (!isDigit(c))
        	return false;
    }
    return true;
}

function isDigit (c)
{
	return ((c >= "0") && (c <= "9"))
}

function isEmpty(s)
{
	return ((s == null) || (s.length == 0))
}

function SeleccionaFechaApli(ind)
 {
   var m=new Date();
    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
   n=m.getMonth();
   Indice=ind;
   msg=window.open("/EnlaceMig/calNom3.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
   msg.focus();
 }


 function Actualiza()
 {
     document.MantNomina.fecha2.value=Fecha[Indice];
 }

function MuestraCalendario(){

	msg=window.open( "<%=request.getContextPath()%>/jsp/prepago/NomPreCalendarioPagInd.jsp","Calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=330,height=260");
	msg.focus();
}
function continuaCal () {
 document.MantNomina.fecha2.value = fechaLim;

}
function actualizacuenta()
{
  document.MantNomina.valorCargo.value=ctaselec;
  document.MantNomina.textcuenta.value=ctadescr;
}

function EnfSelCta()
{
   document.MantNomina.textcuenta.value="";
}

function validaNumero(e) {
		var key = window.event ? e.keyCode : e.which;
		if (key == 8) return true;
		return /\d/.test(String.fromCharCode(key));
	}

function trimString (str)
  {
    str = this != window? this : str;
	return str.replace(/^\s+/g, '').replace(/\s+$/g, '');
  }
function cambiaEmpleado()
{
	var f=document.MantNomina;
	var cadena=f.valorEmpleado.value;
	var folioReturn = "";
	if(cadena.length>0){
		var longitud = cadena.length;
		if (longitud < 7)
			for (i = 1; i <= 7 - longitud; i++)
				folioReturn += "0";
		folioReturn += cadena;
		f.valorEmpleado.value=folioReturn;

	}
}
</SCRIPT>

<form name="Frmfechas" action="">
<%if (session.getAttribute("Bitfechas")!= null)
    out.println(session.getAttribute("Bitfechas"));%>
</form>

<FORM  NAME="MantNomina" METHOD="Post" ACTION="MantNomina">
<INPUT TYPE="hidden" name="horario_seleccionado" value="<%=request.getAttribute("horario_seleccionado")%>">
<INPUT TYPE="hidden" name="horario_seleccionado_nuevo" value="">
<table width="760" border="0" cellspacing="0" cellpadding="0">
<%
	if (request.getAttribute("ContenidoArchivo")== "")
	{
%>
		<table width="760" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center">
					<table cellspacing="2" cellpadding="3" border="0" width="600" class="textabdatcla">
						<tr>
							<td colspan="4" class="tittabdat">Pago de Nomina Individual<br/></td>
						</tr>
						<tr>
							<td class="tabmovtex" width="144">* No de empleado</td>
							<td class="CeldaContenido" align="left" width="181">
								<input type="text" onchange="" class="tabmovtex" value="" maxlength="<%=NomPreEmpleado.LNG_NO_EMPLEADO%>" size="15" name="valorEmpleado" onkeypress="return validaNumero(event);" onblur="cambiaEmpleado();"/>
							</td>
							<td class="tabmovtex" width="99">* No de tarjeta</td>
							<td class="CeldaContenido" align="left" width="154">
								<input type="text" onchange="" class="tabmovtex" value="" maxlength="<%=NomPreTarjeta.LNG_NO_TARJETA%>" size="25" name="valorTarjeta" onkeypress="return validaNumero(event);"/>
							</td>
						</tr>
						<tr>
							<td class="tabmovtex" width="144">* Cuenta de cargo</td>
							<td class="CeldaContenido" align="left" width="181">
								<input class="tabmovtexbol" type="text" value="" size="15" maxlength="30" name="valorCargo" onfocus="blur();"/>
								<nobr/><a href="javascript:PresentarCuentas();"><nobr/><img border="0" style="vertical-align: middle;" src="../../gifs/EnlaceMig/gbo25420.gif"/></a>
							</td>
							<td class="tabmovtex" width="99">* Importe</td>
							<td class="CeldaContenido" align="left" width="154">
								<input type="text" onchange="" class="tabmovtex" value="" maxlength="25" size="25" name="valorImporte"/>
							</td>
						</tr>
						<tr>
							<td class="tabmovtex" width="144">* Fecha de aplicaci&oacute;n</td>
							<td class="CeldaContenido" align="left" width="181">
								<input type="text" size="10" name="fecha2" onfocus="blur();">
								<a href="javascript:MuestraCalendario();"><img src="../../gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" style="vertical-align:middle;"></a>
							</td>
							<td class="tabmovtex" width="99"></td>
							<td class="CeldaContenido" align="left" width="154">
							</td>
						</tr>
						<tr>
							<td colspan="2"></td>
							<td colspan="2">El importe no debe contener comas ni simbolos especiales, solo esta permitido el punto.</td>
						</tr>
						<tr>
							<td class="textabdatcla" colspan="4"><br><b>* Campos requeridos</b></td>
						</tr>
					</table>
					<br>
				<table height="22" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" width="166">
		    		<tr>
        				<td align="center">
        					<a href="javascript:js_consultar();"><img height="22" border="0" width="76" alt="Aceptar" src="../../gifs/EnlaceMig/gbo25280.gif" value="Aceptar" name="Aceptar"/></a>
         				</td>
         			</tr>
         		</table>
			    </td>
			</tr>
		</table>
<%
	}
%>
		<br>
<%
			if (request.getAttribute("TablaTotales")!= null)
					out.println(request.getAttribute("TablaTotales"));
			if (request.getAttribute("ContenidoArchivo")!= null || request.getAttribute("ContenidoArchivo") != "")
					out.println(request.getAttribute("ContenidoArchivo"));
			if (request.getAttribute("botones")!= null)
					out.println(request.getAttribute("botones"));
%>
</table>

<INPUT TYPE="hidden" VALUE="1" NAME="statushrc">
<INPUT TYPE="hidden" VALUE="1" NAME="statusDuplicado">
<INPUT TYPE="hidden" VALUE="0" NAME="operacion">
<INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute("folio") != null) { out.print(request.getAttribute("folio")); } %>" NAME="folio">
<INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute("encFechApli") != null) { out.print(request.getAttribute("encFechApli")); } %>" NAME="encFechApli">
<INPUT TYPE="hidden" VALUE="-1" NAME="registro">
<INPUT TYPE="hidden" VALUE="" NAME="secuencia">
<INPUT TYPE="hidden" VALUE="<%
								if(	session.getAttribute("fechaHoy") !=null)
									out.println( session.getAttribute("fechaHoy"));
								else
									if (request.getAttribute("fechaHoy")!= null)
										out.println(request.getAttribute("fechaHoy"));
							%>" NAME="fechaHoy">
<INPUT TYPE="hidden" NAME="nuevoArchivo">
<INPUT TYPE="hidden" VALUE="<%
								if(	session.getAttribute("tipoArchivo") !=null)
									out.println( session.getAttribute("tipoArchivo"));
								else
									if (request.getAttribute("tipoArchivo")!= null)
										out.println(request.getAttribute("tipoArchivo"));
							%>"  NAME="tipoArchivo">
<INPUT TYPE="hidden" VALUE="<%
								if(	session.getAttribute("horaSistema") !=null)
									out.println( session.getAttribute("horaSistema"));
								else
									if (request.getAttribute("horaSistema")!= null)
										out.println(request.getAttribute("horaSistema"));
							%>" NAME="horaSistema">
<input type="hidden" name="archivoEstatus" value='<%
								if(	session.getAttribute("archivoEstatus") !=null)
									out.println( session.getAttribute("archivoEstatus"));
								else
									if (request.getAttribute("archivoEstatus")!= null)
										out.println(request.getAttribute("archivoEstatus"));
							%>'>
<INPUT TYPE="hidden" VALUE="<%
								if (request.getAttribute("cantidadDeRegistrosEnArchivo")!= null)
								    out.println(request.getAttribute("cantidadDeRegistrosEnArchivo"));
							%>" NAME="cantidadDeRegistrosEnArchivo">
<INPUT TYPE="hidden" VALUE="<%
								if (request.getAttribute("textcuenta")!= null)
								   out.println(request.getAttribute("textcuenta"));
							%>" NAME="textcuenta">
<INPUT TYPE="hidden" VALUE="<%
								if (request.getAttribute("encFechApliformato")!= null)
								   out.println(request.getAttribute("encFechApliformato"));
							%>" NAME="encFechApli">
<%
	if(	session.getAttribute("facultadImportar") !=null)
		out.println( session.getAttribute("facultadImportar"));
	else
		if (request.getAttribute("facultadImportar")!= null)
		{
			out.println(request.getAttribute("facultadImportar"));
		}
%>
<%
	if(	session.getAttribute("facultadAlta") !=null)
		out.println( session.getAttribute("facultadAlta"));
	else
		if (request.getAttribute("facultadAlta")!= null)
		{
			out.println(request.getAttribute("facultadAlta"));
		}
%>
<%
	if(	session.getAttribute("facultadCambios") !=null)
		out.println( session.getAttribute("facultadCambios"));
	else
		if (request.getAttribute("facultadCambios")!= null)
		{
			out.println(request.getAttribute("facultadCambios"));
		}
%>
<%
	if(	session.getAttribute("facultadBajas") !=null)
		out.println( session.getAttribute("facultadBajas"));
	else
		if (request.getAttribute("facultadBajas")!= null)
		{
			out.println(request.getAttribute("facultadBajas"));
		}
%>
<%
	if(	session.getAttribute("facultadEnvio") !=null)
		out.println( session.getAttribute("facultadEnvio"));
	else
		if (request.getAttribute("facultadEnvio")!= null)
		{
			out.println(request.getAttribute("facultadEnvio"));
		}
%>
<%
	if(	session.getAttribute("facultadRecuperar") !=null)
		out.println( session.getAttribute("facultadRecuperar"));
	else
		if (request.getAttribute("facultadRecuperar")!= null)
		{
			out.println(request.getAttribute("facultadRecuperar"));
		}
%>
<%
	if(	session.getAttribute("facultadExportar") !=null)
		out.println( session.getAttribute("facultadExportar"));
	else
		if (request.getAttribute("facultadExportar")!= null)
		{
			out.println(request.getAttribute("facultadExportar"));
		}
%>
<%
	if(	session.getAttribute("facultadBorra") !=null)
		out.println( session.getAttribute("facultadBorra"));
	else
		if (request.getAttribute("facultadBorra")!= null)
		{
			out.println(request.getAttribute("facultadBorra"));
		}
%>
<script>
<%
	if(	session.getAttribute("infoUser") !=null)
		out.println( session.getAttribute("infoUser"));
	else
		if (request.getAttribute("infoUser")!= null) {
			out.println(request.getAttribute("infoUser"));
		}
%>

<%
	if(	session.getAttribute("infoImportados") !=null)
		out.println( session.getAttribute("infoImportados"));
	else
		if (request.getAttribute("infoImportados")!= null) {
			out.println(request.getAttribute("infoImportados"));
		}
%>
</script>
</form>
<jsp:include page="footerPrepago.jsp"/>

<head>
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="-1">
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="-1">
</head>
