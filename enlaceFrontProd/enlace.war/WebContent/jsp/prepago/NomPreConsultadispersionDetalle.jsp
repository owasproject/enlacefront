<jsp:include page="headerPrepago.jsp"/>
<%@page import="mx.altec.enlace.beans.NomPreEmpleado,mx.altec.enlace.bita.BitaConstants"%>
<script language="javascript">

function echeck(str)
{
  var at="@";
  var dot=".";
  var lat=str.indexOf(at);
  var lstr=str.length;
  var ldot=str.indexOf(dot);

  if (str.indexOf(at)==-1)
     return false;
  if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr)
     return false;
  if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr)
      return false;
  if (str.indexOf(at,(lat+1))!=-1)
      return false;
  if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot)
      return false;
  if (str.indexOf(dot,(lat+2))==-1)
      return false;
  if (str.indexOf(" ")!=-1)
      return false;
  return true;
}
function validaMail(campo){
	return campo.value.match(/^[0-9A-Za-z][A-Za-z0-9_.-]*@[A-Za-z0-9_-]+\.[A-Za-z0-9_.]+[A-za-z]$/);
}
function validaEmail()
{
  document.MantNomina.CorreoE.value=trimString(document.MantNomina.CorreoE.value);
  var emailID=document.MantNomina.CorreoE;

  if( trimString(emailID.value)!="")
  {
	  if (!validaMail(emailID))
	  {
		emailID.focus();
		cuadroDialogo("El correo especificado no es v&aacute;lido.", 3);
		return false;
	  }
  }
  else
  {
      cuadroDialogo( "Es necesario incluir el correo para env&iacute;o de detalle" ,3);
     return false;

  }
  return true;
}

function js_descargaArchivo(Nombre)
{
	document.MantNomina.operacion.value="descarga";
	document.MantNomina.action="NomPreImportOpciones";
	document.MantNomina.submit();
}

function js_enviarCorreo()
{
   var seleccionado;
   var procesado;
   var cad="";
   var i=0;

   cad="";
   procesado=false;
   seleccionado=false;

   for(i=0;i<(document.MantNomina.elements.length);i++)
	 {
	   if(document.MantNomina[i].type=="radio")
		 if(document.MantNomina[i].checked)
		 {
		   seleccionado=true;
		   if(document.MantNomina[i].value.indexOf("Procesado")>0)
		    {
			  if(validaEmail())
				{
			      cad=document.MantNomina[i].value;
				  procesado=true;
				}

			  break;
		    }
		   else
			  cuadroDialogo("Esta opci&oacute;n solo aplica para los archivos Procesados.", 3);
		 }
	 }

	if(seleccionado)
	 {
	   if(procesado)
		 {
		   cad=cad.substring(0,cad.indexOf("|"));
		   document.MantNomina[i].value=cad;
		   document.MantNomina.operacion.value="enviarCorreo";
		   document.MantNomina.action="NomPreImportOpciones";
		   document.MantNomina.submit();
		 }
	 }
	else
	  cuadroDialogo("Debe seleccionar un registro para acceder a esta opci&oacute;n", 3);
}

var respuesta=0;

function continua()
{
  if(respuesta==1)
	{
      cad=cad.substring(0,cad.indexOf("|"));
	  document.MantNomina[elemento].value=cad;
	  document.MantNomina.operacion.value="enviarCorreo";
	  document.MantNomina.action="NomPreImportOpciones";
	  document.MantNomina.submit();
	}
}

function js_cancelar()
 {
  var seleccionado=false;
  var cancelado=false;
  var cad="";
  var i=0;

	for(i=0;i<(document.MantNomina.elements.length);i++)
	 {
		if (document.MantNomina[i].type=="radio")
		 if (document.MantNomina[i].checked==true)
		 {
		   seleccionado=true;
		   if (document.MantNomina[i].value.indexOf("Procesado")<0)
			{
			  cancelado=true;
			  cad=document.MantNomina[i].value;
			  break;
			}
		   else
			  cuadroDialogo("Esta opci&oacute;n solo aplica para los archivos Recibidos.", 3);
		 }
	 }

	if (seleccionado)
	 {
	   if(cancelado)
		 {
			var con = confirm("�Esta seguro que desea cancelar el pago?");
			if(con)
			{
				cad=cad.substring(0,cad.indexOf("|"));
				document.MantNomina[i].value=cad;
				document.MantNomina.operacion.value="cancelar";
				document.MantNomina.tipoArchivo.value="cancelado";
				document.MantNomina.action="NomPreImportOpciones";
				document.MantNomina.submit();
			}
			else
				return;
		 }
	 }
	else
     cuadroDialogo("Debe seleccionar un registro para acceder a esta opci&oacute;n", 3);
}


function js_regresar(){
		//document.MantNomina.operacion.value="regresar";
		//document.MantNomina.action="NomPreImportOpciones";
		document.MantNomina.action="NomPredispersion?Modulo=8";
		document.MantNomina.submit();
}

function atras()
{
    if((parseInt(document.MantNomina.prev.value) - Nmuestra)>0){
      document.MantNomina.next.value = document.MantNomina.prev.value;
      document.MantNomina.prev.value = parseInt(document.MantNomina.prev.value) - Nmuestra;
      document.MantNomina.action = "GetBit";
      document.MantNomina.submit();
    }
}

function adelante()
{
 if(parseInt(document.MantNomina.next.value) != parseInt(document.MantNomina.total.value))
 {
    if((parseInt(document.MantNomina.next.value) + Nmuestra)<= parseInt(document.MantNomina.total.value))
    {
      document.MantNomina.prev.value = document.frmbit.next.value;
      document.MantNomina.next.value = parseInt(document.frmbit.next.value) + Nmuestra;
      document.MantNomina.action = "GetBit";
      document.MantNomina.submit();
     }else if((parseInt(document.MantNomina.next.value) + Nmuestra) > parseInt(document.MantNomina.total.value))
     {
      document.MantNomina.prev.value = document.MantNomina.next.value;
      document.MantNomina.next.value = parseInt(document.MantNomina.next.value) + (parseInt(document.MantNomina.total.value) - parseInt(document.MantNomina.next.value));
      document.MantNomina.action = "GetBit";
      document.MantNomina.submit();
     }
 }else
 {
   cuadroDialogo("No hay m&aacute;s registros.",3);
 }
}

</script>

<FORM  NAME="MantNomina" METHOD="Post" ACTION="MantNomina" onsubmit="return false">
   <table width="760" border="0" cellspacing="0" cellpadding="0">
        <%
			if (request.getAttribute("ContenidoArchivo")!= null && request.getAttribute("ContenidoArchivo")!= "")
			{
					out.println(request.getAttribute("ContenidoArchivo"));
			}
			else
			{
		%>
			<table align=center>
			<tr align=center>
			 <td class="tabmovtex"  align=center nowrap>
			   <br>NO EXISTEN DATOS PARA LA CONSULTA<br>
			 </td>
			</tr>
			<tr></tr>
			<tr align=center>
			 <td>
			 	<a href = "NomPredispersion?Modulo=8&<%=BitaConstants.FLUJO%>=<%=BitaConstants.ES_NP_PAGOS_CONSULTA%>" border = 0><img src = ../../gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a>
			 </td>
			</tr>
		   </table>
		   <%
		   }
			if(request.getAttribute("ContenidoArchivo")!= null && request.getAttribute("ContenidoArchivo")!= "" && session.getAttribute("tipoBusqueda").equals("PI"))
			{
		   %>

		   <table align=center>
			<tr>
			 <td class="tabmovtex"  align=center nowrap>
			   <br>Correo: <input type=text name=CorreoE size=40 maxlength="40"><br>
			 </td>
			</tr>
		   </table>
		   <%}%>
		<%
		if (request.getAttribute("botones")!= null)
		 {
			out.println(request.getAttribute("botones"));
		 }
		%>
  </table>

  <INPUT TYPE="hidden" VALUE="0" NAME="operacion">
  <INPUT TYPE="hidden" VALUE="0" NAME="tipo">
  <INPUT TYPE="hidden" VALUE="-1" NAME="registro">
  <INPUT TYPE="hidden" VALUE="<%
								if(	session.getAttribute("fechaHoy") !=null)
									out.println( session.getAttribute("fechaHoy"));
								else
									if (request.getAttribute("fechaHoy")!= null)
										out.println(request.getAttribute("fechaHoy"));
							%>" NAME="fechaHoy">
  <INPUT TYPE="hidden" NAME="nuevoArchivo">
  <INPUT TYPE="hidden" VALUE="<%
								if(	session.getAttribute("tipoArchivo") !=null)
									out.println( session.getAttribute("tipoArchivo"));
								else
									if (request.getAttribute("tipoArchivo")!= null)
										out.println(request.getAttribute("tipoArchivo"));
							%>"  NAME="tipoArchivo">
  <INPUT TYPE="hidden" VALUE="<%
								if(	session.getAttribute("horaSistema") !=null)
									out.println( session.getAttribute("horaSistema"));
								else
									if (request.getAttribute("horaSistema")!= null)
										out.println(request.getAttribute("horaSistema"));
							%>" NAME="horaSistema">

  <input type="hidden" name="archivoEstatus" value='<%
								if(	session.getAttribute("archivoEstatus") !=null)
									out.println( session.getAttribute("archivoEstatus"));
								else
									if (request.getAttribute("archivoEstatus")!= null)
										out.println(request.getAttribute("archivoEstatus"));
							%>'>

  <INPUT TYPE="hidden" VALUE="<%
								if (request.getAttribute("cantidadDeRegistrosEnArchivo")!= null)
								    out.println(request.getAttribute("cantidadDeRegistrosEnArchivo"));
								else
								if(session.getAttribute("cantidadDeRegistrosEnArchivo") !=null)
									out.println( session.getAttribute("cantidadDeRegistrosEnArchivo"));


							%>" NAME="cantidadDeRegistrosEnArchivo">
  <INPUT TYPE="hidden" VALUE="<% if (request.getAttribute("nombre_arch_salida")!= null)
				    out.println(request.getAttribute("nombre_arch_salida"));
			    %>" NAME="nombre_arch_salida">
  <INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute( "lista_nombres_archivos" ) != null ) { out.print( request.getAttribute( "lista_nombres_archivos" ) ); } %>" NAME="lista_nombres_archivos">
  <INPUT TYPE="hidden" VALUE="<%
								if (request.getAttribute("fechaHoy")!= null)
								out.println(request.getAttribute("fechaHoy"));
							%>" NAME="fechaHoy">

<%
	if(	session.getAttribute("facultadImportar") !=null)
		out.println( session.getAttribute("facultadImportar"));
	else
	if (request.getAttribute("facultadImportar")!= null) {
		out.println(request.getAttribute("facultadImportar"));
	}
%>
<%
	if(	session.getAttribute("facultadAlta") !=null)
		out.println( session.getAttribute("facultadAlta"));
	else
	if (request.getAttribute("facultadAlta")!= null) {
		out.println(request.getAttribute("facultadAlta"));
	}
%>
<%
	if(	session.getAttribute("facultadCambios") !=null)
		out.println( session.getAttribute("facultadCambios"));
	else
	if (request.getAttribute("facultadCambios")!= null) {
		out.println(request.getAttribute("facultadCambios"));
	}
%>
<%
	if(	session.getAttribute("facultadBajas") !=null)
		out.println( session.getAttribute("facultadBajas"));
	else
	if (request.getAttribute("facultadBajas")!= null) {
		out.println(request.getAttribute("facultadBajas"));
	}
%>
<%
	if(	session.getAttribute("facultadEnvio") !=null)
		out.println( session.getAttribute("facultadEnvio"));
	else
	if (request.getAttribute("facultadEnvio")!= null) {
		out.println(request.getAttribute("facultadEnvio"));
	}
%>
<%
	if(	session.getAttribute("facultadRecuperar") !=null)
		out.println( session.getAttribute("facultadRecuperar"));
	else
	if (request.getAttribute("facultadRecuperar")!= null) {
		out.println(request.getAttribute("facultadRecuperar"));
	}
%>
<%
	if(	session.getAttribute("facultadExportar") !=null)
		out.println( session.getAttribute("facultadExportar"));
	else
	if (request.getAttribute("facultadExportar")!= null) {
		out.println(request.getAttribute("facultadExportar"));
	}
%>
<%
	if(	session.getAttribute("facultadBorra") !=null)
		out.println( session.getAttribute("facultadBorra"));
	else
	if (request.getAttribute("facultadBorra")!= null) {
		out.println(request.getAttribute("facultadBorra"));
	}
%>
<script>
<%
	if(	session.getAttribute("infoUser") !=null)
		out.println( session.getAttribute("infoUser"));
	else
		if (request.getAttribute("infoUser")!= null) {
			out.println(request.getAttribute("infoUser"));
		}
%>
</script>
</form>
<jsp:include page="footerPrepago.jsp"/>
