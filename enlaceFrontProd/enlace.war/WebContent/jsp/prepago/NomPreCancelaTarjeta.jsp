<%@page import="mx.altec.enlace.beans.*" %>
<%@ include file="/jsp/prepago/headerPrepago.jsp" %>
<script language="javaScript">
<%
       if (request.getAttribute("mensajecancela")!= null){
	     	out.println(request.getAttribute("mensajecancela"));
	    	request.removeAttribute("mensajecancela");
	   }
%>
String.prototype.trim = function() {
 return this.replace(/^\s+|\s+$/g,"");
}
function valida(){
	var f=document.fcancelatarjeta;
	if(f.valorBusqueda.value.trim().length == 0){
		cuadroDialogo("El campo n&uacute;mero de tarjeta es requerido.",1);
        return false;
	}		
	if (f.valorBusqueda.value != "" && !f.valorBusqueda.value.match(/^[0-9]+$/)) {			
		cuadroDialogo("El n&uacute;mero de tarjeta no es v&aacute;lido.", 1);			
		return false;			
	} else if(f.valorBusqueda.value != "" && f.valorBusqueda.value.length < <%=NomPreTarjeta.LNG_NO_TARJETA_MIN%>) {
		cuadroDialogo("Longitud incorrecta de n&uacute;mero de tarjeta", 1);
		return false;				
	}				
	return true;
}
function soloNumeros(evt) { 
    evt = (evt) ? evt : event; 
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : 
        ((evt.which) ? evt.which : 0)); 
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {       
        return false; 
    } 
    return true; 
 }
function envia() {
	var f=document.fcancelatarjeta;
	if(valida()){
		f.opcion.value="consulta";
		f.tarjeta.value=f.valorBusqueda.value;	
		f.action="NomPreCancelaTarjeta";
		f.submit();	
	}
}

</Script>	
<form name="fcancelatarjeta" METHOD="POST" ACTION="">								
<table width="760" border="0" cellspacing="0" cellpadding="0">	
	
		<tr>
		<td align="center">		
		<table cellspacing="2" cellpadding="3" border="0" width="500" class="textabdatcla">
			<tr>
				<td colspan="4" class="tittabdat"><%=request.getAttribute("titTabla")%><br/></td>
			</tr>
			<tr>
				<td class="tabmovtex" width="100px">N&uacute;mero de tarjeta</td>
				<td class="CeldaContenido" align="left">
					<input type="text" onchange="" class="tabmovtex" value="" maxlength="<%=NomPreTarjeta.LNG_NO_TARJETA%>" size="35" name="valorBusqueda" onkeypress="return soloNumeros(event)"/>
				</td>			
			</tr>					
		</table>		
		<br>			
		<table height="22" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" width="166">
    		<tr>		    			
      				<td align="center">
      					<a href="javascript:envia();" border=0 > <img  src="/gifs/EnlaceMig/gbo25280.gif" border=0></a>
       				</td>         				  				
       			</tr>
       		</table>				
	    </td>
	</tr>
</table>
<INPUT TYPE="hidden" NAME="opcion">
<INPUT TYPE="hidden" NAME="tarjeta">
</form>

<%@ include file="/jsp/prepago/footerPrepago.jsp" %>

