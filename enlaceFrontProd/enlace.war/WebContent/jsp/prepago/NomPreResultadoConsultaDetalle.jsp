<%@ page import="mx.altec.enlace.beans.*,java.util.Iterator"%>
<%@ page import="mx.altec.enlace.utilerias.NomPreUtil,mx.altec.enlace.bita.BitaConstants"%>
<jsp:include page="headerPrepago.jsp"/>
<script type="text/javascript">
<!--	
	function exportar() {
		var forma = document.detalleRemesa;
		forma.submit();
	}
	function paginar(direccion) { 
		var forma = document.paginacion;
		forma.accion.value = direccion;
		forma.submit();
	}	
//-->
</script>
<center>
<%	
	if (request.getSession().getAttribute("regresoConsulta") == null) {
		String regresoConsulta = "NomPreRemesas?operacion=consulta";
		if ("consulta".equals(request.getParameter("operacion"))) {
			regresoConsulta = "NomPreRemesas?operacion=iniconsul&" +BitaConstants.FLUJO + "=" + BitaConstants.ES_NP_REMESAS_CONSULTA;
		}
		request.getSession().setAttribute("regresoConsulta", regresoConsulta);		
	}
	
	String regreso = request.getSession().getAttribute("regresoConsulta").toString();			
%>	
<% 
NomPreLM1B lm1b = (NomPreLM1B) request.getAttribute("lm1b");
if (lm1b != null && lm1b.getDetalle() != null && lm1b.getDetalle().size() > 0) {
%>
<form action="${ctx}NomPreRemesas" method="get" name="detalleRemesa">
	<input type="hidden" name="operacion" value="detalle">
	<input type="hidden" name="noRemesa" value="<%=request.getParameter("noRemesa")%>"/>
	<input type="hidden" name="salida" value="excel">	
</form>		
<table cellspacing="0" cellpadding="0" border="0" width="760">
	<tr>
		<td align="center" colspan="10" class="texenccon">Total de tarjetas:<%=lm1b.getDetalle().size()%><br>&nbsp;<br></td>
	</tr>
	<tr>
		<td align="center">						
			<table cellspacing="2" cellpadding="3" border="0" width="369" class="tabfonbla">
				<tr>
					<td align="center" width="130" class="tittabdat">N&uacute;mero Tarjeta</td>
					<td align="center" width="117" class="tittabdat">Fecha de Expiraci&oacute;n</td>
					<td align="center" width="115" class="tittabdat">Estatus</td>
				</tr>
				<% 
				int tamano = lm1b.getDetalle().size();
				int inicio = NomPreUtil.obtenIndiceInicial(request); 
				int fin = NomPreUtil.obtenIndiceFinal(request, tamano);	
				
				Iterator iterator = lm1b.getDetalle().subList(inicio, fin).iterator();
				
				boolean claro = false;
				
				while (iterator.hasNext()) {
					NomPreTarjeta tarjeta = (NomPreTarjeta) iterator.next();
					String sClass = claro ? "textabdatcla" : "textabdatobs";
				%>				
				<tr>						
					<td align="center" width="130" class="<%=sClass%>"><%=tarjeta.getNoTarjeta()%></td>
					<td align="center" width="117" class="<%=sClass%>"><%=tarjeta.getFechaVigencia()%></td>
					<td align="center" width="115" class="<%=sClass%>"><%=tarjeta.getDescEstadoTarjeta()%></td>
				</tr>								
				<%
					claro = !claro;
				}
				%>		
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table height="40" cellspacing="0" cellpadding="0" border="0" width="760">
				<tr><td></td></tr>
				<tr>
					<td align="center" class="texfootpagneg">		
						<form action="${ctx}NomPreRemesas" method="get" name="paginacion">								
							<input type="hidden" name="operacion" value="detalle">
							<input type="hidden" name="noRemesa" value="<%=request.getParameter("noRemesa")%>"/>												
							<input type="hidden" name="inicio" value="<%=inicio%>"/>
							<input type="hidden" name="fin" value="<%=fin%>"/>
							<input type="hidden" name="accion" value=""/>								
						</form>					
						Tarjetas : <%=(fin == 0 ? inicio : inicio + 1) + " - " + fin + " de " + tamano%><br/><br/>
						<% if (inicio != 0) {%>
						<a href="javascript:paginar('A');">< Anteriores 50</a>
						<%}
						if (fin != 0 && fin < lm1b.getDetalle().size()) {%>
						<a href="javascript:paginar('S');">Siguientes 50 ></a>
						<%}%>		
						<br><br>
						<table height="40" cellspacing="0" cellpadding="0" border="0" width="168">					
							<tr>						
								<td height="22" align="right" width="85" valign="top">																
									<a href="javascript:exportar();"><img height="22" border="0" width="85" alt="Exportar" src="/gifs/EnlaceMig/gbo25230.gif" name="imageField4"/></a>
								</td>
								<td height="22" align="right" width="83" valign="top">														
									<a href="${ctx}<%=regreso%>"><img height="22" border="0" width="83" alt="Regresar" src="/gifs/EnlaceMig/gbo25320.gif" name="imageField42"/></a>
								</td>
							</tr>
						</table>																					
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%} else {%>
<table width="760" border="0" cellspacing="0" cellpadding="0">			
	<tr>
		<td align="center">		
			<table cellspacing="2" cellpadding="3" border="0" width="369" class="tabfonbla">
				<tr>
					<td align="left" class="tittabdat">Consulta detalle remesa</td>
				</tr> 
				<tr>
					<td align="center" class="textabdatobs">
						<br/>No se encontraron registros en la b&uacute;squeda<br/><br/>		
					</td>
				</tr>				
			</table>
			<br/>
			<a href="${ctx}<%=regreso%>"><img height="22" border="0" width="83" alt="Regresar" src="/gifs/EnlaceMig/gbo25320.gif" name="imageField42"/></a>
		</td>
	</tr>
</table>
<%}%>
</center>
<jsp:include page="footerPrepago.jsp"/>