<%@ include file="/jsp/prepago/headerPrepago.jsp" %>
<%@page import="mx.altec.enlace.bita.BitaConstants"%>
<script language="javaScript">
<%
  String operConfirma = (String) request.getAttribute("operconfirma");
  String tipoOper=(String)request.getSession().getAttribute("operNP");  
  String url=null;
  if(tipoOper!=null){
	  if(tipoOper.trim().equals("cancela")){
	  	url="NomPreCancelaTarjeta?operacionNP=cancela&" +
						BitaConstants.FLUJO +"=" + BitaConstants.ES_NP_TARJETA_BAJA;
	  }else{
	  url="NomPreCancelaTarjeta?operacionNP=bloqueo&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NP_TARJETA_BLOQUEO;
	  }
  }
  if(operConfirma!=null){
  	request.removeAttribute("operconfirma");
  }
  if (request.getAttribute("mensajecancela")!= null){
 		out.println(request.getAttribute("mensajecancela"));		
		request.removeAttribute("mensajecancela");
  }
  String noEmpleado = request.getAttribute("numempleado") == null ? "" : (String) request.getAttribute("numempleado"); 
  String nombreEmpleado = request.getAttribute("nomempleado") == null ? "" : (String) request.getAttribute("nomempleado"); 
  String noTarjeta = request.getAttribute("numtarjeta") == null ? "" : (String) request.getAttribute("numtarjeta");
  String edoTarjeta = request.getAttribute("edotarjeta") == null ? "" : (String) request.getAttribute("edotarjeta");
 %> 
String.prototype.trim = function() {
 return this.replace(/^\s+|\s+$/g,"");
}
	 var ventanActiva;
	var ventana;
	var respuesta;
	var opcion;	
	function ejecuta() {
		var	f=document.fcancelaconfirma;
		f.opcion.value="ejecuta";
		f.action="${ctx}NomPreCancelaTarjeta";
		f.submit();
	}
	function continua() {
	var	f=document.fcancelaconfirma;
		if (respuesta != 1) {
			return;
		}
		f.action="${ctx}NomPreReposicionTarjeta?opcion=inicia&<%=BitaConstants.FLUJO%>=<%=BitaConstants.ES_NP_TARJETA_REASIGNACION%>";
		f.submit();		
	}
	function regresar() {
		var	f=document.fcancelaconfirma;
		f.opcion.value="inicia";								
		<% 
		if (tipoOper==null){ %>
			f.action="${ctx}NomPreCancelaTarjeta";
		<%}else{%>
			f.action="${ctx}<%=url%>";
		 <%}%>
		
		f.submit();
	}
 </script>
 <form name="fcancelaconfirma" METHOD="POST" ACTION="">
<table width="760" border="0" cellspacing="0" cellpadding="0">	
			
		<tr>
		<td align="center">						
		<table width="500" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">					
			<tr>						
				<td align="center" width="350" class="tittabdat">No. de empleado
				</td>
				<td align="center" width="600" class="tittabdat">Nombre del empleado
				</td>
				<td align="center" width="350" class="tittabdat">No. de tarjeta
				</td>
				<td align="center" width="350" class="tittabdat">Estatus
				</td>
			</tr>
			<tr  bgcolor=#CCCCCC>
				<td class=textabdatobs>
				<%=noEmpleado.length() == 0 ? "&nbsp;" : noEmpleado%>					
				</td>
				<td class=textabdatobs>
				<%=nombreEmpleado.length() == 0 ? "&nbsp;" : nombreEmpleado%>					
				</td>
				<td class=textabdatobs>
				<%=noTarjeta.length() == 0 ? "&nbsp;" : noTarjeta%>					
				</td>
				<td class=textabdatobs>
				<%=edoTarjeta.length() == 0 ? "&nbsp;" : edoTarjeta%>					
				</td>
			</tr>					
		</table>                
		<br>
		<table width="90" border="0" cellspacing="0" cellpadding="0" class="tabfonbla">					
			<tr>
				<%if(operConfirma != null && !operConfirma.trim().equals("ejecutaOK")){%>
				<td align="center">							
					<a href="javascript:ejecuta();" border=0 > <img  src="/gifs/EnlaceMig/gbo25280.gif" border=0></a>
				</td>	
			    <%} %>											
				<td align="center">										
					<a href="javascript:regresar();" border=0 > <img  src="/gifs/EnlaceMig/gbo25320.gif" border=0></a>
				</td>								
			</tr>
		</table>
	    </td>
	</tr>

</table>									

<INPUT TYPE="hidden" NAME="opcion">
<INPUT TYPE="hidden" NAME="numEmp" VALUE="<%=noEmpleado%>">
<INPUT TYPE="hidden" NAME="nomEmp" VALUE="<%=nombreEmpleado%>">
<INPUT TYPE="hidden" NAME="tarjeta" VALUE="<%=noTarjeta%>">
<INPUT TYPE="hidden" NAME="estatus" VALUE="<%=edoTarjeta%>">
</form>
<%@ include file="/jsp/prepago/footerPrepago.jsp" %>

