<%@ page import="mx.altec.enlace.beans.*"%>
<jsp:include page="headerPrepago.jsp"/>
<script type="text/javascript">
<!--		
	function consultar() {
		var forma = document.recepcionRemesa;	
		var valor = forma.noRemesa.value;
		if (valor == "" || !valor.match(/^[0-9]+$/)) {
			cuadroDialogo("Debe proporcionar un n&uacute;mero de remesa v&aacute;lido", 1);							
		} else if(valor.length != <%=NomPreRemesa.LNG_NO_REMESA%>) {
			cuadroDialogo("Longitud incorrecta de n&uacute;mero de remesa", 1);							
		} else {
			forma.submit();
		}
	}
	function limpiar() {
		document.recepcionRemesa.reset();
	}
	function validaNumero(e) {
		var key = window.event ? e.keyCode : e.which;
		if (key == 8) return true;
		return /\d/.test(String.fromCharCode(key));
	}
//-->
</script>
<%session.removeAttribute("aceptados");%>
<form action="${ctx}NomPreRemesas" method="get" name="recepcionRemesa">
	<input type="hidden" name="operacion" value="consultaRecep">	
	<input type="hidden" name="inicial" value="true">
	<table cellspacing="0" cellpadding="0" border="0" width="760">
   		<tr>
	 		<td align="center">
	    		<table cellspacing="2" cellpadding="3" border="0">
					<tr>
		   				<td width="425" colspan="2" class="tittabdat"> Seleccione los datos:</td>
		 			</tr>
		 			<tr align="center">						
				 		<td align="right" class="textabdatcla">No. de Remesa:</td>
				 		<td class="textabdatcla" align="left">
				 			<input type="text" value="" size="22" maxlength="<%=NomPreRemesa.LNG_NO_REMESA%>" class="tabmovtexbol" name="noRemesa" onkeypress="return validaNumero(event);" autocomplete="off"/>				  				 							 						 											
		   				</td>
          			</tr>
 				</table>
        		<br/>
        		<table cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" width="166" style="height: 22px;">
					<tr>
          				<td height="22" align="right" width="90" valign="top"><a href="javascript:consultar();"><img height="22" border="0" width="90" alt="Consultar" src="/gifs/EnlaceMig/gbo25220.gif" name="Consultar"/></a></td>
		  				<td height="22" align="left" width="76" valign="middle"><a href="javascript:limpiar();"><img height="22" border="0" width="76" alt="Limpiar" src="/gifs/EnlaceMig/gbo25250.gif" name="Limpiar"/></a></td>		  
		 			</tr>
				</table>
	 		</td>
    	</tr>
	</table>
</form>
<jsp:include page="footerPrepago.jsp"/>
