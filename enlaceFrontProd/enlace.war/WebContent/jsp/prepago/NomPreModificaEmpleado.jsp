
<%@ include file="/jsp/prepago/headerPrepago.jsp" %>
 <%@page import="java.util.*"%>
 <%@page import="mx.altec.enlace.beans.NomPreEmpleado"%>
<script language="javaScript">
<%
    if (request.getAttribute("mensajeDem")!= null){
	     	out.println(request.getAttribute("mensajeDem"));
	   }

 %>
	String.prototype.trim = function() {
 		return this.replace(/^\s+|\s+$/g,"");
	}
	  function validaAlfanumerico(campo){
	  	if(campo.value!=''){
	  		if(!validaAlfa(campo)){
	  			campo.focus();
	  			cuadroDialogo("Los caracteres ingresados son inv&aacute;lidos", 1);
	  		}
	  	}
	  }
	  function validaCodPostal(campo){
	  	if(campo.value!=''){
	  		if(!validaCodPos(campo)){
	  			campo.focus();
	  			cuadroDialogo("La longitud del campo c&oacute;digo postal es incorrecta.", 1);
	  		}
	  	}
	  }
  function validaNumeros(campo){

  	if(campo.value!=''){
  		if(!validaEntero(campo)){
  			campo.focus();
  			cuadroDialogo("Los caracteres ingresados son inv&aacute;lidos", 1);
  		}
  	}
  }
function validaAlfa(campo){
	return campo.value.match(/^[\s-\w � �]*$/);
}
function validaEntero(campo){
	return campo.value.match(/^(?:\+|-)?\d+$/);
}
function validaCodPos(campo){
	return campo.value.match(/^[0-9]{<%=NomPreEmpleado.LNG_CODIGO_POSTAL%>}$/);
}

function validaTelefono(){
	var f=document.frmepleado;
	var lonTel=f.dirLadaEmp.value.length+f.dirTelefonoEmp.value.length;
	if(lonTel >=10 && lonTel <=13){
		return true;
	}	
	return false;
}
function validaDatos(){
	var f=document.frmepleado;


	if( f.dirCalleEmp.value.trim() == "" ){
		cuadroDialogo("El campo calle es requerido.", 1);
		return false;
	}

	if(f.dirCalleEmp.value!='' && !validaAlfa(f.dirCalleEmp)){
		cuadroDialogo("Los caracteres ingresados en el campo calle son inv&aacute;lidos.", 1);
		return false;
	}

	if( f.dirNumEmp.value.trim() == "" ){
		cuadroDialogo("El campo n&uacute;mero es requerido.", 1);
		return false;
	}

	if(f.dirNumEmp.value!='' && !validaAlfa(f.dirNumEmp)){
		cuadroDialogo("Los caracteres ingresados en el campo n&uacute;mero son inv&aacute;lidos.", 1);
		return false;
	}
	
	if(f.dirNumIntEmp.value!='' && !validaAlfa(f.dirNumIntEmp)){
		cuadroDialogo("Los caracteres ingresados en el campo n&uacute;mero interior son inv&aacute;lidos.", 1);	
		return false;
	}
	
	if( f.dirColoniaEmp.selectedIndex == -1){
		cuadroDialogo("El campo colonia es requerido.", 1);	
		return false;
	}
	
	if( f.dirCodPosEmp.value.trim() == "" ){
		cuadroDialogo("El campo c&oacute;digo postal es requerido.", 1);
		return false;
	}
	
	if(f.dirCodPosEmp.value !='' && !validaEntero(f.dirCodPosEmp)){
		cuadroDialogo("Los caracteres ingresados en el campo c&oacute;digo postal son inv&aacute;lidos.", 1);
		return false;
	}

	if(f.dirCodPosEmp.value !='' && (f.dirCodPosEmp.value.length!=<%=NomPreEmpleado.LNG_CODIGO_POSTAL%>)){
		cuadroDialogo("La longitud del campo c&oacute;digo postal es incorrecta.", 1);
		return false;
	}

	if(f.dirCodPosEmp.value !='' && !validaCodPos(f.dirCodPosEmp)){
		cuadroDialogo("Los caracteres ingresados en el campo c&oacute;digo postal son inv&aacute;lidos.", 1);
		return false;
	}
	
	if(f.dirLadaEmp.value!='' && !validaEntero(f.dirLadaEmp)){
		cuadroDialogo("Los caracteres ingresados en el campo lada son inv&aacute;lidos.", 1);
		return false;
	}

	if(f.dirTelefonoEmp.value!='' && !validaEntero(f.dirTelefonoEmp)){
		cuadroDialogo("Los caracteres ingresados en el campo tel&eacute;fono son inv&aacute;lidos.", 1);
		return false;
	}
	
	if((f.dirLadaEmp.value!='' || f.dirTelefonoEmp.value!='') && !validaTelefono()){
		cuadroDialogo("La longitud del tel&eacute;fono es incorrecta.", 1);
		return false;
	}

	return true;
}

function soloNumeros(evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
        ((evt.which) ? evt.which : 0));
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
 }
	 var ventanActiva;
	var ventana;
	var respuesta;
	var opcion;
function convierteMayus(campo)
{
	tmp = campo.value;
	tmp=tmp.toUpperCase();
	campo.value=tmp;
	return true;
}
function ejecuta() {
	if(validaDatos()){
		var	f=document.frmepleado;
		f.opcion.value="ejecuta";
		f.submit();
	}
}

function cargaCodPostal()
	{	
		var	f=document.frmepleado;
		f.opcion.value="cargaCP";
		f.submit();
	}
 </script>
 <form name="frmepleado" METHOD="POST" ACTION="${ctx }NomPreModificaEmpleado">
	<input type="hidden" name="dirCiudadEmp" value=""/>

<%
String confirma=(String)request.getAttribute("operconfirma");
if(confirma!=null){ %>
<table width="760" border="0" cellspacing="0" cellpadding="0">
	 <tr>
		<td align="center">
		<table width="500" border="0" cellspacing="2" cellpadding="2" bgcolor="#EBEBEB">
			<tr>
				<td align="center" width="600" class="tittabdat" colspan="4">Datos demogr&aacute;ficos
				</td>
			</tr>
			<tr>
				<td width="54%" class="textabdatcla">* Calle</td>
				<td class=textabdatcla width="34%">* No. Ext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No. Int</td>														
				<td colspan="1" class="textabdatcla">* Colonia</td>	
			</tr>
			<tr>
				<td width="70%" class="textabdatcla"><input type="text" value="<%=request.getAttribute("dirCalleEmp") %>" name="dirCalleEmp" maxlength="<%=NomPreEmpleado.LNG_CALLE%>" size="35" onchange="convierteMayus(this);" onblur="validaAlfanumerico(this);" ></td>
				<td width="20%" class="textabdatcla"><input type="text" value="<%=request.getAttribute("dirNumEmp") %>" name="dirNumEmp" maxlength="<%=NomPreEmpleado.LNG_NUM_EXT %>" size="4" onblur="validaAlfanumerico(this);" onchange="convierteMayus(this);">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="<%=request.getAttribute("dirNumIntEmp") %>" name="dirNumIntEmp" maxlength="<%=NomPreEmpleado.LNG_NUM_INT %>" size="4" onblur="validaAlfanumerico(this);" onchange="convierteMayus(this);"></td>
				<td class="tabmovtexbol" nowrap>
							<select name="dirColoniaEmp">
								<%=request.getAttribute("dirColoniaEmp")%>
							</select>
				</td>								
			</tr>
			<tr>
				<td class="textabdatcla">
					* C&oacute;digo postal
				</td>
				<td class="textabdatcla">
					* Delegaci&oacute;n o municipio
				</td>
				<td class="textabdatcla">
					* Ciudad
				</td>											
			</tr>
			<tr>
				<td colspan="1" class="textabdatcla">
					<input type="text" name="dirCodPosEmp" value="<%= request.getAttribute("dirCodPosEmp") %>" maxlength="<%= NomPreEmpleado.LNG_CODIGO_POSTAL %>" size="8" onkeypress="return soloNumeros(event)" onblur="javascript:cargaCodPostal()">
				</td>	
				<td class="textabdatcla">
					<input type="text" name="dirDelegacionEmp" value="<%= request.getAttribute("dirDelegacionEmp") %>" maxlength="<%=NomPreEmpleado.LNG_DELEGACION %>" size="35"  readonly="readonly"> 
				</td>
				<td class="textabdatcla">
					<input type="text" name="dirCiudadEmp" value="<%=request.getAttribute("dirCiudadEmp")%>" maxlength="<%=NomPreEmpleado.LNG_CIUDAD%>" size="35" readonly="readonly">										
				</td>		
			</tr>
			<tr>	
				<td class="textabdatcla">
					* Estado
				</td>												
				<td class="textabdatcla">
					Lada / Tel&eacute;fono
				</td>
			</tr>
			<tr>
				<td class=textabdatcla>
					<input type="text" name="dirEstadoEmp" value="<%=request.getAttribute("dirEstadoEmp")%>" maxlength="50" size="35" readonly="readonly">										
				</td>
				<td class="textabdatcla" colspan="3">
					<input type="text" name="dirLadaEmp" value="<%= request.getAttribute("dirLadaEmp")%>" maxlength="3" size="5" onkeypress="return soloNumeros(event)"> 
					<input type="text" name="dirTelefonoEmp" value="<%= request.getAttribute("dirTelefonoEmp")%>" maxlength="8" size="10" onkeypress="return soloNumeros(event)">
				</td>
			</tr>
			<tr>
				<td colspan="3"></td>
			</tr>
			<tr>
				<td class="textabdatcla" colspan="3" align="left"><br><b>* Campos requeridos</b></td>
			</tr>
		</table>
		<br>
		<table width="90" border="0" cellspacing="0" cellpadding="0" class="tabfonbla">
			<tr>
				<td align="center">
					<a href="javascript:ejecuta();" border=0 > <img  src="/gifs/EnlaceMig/gbo25280.gif" border=0></a>					
				</td>
				<td align="center">
					<a href="${ctx}NomPreBusqueda?opcion=consulta" border=0 > <img  src="/gifs/EnlaceMig/gbo25320.gif" border=0></a>						
				</td>					
			</tr>			
		</table>
	    </td>
	</tr>
</table>
<INPUT TYPE="hidden" NAME="opcion">
<INPUT TYPE="hidden" NAME="numempleado" VALUE="<%=request.getAttribute("numempleado") %>">
<INPUT TYPE="hidden" NAME="numtarjeta" VALUE="<%=request.getAttribute("numtarjeta") %>">
<INPUT TYPE="hidden" NAME="numId" VALUE="<%=request.getAttribute("numId") %>">
<INPUT TYPE="hidden" NAME="pais" VALUE="<%=request.getAttribute("pais") %>">
<%}else{ %>
<table width="760" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
			<table cellspacing="2" cellpadding="3" border="0" width="369" class="tabfonbla">
				<tr>
					<td align="left" class="tittabdat">
						Datos demogr&aacute;ficos
					</td>
				</tr>
				<tr>
					<td align="center" class="textabdatobs">
						<br/>No se encontraron datos relacionados al empleado seleccionado.<br/><br/>
					</td>
				</tr>
			</table>
			<br/>
			<a href="${ctx}NomPreBusqueda?opcion=consulta"><img height="22" border="0" width="83" alt="Regresar" src="/gifs/EnlaceMig/gbo25320.gif" name="imageField42"/></a>
    	</td>
	</tr>
</table>
<%} %>
</form>
<%@ include file="/jsp/prepago/footerPrepago.jsp" %>
