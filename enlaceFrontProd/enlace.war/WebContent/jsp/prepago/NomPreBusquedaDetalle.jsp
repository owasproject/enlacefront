<%@ page import="mx.altec.enlace.utilerias.NomPreUtil" %>
<%@ page import="mx.altec.enlace.beans.*,java.util.*,mx.altec.enlace.beans.NomPreLM1D.Relacion"%>
<%@ include file="/jsp/prepago/headerPrepago.jsp" %>
<%@page import="mx.altec.enlace.bita.BitaConstants"%>

 <script language="javaScript">
<%
String url="NomPreBusqueda?" +	BitaConstants.FLUJO +"=" + BitaConstants.ES_NP_TARJETA_CONSULTA;
%>
String.prototype.trim = function() {
 return this.replace(/^\s+|\s+$/g,"");
}

function validar()
{
	var cta;
	var f=document.frmBusquedaDet;
	var contador=0;
	for (i=0;i<f.elements.length;i++){
		if (f.elements[i].type=="radio"){
			if (f.elements[i].checked==true){
				contador=1;							
			}
		}
	}
	if (contador==1){
		return true;
	}else{
		cuadroDialogo("Usted no ha seleccionado un empleado.",1);
		return false;
	}
	return true;
}
function exportar() {
	var f=document.frmBusquedaDet;
	f.opcion.value="consulta";	
	f.salida.value = "excel";
	f.action="${ctx}NomPreBusqueda";
	f.submit();	
}
function paginar(direccion) { 
	var forma = document.paginacion;
	forma.accion.value = direccion;
	forma.submit();
}
function modifica(){
		
	document.frmBusquedaDet.opcion.value="inicia";
	document.frmBusquedaDet.action ="${ctx}NomPreModificaEmpleado";
	document.frmBusquedaDet.submit();
}

function ejecuta() {

}
 </script>
<form action="${ctx}NomPreBusqueda" method="post" name="frmBusquedaDet">
	<input type="hidden" name="opcion" value="consulta">
	<input type="hidden" name="salida" value="">
<center>
	<% 
	NomPreLM1D lm1d = (NomPreLM1D) request.getAttribute("lm1d");
	if (lm1d != null && lm1d.getDetalle() != null && lm1d.getDetalle().size() > 0) {
	%>
		<table width="760" border="0" cellspacing="0" cellpadding="0">	 		
			<tr>
				<td align="center">						
				<table width="500" border="0" cellspacing="1" cellpadding="2" class="tabfonbla">					
					<tr>
						<td align="center" width="65" class="tittabdat">Sel.						
						</td>
						<td align="center" width="350" class="tittabdat">No. de empleado
						</td>
						<td align="center" width="600" class="tittabdat">Nombre del empleado
						</td>
						<td align="center" width="350" class="tittabdat">No. de tarjeta
						</td>
						<td align="center" width="350" class="tittabdat">Estatus
						</td>
					</tr>								 
					<% 
					int tamano = lm1d.getDetalle().size();
					int inicio = NomPreUtil.obtenIndiceInicial(request); 
					int fin = NomPreUtil.obtenIndiceFinal(request, tamano);																																	
					
					Iterator iterator = lm1d.getDetalle().subList(inicio, fin).iterator();
					
					boolean claro = false;
					int i=0;
					while (iterator.hasNext()) {
						
						Relacion relacionTE = (Relacion) iterator.next();
						String sClass = claro ? "textabdatcla" : "textabdatobs";
					%>
					<tr>				
						<td align="center" width="60" class="<%=sClass%>"> 
					<% if(i==0){	%>
						<input type="radio" name="selempleado" value="<%= relacionTE.getEmpleado().getNoEmpleado()+"|"+relacionTE.getTarjeta().getNoTarjeta()%>" checked> 
					 
					<%}else{%>
					
						<input type="radio" name="selempleado" value="<%= relacionTE.getEmpleado().getNoEmpleado()+"|"+relacionTE.getTarjeta().getNoTarjeta()%>"> 
					<% }%>
						</td>
						<td class="<%=sClass%>"> <%= relacionTE.getEmpleado().getNoEmpleado() %></td>
						<td class="<%=sClass%>"><%= relacionTE.getEmpleado().getNombre()+" "+relacionTE.getEmpleado().getPaterno()+" "+relacionTE.getEmpleado().getMaterno()%></td>
						<td class="<%=sClass%>"><%= relacionTE.getTarjeta().getNoTarjeta() %></td>
						<td class="<%=sClass%>"><%= relacionTE.getTarjeta().getDescEstadoTarjeta()%></td>
					</tr>	
					<%
						claro = !claro;
						i++;
					}
					%>	
					
				</table> 
				     </form>         
				</td>
				</tr>
			<tr>
			<td>
				<table height="40" cellspacing="0" cellpadding="0" border="0" width="760">
					<tr><td></td></tr>
					<tr>
						<td align="center" class="texfootpagneg">								
							<form action="${ctx}NomPreBusqueda" name="paginacion">
								<input type="hidden" name="opcion" value="consulta">
																		
								<input type="hidden" name="inicio" value="<%=inicio%>"/>
								<input type="hidden" name="fin" value="<%=fin%>"/>
								<input type="hidden" name="accion" value=""/>
							</form>
							Registros : <%=(inicio + 1) + " - " + fin + " de " + tamano%><br/><br/>
							<% 
							if (inicio != 0) {%>
							<a href="javascript:paginar('A');">< Anteriores 50</a>
							<%}
							if (fin != 0 && fin < lm1d.getDetalle().size()) {%>
							<a href="javascript:paginar('S');">Siguientes 50 ></a>
							
							<%}%>	
							<br/><br/>
							<table height="40" cellspacing="0" cellpadding="0" border="0" width="168">					
								<tr>						
									<td height="22" align="right" width="85" valign="top">
										<a href="javascript:exportar();"><img border="0" alt="Exportar" src="/gifs/EnlaceMig/gbo25230.gif" name="imageField4"/></a>
									</td>
									<%if(request.getAttribute("modificar") != null && request.getAttribute("modificar").equals("activo")){ %>	
									<td height="22" align="right" width="93" valign="top">
										<a href="javascript:modifica();"><img src="/gifs/EnlaceMig/gbo25510.gif" border=0></a>	
									</td>
									<%}%>
									<td height="22" align="right" width="83" valign="top">
										<a href ="${ctx}<%=url%>"><img border="0" src="/gifs/EnlaceMig/gbo25320.gif" alt="Regresar"></a>	
									</td>
								</tr>
							</table>																																						  																																					
						</td>
					</tr>

				</table>
			</td>
		</tr>
</table>									
		<%
			}else{
		%>
<table width="760" border="0" cellspacing="0" cellpadding="0">			
	<tr>
		<td align="center">		
			<table cellspacing="2" cellpadding="3" border="0" width="369" class="tabfonbla">
				<tr>
					<td align="left" class="tittabdat">
						Consulta relaci&oacute;n tarjeta empleado
					</td>
				</tr> 
				<tr>
					<td align="center" class="textabdatobs">
						<br/>No se encontraron registros en la b&uacute;squeda<br/><br/>		
					</td>
				</tr>				
			</table>
			<br/>
			<a href="${ctx}<%=url%>"><img height="22" border="0" width="83" alt="Regresar" src="/gifs/EnlaceMig/gbo25320.gif" name="imageField42"/></a>
    	</td>
	</tr>
</table>
<%	} %>
									
<center>


<%@ include file="/jsp/prepago/footerPrepago.jsp" %>

