<%@page import="mx.altec.enlace.beans.*" %>
<%@ include file="/jsp/prepago/headerPrepago.jsp" %>
<script language="javaScript">
<%
       if (request.getAttribute("mensajereasigna")!= null){
	     	out.println(request.getAttribute("mensajereasigna"));
	    	request.removeAttribute("mensajereasigna");
	   }
%>
String.prototype.trim = function() {
 return this.replace(/^\s+|\s+$/g,"");
}
function valida(){
	var f=document.freasignatarjeta;	
	if(f.txtTarAnt.value.length == 0|| f.txtTarNueva.value.length == 0){
		cuadroDialogo("Los campos tarjeta anterior y tarjeta nueva son requeridos.", 1);
        return false;
	}				
	if (f.txtTarAnt.value != "" && !f.txtTarAnt.value.match(/^[0-9]+$/)) {			
		cuadroDialogo("El n&uacute;mero de tarjeta anterior no es v&aacute;lido.", 1);			
		return false;			
	} else if(f.txtTarAnt.value != "" && f.txtTarAnt.value.length < <%=NomPreTarjeta.LNG_NO_TARJETA_MIN%>) {
		cuadroDialogo("Longitud incorrecta de n&uacute;mero de tarjeta anterior", 1);
		return false;				
	}					
	if (f.txtTarNueva.value != "" && !f.txtTarNueva.value.match(/^[0-9]+$/)) {			
		cuadroDialogo("El n&uacute;mero de tarjeta nueva no es v&aacute;lido.", 1);			
		return false;			
	} else if(f.txtTarNueva.value != "" && f.txtTarNueva.value.length < <%=NomPreTarjeta.LNG_NO_TARJETA_MIN%>) {
		cuadroDialogo("Longitud incorrecta de n&uacute;mero de tarjeta nueva", 1);
		return false;				
	}				
	return true;
}
function soloNumeros(evt) { 
    evt = (evt) ? evt : event; 
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : 
        ((evt.which) ? evt.which : 0)); 
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {       
        return false; 
    } 
    return true; 
 }
function consulta() {
	var f=document.freasignatarjeta;
	if(valida()){
		f.opcion.value="consulta";
		f.tarjetaAnt.value=f.txtTarAnt.value;	
		f.tarjetaNueva.value=f.txtTarNueva.value;	
		f.action="NomPreReposicionTarjeta";
		f.submit();	
	}
}
</Script>	
<form name="freasignatarjeta" METHOD="POST" ACTION="">							
<table width="760" border="0" cellspacing="0" cellpadding="0">		
		
		<tr>
		<td align="center">		
		<table cellspacing="2" cellpadding="3" border="0" width="500" class="textabdatcla">
			<tr>
				<td colspan="4" class="tittabdat">Reposici&oacute;n de tarjeta<br/></td>
			</tr>
			<tr>
				<td class="tabmovtex">Tarjeta anterior</td>
				<td class="CeldaContenido">
					<input type="text" onchange="" class="tabmovtex" value="" maxlength="<%=NomPreTarjeta.LNG_NO_TARJETA%>" size="35" name="txtTarAnt" onkeypress="return soloNumeros(event)"/>
				</td>
				<td class="tabmovtex">Tarjeta nueva</td>
				<td class="CeldaContenido">
					<input type="text" onchange="" class="tabmovtex" value="" maxlength="<%=NomPreTarjeta.LNG_NO_TARJETA%>" size="35" name="txtTarNueva" onkeypress="return soloNumeros(event)"/>
				</td>
			</tr>					
		</table>		
		<br>				
		<table height="22" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" width="166">
   			<tr>		    			
     				<td align="center">
     					<a href="javascript:consulta();" border=0 > <img  src="/gifs/EnlaceMig/gbo25280.gif" border=0></a>
      				</td>         				  				
      			</tr>
     	</table>				
	    </td>
	</tr>

</table>
<INPUT TYPE="hidden" NAME="opcion">
<INPUT TYPE="hidden" NAME="tarjetaAnt">
<INPUT TYPE="hidden" NAME="tarjetaNueva">
</form>
<%@ include file="/jsp/prepago/footerPrepago.jsp" %>

