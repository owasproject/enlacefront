
	<%@ page import="java.util.StringTokenizer" %>
	<%@ page import="java.util.Vector" %>
	<%@ page import="EnlaceMig.*" %>

	<%
	// --- Obtenci�n de par�metros -----------------------------------------------------
	String parMenuPrincipal = (String)request.getAttribute("MenuPrincipal");
	String parFuncionesMenu = (String)request.getAttribute("newMenu");
	String parEncabezado = (String)request.getAttribute("Encabezado");
	String parCuentas = (String)request.getAttribute("Cuentas");
	String parCuentasDesc = (String)request.getAttribute("CuentasDesc");
	String parMensaje = (String)request.getAttribute("Mensaje");
	String parTrama = (String)request.getAttribute("Trama");
	String parNuevo = (String)request.getAttribute("Nuevo");

	if(parMenuPrincipal == null) parMenuPrincipal = "";
	if(parFuncionesMenu == null) parFuncionesMenu = "";
	if(parEncabezado == null) parEncabezado = "";
	if(parCuentas == null) parCuentas = "";
	if(parCuentasDesc == null) parCuentasDesc = "";
	if(parMensaje == null) parMensaje = "";
	if(parTrama == null) parTrama = "";
	if(parNuevo == null) parNuevo = "";

	%>

<!--== COMIENZA CODIGO HTML ==========================================================-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>

<HEAD>
	<TITLE>Bienvenido a Enlace Internet</TITLE>
	<META NAME="Generator" CONTENT="EditPlus">
	<META NAME="Author" CONTENT="Rafael Villar Villar">
	<LINK rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

	<!-- Scripts =====================================================================-->
	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
	<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
	<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
	<SCRIPT>

	// --- BOTONES ---------------------------------------------------------------------
	function copiar()
		{
		document.Forma.Accion.value = "COPIA_" + ((document.Forma.Sel[0].checked)?("CON"):("FON"));
		document.Forma.submit();
		}

	function cancelar()
		{
		document.Forma.Accion.value = "REGRESA";
		document.Forma.submit();
		}

	// --- Inicio y env�o --------------------------------------------------------------
	function inicia()
		{
		document.Forma.Sel[0].checked = true;
		}

	function validaEnvio()
		{
		if(document.Forma.Accion.value == "") return false;
		return true;
		}

	// --- Funciones de men� -----------------------------------------------------------
	function MM_preloadImages()
		{ //v3.0
		var d=document;
		if(d.images)
			{
			if(!d.MM_p) d.MM_p=new Array();
			var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
			for(i=0; i<a.length; i++)
			if (a[i].indexOf("#")!=0)
				{d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}
			}
		}

	function MM_swapImgRestore()
		{ //v3.0
		var i,x,a=document.MM_sr;
		for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
		}

	function MM_findObj(n, d)
		{ //v3.0
		var p,i,x;
		if(!d) d=document;
		if((p=n.indexOf("?"))>0&&parent.frames.length)
			{
			d=parent.frames[n.substring(p+1)].document;
			n=n.substring(0,p);
			}
		if(!(x=d[n])&&d.all) x=d.all[n];
		for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
		return x;
		}

	function MM_swapImage()
		{ //v3.0
		var i,j=0,x,a=MM_swapImage.arguments;
		document.MM_sr=new Array;
		for(i=0;i<(a.length-2);i+=3)
			if((x=MM_findObj(a[i]))!=null)
				{
				document.MM_sr[j++]=x;
				if(!x.oSrc) x.oSrc=x.src;
				x.src=a[i+2];
				}
		}

	<%= parFuncionesMenu %>

	</SCRIPT>

</HEAD>

<!-- CUERPO ==========================================================================-->

<BODY onload="inicia()" background="/gifs/EnlaceMig/gfo25010.gif" bgcolor=#ffffff>




	<!-- MENU PRINCIPAL Y ENCABEZADO ---------------------------->
	<table border="0" cellpadding="0" cellspacing="0" width="571">
	<tr valign="top"><td width="*">
	<%= parMenuPrincipal %>
	</td></tr></table>
	<%= parEncabezado %>




	<FORM name=Forma  onSubmit="return validaEnvio()" action="TIDispersion">
	<INPUT type=Hidden name="Accion" value="">
	<INPUT type=Hidden name="Trama" value="">
	<INPUT type=Hidden name="Cuentas" value="<%= parCuentas %>">
	<INPUT type=Hidden name="Nuevo" value="<%= parNuevo %>">

	<DIV align=center>

	<TABLE width=250px class="textabdatcla" border=0 cellspacing=0>
	<TR>
		<TD class="tittabdat">
		&nbsp;Seleccione la estructura que desea copiar
		</TD>
	</TR>
	<TR>
		<TD class='tabmovtex' align=center>

		<TABLE width=150><TR><TD class='tabmovtex'>
		<BR>
		<INPUT type=Radio name="Sel" value="Concentracion">&nbsp;Concentraci&oacute;n
		<BR><BR>
		<INPUT type=Radio name="Sel" value="Fondeo">&nbsp;Fondeo autom&aacute;tico
		<BR><BR>
		</TD></TR></TABLE>

		</TD>
	</TR>
	</TABLE>

	<DIV>
	<BR>
	<A href="javascript:copiar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25545.gif" alt="Copiar"></A><A href="javascript:cancelar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25320.gif" alt="Regresar"></A>
	<BR>
	</DIV>

	</DIV>

	</FORM>

</BODY>

</HTML>
