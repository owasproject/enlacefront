<!--
/*************************************************************************
 * Fecha de �ltima modificaci�n : 22 - Dic - 05
 * Proyecto:200525200 Recaudaci�n de Impuestos Federales V4.0
 *
 * Nombre del programador: Leonel Popoca
 * Fecha: Septiembre 2005
 * Modificaci�n: Adaptaciones correspondientes a las
 * especificaciones t�cnicas del SAT versi�n 4.0
 *
 * Nombre del programador: Leonel Popoca
 * Fecha: 22 - Dic - 05
 * Modificaci�n: Se elimin� el c�digo muerto
 ************************************************************************/
-->

<html>
<script type="text/javascript" src="/EnlaceMig/validacionesFiltroCuentas.js"></script>
<script language = "JavaScript"    SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script language = "JavaScript"    SRC= "/EnlaceMig/json3.min.js"></script>
<head>

<script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}


var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}


function formatea(para)
{
	var formateado = "";
	var car = "";

	for(a2=0;a2<para.length;a2++)
	{
		if(para.charAt(a2)==' ')
			car="+";
		else
			car=para.charAt(a2);
			formateado+=car;
	}
	return formateado;
}



function actualizacuenta()
{
  document.impuestos.cuenta.value=ctaselec+"|"+ctatipre+"|"+ctadescr+"|";
//  document.impuestos.textdestino.value=ctaselec+" "+ctadescr;
}
// FSW INDRA
// Marzo 2015
// Enlace PYMES

function obtenerCuentaSeleccionada(combo, elemento) {
	var select = document.getElementById(combo);
	document.getElementById(elemento).value="";
	if (!(select === undefined)) {
		var valor = select.options[select.selectedIndex].value;

		if(valor !== null
	   	&& "" !==valor){
			document.getElementById(elemento).value = obtenerCuenta(valor);
		}
	}
}

function obtenerCuenta(valor){
    var res = valor.split("\|");
    if (res[0]=="") {
      return "";
    }

    if(res.length>1){
	    return res[0] + "|" + res[1] + "|"  + res[2] + "|";
    }
}
// FSW INDRA
// Marzo 2015
// Enlace PYMES




function Enviar() {


	var Url = "";
	var f = document.impuestos;
		obtenerCuentaSeleccionada("comboCuenta0", "cuenta");

		// FSW IDS Inicia Migracion Applets 03.Feb.2017
		//Url = parent.frames['contenido'].Validate();
		Url = "/Enlace/enlaceMig/NuevoPagoImpuestos?ventana=1&tipopago=010";

		
		//f.param.value = parent.frames['contenido'].getPar1();
		f.param.value = window.parent.frames['contenido'].document.getElementById('idForm').cadenaOriginal.value;
		
		//f.paramImp.value = parent.frames['contenido'].getPar2();
		f.paramImp.value = window.parent.frames['contenido'].document.getElementById('idForm').datosImpresion.value;
        
        //f.tramaDesordenada.value = parent.frames['contenido'].getTramaDesordenada();
        f.tramaDesordenada.value = window.parent.frames['contenido'].document.getElementById('idForm').datos.value;

		// vswf ini
        //f.paramXML.value = parent.frames['contenido'].getPar4();
        f.paramXML.value = window.parent.frames['contenido'].document.getElementById('idForm').datosXML.value;
		// vswf fin
		// FSW IDS Fin Migracion Applets 03.Feb.2017

	if( verificar() == true ) {
      alert("Su transaccion se esta realizando.");
	    if(document.impuestos.hdDuplicado.value == "0"){
        document.impuestos.hdDuplicado.value = "1";
		f.param1.value = Url;
		f.action = Url;
		f.target="_parent";
		f.submit();
      }
	}
}

function verificar() {

	var f = document.impuestos;
		if(f.cuenta.value =="" || f.cuenta.length<4) {
			alert("Debe seleccionar una cuenta");
			//document.impuestos.textdestino.value= "";
			return false;
		}
		//f.cuenta.focus();
		
	// FSW IDS Inicia Migracion Applets 03.Feb.2017
	//if( parent.frames['contenido'].validaApplet()=="*"||parent.frames['contenido'].validaApplet()=="EOF%7C"||parent.frames['contenido'].validaApplet()=="" || parent.frames['contenido'].validaApplet() == "EOF|" ) {
	if( window.parent.frames['contenido'].document.getElementById('idForm').cadenaOriginal.value=="*"||window.parent.frames['contenido'].document.getElementById('idForm').cadenaOriginal.value=="EOF%7C"||window.parent.frames['contenido'].document.getElementById('idForm').cadenaOriginal.value=="" || window.parent.frames['contenido'].document.getElementById('idForm').cadenaOriginal.value == "EOF|" ) {
	// FSW IDS Fin Migracion Applets 03.Feb.2017
		alert("Debe de capturar al menos un concepto.");
		return false;
	}

    // FSW IDS Inicia Migracion Applets 03.Feb.2017
	//if( parent.frames['contenido'].validaApplet() == "MSG|" ) {
	if( window.parent.frames['contenido'].document.getElementById('idForm').cadenaOriginal.value == "MSG|" ) {
	// FSW IDS Fin Migracion Applets 03.Feb.2017
		alert("Debe de cerrar la ventana de Dialogo.");
		return false;
	}

	return true;
}



								   function jreAyuda()
								   {
									sURL = '/EnlaceMig/impuestos/jreAyuda.html';
				res  = window.open(sURL, "EnlaceInternet", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=350");
								   }


//-->
</script>

<title>Nuevo Esquema de Pago de Impuestos</title>
<meta name="Codigo de Pantalla" content="s00280">
<meta name="Version" content="1.0">
<meta name="Ultima Modificacion" content="17/08/2002 15:46">

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="/gifs/EnlaceMig/gfo25010.gif" onLoad="consultaCuentas(0,2,1)">

<form name="impuestos" action="" target="mainFrame" method="post">
<INPUT TYPE="hidden" name="param" value="">
<INPUT TYPE="hidden" name="paramImp" value="">
<input type="hidden" name="tramaDesordenada" value="">

<!-- vswf ini -->
<INPUT TYPE="hidden" name="paramXML" value="">
<INPUT TYPE="hidden" name="hdDuplicado" value="0">
<!-- vswf fin -->




<table border="0" cellspacing="0" cellpadding="0" >
	<TR>
		<td colspan=2 class ="texenccon"><img src="/gifs/EnlaceMig/glo25010.gif"></td>
		<td colspan=2 class ="texenccon"><dd>Si se presenta un bloqueo en los campos de captura de su declaraci&oacute;n, d&eacute; click <A HREF="javascript:jreAyuda();">aqu&iacute;.</A>
		<br><dd>Compensaci&oacute;n Universal &nbsp; <font color=red>&iquest;</font>Vas a compensar<font color=red>?</font> &nbsp; Consulta antes los requisitos en la p&aacute;gina del SAT.
		<a href="http://www.sat.gob.mx" target="_blank"> www.sat.gob.mx</a></td>
	</TR>

<!--
	<tr>
	  <td><br></td>
	</tr>
//-->
			<tr valign="top" >
    				<td colspan="4">
		          		<%@ include file="/jsp/nuevopagoimpuestos/filtroConsultaCuentas.jspf" %>
		          	</td>
    			</tr>

		     <tr>
			    <td class="tabmovtexbol" nowrap style="text-align: right;">Cuenta cargo: </td>
                <!--td class="tabmovtex" rowspan="8" nowrap valign="top" align="left"><img src="/gifs/EnlaceMig/gau25010.gif" width="10" height="50"></td-->
			    <td class="tabmovtex" nowrap>


					<%@ include file="/jsp/listaCuentasTransferencias.jspf" %>

					<input type="hidden" id="cuenta" name="cuenta" value=
					 <%
				      if(request.getAttribute("destino")!= null)
                      	  out.print(request.getAttribute("destino"));
					  else
					      out.println("");
				   %>


				   >
                </td>

                <td class="tabmovtexbol" nowrap align=right >Referencia Mancomunidad &nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td class="tabmovtex" nowrap>&nbsp;&nbsp;	&nbsp;&nbsp;
						<INPUT TYPE="text" class="texdrocon" maxlength=16 size=16 NAME="refmancomunidad">
				</td>

              </tr>

		<input type="hidden" name="param1" value="">

</table>
</form>
</body>
	<!-- Funciona desde las versiones de Explorer 5 -->
	<head><META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="-1">
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="-1">
	</head>
</html>
