<jsp:useBean id='facultadesBajaCtas' class='java.util.HashMap' scope='session'/>
<html>
<head>
	<title>Baja de cuentas</TITLE>
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">

<script language="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>

<!-- Estilos //-->
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<%

  String nombreArchivoImp="";
  String nombreArchivoDetalle="";
  String registroDeBaja="";
  String registroDeSolicitud="";
  String totalRegistros="";
  String strMensaje="";
  /**GAE**/
  String msgCuentasCanceladasFinal = "";
  Boolean flagFin = new Boolean(false);
  /**FIN GAE**/

  if(request.getAttribute("nombreArchivoImp")!=null)
    nombreArchivoImp=(String)request.getAttribute("nombreArchivoImp");
  if(request.getAttribute("nombreArchivoDetalle")!=null)
    nombreArchivoDetalle=(String)request.getAttribute("nombreArchivoDetalle");
  if(request.getAttribute("totalRegistros")!=null)
    totalRegistros=(String)request.getAttribute("totalRegistros");
  if(request.getAttribute("registroDeSolicitud")!=null)
    registroDeSolicitud=(String)request.getAttribute("registroDeSolicitud");
  if(request.getAttribute("registroDeBaja")!=null)
    registroDeBaja=(String)request.getAttribute("registroDeBaja");
  if(request.getAttribute("strMensaje")!=null)
    strMensaje=(String)request.getAttribute("strMensaje");

  /**GAE**/
  if(request.getAttribute("msgCuentasCanceladasFinal")!=null)
    msgCuentasCanceladasFinal = (String)request.getAttribute("msgCuentasCanceladasFinal");
  if(request.getAttribute("flagFin")!=null)
  	flagFin = (Boolean)request.getAttribute("flagFin");
  /**FIN GAE**/

int regDif = 0;
int regRec = 0;
  try {
	  regDif = Integer.parseInt(registroDeBaja) + Integer.parseInt(registroDeSolicitud);
	  regRec = Integer.parseInt(totalRegistros) - regDif;
  } catch (Exception e) {
  }
 %>

<style type="text/css">
..componentesHtml
  {
	  font-family: Arial, Helvetica, sans-serif;
	  font-size: 11px;
	  color: #000000;
  }
</style>


<script language="JavaScript">
/*************************************************************************************/
function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function js_regresar()
{
  document.BajaCuentas.action="MMC_Baja";
  document.BajaCuentas.Modulo.value="0";
  document.BajaCuentas.submit();
}

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function js_verDetalle()
{
   var forma=document.BajaCuentas;
   var parametros="";

   parametros+="totalRegistros="+forma.totalRegistros.value;
   parametros+="&nombreArchivoImp=<%=nombreArchivoImp%>";
   parametros+="&nombreArchivoDetalle="+forma.nombreArchivoDetalle.value;
   parametros+="&registroDeSolicitud=<%=registroDeSolicitud%>";
   parametros+="&registroDeBaja=<%=registroDeBaja%>";

   ventanaInfo=window.open('/Enlace/jsp/MMC_EnvioErrores.jsp?'+parametros,'trainerWindow','width=720,height=400,toolbar=no,scrollbars=yes');
 }

/*************************************************************************************/
<%= request.getAttribute( "newMenu" ) %>
/*************************************************************************************/


/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/

</script>
</HEAD>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
      <%= request.getAttribute("MenuPrincipal")%>
    </TD>
  </TR>
</table>
<%= request.getAttribute("Encabezado")%>
<table width="571" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
  </tr>
</table>

<FORM  NAME="BajaCuentas" method=post action="MMC_Baja">

<table width="760" border="0" cellspacing="0" cellpadding="0">

<%if(flagFin.booleanValue()){ %>
  <tr>
  <td align=center>
	<table width="640" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
	 <tr>
	  <td class="tittabdat" align="center"> &nbsp; Nombre de Archivo </td>
	  <!--<td class="tittabdat" align="center"> &nbsp; Nombre de Archivo Detalle</td>//-->
	  <td class="tittabdat" align="center"> &nbsp; Registros Enviados</td>
	  <td class="tittabdat" align="center"> &nbsp; Registros Autorizados</td>
	  <td class="tittabdat" align="center"> &nbsp; Registros Pendientes de Autorizaci&oacute;n</td>
	  <td class="tittabdat" align="center"> &nbsp; Registros Rechazados</td>
	 </tr>
	 <tr>
	  <td class="textabdatobs" nowrap align="center"><%=nombreArchivoImp.substring(nombreArchivoImp.lastIndexOf("/")+1)%></td>
	  <!--<td class="textabdatobs" nowrap align="center"><%=nombreArchivoDetalle%></td>//-->
	  <td class="textabdatobs" nowrap align="center"><%=totalRegistros%>&nbsp;</td>
	  <td class="textabdatobs" nowrap align="center"><%=registroDeBaja%>&nbsp;</td>
	  <td class="textabdatobs" nowrap align="center"><%=registroDeSolicitud%>&nbsp;</td>
	  <td class="textabdatobs" nowrap align="center"><%=regRec%>&nbsp;</td>
	 </tr>
	 <tr>
	  <td colspan=5 class="textabdatcla">
	    <%=strMensaje%>
	  </td>
	 </tr>
	</table>
  </td>
  </tr>
<%}%>
	<!-- GAE -->
<tr><td class="tabmovtex11" align="right">&nbsp;</td></tr>
	<%=msgCuentasCanceladasFinal%>
	<!-- FIN GAE -->
	<tr><td class="tabmovtex11" align="right">&nbsp;</td></tr>
	<tr><td class="tabmovtex11" align="right">&nbsp;</td></tr>
	</table>
	<table align=center border=0 cellspacing=0 cellpadding=0>
	 <tr>
	  <td><A href = "javascript:js_regresar();" border = 0><img src = "/gifs/EnlaceMig/gbo25320.gif" border=0 alt=Regresar></a></td>
	  <%
	    if(!nombreArchivoDetalle.trim().equals("-1") && flagFin.booleanValue()){
	  %>
	  <td><A href = "javascript:js_verDetalle();" border = 0><img src = "/gifs/EnlaceMig/gbo25248.gif" border=0 alt="Detalle"></a></td>
	  <%
	    }
	  %>
	 </tr>
	</table>
   </td>
  </tr>
</table>


 <input type=hidden name=Modulo value=2>
 <input type=hidden name=nombreArchivoDetalle value="<%=nombreArchivoDetalle%>">
 <input type=hidden name=totalRegistros value="<%=totalRegistros%>">

 </form>

</body>
</HTML>