<%
String parConsulta = (String)request.getAttribute("consulta");
String parAlta = (String)request.getAttribute("alta");
String parModifica = (String)request.getAttribute("modifica");
String parBorra = (String)request.getAttribute("borra");
String parTrama = (String)request.getAttribute("Trama");

if(parConsulta == null) parConsulta = "";
if(parAlta == null) parAlta = "";
if(parModifica == null) parModifica = "";
if(parBorra == null) parBorra = "";
if(parTrama == null) parTrama = "";
%>

<HTML>

<HEAD>
	<TITLE>Pruebas con Tuxedo</TITLE>
</HEAD>

<BODY>

	<H1>Pruebas con Tuxedo...</H1>

	<H2>Tramas:</H2>
	<TABLE>
	<TR><TD><B>Consulta:</B></TD><TD><%= parConsulta %></TD></TR>
	<TR><TD><B>Alta:</B></TD><TD><%= parAlta %></TD></TR>
	<TR><TD><B>Modifica:</B></TD><TD><%= parModifica %></TD></TR>
	<TR><TD><B>Borra:</B></TD><TD><%= parBorra %></TD></TR>
	</TABLE>

	<H2>Ultimo regreso</H2>
	<P><PRE><%= parTrama %></PRE></P>

	<HR>

	<H2>Env&iacute;o de tramas:</H2>
	<FORM action="TIConSaldos" method="POST">
	<INPUT type=Hidden name="Accion" value="TUXEDO">
	Trama: <INPUT type=Text name="Trama" size=50><BR>
	<INPUT type=Submit value="Enviar trama">
	</FORM>

	<HR>

</BODY>


