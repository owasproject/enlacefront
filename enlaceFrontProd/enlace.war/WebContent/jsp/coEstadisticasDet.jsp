<%@ page import="java.util.*" %>

<%!

//
String formateaAmoneda(String str)
	{
	int pos = str.indexOf(".");

	if(pos == -1) {pos = str.length(); str += ".";}
	while(pos+3<str.length()) str = str.substring(0,str.length()-1);
	while(pos+3>str.length()) str += "0";
	while(str.length()<4) str= "0" + str;
	for(int a=str.length()-6;a>0;a-=3) str = str.substring(0,a) + "," + str.substring(a);
	str = "$" + str;
	return str;
	}

//
String colocaPunto(String num)
	{
	if(num.length()<2) return num;
	return num.substring(0,num.length()-2) + "." + num.substring(num.length()-2);
	}

//
String moneda(String num)
	{
	return formateaAmoneda(colocaPunto(num.trim()));
	}

%>
	<%
	String estilo = "textabdatobs";
	String linea;
	String primerLinea;
	Vector lineas = new Vector();
	StringTokenizer info = new StringTokenizer((String)request.getAttribute("Info"),"\n");
	StringTokenizer tokens;

	// Se recupera la primer l�nea
	primerLinea = info.nextToken();

	// Se guardan las l�neas en un vector
	try
		{while(info.hasMoreTokens()) lineas.add(info.nextToken());}
	catch(Exception e)
		{System.out.println("Excepci�n en coEstadisticasCon.jsp: " + e);}

	// Se ordenan las l�neas
	int total = lineas.size(), a, b;
	String lin1, lin2;
	for(a=0;a<total-1;a++)
		for(b=a+1;b<total;b++)
			{
			lin1 = (String)lineas.get(a);
			lin2 = (String)lineas.get(b);
			if (lin1.substring(0,lin1.indexOf("|")).compareTo(lin2.substring(0,lin2.indexOf("|")))>0)
				{
				lineas.set(a,lin2);
				lineas.set(b,lin1);
				}
			}

	// Se obtienen los datos de la primera l�nea
	linea = primerLinea;
	String recibidos, porPagar, pagados, liberados, anticipados, cancelados, vencidos, rechazados;
	String num_recibidos, num_porPagar, num_pagados, num_liberados, num_anticipados, num_cancelados, num_vencidos, num_rechazados;
	String nombre, clave;

	nombre = linea.substring(20,100); clave = linea.substring(0,20);
	num_recibidos = linea.substring(100,108); recibidos = moneda(linea.substring(108,129)); linea = linea.substring(129);
	num_porPagar = linea.substring(0,8); porPagar = moneda(linea.substring(8,29)); linea = linea.substring(29);
	num_pagados = linea.substring(0,8); pagados = moneda(linea.substring(8,29)); linea = linea.substring(29);
	num_liberados = linea.substring(0,8); liberados = moneda(linea.substring(8,29)); linea = linea.substring(29);
	num_anticipados = linea.substring(0,8); anticipados = moneda(linea.substring(8,29)); linea = linea.substring(29);
	num_cancelados = linea.substring(0,8); cancelados = moneda(linea.substring(8,29)); linea = linea.substring(29);
	num_vencidos = linea.substring(0,8); vencidos = moneda(linea.substring(8,29)); linea = linea.substring(29);
	num_rechazados = linea.substring(0,8); rechazados = moneda(linea.substring(8,29)); linea = linea.substring(29);
	%>

<html>

<head>
	<title>Banca Virtual</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

	<!-- ----------------------------- Secci�n de JavaScript ---------------------------- -->
	<script language = "JavaScript" src="/EnlaceMig/fw_menu.js"></script>
	<script language = "JavaScript" src="/EnlaceMig/cuadroDialogo.js"></script>
	<script language = "JavaScript" SRC= "/EnlaceMig/scrImpresion.js"></script>

	<script language = "Javascript" >

	// --- Secci�n para el men� - (comienza) --------------------------------------------------
	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_findObj(n, d) { //v3.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	<%= request.getAttribute("newMenu") %>
	// --- Secci�n para el men� - (termina) --------------------------------------------------

	// --- esto se ejecuta cuando se carga la p�gina
	function inicia()
		{
		PutDate();
		MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif',
				'/gifs/EnlaceMig/gbo25111.gif',
				'/gifs/EnlaceMig/gbo25151.gif',
				'/gifs/EnlaceMig/gbo25031.gif',
				'/gifs/EnlaceMig/gbo25032.gif',
				'/gifs/EnlaceMig/gbo25051.gif',
				'/gifs/EnlaceMig/gbo25052.gif',
				'/gifs/EnlaceMig/gbo25091.gif',
				'/gifs/EnlaceMig/gbo25092.gif',
				'/gifs/EnlaceMig/gbo25012.gif',
				'/gifs/EnlaceMig/gbo25071.gif',
				'/gifs/EnlaceMig/gbo25072.gif',
				'/gifs/EnlaceMig/gbo25011.gif');
		}

	</script>

	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>


<!-- SECCION PRINCIPAL ----------------------------------------------------------------->

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="inicia" background="/gifs/EnlaceMig/gfo25010.gif">

	<!-- MENU PRINCIPAL Y ENCABEZADO -->
	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top"><td width="*"><%= request.getAttribute("MenuPrincipal") %></td></tr>
	</table>
	<%= request.getAttribute("Encabezado") %>

	<br><br>

	<div align='center'>

	<table cellspacing="20"><tr><td>

	<table cellpadding='2' cellspacing='1'>
	<tr>
		<td  class = 'texenccon'>
		<b>PROVEEDOR: <%= nombre %></b><br>clave: <%= clave %>
		</td>



	</tr>
	</table>

	<div align="center">
	<table>
	<tr>
		<td class='tittabdat'>Por pagar</td>
		<td class="textabdatcla"><%= num_porPagar %></td>
		<td class="textabdatcla"><%= porPagar %></td>
		<td class='tittabdat'>Liberados</td>
		<td class="textabdatcla"><%= num_liberados %></td>
		<td class="textabdatcla"><%= liberados %></td>
	</tr>
	<tr>
		<td class='tittabdat'>Pagados</td>
		<td class="textabdatcla"><%= num_pagados %></td>
		<td class="textabdatcla"><%= pagados %></td>
		<td class='tittabdat'>Anticipados</td>
		<td class="textabdatcla"><%= num_anticipados %></td>
		<td class="textabdatcla"><%= anticipados %></td>
	</tr>
	<tr>
		<td class='tittabdat'>Cancelados</td>
		<td class="textabdatcla"><%= num_cancelados %></td>
		<td class="textabdatcla"><%= cancelados %></td>
		<td class='tittabdat'>Rechazados</td>
		<td class="textabdatcla"><%= num_rechazados %></td>
		<td class="textabdatcla"><%= rechazados %></td>
	</tr>
	<tr>
		<td class='tittabdat'>Vencidos</td>
		<td class="textabdatcla"><%= num_vencidos %></td>
		<td class="textabdatcla"><%= vencidos %></td>
		<td class='tittabdat'>Recibidos</td>
		<td class="textabdatcla"><%= num_recibidos %></td>
		<td class="textabdatcla"><%= recibidos %></td>
	</tr>
	</table>
	</div>

	<table cellpadding='2' cellspacing='1'>
	<tr><td  class = 'texenccon'><%= (String)request.getAttribute("Sel") %></td><tr>
	</table>




	<table cellpadding='4' cellspacing='1'>
	<tr>
		<td class='tittabdat'>N&uacute;mero de docto.</td>
		<td class='tittabdat'>Tipo de docto.</td>
		<td class='tittabdat'>Importe de docto.</td>
		<td class='tittabdat'>Fecha de vencimiento</td>
		<td class='tittabdat'>Status</td>
		<td class='tittabdat'>Fecha de operaci&oacute;n</td>
	</tr>
	<%

	// Se imprimen las l�neas
	for(a=0;a<total;a++)
		{
		linea = (String)lineas.get(a);
		estilo = (estilo.equals("textabdatobs"))?"textabdatcla":"textabdatobs";
		tokens = new StringTokenizer(linea,"|");
		try
			{
			%>
			<tr>
				<td class='<%= estilo %>' style="text-align:center;" align="center"><%= tokens.nextToken() %></td>
				<td class='<%= estilo %>' style="text-align:center;" align="center"><%= tokens.nextToken() %></td>
				<td class='<%= estilo %>' style="text-align:right;" align="right"><%= tokens.nextToken() %></td>
				<td class='<%= estilo %>' style="text-align:center;" align="center"><%= tokens.nextToken() %></td>
				<td class='<%= estilo %>' style="text-align:center;" align="center"><%= tokens.nextToken() %></td>
				<td class='<%= estilo %>' style="text-align:center;" align="center"><%= tokens.nextToken() %></td>
			</tr>
			<%
			}
		catch(Exception e)
			{System.out.println("EXCEPCION en coEstadisticasDet.jsp: " + e);}
		}
	%>
	</table>

	<br>

	<div align="center">
	<a href="<%= (String)request.getAttribute("Exportacion") %>"><img
	src='/gifs/EnlaceMig/gbo25230.gif' border='0' alt='Exportar'></A><a
	href='javascript:scrImpresion();'><img src='/gifs/EnlaceMig/gbo25240.gif' border='0' alt='Imprimir'></A><a
	href ='javascript:history.back()'><img src='/gifs/EnlaceMig/gbo25320.gif' border='0' alt='Regresar'></a>
	</div>

	</td></tr></table>
	</div>


</body>

</html>
