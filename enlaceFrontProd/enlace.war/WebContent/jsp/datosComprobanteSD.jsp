<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Comprobante con Sello Digital</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/selloDigital.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"></link>
<script type="text/javascript"> var width = 800; var height = 400; window.resizeTo(width, height); window.moveTo(((screen.width - width) / 2), ((screen.height - height) / 2)); </script>
</head>
<body class="formSD">

	<form method="post" class="formSD" id="formSD">
		${htmlSD}
		<input type="hidden" value="${pageContext.request.contextPath}" id="hdContexto" />
		<input type="hidden" value="${camposOper}" id="camposOper" name="camposOper"/>
		<input type="hidden" value="${camposAdicionales}" id="camposAdicionales" name="camposAdicionales"/>
		<input type="hidden" value="${numCamposAdicionales}" id="numCamposAdicionales" name="numCamposAdicionales"/>
	</form>
</body>
</html>
