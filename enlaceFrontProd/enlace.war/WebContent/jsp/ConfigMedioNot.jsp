<%@page import="mx.altec.enlace.bo.BaseResource"%>
<html>
<head>
		<title>Enlace por Internet</title>

		<meta http-equiv="Content-Type" content="text/html;">
		<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

		<script language = "JavaScript" src= "/EnlaceMig/cuadroDialogo.js"></script>
		<script language = "JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
		<script language = "JavaScript" src= "/EnlaceMig/Bitacora.js"></script>
		<script language = "javascript"src="/EnlaceMig/scrImpresion.js"></script>

	<script language = "javascript" >
		<%
			// Datos Formulario
			String compania = (String)request.getAttribute("carrier");
			String tipoNot = (String)request.getAttribute("tipoNotificacion");

			//Otros datos
			String exitoMsg = (String)request.getAttribute("exitoMsg");
			String errorMsg = (String)request.getAttribute("errorMsg");

			//Datos de Sesion
			BaseResource bSession = (BaseResource) request.getSession().getAttribute("session");
			java.util.HashMap carrierCompleto = bSession.getCarrierCompleto();
			String statusCorreo  = bSession.getStatusCorreo();
			String statusCelular = bSession.getStatusCelular();
		%>

		${newMenu}

		//Funcion para remover espacios antes y despues de una cadena
		function trimString (str){
			str = this != window? this : str;
			return str.replace(/^\s+/g, '').replace(/\s+$/g, '');
		}

		//Validador de formato de correo
		function echeck(str){
			var at="@";
		  	var dot=".";
		  	var lat=str.indexOf(at);
		  	var lstr=str.length;
		  	var ldot=str.indexOf(dot);

		  	if (str.indexOf(at)==-1) return false;
		  	if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr) return false;
		  	if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr) return false;
		  	if (str.indexOf(at,(lat+1))!=-1) return false;
		  	if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot) return false;
		  	if (str.indexOf(dot,(lat+2))==-1) return false;
		  	if (str.indexOf(" ")!=-1) return false;
		  	return true;
		}

		//Valida que haya seleccionado por lo menos un medio de notificacion
		function validaMedio() {
			var f = document.FrmConfigMedioNot;
			if (f.notificarMail.checked == false &&
				f.notificarCelular.checked == false) {
			    	cuadroDialogo( "Es necesario seleccionar un medio de notificaci&oacute;n" ,3);
			     	return false;
			    }
			return true;
		}

		//Valida que el Email introducido sea valido
		function validaEmail() {
			var f = document.FrmConfigMedioNot;
			var email = trimString(f.email.value.toLowerCase());

			//Si selecciono el checkbox de Correo
			if (f.notificarMail.checked == true) {
				//Validar que se haya introducido un valor en el campo correo
				if(email == "") {
					cuadroDialogo( "Es necesario incluir el e-mail" ,3);
					return false;
				} else {
					//Validar que sea un correo valido
					if (echeck(email) == false) {
						f.email.focus();
						cuadroDialogo("El e-mail especificado no es v&aacute;lido.", 3);
						return false;
					}
				}

				//Validar que no introduzca uno de los correos no permitidos
		    	if(email == "banca_elactronica@bsantander.com.mx" ||
		    		email == "banca_electronica@bsantander.com.mx" ||
		    		email == "banca_electronica@santander.com.mx" ||
		    		email == "hugrodriguez@santander.com.mx" ||
		    		email == "xanca_elactronica@bsantander.com.mx") {
						cuadroDialogo("El e-mail "+ email + " no es v&aacute;lido.", 3);
       	    			return false;
       	    	}
		    }

			//Validacion exitosa
		  	return true;
		}

		//Valida que el NUMERO CELULAR sea valido
		function validaTelefono() {

			var forma = document.FrmConfigMedioNot;
			var celular = trimString(forma.celular.value);
			var lada = trimString(forma.lada.value);
			var compania = forma.compTel;
			var companiaSeleccionada = compania.selectedIndex;

		  	//Validar la LADA
			if (trimString(lada)!="") {
				if((/^[0-9]{2,3}$/).exec(forma.lada.value) == null) {
			        cuadroDialogo("La Lada especificada no es v&aacute;lida" ,3);
			        forma.lada.focus();
			        return false;
			    }
			} else {
		  		if (forma.notificarCelular.checked == true) {
			    	cuadroDialogo( "Es necesario incluir la lada" ,3);
			     	return false;
			    }
		  	}

			//Validar el CARRIER
			if (companiaSeleccionada <= 0) {
		  		if (forma.notificarCelular.checked==true) {
			    	cuadroDialogo( "Es necesario seleccionar la compa&ntilde;ia telef&oacute;nica" ,3);
			     	return false;
			    }
		  	}

			//Validar el Numero Celular
			if (celular != "") {
				if (lada.length == 3) {
					if((/^[0-9]{7}$/).exec(forma.celular.value)==null) {
				        cuadroDialogo("El celular especificado no es v&aacute;lido" ,3);
				        forma.celular.focus();
				        return false;
				    }
				}
				if (lada.length == 2) {
					if((/^[0-9]{8}$/).exec(forma.celular.value)==null) {
				        cuadroDialogo("El celular especificado no es v&aacute;lido" ,3);
				        forma.celular.focus();
				        return false;
				    }
				}
			} else {
		  		if (forma.notificarCelular.checked==true) {
					cuadroDialogo( "Es necesario incluir el celular" ,3);
			     	return false;
				}
		  	}

		  	//Validacion exitosa
		  	return true;
		}

		function resetDatos() {
			document.FrmConfigMedioNot.reset();
		}

		//Funcion que valida y envia los datos al Servlet
		function enviarDatos() {
			var forma = document.FrmConfigMedioNot;

			//Limpiar Hidden's
		    forma.tipoNotificacion.value = '';
		    //forma.compTel.value = '';

			var notificar = '';
			var sCorreo = forma.statusEmail.value;
			var sCelular = forma.statusCelular.value;
			var validacionesExitosas = false;

			//Validar que haya seleccionado un medio de notificacion
			if (validaMedio() == false) return;

			//Validar que el correo y el celular sean validos si selecciono el check correspondiente
			if (forma.notificarMail.checked == true && forma.notificarCelular.checked == false
				&& validaEmail()) validacionesExitosas = true;

			if (forma.notificarMail.checked == true && forma.notificarCelular.checked == true
				&& validaEmail() && validaTelefono()) validacionesExitosas = true;

			if (forma.notificarMail.checked == false && forma.notificarCelular.checked == true
				&& validaTelefono()) validacionesExitosas = true;

			//Si las validaciones fueron exitosas...
			if (validacionesExitosas) {
				for (j = 0; j < forma.length; j++) {
		  			if(forma.elements[j].type == 'radio' && forma.elements[j].name == 'notificar'){
						if(forma.elements[j].checked){
	       					notificar = forma.elements[j].value;
	       				}
		       		}
				}

				//Recuperar la configuracion de a que usuarios se les notificara
				if (notificar == 'OperUsr') forma.tipoNotificacion.value = 'A';
				if (notificar == 'Usuarios') forma.tipoNotificacion.value = 'B';
				if (notificar == 'SoloaMi') forma.tipoNotificacion.value = 'C';

				//Recuperar el Medio mediante el cual se le notificara al usuario
				if (forma.notificarMail.checked && forma.notificarCelular.checked) {
					forma.tipoNotificacion.value = forma.tipoNotificacion.value + '3';
				} else {
					if (forma.notificarMail.checked) {
						forma.tipoNotificacion.value = forma.tipoNotificacion.value + '1';
					}
					if (forma.notificarCelular.checked) {
						forma.tipoNotificacion.value = forma.tipoNotificacion.value + '2';
					}
				}

				//Se obtiene la compa�ia y se almacena en el hidden
				//var compania = document.FrmConfigMedioNot.compTel.options[document.FrmConfigMedioNot.compTel.selectedIndex].text;
				var compania = forma.compTel.value;
				forma.carrier.value = compania;

				forma.opcion.value = 1;
		  	   	forma.submit();
			}
		}

		function MM_preloadImages() { //v3.0
		  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
		}


		function MM_swapImgRestore() { //v3.0
		  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
		}


		function MM_findObj(n, d) { //v3.0
		  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
		}


		function MM_swapImage() { //v3.0
		  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
		   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
		}

	</script>

</head>
<body>

		<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">

		<table width="571" border="0" cellspacing="0" cellpadding="0">
			<tr valign=top>
    			<td width="*">
			        ${MenuPrincipal}
    			</td>
  			</tr>
		</table>

		<table width="760" border="0" cellspacing="0" cellpadding="0">
  			<tr>
    			<td width="676" valign="top" align="left">
      				<table width="666" border="0" cellspacing="6" cellpadding="0">
        				<tr>
          					<td width="528" valign="top" class="titpag">
          						${Encabezado}
          					</td>
        				</tr>
      				</table>
    			</td>
  			</tr>
		</table>

		<!--<form name = "Frmfechas">
  			<%= request.getAttribute("Bitfechas") %></td>
		</form>-->

<%if(statusCorreo.equals("X") && statusCelular.equals("X")) {%>
	<center>
		 <font face=Arial>
    		No se puede accesar a esta pantalla ya que su informaci�n est� incompleta en el sistema central.
		 </font>
	</center>
<%}else{ %>
<form name="FrmConfigMedioNot" method="post" action="ConfigMedioNot">
<input type="hidden" name="opcion" value="${opcion}">
<input type="hidden" name="tipoNotificacion" value="${tipoNotificacion}">
<input type="hidden" name="carrier" value="${carrier}">
<table width="100%">
		<tr>
			<table width="670">
				<tr>
					<td class="tittabdat" width="400">
		      			Medio de Notificaci�n
		      		</td>
				</tr>
	      	</table>
      	</tr>
		<tr>
			<table>
						<tr>
							<td>
								<table class="textabdatcla" width="665">
									<tr>
										<td class="tabmovtex">
											<input type="checkbox" name="notificarMail" <% if(tipoNot.contains("1") || tipoNot.contains("3")){ %><%= "checked" %><% } %>>
							      		</td>
										<td align="right" width="70" class="tabmovtex">
							      			E-mail
							      		</td>
							      		<td class="tabmovtex">
							      		<%if(statusCorreo.equals("X")) {%>
							      			<input type="text" value="${email}" name="email" size="40" disabled="disabled">
							      		<%}else{ %>
											<input type="text" value="${email}" name="email" size="40">
										<%} %>
							      		</td>


							      	</tr>

							 		<tr>
										<td>
											<input type="checkbox" name="notificarCelular" <% if(tipoNot.contains("2") || tipoNot.contains("3")){ %><%= "checked" %><% } %>>
							      		</td>

										<td align="right" width="50" valign"middle" class="tabmovtex">
							      			SMS-Celular
							      		</td>
							      		<td>
											<table>
												<tr>
													<td class="tabmovtex">
														Lada
										      		</td>
													<td class="tabmovtex">
										      			Celular
										      		</td>
										      		<td class="tabmovtex">
														Compa�ia Telef�nica
													</td>

												</tr>
												<tr>
													<td class="td" class="tabmovtex">
													<%if(statusCelular.equals("X")) {%>
														<input type="text" value="${lada}" name="lada" size="3" maxlength="3" disabled="disabled" > -
													<%}else{ %>
														<input type="text" value="${lada}" name="lada" size="3" maxlength="3"> -
										      		<%} %>
										      		</td>
													<td class="td" class="tabmovtex">
													<%if(statusCelular.equals("X")) {%>
														<input type="text" value="${celular}" name="celular" size="7" maxlength="8" disabled="disabled" >
													<%}else{ %>
										      			<input type="text" value="${celular}" name="celular" size="7" maxlength="8">
										      		<%} %>
										      		</td>
										      		<td class="td" class="tabmovtex" >
														<select name="compTel" id="compTel">
														<option value="0" >&nbsp;</option>
	 													   <%java.util.Iterator it = carrierCompleto.entrySet().iterator();
															while (it.hasNext()) {
																java.util.Map.Entry e = (java.util.Map.Entry)it.next();
																if(!e.getKey().equals("RES")){%>
															<option value="<%=e.getKey()%>" <%if(e.getValue().equals(compania)){ %><%= "selected" %><%}%> ><%=e.getValue() %></option>
															<%}//if
															}//while%>
														</select>
										      		</td>
												</tr>
											</table>
							      		</td>
							      	</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table width="665">
									<tr>
										<td class="tittabdat">
											<input type="radio" name="notificar" value="OperUsr" checked class="tittabdat"  <% if(tipoNot.contains("A")){ %><%= "checked" %><% } %>>
											Notificarme de las operaciones de mis Usuarios
							      		</td>

								    </tr>

									<tr>
										<td class="tittabdat">
											<input type="radio" name="notificar" value="Usuarios" class="tittabdat"  <% if(tipoNot.contains("B")){ %><%= "checked" %><% } %>>
											Notificar s�lo a mis Usuarios
							      		</td>

							      	</tr>

							      	<tr>
										<td class="tittabdat">
											<input type="radio" name="notificar" value="SoloaMi" class="tittabdat" <% if(tipoNot.contains("C")){ %><%= "checked" %><% } %>>
											Notificarme s�lo a mi
							      		</td>


							      	</tr>
						      	</table>
						    </td>
						</tr>
	      	</table>
      	</tr>
		<tr>
			<table>
				<tr>
					<td>
						<a href="javascript:enviarDatos();">
		      			<img name=Enviar value=Enviar src="/gifs/EnlaceMig/gbo25280.gif" width="90" height="22" border="0">
		      		</td>
					<td>
						<a href="javascript:resetDatos(this.form);">
		      			<img name=Limpiar value=Limpiar src="/gifs/EnlaceMig/gbo25250.gif" width="90" height="22" border="0">
		      		</td>
				</tr>
	      	</table>
      	</tr>
</table>
<input type="hidden" name="statusEmail" value="<%=statusCorreo%>">
<input type="hidden" name="statusCelular" value="<%=statusCelular%>">
</form>

		<%if(errorMsg!=null || exitoMsg!=null){ %>
			<%if(errorMsg!=null && exitoMsg==null){ %>
			    <script type="text/javascript">
					cuadroDialogo("<%=errorMsg%>", 3);
				</script>
			<%}else if(exitoMsg!=null && errorMsg==null){ %>
		        <script type="text/javascript">
					cuadroDialogo("<%=exitoMsg%>", 1);
				</script>
			<%}%>
		<%}%>
<%}//else %>
</body>
</html>