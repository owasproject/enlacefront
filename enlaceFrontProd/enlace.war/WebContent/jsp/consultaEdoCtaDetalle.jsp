<%@ page language="java" contentType="text/html;charset=ISO8859_1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.*" %>
<%@page import="mx.altec.enlace.beans.ConsEdoCtaDetalleBean"%>
<jsp:useBean id="edoCtaListDetalle" class="java.util.ArrayList" scope="request" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head>
  <title>Enlace Banco Santander Mexicano</title>

  <script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
  <script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
  <script type="text/javascript" src="/EnlaceMig/ConMov.js"></script>

  <script type="text/javascript">
    var js_diasInhabiles = "<%= request.getAttribute("diasInhabiles") %>";
    var dia;
    var mes;
    var anio;
    var fechaseleccionada;
    var opcioncaledario;
  </script>
    
  <script type="text/javascript">
    function js_regresarEdoCta()
	{
		document.FrmConEdoCtaDet.action="ConsultaEdoCtaServlet";
		document.FrmConEdoCtaDet.opcion.value="1";
		document.FrmConEdoCtaDet.submit();
	}
  </script>
    
  <script type="text/javascript">
	function anteriores() {
    	var pag = document.FrmConEdoCtaDet.numPagDet.value;
    	pag--;
		document.FrmConEdoCtaDet.action="ConsultaEdoCtaServlet";
		document.FrmConEdoCtaDet.opcion.value="2";
		document.FrmConEdoCtaDet.numPagDet.value = pag;
		document.FrmConEdoCtaDet.archivo.value = "";
		document.FrmConEdoCtaDet.submit();
	}
  </script>
    
  <script type="text/javascript">
	function siguientes() {
    	var pag = document.FrmConEdoCtaDet.numPagDet.value;
    	pag++;
		document.FrmConEdoCtaDet.action="ConsultaEdoCtaServlet";
		document.FrmConEdoCtaDet.opcion.value="2";
		document.FrmConEdoCtaDet.numPagDet.value = pag;
		document.FrmConEdoCtaDet.archivo.value = "";
		document.FrmConEdoCtaDet.submit();
	}
  </script>
    
  <script type="text/javascript">
	function Exportar(){
		document.FrmConEdoCtaDet.action="ConsultaEdoCtaServlet";
		document.FrmConEdoCtaDet.opcion.value="2";
		document.FrmConEdoCtaDet.archivo.value = "exp";
		document.FrmConEdoCtaDet.submit();
	}
  </script>
    
  <script type="text/javascript">
	function ExportarTXT(cuenta,opcion){
		document.FrmConEdoCtaDet.action="ConsultaEdoCtaServlet";
		document.FrmConEdoCtaDet.opcion.value=opcion;
		document.FrmConEdoCtaDet.numSecuencaHDN.value=cuenta;
		document.FrmConEdoCtaDet.submit();
	}
  </script>
    
  <script type="text/javascript">
    function Actualiza() {
        if (opcioncalendario==1)
        document.FrmConEdoCtaDet.fecha1.value=fechaseleccionada;
        else if (opcioncalendario==2)
        document.FrmConEdoCtaDet.fecha2.value=fechaseleccionada;
    }
  </script>
    
  <script type="text/javascript">
    var ctaselec;
    var ctadescr;
    var ctatipre;
    var ctatipro;
    var ctaserfi;
    var ctaprod;
    var ctasubprod;
    var tramadicional;
    var cfm;
  </script>
    
  <script type="text/javascript">
    function MM_preloadImages()
    {   //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }
  </script>
    
  <script type="text/javascript">
    function MM_swapImgRestore()
    {   //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }
  </script>
    
  <script type="text/javascript">
    function MM_findObj(n, d)
    { //v3.0
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
        d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
    }
  </script>
    
  <script type="text/javascript">
    function MM_swapImage()
    {   //v3.0
        var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }
  </script>
    
  <script type="text/javascript">
<%
    if( request.getAttribute("newMenu")!=null )
        out.println(request.getAttribute("newMenu"));
%>
  </script>
  
  <script type="text/javascript">
  	function muestraMensaje() {
  		cuadroDialogo("Por incongruencia de datos en la secuencia de domicilio no fue posible obtener el total de cuentas para la secuencia seleccionada", 4);
  	}
  </script>
  
  <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
 </head>
 <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">
  <table border="0" cellpadding="0" cellspacing="0" width="571">
   <tr valign="top">
    <td width="*">
<%
    if( request.getAttribute("MenuPrincipal")!=null )
        out.println(request.getAttribute("MenuPrincipal"));
%>
    </td>
   </tr>
  </table>
<%
    if( request.getAttribute("Encabezado")!=null )
        out.println(request.getAttribute("Encabezado"));
%>
  <table width="760" border="0" cellspacing="0" cellpadding="0">
   <form name = "Frmfechas">
<%
   // if( request.getAttribute("Movfechas")!=null )
   //     out.println(request.getAttribute("Movfechas"));
%>
   </form>
   
  <form name="FrmConEdoCtaDet" method="post"> 
  <input type="hidden" name="opcion" value=""/>
  <input type="hidden" name="numSecuencaHDN" value=""/>
  <input type="hidden" name="numPagDet" value="<%=request.getAttribute("numPagDet")%>"/>
  <input type="hidden" name="totalPaginas" value="<%=request.getAttribute("totalPaginas")%>"/>
  <input type="hidden" name="idEdoCta" value="<%=request.getAttribute("idEdoCta")%>"/>
  <input type="hidden" name="archivo"  value=""/>
  <input type="hidden" name="folio" value="<%=request.getAttribute("folioC")%>"/>
  <input type="hidden" name="usuarioSol" value="<%=request.getAttribute("usuarioSolC")%>"/>
  <input type="hidden" name="fechaIni" value="<%=request.getAttribute("fechaIniC")%>"/>
  <input type="hidden" name="fechaFin" value="<%=request.getAttribute("fechaFinC")%>"/>
  <input type="hidden" name="estatus" value="<%=request.getAttribute("estatusC")%>"/>
  <input type="hidden" name="descErr" value="<%=request.getAttribute("descErrC")%>"/>

   <tr>
    <td align="center">
     <table width="800" border="0" cellspacing="2" cellpadding="3">
      <tr>
       <td class="tittabdat"  align="center"><%=request.getAttribute("Columna1")%></td>
       <td class="tittabdat"  align="center">C&oacute;digo Cliente</td>
       <td class="tittabdat"  align="center"><%=request.getAttribute("Columna2")%></td>
       <td class="tittabdat"  align="center"><%=request.getAttribute("Columna3")%></td>
       <td class="tittabdat"  align="center">Suspensi&oacute;n de env&iacute;o a domicilio (Paperless)</td>
       <td class="tittabdat"  align="center">Estatus</td>
       <td class="tittabdat"  align="center">Descripci&oacute;n de Estatus</td>
      </tr>
      
      <%
      	if(edoCtaListDetalle != null && edoCtaListDetalle.size()>0){
      	
      		String estilo ="";
      		Integer pagIni = (Integer)request.getAttribute("numPagDet");
      		Integer pagFin = (Integer)request.getAttribute("totalPaginas");
      	
      		for(int i=0;i<edoCtaListDetalle.size();i++){
      			ConsEdoCtaDetalleBean result = (ConsEdoCtaDetalleBean)edoCtaListDetalle.get(i);
      			
      			String SeqCred = "";
      			String Tdc20Po= "";
      			String TipoEDC = result.getTipoEdoCta(); 
      			
      			if ("001".equalsIgnoreCase(TipoEDC) ) {
      				SeqCred = (result.getNumSec()!=null) ? result.getNumSec() : " ";
      			}else if ("003".equalsIgnoreCase(TipoEDC) ) {
      			System.out.println("--- ContratoTDC: [" + result.getContratoTDC() + "]");
      				if (result.getContratoTDC()!=null && !result.getContratoTDC().equals(" ")) {
      					SeqCred=result.getContratoTDC();
      					Tdc20Po=(SeqCred.length()==20) ? 
      						SeqCred.substring(0,4)+"-"+SeqCred.substring(4,8)+"-"+SeqCred.substring(8,20) : SeqCred;
      				}else {
      					SeqCred = "";
      				} 
      			}
      			
      			pageContext.setAttribute("SeqCred", SeqCred);
      			pageContext.setAttribute("Tdc20Po", Tdc20Po);
      			pageContext.setAttribute("resultP", result.getPpls());
      			pageContext.setAttribute("resultE", result.getEstatus());
      			pageContext.setAttribute("TipoEDC", TipoEDC);
      			
      			if(i%2==0){
      				estilo = "textabdatobs";
      %>
			      <tr  bgcolor="#CCCCCC">
      <%		}else{ 
      				estilo = "textabdatcla";
      %>
			      <tr bgcolor="#EBEBEB">
      <%		} 
      			pageContext.setAttribute("estilo", estilo);%>

	<c:choose>
		<c:when test="${TipoEDC == '003'  && SeqCred != '' && resultE == 'Aceptado'}">
			<td class="<c:out value='${estilo}' />" align="center">
				<a href="#" title="Descargue las tarjetas relacionadas al contrato" 
					onclick="javascript:ExportarTXT('<%=SeqCred%>','4');"><c:out value="${Tdc20Po}" /></a>
			</td>
		</c:when>
		<c:when test="${TipoEDC == '003'}">
			<td class="<c:out value='${estilo}' />" align="center"><c:out value="${Tdc20Po}" /></td>
		</c:when>
		<c:when test="${TipoEDC == '001'  && resultE == 'Aceptado'}">
			<td class="<c:out value='${estilo}' />" align="center">
				<a href="#" title="Descargue las cuentas relacionadas a la secuencia de Domicilio" 
					onclick="javascript:ExportarTXT('<%=result.getNumCuenta()%>','3');"><c:out value="${SeqCred}" /></a>
			</td>	
		</c:when>
		<c:when test="${TipoEDC == '001'}">
			<td class="<c:out value='${estilo}' />" align="center">&nbsp;</td>
		</c:when>
		
	
	</c:choose>

		<td class="<%=estilo %>" align="center"><%=result.getCodClte() != null ? result.getCodClte() : " "%></td>
	    <td class="<%=estilo %>" align="center"><%=result.getNumCuenta()%></td>	
      	<td class="<%=estilo %>" align="center"><%=result.getDescCta()%></td>
      	<td class="<%=estilo %>" align="center"><%=result.getPpls()%></td>
      	<td class="<%=estilo %>" align="center"><%=result.getEstatus()%></td>
      	<td class="<%=estilo %>" align="center"><%=result.getDescError()%></td>
      </tr>

      <%
      		}
       %>

     </table>
    </td>
   </tr>
   <tr>
    <td align="center">
   		<table align="center" border="0" cellspacing="0" cellpadding="0">
	 		<tr> 
	 			
   				<%if(pagIni.intValue() >1){%>
   
	  				<td><a href="javascript:anteriores();">Anteriores</a>&nbsp;</td>
	  			<%}
	  			
	  			if(pagFin.intValue()!= pagIni.intValue()){
	  			%>
	  				<td>&nbsp;<a href="javascript:siguientes();">Siguientes</a></td>
	  			
	  			<%} %>
	 		</tr>
		</table>
	</td>
   </tr>
   <tr>
   	<td>
   		&nbsp;
   	</td>
   </tr>
   <tr>
    <td align="center">
    	<table align="center" border="0" cellspacing="0" cellpadding="0">
	 		<tr>  
	 			<td><a href = "javascript:js_regresarEdoCta();"><img src = "/gifs/EnlaceMig/gbo25320.gif" border="0" alt="Regresar"/></a></td>
	 			
       			<td><a href = "javascript:Exportar()" > <img border="0" name="imageField" src="/gifs/EnlaceMig/gbo25230.gif" border="0" alt="Exportar" height="22" width="85"/></a></td>

	 		</tr>
		</table>
	</td>
   </tr>
   <%} %>
  </form>
  </table>
  <%
    if(request.getAttribute("MENSAJE_PROBLEMA_DATOS") != null) {
  %>
  		muestraMensaje();
  <%}%>
 </body>
	<head>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	</head>
</html>