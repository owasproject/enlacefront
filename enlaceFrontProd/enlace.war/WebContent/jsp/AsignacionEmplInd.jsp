<%@page import="mx.altec.enlace.beans.NomPreEmpleado"%>
<%@page import="mx.altec.enlace.beans.NomPreTarjeta"%>
<%@page import="mx.altec.enlace.beans.EmpleadoBean"%>
<html>
<head>
<%String tabCodPostal = (String) request.getAttribute("tabCodPostal"); %>
<title>Asignacion Individual</TITLE>
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">
<script language="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javaScript" SRC="/EnlaceMig/cuadroDialogo.js"></script>

<!-- Estilos //-->
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<style type="text/css">
.componentesHtml {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #000000;
}
</style>
<style>
div.fileinputs {
	position: relative;
}

div.fakefile {
	position: absolute;
	top: 0px;
	left: 0px;
	z-index: 1;
}
</style>
<script language="javascript">
/*************************************************************************************/
function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

	var mensaje="<%= request.getAttribute("mensaje") %>";

	 if(mensaje.length>0 && mensaje!="null")
	 {
	  cuadroDialogo(mensaje, 1);
	 }

	<%EmpleadoBean empleadoBean = (EmpleadoBean) request
					.getAttribute("empleadoBean");%>
		var entro = 0;
	window.onload = function(){

		entro = 0;
		var f = document.AltaIndividualForm;
		var valor = f.paisNacimiento.value;

		var valor2 =  sel.options[sel.selectedIndex].value;
			document.AltaIndividualForm.sexocaja.value = valor2;

		    if (valor == "MEXI") {
    f.coloniaExtranjero.style.display = 'none';
    f.coloniaExtranjero.value = "";
    f.entidadFederativaCombo.style.display = 'block';

   } else {

    f.coloniaExtranjero.style.display = 'block';
    f.entidadFederativaCombo.style.display = 'none';


			   }

		}


	function inicia(){
		var f = document.AltaIndividualForm;
		<%if(tabCodPostal!=null && tabCodPostal.trim().equals("1")){ %>
			f.lada.focus();
		<%}%>
	}

	function cargaEntidadFederativa() {
	  var f = document.AltaIndividualForm;
	  var valor = "<%=empleadoBean.getPaisNacimiento().trim()%>";

      if (valor == "MEXI") {
	    f.coloniaExtranjero.style.display = 'none';
	    f.entidadFederativaCombo.style.display = 'block';
	   } else {
	    f.coloniaExtranjero.style.display = 'block';
	    f.entidadFederativaCombo.style.display = 'none';
	   }
	 }

	function validaExtranjero(sel) {
  var f = document.AltaIndividualForm;
  var valor =  sel.options[sel.selectedIndex].value;
  f.paisNacimiento.value = valor;

      if (valor == "MEXI") {
    f.coloniaExtranjero.style.display = 'none';
    f.coloniaExtranjero.value = "";
    f.entidadFederativaCombo.style.display = 'block';

   } else {

    f.coloniaExtranjero.style.display = 'block';
    f.coloniaExtranjero.value = "";
    f.entidadFederativaCombo.style.display = 'none';


   }

  }

		function obtenValor(sel) {
			var valor =  sel.options[sel.selectedIndex].value;
			document.AltaIndividualForm.tipoDocto.value = valor;
		}

		function obtenValorNacionalidad(sel) {
			var valor =  sel.options[sel.selectedIndex].value;
			document.AltaIndividualForm.cveNacionalidad.value = valor;
		}

		function obtenValorPais(sel) {
			var valor =  sel.options[sel.selectedIndex].value;
			document.AltaIndividualForm.paisNacimiento.value = valor;
		}

		function obtenValorEdoCivil(sel) {
			var valor =  sel.options[sel.selectedIndex].value;
			document.AltaIndividualForm.edoCivil.value = valor;
		}

		function obtenValorSexo(sel) {
			var valor =  sel.options[sel.selectedIndex].value;
			document.AltaIndividualForm.sexocaja.value = valor;
		}


			function obtenValorEntidad(sel) {
			var valor =  sel.options[sel.selectedIndex].value;
			document.AltaIndividualForm.cveEstado.value = valor;
		}

		function cambiaEmpleado() {
		var f=document.AltaIndividualForm;
		var cadena=f.noEmpleado.value;
		var folioReturn = "";
		if(cadena.length>0){
			var longitud = cadena.length;
			if (longitud < 7)
				for (i = 1; i <= 7 - longitud; i++)
					folioReturn += "0";
			folioReturn += cadena;
			f.noEmpleado.value=folioReturn;

			}
		}

		function validaRFC() {

		var valor = document.AltaIndividualForm.rfc.value.toUpperCase();

		if (/^[a-zA-Z � �][a-zA-Z � �][a-zA-Z � �][a-zA-Z � �]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9]$/.test(valor)) {
			if(validaLetrasRFC(valor)) {
				if(valida_FechaRFC(valor, 6)) {
					return true;
				}
			}
		}

		return false;
	}

	function validaLetrasRFC(rfc) {

		var resultado="";
		var rfc1, nombre, pat, mat;

		nombre = document.AltaIndividualForm.nombreEmpleado.value.toUpperCase();

		pat = document.AltaIndividualForm.appPaterno.value.toUpperCase();

		mat = document.AltaIndividualForm.appMaterno.value.toUpperCase();

		rfc1 = rfc.toUpperCase().substring(0,4);

		if(mat == "") mat = rfc.substring(2,3);

		nombre = quitaPrep(nombre)
		pat = quitaPrep(pat)
		mat = quitaPrep(mat)

		if (pat.length==0 || nombre.length==0 || mat.length==0 || rfc1.length!=4) return false;
		resultado=pat.charAt(0);
		var vocal = buscaVocal(pat.substring(1));
		if (vocal=="")
			{if (pat.length>1) resultado+=pat.charAt(1); else resultado+=rfc1.charAt(1);}
		else
			resultado+=vocal;

		resultado+=mat.charAt(0);
		resultado+=letraNombre(nombre);
		resultado=frases(resultado);
		if (resultado==rfc1) return true; else return false;
	}


	function quitaPrep(cadena) {
		var temp = "", elem;
		var elem2 = new Array(),j=0;
		elem = cadena.split(" ");

		for (var i=0;i<elem.length;i++)
			if (elem[i]!="" && elem[i]!="DEL" && elem[i]!="DE" && elem[i]!="LA" && elem[i]!="LOS" && elem[i]!="LAS" &&
					elem[i]!="Y" && elem[i]!="MC" && elem[i]!="MAC" && elem[i]!="VON" && elem[i]!="VAN") {
				elem2[j]=elem[i];
				j++;
			}
		if (elem2.length==1) return elem2[0];
		for (var i=0;i<elem2.length;i++) temp+=" "+elem2[i];
		temp=temp.substring(1);
		return temp;
	}

	//
	function buscaVocal(cadena) {
		for (var i=0;i<cadena.length;i++)
			if (cadena.charAt(i)=="A" || cadena.charAt(i)=="E" || cadena.charAt(i)=="I" ||
				cadena.charAt(i)=="O" || cadena.charAt(i)=="U")
				return cadena.charAt(i);
		return "";
	}

	//
	function letraNombre(cadena) {
		var resultado="",temp="",elementos;
		elementos =cadena.split(" ");

		if (elementos.length==1) return cadena.charAt(0);

		temp=elementos[0];

		if (!(temp=="MARIA" || temp=="MA" || temp=="MA." || temp=="JOSE")) return "" +temp.charAt(0);

		temp=elementos[1];
		return "" +temp.charAt(0);
	}

	//
	function frases(f) {
		if (f=="BUEI" || f=="BUEY" || f=="CACA" || f=="CACO" || f=="CAGA" || f=="CAGO" || f=="CAKA" || f=="CAKO" || f=="COGE" ||
			f=="COJA" || f=="COJE" || f=="COJI" || f=="COJO" || f=="CULO" || f=="FETO" || f=="GUEY" || f=="JOTO" || f=="KACA" ||
			f=="KACO" || f=="KAGA" || f=="KAGO" || f=="KOGE" || f=="KOJO" || f=="KAKA" || f=="KULO" || f=="LOCA" || f=="LOCO" ||
			f=="LOKA" || f=="LOKO" || f=="MAME" || f=="MAMO" || f=="MEAR" || f=="MEAS" || f=="MEON" || f=="MION" || f=="MOCO" ||
			f=="MULA" || f=="PEDA" || f=="PEDO" || f=="PENE" || f=="PUTA" || f=="PUTO" || f=="QULO" || f=="RATA" || f=="RUIN")
			return f.substring(0,3)+ "X"
		else
			return f;
	}



	// Valida la Fecha del R.F.C. -------------------------------------------------------------------
	function valida_FechaRFC(valorCampo,Indice) {
		var ValorRegreso = true;
		var mes = valorCampo.substring(Indice, Indice + 2);
		var dia = valorCampo.substring(Indice + 2, Indice + 4);

		if((dia > 0 && dia <= 31) && (mes > 0 && mes <= 12)) {
			if(mes == 2 && dia > 29)	// Para el Mes de Febrero  MARL formato de A�o bisiesto
				{ValorRegreso = false;}
		}
		else {ValorRegreso = false;}

		return ValorRegreso;
	}

	String.prototype.trim = function() {
	 return this.replace(/^\s+|\s+$/g,"");
	}

	function Limpiar() {
			var	forma = document.AltaIndividualForm;
			forma.numTarjeta.value = "";
			forma.nombreEmpleado.value = "";
			forma.rfc.value = "";
			forma.homoclave.value = "";
			forma.appPaterno.value = "";
			forma.appMaterno.value = "";
			forma.noIdentificacion.value = "";
			forma.tipoDocto.value = "IF";
			forma.noEmpleado.value = "";
			forma.fechaNac.value = "";
			forma.edoCivil.value = "S";
			forma.cveNacionalidad.value = "MEXI";
			forma.paisNacimiento.value = "MEXI";
			forma.calle.value = "";
			forma.noExterior.value = "";
			forma.noInterior.value = "";
			forma.colonia.options.length = 0;
			forma.coloniaExtranjero.value = "";
			forma.codPostal.value = "";
			forma.cveEstado.value = "";
			forma.lada.value = "";
			forma.sexocaja.value = "Masculino";
			forma.telefono.value = "";
			forma.codPostalTrab.value = "";
			forma.sucursal.value = "";
			forma.cveEstadoNombre.value = "";
	}

		function altaIndividual() {
		document.getElementById("enviando").style.visibility="hidden";
		document.getElementById("enviando2").style.visibility="visible";
		if (entro == 0) {
			entro = 1;
			var	forma = document.AltaIndividualForm;
			if (validaDatos()) {
				forma.action = "AsignacionEmplServlet?opcion=altaEmpIndividual";
				forma.submit();
			}else{
				document.getElementById("enviando").style.visibility="visible";
				document.getElementById("enviando2").style.visibility="hidden";
			}
		}

		}


		function cargaCodPostal() {

			var	forma = document.AltaIndividualForm;
		    if( forma.codPostal.value.trim() == "" ){
				return false;
			}
			forma.action = "AsignacionEmplServlet?opcion=codigoPostalIndividual";
			forma.submit();
		}



	function validaDatos(){
	entro = 0;
	 var	f = document.AltaIndividualForm;

		if( f.numTarjeta.value.trim() == "" ){
			cuadroDialogo("El campo n&uacute;mero de tarjeta es requerido.", 1);
			return false;
		}

		if (f.numTarjeta.value != "" && !f.numTarjeta.value.match(/^[0-9]+$/)) {
			cuadroDialogo("El n&uacute;mero de tarjeta no es v&aacute;lido.", 1);
			return;
		} else if(f.numTarjeta.value != "" && f.numTarjeta.value.length < <%=NomPreTarjeta.LNG_NO_TARJETA_MIN%>) {
			cuadroDialogo("Longitud incorrecta de n&uacute;mero de tarjeta", 1);
			return;
		}

		if( f.nombreEmpleado.value.trim() == "" ){
			cuadroDialogo("El campo nombre del empleado es requerido.", 1);
			return false;
		}

		if(f.nombreEmpleado.value!='' && !validaNO(f.nombreEmpleado)){
			cuadroDialogo("Los caracteres ingresados en el campo nombre del empleado son inv&aacute;lidos.", 1);
			return false;
		}

		if( f.appPaterno.value.trim() == "" ){
			cuadroDialogo("El campo apellido paterno es requerido.", 1);
			return false;
		}

		if( f.appMaterno.value.trim() == "" ){
			cuadroDialogo("El campo apellido materno es requerido.", 1);
			return false;
		}

		if(f.appPaterno.value.trim() != "" && !validaNO(f.appPaterno)){
			cuadroDialogo("Los caracteres ingresados en el apellido paterno son inv&aacute;lidos.", 1);
			return false;
		}

		if(f.appMaterno.value.trim() != "" && !validaNO(f.appMaterno)){
			cuadroDialogo("Los caracteres ingresados en el apellido materno son inv&aacute;lidos.", 1);
			return false;
		}

		if(f.fechaNac.value.trim() == ""){
			cuadroDialogo("El campo fecha de nacimiento es requerido.",1);
	        return false;
		}

		if(calculaEdad(f.fechaNac.value.trim()) < 16){
			cuadroDialogo("El empleado es menor a 16 a�os.", 1);
			return false;
		}

			fecha_hoy = new Date();
			var anioActual = fecha_hoy.getFullYear();
			var anioNaci = f.fechaNac.value.substring(6,10);
			var anonaci=parseInt(anioNaci, 10)
			var anoAct=parseInt(anioActual, 10)
			var anos = anoAct - anonaci

		if(anos > 100){
			cuadroDialogo("la diferencia del a�o actual y el a�o de nacimiento no debe ser superior a 100.", 1);
			return false;
		}

		if(f.fechaNac.value!='' && !validaFE(f.fechaNac)){
			cuadroDialogo("Los caracteres ingresados en el campo fecha de nacimiento son inv&aacute;lidos.", 1);
			return false;
		} else {
			if(!validaFecha())	{
				return false;
			}
		}
		if(f.rfc.value.trim() == "" ){
			cuadroDialogo("El campo RFC es requerido.", 1);
			return false;
		}

		if(f.rfc.value!='' && !validaRFC(f.rfc)){
			cuadroDialogo("Los caracteres ingresados en el campo RFC  son inv&aacute;lidos.", 1);
			return false;
		}

		var valor =  f.paisNacimientoCombo.options[f.paisNacimientoCombo.selectedIndex].value;
		if( valor != "MEXI" && f.coloniaExtranjero.value.trim() == "" ){
			cuadroDialogo("El campo entidad federativa es requerido.", 1);
			return false;
		}

		if( f.lada.value.trim() == "" ){
			cuadroDialogo("El campo lada es requerido.", 1);
			return false;
		}

		if( f.telefono.value.trim() == "" ){
			cuadroDialogo("El campo telefono es requerido.", 1);
			return false;
		}

			if( f.sucursal.value.trim() == "" ){
			cuadroDialogo("El campo sucursal es requerido.", 1);
			return false;
		}

		var valor = document.AltaIndividualForm.rfc.value.toUpperCase();
		var naci = document.AltaIndividualForm.fechaNac.value.toUpperCase();
		var fenaci = (naci.substring(8, 10) + naci.substring(3, 5) + naci.substring(0, 2));
		var ferfc = valor.substring(4, 10);
		if (fenaci != ferfc) {
			cuadroDialogo("La fecha de nacimiento no concuerda con el RFC", 1);
			return false;
		}

		if(!validaHomoClave(f.homoclave)){
			cuadroDialogo("Los caracteres ingresados en el campo homoclave  son inv&aacute;lidos.", 1);
			return false;
		}

		if(f.noIdentificacion.value.trim() == ""){
			cuadroDialogo("El campo n&uacute;mero de identificaci&oacute;n es requerido.",1);
		       return false;
		}

		if(f.noIdentificacion.value!='' && !validaAlfa(f.noIdentificacion)){
			cuadroDialogo("Los caracteres ingresados en el campo n&uacute;mero de identificaci&oacute;n  son inv&aacute;lidos.", 1);
			return false;
		}


		if( f.calle.value.trim() == "" ) {
			cuadroDialogo("El campo calle es requerido.", 1);
			return false;
		}

		if(f.calle.value!='' && !validaAlfa(f.calle)){
			cuadroDialogo("Los caracteres ingresados en el campo calle son inv&aacute;lidos.", 1);
			return false;
		}

		if( f.noExterior.value.trim() == "" ){
			cuadroDialogo("El campo n&uacute;mero exterior es requerido.", 1);
			return false;
		}

		if(f.noExterior.value!='' && !validaAlfa(f.noExterior)){
			cuadroDialogo("Los caracteres ingresados en el campo n&uacute;mero exterior son inv&aacute;lidos.", 1);
			return false;
		}

		if(f.noInterior.value!='' && !validaAlfa(f.noInterior)){
			cuadroDialogo("Los caracteres ingresados en el campo n&uacute;mero interior son inv&aacute;lidos.", 1);
			return false;
		}

		if(f.coloniaExtranjero.value!='' && !validaAlfa(f.coloniaExtranjero)){
			cuadroDialogo("Los caracteres ingresados en el campo entidad federativa son inv&aacute;lidos.", 1);
			return false;
		}

		if( f.codPostal.value.trim() == "" ){
			cuadroDialogo("El campo c&oacute;digo postal es requerido.", 1);
			return false;
		}
		if(f.codPostal.value !='' && !validaEntero(f.codPostal)){
			cuadroDialogo("Los caracteres ingresados en el campo c&oacute;digo postal son inv&aacute;lidos.", 1);
			return false;
		}
		if(f.codPostal.value !='' && (f.codPostal.value.length!=<%=NomPreEmpleado.LNG_CODIGO_POSTAL%>)){
				cuadroDialogo("La longitud del campo c&oacute;digo postal es incorrecta.", 1);
				return false;
			}
			if(f.codPostal.value !='' && !validaCodPos(f.codPostal)){
				cuadroDialogo("Los caracteres ingresados en el campo c&oacute;digo postal son inv&aacute;lidos.", 1);
				return false;
			}

				if( f.colonia.selectedIndex == -1 && f.coloniaExtranjero.value==''){
			cuadroDialogo("El campo colonia es requerido.", 1);
			return false;
		}

			if( f.codPostalTrab.value.trim() == "" ){
			cuadroDialogo("El campo c&oacute;digo postal del trabajo es requerido.", 1);
			return false;
			}
			if(f.codPostalTrab.value !='' && !validaEntero(f.codPostalTrab)){
				cuadroDialogo("Los caracteres ingresados en el campo c&oacute;digo postal del trabajo son inv&aacute;lidos.", 1);
				return false;
			}
			if(f.codPostalTrab.value !='' && (f.codPostalTrab.value.length!=<%=NomPreEmpleado.LNG_CODIGO_POSTAL%>)){
				cuadroDialogo("La longitud del campo c&oacute;digo postal del trabajo es incorrecta.", 1);
				return false;
			}
			if(f.codPostalTrab.value !='' && !validaCodPos(f.codPostalTrab)){
				cuadroDialogo("Los caracteres ingresados en el campo c&oacute;digo postal del trabajo son inv&aacute;lidos.", 1);
				return false;
			}


			if( f.cveEstado.value.trim() == "" && f.cveEstadoNombre.value.trim() == "" ){
				cuadroDialogo("El campo ciudad o estado es requerido.", 1);
				return false;
			}

			if(f.cveEstado.value!='' && !validaAlfa(f.cveEstado)){
				cuadroDialogo("Los caracteres ingresados en el campo ciudad o estado son inv&aacute;lidos.", 1);
				return false;
			}


			return true;
		}

		function calculaEdad(fecha) {

                fecha_hoy = new Date();
				var anioActual = fecha_hoy.getFullYear();
				var mesActual = fecha_hoy.getMonth()+1;
				var diaActual = fecha_hoy.getDate();
                var dat1 = fecha.split("/")

				var anonaci=parseInt(dat1[2], 10)
				var anoAct=parseInt(anioActual, 10)
				var anos = anoAct - anonaci

				var mesnaci=parseInt(dat1[1], 10)
				var mesAct=parseInt(mesActual, 10)
				var mes  = mesAct - mesnaci

				var dia
				var dianaci
				var diaact

				if(mes < 0){
			    	anos=anos-1;
			    }else if(mes == 0){
			    	dianaci=parseInt(dat1[0], 10)
					diaact=parseInt(diaActual, 10)
					dia = diaact - dianaci;
					if(dia < 0){
						anos=anos-1;
					}
				}
				return anos;

}

		function validaNE(campo){
		return campo.value.match(/^[0-9]{1,<%=NomPreEmpleado.LNG_NO_EMPLEADO%>}$/);
		}
		function validaNO(campo){
			return campo.value.match(/^[\sA-Za-z]*$/);
		}
		function validaFE(campo){
			return campo.value.match(/^\d{1,2}\/\d{1,2}\/\d{2,4}$/);
		}
		function validaHomoClave(campo){
			return campo.value.match(/^[a-zA-Z0-9 � �]*$/);
		}
		function validaMail(campo){
			return campo.value.match(/^[0-9A-Za-z][A-Za-z0-9_.-]*@[A-Za-z0-9_-]+\.[A-Za-z0-9_.]+[A-za-z]$/);
		}
		function validaAlfa(campo){
			return campo.value.match(/^[\s-\w]*$/);
		}
		function validaEntero(campo){
			return campo.value.match(/^(?:\+|-)?\d+$/);
		}
		function validaCodPos(campo){
			return campo.value.match(/^[0-9]{<%=NomPreEmpleado.LNG_CODIGO_POSTAL%>}$/);
		}
		function validaTelefono(){
			var f=document.AltaIndividualForm;
			var lonTel=f.lada.value.length+f.telefono.value.length;
			if(lonTel!=10){
				return false;
			}
			return true;
		}


		function validaFecha()	{
			var ruta = document.AltaIndividualForm.fechaNac;
			if (parseInt(ruta.value.substring(0,1)) >= 0
								&& parseInt(ruta.value.substring(0,1)) <= 3) {
		  		if((parseInt(ruta.value.substring(1,2)) >= 0
		  			&& parseInt(ruta.value.substring(1,2)) <= 9
		  			&& parseInt(ruta.value.substring(0,1)) < 3)
		  			|| (parseInt(ruta.value.substring(1,2)) >= 0
		  			&& parseInt(ruta.value.substring(1,2)) <= 1
		  			&& parseInt(ruta.value.substring(0,1)) == 3)) {

			  			if (parseInt(ruta.value.substring(0,1)) !=0
			  					|| parseInt(ruta.value.substring(1,2))!= 0) {
				  				if ((parseInt(ruta.value.substring(4,5)) > 0
				  					&& parseInt(ruta.value.substring(4,5) )<= 9
				  					&& parseInt(ruta.value.substring(3,4)) == 0)
				  					|| (parseInt(ruta.value.substring(4,5)) >= 0
				  					&& parseInt(ruta.value.substring(4,5)) <= 2
				  					&& parseInt(ruta.value.substring(3,4)) == 1)) {
						  					var ano=new Date().getFullYear();
						  					if (ruta.value.substring(6,10).length == 4
						  						&& parseInt(ruta.value.substring(6,10)) < ano
						  						&& parseInt(ruta.value.substring(6,10)) > 1800) {
								  							if(ruta.value.substring(2,3) == '/' && ruta.value.substring(5,6)=='/') 	{
								  								return true;
								  							} else {
									  							cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar los separadores" ,1);
									  							return false;
									  						}
				  							} else {
						  						cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el a�o" ,1);
						  						return false;
						  					}
				  				} else	{
				  					cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el mes" ,1);
				  					return false;
			  					}
			  			} else {
			  				cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el d&iacute;a" ,1);
			  				return false;
			  			}
		  		} else {
		  			cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el d&iacute;a" ,1);
		  			return false;
		  		}
		  	} else {
				cuadroDialogo( "El formato de fecha es incorrecto. <br> Favor de revisar el d&iacute;a" ,1);
				return false;
			}
		}

	  function flechas(evt) {
		   evt = (evt) ? evt : event;
		   var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
		       ((evt.which) ? evt.which : 0));
		   if (charCode < 48 || charCode > 57) {
		       return false;
		   }
		   return true;
		}
		function soloNumeros(evt) {
		   evt = (evt) ? evt : event;
		   var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
		       ((evt.which) ? evt.which : 0));
		   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		       return false;
		   }
		   return true;
		}

		function convierteMayus(campo) {
			tmp = campo.value;
			str=trim(tmp);
			tmp=str.toUpperCase();
			campo.value=tmp;
			return true;
		}

        function trim(string){
    		return string.replace(/^\s*|\s*$/g, '');
		}


<%= request.getAttribute("newMenu")%>
</script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	bgcolor="#ffffff"
	onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif'); cargaEntidadFederativa(); inicia();"
	background="/gifs/EnlaceMig/gfo25010.gif">
<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top">
		<td width="*"><%= request.getAttribute("MenuPrincipal")%></td>
	</tr>
</table>
<%= request.getAttribute("Encabezado")%>
<table width="571" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
  </tr>
</table>
<FORM NAME="AltaIndividualForm" METHOD="Post" ACTION="AltaIndividual">
<INPUT TYPE="hidden" VALUE="altaInit" NAME="opcion">


<table width="760" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center"><br>
		<table width="600" border="0" cellspacing="2" cellpadding="2"
			bgcolor="#EBEBEB">
			<tr>
				<td width="600" class="tittabdat" colspan="3"><input
					type="radio" name="tipoCarga" value="AL" checked="checked" />En
				l&iacute;nea</td>
			</tr>
			<tr>
				<td class=textabdatcla colspan="1">* No. de tarjeta asignada</td>
				<td class=textabdatcla colspan="2">* Nombre del empleado</td>
			</tr>
			<tr>
				<td class=textabdatcla colspan="1"><input type="text"
					value="<%=empleadoBean.getNumTarjeta()%>" name="numTarjeta"
					maxlength="16" size="25" tabindex="1"
					onkeypress="return soloNumeros(event); return flechas(event);">
				</td>
				<td class=textabdatcla colspan="2"><input type="text"
					value="<%=empleadoBean.getNombreEmpleado()%>" name="nombreEmpleado"
					maxlength="40" size="50" tabindex="2" onchange="convierteMayus(this);">
				</td>
			</tr>
			<tr>
				<td class=textabdatcla>
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td class=textabdatcla>* RFC</td>
						<td class=textabdatcla>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Homoclave</td>
					</tr>
				</table>
				</td>
				<td class=textabdatcla>* Apellido paterno</td>
				<td class=textabdatcla>* Apellido materno</td>
			</tr>
			<tr>
				<td class=textabdatcla>
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td class=textabdatcla><input type="text"
							value="<%=empleadoBean.getRfc()%>" name="rfc" maxlength="10"
							size="10" tabindex="5" onchange="convierteMayus(this);"></td>
						<td class=textabdatcla>&nbsp;<nobr /><input type="text"
							value="<%=empleadoBean.getHomoclave()%>" name="homoclave"
							maxlength="3" size="5" tabindex="6" onchange="convierteMayus(this);"></td>
					</tr>
				</table>
				</td>
				<td class=textabdatcla><input type="text"
					value="<%=empleadoBean.getAppPaterno()%>" name="appPaterno"
					maxlength="30" size="22" tabindex="3" onchange="convierteMayus(this);">
				</td>
				<td class=textabdatcla><input type="text"
					value="<%=empleadoBean.getAppMaterno()%>" name="appMaterno"
					maxlength="<%=NomPreEmpleado.LNG_MATERNO%>" size="22" tabindex="4"
					onchange="convierteMayus(this);"></td>
			</tr>
			<tr bgcolor=#FFFFFF>
				<td class=textabdatcla>* Tipo de Identificaci�n</td>
				<td class=textabdatcla>* No. de identificaci&oacute;n</td>
				<%--<td class=textabdatcla colspan="2">
							* Tipo de identificaci&oacute;n
						</td>--%>
			</tr>

			<tr>
				<td class=textabdatcla><input type="hidden" name="tipoDocto"
					value="<%=empleadoBean.getTipoDocto()%>"> <select
					name="tipoDoctoCombo" tabindex="7" onchange="obtenValor(this)">
					<option value="IF" <%if (empleadoBean.getTipoDocto().equals("IF")) {%>selected <%}%>>IFE</option>
					<option value="CP" <%if (empleadoBean.getTipoDocto().equals("CP")) {%>selected <%}%>>CEDULA PROFESIONAL</option>
					<option value="FM" <%if (empleadoBean.getTipoDocto().equals("FM")) {%>selected <%}%>>FORMA MIGRATORIA</option>
					<option value="ID" <%if (empleadoBean.getTipoDocto().equals("ID")) {%>selected <%}%>>ID-USA, TARJETA DE IDENTIDAD</option>
					<option value="CM" <%if (empleadoBean.getTipoDocto().equals("CM")) {%>selected <%}%>>CERTIFICADO MATRICULA CONSULAR</option>
					<option value="IS" <%if (empleadoBean.getTipoDocto().equals("IS")) {%>selected <%}%>>CREDENCIAL DE ISSSTE</option>
					<option value="LC" <%if (empleadoBean.getTipoDocto().equals("LC")) {%>selected <%}%>>LICENCIA PARA CONDUCIR</option>
					<option value="LU" <%if (empleadoBean.getTipoDocto().equals("LU")) {%>selected <%}%>>LICENCIA DE CONDUCIR U.S.A.</option>
					<option value="P" <%if (empleadoBean.getTipoDocto().equals("P")) {%>selected <%}%>>PASAPORTE</option>
					<option value="TM" <%if (empleadoBean.getTipoDocto().equals("TM")) {%>selected <%}%>>TARJ. UNICA IDENTIDAD MILITAR</option>
					<option value="SM" <%if (empleadoBean.getTipoDocto().equals("SM")) {%>selected <%}%>>CARTILLA SERVICIO MILITAR NAL</option>
					<option value="SS" <%if (empleadoBean.getTipoDocto().equals("SS")) {%>selected <%}%>>CREDENCIAL DE PENSIONADOS IMSS</option>

				</select></td>

				<td class=textabdatcla><input type="text"
					value="<%=empleadoBean.getNoIdentificacion()%>"
					name="noIdentificacion" maxlength="20" size="23" tabindex="8" onchange="convierteMayus(this);"></td>
			</tr>
			<tr bgcolor=#FFFFFF>
				<td class=textabdatcla>  No. de empleado</td>
				<td class=textabdatcla>* Fecha de nacimiento<br>
				(dd/mm/aaaa)</td>
				<td class=textabdatcla colspan="3">* Nacionalidad</td>
			</tr>
			<tr>
				<td class=textabdatcla><input type="text"
					value="<%=empleadoBean.getNoEmpleado()%>" name="noEmpleado"
					maxlength="<%=NomPreEmpleado.LNG_NO_EMPLEADO%>" size="20" maxlength="7"
					onkeypress="return soloNumeros(event); return flechas(event);"
					tabindex="9" onblur="cambiaEmpleado();"></td>
				<td class=textabdatcla><input type="text"
					value="<%=empleadoBean.getFechaNac()%>" name="fechaNac"
					maxlength="<%=NomPreEmpleado.LNG_FECHA_NACIMIENTO%>" size="12" tabindex="10">
				</td>
				<td class=textabdatcla><input type="hidden"
					name="cveNacionalidad"
					value="<%=empleadoBean.getCveNacionalidad()%>"> <select
					name="cveNacionalidadCombo" tabindex="11"
					onchange="obtenValorNacionalidad(this);">

					<option value="MEXI"
						<%if (empleadoBean.getCveNacionalidad().equals("MEXI")) {%>
						selected <%}%>>MEXI=Mexicana</option>
					<option value="USA"
						<%if (empleadoBean.getCveNacionalidad().equals("USA")) {%>
						selected <%}%>>USA=Estadounidense</option>
					<option value="CANA"
						<%if (empleadoBean.getCveNacionalidad().equals("CANA")) {%>
						selected <%}%>>CANA=Canadiense</option>
					<option value="ESPA"
						<%if (empleadoBean.getCveNacionalidad().equals("ESPA")) {%>
						selected <%}%>>ESPA=Espa�ola</option>
					<option value="FRA"
						<%if (empleadoBean.getCveNacionalidad().equals("FRA")) {%>
						selected <%}%>>FRAN=Franc&eacute;s</option>

				</select></td>
			</tr>
			<tr bgcolor=#FFFFFF>
				<td class=textabdatcla colspan="1" height="24">* Pa&iacute;s de
				nacimiento</td>
				<td class=textabdatcla colspan="2" height="24">* Entidad
				federativa</td>
			</tr>
			<tr>
				<td class=textabdatcla height="24"><input type="hidden"
					name="paisNacimiento" value="<%=empleadoBean.getPaisNacimiento()%>">
				<select name="paisNacimientoCombo" tabindex="12"
					onchange="obtenValorPais(this); validaExtranjero(this);">

					<option value="MEXI"
						<%if (empleadoBean.getPaisNacimiento().equals("MEXI")) {%>
						selected <%}%>>MEXI=Mexicana</option>
					<option value="USA "
						<%if (empleadoBean.getPaisNacimiento().equals("USA ")) {%>
						selected <%}%>>USA=Estadounidense</option>
					<option value="CANA"
						<%if (empleadoBean.getPaisNacimiento().equals("CANA")) {%>
						selected <%}%>>CANA=Canadiense</option>
					<option value="ESPA"
						<%if (empleadoBean.getPaisNacimiento().equals("ESPA")) {%>
						selected <%}%>>ESPA=Espa�ola</option>
					<option value="FRAN"
						<%if (empleadoBean.getPaisNacimiento().equals("FRAN")) {%>
						selected <%}%>>FRA=Franc&eacute;s</option>

				</select></td>
				<td class=textabdatcla height="24"><select name="entidadFederativaCombo"
					tabindex="13" onchange="obtenValorEntidad(this);" size="1">

					<option value="AS"
						<%if (empleadoBean.getCveEstado().equals("AS")) {%> selected <%}%>>AGUASCALIENTES</option>
					<option value="BC"
						<%if (empleadoBean.getCveEstado().equals("BC")) {%> selected <%}%>>BAJA
					CALIFORNIA</option>
					<option value="BS"
						<%if (empleadoBean.getCveEstado().equals("BS")) {%> selected <%}%>>BAJA
					CALIFORNIA SUR</option>
					<option value="CC"
						<%if (empleadoBean.getCveEstado().equals("CC")) {%> selected <%}%>>CAMPECHE</option>
					<option value="CH"
						<%if (empleadoBean.getCveEstado().equals("CH")) {%> selected <%}%>>CHIHUAHUA</option>
					<option value="CL"
						<%if (empleadoBean.getCveEstado().equals("CL")) {%> selected <%}%>>COAHUILA</option>
					<option value="CM"
						<%if (empleadoBean.getCveEstado().equals("CM")) {%> selected <%}%>>COLIMA</option>
					<option value="CS"
						<%if (empleadoBean.getCveEstado().equals("CS")) {%> selected <%}%>>CHIAPAS</option>
					<option value="DF"
						<%if (empleadoBean.getCveEstado().equals("DF")) {%> selected <%}%>>DISTRITO
					FEDERAL</option>
					<option value="DG"
						<%if (empleadoBean.getCveEstado().equals("DG")) {%> selected <%}%>>DURANGO</option>
					<option value="GR"
						<%if (empleadoBean.getCveEstado().equals("GR")) {%> selected <%}%>>GUERRERO</option>
					<option value="GT"
						<%if (empleadoBean.getCveEstado().equals("GT")) {%> selected <%}%>>GUANAJUATO</option>
					<option value="HG"
						<%if (empleadoBean.getCveEstado().equals("HG")) {%> selected <%}%>>HIDALGO</option>
					<option value="JC"
						<%if (empleadoBean.getCveEstado().equals("JC")) {%> selected <%}%>>JALISCO</option>
					<option value="MC"
						<%if (empleadoBean.getCveEstado().equals("MC")) {%> selected <%}%>>MEXICO</option>
					<option value="MN"
						<%if (empleadoBean.getCveEstado().equals("MN")) {%> selected <%}%>>MICHOACAN</option>
					<option value="MS"
						<%if (empleadoBean.getCveEstado().equals("MS")) {%> selected <%}%>>MORELOS</option>
					<option value="NL"
						<%if (empleadoBean.getCveEstado().equals("NL")) {%> selected <%}%>>NUEVO
					LEON</option>
					<option value="NT"
						<%if (empleadoBean.getCveEstado().equals("NT")) {%> selected <%}%>>NAYARIT</option>
					<option value="OC"
						<%if (empleadoBean.getCveEstado().equals("OC")) {%> selected <%}%>>OAXACA</option>
					<option value="PL"
						<%if (empleadoBean.getCveEstado().equals("PL")) {%> selected <%}%>>PUEBLA</option>
					<option value="QR"
						<%if (empleadoBean.getCveEstado().equals("QR")) {%> selected <%}%>>QUINTANA
					ROO</option>
					<option value="QT"
						<%if (empleadoBean.getCveEstado().equals("QT")) {%> selected <%}%>>QUERETARO</option>
					<option value="SL"
						<%if (empleadoBean.getCveEstado().equals("SL")) {%> selected <%}%>>SINALOA</option>
					<option value="SP"
						<%if (empleadoBean.getCveEstado().equals("SP")) {%> selected <%}%>>SAN
					LUIS POTOSI</option>
					<option value="SR"
						<%if (empleadoBean.getCveEstado().equals("SR")) {%> selected <%}%>>SONORA</option>
					<option value="TC"
						<%if (empleadoBean.getCveEstado().equals("TC")) {%> selected <%}%>>TABASCO</option>
					<option value="TL"
						<%if (empleadoBean.getCveEstado().equals("TL")) {%> selected <%}%>>TLAXCALA</option>
					<option value="TS"
						<%if (empleadoBean.getCveEstado().equals("TS")) {%> selected <%}%>>TAMAULIPAS</option>
					<option value="VZ"
						<%if (empleadoBean.getCveEstado().equals("VZ")) {%> selected <%}%>>VERACRUZ</option>
					<option value="YN"
						<%if (empleadoBean.getCveEstado().equals("YN")) {%> selected <%}%>>YUCATAN</option>
					<option value="ZS"
						<%if (empleadoBean.getCveEstado().equals("ZS")) {%> selected <%}%>>ZACATECAS</option>

				</select><input type="text" name="coloniaExtranjero"
					value="<%=empleadoBean.getColoniaExtranjero()%>" tabindex="13"
					onchange="convierteMayus(this);" style="display: none;" size="19">
				</td>

			</tr>
			<tr bgcolor=#FFFFFF>
				<td class=textabdatcla>* Estado civil</td>
				<td class=textabdatcla>* Sexo</td>
				<td class=textabdatcla></td>
			</tr>
			<tr>
				<td class=textabdatcla><input type="hidden" name="edoCivil"
					value="<%=empleadoBean.getEdoCivil()%>">
					<select
					name="edoCivilCombo" tabindex="14" onchange="obtenValorEdoCivil(this)">
					<option value="S"
                    <%if (empleadoBean.getEdoCivil().equals("S")) {%> selected <%}%> >Soltero</option>
					<option value="C"
					<%if (empleadoBean.getEdoCivil().equals("C")) {%> selected <%}%> >Casado</option>
					<option value="V"
					<%if (empleadoBean.getEdoCivil().equals("V")) {%> selected <%}%> >Viudo</option>
					<option value="U"
					<%if (empleadoBean.getEdoCivil().equals("U")) {%> selected <%}%> >Union Libre</option>
					<option value="D"
					<%if (empleadoBean.getEdoCivil().equals("D")) {%> selected <%}%> >Divorciado</option>
				</select></td>
				<td class=textabdatcla>
				<input type="hidden" name="sexocaja" value="Masculino">
				<select name="sexo" tabindex="15" onchange="obtenValorSexo(this)" >
					<option value="Masculino"
					<%if (empleadoBean.getSexo().equals("Masculino")) {%> selected <%}%> >Masculino</option>
					<option value="Femenino"
					<%if (empleadoBean.getSexo().equals("Femenino")) {%> selected <%}%>  >Femenino</option>
				</select></td>
				<td></td>
			</tr>
			<tr>
				<td class=textabdatcla width="54%">* Calle</td>
				<td class=textabdatcla width="34%">* No.
				Ext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No.
				Int</td>
				<td class=textabdatcla colspan="1">* Colonia</td>
			</tr>
			<tr>
				<td class=textabdatcla width="70%"><input type="text"
					value="<%=empleadoBean.getCalle()%>" name="calle"
					maxlength="<%=NomPreEmpleado.LNG_CALLE%>" size="30" tabindex="16"
					onchange="convierteMayus(this);"></td>
				<td class=textabdatcla width="20%"><input type="text"
					value="<%=empleadoBean.getNoExterior()%>" name="noExterior"
					maxlength="<%=NomPreEmpleado.LNG_NUM_EXT%>" size="5" tabindex="17"
					onchange="convierteMayus(this);"> &nbsp;&nbsp;&nbsp;&nbsp;<input
					type="text" value="<%=empleadoBean.getNoInterior()%>"
					name="noInterior" maxlength="<%=NomPreEmpleado.LNG_NUM_INT%>"
					size="5" tabindex="18" onchange="convierteMayus(this);"></td>
				<td class=textabdatcla><select name="colonia" tabindex="19">
					<%=request.getAttribute("colonia")%>
				</select></td>

			</tr>
			<tr>
				<td class=textabdatcla>* C&oacute;digo postal</td>

			</tr>
			<tr>
				<td class=textabdatcla colspan="1"><input type="text"
					value="<%=request.getAttribute("codPostal") == null ? ""
					: request.getParameter("codPostal")%>"
					name="codPostal"
					onkeypress="return soloNumeros(event); return flechas(event);"
					maxlength="<%=NomPreEmpleado.LNG_CODIGO_POSTAL%>" tabindex="20"
					onblur="cargaCodPostal()" size="12"></td>
				<td class=textabdatcla></td>
				<td class=textabdatcla></td>
			</tr>
			<tr>
				<td class=textabdatcla>* Estado</td>
				<td class=textabdatcla colspan="3">*Lada / *Tel&eacute;fono</td>
			</tr>
			<tr>
				<td class=textabdatcla><input type="hidden" name="cveEstado"
					value="<%=request.getAttribute("cveEstado")%>"> <input
					type="text" value="<%=request.getAttribute("cveEstadoNombre")%>"
					name="cveEstadoNombre" disabled
					maxlength="<%=NomPreEmpleado.LNG_ESTADO%>" size="27"
					onchange="convierteMayus(this);"></td>
				<td class=textabdatcla colspan="3"><input type="text"
					value="<%=empleadoBean.getLada()%>" name="lada" maxlength="3"
					size="5" tabindex="21"
					onkeypress="return soloNumeros(event); return flechas(event);">
				<input type="text" value="<%=empleadoBean.getTelefono()%>"
					name="telefono" maxlength="8" size="10" tabindex="22"
					onkeypress="return soloNumeros(event); return flechas(event);">
				</td>
			</tr>

			<tr>
				<td class=textabdatcla>* CP Trabajo</td>
				<td class=textabdatcla colspan="3">* Sucursal</td>
			</tr>
			<tr>
				<td class=textabdatcla><input type="text"
					value="<%=empleadoBean.getCodPostalTrab()%>" name="codPostalTrab"
					maxlength="5" size="25" tabindex="23"
					onkeypress="return soloNumeros(event); return flechas(event);">
				</td>
				<td class=textabdatcla colspan="3"><input type="text"
					value="<%=empleadoBean.getSucursal()%>" name="sucursal"
					maxlength="4" size="25" tabindex="24"
					onkeypress="return soloNumeros(event); return flechas(event);">
				</td>
			</tr>

			<tr>
				<td></td>
			</tr>
			<tr>
				<td class="textabdatcla"><br>
				<b>* Campos requeridos</b></td>
				<td class="textabdatcla" colspan="2"><br>
				* No ponga acentos ni caracteres especiales</td>
			</tr>
		</table>
		<br>
		<!-- #BeginLibraryItem "/Library/footerPaginacion.lbi" -->
		<table id="enviando2" style="visibility:hidden" border=0 align=center cellpadding=0 cellspacing=0>
   			<tr align="center" class="tabmovtex">
   	 			<td>Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>
    		</tr>
    	</table>
		<table id="enviando" align=center border=0 cellspacing=0 cellpadding=0>
	  		<tr>
				<td align="center"><input type="image" border="0"
					name="Aceptar" src="../../gifs/EnlaceMig/gbo25280.gif" width="90"
					height="22" alt="Aceptar"
					onclick="javascript:altaIndividual();return false"></td>
				<td><input type="image" border="0" name="Limpiar"
					src="../../gifs/EnlaceMig/gbo25250.gif" width="90" height="22"
					alt="Limpiar" onclick="javascript:Limpiar();return false"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<div align="center"></div>

</form>
</body>
</html>