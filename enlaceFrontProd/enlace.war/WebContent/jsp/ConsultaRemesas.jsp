<%@ page import="mx.altec.enlace.beans.*,java.util.*"%>
<%@ page import="mx.altec.enlace.bita.BitaConstants"%>
<jsp:include page="/jsp/prepago/headerPrepago.jsp"/>
<script language="javascript">

<%
 String FechaHoy="";
 FechaHoy=(String)request.getAttribute("FechaHoy");
 if(FechaHoy==null)
    FechaHoy="";
%>

//Indice Calendario
	var Indice=0;


//Arreglos de fechas
<%
	if(request.getAttribute("VarFechaHoy")!= null)
		out.print(request.getAttribute("VarFechaHoy"));
%>

//Dias inhabiles
	diasInhabiles='<% if(request.getAttribute("DiasInhabiles")!= null) out.print(request.getAttribute("DiasInhabiles")); %>';

<%
	String DiaHoy="";
	if(request.getAttribute("DiaHoy")!= null)
 		DiaHoy=(String)request.getAttribute("DiaHoy");
%>

var js_diasInhabiles = "<%= request.getAttribute("diasInhabiles") %>";
 var fechaseleccionada;
 var opcioncalrem;

 var mensaje="<%= request.getAttribute("mensaje") %>";

 if(mensaje.length>0 && mensaje!="null")
 {
 cuadroDialogo(mensaje, 1);
 }
function validar()
{

val=false;
valcheck=false;
	var f=document.MantNomina;
	var radios = document.MantNomina.tipoFecha;
	var checks = document.MantNomina.chkEstado;

	if(f.fecha1.value.length > 0 && f.fecha2.value.length>0 )
	{
		if (mayor(f.fecha2.value, f.fecha1.value)){
				cuadroDialogo("La Fecha Inicial no puede ser mayor a la Fecha Final", 1);
				return false;
			}
	}

	for(var i = 0; i < radios.length; i++) {
		if( radios[i].checked == true )
		{
		val = true;

		}
	}


	if(val)
	{

	 if(f.fecha1.value.length == 0 || f.fecha2.value.length==0)
		   {
		   		cuadroDialogo("Las fechas son obligatorias si activas un tipo de Fecha", 1);
		   		return false;
		   }

	}
	else
	{

		if(f.fecha1.value.length > 0 || f.fecha2.value.length>0)
			{
			cuadroDialogo("Se debe de seleccionar tipo fecha si se especifican fechas", 1);
			   		return false;
			}

			if(f.fecha1.value.length == 0 && f.fecha2.value.length==0 && f.folioRemesa.value.length==0)
			{
			cuadroDialogo("Si no se ingresa ning�n filtro, el �nico obligatorio es el Folio de Remesa", 1);
			   		return false;
			}

			if (f.folioRemesa.value != "" && !f.folioRemesa.value.match(/^[0-9]+$/)) {
				cuadroDialogo("El n&uacute;mero de remesa no es v&aacute;lido.", 1);
				return;
			}

	}
	return true;
}

function mayor(fecha, fecha2){
var fechaIni=fecha.split("/");
var fechaFin=fecha2.split("/");
var fechainicial=fechaIni[2]+fechaIni[1]+fechaIni[0];
var fechafinal=fechaFin[2]+fechaFin[1]+fechaFin[0];
	if(parseInt(fechainicial)>parseInt(fechafinal)){
		return(true);
	}else{
		return(false);
	}
}

function consultar(){
	if(validar()){
		document.MantNomina.operacion.value="8";
		document.MantNomina.submit();
	}
}

function limpiar() {
	document.MantNomina.reset();
}
function WindowCalendar()
    {
        var m=new Date();
        var dia1;
        m.setFullYear(document.Frmfechas.strAnio.value);
        m.setMonth(document.Frmfechas.strMes.value);
        m.setDate(document.Frmfechas.strDia.value);
        n=m.getMonth();
        dia=document.Frmfechas.strDia.value;
        dia1 = parseInt(dia,10);
        dia = dia1 + 25;
        mes=document.Frmfechas.strMes.value;
        anio=document.Frmfechas.strAnio.value;
        opcioncalrem=2;

        msg=window.open("/EnlaceMig/calpas1.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
        msg.focus();
    }

    function WindowCalendar1()
    {
        var m=new Date();
        m.setFullYear(document.Frmfechas.strAnio.value);
        m.setMonth(document.Frmfechas.strMes.value);
        m.setDate(document.Frmfechas.strDia.value);
        n=m.getMonth();
        dia=document.Frmfechas.strDia.value;
        mes=document.Frmfechas.strMes.value;
        anio=document.Frmfechas.strAnio.value;

        msg=window.open("/EnlaceMig/calNom2.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
        msg.focus();
    }

        function Actualiza()
    {
        if (opcioncalrem==1)
        document.MantNomina.fecha1.value=fechaseleccionada;
        else if (opcioncalrem==2)
        document.MantNomina.fecha2.value=fechaseleccionada;
    }

    function soloNumeros(evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
        ((evt.which) ? evt.which : 0));
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
 }

 /*Funcion para actualizar el registro*/
function Actualiza(){
	if(Indice==0)
		document.MantNomina.fecha2.value=Fecha[Indice];
	if(Indice==1)
		document.MantNomina.fecha1.value=Fecha[Indice];
}

 /*Funcion para mostrar los calendarios*/
function js_calendario(ind){
	var m = new Date()
	Indice = ind;
    n = m.getMonth();
    msg = window.open("/EnlaceMig/EI_Calendario.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
    msg.focus();
}
</script>
<form name = "Frmfechas">
  <Input type = "hidden" name ="strAnio" value = "<%=request.getAttribute("anioRem") %>">
  <Input type = "hidden" name ="strMes" value = "<%=request.getAttribute("mesRem") %>">
  <Input type = "hidden" name ="strDia" value = "<%=request.getAttribute("diaRem") %>">
</form>
<form action="${ctx}AdmonRemesasServlet" method="get" name="MantNomina">
	<input type="hidden" name="operacion" value="1">
	<input type="hidden" name="inicio" value="inicio">
		<table cellspacing="0" cellpadding="0" border="0" width="760">
			<tr>
			 <td align="center">
			    <table border="0" cellspacing="2" cellpadding="3">
				 <tr>
				   <td class="tittabdat" nowrap> Seleccione los datos:</td>
				 </tr>
				 <tr align="center">
				   <td class="textabdatcla" valign="top">
				   <table width="420" border="0" cellspacing="0" cellpadding="5">
					    <tr>
							<td align="right" class="tabmovtex" nowrap>Tipo de Fecha:</td>
							<td class="tabmovtex" nowrap>
								<input type="radio" value="A" name="tipoFecha" />Asignaci&oacute;n
								<input type="radio" value="C" name="tipoFecha" />Cancelaci&oacute;n
								<input type="radio" value="E" name="tipoFecha" />Enviada
							</td>
						</tr>
						<tr>
		                 <td align="right" class="tabmovtex">Fecha Inicial:</td>
						 <td class="tabmovtex" nowrap valign="middle"><input type="text" name="fecha2" size="12" class="tabmovtex" onfocus="blur();"><a href ="javascript:js_calendario(0);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" style="vertical-align:middle;"></a></td>
						</tr>
						<tr>
						 <td align="right" class="tabmovtex">Fecha Final</td>
						 <td class="tabmovtex" nowrap valign="middle"><input type="text" name="fecha1" size="12" class="tabmovtex" onfocus="blur();"><a href ="javascript:js_calendario(1);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" style="vertical-align:middle;"></a></td>
						</tr>
						<tr>
						 <td align="right" class="tabmovtex">Folio de Remesa:</td>
						 <td class="tabmovtex" nowrap><input type="text" name="folioRemesa" onkeypress="return soloNumeros(event)" size="15" maxlength=15 class="tabmovtex"></td>
						</tr>

					   </table>
				   </td>
		          </tr>
		        </table>
			 </td>
		    </tr>
			<tr>
			<td>
				<table height="40" cellspacing="0" cellpadding="0" border="0" width="760">
					<tr><td></td></tr>
					<tr>
						<td align="center" class="texfootpagneg">
							<table height="40" cellspacing="0" cellpadding="0" border="0" width="168">
								<tr>
									<td>
						 		 		<a href = "javascript:consultar();"><img src="/gifs/EnlaceMig/gbo25220.gif" border=0 alt="Consultar" width="80" height="22"></a>
                        			</td>
									<td >
										<a href="javascript:limpiar();"><img src="/gifs/EnlaceMig/gbo25250.gif" border="0" alt="Limpiar" width="80" height="22"> </a>
									</td>
								</tr>
							</table>
						</td>
					</tr>

				</table>
			</td>
		</tr>
	</table>
</form>
<jsp:include page="/jsp/prepago/footerPrepago.jsp"/>