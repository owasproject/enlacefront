<%@page contentType="text/html"%>

<jsp:useBean id="fechaLib" class="java.util.GregorianCalendar" scope="session"/>
<jsp:useBean id="fechaLim" class="java.util.GregorianCalendar" scope="session"/>
<jsp:useBean id="Mensaje"  class="java.lang.String" scope="request"/>


<%!
    java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat ("dd/MM/yyyy");
    boolean escribe = false;
    long NumPago = 0;
%>


<%@page import="java.util.Date"%>
<html>
<head>
<title>Enlace Registro de Pago Directo en L&iacute;nea</title>
<script type="text/javascript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/validacionesFiltroCuentas.js"></script>
<script type="text/javascript">

var fechaLib;
var fechaLim;
var modulo = 'Registro';
var fecha_inicial='<%=sdf.format (new Date())%>';

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************************/

function VerificaFechas(txtLibramiento, txtLimite)
 {

 lib_year         = parseInt(txtLibramiento.value.substring(6,10),10);
 lib_month        = parseInt(txtLibramiento.value.substring(3, 5),10);
 lib_date         = parseInt(txtLibramiento.value.substring(0, 2),10);
 fechaLibramiento = new Date(lib_year, lib_month, lib_date);

 lim_year     = parseInt(txtLimite.value.substring(6,10),10);
 lim_month    = parseInt(txtLimite.value.substring(3, 5),10);
 lim_date     = parseInt(txtLimite.value.substring(0, 2),10);
 fechaLimite  = new Date(lim_year, lim_month, lim_date);

     if( fechaLibramiento > fechaLimite)
      {
		cuadroDialogo("La Fecha de Inicio no puede ser mayor a la Fecha Final.", 3);
        return false;
      }

  return true;

 }


var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}
function actualizacuenta()
{
  //document.frmRegistro.CuentaCargo.value=ctaselec +"  "+ctadescr;
  document.frmRegistro.cboCuentaCargo.value=ctaselec;

}

<%= request.getAttribute("newMenu") %>

function MuestraCalendario () {
	if(document.frmRegistro.opcion[0].checked){
		return;
    }else{
		if(modulo == 'Registro')
	    fechaLib = document.frmRegistro.FechaLibramiento.value;
		else
		fechaLim= document.frmRegistro.FechaLimite.value;
	    msg=window.open("jsp/pdCalendarioPag.jsp","Calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
	    msg.focus();
    }
}

function continua () {
	fecha = "" + fechaLim;
	if (fecha != "undefined") {
	    if (modulo == 'Registro') {
	        document.frmRegistro.FechaLimite.value = fechaLim;
	    } else {
	        document.frmRegistro.FechaLibramiento.value = fechaLim;
	    }
	}
}

function seleccionaFechas(){

	if(document.frmRegistro.opcion[1].checked){
		document.frmRegistro.FechaLibramiento.value = "";
		document.frmRegistro.FechaLimite.value = "";
	}
	if(document.frmRegistro.opcion[0].checked){
		document.frmRegistro.FechaLibramiento.value = fecha_inicial;
		document.frmRegistro.FechaLimite.value = fecha_inicial;
		return;
	}
}

// FSW INDRA
// Marzo 2015
// Enlace PYMES

function obtenerCuentaSeleccionada(combo, elemento) {
	var select = document.getElementById(combo);
	if (!(select === undefined)) {
		var valor = select.options[select.selectedIndex].value;

		if(valor !== null
	   	&& "" !==valor){
			document.getElementById(elemento).value = obtenerCuenta(valor);
		}
	}
}

function obtenerCuenta(valor){
    var res = valor.split("\|");
	 if (res[0]=="") {
    	return "";
    };
    if(res.length>1){
	    return res[0];
    }
}
// FSW INDRA
// Marzo 2015
// Enlace PYMES


function consultarLC(){
	obtenerCuentaSeleccionada("comboCuenta0", "cboCuentaCargo");


	if(document.frmRegistro.FechaLibramiento==null || document.frmRegistro.FechaLibramiento.value==""
		&& document.frmRegistro.FechaLimite!==null || document.frmRegistro.FechaLimite.value==""){
		cuadroDialogo('Falta seleccionar el rango de fechas para la busqueda', 3);
		return;
	}

	if(document.frmRegistro.cboCuentaCargo==null ||
			document.frmRegistro.cboCuentaCargo.value==''){
		cuadroDialogo('Falta seleccionar la cuenta', 3);
		return;
	}
		document.frmRegistro.opcionConSUA.value='1C';
		document.frmRegistro.submit();


}
/**
*Funcion para regresar a Pago SUA.
*
**/

function regresarConsulta(){
	document.CapturaLC.opcionConSUA.value="1";
	document.CapturaLC.action="SUALineaCapturaServlet?opcionLC=1&opcionConSUA=1";
	document.CapturaLC.submit();
	return;
}
</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>

</HEAD>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
style=" bgcolor: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);"
onLoad="consultaCuentas(0,2,1);MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');<%if (!Mensaje.equals("")) out.println("cuadroDialogo('" + Mensaje + "', 1)");%>" >
<%-- modificar para cuando es NomBen y cuando es no registrado ya que lo esta tomando para nomben y este no existe obviamente --%>
<%--if (escribe && request.getAttribute("Escibe" ) == null) out.print ("frmRegistro.NomBen.value = " + Pago.getClaveBen () + ";");--%>


<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <%-- MENU PRINCIPAL --%>
       <%= request.getAttribute("MenuPrincipal") %></td>
  </tr>
</TABLE>
 <%= request.getAttribute("Encabezado") %>
<FORM  NAME="frmRegistro"  method="post"  action="SUAConsultaPagoServlet">
<input type="hidden" value="1" name="opcionConSUA"/>
<table width="760" border="0" cellspacing="0" cellpadding="0">

    <tr>
      <td align="center">
        <table width="70%" border="0" cellspacing="2" cellpadding="3">
          <tr>
            <td class="tittabdat" colspan="2"> Periodo a Consultar</td>
          </tr>
          <tr valign="top" >
    					<td colspan="3">
		          			<%@ include file="/jsp/filtroConsultaCuentasPRSAT.jspf" %>
		          		</td>
		  </tr>

          <tr align="center">
            <td class="textabdatcla" valign="top" colspan="2">
              <table border="0" cellspacing="0" cellpadding="0">
                <tr valign="top">
                  <td align="center">
                    <table border="0" cellspacing="5" cellpadding="0">
                      <tr>
                             <td colspan="4" class="textabdatcla">Cuenta:
		    						<%-- <input type="text" tabindex="1" name="CuentaCargo"  class="tabmovtexbol"  size="25" onfocus="blur();">
		    						 <A tabindex="2" HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle/></A>
					    				<input type="hidden"  name="cboCuentaCargo" /> --%>
					    				<div id="CtaNombre" ></div>

					    				<%@ include file="/jsp/listaCuentasTransferencias.jspf" %>
 										<input type="hidden" name="cboCuentaCargo" id="cboCuentaCargo" value=""/>


		    				</td>
                      </tr>
                      <tr class="textabdatcla">
                      	<td colspan="2" width="100px">
                      		<input type="radio" name="opcion" checked onclick="javascript:seleccionaFechas();" value="delDia"/>Del D&iacute;a </td>
                      	<td></td>
                      	<td></td>
                      </tr>
                      <tr class="textabdatcla">
                      	<td colspan="2"  width="100px">
                      		<input type="radio" name="opcion" onclick="javascript:seleccionaFechas();" value="historico"/>Hist&oacute;rico </td>
                      	<td></td>
                      	<td></td>
                      </tr>
                      <tr>
                        <td  colspan="3"class="tabmovtex" nowrap valign="middle" align="right">De la Fecha:</td>
                        <td class="tabmovtex" nowrap valign="middle" colspan="2">
						  <input type="text" name="FechaLibramiento" size="20" class="tabmovtex"
						  	value="<%=sdf.format (new Date())%>" onFocus="blur();" maxlength="10" />

						  <a href="javascript:modulo = 'Consultas';MuestraCalendario();"><img src="/gifs/EnlaceMig/gbo25410.gif" style=" border: 0;" width="12" height="14" alt="Calendario"/></a>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="3" class="tabmovtex" nowrap valign="middle" align="right">A la Fecha:</td>
                        <td class="tabmovtex" nowrap valign="middle" colspan="2">
						  <input type="text" name="FechaLimite" size="20" class="tabmovtex"  onFocus="blur();" maxlength="10"
						  value="<%=sdf.format (new Date())%>" />
					      <a href="javascript:modulo = 'Registro';MuestraCalendario();"><img SRC="/gifs/EnlaceMig/gbo25410.gif" style=" border: 0;" width="12" height="14" alt="Calendario"/></a>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
          	<td>

          	</td>
          </tr>
          <tr>
          	<td align="center">
          		<table>
		    		<tr>
		    			<td>&nbsp;</td>
		    			<td>
		    				<a href="javascript:consultarLC();"><img style=" border: 0;" name="consultarLC" src="/gifs/EnlaceMig/gbo25220.gif" alt="Consultar" /></a>
		    			</td>
		    			<td><%--a href="javascript:regresarConsulta();"><img border="0" name="inicio" src="/gifs/EnlaceMig/gbo25320.gif" alt="Regresar"--%></td>
		    		</tr>
		    	</table>
          	</td>
          </tr>
        </table>

      </td>
    </tr>

</table>
  </form>

</BODY>
</HTML>
<%
    request.removeAttribute("fechaLib");
    request.removeAttribute("fechaLim");
    request.removeAttribute("Mensaje");
%>