<%@page contentType="text/html"%>
<%-- EN SEVERO ESTADO DE PRUEBA, RAFAEL MARTINEZ  --%>
<%@ page import="java.util.ArrayList" %>
<%@ page  import="EnlaceMig.pdBeneficiario" %>
<%@ page  import="mx.altec.enlace.bo.pdConsultaFiltroB"%>
<jsp:useBean id="Filtro" class="mx.altec.enlace.bo.pdConsultaFiltroB" scope="session" />

<%-- Lee un bean de request.getAttribute("pdConsultaFiltroB")
	de este obtiene:
		la fecha maxima de la consulta,
		las cuentas del contrato,
		los beneficiarios
	debe hacer:
		<td>
		checkbox para
			Registrados
			Pendientes
			Cancelados
			Liquidados
			Vencidos
			Modificados
			.Inicialmente no esta seleccionado ninguno de ellos, y si se hace submit
				sin seleccionar ninguno se tomara como si estuvieran todos seleccionados
		<td>
		<select> con
				<option> para cada cuenta
		textbox para fecha inicial, en focus, despliega una ventana con un calendario...
		textbox para fecha final, funciona igual que inicial, pero no puede ser mayor a
			pdConsultaFiltro.getFechaFin()
		textbox para importe
		textbox para pago inicial
		textbox para pago final
		<select> con
			<option value='pdConsultaFiltro.getABeneficiarios().index( X ).sID' >
				pdConsultaFiltro.getABeneficiarios().index( X ).sNombre
				para cada beneficiario

		Aceptar, cancelar( browser.history.back() )


--%>
<html>
<head><title>Filtro de Consulta - Pago Directo</title>
<meta http-equiv="Content-Type" content="text/html">
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language='javascript'>
<!--
var respuesta = 0;
var campoTexto = "";

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function js_ValidaFecha( txtDeFecha, txtAFecha){
	lib_year         = parseInt(txtDeFecha.value.substring(6,10),10);
	lib_month        = parseInt(txtDeFecha.value.substring(3, 5),10);
	lib_date         = parseInt(txtDeFecha.value.substring(0, 2),10);
	DeFecha = new Date(lib_year, lib_month, lib_date);

	lim_year     = parseInt(txtAFecha.value.substring(6,10),10);
	lim_month    = parseInt(txtAFecha.value.substring(3, 5),10);
	lim_date     = parseInt(txtAFecha.value.substring(0, 2),10);
	AFecha  = new Date(lim_year, lim_month, lim_date);

     if( DeFecha > AFecha){
        alert("La Fecha inicial del cheque no puede ser mayor a la Fecha Final.");
        return false;
     }
  return true;
}

function js_seleccionaFecha(ind){
	var m=new Date();
	n=m.getMonth();
	Indice=ind;
//verificar
	if (Indice == 0){
		msg=window.open("/EnlaceMig/EI_Calendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
		msg.focus();
	}
	if(Indice==1){
		msg=window.open("/EnlaceMig/chesCalendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
	    msg.focus();
	}
 }
function js_validaForma(frm){
	if(ValidaTodo(forma,evalua,errores)){
			frm.opdcOpcion.value=1;
			frm.submit();
    }
}

function despliegaEstatus()
 {
   ventanaInfo1=window.open('','trainerWindow','width=460,height=230,toolbar=no,scrollbars=yes');
   ventanaInfo1.document.open();
   ventanaInfo1.document.write("<html>");
   ventanaInfo1.document.writeln("<head>\n<title>Estatus</title>\n");

   ventanaInfo1.document.writeln("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
   ventanaInfo1.document.writeln("<script language='javascript' src='/EnlaceMig/scrImpresion.js'></script>");
   ventanaInfo1.document.writeln("</head>");
   ventanaInfo1.document.writeln("<body bgcolor='white'>");
   ventanaInfo1.document.writeln("<form>");
   ventanaInfo1.document.writeln("<table border=0 width=420 class='textabdatcla' align=center>");
   ventanaInfo1.document.writeln("<tr><th class='tittabdat'>Informaci&oacute;n de la Operaci&oacute;n</th></tr>");
   ventanaInfo1.document.writeln("<tr><td class='tabmovtex1' align=center>");

   ventanaInfo1.document.writeln("<table border=0 align=center><tr><td class='tabmovtex1' align=center>");
   ventanaInfo1.document.writeln("<img src='" + document.ChesPrincipal.imgResumen.value + "'></td>");
   ventanaInfo1.document.writeln("<td class='tabmovtex1'><br>" + document.ChesPrincipal.resumenArchivo.value  + "<br><br>");
   ventanaInfo1.document.writeln("</td></tr></table>");

   ventanaInfo1.document.writeln("</td></tr>");
   ventanaInfo1.document.writeln("<tr><td class='tabmovtex1'>");
   ventanaInfo1.document.writeln(document.ChesPrincipal.listaErrores.value+"<br><br>");
   ventanaInfo1.document.writeln("</td></tr>");
   ventanaInfo1.document.writeln("</table>");
   ventanaInfo1.document.writeln("<table border=0 align=center >");
   ventanaInfo1.document.writeln("<tr><td align=center><br><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
   ventanaInfo1.document.writeln("</table>");
   ventanaInfo1.document.writeln("</form>");
   ventanaInfo1.document.writeln("</body>\n</html>");
   ventanaInfo1.focus();
}
function js_imprimeSelectCuentas(){

	var Ctas = {
				<% String [] ctas = Filtro.getArrCuentas();
					for(int i=0;i< ctas.length ;++i)
						{%>"<%= ctas[i] %>"<% if (i+1 < ctas.length){ %>,
							<%}
						}%>
			};
			var i = 0;
			for( i= 0; Ctas.length ;++i){
				document.writeln(" <option value='" + Ctas[i] + "'>" + Ctas[i] + "</option>");
			}
}

/*************************************************************************************/
/*
 6 N Requerido Numerico
 5 T Requerido Texto
 3 S Requerido Seleccion
 2 n Numerico
 1 t Texto
 0 i Ignorado
*/

 errores = new Array(	"Registrados",
						"Pendientes",
						"Cancelados",
						"Liquidados",
						"Vencidos",
						"Modificados",
						"Cuenta",
						"Fecha Inicial",
						"Fecha Limite",
						"Importe",
						"No. de pago Inicial",
						"No. de pago Final",
						"Beneficiario");
 evalua = "xxxxxxxxxxxxx";

function SeleccionaFecha(ind){
   var m=new Date();
   n=m.getMonth();
   Indice=ind;
	if (Indice == 0){
		msg=window.open("/EnlaceMig/EI_Calendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
	msg.focus();
	}
	if(Indice==1){
		msg=window.open("/EnlaceMig/chesCalendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
	    msg.focus();
	}
}

function Actualiza(){
   if(Indice==0)
     document.pdConsultaFiltro.txtDeFecha.value=Fecha[Indice];
   else
     document.pdConsultaFiltro.txtAFecha.value=Fecha[Indice];
}

-->
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

<%-- Inicio Encabezado y menu --%>
<%= request.getAttribute("newMenu") %>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif'); <%= request.getAttribute("despliegaEstatus" ) %>"
background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
       <%= request.getAttribute("MenuPrincipal") %></TD>
  </TR>
</TABLE>

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="676" valign="top" align="left">
      <table width="666" border="0" cellspacing="6" cellpadding="0">
        <tr>
          <td width="528" valign="top" class="titpag"><%= request.getAttribute("Encabezado") %></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<%-- Fin Encabezado y menu --%>
<table width="760" border="0" cellspacing="0" cellpadding="0">
<form  name="pdConsultaFiltro" onSubmit="return js_validaForma(this);"  method="post" action="pdConsultas">
<tr>
    <td>
	<table><tr><td class="tabmovtex" nowrap valign="middle">Registrados<input type='checkbox' name='bRegistrados'  >
		</td></tr>
		<tr><td class="tabmovtex" nowrap valign="middle">Pendientes<input type='checkbox' name='bPendientes'  >
        </td></tr>
		<tr><td class="tabmovtex" nowrap valign="middle">Cancelados<input type='checkbox' name='bCancelados'  >
        </td></tr>
		<tr><td class="tabmovtex" nowrap valign="middle">Liquidados<input type='checkbox' name='bLiquidados'  >
        </td></tr>
		<tr><td class="tabmovtex" nowrap valign="middle">Vencidos<input type='checkbox' name='bVencidos'  >
        </td></tr>
		<tr><td class="tabmovtex" nowrap valign="middle">Modificados<input type='checkbox' name='bModificados'  >
		</td></tr>
	</table>
    </td>
<!-- Checar Cuentas -->
    <td><table>
<tr><td class="tabmovtex" nowrap valign="middle">Cuenta<select name='sCuenta'>Cuentas
			<script language='javascript'>
				<!--
					js_imprimeSelectCuentas();
				-->
			</script>
		</select>
		</td></tr>
		<tr><td class="tabmovtex" nowrap valign="middle">
			Fecha Inicial<input type="text" name="txtDeFecha" size="22" class="tabmovtex" value="<%= Filtro.getFechaFin() %>" size=10 onFocus='blur();' maxlength=10><a href="javascript:SeleccionaFecha(0);">
			<img border="0" name="imageField" src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" alt="Calendario"></a>
		</td></tr>
		<tr><td class="tabmovtex" nowrap valign="middle">
			Fecha Final<input type="text" name="txtAFecha" size="22" class="tabmovtex" value="<%= Filtro.getFechaFin() %>" size=10 onFocus='blur();' maxlength=10><a href="javascript:SeleccionaFecha(1);">
			<img border="0" name="imageField" src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" alt="Calendario"></a>
		</td></tr>
		<tr><td class="tabmovtex" nowrap valign="middle">
			Importe
		</td></tr>
		<tr><td class="tabmovtex" nowrap valign="middle">
			No. de pago Inicial
		</td></tr>
		<tr><td class="tabmovtex" nowrap valign="middle">
			No. de pago Final
		</td></tr>
		<tr><td class="tabmovtex" nowrap valign="middle">
			Beneficiario
		</td></tr>

	</table></td>
</tr>
<tr>
	<table><tr>
		<td align="right" valign="middle" width="78">
			<A href = "javascript:js_envio();" border=0><img src="/gifs/EnlaceMig/gbo25520.gif" border=0 alt="Aceptar" width="78" height="22"></a>
			<A href = "javascript:js_importar();" border = 0><img src="/gifs/EnlaceMig/gbo25280.gif" border=0 alt="Aceptar" width="80" height="22"></a>
			<a href="javascript:ValidaForma(document.frmConsulta);">
			<img border="0" name="Aceptar" src="/gifs/EnlaceMig/gbo25220.gif" width="90" height="22" alt="Consultar"></a>
		</td></tr>></table>
</tr>
	<input type="hidden" name="pdcOpcion" value=1>
</form>
</table>

</body>
</html>
