<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="mx.altec.enlace.utilerias.EIGlobal" %>
<%@page import="mx.altec.enlace.beans.CuentaODD4Bean"%>
<%@page import="mx.altec.enlace.utilerias.Global"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Descarga de Estados de Cuenta PDF</title>

<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript">

<%
  EIGlobal.mensajePorTrace(" >>>>>>>> Iniciando JSP",EIGlobal.NivelLog.DEBUG);

  if(request.getAttribute("TiempoTerminado")!=null) out.print(request.getAttribute("TiempoTerminado"));
  String codigoError = request.getAttribute("ERROR") != null ? request.getAttribute("ERROR").toString() : "";
  String mensajeError = request.getAttribute("MENSAJEERROR") != null ? request.getAttribute("MENSAJEERROR").toString() : "";
%>

/************************************************************************************/
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
/********************************************************************************/
<%
       if (request.getAttribute("newMenu")!= null) {
       out.println(request.getAttribute("newMenu"));
       }
%>


//<!-- modificacion para integracion pva 07/03/2002 -->
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

var periodos = '';
var periodosAgregados = '';
var resultadoWS;

var ventana;

var nombreMeses = {
	'ENE' : 'Enero',
	'FEB' : 'Febrero',
	'MAR' : 'Marzo',
	'ABR' : 'Abril',
	'MAY' : 'Mayo',
	'JUN' : 'Junio',
	'JUL' : 'Julio',
	'AGO' : 'Agosto',
	'SEP' : 'Septiembre',
	'OCT' : 'Octubre',
	'NOV' : 'Noviembre',
	'DIC' : 'Diciembre'};

var numeroMeses = {
	'ENE' : '01',
	'FEB' : '02',
	'MAR' : '03',
	'ABR' : '04',
	'MAY' : '05',
	'JUN' : '06',
	'JUL' : '07',
	'AGO' : '08',
	'SEP' : '09',
	'OCT' : '10',
	'NOV' : '11',
	'DIC' : '12'};

function myRowObject(one, two){
	this.one = one; // input checkbox
}

function trim(valorInput) {
	return valorInput.replace(/^\s+|\s+$/g,"");
}

function ValidaPeriodo() {
	if (!validaPreviaADescarga()) {
		return;
	}
	document.Datos.action = "DescargaEdoCtaPDFServlet?ventana=2";
	var slcPeriodos=document.getElementById('slcPeriodos');	
	
	if(slcPeriodos.length ==0){
	alert("No existen periodos disponibles para la cuenta seleccionada");
	return;
	}
	
	
	var slcPeriodosValor = slcPeriodos[slcPeriodos.selectedIndex].value;
	var periodoSeleccionado = slcPeriodos[slcPeriodos.selectedIndex].text;
	var folioOD=slcPeriodosValor.substring(0,8);
	var tipoedc=slcPeriodosValor.substring(9,12);
	var periodoNumero=slcPeriodosValor.substring(13);
	var cuentaSeleccionada = document.getElementById("cuentaTXT").value;
	var codigoCliente = document.getElementById('codigoCliente').value;
	var folioOnDemand = slcPeriodos[slcPeriodos.selectedIndex].value;
	var mesSeleccionado = periodoSeleccionado.substring(0, 3);
	var anioSeleccionado = periodoSeleccionado.substr(periodoSeleccionado.length - 4);
	var anioMes = '[' + anioSeleccionado + '' +  numeroMeses[mesSeleccionado] + '@' + folioOnDemand + ']';

	if ( slcPeriodosValor.indexOf('00000000@000')!=-1) {
		cuadroDialogo("No se gener&oacute; Estado de Cuenta para el periodo seleccionado.",4);
	} else if(periodosAgregados.indexOf(anioMes) >= 0){
			cuadroDialogo('El periodo ya ha sido agregado', 4);
		} else {
				var uri="DescargaEdoCtaPDFServlet?ventana=5&cadena="+periodoSeleccionado+"@"+folioOD+"@"+tipoedc+"@"+cuentaSeleccionada+"@"+codigoCliente+"@"+periodoNumero;
				funcionAjax(uri);
			};
}

function funcionAjax(uri) {
	var webService = "<%=Global.validaExistenciaPDFWsdlLocation%>";
	var xmlhttp;
	//Obtener el objeto xmlhttp dependiendo del explorador.
	if (window.XMLHttpRequest) {
	  xmlhttp=new XMLHttpRequest();
	} else {
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	//Esta funcion se ejecutara al obtener la respuesta de Ajax
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			var regreso=xmlhttp.responseText;
			var codError = regreso.substring(0,3);
			if (codError == '301') {
				MensajeErrorOD();
			}
			else if (codError == '100') {
				IniciaDescarga();
			} else {
				cuadroDialogo("Su transacci&oacute;n no puede ser atendida, intente m&aacute;s tarde", 4);
			}
		}
	}
	//Generar la peticion Ajax
	xmlhttp.open("GET",uri,true);
	xmlhttp.send();
}

function MensajeErrorOD(){
	cuadroDialogo("Estimado Cliente. El periodo solicitado no se encuentra disponible para su descarga. "+
				  "Es necesario que sea solicitado desde el men&uacute; \"Estados de Cuenta &gt; "+
				  "Periodos Hist&oacute;ricos Formato PDF &gt; Solicitud\" de Enlace y esperar a que el "+
				  "estatus cambie a Disponible.",4);
}

function IniciaDescarga(){
	var slcPeriodos=document.getElementById('slcPeriodos');
	var slcPeriodosValor = slcPeriodos[slcPeriodos.selectedIndex].value;
	var periodoSeleccionado = slcPeriodos[slcPeriodos.selectedIndex].text;
	var folioOD=slcPeriodosValor.substring(0,8);
	var tipoedc=slcPeriodosValor.substring(9,12);
	var periodoNumero=slcPeriodosValor.substring(13);
	var cuentaSeleccionada = document.getElementById("cambioCuentaTXT").value;
	var codigoCliente = document.getElementById('codigoCliente').value;
	var folioOnDemand = slcPeriodos[slcPeriodos.selectedIndex].value;
	var mesSeleccionado = periodoSeleccionado.substring(0, 3);
	var anioSeleccionado = periodoSeleccionado.substr(periodoSeleccionado.length - 4);

	var cadenaValores=document.getElementById('cadenaValores');
	cadenaValores.value=folioOD+"@"+tipoedc+"@"+cuentaSeleccionada+"@"+codigoCliente+"@"+
			folioOnDemand+"@"+mesSeleccionado+"@"+anioSeleccionado+"@"+periodoNumero;
	document.Datos.submit();
}

function descargarCuentas(){
	document.Datos.action = document.Datos.action.replace('ventana=2', 'ventana=3');
	document.Datos.action = document.Datos.action.replace('ventana=4', 'ventana=3');
	document.Datos.submit();
}

function CambiarCuenta() {
	document.getElementById("cambioCuentaTXT").value = trim(document.getElementById("cambioCuentaTXT").value);
	var valorOriginal = "<%=request.getAttribute("cambioCuentaTXT")%>";
	var valorNuevo = document.getElementById("cambioCuentaTXT").value;
	var cuentasRelacionadas = obtieneCuentasRelacionadas();

	if(trim(valorNuevo) == null || trim(valorNuevo) == "") {
		return;
	} else if (trim(valorOriginal) != trim(valorNuevo)) {
		for (var i = 0; i < cuentasRelacionadas.length; i++) {
			if (trim(valorNuevo) == cuentasRelacionadas[i]) {
				document.Datos.action = document.Datos.action.replace('ventana=2', 'ventana=4');
				document.Datos.action = document.Datos.action.replace('ventana=3', 'ventana=4');
				document.Datos.submit();
				return;
			}
		}
		return;
	}
}

function validaPreviaADescarga() {
	var valorOriginal = "<%=request.getAttribute("cambioCuentaTXT")%>";
	var valorNuevo = document.getElementById("cambioCuentaTXT").value;
	var cuentasRelacionadas = obtieneCuentasRelacionadas();

	if(trim(valorNuevo) == null || trim(valorNuevo) == "") {
		cuadroDialogo('Por favor, introduzca una cuenta para proceder con la descarga del Estado de Cuenta.', 4);
		return false;
	} else if (trim(valorOriginal) != trim(valorNuevo)) {
		for (var i = 0; i < cuentasRelacionadas.length; i++) {
			if (trim(valorNuevo) == cuentasRelacionadas[i]) {
				document.Datos.action = document.Datos.action.replace('ventana=2', 'ventana=4');
				document.Datos.action = document.Datos.action.replace('ventana=3', 'ventana=4');
				return true;
			}
		}
		cuadroDialogo("El n\u00famero de cuenta debe formar parte de las cuentas " +
		"relacionadas a la secuencia de domicilio.\n Para consultar estas " +
		"cuentas de clic en el link del n\u00famero de secuencia de la pantalla." , 4);
		return false;
	} else {
		return true;
	}
}

function obtieneCuentasRelacionadas() {
	var cuentasRelacionadas = new Array();
	<%
	List<CuentaODD4Bean> cuentasRelacionadas = (List<CuentaODD4Bean>) request.getSession().getAttribute("cuentasRelacionadas");
	String cuentaTmp = null;
	String cuentaFormato = null;
	int i = 0;
	for (CuentaODD4Bean obj : cuentasRelacionadas) {
		cuentaTmp = obj.getNumeroCuenta().trim();
		if (cuentaTmp.indexOf('B') == 0 && cuentaTmp.indexOf("BME") != 0) {
			cuentaFormato = "BME".concat(cuentaTmp.substring(1, cuentaTmp.length()));
		} else if (cuentaTmp.startsWith("0")){
			cuentaFormato = cuentaTmp.substring(1, cuentaTmp.length());
		} else {
			cuentaFormato = cuentaTmp;
		}
		%>
		cuentasRelacionadas[<%=i%>] = "<%=cuentaFormato.trim()%>";
		<%
		i++;
	}
	%>
	return cuentasRelacionadas;
}

function inicio() {
	var error = "<%=codigoError%>";
	var mensajeError = "<%=mensajeError%>";
	if(error == "ERRORIN01") {
		cuadroDialogo("El n&uacute;mero de cuenta debe formar parte de las cuentas relacionadas a la secuencia " +
					"de domicilio. Para consultar estas cuentas de clic en el link del n&uacute;mero de secuencia de la pantalla",4);
	} else if(error == "ERRORIN00") {
		cuadroDialogo(mensajeError,4);
	}
}

function Regresar() {
	document.Datos.action = "DescargaEdoCtaPDFServlet?ventana=0";
	document.Datos.submit();
}

	function noenter(e) {
	    e = e || window.event;
	    var key = e.keyCode || e.charCode;
	    return key !== 13; 
           }

</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
 
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor="#ffffff" leftMargin="0" topMargin="0" marginwidth="0" marginheight="0" onLoad="inicio();<%if ( request.getAttribute("URLEdoCtaPDF")!= null ) {out.print("modificaBanderaCarga()");}%>">
 
<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<%
           if (request.getAttribute("MenuPrincipal")!= null) {
           out.println(request.getAttribute("MenuPrincipal"));
           }
    %>
   </td>
  </tr>
</table>

<%
           if (request.getAttribute("Encabezado")!= null) {
           out.println(request.getAttribute("Encabezado"));
           }
%>

<form  name="Datos" method="post" action="DescargaEdoCtaPDFServlet?ventana=2">
	<p/>
	<table width="760" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center">
				<table align="center">
					<input type="hidden" id="cuentaSeleccionada" name="cuentaSeleccionada" value="<%=request.getAttribute("cuentaSeleccionda")%>"/>
					<input type="hidden" id="cuentaTXT" name="cuentaTXT" value="<%=request.getAttribute("cambioCuentaTXT")%>"/>
					<input type="hidden" id="codigoCliente" name="codigoCliente" value="<%=request.getAttribute("codigoCliente")%>"/>
					<input type="hidden" id="numeroSecuencia" name="numeroSecuencia" value="<%=request.getAttribute("numeroSecuencia")%>"/>
					<input type="hidden" id="cuentasTarjetas" name="cuentasTarjetas" value="<%=request.getAttribute("cuentaFormatoOriginal")%>"/>
					<input type="hidden" id="periodosSoliciar" name="periodosSoliciar" value=""/>
					<input type="hidden" id="cadenaValores" name="cadenaValores" value=""/>
					<input type="text" id="idBanderaCarga" name="banderaCarga" value="0" style="display: none"/>

					<tr class="tabdatfonazu">
						<th class="tittabdat"> Secuencia </th>
						<th class="tittabdat"> Cuenta (s) </th>
						<th class="tittabdat"> Titular de la Cuenta </th>
						<th class="tittabdat"> Domicilio Registrado </th>
					</tr>
				  	<tr class="textabdatobs">
				   		<td class="tabmovtex">
				   			<a href="#"
				   				title="Descargue las cuentas relacionadas a la secuencia de domicilio."
				   					onclick="javascript:descargarCuentas();">
				   				<%=request.getAttribute("numeroSecuencia")%>
				   			</a>
						</td>
						<td class="tabmovtex">
							<%=request.getAttribute("cuentaSeleccionda")%>
						</td>
						<td class="tabmovtex">
							<%=request.getAttribute("titularCuenta")%>
						</td>
						<td class="tabmovtex">
							<%=request.getAttribute("domicilioRegistrado")%>
						</td>
				  	</tr>
				</table>
				<table>
					<tr>
						<td colspan="4" class="texencconbol">
						<%=request.getAttribute("MENSAJE_PROBLEMA_DATOS") != null ? request.getAttribute("MENSAJE_PROBLEMA_DATOS") : "" %>
						</td>
					</tr>
				</table>
				<br/>
				<table>
					<tr class="tabdatfonazu">
						<th class="tittabdat">Cambio de cuenta. </th>
					</tr>
					<tr>
						<td><input type="text" size="20" name="cambioCuentaTXT" id="cambioCuentaTXT" value="<%=request.getAttribute("cambioCuentaTXT")%>" onblur="CambiarCuenta()" onkeypress="return noenter(event)"/></td>
					</tr>
				</table>
				<br/>
				<table align="center">
					<tr class="tabdatfonazu">
						<th class="tittabdat"> Solicitud de Descarga de Estados de Cuenta. </th>
					</tr>
					<tr align="center" class="tabmovtex">
				   		<td class="textabdatcla" style="background-color:white;">
							Periodo:<select id="slcPeriodos" name="slcPeriodos">
							<%
								EIGlobal.mensajePorTrace("Listando periodosRecientes",EIGlobal.NivelLog.DEBUG);
								List periodosRecientes = (List)request.getAttribute("periodosRecientes");
						        for(int contadorPeriodos = 0; contadorPeriodos < periodosRecientes.size(); contadorPeriodos++){
						        	String periodoTx = (String)periodosRecientes.get(contadorPeriodos);
						            if(periodoTx == null  || periodoTx.length() == 0){
						                continue;
						            }

									String[] periodoTxSeparado = periodoTx.split(":::");

									if(periodoTxSeparado.length != 2){
										continue;
									}

									if (!periodoTxSeparado[1].contains("00000000")) {
			    			%>
							<option value="<%=periodoTxSeparado[1]%>"><%=periodoTxSeparado[0]%></option>
							<%
									}
								}
								EIGlobal.mensajePorTrace("Fin Lista periodosRecientes",EIGlobal.NivelLog.DEBUG);
							 %>
							</select>

							<a href="javascript:ValidaPeriodo();">
								<img alt="El tiempo que tomar&aacute; la descarga del archivo depender&aacute; de su velocidad de conexi&oacute;n." src="/gifs/EnlaceMig/Descarga.gif" border="0"/>
							</a>
						</td>
				  	</tr>
				  	<tr><td>&nbsp;</td></tr>
				  	<tr><td align="center">
							<a href="javascript:Regresar();"><img src="/gifs/EnlaceMig/gbo25320.gif" alt="Regresar" border="0"/></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
<div id="descarga" style="display:none;">
	<%
	  String URLEdoCtaPDF = (request.getAttribute("URLEdoCtaPDF")!= null) ? (String)request.getAttribute("URLEdoCtaPDF") : "";
	  out.println("<input type='hidden' name='URLEdoCtaPDF' value='"+URLEdoCtaPDF+"' />");
	  if ( URLEdoCtaPDF.trim().length()>0 ) {
			EIGlobal.mensajePorTrace("URLEdoCtaPDF contiene la url ["+URLEdoCtaPDF+"]",EIGlobal.NivelLog.DEBUG);
			out.print("<script>"+
						"function modificaBanderaCarga() {"+
							"var banderaCarga = document.getElementById(\"idBanderaCarga\");"+
							"if (banderaCarga.value == \"0\") {"+
								"msg=window.open(\""+URLEdoCtaPDF+"\",\"\",\"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=400,height=200\");"+
								"msg.focus();"+
							"}"+
							"document.getElementById(\"idBanderaCarga\").value = \"1\";"+
						"}"+
						"</script>"+
						"");
	  }
	%>
</div>
	</table>
</form>

</body>
</html>