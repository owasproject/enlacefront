<%@ page import="java.util.*" %>
<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>

<%!

//
String formateaAmoneda(String str)
	{
	return "$"+str;
	}
//
String colocaPunto(String num)
	{
	if(num.length()<2) return num;
	return num.substring(0,num.length()-2) + "." + num.substring(num.length()-2);
	}

//
String moneda(String num)
	{
	return formateaAmoneda(colocaPunto(num.trim()));
	}

%>

<html>

<head>
	<title>Banca Virtual</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

	<!-- ----------------------------- Secci�n de JavaScript ---------------------------- -->
	<script language = "JavaScript" src="/EnlaceMig/fw_menu.js"></script>
	<script language = "JavaScript" src="/EnlaceMig/cuadroDialogo.js"></script>
	<script language = "JavaScript" SRC= "/EnlaceMig/scrImpresion.js"></script>

	<script language = "Javascript" >

	// --- Secci�n para el men� - (comienza) --------------------------------------------------
	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_findObj(n, d) { //v3.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	<%= request.getAttribute("newMenu") %>
	// --- Secci�n para el men� - (termina) --------------------------------------------------

	// --- esto se ejecuta cuando se carga la p�gina
	function inicia()
		{
		PutDate();
		MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif',
				'/gifs/EnlaceMig/gbo25111.gif',
				'/gifs/EnlaceMig/gbo25151.gif',
				'/gifs/EnlaceMig/gbo25031.gif',
				'/gifs/EnlaceMig/gbo25032.gif',
				'/gifs/EnlaceMig/gbo25051.gif',
				'/gifs/EnlaceMig/gbo25052.gif',
				'/gifs/EnlaceMig/gbo25091.gif',
				'/gifs/EnlaceMig/gbo25092.gif',
				'/gifs/EnlaceMig/gbo25012.gif',
				'/gifs/EnlaceMig/gbo25071.gif',
				'/gifs/EnlaceMig/gbo25072.gif',
				'/gifs/EnlaceMig/gbo25011.gif');
		}

	</script>

	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>


<!-- SECCION PRINCIPAL ----------------------------------------------------------------->

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="inicia" background="/gifs/EnlaceMig/gfo25010.gif">
 <Input type="hidden" name="Redivisa" value ="<%= request.getAttribute("Redivisa") %>">
	<!-- MENU PRINCIPAL Y ENCABEZADO -->
	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top"><td width="*"><%= request.getAttribute("MenuPrincipal") %></td></tr>
	</table>
	<%= request.getAttribute("Encabezado") %>

	<br><br>

	<% String cuentaCargo = request.getAttribute("cuentaCargo")!=null?(String)request.getAttribute("cuentaCargo"):"";
		if(cuentaCargo==null || cuentaCargo.length()<=0)
			cuentaCargo = "Todas";
	 %>
	<table cellspacing="20"><tr><td>

	<table cellpadding='2' cellspacing='1'>
	<tr><td  class = 'texenccon'><%= (String)request.getAttribute("Sel") %></td></tr>
	</table>
	<%
		String estilo = "textabdatobs";
		String linea;
		Vector lineas = new Vector();
		StringTokenizer info = new StringTokenizer((String)request.getAttribute("Info"),"\n");

		// Se guardan las l�neas en un vector
		try
			{while(info.hasMoreTokens()) lineas.add(info.nextToken());}
		catch(Exception e)
			{EIGlobal.mensajePorTrace("Excepci�n en coEstadisticasCon.jsp: " + e.getMessage(), EIGlobal.NivelLog.ERROR);}

		// Se ordenan las l�neas
		int total = lineas.size(), a, b;
		String lin1, lin2;
		for(a=0;a<total-1;a++)
			for(b=a+1;b<total;b++)
		{
		lin1 = (String)lineas.get(a);
		lin2 = (String)lineas.get(b);
		if (lin1.substring(20).compareTo(lin2.substring(20))>0)
			{
			lineas.set(a,lin2);
			lineas.set(b,lin1);
			}
		}

		ArrayList listaMXN = new ArrayList();
		ArrayList listaUSD = new ArrayList();
		for(a=0;a<total;a++){
			String[] datos = new String[20];
			linea = (String)lineas.get(a);
			datos[0] = "coEstadisticas?tipoReq=2&Proveedor=" + linea.substring(0,20).trim() +
						"&fecha1=" + request.getAttribute("fecha1") + "&fecha2=" + request.getAttribute("fecha2");
			linea = linea.substring(20);
			datos[1] = linea.substring(0,20);
			datos[2] = linea.substring(20,100);
			datos[3] = linea.substring(100,108);
			datos[4] = linea.substring(108,129);
			linea = linea.substring(129);
			datos[5] = linea.substring(0,8);
			datos[6] = linea.substring(8,29);
			linea = linea.substring(29);
			datos[7] = linea.substring(0,8);
			datos[8] = linea.substring(8,29);
			linea = linea.substring(29);
			datos[9] = linea.substring(0,8);
			datos[10] = linea.substring(8,29);
			linea = linea.substring(29);
			datos[11] = linea.substring(0,8);
			datos[12] = linea.substring(8,29);
			linea = linea.substring(29);
			datos[13] = linea.substring(0,8);
			datos[14] = linea.substring(8,29);
			linea = linea.substring(29);
			datos[15] = linea.substring(0,8);
			datos[16] = linea.substring(8,29);
			linea = linea.substring(29);
			datos[17] = linea.substring(0,8);
			datos[18] = linea.substring(8,29);
			linea = linea.substring(29);
			datos[19] = linea;
			if(linea.equalsIgnoreCase("MXN"))
				listaMXN.add(datos);
			else if(linea.equalsIgnoreCase("USD"))
					listaUSD.add(datos);
		}
	%>
	<div align='center'>
	<%
	String url = "";
	if(listaMXN.size()>0){ %>
	<table cellpadding='4' cellspacing='1'>
	<tr>
		<td colspan="18">
			<table align="left" cellpadding='2' cellspacing='1'>
				<tr><td  class = 'texenccon'>REPORTE DE ESTAD&Iacute;STICAS EN MONEDA NACIONAL</td></tr>
				<tr><td  class = 'texenccon'>Cuenta Cargo: <%= listaMXN.size()==0?"No existen registros para mostrar":cuentaCargo %></td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class='tittabdat'>Clave prov.</td>
		<td class='tittabdat'>Nombre o raz&oacute;n social</td>
		<td class='tittabdat'>No.</td>
		<td class='tittabdat'>Recibidos</td>
		<td class='tittabdat'>No.</td>
		<td class='tittabdat'>Por pagar</td>
		<td class='tittabdat'>No.</td>
		<td class='tittabdat'>Pagados</td>
		<td class='tittabdat'>No.</td>
		<td class='tittabdat'>Liberados no pagados</td>
		<td class='tittabdat'>No.</td>
		<td class='tittabdat'>Anticipados</td>
		<td class='tittabdat'>No.</td>
		<td class='tittabdat'>Cancelados</td>
		<td class='tittabdat'>No.</td>
		<td class='tittabdat'>Vencidos</td>
		<td class='tittabdat'>No.</td>
		<td class='tittabdat'>Rechazados</td>

		<!-- td class='tittabdat'>Divisa</td-->
	</tr>
	<%
		// Se imprimen las l�neas en Pesos
		for(a=0;a<listaMXN.size();a++)
			{
			String[] datos = (String[])listaMXN.get(a);
			//linea = (String)lineas.get(a);
			estilo = (estilo.equals("textabdatobs"))?"textabdatcla":"textabdatobs";
			url = datos[0]; /*"coEstadisticas?tipoReq=2&Proveedor=" + linea.substring(0,20).trim() +
		"&fecha1=" + request.getAttribute("fecha1") + "&fecha2=" + request.getAttribute("fecha2");*/
			//linea = linea.substring(20);
	%>
		<tr>
			<td class='<%= estilo %>' style="text-align:center;" align="center"><%= datos[1] %></td>
			<td class='<%= estilo %>'><!--a href="<%= url %>"--><%= datos[2] %><!--/a--></td>

			<td class='<%= estilo %>' style="text-align:center;" align="center"><%= datos[3] %></td>
			<td class='<%= estilo %>' align="right"><%= moneda(datos[4]) %></td>
			<% //linea = linea.substring(129); %>

			<td class='<%= estilo %>' style="text-align:center;" align="center"><%= datos[5] %></td>
			<td class='<%= estilo %>' align="right"><%= moneda(datos[6]) %></td>
			<% //linea = linea.substring(29); %>

			<td class='<%= estilo %>' style="text-align:center;" align="center"><%= datos[7] %></td>
			<td class='<%= estilo %>' align="right"><%= moneda(datos[8]) %></td>
			<% //linea = linea.substring(29); %>

			<td class='<%= estilo %>' style="text-align:center;" align="center"><%= datos[9] %></td>
			<td class='<%= estilo %>' align="right"><%= moneda(datos[10]) %></td>
			<% //linea = linea.substring(29); %>

			<td class='<%= estilo %>' style="text-align:center;" align="center"><%= datos[11] %></td>
			<td class='<%= estilo %>' align="right"><%= moneda(datos[12]) %></td>
			<% //linea = linea.substring(29); %>

			<td class='<%= estilo %>' style="text-align:center;" align="center"><%= datos[13] %></td>
			<td class='<%= estilo %>' align="right"><%= moneda(datos[14]) %></td>
			<% //linea = linea.substring(29); %>

			<td class='<%= estilo %>' style="text-align:center;" align="center"><%= datos[15] %></td>
			<td class='<%= estilo %>' align="right"><%= moneda(datos[16]) %></td>
			<% //linea = linea.substring(29); %>

			<td class='<%= estilo %>' style="text-align:center;" align="center"><%= datos[17] %></td>
			<td class='<%= estilo %>' align="right"><%= moneda(datos[18]) %></td>
			<% //linea = linea.substring(29); %>

		</tr>
		<%
		}
		if(listaMXN.size()==0){
	%>
		<tr>
			<td class='<%= estilo %>' align="left" colspan="18">No existen registros para mostrar</td>
		</tr>
	<% } %>
	</table>
	<br>
	<%} %>

	<%if(listaUSD.size()>0){ %>
	<table cellpadding='4' cellspacing='1'>
	<tr>
		<td colspan="18">
			<table align="left" cellpadding='2' cellspacing='1'>
				<tr><td  class = 'texenccon'>REPORTE DE ESTAD&Iacute;STICAS EN D&Oacute;LAR AMERICANO</td></tr>
				<tr><td  class = 'texenccon'>Cuenta Cargo: <%= listaUSD.size()==0?"No existen registros para mostrar":cuentaCargo %></td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class='tittabdat'>Clave prov.</td>
		<td class='tittabdat'>Nombre o raz&oacute;n social</td>
		<td class='tittabdat'>No.</td>
		<td class='tittabdat'>Recibidos</td>
		<td class='tittabdat'>No.</td>
		<td class='tittabdat'>Por pagar</td>
		<td class='tittabdat'>No.</td>
		<td class='tittabdat'>Pagados</td>
		<td class='tittabdat'>No.</td>
		<td class='tittabdat'>Liberados no pagados</td>
		<td class='tittabdat'>No.</td>
		<td class='tittabdat'>Anticipados</td>
		<td class='tittabdat'>No.</td>
		<td class='tittabdat'>Cancelados</td>
		<td class='tittabdat'>No.</td>
		<td class='tittabdat'>Vencidos</td>
		<td class='tittabdat'>No.</td>
		<td class='tittabdat'>Rechazados</td>
	</tr>
	<%
		estilo = "textabdatobs";

		// Se imprimen las l�neas en Dolares
		for(a=0;a<listaUSD.size();a++)
			{
			String[] datos = (String[])listaUSD.get(a);
			estilo = (estilo.equals("textabdatobs"))?"textabdatcla":"textabdatobs";
			url = datos[0];
	%>
		<tr>
			<td class='<%= estilo %>' style="text-align:center;" align="center"><%= datos[1] %></td>
			<td class='<%= estilo %>'><!--a href="<%= url %>"--><%= datos[2] %><!--/a--></td>

			<td class='<%= estilo %>' style="text-align:center;" align="center"><%= datos[3] %></td>
			<td class='<%= estilo %>' align="right"><%= moneda(datos[4]) %></td>

			<td class='<%= estilo %>' style="text-align:center;" align="center"><%= datos[5] %></td>
			<td class='<%= estilo %>' align="right"><%= moneda(datos[6]) %></td>

			<td class='<%= estilo %>' style="text-align:center;" align="center"><%= datos[7] %></td>
			<td class='<%= estilo %>' align="right"><%= moneda(datos[8]) %></td>

			<td class='<%= estilo %>' style="text-align:center;" align="center"><%= datos[9] %></td>
			<td class='<%= estilo %>' align="right"><%= moneda(datos[10]) %></td>

			<td class='<%= estilo %>' style="text-align:center;" align="center"><%= datos[11] %></td>
			<td class='<%= estilo %>' align="right"><%= moneda(datos[12]) %></td>

			<td class='<%= estilo %>' style="text-align:center;" align="center"><%= datos[13] %></td>
			<td class='<%= estilo %>' align="right"><%= moneda(datos[14]) %></td>

			<td class='<%= estilo %>' style="text-align:center;" align="center"><%= datos[15] %></td>
			<td class='<%= estilo %>' align="right"><%= moneda(datos[16]) %></td>

			<td class='<%= estilo %>' style="text-align:center;" align="center"><%= datos[17] %></td>
			<td class='<%= estilo %>' align="right"><%= moneda(datos[18]) %></td>
		</tr>
		<%
		}
		if(listaUSD.size()==0){
	%>
		<tr>
			<td class='<%= estilo %>' align="left" colspan="18">No existen registros para mostrar</td>
		</tr>
	<% } %>
	</table>
	<br>
	<%} %>
	<a href="<%= (String)request.getAttribute("Exportacion") %>"><img
	src='/gifs/EnlaceMig/gbo25230.gif' border='0' alt='Exportar'></A><a
	href='javascript:scrImpresion();'><img src='/gifs/EnlaceMig/gbo25240.gif' border='0' alt='Imprimir'></A><a
	href ='coEstadisticas'><img src='/gifs/EnlaceMig/gbo25320.gif' border='0' alt='Regresar'></a>
	</div>
	</td></tr></table>
</body>
</html>