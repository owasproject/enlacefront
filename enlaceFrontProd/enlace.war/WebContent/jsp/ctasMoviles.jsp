<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.*" %>
<%@page import="mx.altec.enlace.beans.CuentaMovilBean"%>
<jsp:useBean id="ctasList" class="java.util.ArrayList" scope="request" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
 <head>
  <title>Enlace Banco Santander Mexicano</title>

  <script type="text/javascript"  src="/EnlaceMig/cuadroDialogo.js"></script>
  <script type="text/javascript"  src="/EnlaceMig/fw_menu.js"></script>
  <script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
  <script type="text/javascript">

    function exportar(){
	    document.FrmConCtaMoviles.action="CtasMoviles";
		document.FrmConCtaMoviles.opcion.value="2";
		document.FrmConCtaMoviles.submit();
    }
</script>
<script type="text/javascript">
	function anteriores()
    {
    	var pag = document.FrmConCtaMoviles.numPag.value;
    	pag--;
		document.FrmConCtaMoviles.action="CtasMoviles";
		document.FrmConCtaMoviles.opcion.value="1";
		document.FrmConCtaMoviles.numPag.value = pag;
		document.FrmConCtaMoviles.submit();
	}
</script>
<script type="text/javascript">
	function siguientes()
    {
    	var pag = document.FrmConCtaMoviles.numPag.value;
    	pag++;
		document.FrmConCtaMoviles.action="CtasMoviles";
		document.FrmConCtaMoviles.opcion.value="1";
		document.FrmConCtaMoviles.numPag.value = pag;
		document.FrmConCtaMoviles.submit();
	}
</script>
<script type="text/javascript">
    function MM_preloadImages()
    {   //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }
</script>
<script type="text/javascript">
    function MM_swapImgRestore()
    {   //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }
</script>
<script type="text/javascript">
    function MM_findObj(n, d)
    { //v3.0
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
        d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
    }
</script>
<script type="text/javascript">
    function MM_swapImage()
    {   //v3.0
        var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }
<%
    if( request.getAttribute("newMenu")!=null )
        out.println(request.getAttribute("newMenu"));
%>
  </script>
  <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
 </head>
 <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" style="background='/gifs/EnlaceMig/gfo25010.gif'; background-color='#ffffff'">
  <table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
   <tr valign="">
    <td width="">
<%
    if( request.getAttribute("MenuPrincipal")!=null )
        out.println(request.getAttribute("MenuPrincipal"));
%>
    </td>
   </tr>
  </table>
<%
    if( request.getAttribute("Encabezado")!=null )
        out.println(request.getAttribute("Encabezado"));
%>
  <table width="760" border="0" cellspacing="0" cellpadding="0">
   <form name = "Frmfechas">
<%
   // if( request.getAttribute("Movfechas")!=null )
   //     out.println(request.getAttribute("Movfechas"));
%>
   </form>
  <form name="FrmConCtaMoviles" method="post">
  <input type="hidden" name="opcion" value=""/>
  <input type="hidden" name="numPag" value="<%=request.getAttribute("numPag")%>"/>
  <input type="hidden" name="totalPaginas" value="<%=request.getAttribute("totalPaginas")%>"/>
   <tr>
    <td align="center">
     <table width="500" border="0" cellspacing="2" cellpadding="3">
      <tr>
       <td class="tittabdat"  align="center">N&uacute;mero M&oacute;vil</td>
       <td class="tittabdat"  align="center" >Nombre del titular</td>
       <td class="tittabdat"  align="center">Tipo de cuenta</td>
       <td class="tittabdat"  align="center">Banco</td>
      </tr>

      <%

      	if(ctasList != null && ctasList.size()>0){

      	String estilo ="";
      	Integer pagIni = (Integer)request.getAttribute("numPag");
      	Integer pagFin = (Integer)request.getAttribute("totalPaginas");

      		for(int i=0;i<ctasList.size();i++){
      			CuentaMovilBean result = (CuentaMovilBean)ctasList.get(i);

      			if(i%2==0){
      				estilo = "textabdatobs";
       %>
			      <tr style="background-color='#CCCCCC'">
      <%		}else{
      				estilo = "textabdatcla";
      %>
			      <tr style="background-color='#EBEBEB'">
      <%		} %>

      	<td class="<%=estilo %>" align="center"><%=result.getNumeroMovil()%></td>
      	<td class="<%=estilo %>" align="center"><%=result.getTitular()%></td>
      	<td class="<%=estilo %>" align="center"><%=result.getTipoCuenta()%></td>
      	<td class="<%=estilo %>" align="center"><%=result.getDescBanco()%></td>
      </tr>

      <%
      		}

       %>

     </table>
    </td>
   </tr>
   <tr>
    <td align="center">
   		<table border="0" cellspacing="0" cellpadding="0">
	 		<tr>

   				<%if(pagIni.intValue() >1){%>

	  				<td><a href="javascript:anteriores();">Anteriores</a>&nbsp;</td>
	  			<%}

	  			if(pagFin.intValue()!= pagIni.intValue()){
	  			%>
	  				<td>&nbsp;<a href="javascript:siguientes();">Siguientes</a></td>

	  			<%} %>
	 		</tr>
		</table>
	</td>
   </tr>
   <tr>
   	<td>
   		&nbsp;
   	</td>
   </tr>
   <tr>
    <td align="center">
    	<table border="0" cellspacing="0" cellpadding="0">
	 		<tr>
	  			<td><a href = "javascript:exportar();"><img src = "/gifs/EnlaceMig/gbo25230.gif" style="border='0'" alt="Exportar"/></a></td>
	  			<td><a href = "javascript:scrImpresion();"><img src = "/gifs/EnlaceMig/gbo25240.gif" style="border='0'" alt="Imprimir"/></a></td>
	  			<td><a href = "ContCtas_AL"><img src = "/gifs/EnlaceMig/gbo25320.gif" style="border='0'" alt="Regresar"/></a></td>
	 		</tr>
		</table>
	</td>
   </tr>
  </form>
  </table>
  <%	} %>
 </body>
	<head>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	</head>
</html>