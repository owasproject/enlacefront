<html>
<head>
	<title>Transferencia Internacionales</TITLE>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javaScript" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript">

<%= request.getAttribute("mensajeError") %>
/*Error en la importacion del archivo ....*/
/*************************************************************************************/

/*
   M Mail Requerido
   E Requerido Texto y Numeros (a-z, A-Z, 0-9)
 6 N Requerido Numerico
 5 T Requerido Texto
 3 S Requerido Seleccion
 2 n Numerico
 1 t Texto
 0 i Ignorado
   e Texto y Numeros (a-z, A-Z, 0-9)
   m Mail
*/

 var MAX_REG=<%= request.getAttribute("NumeroRegistros") %>;
 var nav4 = window.Event ? true : false;

 errores=new Array(
				   "Ventanilla", //1
				   "Enlace",     //2
				   "TipoCambioDolar", //3
                   "radio",      //4
                   " la CUENTA ORDENANTE", //5
                   " la CUENTA DEL BENEFICIARIO",//6
                   " Hidden ", //7
				   " el CONCEPTO", //8
                   " el IMPORTE",  //9
				   " el IMPORTE DIVISA", //10
				   " el IMPORTE EN DOLLS", //11
				   "radio", //12
				   "radio", //13
				   " la CLAVE", //14
				   " el RFC",   //15
				   " el TIPO DE CAMBIO", //16
				   " el IMPORTE IVA",   //17
                   "fechA",   //18
                   "radiO",   //19
                   " la CUENTA ORDENANTE", //20
				   " la CUENTA DEL BENEFICIARIO", //21
				   " la CLAVE",  //22
				   " la DIVISA", //23
				   " el PAIS",   //24
				   " la CLAVE ABA", //25
				   "la CIUDAD cve", //26
				   " la CIUDAD",    //27
				   "el BANCO cve",  //28
				   " el BANCO",     //29
                   " el BENEFICIARIO", //30
                   " el CONCEPTO",    //31
				   " el IMPORTE",
				   " el IMPORTE EN DOLLS",
                   " el IMPORTE DIVISA",
				   "radio",
				   "radio",
				   " la CLAVE",
				   " el RFC",
				   " el TIPO DE CAMBIO",
				   " el IMPORTE IVA",
                   "Fecha",
                   "Radio",
                   "-Archivo",
                   "Modulo",
                   "TransReg",
                   "TransNoReg",
                   "TransArch",
                   "-Agregar");

evalua=new Array("XXXXtTteNNNxxtennXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                 "XXXXXXXXXXXXXXXXXXXtEtTSntGtGEeNNNXXtennXXXXXXXXXXXXXXX",
                 "XXXXXXXXXXXXXXXxXxXXXxxxxXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
				 "XXXXXXXXXXXXXXXXXXXtEtTSntGtGEeNNNXXtennXXXXXXXXXXXXXXX");

function isDigit(c)
{
  var res=true;
  var cont=0;

  for(i=0;i<c.length;i++)
   {
     if(! ( (c.charAt(i) >= "0") && (c.charAt(i) <= "9") ) )
	  {
        if(c.charAt(i)==".")
         {
           if(cont>=1)
             res=false;
           cont++;
         }
		else
		 res=false;
	  }
   }
  return res;
}

/*<input type=Hidden name=CargoReg VALUE="1|82500129090|Descripcion 2|" > */
function cambiaImpMN(cmn, cdiv, cusd)
 {
   forma=document.transferencia;

   var divusd=forma.txtEnlace2.value;
   var divmn=forma.txtEnlace.value;
   var imp=cmn.value;
   var di=forma.Directo_Inverso.value; /*Modif PVA 07/08/2002*/

   if(trimString(imp)!="")
    {
      if( isDigit(imp) )
       {
		 if(
		    (trimString(forma.montoMNreal.value)!="") &&
		    ( redondea(forma.montoMNreal.value)==imp)
		   )

		   {
		     /* No cambiar los valores */
			 /*
		     alert("No cambio.");
			 alert("Real: "+forma.montoMNreal.value);
			 alert("Real redondeado: "+redondea(forma.montoMNreal.value));
			 alert("Campo MN: "+cmn.value);
			 */
		   }
		  else
		   {
		     cdiv.value=redondea(imp/divmn);
   			 /*Modif PVA 07/08/2002*/
			 forma.montoDivisareal.value=imp/divmn;
             //if(di=="D")
             if(di=="I") /*Directo*/
		     {
			   cusd.value=redondea(cdiv.value/divusd);
   		       forma.montoUSDreal.value=(imp/divmn)/divusd;
			 }
			 else  /*Inverso*/
			 {
			   cusd.value=redondea(cdiv.value*divusd);
               forma.montoUSDreal.value=(imp/divmn)*divusd;
		     }
             /*///////// */
		   }
       }
      else
        cuadroDialogo("El IMPORTE no es correcto.",1);
    }
 }

function cambiaImpDIV(cdiv,cmn,cusd)
 {
   forma=document.transferencia;

   var divusd=forma.txtEnlace2.value;
   var divmn=forma.txtEnlace.value;
   var imp=cdiv.value;
   var di=forma.Directo_Inverso.value; /*Modif PVA 07/08/2002*/


   if(trimString(imp)!="")
    {
      if( isDigit(imp) )
       {


		 if(
		    (trimString(forma.montoDivisareal.value)!="") &&
		    ( redondea(forma.montoDivisareal.value)==imp)
		   )

		   {
		     /* No cambiar los valores */

		   }
		  else
		   {
		      /*Modif PVA 07/08/2002*/
            // if(di=="D")
             if(di=="I") /*Directo*/
		      {
			    cusd.value=redondea(imp/divusd);
				forma.montoUSDreal.value=imp/divusd;
		      }
			  else  /*Inverso*/
		      {
			    cusd.value=redondea(imp*divusd);
				forma.montoUSDreal.value=imp*divusd;
              }
              cmn.value=redondea(imp*divmn);
			  forma.montoMNreal.value=imp*divmn;
		  }

       }
      else
        cuadroDialogo("El IMPORTE no es correcto.",1);
    }
 }

function cambiaImpUSD(cusd,cmn,cdiv)
 {
   forma=document.transferencia;

   var divusd=forma.txtEnlace2.value;
   var divmn=forma.txtEnlace.value;
   var imp=cusd.value;
   var di=forma.Directo_Inverso.value; /*Modif PVA 07/08/2002*/


   if(trimString(imp)!="")
    {
      if( isDigit(imp) )
       {
	     if(
		    (trimString(forma.montoUSDreal.value)!="") &&
		    ( redondea(forma.montoUSDreal.value)==imp)
		   )

		   {
		     /* No cambiar los valores */
			 /*
		     alert("No cambio.");
			 alert("Real: "+forma.montoUSDreal.value);
			 alert("Real redondeado: "+redondea(forma.montoUSDreal.value));
			 alert("Campo USD: "+cmn.value);
			 */
		   }
		  else
		   {
     		 /*Modif PVA 07/08/2002*/
             //if(di=="D")
             if(di=="I")
			 {
			   cdiv.value=redondea(imp*divusd);
			   forma.montoDivisareal.value=imp*divusd;
		  	   cmn.value=redondea(cdiv.value*divmn);
   		       forma.montoMNreal.value=(imp*divusd*divmn);
             }
			 else
			 {
			   cdiv.value=redondea(imp/divusd);
			   forma.montoDivisareal.value=imp/divusd;
   		  	   cmn.value=redondea(cdiv.value*divmn);
   		       forma.montoMNreal.value=((imp/divusd)*divmn);
             }

		   }
       }
      else
        cuadroDialogo("El IMPORTE no es correcto.",1);
    }
 }

function redondea(num)
{
  var a=0;
  var b=0;

  /*alert("antes "+num);*/
  a=parseInt(Math.round(num*100));
  b=a/100;
  /*alert("despues "+b);*/
  return b;
}

function Consultar()
 {
   document.transferencia.Modulo.value=0;
   document.transferencia.action="MTI_Consultar?Modulo=0";
   document.transferencia.submit();
 }

var respuesta=0;

function continua()
{
  if(respuesta==1)
    document.tabla.submit();
}

/* Para cotizar transferir solo la transferencia actual*/
function CotizarTransferir()
 {
   if(ValidaForma(document.transferencia))
    {
      if(transEnLinea()<=MAX_REG)
        {
          document.tabla.TransReg.value=document.transferencia.TransReg.value;
          document.tabla.TransNoReg.value=document.transferencia.TransNoReg.value;
          document.tabla.strCheck.value="1";
          document.tabla.action="MTI_Cotizar";
          cuadroDialogo(" \"El tipo de cambio de su cotizaci&oacute;n puede cambiar si han transcurrido m&aacute;s de 3 minutos.\" ",2);
          /*document.tabla.submit();*/
        }
       else   //Modif PVA 02/05/2003
        cuadroDialogo("No puedes realizar m&aacute;s de "+ MAX_REG +" transferencias en l&iacute;nea.",1);
    }
 }

/* Para cotizar transferir despues de agregar las transferencias.*/
function Enviar()
 {
   if(ValidaTabla(document.tabla))
    {
      document.tabla.action="MTI_Cotizar";
	  cuadroDialogo(" \"El tipo de cambio de su cotizaci&oacute;n puede cambiar si han transcurrido m&aacute;s de 3 minutos.\" ",2);
    }
 }

function validaTipoCambio(){
   forma=document.transferencia;
   if(trimString(forma.tipoEspecial.value)==""){
   	 document.transferencia.ImporteReg.readOnly = false;
   	 document.transferencia.ImporteRegUSD.readOnly = false;
   }else{
     document.transferencia.ImporteReg.readOnly = true;
   	 document.transferencia.ImporteRegUSD.readOnly = true;
   }
}

function verificaClave(campo,clave,montoMN,MontoUSD,MontoDiv)
 {
   if(trimString(clave.value)=="")
     campo.blur();

 }

function verificaRFC(campo,radio)
 {
   if(radio[0].checked==true)
    campo.blur();
 }

function verificaPais()
 {
   forma=document.transferencia;
   if(forma.Pais.options[forma.Pais.selectedIndex].value=='USA ')
    {
	  forma.Descripcion.value="";
	  forma.Ciudad.value="0";
	  forma.DescripcionBanco.value="";
	  forma.Banco.value="0";
	}
   else
    {
	  forma.Descripcion.value="";
	  forma.Ciudad.value="";
	  forma.DescripcionBanco.value="";
	  forma.Banco.value="";
	  forma.ClaveABA.value="";
    }
 }

function verificaClaveABA()
 {
   forma=document.transferencia;
   /*Ahora solo se selecciona por catalogo ...*/
   if(forma.Pais.options[forma.Pais.selectedIndex].value!='USA ')
     forma.ClaveABA.blur();
 }

function verificaCiudad()
 {
   forma=document.transferencia;
   if(forma.Pais.options[forma.Pais.selectedIndex].value=='USA ')
     document.transferencia.Descripcion.blur();
 }

function verificaBanco()
 {
   forma=document.transferencia;
   if(forma.Pais.options[forma.Pais.selectedIndex].value=='USA ')
     document.transferencia.DescripcionBanco.blur();
 }

function Agregar()
 {
   MAX_REG=1;
   textToUpper(document.transferencia);
   if(ValidaForma(document.transferencia))
	{
	  if(transEnLinea()<=MAX_REG)
		{
		  document.transferencia.action="MTI_Transferencia";
		  document.transferencia.submit();
		}
	   else //Modif PVA 02/05/2003
		cuadroDialogo("No puedes realizar m&aacute;s de "+ MAX_REG +" transferencia en l&iacute;nea.",1);
	}
 }

function ValidaRFC(rfc)
 {
   var fecha=6;

   var formato="";

   switch(rfc.value.length)
	 {
	   case 13: formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9][0-9,A-Z,a-z][0-9,A-Z,a-z][0-9,A-Z,a-z]$/; break;
	   case 10: formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9]$/; break;
	   case 12: formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9][0-9,A-Z,a-z][0-9,A-Z,a-z][0-9,A-Z,a-z]$/; break;
	   case 9: formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9]$/; break;
    }
   if(formato=="")
	 return false;
   if(rfc.value.length==9 || rfc.value.length==12)
     fecha=5;
   if(!formato.test(rfc.value))
     return false;

   var mes=rfc.value.substring(fecha,fecha+2);
   var dia=rfc.value.substring(fecha+2,fecha+4);
   if( (dia >0 && dia<=31) && (mes >0 && mes<=12) )
    {
      if(mes==2 && dia >=30)
       return false;
	 }
	else
	 return false;
   return true;
 }

function SeleccionaDivisa()
 {
   var forma=document.transferencia;

   /*forma.CveDivisa.value=forma.Moneda.options[forma.Moneda.selectedIndex].value;*/
   forma.CveDivisa.value=forma.inicialCveDivisa.value;
   forma.Moneda.value=forma.inicialDesDivisa.value;
 }

function InsertaReg(forma)
  {
    var tot=0;
    var var1="";
	var tipoCuenta1="";
	var tipoCuenta2="";

	var impTmp=0;
	var longitud=0;
	var campo="";
	var di=forma.Directo_Inverso.value; /*Modif PVA 07/08/2002*/
	var carTmp=forma.AbonoReg.value;

    /*modificacion para validar el tipo y clave en transferencias a la par*/
	dcargo=forma.CargoReg.value.substring(forma.CargoReg.value.indexOf("|")+1,forma.CargoReg.value.length);
	tipoCuenta1=forma.CargoReg.value.substring(0,forma.CargoReg.value.indexOf("|"));

	/*"821331003000|beneficiario@Plaza!EUR $ANGO#Banco%"*/
	dabono=carTmp.substring(0,carTmp.indexOf('@'));
	dplaza=carTmp.substring(carTmp.indexOf('@')+1,carTmp.indexOf('!'));
	ddivisa=carTmp.substring(carTmp.indexOf('!')+1,carTmp.indexOf('$'));
	dpais=carTmp.substring(carTmp.indexOf('$')+1,carTmp.indexOf('#'));
	dbanco=carTmp.substring(carTmp.indexOf('#')+1,carTmp.indexOf('%'));
	dclaveABA=carTmp.substring(carTmp.indexOf('%')+1,carTmp.length);

	/********************************************************
	Siempre se va a enviar el importe de la divisa
	Verifica el formato del Importe 2 decimales solamente.*/
	/*
    if(trimString(tipoCuenta1)=="6")
	  campo=forma.ImporteRegUSD;
	else
	  campo=forma.ImporteReg;
	*/
	campo=forma.ImporteRegDivisa;

	impTmp=trimString(campo.value);

     if(impTmp.indexOf(".")<0)
	  impTmp+=".00";
	 longitud=impTmp.substring(impTmp.indexOf(".")+1,impTmp.length).length;
	 if(longitud>2)
	  {
	    campo.focus();
		cuadroDialogo("Solo se permiten 2 decimales en el Importe.",1);
		return false;
	  }
	 else
	  {
	    if(longitud==1)
		 impTmp+="0";
		if(longitud==0)
		 impTmp+="00";
	    campo.value=impTmp;
	  }

	/*alert("El importe antes es: "+campo.value);*/

	/*******************************************************/
	/*
	alert("cuenta: "+tipoCuenta1);
	alert("divisa:"+ddivisa);
	*/

	if(tipoCuenta1=="6")
     {
       if(trimString(ddivisa)=="USD" || trimString(ddivisa)=="DA")
        {
          forma.CveEspecial.value="";
          forma.tipoEspecial.value="";
        }
     }

	dclaveEspecial=forma.CveEspecial.value;
	dtipoEspecial=forma.tipoEspecial.value;


	if(trimString(dclaveEspecial)!="")
	 {
	  if(trimString(dtipoEspecial)=="")
	   {
	     forma.tipoEspecial.focus();
	     cuadroDialogo("Debe especificar el tipo de cambio especial para la clave.");
	     return false;
	   }
	  else
	   {
		   //verificar que el tipo de cambio especial es de 6 decimales
		    var tmp1=trimString(dtipoEspecial);
			var tmp2=0;
            if(dtipoEspecial.indexOf(".")<0)
              dtipoEspecial+=".000000";

            tmp2=dtipoEspecial.substring(dtipoEspecial.indexOf(".")+1,dtipoEspecial.length).length;
            if(tmp2>6)
             {
               forma.tipoEspecial.focus();
               cuadroDialogo("Solo se permiten 6 decimales en el Tipo de cambio.",1);
               return false;
             }
			else
             {
			   var ceros="0000000";
               dtipoEspecial+=ceros.substring(0,(6-tmp2));
               forma.tipoEspecial.value=dtipoEspecial;
             }
	   }
	 }
	else
	 {
	   if(trimString(dtipoEspecial)!="")
	    {
		  forma.CveEspecial.focus();
		  cuadroDialogo("Debe especificar la clave para tipo de cambio especial.");
		  return false;
		}
	 }

	/*Modif PVA 09/08/2002 */
	/*
     if((trimString(dclaveEspecial)!="")&&
	    (trimString(dtipoEspecial)!=""))
     {

        if ((trimString(tipoCuenta1)=="6")&&(di=="D"))
		  campo.value=eval(forma.ImporteRegDivisa.value)/eval(dtipoEspecial);
		else
		  campo.value=eval(dtipoEspecial)*eval(forma.ImporteRegDivisa.value);

		impTmp=trimString(campo.value);
        if(impTmp.indexOf(".")<0)
         impTmp+=".00";
        longitud=impTmp.substring(impTmp.indexOf(".")+1,impTmp.length).length;
        //if(longitud>2)
		// impTmp=impTmp.substring(0,impTmp.indexOf(".")+2);
		if(longitud==1)
         impTmp+="0";
        if(longitud==0)
         impTmp+="00";
        campo.value=impTmp;
		//alert("El importe se modifico en : "+campo.value);
	 }
	 */
    /*/////////////////*/

	dRFC=forma.RFCReg.value.toUpperCase();
	dImporteIva=forma.ImporteIVAReg.value;

	for(i1=0;i1<forma.length;i1++)
     {
       if(forma.elements[i1].type=='radio' && forma.elements[i1].name=='EdoFiscalReg')
	    if(forma.elements[i1].checked==true)
         if(forma.elements[i1].value==0)
		  {
		    dRFC="";
			dImporteIva="";
		  }
		 else
		  {
		    if(trimString(forma.RFCReg.value)=="")
		     {
		       forma.RFCReg.focus();
		       cuadroDialogo("El RFC es obligatorio.",3);
		       return false;
  		     }
		    if(trimString(forma.ImporteIVAReg.value)=="")
		     {
		       forma.ImporteIVAReg.focus();
		       cuadroDialogo("El IMPORTE IVA es obligatorio.",3);
		       return false;
		     }
 		    if(!ValidaRFC(forma.RFCReg))
		     {
		       forma.RFCReg.focus();
		       cuadroDialogo("el RFC no es v&aacute;lido.",1);
		       return false;
	   	     }

 			dImporteIva=trimString(dImporteIva);
            if(dImporteIva.indexOf(".")<0)
              dImporteIva+=".00";
            longitud=dImporteIva.substring(dImporteIva.indexOf(".")+1,dImporteIva.length).length;
            if(longitud>2)
             {
               forma.ImporteIVAReg.focus();
               cuadroDialogo("Solo se permiten 2 decimales en el Importe.",1);
               return false;
             }
            else
             {
               if(longitud==1)
                 dImporteIva+="0";
               if(longitud==0)
                 dImporteIva+="00";
               forma.ImporteIVAReg.value=dImporteIva;
             }
	      }
	  }

    dfecha=forma.FechaReg.value;
    dtipoCambio=forma.TCE_Venta.value;
    if(trimString(forma.ConceptoReg.value)=="")
      dconcepto="TRANSFERENCIA INTERNACIONAL"
    else
      dconcepto=forma.ConceptoReg.value;

    dimporte=campo.value;
	/*alert("El importe al final es: "+campo.value);*/

	if(trimString(dImporteIva)!="")
	 if(dImporteIva.indexOf('.')<0)
      dImporteIva+=".00";

    if(trimString(forma.TransReg.value)=="" || forma.TransReg.value=="null")
      var1="";
	var1=dcargo+dabono+"|"+dimporte+"|"+dconcepto+"|"+ddivisa+"|"+dtipoCambio+"|"+dpais+"|"+dclaveABA+"|"+dplaza+"|"+dbanco+"|"+dfecha+"|"+dRFC+"|"+dImporteIva+"|"+dclaveEspecial+"|"+dtipoEspecial+"@";
	if(forma.TransReg.value.indexOf(var1)>=0)
	   var1="";

	/*
	alert("Cargo: "+dcargo);
	alert("Abono: "+dabono);
	alert("Importe: "+dimporte);
	alert("Concepto: "+dconcepto);
	alert("tipoCuenta1: "+tipoCuenta1);
	alert("Divisa: "+ddivisa);
	alert("TipoCambio: "+dtipoCambio);
	alert("Pais: "+dpais);
	alert("Clave ABA: "+dclaveABA);
	alert("Plaza: "+dplaza);
	alert("Banco: "+dbanco);
	alert("Fecha: "+dfecha);
	alert("RFC: "+dRFC);
	alert("Importe IVA: "+dImporteIva);
	alert("Clave ESP: "+dclaveEspecial);
	alert("Tipo ESP: "+dtipoEspecial);
	alert(" Trama: "+ var1);
	*/

	/*alert("la clave especial: "+dclaveEspecial);*/

    forma.TransReg.value+=var1;
	return true;
  }

function InsertaNoReg(forma)
  {
    var tot2=0;
    var var2="";

	var impTmp=0;
	var longitud=0;
	var tipoCuenta1="";
	var campo="";
	var di=forma.Directo_Inverso.value; /*Modif PVA 07/08/2002*/

	if(trimString(forma.ClaveABA.value)!="")
	 if(trimString(forma.ClaveABA.value).length<=7)
	  {
	    forma.ClaveABA.focus();
	    cuadroDialogo("La clave ABA debe ser de 9 digitos.",3);
	    return false;
	  }
	dcargo=forma.CargoNoReg.value.substring(forma.CargoNoReg.value.indexOf("|")+1,forma.CargoNoReg.value.length);
	tipoCuenta1=forma.CargoNoReg.value.substring(0,forma.CargoNoReg.value.indexOf("|"));

    dabono=forma.AbonoNoReg.value;
    dbanco=forma.DescripcionBanco.value;
    dplaza=forma.Descripcion.value;
    dbeneficiario=forma.Beneficiario.value;

	dRFC=forma.RFC.value.toUpperCase();
	dImporteIva=forma.ImporteIVA.value;
	dtipoCambio=forma.TCE_Venta.value;
	dpais=forma.Pais.options[forma.Pais.selectedIndex].value;
	dclaveABA=forma.ClaveABA.value;
    ddivisa=forma.CveDivisa.value;

	/*******************************************************/
	/*
	Siempre se va a enviar el importe en divisa
	Verifica el formato del Importe 2 decimales solamente.
	*/
	/*
	if(trimString(tipoCuenta1)=="6")
	 campo=forma.ImporteNoRegUSD;
	else
	 campo=forma.ImporteNoReg;
	*/
	campo=forma.ImporteNoRegDivisa;

	impTmp=trimString(campo.value);

     if(impTmp.indexOf(".")<0)
	  impTmp+=".00";
	 longitud=impTmp.substring(impTmp.indexOf(".")+1,impTmp.length).length;
	 if(longitud>2)
	  {
	    campo.focus();
		cuadroDialogo("Solo se permiten 2 decimales en el Importe.",1);
		return false;
	  }
	 else
	  {
	    if(longitud==1)
		 impTmp+="0";
		if(longitud==0)
		 impTmp+="00";
	    campo.value=impTmp;
	  }
	/*******************************************************/

	dclaveEspecial=forma.CveEspecialNoReg.value;
	dtipoEspecial=forma.tipoEspecialNoReg.value;

	/*
	alert("cuenta: "+tipoCuenta1);
	alert("divisa:"+ddivisa);
	*/

	if(tipoCuenta1=="6")
     {
       if(trimString(ddivisa)=="USD" || trimString(ddivisa)=="DA")
        {
          forma.CveEspecialNoReg.value="";
          forma.tipoEspecialNoReg.value="";
        }
     }

	if(trimString(dclaveEspecial)!="")
	 {
	  if(trimString(dtipoEspecial)=="")
	   {
	     forma.tipoEspecialNoReg.focus();
	     cuadroDialogo("Debe especificar el tipo de cambio especial para la clave.");
	     return false;
	   }
	  else
	   {
		   //verificar que el tipo de cambio especial es de 6 decimales
		    var tmp1=trimString(dtipoEspecial);
			var tmp2=0;
            if(dtipoEspecial.indexOf(".")<0)
              dtipoEspecial+=".000000";

            tmp2=dtipoEspecial.substring(dtipoEspecial.indexOf(".")+1,dtipoEspecial.length).length;
            if(tmp2>6)
             {
               forma.tipoEspecialNoReg.focus();
               cuadroDialogo("Solo se permiten 6 decimales en el Tipo de cambio.",1);
               return false;
             }
			else
             {
			   var ceros="0000000";
               dtipoEspecial+=ceros.substring(0,(6-tmp2));
               forma.tipoEspecialNoReg.value=dtipoEspecial;
             }
	   }

	 }
	else
	 {
	   if(trimString(dtipoEspecial)!="")
	    {
		  forma.CveEspecialNoReg.focus();
		  cuadroDialogo("Debe especificar la clave para tipo de cambio especial.");
		  return false;
		}
	 }

     /*Modif PVA 09/08/2002 */
	 /*
     if((trimString(dclaveEspecial)!="")&&
	    (trimString(dtipoEspecial)!=""))
     {

        if ((trimString(tipoCuenta1)=="6")&&(di=="D"))
		  campo.value=eval(forma.ImporteNoRegDivisa.value)/eval(dtipoEspecial);
		else
	 	  campo.value=eval(dtipoEspecial)*eval(forma.ImporteNoRegDivisa.value);
		impTmp=trimString(campo.value);
        if(impTmp.indexOf(".")<0)
         impTmp+=".00";
        longitud=impTmp.substring(impTmp.indexOf(".")+1,impTmp.length).length;
       // if(longitud>2)
		// impTmp=impTmp.substring(0,impTmp.indexOf(".")+2);
		if(longitud==1)
         impTmp+="0";
        if(longitud==0)
         impTmp+="00";
        campo.value=impTmp;
	 }
	 */
    /*/////////////////*/

	for(i1=0;i1<forma.length;i1++)
     {
       if(forma.elements[i1].type=='radio' && forma.elements[i1].name=='EdoFiscal')
	    if(forma.elements[i1].checked==true)
         if(forma.elements[i1].value==0)
		  {
		    dRFC="";
			dImporteIva="";
		  }
		 else
		  {
		    if(trimString(forma.RFC.value)=="")
		     {
		       forma.RFC.focus();
		       cuadroDialogo("El RFC es obligatorio.",3);
			   return false;
  		     }
		    if(trimString(forma.ImporteIVA.value)=="")
		     {
		       forma.ImporteIVA.focus();
		       cuadroDialogo("El IMPORTE IVA es obligatorio.",3);
			   return false;
		     }
			if(!ValidaRFC(forma.RFC))
			 {
			   forma.RFC.focus();
	           cuadroDialogo("el RFC no es v�lido.",1);
			   return false;
			 }
			dImporteIva=trimString(dImporteIva);
            if(dImporteIva.indexOf(".")<0)
              dImporteIva+=".00";
            longitud=dImporteIva.substring(dImporteIva.indexOf(".")+1,dImporteIva.length).length;
            if(longitud>2)
             {
               forma.ImporteIVA.focus();
               cuadroDialogo("Solo se permiten 2 decimales en el Importe.",1);
               return false;
             }
            else
             {
               if(longitud==1)
                 dImporteIva+="0";
               if(longitud==0)
                 dImporteIva+="00";
               forma.ImporteIVA.value=dImporteIva;
             }
	      }
	  }

    dfecha=forma.FechaNoReg.value;
	if(trimString(forma.ConceptoNoReg.value)=="")
      dconcepto="TRANSFERENCIA INTERNACIONAL";
    else
      dconcepto=forma.ConceptoNoReg.value;

    dimporte=campo.value;
	if(trimString(dImporteIva)!="")
	 if(dImporteIva.indexOf('.')<0)
      dImporteIva+=".00";

	/*alert("la clave especial: "+dclaveEspecial);*/

    if(trimString(forma.TransNoReg.value)=="" || forma.TransNoReg.value=="null")
      var2="";
    var2=dcargo+dabono+"|"+dbeneficiario+"|"+dimporte+"|"+dconcepto+"|"+ddivisa+"|"+dtipoCambio+"|"+dpais+"|"+dclaveABA+"|"+dplaza+"|"+dbanco+"|"+dfecha+"|"+dRFC+" |"+dImporteIva+"|"+dclaveEspecial+"|"+dtipoEspecial+"@";
	if(forma.TransNoReg.value.indexOf(var2)>=0)
	   var2="";
    forma.TransNoReg.value+=var2;
	return true;
  }

function ValidaTabla(forma)
 {
   var cont=0;
   var str="";

   if(forma.TransArch.value.length<=1)
    {
	  for(i2=0;i2<forma.length;i2++)
	   {
	     if(forma.elements[i2].type=='checkbox')
		  {
		    if(forma.elements[i2].checked==true)
			 {
			   str+="1";
			   cont++;
			 }
			else
			  str+="0";
		  }
	   }
	  if(cont==0)
	   {
	     /*alert("Debe Seleccionar m�nimo una transferencia.");*/
		 cuadroDialogo("Debe Seleccionar m�nimo una transferencia.",3);
		 return false;
	   }
	  forma.strCheck.value=str;
	}
   return true;
 }

function ValidaClaveABA(forma,ind)
 {
   if(indice==1)
    {
	  if(trimString(forma.ClaveABA.value)=="")
	   {
	     if(!ValidaTodo(forma,evalua[ind],errores))
		   return false;
	   }
	  else
	   if(!ValidaTodo(forma,evalua[3],errores))
	     return false;
	}
   else
    if(!ValidaTodo(forma,evalua[ind],errores))
	  return false;
   return true;
 }

function ValidaForma(forma)
 {
   for(i1=0;i1<forma.length;i1++)
    {
     if(forma.elements[i1].type=='radio' && forma.elements[i1].name!='EdoFiscal' && forma.elements[i1].name!='EdoFiscalReg')
      if(forma.elements[i1].checked==true)
       {
        indice=forma.elements[i1].value;
		if(indice==1)
		 {
		   if(forma.AbonoNoReg.value.length>20)
			{
			  /*alert("La CUENTA DE ABONO no es v�lida.");*/
			  forma.AbonoNoReg.focus();
			  cuadroDialogo("La CUENTA DE ABONO no es v&aacute;lida.",3);
			  return false;
			}
		 }
		 
		 if (dirBenef == 'true'){//INDRA-SWIFT-P022574-ini
		 	if(!validaDatosBenefi(forma)){
			  return false;
		  }
		 }//INDRA-SWIFT-P022574-ini
		 
         if(ValidaClaveABA(forma,forma.elements[i1].value,indice))
          {
           if(indice==2)
            {
			  if(nav4)
              if(trimString(forma.Archivo.value)=="")
               {
 		         forma.Archivo.focus();
				 /*alert("Debe especificar nombre de Archivo.");*/
				 cuadroDialogo("Debe especificar nombre de Archivo.",3);
                 return false;
			   }
            }

           if(indice==0)
            {
              /*
			  modificacion para integracion
			  //s1=forma.CargoReg.options[forma.CargoReg.selectedIndex].value;
              //s2=forma.AbonoReg.options[forma.AbonoReg.selectedIndex].value;
			  */
			  s1=forma.CargoReg.value;
              s2=forma.AbonoReg.value;
              if(s1==s2)
               {
                 /*alert("La cuenta de Cargo no puede ser igual\na la cuenta de Abono.");*/
				 forma.AbonoReg.focus();
				 cuadroDialogo("La cuenta de Cargo no puede ser igual\na la cuenta de Abono.",3);
                 return false;
               }
              if(forma.Modulo.value=='1')
               if(!InsertaReg(forma))
			    return false;
            }

           if(indice==1)
            {
			  if(forma.Modulo.value=='1')
			   if(!InsertaNoReg(forma))
			     return false;
		    }

           return true;
         }
        return false;
       }
	   if (forma.elements.length >= 23){ //MSD - Q05-0024447
		if (document.transferencia.Registradas.value != 0 && document.transferencia.Registradas[1].checked == true){

			if (forma.elements[23].value == "USA "){
				if (forma.elements[28].value == ""){
					cuadroDialogo("Debe seleccionar Banco", 3);
					return false;
				}
				if (forma.elements[24].value == ""){
					cuadroDialogo("Debe seleccionar Clave ABA", 3);
					return false;
				}
			}
			else{
				if (forma.elements[28].value == ""){
					cuadroDialogo("Debe seleccionar Banco", 3);
					return false;
				}
			}
		}
	   }//--

	}
   return false;
 }

 // Validacion de los datos complementarios de la cuenta beneficiaria
 // INDRA-SWIFT-P022574-ini 
 function validaDatosBenefi(forma)
 {
	domicilioBenef=trimString(forma.calleBenef.value);
	ciudadBenef=trimString(forma.ciudadBenef.value);
	noIdBenef=trimString(forma.idBenef.value);
	
	paisBenef=trimString(forma.paisBIBenef.options[forma.paisBIBenef.selectedIndex].value);
	   
   if(paisBenef==0)
	{
	  forma.paisBIBenef.focus();
	  cuadroDialogo("Debe seleccionar un Pa&iacute;s para el beneficiario",1);
	  return false;
	}
	
	if(domicilioBenef == "")
	{
		forma.calleBenef.focus();
		cuadroDialogo("El Domicilio del beneficiario es obligatorio.",1);
		return false;
	}
	if(!especialesBI(forma.calleBenef,"el Domicilio")){
		  return false;
	}
		  
	if(ciudadBenef == "")
	{
		forma.ciudadBenef.focus();
		cuadroDialogo("La Ciudad del beneficiario es obligatorio.",1);
		return false;
	}
	if(!especialesBI(forma.ciudadBenef,"la ciudad del beneficiario")){
		  return false;
	}
	
	if(noIdBenef == "")
	{
		forma.idBenef.focus();
		cuadroDialogo("El N&uacute;mero de Identificaci&oacute;n del beneficiario es obligatorio.",1);
		return false;
	}
	if(!especialesBI(forma.idBenef,"el N&uacute;mero de Identificaci&oacute;n")){
		  return false;
	}
	
	return true;
 }//INDRA-SWIFT-P022574-fin
 
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function especialesBI(Txt1,campo)
{
   var strEspecial=" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789,";
   var cont=0;

    for(i0=0;i0<Txt1.value.length;i0++)
	 {
       Txt2 = Txt1.value.charAt(i0);
	   if( strEspecial.indexOf(Txt2)!=-1 )
		 cont++;
     }
	if(cont!=i0)
	 {
	   Txt1.focus();
	   cuadroDialogo("No se permiten caracteres especiales para " + campo,3);
	   return false;
	 }
   return true;
}
 
function VerificaFac(_val)
{
  if(_val==2)
   {
     if(document.transferencia.TransNoReg.value.length>1 || document.transferencia.TransReg.value.length>1)
	  {
	    /*alert("Solo puedes agregar transferencias en l�nea.");*/
		cuadroDialogo("Solo puedes agregar transferencias en l&iacute;nea.",1);
		document.transferencia.Registradas[0].checked=true;
	  }
   }
  else
   {
     if(document.transferencia.TransArch.value.length>1)
	  {
	    /*alert("Solo puedes agregar transferencias dese archivo.");*/
		cuadroDialogo("Solo puedes agregar transferencias dese archivo.",1);
		document.transferencia.Registradas[2].checked=true;
	  }
   }
}

function archivoErrores()
 {
   SeleccionaDivisa();
   /*
   ventanaInfo1=window.open('/Download/Errores.html','trainerWindow','width=450,height=300,toolbar=no,scrollbars=yes');
   ventanaInfo1.focus();
   */
 }

//Modif PVA 02/05/2003
function quitar_arroba(banco_verificar)
{
  var vqa="";
  for(i0=0;i0<banco_verificar.length;i0++)
  {
     if(banco_verificar.charAt(i0)!='@')
       vqa+=banco_verificar.charAt(i0);
  }
  return vqa;
}


function Selecciona(S)
 {
   var v1="";
   var v2="";
   var v3="";

   if(S==2)
	{
	  if(ventanaInfo2.document.Forma.SelBanco.selectedIndex == -1) { //MSD Q05-24447
	  	window.alert("Debe seleccionar un Banco");
		ventanaInfo2.focus();
		return;
	  }

	  ventanaInfo2.close();
	  v1=ventanaInfo2.document.Forma.SelBanco.options[ventanaInfo2.document.Forma.SelBanco.selectedIndex].value;
	  v2=ventanaInfo2.document.Forma.SelBanco.options[ventanaInfo2.document.Forma.SelBanco.selectedIndex].text;
	  if(v1)
	    document.transferencia.Banco.value=v1;
	  if(v2)
	    document.transferencia.DescripcionBanco.value=v2;


	  //Inicia Modif PVA 02/05/2003
	  document.transferencia.Banco.value=quitar_arroba(document.transferencia.Banco.value);
	  document.transferencia.DescripcionBanco.value=quitar_arroba(document.transferencia.DescripcionBanco.value);
	  document.transferencia.Ciudad.value="";
	  document.transferencia.Descripcion.value="";
	  document.transferencia.ClaveABA.value="";
      //fin Modif PVA 02/05/2003

	}
  if(S==3)
	{
	  if(ventanaInfo2.document.Forma.SelCiudad.selectedIndex == -1) { //MSD Q05-24447
	      window.alert("Debe seleccionar una Ciudad");
	      return;
	  }

	  v1=ventanaInfo2.document.Forma.SelCiudad.options[ventanaInfo2.document.Forma.SelCiudad.selectedIndex].value;
	  v2=ventanaInfo2.document.Forma.SelCiudad.options[ventanaInfo2.document.Forma.SelCiudad.selectedIndex].text;

	  if(v1)
	    document.transferencia.Ciudad.value=v1;
	  if(v2)
	    document.transferencia.Descripcion.value=v2;
	}
  if(S==4)
   {
      if(ventanaInfo2.document.Forma.SelClave.selectedIndex == -1) { //MSD Q05-24447
	  	window.alert("Debe seleccionar una Clave ABA");
		ventanaInfo2.focus();
		return;
      }
	  else {
	  	ventanaInfo2.close();
	  }

      v1=ventanaInfo2.document.Forma.SelClave.options[ventanaInfo2.document.Forma.SelClave.selectedIndex].value;
	  v2=ventanaInfo2.document.Forma.SelClave.options[ventanaInfo2.document.Forma.SelClave.selectedIndex].text;

	  if(v1)
	    document.transferencia.Ciudad.value=v1;
	  if(v2)
	   {
	     document.transferencia.Descripcion.value=trimString(v2.substring(v2.indexOf("-")+1,v2.length));
		 document.transferencia.ClaveABA.value=trimString(v2.substring(0,v2.indexOf("-")));
	   }
    }
   if(S==5)
    {
      v1=ventanaInfo2.document.Forma.CiudadABA.value;
      v2=ventanaInfo2.document.Forma.BancoABA.value;
	  v3=ventanaInfo2.document.Forma.EstadoABA.value;

	  if(v1 && v2 && v3)
	   {
	     document.transferencia.Descripcion.value=v1+", "+v3;
		 document.transferencia.Ciudad.value="1";
		 document.transferencia.DescripcionBanco.value=v2;
		 document.transferencia.Banco.value="1";
	   }
	  else
	   {
	     document.transferencia.Descripcion.value="";
		 document.transferencia.Ciudad.value="";
		 document.transferencia.DescripcionBanco.value="";
		 document.transferencia.Banco.value="";
	   }
    }
 }

function formatea(para)
 {
   var formateado="";
   var car="";

   for(a2=0;a2<para.length;a2++)
    {
      if(para.charAt(a2)==' ')
       car="+";
      else
       car=para.charAt(a2);

      formateado+=car;
    }
   return formateado;
 }

function TraerClavesABA()
 {
   forma=document.transferencia;
   if(forma.Pais.options[forma.Pais.selectedIndex].value=='USA ')
    {
      if(trimString(document.transferencia.Banco.value)!="" &&
         trimString(document.transferencia.DescripcionBanco.value)!="")
       {
         var Banco=document.transferencia.DescripcionBanco.value;
         Banco=formatea(Banco);
         ventanaInfo2=window.open('EI_Bancos?cerrar=1&BancoTxt='+Banco+"&Clave=1",'Bancos','width=250,height=350,toolbar=no,scrollbars=no');
         /*ventanaInfo2=window.open('/Download/EI_Claves.html','Bancos','width=280,height=350,toolbar=no,scrollbars=no');*/
         ventanaInfo2.focus();
       }
      else
       cuadroDialogo("Seleccione un Banco.",1)
    }
   else
     cuadroDialogo("Solo aplica a Estados Unidos.",1)
 }

function TraerDatosDeClave(clave)
{
   forma=document.transferencia;

   clave=trimString(clave);
   if(clave=="")
     return ;
   if(isDigit(clave) && clave.length>7)
    {
      if(forma.Pais.options[forma.Pais.selectedIndex].value=='USA ')
       {
         ventanaInfo2=window.open('EI_Bancos?BancoTxt=ABA&Clave='+trimString(clave),'Claves','width=300,height=180,toolbar=no,scrollbars=no');
         ventanaInfo2.focus();
       }
	}
   else
    {
      forma.ClaveABA.focus();
	  cuadroDialogo("La clave ABA debe ser de 9 d&iacute;gitos y num&eacute;rica.",3);
	}
}

function TraerBancos()
 {
   forma=document.transferencia;
   if(forma.Pais.options[forma.Pais.selectedIndex].value=='USA ')
    {
	  ventanaInfo2=window.open('EI_Bancos?cerrar=1','Bancos','width=280,height=350,toolbar=no,scrollbars=no');
	  ventanaInfo2=window.open('/Download/EI_Bancos.html','Bancos','width=250,height=350,toolbar=no,scrollbars=no');
	  ventanaInfo2.focus();
	  /*
	  //Modif PVA 08/08/2002
      //cuadroDialogo("Debe Seleccionar la clave ABA.",1)
	  */
    }
   else
     cuadroDialogo("Solo aplica a Estados Unidos.",1)
 }

function TraerCiudades()
 {
   forma=document.transferencia;
   if(forma.Pais.options[forma.Pais.selectedIndex].value=='USA ')
    {
	  ventanaInfo2=window.open('EI_Ciudades','Ciudades','width=250,height=350,toolbar=no,scrollbars=no');
	  ventanaInfo2=window.open('/Download/EI_Ciudades.html','Ciudades','width=250,height=350,toolbar=no,scrollbars=no');
	  ventanaInfo2.focus();
	}
   else
	 cuadroDialogo("Solo aplica a Estados Unidos.",1)
 }

/**************************************************************************/
/**************************************************************************/
/**************************************************************************/
var registrosTabla=new Array();
var camposTabla=new Array();
var totalCampos=0;
var totalRegistros=0;

function numeroRegistros(campo2)
    {
      totalRegistros=0;
      for(i=0;i<campo2.length;i++)
       {
         if(campo2.charAt(i)=='@')
           totalRegistros++;
       }
    }
/**************************************************************************/
/**************************************************************************/
/**************************************************************************/

function transEnLinea()
 {
   var reg=0;
   var noreg=0;
   var total=0;

   numeroRegistros(document.transferencia.TransReg.value);
   reg=totalRegistros;
   numeroRegistros(document.transferencia.TransNoReg.value);
   noreg=totalRegistros;
   total=reg+noreg;
   return total;
 }

<!-- *********************************************** -->
<!-- modificaci�n para integraci�n pva 07/03/2002    -->
<!-- *********************************************** -->
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;
var domBenef;
var dirBenef;

function PresentarCuentasAbonoReg()
{
  opcion=2;
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=2","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();

}

function actualizacuenta(flag)
{
  if(opcion==2)
   {
	 document.transferencia.AbonoReg.value=ctaselec+"|"+ctatipre+"@"+cfm+"!"+ctaprod+"$"+ctadescr+"#"+ctatipro+"%"+ctasubprod;
	 document.transferencia.textAbonoReg.value=ctaselec+" "+ctatipre;
     if(flag == true){
       cuadroDialogo("Falta informaci\u00F3n para la cuenta del beneficiario  seleccionada, favor de complementarla",1);
       return false;    
     }
   }
}

function limpiaCuenta()
{
  document.transferencia.AbonoReg.value="";
  document.transferencia.textAbonoReg.value="";
}

//Inicia Funciones MA
function actualizaClave(){
	var lisBancos = document.getElementById('busDesBanco');
	var opciones = lisBancos.options[lisBancos.selectedIndex].value;
	var opc = opciones.split("|");
	document.getElementById('busCveAbaSwift').value = trim(opc[0]);
	document.getElementById('busDesCiudad').value = trim(opc[1]);
}

function Buscar(){
	if(trim(document.frmBuscaBanco.busCveAbaSwift.value)==""
	&& trim(document.frmBuscaBanco.busDesCiudad.value)==""){
		document.frmBuscaBanco.busDesCiudad.focus();
	    cuadroDialogo("Debe capturar la ciudad o la clave ABA/SWIFT",3);
	}else if(trim(document.frmBuscaBanco.busDesCiudad.value)!=""
	&& !EsAlfa(document.frmBuscaBanco.busDesCiudad.value)){
		document.frmBuscaBanco.busDesCiudad.focus();
	    cuadroDialogo("La ciudad es incorrecta.",3);
	}else if(trim(document.frmBuscaBanco.busCveAbaSwift.value)!=""
	&& !EsAlfaNumerica(document.frmBuscaBanco.busCveAbaSwift.value)){
		document.frmBuscaBanco.busCveAbaSwift.focus();
	    cuadroDialogo("La clave ABA/SWIFT es incorrecta.",3);
	}else if(trim(document.frmBuscaBanco.busCveAbaSwift.value)!=""
	&& (document.frmBuscaBanco.busCveAbaSwift.value.length<8 || document.frmBuscaBanco.busCveAbaSwift.value.length>11)){
		document.frmBuscaBanco.busCveAbaSwift.focus();
	    cuadroDialogo("La clave ABA/SWIFT debe ser de 8 a 11 digitos",3);
	}else{
		document.frmBuscaBanco.Modulo.value=1;
		document.frmBuscaBanco.busquedaNueva.value=1;
		document.frmBuscaBanco.submit();
	}
}

function Limpiar(){
	document.frmBuscaBanco.busDesCiudad.value="";
	document.frmBuscaBanco.busCveAbaSwift.value="";
	document.frmBuscaBanco.busDesBanco.selectedIndex = 0;
}

function AceptarBanco(){
	if(trim(document.frmBuscaBanco.busCveAbaSwift.value)==""
	|| trim(document.frmBuscaBanco.busDesCiudad.value)==""){
		document.frmBuscaBanco.busDesCiudad.focus();
	    cuadroDialogo("La ciudad y la clave ABA/SWIFT son requeridos.",3);
	}else if(trim(document.frmBuscaBanco.busDesCiudad.value)!=""
	&& !EsAlfa(document.frmBuscaBanco.busDesCiudad.value)){
		document.frmBuscaBanco.busDesCiudad.focus();
	    cuadroDialogo("La ciudad es incorrecta.",3);
	}else if(trim(document.frmBuscaBanco.busCveAbaSwift.value)!=""
	&& !EsAlfaNumerica(document.frmBuscaBanco.busCveAbaSwift.value)){
		document.frmBuscaBanco.busCveAbaSwift.focus();
	    cuadroDialogo("La clave ABA/SWIFT es incorrecta.",3);
	}else if(trim(document.frmBuscaBanco.busCveAbaSwift.value)!=""
	&& (document.frmBuscaBanco.busCveAbaSwift.value.length<8 || document.frmBuscaBanco.busCveAbaSwift.value.length>11)){
		document.frmBuscaBanco.busCveAbaSwift.focus();
	    cuadroDialogo("La clave ABA � SWIFT debe ser de 8 a 11 digitos",3);
	}else{
		document.frmBuscaBanco.busquedaNueva.value=0;
		document.frmBuscaBanco.submit();
	}
}

function trim(cadena){
	var str = "" + cadena;
	while(str.substring(0,1) == ' ') str = str.substring(1);
	while(str.substring(str.length-1) == ' ') str = str.substring(0,str.length-1);
	return str;
}

function Caracter_EsNumerico(caracter){
	var ValorRetorno = true;
	if( !(caracter >= "0" && caracter <= "9") ) ValorRetorno = false;
	return ValorRetorno;
}

function Caracter_EsAlfa(caracter)
{
	var ValorRetorno = true;

	if ( !(caracter >= "A" && caracter <= "Z") ) //Si no es Es Alpha Mayusculas
		if( !(caracter >= "&Aacute;" && caracter <= "&Uacute;") )
			if ( !(caracter >= "a" && caracter <= "z") ) // Si no es Alpha Minusculas
				if( !(caracter >= "&aacute;" && caracter <= "&uacute;") )
						ValorRetorno = false;
	return ValorRetorno;
}

function EsAlfaNumerica(cadena)
{
	var retorno = true;

	for(a=0;a<cadena.length && retorno;a++) {
		caracter = cadena.substring(a,a+1);
		if (!Caracter_EsAlfa(caracter))
			if (!Caracter_EsNumerico(caracter))
				retorno = false;
	}
	return retorno;
}

function EsAlfa(cadena)
{
	var retorno = true;

	for(a=0;a<cadena.length && retorno;a++) {
		caracter = cadena.substring(a,a+1);
		if (!Caracter_EsAlfa(caracter))
			if(caracter!=" ")
				retorno = false;
	}
	return retorno;
}
//Fin Funciones MA

/********************************************************************************/

function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/
<%= request.getAttribute("newMenu") %>
</script>
<!--
<%= request.getAttribute("strScript") %>
//-->
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0" onLoad='limpiaCuenta();'>

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%= request.getAttribute("MenuPrincipal") %>
   </td>
  </tr>
</table>

<%= request.getAttribute("Encabezado") %>

<FORM  NAME="transferencia" method=post onSubmit="return ValidaForma(this);" action="MTI_Transferencia">

 <table align=center border=0 cellpadding=0 cellspacing=0 >
 <tr>
  <td>

  <table ALIGN=center BORDER=0 cellpadding=2 cellspacing=3 class='textabdatcla'>
   <tr>
    <td class='textabdatcla' colspan=4 width='100%'>
	 <table class='textabdatcla' cellspacing=3 cellpadding=2 border=0 width='100%'>
	  <tr>
	   <td class='textabdatcla' align=right>Tipo Camb. Ventanilla <%= request.getAttribute("inicialCveDivisa")%>:&nbsp; </td>
	   <td class='textabdatcla' ><input type=text name=txtVentanilla size=10 value='<%= request.getAttribute("TCV_Venta")%>'  onFocus='blur();'></td>
	   <td class='textabdatcla' align=right>Tipo Camb. Enlace <%= request.getAttribute("inicialCveDivisa") %> contra Peso &nbsp; </td>
	   <td class='textabdatcla' ><input type=text name=txtEnlace size=10 value='<%= request.getAttribute("TCE_Venta") %>'      onFocus='blur();'></td>
	   <td class='textabdatcla' align=right>Tipo Camb. Enlace <%= request.getAttribute("inicialCveDivisa") %> contra Dol&aacute;r &nbsp; </td>
	   <td class='textabdatcla' ><input type=text name=txtEnlace2 size=10 value='<%= request.getAttribute("TCE_Dolar") %>'      onFocus='blur();'></td>
	  </tr>
	 </table>
	</td>
   </tr>

   <tr>
    <td colspan=4 bgcolor=white><br></td>
   </tr>

   <tr>
	 <td colspan=4 class="tittabdat"><INPUT TYPE="radio" value=0 NAME="Registradas" onClick='VerificaFac(0);' checked>&nbsp;<b>Cuentas Registradas</b></td>
   </tr>

   <tr>
     <td class='textabdatcla' colspan=4 width='100%'>
	   <table border=0 class='textabdatcla' cellspacing=3 cellpadding=2 width='100%'>
	    <tr>
		 <td class="tabmovtex" colspan=2 width='50%'>Ordenante</td>
		 <td class="tabmovtex" colspan=2 width='50%'>Cuenta del Beneficiario</td>
		</tr>
		<tr>
		 <td colspan=2 class="tabmovtex">
  		  <table border=1 width='100%' cellpadding=2 cellspacing=0>
           <tr>
            <td class="tabmovtex" width='100%'>
             <%= request.getAttribute("cuentasAsociadasCargo") %>
            </td>
           </tr>
          </table>

		 </td>
		 <td colspan=2 class="tabmovtex">
		  <input type="text" name=textAbonoReg  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="">
		  <A HREF="javascript:PresentarCuentasAbonoReg();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" border=0 align=absmiddle></A>
		  <input type="hidden" name="AbonoReg" value="">
		 </td>
	    </tr>
		<tr>
		 <td class="tabmovtex" colspan=4 ><br></td>
		</tr>

		<tr>
		 <td class="tabmovtex">Concepto:</td>
		 <td class="tabmovtex"><input type="text" SIZE="25" MAXLENGTH="60" NAME="ConceptoReg"></td>
		 <td class="tabmovtex" colspan=2>
		   <table border=0 cellpadding=0 cellspacing=0 width='100%'>
		     <tr>
		      <td class="tabmovtex" width='20%'>Importe MN: &nbsp; </td>
			  <td class="tabmovtex" width='25%'><INPUT TYPE="text" NAME="ImporteReg" SIZE="12" MAXLENGTH="15" value='' onBlur='cambiaImpMN(this,document.transferencia.ImporteRegDivisa,document.transferencia.ImporteRegUSD);'></td>
			  <td class="tabmovtex"> &nbsp; Importe Divisa: &nbsp; </td>
			  <td class="tabmovtex"><INPUT TYPE="text" NAME="ImporteRegDivisa" SIZE="12" MAXLENGTH="15" value='' onBlur='cambiaImpDIV(this,document.transferencia.ImporteReg,document.transferencia.ImporteRegUSD);'></td>
			 </tr>
		   </table>
		 </td>
		</tr>

		<tr>
		  <td class="tabmovtex" colspan=2><br></td>
		  <td class="tabmovtex" colspan=2>
		   <table border=0 cellpadding=0 cellspacing=0 width='100%'>
		     <tr>
			  <td class="tabmovtex" width='20%'>Importe Dlls: &nbsp; </td>
			  <td class="tabmovtex" width='25%'><INPUT TYPE="text" NAME="ImporteRegUSD" SIZE="12" MAXLENGTH="15" onBlur='cambiaImpUSD(this,document.transferencia.ImporteReg,document.transferencia.ImporteRegDivisa);'></td>
			  <td class="tabmovtex"><br></td>
			  <td class="tabmovtex"><br></td>
			 </tr>
		   </table>
		  </td>
		</tr>

		<tr>
		 <td class='tabmovtex' colspan=2><br></td>
		 <td class='tabmovtex' colspan=2>Requiere estado de cuenta fiscal? &nbsp; <input type=radio name=EdoFiscalReg value=0 checked> No &nbsp; <input type=radio name=EdoFiscalReg value=1> Si</td>
        </tr>

		<tr>
		 <td class='tabmovtex'>Clave</td>
		 <td class='tabmovtex'><input type=text size=10 maxlength=10 name=CveEspecial></td>
         <td class='tabmovtex'>RFC Beneficiario: &nbsp; </td>
		 <td class='tabmovtex'><input type=text name=RFCReg size=13 maxlength=13 onFocus="verificaRFC(this,document.transferencia.EdoFiscalReg)"></td>
        </tr>

		<tr>
		 <td class='tabmovtex'>Tipo de Cambio</td>
		 <td class='tabmovtex'><input type=text size=10 maxlength=10 name=tipoEspecial onFocus="verificaClave(this,document.transferencia.CveEspecial);" onblur="validaTipoCambio();"></td>
		 <td class='tabmovtex'>Importe IVA: &nbsp;</td>
		 <td class='tabmovtex'><input type=text name=ImporteIVAReg size=13 maxlength=12 onFocus="verificaRFC(this,document.transferencia.EdoFiscalReg)",></td>
		</tr>
		<!--  Se agrega para datos del beneficiario INDRA-SWIFT-P022574-ini //-->
		<tr>
			<td colspan="6">
				<div id="tblDirBenefi" style="display: none;">
				<table border=0 align=left width="100%">
					<tr>
						<td class="tabmovtex" width="120">Domicilio:</td>
					 	<td class="tabmovtex">
					 		<INPUT value="<%=request.getAttribute("calleBenef")!= null ? request.getAttribute("calleBenef") : "" %>" type="text" class="componentesHtml" SIZE="25" MAXLENGTH="40" NAME="calleBenef" />
					 	</td>
					 	<td width="85"></td>
						
						<td class="tabmovtex">Pa&iacute;s Beneficiario</td>
					 	<td class="tabmovtex">
							<SELECT NAME="paisBIBenef" class="componentesHtml">
						   		<option value="0" class="tabmovtex"> Seleccione un Pais </option>
						   		<%
						   		String comboPaises="";
					       		if(request.getAttribute("comboPaises")!=null)
							  	comboPaises=(String)request.getAttribute("comboPaises");
						   		%>
						   		<%=comboPaises%>
						 	</SELECT>
						</td>
				   	</tr>
		           	
		           	<tr>
						<td class="tabmovtex" width="120">Ciudad Beneficiario:</td>
					 	<td class="tabmovtex">
					 		<INPUT value="<%=request.getAttribute("ciudadBenef")!= null ? request.getAttribute("ciudadBenef") : "" %>" type="text" class="componentesHtml" SIZE="25" MAXLENGTH="40" NAME="ciudadBenef" />
					 	</td>
					 	<td width="85"></td>
					 
					 	<td class="tabmovtex">N&uacute;mero de Identificaci&oacute;n:</td>
					 	<td class="tabmovtex">
					 		<INPUT value="<%=request.getAttribute("idBenef")!= null ? request.getAttribute("idBenef") : "" %>" type="text" class="componentesHtml" SIZE="25" MAXLENGTH="40" NAME="idBenef" />
					 	</td>
				   	</tr>
				 </table>
				 </div>
			</td>
		</tr>
		<!--  Fin de datos del beneficiario INDRA-SWIFT-P022574-fin //-->
	   </table>
	 </td>
	</tr>

	<input type=hidden name=FechaReg size=10 value='<%= request.getAttribute("Fecha") %>' onFocus='blur();'>

	<tr>
	 <td bgcolor=white colspan=4><br></td>
	</tr>

<!--  Modulo para las cuentas no registradas //-->
   <% if(request.getAttribute("cuentasNoRegistradas")!=null) out.print(request.getAttribute("cuentasNoRegistradas")); %>
<!--  ************************************** //-->

<!--  Modulo para la importacion desde archivo //-->
   <% if(request.getAttribute("strImportar")!=null) out.print(request.getAttribute("strImportar")); %>
<!--  **************************************** //-->
  </table>

   </td>
  </tr>
 </table>

  <p>
  <input type=hidden name=Modulo value=<%= request.getAttribute("Modulo") %>>
  <input type=hidden name=TransReg value='<%= request.getAttribute("TransReg") %>'>
  <input type=hidden name=TransNoReg value='<%= request.getAttribute("TransNoReg") %>'>
  <input type=hidden name=TransArch value='<%= request.getAttribute("TransArch") %>'>
  <input type=hidden name=TCV_Venta value='<%= request.getAttribute("TCV_Venta") %>'>
  <input type=hidden name=TCE_Venta value='<%= request.getAttribute("TCE_Venta") %>'>
  <input type=hidden name=TCE_Dolar value='<%= request.getAttribute("TCE_Dolar") %>'>
  <input type=hidden name=inicialCuentaCargo value='<%= request.getAttribute("inicialCuentaCargo") %>'>
  <input type=hidden name=inicialImporte value='<%= request.getAttribute("inicialImporte") %>'>
  <input type=hidden name=inicialDesDivisa value='<%= request.getAttribute("inicialDesDivisa") %>'>
  <input type=hidden name=inicialCveDivisa value='<%= request.getAttribute("inicialCveDivisa") %>'>
  <input type=hidden name=inicialCveEspecial value=''>

  <table align=center border=0 cellspacing=0 cellpadding=0>
    <tr>
	 <!-- Agregar //-->
     <td><A href = "javascript:Agregar();" border = 0><img src = "/gifs/EnlaceMig/gbo25222.gif" border=0></a></td>
     <!-- Consultar anteriores
	 <td><A href = "javascript:Consultar();" border = 0><img src = "/gifs/EnlaceMig/gbo25400.gif" border=0></a></td>
	 //-->
    </tr>
  </table>

  <input type=hidden name=montoUSDreal value="">
  <input type=hidden name=montoMNreal value="">
  <input type=hidden name=montoDivisareal value="">
  <!-- Modif PVA 07/08/2002 -->
  <input type=hidden name=Directo_Inverso size=10 value='<% if(request.getSession().getAttribute("Directo_Inverso")!=null) out.print(request.getSession().getAttribute("Directo_Inverso"));%>'>

 </form>
<%if(request.getAttribute("muestraBusquedaBancos")!=null && request.getAttribute("muestraBusquedaBancos").equals("S")){ %>
<FORM  NAME="frmBuscaBanco" method=post action="MTI_Transferencia">
	<%if(request.getAttribute("tablaValDatos")!=null){
		out.print(request.getAttribute("tablaValDatos"));
	}%>
	<input type=hidden name=TransReg value='<%= request.getAttribute("TransReg") %>'>
	<input type=hidden name=TransNoReg value='<%= request.getAttribute("TransNoReg") %>'>
	<input type=hidden name=TCV_Venta value='<%= request.getAttribute("TCV_Venta") %>'>
	<input type=hidden name=TCE_Venta value='<%= request.getAttribute("TCE_Venta") %>'>
	<input type=hidden name=TCE_Dolar value='<%= request.getAttribute("TCE_Dolar") %>'>
	<input type=hidden name=inicialImporte value='<%= request.getAttribute("inicialImporte") %>'>
	<input type=hidden name=inicialCveDivisa value='<%= request.getAttribute("inicialCveDivisa") %>'>
	<input type=hidden name=inicialCuentaCargo value='<%= request.getAttribute("inicialCuentaCargo") %>'>
	<input type=hidden name=Registradas value='<%= request.getAttribute("Registradas") %>'>
	<input type=hidden name=ImporteRegUSD value='<%= request.getAttribute("ImporteRegUSD") %>'>
	<input type=hidden name=ImporteReg value='<%= request.getAttribute("ImporteReg") %>'>
	<input type=hidden name=ImporteRegDivisa value='<%= request.getAttribute("ImporteRegDivisa") %>'>
	<input type=hidden name=ImporteReg value='<%= request.getAttribute("ImporteReg") %>'>
	<input type=hidden name=Modulo value='<%= request.getAttribute("Modulo") %>'>
	<input type=hidden name=CveEspecial value='<%= request.getAttribute("CveEspecial") %>'>
	<input type=hidden name=tipoEspecial value='<%= request.getAttribute("tipoEspecial") %>'>
	<input type=hidden name=busquedaNueva value='0'>
</FORM>
<%}else if(request.getAttribute("muestraBusquedaBancos")!=null && request.getAttribute("muestraBusquedaBancos").equals("N")){  %>
 <FORM  NAME="tabla" method=post onSubmit="return ValidaTabla(this);" action="MTI_Cotizar">
  <table border=0 align=center>
   <tr>
    <td align=right class='textabref'>
	 <% if(request.getAttribute("totalRegistros")!=null )   out.print(request.getAttribute("totalRegistros"));%>&nbsp;
	 <% if(request.getAttribute("importeTotal")!=null)  out.print(request.getAttribute("importeTotal")); %>
	</td>
   </tr>

   <tr>
     <td><% if(request.getAttribute("Tabla")!=null) out.print(request.getAttribute("Tabla"));%></td>
   </tr>

   <tr>
     <td class='textabref'><% if(request.getAttribute("Mensaje")!=null) out.print(request.getAttribute("Mensaje")); %></td>
   </tr>

   <tr>
    <td align=center><br><% if(request.getAttribute("Botones")!=null) out.print(request.getAttribute("Botones")); %></td>
   </tr>
 </table>

   <input type=hidden name=strCheck>
   <input type=hidden name=Lineas value='<%= request.getAttribute("Lineas") %>'>
   <input type=hidden name=Importe value='<%= request.getAttribute("importeTotal") %>'>
   <input type=hidden name=Modulotabla value=<%= request.getAttribute("ModuloTabla") %>>
   <input type=hidden name=TransReg value='<%= request.getAttribute("TransReg") %>'>
   <input type=hidden name=TransNoReg value='<%= request.getAttribute("TransNoReg") %>'>
   <input type=hidden name=TransArch value='<%= request.getAttribute("TransArch") %>'>
   <input type=hidden name=TCV_Venta value='<%= request.getAttribute("TCV_Venta") %>'>
   <input type=hidden name=TCE_Venta value='<%= request.getAttribute("TCE_Venta") %>'>
   <input type=hidden name=TCE_Dolar value='<%= request.getAttribute("TCE_Dolar") %>'>
   <input type=hidden name=inicialCuentaCargo value='<%= request.getAttribute("inicialCuentaCargo") %>'>
   <input type=hidden name=inicialImporte value='<%= request.getAttribute("inicialImporte") %>'>
   <input type=hidden name=inicialDesDivisa value='<%= request.getAttribute("inicialDesDivisa") %>'>
   <input type=hidden name=inicialCveDivisa value='<%= request.getAttribute("inicialCveDivisa") %>'>
   <input type=hidden name=Result value='<%= request.getAttribute("Result") %>'>
   <input type=hidden name=inicialCveEspecial value=''>

 </form>
 <%} %>

<%=
	(request.getAttribute("mensaje") != null)?
	("<script language='javascript'>cuadroDialogo(" + (String)request.getAttribute("mensaje") +
	");</script>"):""
%>
</body>
</html>