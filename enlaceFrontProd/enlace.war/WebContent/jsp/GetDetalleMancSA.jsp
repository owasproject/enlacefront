
<%@page contentType="text/html"%>
<%--
Autor: Horacio Oswaldo Ferro D�az
Pagina para desplegar el resultado de la consulta de Mancomunidad
--%>
<jsp:useBean id="ResultadosMancDetalle" scope="session" class="java.util.ArrayList"/>
<%@page import="mx.altec.enlace.bo.DatosMancSA"%>
<%--
<jsp:useBean id="CuentaConsulta" scope="session" class="java.lang.String"/>
<jsp:useBean id="SeleccionaTodos" scope="session" class="java.lang.Boolean"/>
<jsp:useBean id="IndexMancomunidad" scope="session" class="java.lang.Integer"/>--%>
<%
	Boolean SeleccionaTodos = (Boolean) session.getAttribute("SeleccionaTodos");
	Integer IndexMancomunidad = (Integer) session.getAttribute("IndexMancDetalle");
 %>


<%!
    public static final String ANTERIORES = "<a href='javascript:anteriores ();'>Anteriores&nbsp;";
    public static final String SIGUIENTES = "<a href='javascript:siguientes ();'>Siguientes&nbsp;";
%>
<html>
<head>
  <title>Banca Virtual</title>
  <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
  <script language='JavaScript' src='/EnlaceMig/cuadroDialogo.js'></script>
  <script language='JavaScript' src='/EnlaceMig/scrImpresion.js'></script>
  <script language='JavaScript1.2' src='/EnlaceMig/fw_menu.js'></script>
  <%String nominasAttr=(String)request.getSession().getAttribute("nominasAttr");
        if(nominasAttr==null){
        	nominasAttr="";
        }
  %>
  <script language='JavaScript'>

    <%-- Inicio Java Script App --%>
    function MM_preloadImages() { //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }

    function MM_swapImgRestore() { //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }

    function MM_findObj(n, d) { //v3.0
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
            d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
    }

    function MM_swapImage() { //v3.0
        var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }
    <%=session.getAttribute ("newMenu")%>
    <%-- Fin Java Script App --%>

    function CheckAll() {
        // Modificacion Paula Hern�ndez
        for(i = 0;i < document.FrmAutoriza.elements.length; i++)
        if(document.FrmAutoriza.elements[i].type == "checkbox")
           document.FrmAutoriza.elements[i].checked = document.FrmAutoriza.todos.checked;
        return true;
    }

    function anteriores () {
        document.FrmAutoriza.action = "AutorizaAMancomunidadSA";
        document.FrmAutoriza.OpcPag.value = "<%=mx.altec.enlace.servlets.GetAMancomunidad.PREV%>";
        document.FrmAutoriza.submit ();
    }

    function siguientes () {
        document.FrmAutoriza.action = "AutorizaAMancomunidadSA";
        document.FrmAutoriza.submit ();
    }

	function autorizaTodos () {
	    //Desmarca los registros que hubieran estado seleccionados
	    for(i = 0;i < document.FrmAutoriza.elements.length; i++)
    	    if(document.FrmAutoriza.elements[i].type == "checkbox")
        	   document.FrmAutoriza.elements[i].checked = false;
		document.FrmAutoriza.opSubmit.value = "1";
        document.FrmAutoriza.action = "AutorizaAMancomunidadSA";
        document.FrmAutoriza.submit ();
    }

	function regresar () {
		document.FrmAutoriza.opSubmit.value = "0";
        document.FrmAutoriza.action = "GetAMancomunidadSA";
        document.FrmAutoriza.submit ();
    }

    function autoriza (Opcion) {
//		alert("opcion = " + Opcion);
		document.FrmAutoriza.TipoOp.value = Opcion;
        if (Opcion == "1")
			document.FrmAutoriza.Autoriza.value = "1";
        if (Opcion == "2") {
			document.FrmAutoriza.Autoriza.value = "2";
		}
		<%if("NOMI".equals(nominasAttr) || "NOIT".equals(nominasAttr)){%>
		 if (document.FrmAutoriza.Cancelado.value == "S")
		 	cuadroDialogo ("Archivo u operaci�n previamente cancelado.",1);
		 else if (document.FrmAutoriza.Cancelado.value == "A")
			cuadroDialogo ("Archivo previamente autorizado.",1);
		 else
         	document.FrmAutoriza.submit ();
        <%}else {%>
        if (validaFolios ()) document.FrmAutoriza.submit ();
        <%}%>
    }

    function validaFolios () {
        var total = document.FrmAutoriza.elements.length;
        var j = 0;
        for (i = 0; i < total; i++) {
            var e = document.FrmAutoriza.elements[i];
            if (e.type == 'checkbox' && e.name != 'todos') {
                if (e.checked) {
                    j++;
                }
            }
        }

//	alert("valor j=" + j);
        if (j == 0) {
            cuadroDialogo ("Usted no ha seleccionado ninguna operaci&oacute;n.\n\nPor favor seleccione la(s) operacion(es) \npendiente(s) a modificar.",1);
            return false;
        }
        return true;
    }
  </script>
  <link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>
</head>
<body topmargin='0' leftmargin='0' marginheight='0' marginwidth='0' bgcolor='#ffffff' onload='MM_preloadImages("/gifs/EnlaceMig/gbo25131.gif","/gifs/EnlaceMig/gbo25111.gif","/gifs/EnlaceMig/gbo25151.gif","/gifs/EnlaceMig/gbo25031.gif","/gifs/EnlaceMig/gbo25032.gif","/gifs/EnlaceMig/gbo25051.gif","/gifs/EnlaceMig/gbo25052.gif","/gifs/EnlaceMig/gbo25091.gif","/gifs/EnlaceMig/gbo25092.gif","/gifs/EnlaceMig/gbo25012.gif","/gifs/EnlaceMig/gbo25071.gif","/gifs/EnlaceMig/gbo25072.gif","/gifs/EnlaceMig/gbo25011.gif")' background='/gifs/EnlaceMig/gfo25010.gif'>
  <table border='0' cellpadding='0' cellspacing='0' width='571'>
    <tr valign='top'>
      <td width='*'>
        <%=session.getAttribute ("MenuPrincipal")%>
      </td>
    </tr>
  </table>
  <%=session.getAttribute ("Encabezado")%>
  <br>
  <form onsubmit='' method='POST' action='AutorizaAMancDetalleSA' name='FrmAutoriza'>
    <input type='hidden' name='OpcPag' value='<%=mx.altec.enlace.servlets.AutorizaAMancomunidadSA.NEXT%>'>
    <input type='hidden' name='Opcion' value='1'>
    <input type='hidden' name='Autoriza' value='2'>
	<input type='hidden' name='TipoOp' id="TipoOp" value=''>
	<input type='hidden' name='opSubmit' value=''>
	<%if(!nominasAttr.equals("")) {
		if (((DatosMancSA) ResultadosMancDetalle.get(0)).getEstatus().equals("C")
				|| ((DatosMancSA) ResultadosMancDetalle.get(0)).getEstatus().equals("N")) { //El archivo esta cancelado
		%>
			<input type='hidden' name='Cancelado' value='S'>
		<%} else if (((DatosMancSA) ResultadosMancDetalle.get(0)).getEstatus().equals("A")){ %>
			<input type='hidden' name='Cancelado' value='A'>
		<%}else { %>
			<input type='hidden' name='Cancelado' value='N'>
		<%}
	}%>


    <table width='1000' border='0' cellspacing='2' cellpadding='3' class='tabfonbla'>
      <tr>
        <td colspan='12' class='texenccon'>Total de registros&nbsp;:&nbsp;<%=ResultadosMancDetalle.size ()%></td>
      </tr>
      <tr>
        <td colspan='12' class='texenccon'>Periodo del&nbsp;<%=request.getSession ().getAttribute ("FechaIni")%>&nbsp;al&nbsp;<%=request.getSession ().getAttribute ("FechaFin")%></td>
      </tr>
   <%if (((DatosMancSA) ResultadosMancDetalle.get(0)).getTipo_operacion().equals("DIBT")) { %>
      <tr>
        <td align='right' class='tittabdat'>
          Todos<input type='checkbox' name='todos' onclick='CheckAll ()'>
        </td>
        <td align='center' class='tittabdat'>
          Fecha Registro.
        </td>
        <td align='center' class='tittabdat'>
          Folio Reg.
        </td>
        <td align='center'class='tittabdat'>
          Cta. cargo
        </td>
        <td align='center'class='tittabdat'>
          Cta. abono/M&oacute;vil
        </td>
        <td align='center' class='tittabdat'>
          Beneficiario
        </td>
        <td align='center' class='tittabdat'>
          Banco
        </td>
        <td align='center' class='tittabdat'>
          Importe
        </td>
        <td align='center' class='tittabdat'>
          Concepto
        </td>
        <td align='center' class='tittabdat'>
          Referencia Interbancaria
        </td>
        <td align='center' class='tittabdat'>
          Forma Aplicaci&oacute;n
        </td>
        <td align='center' class='tittabdat'>
          Usuario Registr&oacute;
        </td>
	<td align='center' class='tittabdat'>
	  Usuario Autorizaci&oacute;n 2
	</td>
	<td align='center' class='tittabdat'>
	  Usuario Autorizaci&oacute;n 3
	</td>
	<td align='center' class='tittabdat'>
	  Usuario Autorizaci&oacute;n 4
	</td>
	<td align='center' class='tittabdat'>
          Estatus
        </td>
      </tr>
      <%}else if("NOIT".equals(nominasAttr)){%>
      <tr>
         <td align='center'>
          &nbsp;
         </td>
        <td align='center' class='tittabdat'>
          Cuenta de Cargo
        </td>
        <td align='center' class='tittabdat'>
          Nombre del Empleado
        </td>
        <td align='center' class='tittabdat'>
          Tipo de cuenta
        </td>
        <td align='center' class='tittabdat'>
          Cuenta de Abono/M&oacute;vil
        </td>
        <td align='center' class='tittabdat'>
          Importe
        </td>
        <td align='center' class='tittabdat'>
          Banco Receptor
        </td>
        <td align='center' class='tittabdat'>
          Plaza Banxico
        </td>
      </tr>
      <%}else if("NOMI".equals(nominasAttr)){%>
      <tr>
      <td align='center'>
        &nbsp;
      </td>
      <td align='center' class='tittabdat'>
        Cuenta de Cargo
      </td>
      <td align='center' class='tittabdat'>
        N&uacute;mero de Empleado
      </td>
      <td align='center' class='tittabdat'>
        Apellido paterno
      </td>
      <td align='center' class='tittabdat'>
        Apellido materno
      </td>
      <td align='center' class='tittabdat'>
        Nombre del Empleado
      </td>
      <td align='center' class='tittabdat'>
        Cuenta de Abono/M&oacute;vil
      </td>
      <td align='center' class='tittabdat'>
        Importe
      </td>
    </tr>
    <%}else{%>
     <tr>
        <td align='right' class='tittabdat'>
          Todos<input type='checkbox' name='todos' onclick='CheckAll ()'>
        </td>
        <td align='center' class='tittabdat'>
          Fecha Registro.
        </td>
        <td align='center' class='tittabdat'>
          Folio Reg.
        </td>
        <td align='center'class='tittabdat'>
          Tipo Oper.
       </td>
        <td align='center' class='tittabdat'>
          Cta. cargo
        </td>
        <td align='center' class='tittabdat'>
          Descripci&oacute;n
        </td>
        <td align='center' class='tittabdat'>
          Cta. abono/M&oacute;vil
        </td>
        <td align='center' class='tittabdat'>
          Descripci&oacute;n
        </td>
        <td align='center' class='tittabdat'>
          Importe
        </td>
        <td align='center' class='tittabdat'>
          Fecha de Aplicaci&oacute;n
        </td>
        <td align='center' class='tittabdat'>
          Usuario Registr&oacute;
        </td>
	<td align='center' class='tittabdat'>
	  Usuario Autorizaci&oacute;n 2
	</td>
	<td align='center' class='tittabdat'>
	  Usuario Autorizaci&oacute;n 3
	</td>
	<td align='center' class='tittabdat'>
	  Usuario Autorizaci&oacute;n 4
	</td>
        <td align='center' class='tittabdat'>
          Estatus
        </td>
      </tr>
      <%} %>

      <%
        int paginacion = 50;
        int index = IndexMancomunidad.intValue ();
        int siguiente = index + paginacion;
        int restantes = (ResultadosMancDetalle.size () - (index + 50)) > paginacion ? paginacion : ResultadosMancDetalle.size () - (index + 50);
        System.out.println ("Indice: " + index);
        System.out.println ("Restantes: " + restantes);
        java.util.ListIterator liResultados = ResultadosMancDetalle.listIterator (index);
        if (((DatosMancSA) ResultadosMancDetalle.get(0)).getTipo_operacion().equals("DIBT")) {
            while (liResultados.hasNext () && index < siguiente) {
            Object o = liResultados.next ();
            try {
      %>
      <%= new mx.altec.enlace.bo.DatosMancSA().getHTMLFormatTI(index, (mx.altec.enlace.bo.DatosMancSA) o)%>
      <%
            } catch (Exception ex) {
				System.out.println ("****ENTRO A UN A EXCEPTION****: " );
                ex.printStackTrace ();
                break;
            }
            index++;
            }
        }else if("NOIT".equals(nominasAttr)){
        	while (liResultados.hasNext () && index < siguiente) {
                Object o = liResultados.next ();
                try {
    		      %>
    		      <%= new mx.altec.enlace.bo.DatosMancSA().getHTMLFormatNomiInte(index, (mx.altec.enlace.bo.DatosMancSA) o)%>
    		      <%
                } catch (Exception ex) {
    				System.out.println ("****ENTRO A UN A EXCEPTION****: " );
                    ex.printStackTrace ();
                    break;
                }
                index++;
                }
        }else if("NOMI".equals(nominasAttr)){
        	while (liResultados.hasNext () && index < siguiente) {
            Object o = liResultados.next ();
            try {
		      %>
		      <%= new mx.altec.enlace.bo.DatosMancSA().getHTMLFormatNomi(index, (mx.altec.enlace.bo.DatosMancSA) o)%>
		      <%
            } catch (Exception ex) {
				System.out.println ("****ENTRO A UN A EXCEPTION****: " );
                ex.printStackTrace ();
                break;
            }
            index++;
            }
         }else {
        	while (liResultados.hasNext () && index < siguiente) {
            Object o = liResultados.next ();
            try {
      %>
      <%= new mx.altec.enlace.bo.DatosMancSA().getHTMLFormatTMB(index, (mx.altec.enlace.bo.DatosMancSA) o)%>
      <%
            } catch (Exception ex) {
				System.out.println ("****ENTRO A UN A EXCEPTION****: " );
                ex.printStackTrace ();
                break;
            }
            index++;
            }
        }
        %>
      <tr>
        <td colspan='13' align='center' class='texenccon'>
          <%=IndexMancomunidad.intValue () == 0 ? "" : ANTERIORES + paginacion + "</a>&nbsp;"%><%=liResultados.hasNext () ? SIGUIENTES + restantes + "</a>" : ""%>
        </td>
      </tr>
	  <tr>
	  <td align='center' colspan='13'>
	  <br><br>
			   Ahora puede realizar el envio de las Transferencias.<br>
			   <i>Este proceso puede tardar <font color=red>varios minutos</font>, por favor espere ...</i><br><br>
	  </td>
	  </tr>
	  <tr>
        <td colspan='13' align='center' class='texenccon'>
		  <a href='javascript:autorizaTodos();'><img border='0' src='/gifs/EnlaceMig/gbo25430b.gif' width='92' height='22' alt='Autorizar Todas'></a>
         <%if("NOMI".equals(nominasAttr) || "NOIT".equals(nominasAttr)){%>
         	&nbsp;
         <%}else{ %>
          <a href='javascript:autoriza (1);'><img border='0' src='/gifs/EnlaceMig/gbo25430.gif' width='92' height='22' alt='Autorizar'></a>
          <%} %>
          <a href='javascript:autoriza (2);'><img border='0' src='/gifs/EnlaceMig/gbo25190.gif' width='85' height='22' alt='Cancelar'></a>
          <a href='javascript:scrImpresion ();'><img border='0' src='/gifs/EnlaceMig/gbo25240.gif' width='83' height='22' alt='Imprimir'></a>
          <a href='<%=session.getAttribute ("ArchivoManc")%>'><img border='0' src='/gifs/EnlaceMig/gbo25230.gif' width='85' height='22' alt='Exportar'></a>
          <A href='javascript:regresar();'><img border="0" name="imageField32" src="/gifs/EnlaceMig/gbo25320.gif" width="83" height="22" alt="Regresar"></a>
        </td>
      </tr>
    </table>
  </form>
</body>
</html>