<HTML>
<HEAD><TITLE>Enlace</TITLE>

<!-- #BeginEditable "MetaTags" -->
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s25660">
<meta name="Proyecto" content="Portal">
<meta name="Version" content="1.0">
<meta name="Ultima version" content="16/05/2001 18:00">
<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico">
<!-- #EndEditable -->

<script language = "JavaScript"    SRC = "/EnlaceMig/ValidaFormas.js"></script>
<script language = "JavaScript1.2" src = "/EnlaceMig/fw_menu.js"></script>

<script languaje="javaScript">

 function enviarForma(f)
 {
    f.ventana.value=2;
	f.submit();
 }

</script>

<script languaje="javaScript">

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

<%= request.getAttribute("newMenu") %>

</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</HEAD>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')"
background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
       <%= request.getAttribute("MenuPrincipal") %></TD>
  </TR>
</TABLE>

<%= request.getAttribute("Encabezado" ) %>

<!-- <%= request.getAttribute("glbDebug" ) %> -->

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <FORM  NAME="frmDeclara" method=post onSubmit="return validaArchivo(this);" action="ImpDeclaracion">
    <tr>
      <td align="center">
        <table border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
          <tr>
            <td class="tittabdat" colspan="2" nowrap> Datos de la declaraci&oacute;n</td>
          </tr>
          <tr align="center">
            <td class="textabdatcla" valign="top" colspan="2">
              <table border="0" cellspacing="2" cellpadding="3">
                <tr>
                  <td class="tabmovtex" colspan="4" nowrap>N&uacute;mero de folio:</td>
                </tr>
                <tr>
                  <td class="tabmovtex" colspan="4">
					<INPUT TYPE="text" NAME="numFolio" size="25" onFocus='blur();' class="tabmovtex"
					VALUE="<%= request.getAttribute("numFolio" ) %>">
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex" colspan="4" nowrap>RFC:</td>
                </tr>
                <tr>
                  <td class="tabmovtex" colspan="4">
					<INPUT TYPE="text" NAME="RFC" size="25" onFocus='blur();' class="tabmovtex"
					VALUE="<%= request.getAttribute("RFC" ) %>">
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex" colspan="4" nowrap>Raz&oacute;n social:</td>
                </tr>
                <tr>
                  <td class="tabmovtex" colspan="4">
					<INPUT TYPE="text" NAME="razonSocial" size="30" onFocus='blur();' class="tabmovtex"
				    VALUE="<%= request.getAttribute("razonSocial" ) %>">
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex" colspan="4" nowrap>Periodo de pago:</td>
                </tr>
                <tr>
                  <td class="tabmovtex" width="70" align="right" nowrap> De la
                    fecha:</td>
                  <td align="left" class="tabmovtex">
					<INPUT TYPE="text" NAME="periodoInicio" size="10" onFocus='blur();' class="tabmovtex"
					VALUE="<%= request.getAttribute("periodoInicio" ) %>">
                  </td>
                  <td width="70" class="tabmovtex" align="right" nowrap>A la fecha:
                  </td>
                  <td align="left" class="tabmovtex">
					<INPUT TYPE="text" NAME="periodoFin" size="10" onFocus='blur();' class="tabmovtex"
					VALUE="<%= request.getAttribute("periodoFin" ) %>">
                  </td>
                </tr>
                <tr>
                  <td class="tabmovtex" colspan="4" nowrap> Importe:</td>
                </tr>
                <tr align="left">
                  <td class="tabmovtex" colspan="4">
					<INPUT TYPE="text" NAME="importe" size="20" onFocus='blur();' class="tabmovtex"
					VALUE="<%= request.getAttribute("importe" ) %>">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <br>
        <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td width="85">
			  <A href = "javascript:enviarForma(document.frmDeclara);" border = 0><img src = "/gifs/EnlaceMig/gbo25520.gif" border=0 width="78" height="22" alt="Enviar"></a>
            </td>
          </tr>
        </table>
        <br>
      </td>
    </tr>
	  <input type=hidden name=ventana value=0>
	  <input type=hidden name="decDatos" value="<%= request.getAttribute("decDatos" ) %>">
  </form>
</table>

</BODY>
</HTML>
