<%@ page language="java" %>
<%@ page contentType="text/html;charset=ISO8859_1"%>
<%@ page import="mx.altec.enlace.bo.InterpretaPD15" %>
<%@page import="mx.altec.enlace.bo.DisposicionCE"%>
<%@page import="mx.altec.enlace.bo.CotizacionCE"%>
<%@page import="mx.altec.enlace.bo.FormatosMoneda"%>
<%!InterpretaPD15 pd15;%>
<%!DisposicionCE dce;%>
<%!CotizacionCE cce;%>
<%!FormatosMoneda fm;%>
<%--! String mensajeConfirmacion=new String(""); --%>
<%!String abre;%>

<%--
   23/10/2002 Se elimin� que se reciba informaci�n del comprobante aqu�. Se env�a hasta que hace click en la liga.
   25/10/2002 Se revis� el JavaScript.
   08/11/2002 Se imprime un salto de carro cuando no hay concepto.
   15/11/2002 Dise�o.
--%>


<html>
<head>
<Meta name="versionServlet" content="2.2.0">
<title>Comprobante de operaci&oacute;n</title>

<!-- Autor: Hugo Ruiz Zepeda -->

<script language="JavaScript" type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript" type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript" src="/EnlaceMig/Convertidor.js"></script>

<script language="JavaScript1.2" type="text/javascript">

function despliegaComprobante()
{
   window.open("ceVista?pantalla=4", "Imprimir", "dependent=true,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=450,height=450");
}

</script>



<script language="JavaScript" type="text/javascript">
function MM_preloadImages() { //v3.0

	var d=document;

	if(d.images){

		if(!d.MM_p)

			d.MM_p=new Array();

		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}

	}

}

function MM_swapImgRestore() { //v3.0

	var i,x,a=document.MM_sr;

	for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_findObj(n, d) { //v3.0

	var p,i,x;

	if(!d) d=document;

	if((p=n.indexOf("?"))>0&&parent.frames.length) {

	d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

	if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

	for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;

}


function MM_swapImage() { //v3.0

var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}
 	<%= (String)request.getAttribute("newMenu") %>
</script>


<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</head>
<%
  out.print("<!--");
  out.print('\u0048');
  out.print('\u0052');
  out.print('\u005A');
  out.println("-->");
%>
<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor="#ffffff" leftMargin="0" topMargin="0" marginwidth="0" marginheight="0">
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
	   <%=(String)request.getAttribute("MenuPrincipal")%>
       </TD>
  </TR>
</TABLE>

<%=(String)request.getAttribute("Encabezado")%>




<%
   pd15=(InterpretaPD15)request.getAttribute("disposicion");
   dce=(DisposicionCE)request.getAttribute("disp02");
   cce=(CotizacionCE)request.getAttribute("cot01");
   fm=new FormatosMoneda();

   /*
   mensajeConfirmacion=request.getParameter("mensajeConfirmacion");
   request.setAttribute("mensajeConfirmacion", mensajeConfirmacion);
   */

  if(pd15==null)
  {
    //out.println("disposicion es nulo<BR>");
    pd15=new InterpretaPD15();
  }
  else
  {
    //out.println("disposicion no es nulo<BR>");
  }

  if(dce==null)
  {
    //out.println("disp02 es nulo<BR>");
    dce=new DisposicionCE();
  }
  else
  {
    //out.println("dispo02 no es nulo<BR>");
  }

  if(cce==null)
  {
    //out.println("cotizacion es nulo<BR>");
    cce=new CotizacionCE();
  }
  else
  {
    //out.println("cotizacion no es nulo<BR>");
  }
%>

<TABLE  width="760" border="0" cellspacing="0" cellpadding="0">

<TR>
<TD colspan="2" align="center">

<BR>

<TABLE width="450" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
<TR>
<TD align="center" class="tittabdat">Cuenta</TD>
<TD align="center" class="tittabdat">Descripci&oacute;n</TD>
<TD align="center" class="tittabdat">L&iacute;nea de cr&eacute;dito</TD>
</TR>


<TR bgcolor="#CCCCCC">
<TD align="center" class="textabdatcla"><%=cce.getCuenta() %></TD>
<TD align="center" class="textabdatcla"><%=cce.getDescripcion() %></TD>
<TD align="center" class="textabdatcla"><%=cce.getLineaCredito() %></TD>
</TR>
</TABLE>
	<BR><BR>

</TD>
</TR>

 <TR>
  <TD>

  <table width="100%" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
  <TR>
    <TD align="center" class="tittabdat">Contrato</TD>
    <TD align="center" class="tittabdat">Fecha y hora del movimiento</TD>
    <TD align="center" class="tittabdat">Usuario</TD>
    <TD align="center" class="tittabdat">Referencia</TD>
    <TD align="center" class="tittabdat">Descripci&oacute;n del movimiento</TD>
    <TD align="center" class="tittabdat">Importe</TD>
    <TD align="center" class="tittabdat">N&uacute;mero de disposici&oacute;n</TD>
    <TD align="center" class="tittabdat">Tasa</TD>
    <TD align="center" class="tittabdat">Plazo</TD>
    <TD align="center" class="tittabdat">Inter&eacute;s</TD>
    <TD align="center" class="tittabdat">Total</TD>
    <TD align="center" class="tittabdat">Fecha de vencimiento</TD>
  </TR>

<%-- abre=new String("window.open('jsp/ImprimeComprobanteCE.jsp?mensajeConfirmacion='"+mensajeConfirmacion+", 'Imprimir', 250, 200);return false;"); --%>

  <TR bgcolor="#CCCCCC">
    <TD align="center" class="textabdatobs"><%=dce.getContrato()%></TD>
    <TD align="center" class="textabdatobs"><%=dce.getFechaOperacion()%><BR><%=dce.getHoraOperacion()%></TD>
    <TD align="center" class="textabdatobs"><script>document.write(cliente_7To8("<%=dce.getUsuario()%>"));</script></TD>
    <!--<TD align="center" class="textabdatobs"><A href="" onClick="window.open('ceVista?pantalla=5', 'Imprimir', "");return false;" border="0"></A></TD>-->
    <TD align="center" class="textabdatobs"><A href="" onClick="despliegaComprobante();return false;" border="0"><%=pd15.getNumeroReferencia()%></A></TD>
    <TD align="center" class="textabdatobs">
      <% if(cce.getConcepto().length()>0)
            out.println(cce.getConcepto());
         else
            out.println("<BR>");
      %>
    </TD>
    <TD align="center" class="textabdatobs">$<% out.println(fm.daMonedaConComas(cce.getMonto(), false)); %></TD>
    <TD align="center" class="textabdatobs"><%=pd15.getNumeroDisposicion()%></TD>
    <TD align="center" class="textabdatobs"><%=cce.getTasa()%>%</TD>
    <TD align="center" class="textabdatobs"><%=pd15.getPlazo()%> d&iacute;as</TD>
    <TD align="center" class="textabdatobs">$<%=cce.getInteres()%></TD>
    <TD align="center" class="textabdatobs">$<%=cce.getImporteTotal()%></TD>
    <TD align="center" class="textabdatobs"><%=pd15.getFechaVencimiento()%></TD>
  </TR>

   <TR>
      <TD valign="top" align="justify" class="textabref" colspan="11">
         <BR>Si desea obtener su comprobante, d&eacute; <I>click</I> en el n&uacute;mero de referencia subrayado.
      </TD>
   </TR>


<!--
<TR>
 <TD align="center" colspan="11">

	<BR><BR>


   <Table border="0" cellspacing="0" cellpadding="0" class="tabfonbla">
     <TR>
       <TD align="right">
    	   <A href="ceVista?pantalla=0" border="0">
		      <img src="/gifs/EnlaceMig/gbo25320.gif" hspace="0" vspace="0" border="0" align="Middle" alt="Regresar">
         </A>
       </TD>
       <TD>
	<A href=""  onClick="window.print();return false">
	<img src="/gifs/EnlaceMig/gbo25210.gif" hspace="0" vspace="0" border="0" align="Middle" alt="Imprimir comprobante" usemap=#"">
 	 </A>
       </TD>
      </TR>
    </Table>


 </TD>
</TR>
-->

</TABLE>

<BR><BR>



  </TD>
 </TR>
</TAble>


</body>
</html>
