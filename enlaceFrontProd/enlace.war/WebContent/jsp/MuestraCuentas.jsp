<html>

<%
	String usrSnetComAdmin = (String) session.getAttribute("usrSnetComAdmin");
%>
<head>
<title>Banca Virtual</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script language = "JavaScript" type="text/javascript" SRC= "/EnlaceMig/passmark.js"></script>
<script language="JavaScript">
<!--

window.parent.LOGUEADO = true;

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">


</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceInternet/gbo25171.gif','/gifs/EnlaceInternet/gbo25111.gif','/gifs/EnlaceInternet/gbo25131.gif','/gifs/EnlaceInternet/gbo25151.gif')" background="/gifs/EnlaceInternet/gfo25010.gif">
<!-- #BeginEditable "Contenido" -->


<table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="596" valign="top">
      <table width="596" border="0" cellspacing="0" cellpadding="0" class="tabfonbla">
        <tr valign="top">
          <td rowspan="2"><img src="/gifs/EnlaceMig/glo25010.gif" width="237" height="41"></td>
          <td rowspan="2"><a href="javascript:;" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contactanos','','/gifs/EnlaceMig/gbo25111.gif',1)"><img src="/gifs/EnlaceMig/gbo25110.gif" width="30" height="33" name="contactanos" border="0"></a></td>
          <td rowspan="2"><img src="/gifs/EnlaceMig/gbo25120.gif" width="59" height="20" alt="Cont&aacute;ctenos"></td>
          <td rowspan="2"><a href="javascript:;" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('atencion','','/gifs/EnlaceMig/gbo25131.gif',1)"><img src="/gifs/EnlaceMig/gbo25130.gif" width="36" height="33" name="atencion" border="0"></a></td>
          <td rowspan="2"><img src="/gifs/EnlaceMig/gbo25140.gif" width="90" height="20" alt="Atenci&oacute;n telef&oacute;nica"></td>
          <td rowspan="2"><a href="javascript:;" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('centro','','/gifs/EnlaceMig/gbo25151.gif',1)"><img src="/gifs/EnlaceMig/gbo25150.gif" width="33" height="33" name="centro" border="0"></a></td>
          <td rowspan="2"><img src="/gifs/EnlaceMig/gbo25160.gif" width="93" height="20" alt="Centro de mensajes"></td>
          <td width="18" bgcolor="#DE0000"><img src="/gifs/EnlaceMig/gau25010.gif" width="18" height="17"></td>
        </tr>

		<tr valign="top">
          <td valign="top"><img src="/gifs/EnlaceMig/gau25010.gif" width="18" height="24"></td>
		  </tr>
		</table>
		<td valign="top" rowspan="2">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
        <tr>
          <td bgcolor="#DE0000"><img src="/gifs/EnlaceMig/gau25010.gif" width="35" height="17"></td>
          <td align="center" rowspan="2"><img src="/gifs/EnlaceMig/glo25020.gif" width="57" height="41" alt="Banco Santander Mexicano"></td>
          <td width="100%" bgcolor="#DE0000"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="17"></td>
        </tr>
        <tr>
          <td><img src="/gifs/EnlaceMig/gau25010.gif" width="35" height="24"></td>
          <td><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="24"></td>
        </tr>
      </table>
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="right" class="tabfonbla" width="126"><img src="/gifs/EnlaceMig/gba25010.gif" width="126" height="33" alt="Sitio seguro"></td>
          <td align="right" class="tabfonbla" rowspan="2" width="50">&nbsp;</td>
        </tr>
      </table>
	  </td>

      </table>

    </td>

  </tr>
  <tr>
    <td width="596" valign="bottom" align="right">
      <table width="596" border="0" cellspacing="6" cellpadding="0" align="right">
        <tr>
		<%= request.getAttribute( "Encabezado" ) %>
        </tr>
      </table>
    </td>
  </tr>
</table>
<!-- CONTENIDO INICIO -->
<FORM NAME="frmCuentas" METHOD="post" ACTION="logout?destino=supernetComer">
<table width="760" align="left" border="0" cellspacing="2" cellpadding="3" align="center">

	<tr>
		<td>
			<br>
		</td>
	</tr>
	<tr>
		<td>
			<table width="320" border="0" cellspacing="2" cellpadding="3" align="center">
				<tr>
					<td class="tittabdat" align="center"> Seleccione la cuenta con la que desea operar en Supernet Comercios</td>
				</tr>
				<tr>
		            <td align="center" class="tabmovtex">
						<SELECT  NAME="lstCuentas">
							<%= request.getAttribute("ctasComercios") %>
						</SELECT>
					</td>
				</tr>
				<tr>
				<%if(usrSnetComAdmin.equals("admin")){ %>
					<input type="hidden" name="ctasSnetComAdmin" value="<%=request.getSession().getAttribute("ctasSnetComAdmin")%>"/>					
				<%}%>				
				<tr>
				<td></td>
				</tr>
					<td align="center">
						<input type="image" border="0" name="Enviar" src="/gifs/EnlaceMig/gbo25520.gif" alt="Enviar">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</FORM>

<!-- CONTENIDO FINAL -->
</body>
</html>

<Script language = "JavaScript">
<!--
 function VentanaAyuda(ventana){
    hlp=window.open("/EnlaceMig/ayuda.html#" + ventana ,"hlp","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
    hlp.focus();
}
//-->
</Script>
<!-- 2007.01 -->