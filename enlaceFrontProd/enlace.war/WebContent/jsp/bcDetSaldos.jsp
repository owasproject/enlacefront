<%@page import="java.util.List,java.util.Iterator" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Enlace</title>
<!-- Include menu on page -->
<SCRIPT LANGUAGE="JavaScript" SRC="/EnlaceMig/fw_menu.js">
</SCRIPT>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<!-- Code to display a Dialog Window on error  -->
<Script Language = "JavaScript" SRC="/EnlaceMig/cuadroDialogo.js">
</Script>
<SCRIPT LANGUAGE="JavaScript">
<!--
/**Load images.
*/
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n];
	for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  return x;
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments;
  document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){
      document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];
   }
}
/*****************************************/
<!-- Include Java Script code for menus -->
<%= request.getAttribute("newMenu") %>

/**
   Validates a form and then submit it.
	 Param Frm is a Form Object.
	 Param Devolution is assigned to devol var
*/
function doSubmit(Frm,devolution){
   StrFacMsgError = "No tiene facultad para consultar devoluciones.";
   Frm.devol.value = devolution;
   StrFaculty = Frm.DetailFaculty.value;

   if(StrFaculty == "" || StrFaculty == "false" || StrFaculty == "null"){
       cuadroDialogo(StrFacMsgError,3);


   }else{
       Frm.submit();
   }
}

function printBalances(){
   scrImpresion();
}

function validateExport(){
   StrFacMsgError = "No tiene facultad para exportar archivos.";

   cuadroDialogo(StrFacMsgError,3);


}

function displayPathError(){
   StrExpMsgError = "Error al exportar archivo de consulta de saldos.";

   cuadroDialogo(StrExpMsgError,3);


}

//-->
</SCRIPT>
<LINK REL="stylesheet" HREF="/EnlaceMig/consultas.css" TYPE="text/css">
</head>
<BODY topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
 BGCOLOR="#ffffff"
 onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif',
			  '/gifs/EnlaceMig/gbo25111.gif',
			  '/gifs/EnlaceMig/gbo25151.gif',
			  '/gifs/EnlaceMig/gbo25031.gif',
			  '/gifs/EnlaceMig/gbo25032.gif',
			  '/gifs/EnlaceMig/gbo25051.gif',
			  '/gifs/EnlaceMig/gbo25052.gif',
			  '/gifs/EnlaceMig/gbo25091.gif',
			  '/gifs/EnlaceMig/gbo25092.gif',
			  '/gifs/EnlaceMig/gbo25012.gif',
			  '/gifs/EnlaceMig/gbo25071.gif',
			  '/gifs/EnlaceMig/gbo25072.gif',
			  '/gifs/EnlaceMig/gbo25011.gif');"
 background="/gifs/EnlaceMig/gfo25010.gif">


<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- HTML MAIN MENU -->
	   <%= request.getAttribute("MenuPrincipal") %>
    </td>
  </TR>
</TABLE>

<!-- Table with all the options user has selected to
     arrive this page. For instance "Servicios > Chequera Seguridad >
		 Registro de Cheques > En L�nea".
-->
<%= request.getAttribute("Encabezado" ) %>

<!-- ***** MAIN FORM OF THIS PAGE *****-->
<!-- Tab #1 -->
<table width="760" border="0" cellspacing="0" cellpadding="0">
<Form Name="FrmBalance" METHOD="POST" ACTION="bcConDevoluciones">
   <tr>
      <td align="center">
      <!-- Tab #2 -->
          <table width=650 border=0>
              <tr>
                  <td class="texenccon">
			      <span class="texencconbol">Cuenta:</span>
                  &nbsp;<%= (String)(request.getParameter("Account")) %>&nbsp;
                  </td>
              </tr>
              <tr>
                  <td class="texenccon">
                  <span class="texencconbol">Per&iacute;odo:</span>
                  &nbsp;Del <%= (String)(request.getParameter("FirstDate")) %>
                  al <%= (String)(request.getParameter("LastDate")) %>
                  </td>
              </tr>
          </table><!-- Tab #2 -->

          <!-- Tab #3 -->
          <table width="650" border="0" cellspacing="2" cellpadding="3"
				  class="tabfonbla">
              <tr>
                  <td align="center" class="tittabdat">Fecha</TD>
                  <td align="center" class="tittabdat">Horario de Operaci&oacute;n</TD>
                  <td align="center" class="tittabdat">Cr&eacute;dito</TD>
                  <td align="center" class="tittabdat">Techo Asignado</TD>
                  <td align="center" class="tittabdat">Devol.</TD>
                  <td align="center" class="tittabdat">Techo Real</TD>
				  <td align="center" class="tittabdat">Monto por Disponer</TD>
                  <td align="center" class="tittabdat">Saldo en Chequera</TD>
                  <td align="center" class="tittabdat">Cr&eacute;dito Dispuesto</TD>
				  <td align="center" class="tittabdat">Monto Operado</TD>
                  <td align="center" class="tittabdat">Estatus de Fondeo</TD>
              </tr>
<%
List lstBalances = (List)(request.getAttribute("BalancesList"));
Iterator itList = lstBalances.iterator();
int intIdxCol = 0;
int intIdxRow = 0;
String StrLine = "";
String StrHtmlSpace = "&nbsp;";

while(itList.hasNext()){
    StrLine = "";

    if((intIdxRow % 2) == 0){
        //if its pair column
        if(intIdxCol == 0){
            //if it's the first column of row
%>
              <tr  bgcolor=#CCCCCC>
                  <td class=textabdatobs>
<%
            StrLine = (String)(itList.next()) + StrHtmlSpace;						//if it's the first column of row Fecha
            out.print(StrLine);
            intIdxCol++;
%>
                  </TD>
<%
        }else{
            //print all the rest of columns of the row
            String strDevol = (String)(itList.next());								//print all the rest of columns of the row
            StrLine = strDevol + StrHtmlSpace;
%>
                  <td class=textabdatobs>
<%
            if(intIdxCol == 4){
                //"Total de Devoluciones" Column
%>
                   <a href="javaScript:doSubmit(document.forms[0],'<%out.print(strDevol);%>')">
<%
                out.print(StrLine);
%>
		   </a>
<%
            }else{
                out.print(StrLine);
            }
%>
                  </TD>
<%
            intIdxCol++;

             if((intIdxCol % 11) == 0){
                //if its the end of row
%>
              </tr>
<%
                 intIdxRow++;
                 intIdxCol = 0;
             }//end if
         }//end elseif(intIdxCol == 0)
     }else{
         //if its odd column
         if(intIdxCol == 0){
             //if it's the first column of row
%>
             <tr  bgcolor=#EBEBEB>
                 <td class=textabdatcla>
<%
             StrLine = (String)(itList.next()) + StrHtmlSpace;
             out.print(StrLine);
             intIdxCol++;
%>
                 </TD>
<%
         }else{
             //print all the rest of columns in row
             String strDevol = (String)(itList.next());
             StrLine = strDevol + StrHtmlSpace;
%>
                 <td class=textabdatcla>
<%
             if(intIdxCol == 4){
                 //"Total de Devoluciones" Column
%>
                    <a href="javaScript:doSubmit(document.forms[0],'<%out.print(strDevol);%>')">
<%
                 out.print(StrLine);
%>
                    </a>
<%
             }else{
                 out.print(StrLine);
             }//end elseif(intIdxCol == 4)
%>
                </TD>
<%
             intIdxCol++;

             if((intIdxCol % 11) == 0){
                 //if its the end of row
%>
            </tr>
<%
                 intIdxRow++;
                 intIdxCol = 0;
             }//end if
         }//end if(intIdxCol == 0)
     }//end elseif((intIdxRow % 2) == 0)
}//end while(itList.hasNext())
%>
        </table> <!-- tab 3 -->
     	<br>
 	<br>
        <!-- tab 5 -->
        <table width="90" border="0" cellspacing="0" cellpadding="0"
		     class="tabfonbla">
            <tr>
	        <td align="center">
	              <a href="javascript:printBalances()"><img border="0" name="Imprimir"
					  	src="/gifs/EnlaceMig/gbo25240.gif" width="90" height="22"
					  	alt="Imprimir"></a>
                </TD>
                <td align="center">
<%
String StrExpFaculty = (String)(request.getAttribute("ExportFaculty"));
String StrExpPath = (String)(request.getAttribute("ExportPath"));

if(null == StrExpFaculty || StrExpFaculty.equals("false") ||
   StrExpFaculty.equals("")){
%>
                  <a href="javascript:validateExport()">
<%
}else{
    if(StrExpFaculty.equals("true")){
        if(null == StrExpPath || StrExpPath.equals("")){
%>
                  <a href="javascript:displayPathError()">
<%
        }else{
%>
                  <a href='/Download/<%= (String)(request.getAttribute("ExportPath")) %>'>
<%
        }//end else if
    }//end if
}//end elseif
%>
                     <img border="0" name="Exportar"
					  src="/gifs/EnlaceMig/gbo25230.gif" width="90" height="22"
					  	   alt="Exportar">
                  </a>
              </TD>
              <td align="center">
                  <a href="bcConSaldos?event=initSaldos">
				      <img border="0" name="Regresar"
					   src="/gifs/EnlaceMig/gbo25320.gif" width="90" height="22"
					alt="Regresar"></a>
              </TD>
          </tr>
      </table><!-- tab 5 -->
  </tr><!-- tab 1 row 1-->
   <Input Type=Hidden Name="Account"
	  value=<%= (String)(request.getParameter("Account")) %>>
   <Input Type=Hidden Name="FirstDate"
	  Value=<%= (String)(request.getParameter("FirstDate")) %>>
   <Input Type=Hidden Name="LastDate"
	  Value=<%= (String)(request.getParameter("LastDate")) %>>
   <Input Type=Hidden name="event" value="initDetail">
   <Input Type=Hidden name="devol" value="">
   <INPUT TYPE=Hidden NAME="ExportFaculty"
	  VALUE='<%= (String)(request.getAttribute("ExportFaculty")) %>'>
   <INPUT TYPE=Hidden NAME="DetailFaculty"
	  VALUE='<%= (String)(request.getAttribute("DetailFaculty")) %>'>
   <INPUT TYPE=HIDDEN NAME="ExportLink"
	  VALUE='/Download/<%= (String)(request.getAttribute("ExportPath")) %>'>
</FORM>
</table><!-- tab 1 -->
<!--version 1.1.1 -->
</body>
</html>
