<html>

<head>
	<title>Banca Virtual</title>
	<!--meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"-->
	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

<script language="javaScript" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript" src="/EnlaceMig/cuadroDialogo.js"></script>
	<script language="JavaScript">

	function definir_valores_checkbox()
		{
		var contador=0;
		document.PMConsultCuentas.cuentaseliminar.value="";
		valor_asignar="";
		for (i=0;i<document.PMConsultCuentas.elements.length;i++)
			{
			if (document.PMConsultCuentas.elements[i].type=="checkbox")
				{
				if (document.PMConsultCuentas.elements[i].checked==true)
					{
					document.PMConsultCuentas.cuentaseliminar.value=document.PMConsultCuentas.cuentaseliminar.value +document.PMConsultCuentas.elements[i].value+"@";
					contador++;
					}
				}
			}
		document.PMConsultCuentas.contctaseliminar.value=contador;
		}

	function validar()
		{
		var result=false;
		/*alert("funcion validar");*/
		/* definir_valore s_checkbox()*/
		if (trimString(document.PMConsultCuentas.textcuenta.value).length>0)
			result=true;
		else
			cuadroDialogo("Debe seleccionar una cuenta",3);
		return result;
		}

	/*-- *********************************************** --*/
	/*-- modificación para integración pva 07/03/2002    --*/
	/*-- *********************************************** --*/
	var ctaselec;
	var ctadescr;
	var ctatipre;
	var ctatipro;
	var ctaserfi;
	var ctaprod;
	var ctasubprod;
	var tramadicional;
	var cfm;

	function PresentarCuentas()
		{
		msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
		msg.focus();
		}

	function actualizacuenta()
		{
		document.PMConsultCuentas.cuenta.value=ctaselec;
		document.PMConsultCuentas.textcuenta.value=ctaselec+" "+ctadescr;
		}

	</script>

	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>

	<script language="JavaScript">

	function MM_preloadImages()
		{ /*v3.0*/

		var d=document;
		if(d.images)
			{
			if(!d.MM_p) d.MM_p=new Array();
			var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
			for(i=0; i<a.length; i++)
			if (a[i].indexOf("#")!=0)
				{d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}
			}
		}

	function MM_swapImgRestore()
		{ /*v3.0*/

		var i,x,a=document.MM_sr;
		for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
		}

	function MM_findObj(n, d)
		{ /*v3.0*/

		var p,i,x;
		if(!d) d=document;
		if((p=n.indexOf("?"))>0&&parent.frames.length)
			{
			d=parent.frames[n.substring(p+1)].document;
			n=n.substring(0,p);
			}
		if(!(x=d[n])&&d.all) x=d.all[n];
		for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
		return x;
		}

	function MM_swapImage()
		{ /*v3.0*/
		var i,j=0,x,a=MM_swapImage.arguments;
		document.MM_sr=new Array;
		for(i=0;i<(a.length-2);i+=3)
			if((x=MM_findObj(a[i]))!=null)
				{
				document.MM_sr[j++]=x;
				if(!x.oSrc) x.oSrc=x.src;
				x.src=a[i+2];
				}
		}

	<% if (request.getAttribute("newMenu")!= null) out.println(request.getAttribute("newMenu")); %>
	</script>
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">

	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top">
		<td width="*">

		<!-- MENU PRINCIPAL -->
		<%
		if(session.getAttribute("MenuPrincipal")!=null)
			{out.println(session.getAttribute("MenuPrincipal"));}
		else if (request.getAttribute("MenuPrincipal")!= null)
			{out.println(request.getAttribute("MenuPrincipal"));}
		%>

		</TD>
	</TR>
	</TABLE>

	<%
/*	if(session.getAttribute("Encabezado")!=null)
		{out.println(session.getAttribute("Encabezado"));}
	else*/ if (request.getAttribute("Encabezado")!= null)
		{out.println(request.getAttribute("Encabezado"));}
	%>

	<FORM NAME=PMConsultCuentas METHOD=POST ACTION="CuentasImpuestos?OPCION=11" onSubmit="return validar();">
	<p>

	<table width="760" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
		<!-- Modificacion para integración
		<%= request.getAttribute("mensaje_salida") %>
		-->
		<table border="0" cellspacing="2" cellpadding="3">
		<tr><td class="tittabdat" colspan="2" nowrap> Seleccione la cuenta a eliminar</td></tr>
		<tr>
			<td class="textabdatcla" valign="top" colspan="2">
			<table border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center">
				<input type="text" name=textcuenta  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="">
				<A HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle></A>
				<input type="hidden" name="cuenta" value="">
				</td>
			</tr>
			</table>
			</td>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td align="center">
		<table width="68" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="22">
		<tr>
			<td align="center">
			<input type="image" border="0" name="Baja" src="/gifs/EnlaceMig/gbo25490.gif" width="68" height="22" alt="Baja">
			</td>
		</tr>
		</table>
		</td>
	</tr>
	</table>

	<INPUT TYPE=hidden NAME=cuentaseliminar VALUE=0>
	<INPUT TYPE=hidden NAME=contctaseliminar VALUE=0>
	</FORM>

	<!-- CONTENIDO FINAL -->

</body>

</html>

<Script language = "JavaScript">

	function VentanaAyuda(ventana)
		{
		hlp=window.open("/EnlaceMig/ayuda.html#" + ventana ,"hlp","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
		hlp.focus();
		}

</Script>