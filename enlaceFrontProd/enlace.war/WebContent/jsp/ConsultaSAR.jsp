<html>
<head>

<title>SAR - Consulta / Cancelaci&oacute;n de Pagos</title>

<!-- #BeginEditable "MetaTags" -->
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Proyecto" content="SAR Fase II">
<meta name="Version" content="1.0">
<meta name="Ultima version" content="">
<meta name="Desarrollo" content="Getronics Mexico">
<!-- #EndEditable -->


<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<Script language = "Javascript" >

<%
  String fechaHoy="";
  if(request.getAttribute("FechaHoy")!= null)
    fechaHoy=(String)request.getAttribute("FechaHoy");
%>

//Indice Calendario
var Indice=0;

//Arreglos de fechas
<%
  if(request.getAttribute("VarFechaHoy")!= null)
    out.print(request.getAttribute("VarFechaHoy"));
%>
//Dias inhabiles
diasInhabiles='<% if(request.getAttribute("DiasInhabiles")!= null) out.print(request.getAttribute("DiasInhabiles")); %>';


<%= request.getAttribute("newMenu") %>


/***************   Inician Funciones Javascript       **********

 @param s cadena
 @return boolean true, false
 @descripcion verifica se una cadena esta vacia
*/
function isEmpty(s)
{
  return ((s == null) || (trimString(s).length == 0));
}

/**
 @param str cadena
 @return str cadena sin espacios
 @descripcion elimina espacios a la derecha y a la izquierda de una cadena
*/
function trimString (str)
{
  str = this != window? this : str;
  return str.replace(/^\s+/g, '').replace(/\s+$/g, '');  //Quita espacios
}

/**
 @param num cadena numerica
 @return boolean true, false
 @descripcion verifica una expresion para determinar si es tipo entera sin signo
  utilizando expresiones regulares
*/
function isInteger(num)
{
  var TemplateI =/^\d+$/;	 //Formato de numero entero sin signo
  return TemplateI.test(num); //Compara "num" con el formato "Template"
}

/**
 @param num cadena numerica
 @return boolean true, false
 @descripcion evalua una cadena numerica y determina si cumple con el formato
  float sin signo
  d(n)
  d(n).d(n)
  .d(n)
  d(n).
  utilizando expresiones regulares.
*/
function isFloat(num)
{
  var TemplateF=/^\d*\.{1}\d+$|^\d+\.{0,1}\d*$/;  //Formato de numero flotante sin signo
  return TemplateF.test(num);                   //Compara "num" con el formato "Template"
}

/**
 @param rfc cadena tipo rfc
 @return boolean true, false
 @descripcion valida una cadena y determina si cumple con el formato rfc.
  evalua:
  formato rfc personas fisicas sin homoclave
  formato rfc personas fisicas con homoclave
  formato rfc morales fisicas sin homoclave
  formato rfc morales fisicas con homoclave
  valida en fechas, los dias y meses validos
  valida para febrero unicamente dia hasta dia 29
*/
function verificaRFC(rfc)
{
  var fecha=6,mes=1,dia=1;
  var TemplateRFC=/^[a-z]{3,4}[0-9]{6}(\t*|[a-z0-9]{3})$/i;
  if(TemplateRFC.test(rfc))
   {
     fecha=(rfc.length==9 || rfc.length==12)?5:6;
	 mes=rfc.substring(fecha,fecha+2);
     dia=rfc.substring(fecha+2,fecha+4);
     if( (dia >0 && dia<=31) && (mes >0 && mes<=12) )
	    if( !(eval(mes)==2 && eval(dia) >=30) )
           return true;
   }
  return false;
}

function verificaFechas(ind_1,ind_2)
 {
	 var forma=document.FrmConsultaSAR;
	 
	 	var fechaIni = forma.Fecha1_01.value;
	   	var fechaFin = forma.Fecha1_02.value;
	   
	  	dia1[0] = fechaIni.substring(0,2);
	   	mes1[0] = fechaIni.substring(3,5);
	   	anio1[0] = fechaIni.substring(6,10);
	   
	   	dia1[1] = fechaFin.substring(0,2);
	   	mes1[1] = fechaFin.substring(3,5);
	   	anio1[1] = fechaFin.substring(6,10);

	 if(anio1[ind_1]==anio1[ind_2]){
	 	if(mes1[ind_1]>mes1[ind_2])
      	{
			cuadroDialogo("La fecha Final no debe ser menor a la inicial.",3);
        	return false;
      	}
     	if(mes1[ind_1]==mes1[ind_2])
      		if(dia1[ind_1]>dia1[ind_2])
       		{
		 		cuadroDialogo("La fecha Inicial debe ser menor a la final.",3);
         		return false;
       		}
	 }else if(anio1[ind_1]>anio1[ind_2]){
		 cuadroDialogo("La fecha Final no debe ser menor a la inicial.",3);
         return false;
	 }
     return true;
 }


function verificaReferencia()
 {
	var forma=document.FrmConsultaSAR;
	referencia=forma.referencia.value;
	if(!isEmpty(referencia))
	  if(!isInteger(referencia))
		{
		  cuadroDialogo("El n&uacute;mero de referencia no es v&aacute;lido.",3);
		  return false;
		}
	return true;
 }

function validaImportes()
{
  var forma=document.FrmConsultaSAR;
  var importeI=trimString(forma.importeInicial.value);
  var importeF=trimString(forma.importeFinal.value);

  if(!verificaImporte(forma.importeInicial,forma.impInicial) || !verificaImporte(forma.importeFinal,forma.impFinal))
	return false;
  if( (importeI!="" && importeF=="") || (importeI=="" && importeF!="") )
	{
	  cuadroDialogo("No se puede especificar solo un importe,<br> introduzca los dos para realizar la consulta.",3);
	  return false;
	}
  if(importeI>importeF)
	{
	  cuadroDialogo("El importe final debe ser mayor al importe inicial.",3);
	  return false;
	}
  return true;
}


function verificaImporte(objOrigen,objDestino)
 {
	var importe=objOrigen.value;
	var ceros="000";

	if(!isEmpty(importe))
	 {
	  if(!isFloat(importe))
		{
		  cuadroDialogo("El importe no es v&aacute;lido.",3);
		  return false;
		}
	   else
	    {
		   if(importe.indexOf(".")>=0)
			{
			  enteros=importe.substring(0,importe.indexOf("."));
			  decimales=importe.substring(importe.indexOf(".")+1);
			  if(decimales.length>2)
				{
				  cuadroDialogo("Solo se permiten 2 decimales en importes.",3);
				  return false;
				}
			  if(decimales.length<2)
				  decimales+=ceros.substring(0,2-decimales.length);
			}
		   else
			{
			  enteros=importe;
			  decimales="00";
			}
		  objDestino.value=enteros+decimales;
	    }
	 }
	return true;
 }



function js_consultar()
 {
	var forma=document.FrmConsultaSAR;
	if(isEmpty(forma.Fecha1_01.value) || isEmpty(forma.Fecha1_02.value))
	 {
		cuadroDialogo("Debe especificar un rango de fechas",3);
		return;
	 }
    if(!verificaFechas(0,1))
	return;
	if( !validaImportes() || !verificaReferencia())
	  return;
	if(!isEmpty(forma.RFC.value))
	  if(!verificaRFC(forma.RFC.value))
	  {
		  cuadroDialogo("El formato del RFC no es v&aacute;lido",3);
		  return;
	  }

    forma.action="ConsultaUsuarios";
    forma.submit();
 }


function FrmClean()
 {
   document.FrmConsultaSAR.reset();
 }

function Actualiza()
 {
   if(Indice==0)
     document.FrmConsultaSAR.Fecha1_01.value=Fecha[Indice];
   if(Indice==1)
     document.FrmConsultaSAR.Fecha1_02.value=Fecha[Indice];
 }

function js_calendario(ind)
 {
    var m=new Date()

	Indice=ind;
    n=m.getMonth();
    msg=window.open("/EnlaceMig/EI_Calendario.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
    msg.focus();
 }

var ctaselec;
var ctadescr;

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}
function actualizacuenta()
{
  document.FrmConsultaSAR.cuenta.value=ctaselec;
  document.FrmConsultaSAR.cuentaCargo.value=ctaselec+" "+ctadescr;
}

function EnfSelCta()
{
   document.FrmConsultaSAR.cuentaCargo.value="";
}

/**********************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>

<!---------------------------------------------------------------------------------------------->


<script language="JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript"src="/EnlaceMig/scrImpresion.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');EnfSelCta();" background="/gifs/EnlaceMig/gfo25010.gif">

<!-- #BeginLibraryItem "/Library/navegador.lbi" -->
<table width="571" border="0" cellspacing="0" cellpadding="0">
  <tr valign=top>
    <td width="*">
		<!-- MENU PRINCIPAL -->
        <%= request.getAttribute("MenuPrincipal") %>
    </td>
  </tr>
</table>
<!-- #EndLibraryItem --><!-- #BeginEditable "Contenido" -->

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="676" valign="top" align="left">
      <table width="666" border="0" cellspacing="6" cellpadding="0">
        <tr>
          <td width="528" valign="top" class="titpag"><%= request.getAttribute("Encabezado") %></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <form name="FrmConsultaSAR"  action="ConsultaUsuarios">
    <tr>
      <td align="center">
        <table width="670" border="0" cellspacing="2" cellpadding="3" bgcolor="#FFFFFF">
          <tr>
            <td class="tittabdat"> <p><strong>Selecci&oacute;n de Filtro:</strong></p>
              </td>
          </tr>
          <tr align="center">
            <td height="120" valign="top" class="textabdatcla"> <table width="660" border="0" cellspacing="0" cellpadding="0">
                <!--DWLayoutTable-->
                <tr valign="top">
                  <td width="420" rowspan="2" align="left"> <table width="420" height="65" border="0" cellpadding="5" cellspacing="0">
                      <!--DWLayoutTable-->
                      <tr>
                        <td width="149" height="32" align="right" nowrap class="tabmovtex">Cuenta
                          de cargo:</td>
                        <td nowrap class="tabmovtex"> <input name=cuentaCargo type="text" class="tabmovtexbol" id="cuentaCargo" onfocus="blur();" value="" size=30 maxlength=30>
                          <A HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle></A>
                          <input type="hidden" name="cuenta" value=""> </td>
                      </tr>
                      <tr>
                        <td height="32" align="right" class="tabmovtex">De la
                          fecha:<br> </td>
                        <td valign="middle" nowrap class="tabmovtex"> <input name="Fecha1_01" type="text" class="tabmovtex" id="Fecha1_01" OnFocus = "blur();" value='<%=fechaHoy%>'  size="12">
                          <a href="javascript:js_calendario(0);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></a></td>
                      </tr>
                      <tr>
                        <td height="32" align="right" class="tabmovtex">A la fecha:</td>
                        <td nowrap class="tabmovtex"> <input name="Fecha1_02" type="text" class="tabmovtex" id="Fecha1_02" OnFocus = "blur();" value='<%=fechaHoy%>' size="12">
                          <a href="javascript:js_calendario(1);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></a></td>
                      </tr>
                      <tr>
                        <td height="32" align="right" class="tabmovtex">Importe
                          inicial:</td>
                        <td nowrap class="tabmovtex"> <input name="importeInicial" type="text" class="tabmovtex" id="importeInicial" size="15" maxlength=15>
                        </td>
                      </tr>
                      <tr>
                        <td height="32" align="right" class="tabmovtex">Importe
                          final:</td>
                        <td nowrap class="tabmovtex"><input name="importeFinal" type="text" class="tabmovtex" id="importeFinal" size="15" maxlength=15></td>
                      </tr>
                      <tr>
                        <td height="32" align="right" class="tabmovtex">Tipo de
                          L&iacute;nea de Captura:</td>
                        <td nowrap class="tabmovtex"><input name="chkTipoLC" type="radio" value="00" checked>
                          Todas</td>
                      </tr>
                      <tr>
                        <td height="32">&nbsp;</td>
                        <td nowrap class="tabmovtex"> <input name="chkTipoLC" type="radio" value="85">
                          Retiro y Vivienda </td>
                      </tr>
                      <tr>
                        <td height="32">&nbsp;</td>
                        <td nowrap class="tabmovtex"><input type="radio" name="chkTipoLC" value="86">
                          Pagos Extempor&aacute;neos </td>
                      </tr>
                      <tr>
                        <td height="32">&nbsp;</td>
                        <td nowrap class="tabmovtex"> <input type="radio" name="chkTipoLC" value="87">
                          Ahorro Voluntario </td>
                      </tr>
                      <tr>
                        <td height="32">&nbsp;</td>
                        <td nowrap class="tabmovtex"><input type="radio" name="chkTipoLC" value="88">
                          Cr&eacute;dito de Vivienda </td>
                      </tr>
                      <tr>
                        <td height="32">&nbsp;</td>
                        <td nowrap class="tabmovtex"><input type="radio" name="chkTipoLC" value="89">
                          Ahorro Voluntario Cetes </td>
                      </tr>
                      <tr>
                        <td height="32"></td>
                        <td width="251"></td>
                      </tr>
                    </table></td>
                  <td width="240" height="207" align="left" valign="top"> <table width="240" height="169" border="0" cellpadding="3" cellspacing="0">
                      <tr>
                        <td width="107" height="33" align="right" nowrap class="tabmovtex">RFC
                          dependencia:</td>
                        <td colspan="2" align="left" valign="middle" class="tabmovtex">
                          <input name="RFC" type="text" class="tabmovtex" id="RFC2" size="15" maxlength=12></td>
                      </tr>
                      <tr>
                        <td height="33" align="right" nowrap class="tabmovtex">Referencia:</td>
                        <td colspan="2" align="left" valign="middle" class="tabmovtex">
                          <input name="referencia" type="text" class="tabmovtex" id="referencia2" size="15" maxlength=12>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="3" align="right" nowrap class="tabmovtex">&nbsp;
                        </td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex" nowrap>Estatus:</td>
                        <td width="15" align="center" valign="middle" class="tabmovtex"><input name="chkEstatusArc" type="radio" value="A" checked>
                        </td>
                        <td width="100" nowrap class="tabmovtex">Pagados</td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex" nowrap>&nbsp;</td>
                        <td class="tabmovtex" align="center" valign="middle"><input type="radio" name="chkEstatusArc" value="C">
                        </td>
                        <td class="tabmovtex" nowrap>Cancelados</td>
                      </tr>
                      <tr>
                        <td height="25" align="right" nowrap class="tabmovtex">&nbsp;</td>
                        <td class="tabmovtex" align="center" valign="middle"><input type="radio" name="chkEstatusArc" value="R">
                        </td>
                        <td class="tabmovtex" nowrap>Rechazados</td>
                      </tr>
                    </table>
                    <p>&nbsp;</p></td>
                </tr>
                <tr valign="top">
                  <td height="161">&nbsp;</td>
                </tr>
              </table></td>
          </tr>
        </table>
        <br>

		<table width="166" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="22">
          <tr>
            <td align="right" valign="top" height="22" width="90"><a href="javascript:js_consultar();">
              <img border="0" name=Enviar value=Enviar src="/gifs/EnlaceMig/gbo25220.gif" width="90" height="22" alt="Consultar"></td>
            <td align="left" valign="middle" height="22" width="76"><a href="javascript:FrmClean();">
              <img name=limpiar value=Limpiar border="0" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar">
			  </a></td>
          </tr>
        </table></td>
    </tr>
	<input type="hidden" value="7" name="Modulo">
	<input type="hidden" name="impInicial">
	<input type="hidden" name="impFinal">
  </form>
</table>
<!-- #EndEditable -->
</body>
<!-- #EndTemplate --></html>