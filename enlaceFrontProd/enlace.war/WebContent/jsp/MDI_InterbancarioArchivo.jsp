<html>
<head>
	<title>Deposito Interbancario</TITLE>
<script src="/EnlaceMig/jquery-1.11.2.min.js" type="text/javascript"></script>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" SRC= "/EnlaceMig/passmark.js"></script>
<script type="text/javascript" src="/EnlaceMig/FavoritosEnlaceJS.js"></script>
<script type="text/javascript">

<%
String ArchivoErr = "";
ArchivoErr = (String)request.getAttribute("ArchivoErr");
if(ArchivoErr==null)
	ArchivoErr = "";

%>
<%= ArchivoErr %>

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************************/

function Enviar()
 {
   /* CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token
 	 * ahora se enviara al servlet MDI_Enviar para que primero valide
 	 * el token y despues de ahi se redirige al servlet MDI_Cotizar*/
//    document.tabla.action="MDI_Cotizar";
   document.tabla.action="MDI_Enviar";
   document.tabla.submit();
 }

 function ValidaTabla(forma)
 {
   var cont=0;
   var str="";
   var chb;
   var txtFav="";
   var strDescFavoritos="";


      for(i2=0;i2<forma.length;i2++)
       {
         if(forma.elements[i2].type=='checkbox')
          {
        	 chb = forma.elements[i2];

            if(forma.elements[i2].checked==true)
             {
               str+="1";
               var descFav;        
               if((chb.id).indexOf('favChb')>-1){
               	   descFav = trimString(document.getElementById("favTxt"+(chb.id).substring(6)).value);
               	   if( descFav === '' ){
			   			cuadroDialogo("No se ha proporcionado una descripci&oacute;n para el favorito.",3);
     					return false;
			       }
			       if(!validaEspecialesFavorito(document.getElementById("favTxt"+(chb.id).substring(6)),"Favoritos"))
		    	   {	
		    			return false;
		    	   }
            	   txtFav += forma.elements[i2+1].value +"@";
               } else {
               		cont++;
               }               
             }
            else
              str+="0";
          }
       }
      /*if(cont==0)
       {
         cuadroDialogo("Debe Seleccionar m&iacute;nimo una transferencia.",3);
         return false;
       }*/

      forma.strCheck.value=str;
      forma.strFav.value=txtFav;

   return true;
 }

 //jppm función que realiza la validación del archivo incidencia IM199738
 function ValidaDuplicados()
 {

	 if(ValidaTabla(document.tabla))
   {
		document.tabla.action="MDI_Interbancario";
		document.tabla.submit();
	}
 }

 function modificarTxt(chbFavorito,numRen){
	   var txtFavorito = document.getElementById("favTxt"+numRen);

	   if(chbFavorito.checked == 0){ //deshabilitado
		   txtFavorito.disabled = true;
		   txtFavorito.value ="";
	   }else{
		   txtFavorito.disabled = false;
		   txtFavorito.value ="";
	   }
}

function archivoErrores()
 {
   ventanaInfo1=window.open('','trainerWindow','width=450,height=300,toolbar=no,scrollbars=yes');
   ventanaInfo1.document.open();
   ventanaInfo1.document.write("<html>");
   ventanaInfo1.document.writeln("<head>\n<title>Estatus</title>\n</head>");
   ventanaInfo1.document.writeln("<body bgcolor='white'>");
   ventanaInfo1.document.writeln("<center>");
   ventanaInfo1.document.writeln("<table border=0 width=420>");
   ventanaInfo1.document.writeln("<form>");
   ventanaInfo1.document.writeln("<tr><th bgcolor='red'><font color='white'>informacion del Archivo Importado</font></th></tr>");
   ventanaInfo1.document.writeln("<tr><td>");
   ventanaInfo1.document.writeln(document.tabla.archivoEstatus.value);
   ventanaInfo1.document.writeln("</td></tr>");
   //ventanaInfo1.document.writeln("<tr><td align=center><br><input type=button value=' Cerrar ventana ' onClick='window.close();'></td></tr>");
   ventanaInfo1.document.writeln("<tr><td><br></td></tr>");
   ventanaInfo1.document.writeln("<tr><td align=center><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/cerrar.gif' border=0></a></td></tr>");
   ventanaInfo1.document.writeln("</form>");
   ventanaInfo1.document.writeln("</table>");
   ventanaInfo1.document.writeln("</center><p>");
   ventanaInfo1.document.writeln("</body>\n</html>");
   ventanaInfo1.focus();
}


function operacionesEnDolaresExport(){
	var count = 1;

	if($("#divisaOper").val() === "USD"){
		$("input:checkbox").each(   
   			function() {
   				$("#favChb"+count).attr('disabled', true);     
   				count++;   	
    		}    		
		);
	}		
}

<%
String newMenu = "";
newMenu = (String)request.getAttribute("newMenu");
if(newMenu==null)
	newMenu = "";
%>

<%= newMenu %>
</script>


<%--  OPC:PASSMARK 16/12/2006 ******************* BEGIN --%>
<%
	String FSOnuevo = (String)request.getAttribute("_fsonuevoMID");
	if( FSOnuevo!=null ) {
%>

	<SCRIPT type="text/javascript">
	  var expiredays = 365;
  	  var ExpireDate = new Date ();
      ExpireDate.setTime(ExpireDate.getTime() + (expiredays * 24 * 3600 * 1000));
      if (!(num=GetCookie("PMDATA"))) { }
	  DeleteCookie("PMDATA");
      SetCookie("PMDATA", "<%=FSOnuevo%>", ExpireDate,"/");
	</SCRIPT>
<%
	 request.removeAttribute("_fsonuevoMID");
	}
%>
<%--  OPC:PASSMARK 16/12/2006 ******************* END --%>


<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>

<body onLoad="operacionesEnDolaresExport();" background="/gifs/EnlaceMig/gfo25010.gif" style=" bgColor: #ffffff;" leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<%-- MENU PRINCIPAL --%>
<%
String MenuPrincipal = "";
MenuPrincipal = (String)request.getAttribute("MenuPrincipal");
if(MenuPrincipal==null)
	MenuPrincipal = "";
%>

	<%= MenuPrincipal %>
   </td>
  </tr>
</table>

<%
String Encabezado ="";
Encabezado = (String)request.getAttribute("Encabezado") ;
if (Encabezado==null)
	Encabezado = "";
%>

<%= Encabezado %>

 <FORM  NAME="tabla" method=post action="MDI_Cotizar">
  <input type="hidden" name="divTrx" value="<%= request.getAttribute("divisaImportArchivo") %>"/> 
 <input type="hidden" id="divisaOper" name="divisaOper" value="<%= request.getAttribute("divisaImportArchivo") %>"/>
   <p>
   <%-- Datos del Usuario --%>
   <%--
   <table border=0 align=center>
   <tr>
     <th bgcolor='#DDDDDD'><font color=black>Datos del Usuario</font></th>
   </tr>
   <tr>
     <td align="center">
       <font size="2" face="Arial, Helvetica, sans-serif">
	     <b>Contrato: </b><%= request.getAttribute("NumContrato") %> <%= request.getAttribute("NomContrato") %>
	   </font>
	 </td>
   </tr>
   <!--
   <tr>
     <td bgcolor='#FFFFFF'>
	   <b>Usuario: </b><%= request.getAttribute("NumUsuario") %> <%= request.getAttribute("NomUsuario") %>
	 </td>
   </tr>
   <tr>
     <td><br></td>
   </tr>
   //--%>

  <table border=0 align="center" nowrap>
   <tr>
<%
String importeTotal = "";
importeTotal = (String)request.getAttribute("importeTotal");
if(importeTotal==null)
	importeTotal="";
%>

 <td align="right" class="textabref">N&uacute;mero de Registros <span class="texinfo2"><%= request.getAttribute("Lineas") %></span> Importe Total <span class="texinfo2"><%= importeTotal %> 	
     </span>&nbsp;<span class="texinfo2"><%= request.getAttribute("divisaImportArchivo") %></span></td>
   </tr>
   <tr>
<%
String Tabla = "";
Tabla = (String)request.getAttribute("Tabla");
if(Tabla ==null)
	Tabla = "";
%>

     <td><%= Tabla %></td>
   </tr>

   <tr>
    <td></br></td>
   </tr>

   <tr>
    <td align="center">

	<%
	String Botones = "";
	Botones = (String) request.getAttribute("Botones");
	if(Botones ==null)
		Botones = "";

	%>
     <%= request.getAttribute("Botones") %>
	</td>
   </tr>
  </table>


   <input type="hidden" name="Lineas" value="<%= request.getAttribute("Lineas") %>"/>
   <input type="hidden" name="Importe" value="<%= request.getAttribute("importeTotal") %>"/>
   <input type="hidden" name="Modulotabla" value=<%= request.getAttribute("ModuloTabla") %>>
   <input type="hidden" name="TransReg" value="<%= request.getAttribute("TransReg") %>"/>
   <input type="hidden" name="TransNoReg" value="<%= request.getAttribute("TransNoReg") %>"/>

   <input type="hidden" name="archivoEstatus" value="<%= request.getAttribute("archivoEstatus") %>"/>
   <input type="hidden" name="Registradas" value="<%=request.getAttribute("Registradas") %>"/>
   <input type="hidden" name="Modulo" value="2"><%--jppm--%>
   <input type="hidden" name="ArchivoNomInter" value="<%= request.getAttribute("ArchivoNomInter") %>"/>


	<!-- Indra PYME Mayo Importar archivo programadas -->
	<input type="hidden" name="fecha_completaReg" value="<%= request.getAttribute("fecha_completaReg") %>"/>
   <input type="hidden" name="Fecha" value="<%= request.getAttribute("Fecha") %>"/>
   <input type="hidden" name="strArc" value="<%= request.getAttribute("strArc") %>"/>
    <input type="hidden" name="TransArch" value="<%= request.getAttribute("TransArch") %>"/>
    <input type="hidden" name="strCheck">
   <input type="hidden" name="strFav" />

	<!-- Indra PYME Mayo Importar archivo programadas -->
 </FORM>
 </center>

</BODY>
</HTML>
<%-- 2007.01 --%>