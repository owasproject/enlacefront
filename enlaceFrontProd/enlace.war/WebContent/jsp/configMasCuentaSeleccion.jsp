<%@ page import="java.util.List, javax.servlet.http.HttpServletRequest" %>
<%@ page import="java.util.ArrayList, mx.altec.enlace.beans.ConfEdosCtaArchivoBean" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Selecci&oacute;n de Cuentas</title>

<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<link href="/EnlaceMig/consultas.css" rel="stylesheet" type="text/css" id = "estilos"/>

<script type="text/javascript">

<%if(request.getAttribute("TiempoTerminado")!=null) out.print(request.getAttribute("TiempoTerminado"));%>

/******************  ********* ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/
<%
       if (request.getAttribute("newMenu")!= null) {
       out.println(request.getAttribute("newMenu"));
       }
%>

function EnviarForma() {

	if(validarDatos()){
		var formato = document.getElementById("formato").value;
		if(formato == '1'){
			document.getElementById("accion").value = "1";
		}	
		recuperarValores();	
		document.forms["formulario"].submit();
	}
	  
}
function SelectMoveAll(SS1,SS2) 
{
	var SelID='';
    var SelText='';
	for (i=SS1.options.length - 1; i>=0; i--)
	{
		    SelID=SS1.options[i].value;
            SelText=SS1.options[i].text;
            var newRow = new Option(SelText,SelID);
            SS2.options[SS2.length]=newRow;
            SS1.options[i]=null;
	}
	SelectSort(SS2);
}
function SelectMoveRows(SS1,SS2)
{
    var SelID='';
    var SelText='';
    // Move rows from SS1 to SS2 from bottom to top
    for (i=SS1.options.length - 1; i>=0; i--)
    {
        if (SS1.options[i].selected == true)
        {
            SelID=SS1.options[i].value;
            SelText=SS1.options[i].text;
            var newRow = new Option(SelText,SelID);
            SS2.options[SS2.length]=newRow;
            SS1.options[i]=null;
        }
    }
    SelectSort(SS2);
}

function SelectSort(SelList)
{
    var ID='';
    var Text='';
    for (x=0; x < SelList.length - 1; x++)
    {
        for (y=x + 1; y < SelList.length; y++)
        {
            if (SelList[x].text > SelList[y].text)
            {
                // Swap rows
                ID=SelList[x].value;
                Text=SelList[x].text;
                SelList[x].value=SelList[y].value;
                SelList[x].text=SelList[y].text;
                SelList[y].value=ID;
                SelList[y].text=Text;
            }
        }
    }
}

function validarDatos(){
	var longitud = document.formulario.configurar.options.length;
	if(longitud > 0){
		return true;
	}else{
		cuadroDialogo('Por favor, seleccione una cuenta para continuar con el proceso de modificaci\u00f3n',4);
		return false;
	}
}

function recuperarValores(){
	var cadena = ''; 
	SS1 = document.formulario.configurar;
	
	for (var i = 0; i < SS1.options.length; i++) {
	    SelText=SS1.options[i].text;
	    cadena += SelText+'#'; 
    }
    document.getElementById("cadenaCuenta").value = cadena;    
}
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor="#ffffff" leftMargin="0" topMargin="0" marginwidth="0" marginheight="0" onLoad="">

<table border="0px" cellpadding="0px" cellspacing="0px" width="571px">
  <tr valign="top">
   <td width="*">
	<%
           if (request.getAttribute("MenuPrincipal")!= null) {
           out.println(request.getAttribute("MenuPrincipal"));
           }
    %>
   </td>
  </tr>
</table>

<%
           if (request.getAttribute("Encabezado")!= null) {
           out.println(request.getAttribute("Encabezado"));
           }


%>

<form  name="formulario" id="formulario" method="post" action="ConfigMasPorCuentaServlet">
	<input type="hidden" id="accion" name="accion" value=""/>
	<input type="hidden" id="cadenaCuenta" name="cadenaCuenta" value=""/>
	  <p/>
	  	<table width="760" cellspacing="1" cellpadding="1" border="0" class="tabfonbla" style="font-weight:bold;">
	  		<thead class="tittabdat">
	  			<tr>
	  			<td>Seleccione las cuentas para las que desea modificar el estatus de env&iacute;o</td>
	  			</tr>
	  		</thead>
			<tbody class="textabdatcla">
				<tr>
					<td align="center">
						<table align="center" border="0px" cellspacing="0px" cellpadding="3px" class="textabdatcla" width="450px">
							<tr>
								<td>
					    			<table border="0" width="400px" height="100px" align="center">
					    				<tr>
											<td colspan="1" align="center"> 
												<select id="cuentas" name="cuentas" multiple="multiple" size="10" style="width:300px;">
													<%
													List lista = new ArrayList();
													try{
							   							lista = request.getSession().getAttribute("listaCuentasEdoCta") != null ? (List)request.getSession().getAttribute("listaCuentasEdoCta") : null;
							   							
							   						 }catch(NullPointerException e){
							   						 	System.out.println("La lista viene vacia.-->"+e.getMessage());
							   						 	e.printStackTrace();
							   						 }
							   						 	if(lista != null && lista.size() > 0){
							   						 		for(int i = 0 ; i < lista.size(); i++) { 
																try{	
																	if(((ConfEdosCtaArchivoBean)lista.get(i)).getNomCuenta().length() != 16){	   					
							   						%>	
																		<option value="<%= ((ConfEdosCtaArchivoBean)lista.get(i)).getNomCuenta() %>">
																			<%= ((ConfEdosCtaArchivoBean)lista.get(i)).getNomCuenta() %> <%= ((ConfEdosCtaArchivoBean)lista.get(i)).getNombreTitular() %>
																		</option>
													<%	
																	}
																}catch(NullPointerException e){
																System.out.println("Excepcion YYY.-->"+e.getMessage());
																e.printStackTrace();
																}
															}
														}else{
													 %>
													 	<script type="text/javascript">
													 		cuadroDialogo("No existen cuentas asociadas para modificar el estatus de env&iacute;o del estado de cuenta impreso",4);
													 	</script>
													 <%} %>
												</select>
											</td>
											<td>
												<input type="button" style="width:40px;" id="agregarT" name="agregarT" value="&gt;&gt;" onclick="SelectMoveAll(document.formulario.cuentas,document.formulario.configurar)"/><br/>
												<input type="button" style="width:40px;" id="agregar" name="agregar" value="&gt;" onclick="SelectMoveRows(document.formulario.cuentas,document.formulario.configurar)"/><br/>
												<input type="button" style="width:40px;" id="quitar" name="quitar" value="&lt;" onclick="SelectMoveRows(document.formulario.configurar,document.formulario.cuentas)"/><br/>
												<input type="button" style="width:40px;" id="quitarT" name="agregarT" value="&lt;&lt;" onclick="SelectMoveAll(document.formulario.configurar,document.formulario.cuentas)"/><br/>
												
											</td>
											
											<td colspan="1" align="center"> 
												<select id="configurar" name="configurar" multiple="multiple" size="10" style="width: 300px">
													
												</select>
											</td>
										</tr>
					    			</table>
					    		</td>
					    	</tr>
						</table>
	
						  <br/>
						  <table border="0px" cellpadding="0px" cellspacing="0px" align="center">
						   <tr>
						     <td>
						     	<a href="javascript:EnviarForma();"><img src="/gifs/EnlaceMig/gbo25280.gif" border="0"/></a>
						     	<input type="hidden" id="formato" name="formato" value="1" />
						     </td>	
						   </tr>
						  </table>
					</td>
				</tr>
			</tbody>
		</table>
		<br/>
		<br/>
<%	if(request.getAttribute("folio") != null){ %>
		<script type="text/javascript">
			cuadroDialogo("Se ha generado el n&uacute;mero de folio <%=request.getAttribute("folio")%>.\n Utilice este n&uacute;mero para consultar el estatus de la modificaci&oacute;n en el modulo de consulta.",1);
		</script>
<%	} %>
</form>

</body>
</html>